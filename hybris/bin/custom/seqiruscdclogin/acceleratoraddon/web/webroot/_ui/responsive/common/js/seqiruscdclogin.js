/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

gigyaHybris = window.gigyaHybris || {};
gigyaHybris.gigyaFunctions = gigyaHybris.gigyaFunctions || {};

// Request Id Token from CDC 
window.__gigyaConf = {include:'id_token'};

gigyaHybris.gigyaFunctions.raasLogin = function(response) {
	var sourceUrl = window.location.pathname;
	var queryString = window.location.search;
	createRememberMeCookie();
    jQuery.ajax(ACC.config.contextPath + "/gigyaraas/login", {
        data: {
            gigyaData: JSON.stringify(response)
        },
        dataType: "json",
        type: "post"
    }).done(function(data, textStatus, jqXHR) {
        if (data.code !== 0) {
            ACC.colorbox.open(data.message,{
              html : $(document).find("#dialog").html(),
              maxWidth:"100%",
              width:"420px",
              initialWidth :"420px",
              height:"300px"
            });
        } else {
        	if(sourceUrl=='/login' || sourceUrl==ACC.config.contextPath+'/login')
        	{
        		window.location = ACC.config.contextPath + '/';
        	}
        	else if (queryString=='?su=ja')
        	{
        		window.location = ACC.config.contextPath + '/joinaccount';
        	}
        	else
        	{
            	window.location = ACC.config.contextPath + '/businessDetails';
            }
        }
    });
};

function createRememberMeCookie()
{
      if ($('#gigya-checkbox-remember').is(':checked') || $('#remember_me').is(':checked'))
      {
            var username = $('#username').val();
            // alert(username);
            $.cookie('remember-me', username, {
						expires : 3
	               });
	               
	             //  alert("done");
      }
      else
      {
            try 
            {
                  document.cookie = "remember-me = ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
            } 
            catch (e) {}
      }
}

gigyaHybris.gigyaFunctions.raasEditProfile = function(response) {
	//alert("in raasEditProfile");
    $.ajax(ACC.config.contextPath + "/gigyaraas/profile", {
        data: {
            gigyaData: JSON.stringify(response.response)
        },
        dataType: "json",
        type: "post"
    }).done(function(data, textStatus, jqXHR) {
    //alert("data.code:"+data.code)
        if (data.code !== 0) {
            ACC.colorbox.open(data.message,{
                html : $(document).find("#dialog").html(),
                maxWidth:"100%",
                width:"420px",
                initialWidth :"420px",
                height:"300px"
              });
        } else {
          		var customerData = data.customerData;
				    $('#profile_name').html(customerData.firstName+" "+customerData.lastName);
				    $('#profile_job').html(customerData.jobTitle);
				    $('#profile_phone').html(customerData.phoneNumber);
				    $('.profile_header').html(customerData.firstName+" "+customerData.lastName);
				    $('.profile-details').show();		
           // window.location.reload(false);
        }
    });
};


gigyaHybris.gigyaFunctions.raasClick = function() {
    $(".gigya-raas-link").click(
        function(event) {
            event.preventDefault();
            var id = $(this).attr("data-gigya-id");
            //alert("In raasClick, show screenset of id:"+id);
            gigya.accounts.showScreenSet(window.gigyaHybris.raas[id]);
        });
};


gigyaHybris.gigyaFunctions.raasEmbed = function() {
    if (gigyaHybris.raas) {
        $.each(gigyaHybris.raas, function(name, params) {
       // alert("In raasEmbed, params:"+params.screenSet);
            if(!params.profileEdit && params.containerID){
               // gigya.accounts.showScreenSet(params);
               customLangParams={
invalid_login_or_password: 'The Email Address or Password you entered does not match our records. Please try again or click Forgot Password'
};
params={
    screenSet: params.screenSet,
    startScreen : params.startScreen,
    containerID : params.containerID,
    customLang: customLangParams
};
gigya.accounts.showScreenSet(params);
            }
            
            if(params.profileEdit && params.containerID){
           // alert("-->ProfileEdit: profiledit, screenset:"+params.screenSet);
                gigya.accounts.showScreenSet({
                    screenSet : params.screenSet,
                    startScreen : params.startScreen,
                    containerID : params.containerID,
                    onAfterSubmit : gigyaHybris.gigyaFunctions.raasEditProfile
                });
            }
        });
    }
};

gigyaHybris.gigyaFunctions.setAccountResidency = function(accountResidency) {
    gigya.setAccountResidency(accountResidency);
};

/*
 * Register login events
 */
function gigyaRegister() {
//alert("in gigyaRegister");
    if (ACC.gigyaUserMode === "raas") {
        gigya.accounts.addEventHandlers({
            onLogin: gigyaHybris.gigyaFunctions.raasLogin
        });
    }
}

function interceptLogoutClickEvent(e) {
    var target = e.target || e.srcElement;
    if (target.tagName === 'A' && target.getAttribute('href').endsWith('/logout')) {
        gigya.accounts.logout();
    }
}


$(document).ready(function() {
    gigyaRegister();
    gigyaHybris.gigyaFunctions.raasClick();
    gigyaHybris.gigyaFunctions.raasEmbed();
    if (document.addEventListener) {
        document.addEventListener('click', interceptLogoutClickEvent);
    } else if (document.attachEvent) {
        document.attachEvent('onclick', interceptLogoutClickEvent);
    }
});

