/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.seqiruscdclogin.controllers;

/**
 */
public interface SeqiruscdcloginControllerConstants
{
	// implement here controller constants used by this extension
}
