/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.seqiruscdclogin.security;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.util.CookieGenerator;

import com.seqirus.seqiruscdclogin.constants.SeqiruscdcloginConstants;
import com.seqirus.seqiruscdclogin.controllers.ControllerConstants;

import de.hybris.platform.acceleratorstorefrontcommons.security.StorefrontLogoutSuccessHandler;
import de.hybris.platform.servicelayer.config.ConfigurationService;

/**
 * Logout success handler to handle logout with gigya session.
 */
public class DefaultGigyaLogoutSuccessHandler extends StorefrontLogoutSuccessHandler 
{
	/** The configuration service. */
	private ConfigurationService configurationService;

	/** The cookie generator. */
	private CookieGenerator cookieGenerator;

	/** The redirect strategy. */
	@Resource
	private RedirectStrategy redirectStrategy;

	/**
	 * Sets the redirect strategy.
	 *
	 * @param redirectStrategy the new redirect strategy
	 */
	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	/**
	 * On logout success.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param authentication the authentication
	 * @throws IOException      Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Override
	public void onLogoutSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException, ServletException {
		// delete cookies created for managing session with gigya if required
		final Cookie gigyaExpCookie = getCookieWithPrefix(request, ControllerConstants.GLT_EXP_COOKIE);
		if (gigyaExpCookie != null) {
			removeCookie(response, gigyaExpCookie);
		}

		final Cookie siteCookie = getCookieWithSuffix(request, ControllerConstants.LOGION_TOKEN);
		if (siteCookie != null) {
			removeCookie(response, siteCookie);
		}
		// Redirect to changepasswordsuccess page if invoked from change password page
		redirectAfterChangePassword(request, response);
		
		super.onLogoutSuccess(request, response, authentication);
	}

	/**
	 * Redirect after change password.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void redirectAfterChangePassword(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException 
	{
		String referer = request.getHeader(SeqiruscdcloginConstants.REFERER);
		String queryString = request.getQueryString();

		if (StringUtils.isNotBlank(queryString)) 
		{
			String[] queryParams = queryString.split("&");
			String cpsParam = queryParams[0];
			String siteParam = queryParams[1];

			if (referer.contains(SeqiruscdcloginConstants.CDC_CHANGE_PASSWORD_URL) && cpsParam.equals(SeqiruscdcloginConstants.CDC_CHANGE_PASSWORD_PARAM)) 
			{
				redirectStrategy.sendRedirect(request, response, SeqiruscdcloginConstants.CDC_CHANGE_PASSWORD_SUCCESS_URL + "?" + siteParam);
			}
		}
	}

	/**
	 * Removes the cookie.
	 *
	 * @param response       the response
	 * @param gigyaExpCookie the gigya exp cookie
	 */
	private void removeCookie(final HttpServletResponse response, Cookie gigyaExpCookie) {
		cookieGenerator.setCookieName(gigyaExpCookie.getName());
		cookieGenerator.removeCookie(response);
	}

	/**
	 * Gets the cookie with prefix.
	 *
	 * @param request the request
	 * @param prefix  the prefix
	 * @return the cookie with prefix
	 */
	private Cookie getCookieWithPrefix(HttpServletRequest request, String prefix) {
		return getCookieContainingCode(request, prefix, true);
	}

	/**
	 * Gets the cookie with suffix.
	 *
	 * @param request the request
	 * @param suffix  the suffix
	 * @return the cookie with suffix
	 */
	private Cookie getCookieWithSuffix(HttpServletRequest request, String suffix) {
		return getCookieContainingCode(request, suffix, false);
	}

	/**
	 * Gets the cookie containing code.
	 *
	 * @param request  the request
	 * @param code     the code
	 * @param isPrefix the is prefix
	 * @return the cookie containing code
	 */
	private Cookie getCookieContainingCode(HttpServletRequest request, String code, boolean isPrefix) {
		if (ArrayUtils.isNotEmpty(request.getCookies())) {
			for (final Cookie cookie : request.getCookies()) {
				if ((isPrefix && StringUtils.startsWith(cookie.getName(), code))
						|| (!isPrefix && StringUtils.endsWith(cookie.getName(), code))) {
					return cookie;
				}
			}
		}
		return null;
	}

	/**
	 * Gets the configuration service.
	 *
	 * @return the configuration service
	 */
	public ConfigurationService getConfigurationService() {
		return configurationService;
	}

	/**
	 * Sets the configuration service.
	 *
	 * @param configurationService the new configuration service
	 */
	@Required
	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	/**
	 * Gets the cookie generator.
	 *
	 * @return the cookie generator
	 */
	public CookieGenerator getCookieGenerator() {
		return cookieGenerator;
	}

	/**
	 * Sets the cookie generator.
	 *
	 * @param cookieGenerator the new cookie generator
	 */
	@Required
	public void setCookieGenerator(CookieGenerator cookieGenerator) {
		this.cookieGenerator = cookieGenerator;
	}
}
