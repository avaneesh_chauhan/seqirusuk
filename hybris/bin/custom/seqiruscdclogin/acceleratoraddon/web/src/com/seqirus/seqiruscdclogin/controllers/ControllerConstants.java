/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.seqiruscdclogin.controllers;

public interface ControllerConstants {
	final String ADDON_PREFIX = "addon:/seqiruscdclogin/";

	String GLT_COOKIE = "glt_";
	String GAC_COOKIE = "gac_";

	String GLT_EXP_COOKIE = "gltexp_";

	String LOGION_TOKEN = "-LoginToken";
	
	String SAME_SITE_ATTRIBUTE_LAX = "; SameSite=Lax";
	
	String GAC_COOKIE_VALUE = "gacCookieValue";

	String GAC_COOKIE_NAME = "gacCookieName";
	
	String GLT_EXP_COOKIE_SAMESITE_ATTR_VAL="seqiruscdclogin.gltexp.cookie.samesite.value";

	interface Views {
		interface Pages {
			interface Account {
				String AccountLoginPage = ADDON_PREFIX + "pages/account/accountLoginPage";// NOSONAR
			}

			interface Checkout {
				String CheckoutLoginPage = ADDON_PREFIX + "pages/checkout/checkoutLoginPage";// NOSONAR
			}
		}

		interface Fragments {
			interface Checkout // NOSONAR
			{
				String TermsAndConditionsPopup = "fragments/checkout/termsAndConditionsPopup"; // NOSONAR
			}
		}
	}
}
