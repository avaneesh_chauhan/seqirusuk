/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.seqiruscdclogin.constants;

/**
 * Global class for all Seqiruscdclogin constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class SeqiruscdcloginConstants extends GeneratedSeqiruscdcloginConstants
{
	public static final String EXTENSIONNAME = "seqiruscdclogin";
	public static final String CDC_CHANGE_PASSWORD_URL = "changepassword";
	public static final String CDC_CHANGE_PASSWORD_SUCCESS_URL = "/changepasswordsuccess";
	public static final String CDC_CHANGE_PASSWORD_PARAM = "cps=true";
	public static final String REFERER = "Referer";

	private SeqiruscdcloginConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
