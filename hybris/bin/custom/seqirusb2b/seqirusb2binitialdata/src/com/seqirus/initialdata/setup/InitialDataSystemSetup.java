/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.commerceservices.setup.events.SampleDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.seqirus.initialdata.constants.Seqirusb2bInitialDataConstants;

/**
 * This class provides hooks into the system's initialization and update processes.
 */
@SystemSetup(extension = Seqirusb2bInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup
{

	/** The Constant LOG. */
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(InitialDataSystemSetup.class);

	/** The Constant IMPORT_CORE_DATA. */
	private static final String IMPORT_CORE_DATA = "importCoreData";

	/** The Constant IMPORT_SAMPLE_DATA. */
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";

	/** The Constant ACTIVATE_SOLR_CRON_JOBS. */
	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";

	/** The Constant IMPORT_STAGING_DATA. */
	private static final String IMPORT_STAGING_DATA = "importStagingData";

	/** The Constant IMPORT_PRODUCTION_DATA. */
	private static final String IMPORT_PRODUCTION_DATA = "importProdData";

	/** The Constant IMPORT_QA_DATA. */
	private static final String IMPORT_QA_DATA = "importQAData";

	/** The core data import service. */
	private CoreDataImportService coreDataImportService;

	/** The sample data import service. */
	private SampleDataImportService sampleDataImportService;

/*	@Resource(name = "configurationService")
	private ConfigurationService configurationService;*/

/** The Constant SEQIRUSB2B_UK. */
private static final String SEQIRUSB2B_UK = "seqirusb2b-uk";

/** The Constant SEQIRUSB2B_FLU360. */
	private static final String SEQIRUSB2B_FLU360 = "seqirusb2b-flu360";

/** The Constant FLU360_IMPORT_ROOT. */
private static final String FLU360_IMPORT_ROOT = "/seqirusb2binitialdata/import";

/** The Constant IMPORT_FLU360_CDC_DATA. */
private static final String IMPORT_FLU360_CDC_DATA = "importFlu360CDCData";

/**
 * Generates the Dropdown and Multi-select boxes for the project data import.
 *
 * @return the initialization options
 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_FLU360_CDC_DATA, "Import Flu360 CDC Data", true));

		/*final String environmentName = configurationService.getConfiguration().getString("environment.name");
		params.add(createBooleanSystemSetupParameter(IMPORT_STAGING_DATA, "Import Staging Data",
				null != environmentName && environmentName.equals("STAG")));
		params.add(createBooleanSystemSetupParameter(IMPORT_PRODUCTION_DATA, "Import Production Data",
				null != environmentName && environmentName.equals("PROD")));
		params.add(createBooleanSystemSetupParameter(IMPORT_QA_DATA, "Import QA Data",
				null != environmentName && environmentName.equals("QA")));*/
		// Add more Parameters here as you require

		return params;
	}

	/**
	 * Implement this method to create initial objects. This method will be called by system creator during
	 * initialization and system update. Be sure that this method can be called repeatedly.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		// Add Essential Data here as you require

		importImpexFile(context, "/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-registration.impex");
		importImpexFile(context, "/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-login.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-consenttemplate.impex");
		importImpexFile(context, "/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-resetpassword.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-changepassword.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-changepasswordsuccess.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cms-content.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/sampledata/contentCatalogs/seqirusb2b-ukContentCatalog/cms-content.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/sampledata/contentCatalogs/seqirusb2b-ukContentCatalog/returnsandcredit_en.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-updateprofile.impex");
		importImpexFile(context, "/seqirusb2basm/import/contentCatalogs/seqirusb2b-ukContentCatalog/cms-content.impex");

		importImpexFile(context, "/seqirusb2basm/import/common/user-groups.impex");

		importImpexFile(context, "/seqirusb2bworkflow/import/contentCatalogs/seqirusb2b-ukContentCatalog/cms-content.impex");

	// flu360 impexes

				importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-flu360tCatalog/cms-content.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/sampledata/contentCatalogs/seqirusb2b-flu360tCatalog/cms-content.impex");
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization. <br>
	 * Add import data for each site you have configured
	 *
	 * <pre>
	 * final List<ImportData> importData = new ArrayList<ImportData>();
	 *
	 * final ImportData sampleImportData = new ImportData();
	 * sampleImportData.setProductCatalogName(SAMPLE_PRODUCT_CATALOG_NAME);
	 * sampleImportData.setContentCatalogNames(Arrays.asList(SAMPLE_CONTENT_CATALOG_NAME));
	 * sampleImportData.setStoreNames(Arrays.asList(SAMPLE_STORE_NAME));
	 * importData.add(sampleImportData);
	 *
	 * getCoreDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
	 *
	 * getSampleDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
	 * </pre>
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final List<ImportData> importData = new ArrayList<ImportData>();


		final ImportData seqirusImportData = new ImportData();
		seqirusImportData.setProductCatalogName(SEQIRUSB2B_UK);
		seqirusImportData.setContentCatalogNames(Arrays.asList(SEQIRUSB2B_UK));
		seqirusImportData.setStoreNames(Arrays.asList(SEQIRUSB2B_UK));
		importData.add(seqirusImportData);

		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

		getSampleDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new SampleDataImportedEvent(context, importData));

		// flu360

		final List<ImportData> importDataflu360 = new ArrayList<ImportData>();


		final ImportData seqirusImportDataflu360 = new ImportData();
		seqirusImportDataflu360.setProductCatalogName(SEQIRUSB2B_FLU360);
		seqirusImportDataflu360.setContentCatalogNames(Arrays.asList(SEQIRUSB2B_FLU360));
		seqirusImportDataflu360.setStoreNames(Arrays.asList(SEQIRUSB2B_FLU360));
		importDataflu360.add(seqirusImportDataflu360);

		//Import CDC Data for Flu360
		importCDCImpexesForFlu360(context);


		getCoreDataImportService().execute(this, context, importDataflu360);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importDataflu360));

		getSampleDataImportService().execute(this, context, importDataflu360);
		getEventService().publishEvent(new SampleDataImportedEvent(context, importDataflu360));
	}


	/**
	 * Import CDC impexes for flu 360.
	 *
	 * @param context
	 *           the context
	 */
	private void importCDCImpexesForFlu360(final SystemSetupContext context)
	{
		if (this.getBooleanSystemSetupParameter(context, IMPORT_FLU360_CDC_DATA))
		{
			importImpexFile(context,
					FLU360_IMPORT_ROOT + "/coredata/contentCatalogs/seqirusb2b-flu360ContentCatalog/cdc-login.impex");
			importImpexFile(context,
					FLU360_IMPORT_ROOT + "/coredata/contentCatalogs/seqirusb2b-flu360ContentCatalog/cdc-registration.impex");
			importImpexFile(context,
					FLU360_IMPORT_ROOT + "/coredata/contentCatalogs/seqirusb2b-flu360ContentCatalog/cdc-forgotpassword.impex");

			importImpexFile(context,
					FLU360_IMPORT_ROOT + "/coredata/contentCatalogs/seqirusb2b-flu360ContentCatalog/cdc-resetpassword.impex");
			importImpexFile(context,
					FLU360_IMPORT_ROOT + "/coredata/contentCatalogs/seqirusb2b-flu360ContentCatalog/cdc-consenttemplate.impex");
			importImpexFile(context,
					FLU360_IMPORT_ROOT + "/coredata/contentCatalogs/seqirusb2b-flu360ContentCatalog/cms-profileactivation.impex");
		}

	}

	/**
	 * Gets the core data import service.
	 *
	 * @return the core data import service
	 */
	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	/**
	 * Sets the core data import service.
	 *
	 * @param coreDataImportService
	 *           the new core data import service
	 */
	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	/**
	 * Gets the sample data import service.
	 *
	 * @return the sample data import service
	 */
	public SampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}

	/**
	 * Sets the sample data import service.
	 *
	 * @param sampleDataImportService
	 *           the new sample data import service
	 */
	@Required
	public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}
}
