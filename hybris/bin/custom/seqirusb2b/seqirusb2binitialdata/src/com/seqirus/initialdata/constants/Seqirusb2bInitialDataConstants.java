/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.initialdata.constants;

/**
 * Global class for all Seqirusb2bInitialData constants.
 */
public final class Seqirusb2bInitialDataConstants extends GeneratedSeqirusb2bInitialDataConstants
{
	public static final String EXTENSIONNAME = "seqirusb2binitialdata";

	private Seqirusb2bInitialDataConstants()
	{
		//empty
	}
}
