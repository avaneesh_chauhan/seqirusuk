# -----------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
#
# Import the CMS content for the site emails
#
$contentCatalog=seqirusb2b-ukContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]
$wideContent=CMSImageComponent,BannerComponent,SimpleBannerComponent,CMSLinkComponent,CMSParagraphComponent

# Import config properties into impex macros
UPDATE GenericItem[processor=de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor];pk[unique=true]
$jarResourceCms=$config-jarResourceCmsValue
$emailPackageName=$config-emailContextPackageName
$documentsResource=jar:com.seqirus.initialdata.setup.InitialDataSystemSetup&/seqirusb2binitialdata/import/sampledata/contentCatalogs/$contentCatalog

# Email velocity templates
INSERT_UPDATE RendererTemplate;code[unique=true];contextClass;rendererType(code)[default='velocity']
;seqirus_Email_Join_Account_Body;$emailPackageName.CustomerEmailContext
;seqirus_Email_Join_Account_Subject;$emailPackageName.CustomerEmailContext

;seqirus_Email_New_Registration_Body;$emailPackageName.CustomerEmailContext
;seqirus_Email_New_Registration_Subject;$emailPackageName.CustomerEmailContext

;seqirus_WelcomeEmail_Body;$emailPackageName.CustomerEmailContext
;seqirus_WelcomeEmail_Subject;$emailPackageName.CustomerEmailContext

;seqirus_Email_Forgot_Pwd_Body;$emailPackageName.ForgottenPasswordEmailContext
;seqirus_Email_Forgot_Pwd_Subject;$emailPackageName.ForgottenPasswordEmailContext

;seqirus_Email_Change_Pwd_Body;$emailPackageName.SeqirusChangePasswordEmailContext
;seqirus_Email_Change_Pwd_Subject;$emailPackageName.SeqirusChangePasswordEmailContext

;seqirus_Email_Update_Registration_Body;$emailPackageName.CustomerEmailContext
;seqirus_Email_Update_Registration_Subject;$emailPackageName.CustomerEmailContext

# Email page Template
INSERT_UPDATE EmailPageTemplate;$contentCV[unique=true];uid[unique=true];name;active;frontendTemplateName;subject(code);htmlTemplate(code);restrictedPageTypes(code)
;;CustomerRegistrationEmailTemplate;Customer Registration Email Template;true;CustomerRegistrationEmailTemplate;seqirus_Email_Join_Account_Subject;seqirus_Email_Join_Account_Body;EmailPage
;;ForgottenPasswordEmailTemplate;Forgot Password Email Template;true;ForgottenPasswordEmailTemplate;seqirus_Email_Forgot_Pwd_Subject;seqirus_Email_Forgot_Pwd_Body;EmailPage
;;ChangePasswordEmailTemplate;Forgot Password Email Template;true;ForgottenPasswordEmailTemplate;seqirus_Email_Change_Pwd_Subject;seqirus_Email_Change_Pwd_Body;EmailPage
;;NewRegistrationEmailTemplate;New Registration Email Template;true;NewRegistrationEmailTemplate;seqirus_Email_New_Registration_Subject;seqirus_Email_New_Registration_Body;EmailPage
;;UpdateRegistrationEmailTemplate;Update Registration Email Template;true;UpdateRegistrationEmailTemplate;seqirus_Email_Update_Registration_Subject;seqirus_Email_Update_Registration_Body;EmailPage
;;WelcomeEmailToCustomerEmailTemplate;WelcomeEmail Template;true;WelcomeEmailToCustomerEmailTemplate;seqirus_WelcomeEmail_Subject;seqirus_WelcomeEmail_Body;EmailPage


# Templates for CMS Cockpit Page Edit
UPDATE EmailPageTemplate;$contentCV[unique=true];uid[unique=true];velocityTemplate[translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
;;CustomerRegistrationEmailTemplate;$jarResourceCms/structure-view/structure_customerRegistrationEmailTemplate.vm
;;ForgottenPasswordEmailTemplate;$jarResourceCms/structure-view/structure_forgottenPasswordEmailTemplate.vm
;;ChangePasswordEmailTemplate;$jarResourceCms/structure-view/structure_changePasswordEmailTemplate.vm
;;NewRegistrationEmailTemplate;$jarResourceCms/structure-view/structure_newRegistrationEmailTemplate.vm
;;UpdateRegistrationEmailTemplate;$jarResourceCms/structure-view/structure_updateRegistrationEmailTemplate.vm
;;WelcomeEmailToCustomerEmailTemplate;$jarResourceCms/structure-view/structure_welcomeEmailToCustomerEmailTemplate.vm

INSERT_UPDATE ComponentTypeGroup;code[unique=true]
;email

INSERT_UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];name;&componentRef;;;;content;
;;JoinAccountTopRightParagraphComponent;Join Account Top Right Information;JoinAccountTopRightParagraphComponent;;;;Successful Join Account;
;;NewRegTopRightParagraphComponent;New Registration Top Right Information;NewRegTopRightParagraphComponent;;;;Seqirus Account Registration Complete;
;;ForgotPasswordTopRightParagraphComponent;Forgot Password Top Right Information;ForgotPasswordTopRightParagraphComponent;;;;Password Reset Request;
;;ChangePasswordTopRightParagraphComponent;Change Password Top Right Information;ChangePasswordTopRightParagraphComponent;;;;Password Reset Complete;
;;BottomRightParagraphComponent;Bottom Right Information;BottomRightParagraphComponent;;;;<b>Seqirus UK Limited</b><br>Point, 29 Market Street,<br>Maidenhead, Berkshire, SL6 8AA;
;;CopyrightParagraphComponent;Copyright Information;CopyrightParagraphComponent;;;;© 2021 Seqirus. All rights reserved. Seqirus is a trademark of Seqirus UK Limited or its affiliates.;
;;UpdateRegTopRightParagraphComponent;Update Registration Top Right Information;UpdateRegTopRightParagraphComponent;;;;Seqirus Account Update Complete;
;;WelcomeEmailTopRightParagraphComponent;Welcome Email Top Right Information;WelcomeEmailTopRightParagraphComponent;;;;Seqirus Welcome Email;


INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='CustomerRegistrationEmailTemplate'];validComponentTypes(code);compTypeGroup(code)
;SiteLogo;;$wideContent;logo
;TopContent;;$wideContent;wide
;BottomContent;;$wideContent;wide
;CopyrightContent;;$wideContent;wide

INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='NewRegistrationEmailTemplate'];validComponentTypes(code);compTypeGroup(code)
;SiteLogo;;$wideContent;logo
;TopContent;;$wideContent;wide
;BottomContent;;$wideContent;wide
;CopyrightContent;;$wideContent;wide

INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='WelcomeEmailToCustomerEmailTemplate'];validComponentTypes(code);compTypeGroup(code)
;SiteLogo;;$wideContent;logo
;TopContent;;$wideContent;wide
;BottomContent;;$wideContent;wide
;CopyrightContent;;$wideContent;wide

INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='ForgottenPasswordEmailTemplate'];validComponentTypes(code);compTypeGroup(code)
;SiteLogo;;$wideContent;email
;TopContent;;$wideContent;email
;BottomContent;;$wideContent;email
;CopyrightContent;;$wideContent;wide

INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='ChangePasswordEmailTemplate'];validComponentTypes(code);compTypeGroup(code)
;SiteLogo;;$wideContent;email
;TopContent;;$wideContent;email
;BottomContent;;$wideContent;email
;CopyrightContent;;$wideContent;wide

INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='UpdateRegistrationEmailTemplate'];validComponentTypes(code);compTypeGroup(code)
;SiteLogo;;$wideContent;logo
;TopContent;;$wideContent;wide
;BottomContent;;$wideContent;wide
;CopyrightContent;;$wideContent;wide


# Create Content Slots
INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];name;active
;;CustomerRegistrationEmailTopSlot;Default customerRegistration Email Top Slot;true
;;NewRegistrationEmailTopSlot;Default newRegistration Email Top Slot;true
;;ForgottenPasswordEmailTopSlot;Default Forgotten Password Email Top Slot;true
;;ChangePasswordEmailTopSlot;Default Change Password Email Top Slot;true
;;EmailBottomSlot;Default Email Bottom Slot;true
;;EmailSiteLogoSlot;Default Email Site Slot;true
;;CopyrightSlot;Copy Right Slot;true
;;UpdateRegistrationEmailTopSlot;Default updateRegistration Email Top Slot;true
;;WelcomeEmailTopSlot;Default welcome Email Top Slot;true

# Bind Content Slots to Email Page Templates
INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='CustomerRegistrationEmailTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;SiteLogo-CustomerRegistrationEmail;SiteLogo;;EmailSiteLogoSlot;true
;;TopContent-CustomerRegistrationEmail;TopContent;;CustomerRegistrationEmailTopSlot;true
;;BottomContent-CustomerRegistrationEmail;BottomContent;;EmailBottomSlot;true
;;CopyrightContent-CustomerRegistrationEmail;CopyrightContent;;CopyrightSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='NewRegistrationEmailTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;SiteLogo-NewRegistrationEmail;SiteLogo;;EmailSiteLogoSlot;true
;;TopContent-NewRegistrationEmail;TopContent;;NewRegistrationEmailTopSlot;true
;;BottomContent-NewRegistrationEmail;BottomContent;;EmailBottomSlot;true
;;CopyrightContent-NewRegistrationEmail;CopyrightContent;;CopyrightSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='ForgottenPasswordEmailTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;SiteLogo-ForgottenPasswordEmail;SiteLogo;;EmailSiteLogoSlot;true
;;TopContent-ForgottenPasswordEmail;TopContent;;ForgottenPasswordEmailTopSlot;true
;;BottomContent-ForgottenPasswordEmail;BottomContent;;EmailBottomSlot;true
;;CopyrightContent-ForgottenPasswordEmail;CopyrightContent;;CopyrightSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='ChangePasswordEmailTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;SiteLogo-ChangePasswordEmail;SiteLogo;;EmailSiteLogoSlot;true
;;TopContent-ChangePasswordEmail;TopContent;;ChangePasswordEmailTopSlot;true
;;BottomContent-ChangePasswordEmail;BottomContent;;EmailBottomSlot;true
;;CopyrightContent-ChangePasswordEmail;CopyrightContent;;CopyrightSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='UpdateRegistrationEmailTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;SiteLogo-UpdateRegistrationEmail;SiteLogo;;EmailSiteLogoSlot;true
;;TopContent-UpdateRegistrationEmail;TopContent;;UpdateRegistrationEmailTopSlot;true
;;BottomContent-UpdateRegistrationEmail;BottomContent;;EmailBottomSlot;true
;;CopyrightContent-UpdateRegistrationEmail;CopyrightContent;;CopyrightSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='WelcomeEmailToCustomerEmailTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;SiteLogo-WelcomeEmail;SiteLogo;;EmailSiteLogoSlot;true
;;TopContent-WelcomeEmail;TopContent;;WelcomeEmailTopSlot;true
;;BottomContent-WelcomeEmail;BottomContent;;EmailBottomSlot;true
;;CopyrightContent-WelcomeEmail;CopyrightContent;;CopyrightSlot;true


# Email Pages
INSERT_UPDATE EmailPage;$contentCV[unique=true];uid[unique=true];name;masterTemplate(uid,$contentCV);defaultPage;approvalStatus(code)[default='approved']
;;CustomerRegistrationEmail;Customer Registration Email;CustomerRegistrationEmailTemplate;true;
;;ForgottenPasswordEmail;Forgot Password Email;ForgottenPasswordEmailTemplate;true;
;;ChangePasswordEmail;Change Password Email;ChangePasswordEmailTemplate;true;
;;NewRegistrationEmail;New Registration Email;NewRegistrationEmailTemplate;true;
;;UpdateRegistrationEmail;Update Registration Email;UpdateRegistrationEmailTemplate;true;
;;WelcomeEmailToCustomer;Welcome Email;WelcomeEmailToCustomerEmailTemplate;true;

# Preview Image for use in the CMS Cockpit
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];mime;realfilename;@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite=true]
;;EmailPageModel_preview;text/gif;EmailPageModel_preview.gif;$jarResourceCms/preview-images/EmailPageModel_preview.gif

UPDATE EmailPage;$contentCV[unique=true];uid[unique=true];previewImage(code, $contentCV)
;;CustomerRegistrationEmail;EmailPageModel_preview
;;ForgottenPasswordEmail;EmailPageModel_preview
;;ChangePasswordEmail;EmailPageModel_preview
;;NewRegistrationEmail;EmailPageModel_preview
;;UpdateRegistrationEmail;EmailPageModel_preview
;;WelcomeEmailToCustomer;EmailPageModel_preview

INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];cmsComponents(uid,$contentCV)
;;CustomerRegistrationEmailTopSlot;JoinAccountTopRightParagraphComponent
;;ForgottenPasswordEmailTopSlot;ForgotPasswordTopRightParagraphComponent
;;ChangePasswordEmailTopSlot;ChangePasswordTopRightParagraphComponent
;;NewRegistrationEmailTopSlot;NewRegTopRightParagraphComponent
;;UpdateRegistrationEmailTopSlot;UpdateRegTopRightParagraphComponent
;;EmailBottomSlot;BottomRightParagraphComponent
;;EmailSiteLogoSlot;EmailSiteLogoComponent
;;CopyrightSlot;CopyrightParagraphComponent
;;WelcomeEmailTopSlot;WelcomeEmailTopRightParagraphComponent