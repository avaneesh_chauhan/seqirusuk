/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.masterserver.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.licence.Licence;
import de.hybris.platform.masterserver.StatisticsPayloadEncryptor;
import de.hybris.platform.servicelayer.user.UserConstants;
import de.hybris.platform.util.collections.fast.YLongSet;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import com.hybris.statistics.StatisticsGateway;
import com.hybris.statistics.collector.BusinessStatisticsCollector;
import com.hybris.statistics.collector.StatisticsCollector;
import com.hybris.statistics.collector.SystemStatisticsCollector;


public class DefaultStatisticsGateway implements StatisticsGateway<StatisticsPayload>
{
	private static final Logger LOG = Logger.getLogger(DefaultStatisticsGateway.class);
	private static final int SEND_STATS_INTERVAL_IN_HOURS = 8;
	private static final String ANONYMOUS_UID = "anonymous";
	private static final String HOME_URL = "https://license.hybris.com/store";
	// Setting this variable in OS environment will force gathering data each $SEND_STATS_INTERVAL_IN_HOURS$ seconds
	public static final String MS_FORCE_DEVELOPMENT_ENV = "msForceDevelopment";

	private final Set<BusinessStatisticsCollector> businessCollectors;
	private final Set<SystemStatisticsCollector> systemCollectors;

	private Map<String, Map<String, Map<String, Object>>> systemStatistics;
	private volatile DateTime lastStatsSentAt;
	private final ConcurrentMap<String, YLongSet> loggedBackOfficeUsers = new ConcurrentHashMap<>();
	// PATCH
	//private final Map<String, String> webModules;

	private final StatisticsPayloadEncryptor encryptor;
	private final boolean forceDevelopmentModeFromEnv;

	public DefaultStatisticsGateway(final Set<BusinessStatisticsCollector> businessCollectors,
			final Set<SystemStatisticsCollector> systemCollectors, final Map<String, String> existingWebModules,
			final StatisticsPayloadEncryptor encryptor)
	{
		this.businessCollectors = businessCollectors;
		this.systemCollectors = systemCollectors;
		// PATCH
		//this.webModules = Maps.newHashMap(existingWebModules);
		this.encryptor = encryptor;
		this.forceDevelopmentModeFromEnv = isForceDevelopmentModeFromEnv();
	}

	@Override
	public StatisticsPayload getStatisticsPayload()
	{
		StatisticsPayload statisticsPayload = null;
		if (forceDevelopmentModeFromEnv || (isSendingStatsAgreedInLicense() && isGenerateStatsRequired()))
		{
			try
			{
				final Map<String, Map> mainResult = new LinkedHashMap<>();
				boolean sendStats = false;

				final Map<String, Map<String, Map<String, Object>>> collectorsInfoStatistics = getCollectorsInfoStatistics();
				final Map<String, Map<String, Map<String, Object>>> sessionStatistics = getSessionStatistics();
				final Map<String, Map<String, Map<String, Object>>> systemStatistics = getSystemStatistics();
				final Map<String, Map<String, Map<String, Object>>> businessStatistics = getBusinessStatistics();

				if (!collectorsInfoStatistics.isEmpty())
				{
					mainResult.putAll(collectorsInfoStatistics);
					sendStats = true;
				}

				if (sessionStatistics != null && !sessionStatistics.isEmpty())
				{
					mainResult.putAll(sessionStatistics);
					sendStats = true;
				}

				if (systemStatistics != null && !systemStatistics.isEmpty())
				{
					mainResult.putAll(systemStatistics);
					sendStats = true;
				}

				if (businessStatistics != null && !businessStatistics.isEmpty())
				{
					mainResult.putAll(businessStatistics);
					sendStats = true;
				}

				if (sendStats)
				{
					statisticsPayload = encryptor.encrypt(toJson(mainResult), HOME_URL);
				}
			}
			catch (final Exception e)
			{
				if (isLogExceptionsInDebug())
				{
					LOG.debug(e.getMessage(), e);
				}
			}
		}

		return statisticsPayload;
	}

	private Map<String, Map<String, Map<String, Object>>> getCollectorsInfoStatistics()
	{
		final Map<String, Map<String, Map<String, Object>>> result = new LinkedHashMap<>();
		try
		{
			final Set<StatisticsCollector> allCollectors = Sets.union(systemCollectors, businessCollectors);
			final Map<String, Object> collectorsInfoStats = new LinkedHashMap<>();
			int i = 1;
			for (final StatisticsCollector collector : allCollectors)
			{
				collectorsInfoStats.put(Integer.toString(i++), collector.getClass().getName());
			}

			if (!collectorsInfoStats.isEmpty())
			{
				final ImmutableMap<String, Map<String, Object>> overall = ImmutableMap.<String, Map<String, Object>> builder()
						.put("collectors", collectorsInfoStats).build();
				result.put("collectorsInfo", overall);
			}
		}
		catch (final Exception e)
		{
			if (isLogExceptionsInDebug())
			{
				LOG.debug(e.getMessage(), e);
			}
		}
		return result;
	}

	/**
	 * To force stats generation on backoffice page in development mode export environment variable
	 * msForceDevelopment=true. In this case stats will be generated each 8 seconds.
	 */
	boolean isForceDevelopmentModeFromEnv()
	{
		boolean result = false;
		try
		{
			result = Boolean.parseBoolean(System.getenv(MS_FORCE_DEVELOPMENT_ENV));
		}
		catch (final Exception e)
		{
			if (isLogExceptionsInDebug())
			{
				LOG.debug(e.getMessage(), e);
			}
		}

		return result;
	}

	public void updateLoggedInUsersStats(final String webAppContext)
	{
		// PATCH
		//		final String webApp = webModules.get(webAppContext);
		//
		//		if (webApp != null)
		//		{
		//		final YLongSet loggedInUsers = getLoggedInUsersContainerForWebApp(/* webApp */ webAppContext);
		//			final PK currentUserPk = getCurrentUserPk();
		//			if (currentUserPk != null)
		//			{
		//				synchronized (this)
		//				{
		//					loggedInUsers.add(currentUserPk.getLongValue());
		//				}
		//			}
		//		}

	}

	private YLongSet getLoggedInUsersContainerForWebApp(final String webApp)
	{
		YLongSet container = loggedBackOfficeUsers.get(webApp);
		if (container == null)
		{
			container = new YLongSet();
			final YLongSet previousContainer = loggedBackOfficeUsers.putIfAbsent(webApp, container);

			if (previousContainer != null)
			{
				container = previousContainer;
			}
		}

		return container;
	}

	protected boolean isSendingStatsAgreedInLicense()
	{
		return Licence.getDefaultLicence().isMasterServerEnabled();
	}

	protected boolean isGenerateStatsRequired()
	{
		if (lastStatsSentAt == null)
		{
			synchronized (this)
			{
				if (lastStatsSentAt == null)
				{
					lastStatsSentAt = DateTime.now();
					return true;
				}
			}
		}

		final boolean isDateBefore;
		final DateTime now = DateTime.now();
		synchronized (this)
		{
			isDateBefore = lastStatsSentAt.plusHours(SEND_STATS_INTERVAL_IN_HOURS).isBefore(now);
			if (isDateBefore)
			{
				lastStatsSentAt = now;
				return true;
			}
		}
		return false;
	}

	protected PK getCurrentUserPk()
	{
		User user = null;
		try
		{
			user = (User) JaloSession.getCurrentSession().getAttribute(UserConstants.USER_SESSION_ATTR_KEY);
		}
		catch (final Exception e)
		{
			if (isLogExceptionsInDebug())
			{
				LOG.debug(e.getMessage(), e);
			}
		}
		return user == null || ANONYMOUS_UID.equals(user.getUid()) ? null : user.getPK();
	}

	private Map<String, Map<String, Map<String, Object>>> getSessionStatistics()
	{
		try
		{
			final Map<String, Map<String, Map<String, Object>>> result = new LinkedHashMap<>();
			final Map<String, Object> userStats = new LinkedHashMap<>();

			for (final Entry<String, YLongSet> entry : loggedBackOfficeUsers.entrySet())
			{
				final int numLoggedUsers = entry.getValue().size();
				if (numLoggedUsers != 0)
				{
					userStats.put(entry.getKey(), Integer.valueOf(numLoggedUsers));
				}
			}

			if (!userStats.isEmpty())
			{
				final ImmutableMap<String, Map<String, Object>> overall = ImmutableMap.<String, Map<String, Object>> builder()
						.put("backOfficeOverallUsers", userStats).build();
				result.put("session", overall);
				return result;
			}
			else
			{
				return null;
			}
		}
		catch (final Exception e)
		{
			return null;
		}
	}

	private Map<String, Map<String, Map<String, Object>>> getSystemStatistics()
	{
		if (systemStatistics == null)
		{
			systemStatistics = getStatisticsFor("system", systemCollectors);
		}
		return systemStatistics;
	}

	private Map<String, Map<String, Map<String, Object>>> getBusinessStatistics()
	{
		return getStatisticsFor("business", businessCollectors);
	}

	private Map<String, Map<String, Map<String, Object>>> getStatisticsFor(final String statName,
			final Set<? extends StatisticsCollector> statCollectors)
	{
		final Map<String, Map<String, Map<String, Object>>> result = new LinkedHashMap<>();

		for (final StatisticsCollector<Map<String, Map<String, Object>>> collector : statCollectors)
		{
			try
			{
				final Map<String, Map<String, Object>> collectorResult = collector.collectStatistics();
				if (collectorResult != null)
				{
					final Map<String, Map<String, Object>> stat = result.get(statName);
					if (stat == null)
					{
						result.put(statName, new LinkedHashMap<>());
					}

					result.get(statName).putAll(collectorResult);
				}
			}
			catch (final Exception e)
			{
				if (isLogExceptionsInDebug())
				{
					LOG.debug(e.getMessage(), e);
				}
			}
		}

		return result;
	}

	private boolean isLogExceptionsInDebug()
	{
		return Licence.getDefaultLicence().isDemoOrDevelopLicence() && LOG.isDebugEnabled();
	}

	private String toJson(final Map input) throws Exception
	{
		final ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(input);
	}
}
