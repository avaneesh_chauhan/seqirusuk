/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.masterserver.impl;

import de.hybris.platform.masterserver.StatisticsGatewayFactory;
import de.hybris.platform.masterserver.StatisticsPayloadEncryptor;

import java.security.interfaces.RSAPublicKey;
import java.util.HashSet;
import java.util.Set;

import org.reflections.Reflections;

import com.hybris.encryption.asymmetric.impl.RSAEncryptor;
import com.hybris.encryption.asymmetric.impl.RSAKeyPairManager;
import com.hybris.encryption.symmetric.impl.AESEncryptor;
import com.hybris.statistics.StatisticsGateway;
import com.hybris.statistics.collector.BusinessStatisticsCollector;
import com.hybris.statistics.collector.StatisticsCollector;
import com.hybris.statistics.collector.SystemStatisticsCollector;


public final class DefaultStatisticsGatewayFactory implements StatisticsGatewayFactory<StatisticsPayload>
{
	public static final String MASTERSERVER_PUBLIC_KEY_FILE = "/security/hybris_license_key_pub.der";

	private static final StatisticsGatewayFactory<StatisticsPayload> factory = new DefaultStatisticsGatewayFactory();
	private volatile StatisticsGateway<StatisticsPayload> statisticsGateway;
	private final Reflections localScanner = new Reflections("de.hybris.platform.masterserver.collector");
	private final Reflections simpleStatsScanner = new Reflections("com.hybris.statistics.collector");
	private final RSAPublicKey publicKey;

	private DefaultStatisticsGatewayFactory()
	{
		this.publicKey = new RSAKeyPairManager().getPublicKey(MASTERSERVER_PUBLIC_KEY_FILE);
	}

	public static StatisticsGatewayFactory<StatisticsPayload> getInstance()
	{
		return factory;
	}

	@Override
	public StatisticsGateway<StatisticsPayload> getOrCreateStatisticsGateway()
	{
		try
		{
			if (statisticsGateway == null)
			{
				synchronized (this)
				{
					if (statisticsGateway == null)
					{
						final StatisticsPayloadEncryptor encryptor = new DefaultStatisticsPayloadEncryptor(new AESEncryptor(),
								new RSAEncryptor(), publicKey);
						statisticsGateway = new DefaultStatisticsGateway(getCollectorsFor(BusinessStatisticsCollector.class),
								getCollectorsFor(SystemStatisticsCollector.class),
								null /* PATCH webapps not needed any more */, encryptor);
					}
				}
			}
			return statisticsGateway;
		}
		catch (final Exception e)
		{
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private <T extends StatisticsCollector> Set<T> getCollectorsFor(final Class<T> klass)
			throws InstantiationException, IllegalAccessException
	{
		final Set<T> collectors = new HashSet<>();
		final Set<Class<? extends T>> localImpls = localScanner.getSubTypesOf(klass);
		final Set<Class<? extends T>> simpleStatsImpls = simpleStatsScanner.getSubTypesOf(klass);

		for (final Class<? extends T> _klass : localImpls)
		{
			if (!_klass.isAnonymousClass())
			{
				collectors.add(_klass.newInstance());
			}
		}

		for (final Class<? extends T> _klass : simpleStatsImpls)
		{
			if (!_klass.isAnonymousClass())
			{
				collectors.add(_klass.newInstance());
			}
		}

		return collectors;
	}

}
