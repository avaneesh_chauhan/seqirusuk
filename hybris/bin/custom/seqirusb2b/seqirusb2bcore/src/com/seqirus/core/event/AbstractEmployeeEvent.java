/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.store.BaseStoreModel;


/**
 * Abstract base class for employee events.
 */
public abstract class AbstractEmployeeEvent<T extends BaseSiteModel> extends AbstractEvent
{
	private BaseStoreModel baseStore;
	private T site;
	private EmployeeModel employee;
	private LanguageModel language;
	private CurrencyModel currency;

	/**
	 * @return the baseStore
	 */
	public BaseStoreModel getBaseStore()
	{
		return baseStore;
	}

	/**
	 * @param baseStore
	 *           the baseStore to set
	 */
	public void setBaseStore(final BaseStoreModel baseStore)
	{
		this.baseStore = baseStore;
	}

	/**
	 * @return the baseSite
	 */
	public T getSite()
	{
		return site;
	}

	/**
	 * @param site
	 *           the baseSite to set
	 */
	public void setSite(final T site)
	{
		this.site = site;
	}


	/**
	 * @return the employee
	 */
	public EmployeeModel getEmployee()
	{
		return employee;
	}

	/**
	 * @param employee
	 *           the employee to set
	 */
	public void setEmployee(final EmployeeModel employee)
	{
		this.employee = employee;
	}

	/**
	 * @return the language
	 */
	public LanguageModel getLanguage()
	{
		return language;
	}

	/**
	 * @param language
	 *           the language to set
	 */
	public void setLanguage(final LanguageModel language)
	{
		this.language = language;
	}

	/**
	 *
	 * @return CurrencyModel
	 */
	public CurrencyModel getCurrency()
	{
		return currency;
	}

	/**
	 *
	 * @param currency
	 */
	public void setCurrency(final CurrencyModel currency)
	{
		this.currency = currency;
	}

}
