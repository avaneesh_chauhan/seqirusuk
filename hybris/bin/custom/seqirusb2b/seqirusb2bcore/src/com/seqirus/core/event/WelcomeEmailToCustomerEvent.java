/**
 *
 */
package com.seqirus.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;


/**
 * @author 813784
 *
 */
public class WelcomeEmailToCustomerEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{
	/**
	 * Default Constructor
	 *
	 */
	public WelcomeEmailToCustomerEvent()
	{
		super();
	}


}
