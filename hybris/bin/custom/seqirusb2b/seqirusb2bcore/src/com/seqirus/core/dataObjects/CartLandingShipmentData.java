/**
 *
 */
package com.seqirus.core.dataObjects;
import java.util.List;
import com.seqirus.core.dataObjects.ShipdataCartLanding;

public class CartLandingShipmentData 
{
	
/**
	 * @return the data
	 */
	public List<ShipdataCartLanding> getData()
	{
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<ShipdataCartLanding> data)
	{
		this.data = data;
	}

public List<ShipdataCartLanding> data;
	
}
