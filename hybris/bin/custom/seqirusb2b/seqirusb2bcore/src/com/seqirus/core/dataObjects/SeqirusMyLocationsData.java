/**
 *
 */
package com.seqirus.core.dataObjects;

/**
 * @author 614269
 *
 */
public class SeqirusMyLocationsData
{

	String addressId;
	String name;
	String status;
	String type;
	String address;

	/**
	 * @return the addressId
	 */
	public String getAddressId()
	{
		return addressId;
	}

	/**
	 * @param addressId
	 *           the addressId to set
	 */
	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final String status)
	{
		this.status = status;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type
	 *           the type to set
	 */
	public void setType(final String type)
	{
		this.type = type;
	}

	/**
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * @param address
	 *           the address to set
	 */
	public void setAddress(final String address)
	{
		this.address = address;
	}


}
