package com.seqirus.core.dataObjects;

import java.util.List;

public class OrderDetailJSON {

	public static final String ORDER_PLACED="OrderPlaced";
	public static final String ORDER_INPROCESS="InProcess";
	public static final String ORDER_COMPLETE="Complete";
	public static final String ORDER_CANCELLED="Cancelled";

	public String orderID;
	public String orderType;
	public String orderStatus;
	public String partnerName;
	public String orderDate;
	public String shipToPartner;
	public double totalNetValue;
	public String addressLine1;
	public String addressLine2;
	public String city;
	public String state;
	public String zipCode;
	public String poNumber;

	public String soldToPartnerID;
	public List<OrderDetailJSON> orders;
	public int orderCount;

	public List<ProductDetailJSON> products;
	public int shipmentCount;
	public List<ShipmentDetailJSON> shipments;

	@Override
	public String toString() {
		return "OrderDetailJSON [orderID=" + orderID + ", orderType=" + orderType + ", orderStatus=" + orderStatus
				+ ", partnerName=" + partnerName + ", orderDate=" + orderDate + ", shipToPartner=" + shipToPartner + ", totalNetValue=" + totalNetValue
				+ ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", city=" + city + ", state="
				+ state + ", zipCode=" + zipCode + ", poNumber=" + poNumber + ", soldToPartnerID=" + soldToPartnerID
				+ ", orders=" + orders + ", orderCount=" + orderCount + ", products=" + products + ", shipmentCount="
				+ shipmentCount + ", shipments=" + shipments + "]";
	}


}


