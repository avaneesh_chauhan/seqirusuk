/**
 *
 */
package com.seqirus.core.orders.service;

import java.util.List;

import com.seqirus.core.dataObjects.CustomerSummary;
import com.seqirus.core.dataObjects.OrgSummary;
import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.dataObjects.OrderSummary;
import com.seqirus.core.dataObjects.PartnerSummary;

/**
 * @author 614269
 *
 */
public interface SeqirusOrdersService
{

	/**
	 * @param customerId
	 * @param season
	 * @return
	 */
	List<OrderSummary> getOrders(String customerId, String season);

	/**
	 * @return
	 */
	SeasonEntryModel getSeasonEntry();
	
	PartnerSummary fetchPartnerSummary(final String accountNumber);
	
	CustomerSummary fetchCustomerDetail(final String accountNumber, final String zipCode);
	
	OrgSummary fetchPartnerDetails(String accountNumber,  String partnerType);
}
