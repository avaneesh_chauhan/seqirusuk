/**
 *
 */
package com.seqirus.core.dataObjects;
import java.util.ArrayList;
import java.util.List;
public class OrderReviewForm 
{
	
	/**
	 * @return the orderReviewdata
	 */
	public List<OrderReviewData> getOrderReviewdata()
	{
		return orderReviewdata;
	}

	/**
	 * @param orderReviewdata the orderReviewdata to set
	 */
	public void setOrderReviewdata(List<OrderReviewData> orderReviewdata)
	{
		this.orderReviewdata = orderReviewdata;
	}

	List<OrderReviewData> orderReviewdata =  new ArrayList<>();; 
}
