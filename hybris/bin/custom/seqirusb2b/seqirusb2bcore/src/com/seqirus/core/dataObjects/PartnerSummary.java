/**
 *
 */
package com.seqirus.core.dataObjects;

import java.util.List;
import java.util.Map;

import com.seqirus.core.dataObjects.PartnerDataJSON;


/**
 * @author 132793
 *
 */
public class PartnerSummary
{

	Map<String, List<String>> accounts;

	public PartnerSummary(final Map<String, List<String>> accounts)
	{
		super();
		this.accounts = accounts;
	}

	public String getBillToAccount()
	{
		return getData(accounts.get(PartnerDataJSON.BILLTO));
	}

	private String getData(final List<String> dataList)
	{
		if (dataList.isEmpty())
		{
			return null;
		}
		else
		{
			return dataList.get(0);
		}
	}

	public String getPayerAccount()
	{
		return getData(accounts.get(PartnerDataJSON.PAYER));
	}

	public String getSoldToAccount()
	{
		return getData(accounts.get(PartnerDataJSON.SOLDTO));
	}

	public int getShippingCount()
	{
		return accounts.get(PartnerDataJSON.SHIPTO).size();
	}

	@Override
	public String toString()
	{
		return "PartnerSummary [accountMap=" + accounts + "]";
	}

}
