/**
 *
 */
package com.seqirus.core.dataObjects;
import java.util.ArrayList;
import java.util.List;
import com.seqirus.core.dataObjects.ShipLoctionQty;
public class OrderCheckoutLocations 
{
	public String orgName;
	
	public String address;
	
	public String productCode;
	
	public List<ShipLoctionQty> locationQty= new ArrayList<ShipLoctionQty>();

	/**
	 * @return the orgName
	 */
	public String getOrgName()
	{
		return orgName;
	}

	/**
	 * @param orgName the orgName to set
	 */
	public void setOrgName(String orgName)
	{
		this.orgName = orgName;
	}

	/**
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address)
	{
		this.address = address;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode()
	{
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * @return the locationQty
	 */
	public List<ShipLoctionQty> getLocationQty()
	{
		return locationQty;
	}

	/**
	 * @param locationQty the locationQty to set
	 */
	public void setLocationQty(List<ShipLoctionQty> locationQty)
	{
		this.locationQty = locationQty;
	}
	
}
