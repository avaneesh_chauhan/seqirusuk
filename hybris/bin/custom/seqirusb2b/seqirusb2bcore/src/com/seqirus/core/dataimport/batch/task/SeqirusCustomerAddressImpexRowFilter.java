package com.seqirus.core.dataimport.batch.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexRowFilter;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.seqirus.core.enums.AddressType;


/**
 *
 * This file SeqirusCustomerAddressImpexRowFilter.java is used Customer Address Filter.
 *
 */
public class SeqirusCustomerAddressImpexRowFilter implements ImpexRowFilter
{
	private static final Logger LOG = Logger.getLogger(SeqirusCustomerAddressImpexRowFilter.class);
	private static final boolean IS_DEBUG_ENABLED = LOG.isDebugEnabled();

	private String addressTypeRowIndex;
	private String addressIdRowIndex;

	private String addressFlagRowIndex;
	private String addressRowIndex;

	private String currentAddressType;

	/**
	 * This method is used to validate AddressType and set the related value to Impex rows.
	 *
	 * @param row
	 * @return boolean
	 */
	@Override
	public boolean filter(final Map<Integer, String> row)
	{
		boolean isvalid = true;
		final String addressType = row.get(Integer.valueOf(addressTypeRowIndex));

		if (StringUtils.isEmpty(addressType))
		{
			LOG.error("Mandatory attribute Miss: AddressType:" + addressType);
			//Not setting isvalid=false, as want to fail as mandatory attribute missing
		}
		else if (StringUtils.equalsIgnoreCase(addressType.trim(), AddressType.SP.getCode())
				|| StringUtils.equalsIgnoreCase(addressType.trim(), AddressType.BP.getCode())
				|| StringUtils.equalsIgnoreCase(addressType.trim(), AddressType.PY.getCode())//added for payto address
				|| StringUtils.equalsIgnoreCase(addressType.trim(), AddressType.SH.getCode()))
		{
			if (StringUtils.equalsIgnoreCase("validation", currentAddressType))
			{
				isvalid = true;
			}
			else if (StringUtils.equalsIgnoreCase(addressType.trim(), currentAddressType))
			{
				final String addressId = row.get(Integer.valueOf(addressIdRowIndex));
				row.put(Integer.valueOf(addressRowIndex), addressId);
				row.put(Integer.valueOf(addressFlagRowIndex), "TRUE");
				isvalid = true;
			}
			else
			{
				if (IS_DEBUG_ENABLED)
				{
					//Not a error case, just not processing the row for current addresstype
					LOG.debug("ignoring current row  due to addresstype mismatch, addressType to process:" + currentAddressType
							+ " and  addressType in current row:" + addressType);
				}
				isvalid = false;
			}
		}
		else
		{
			LOG.error("ignoring current row due to Data issue, Incorrect AddressType:" + addressType);
			isvalid = false;
		}

		return isvalid;
	}

	/**
	 * @return the addressTypeRowIndex
	 */
	public String getAddressTypeRowIndex()
	{
		return addressTypeRowIndex;
	}

	/**
	 * @param addressTypeRowIndex
	 *           the addressTypeRowIndex to set
	 */
	@Required
	public void setAddressTypeRowIndex(final String addressTypeRowIndex)
	{
		this.addressTypeRowIndex = addressTypeRowIndex;
	}



	/**
	 * @return the addressIdRowIndex
	 */
	public String getAddressIdRowIndex()
	{
		return addressIdRowIndex;
	}

	/**
	 * @param addressIdRowIndex
	 *           the addressIdRowIndex to set
	 */
	@Required
	public void setAddressIdRowIndex(final String addressIdRowIndex)
	{
		this.addressIdRowIndex = addressIdRowIndex;
	}

	/**
	 * @return the addressFlagRowIndex
	 */
	public String getAddressFlagRowIndex()
	{
		return addressFlagRowIndex;
	}

	/**
	 * @param addressFlagRowIndex
	 *           the addressFlagRowIndex to set
	 */
	@Required
	public void setAddressFlagRowIndex(final String addressFlagRowIndex)
	{
		this.addressFlagRowIndex = addressFlagRowIndex;
	}

	/**
	 * @return the addressRowIndex
	 */
	public String getAddressRowIndex()
	{
		return addressRowIndex;
	}

	/**
	 * @param addressRowIndex
	 *           the addressRowIndex to set
	 */
	@Required
	public void setAddressRowIndex(final String addressRowIndex)
	{
		this.addressRowIndex = addressRowIndex;
	}

	/**
	 * @return the currentAddressType
	 */
	public String getCurrentAddressType()
	{
		return currentAddressType;
	}

	/**
	 * @param currentAddressType
	 *           the currentAddressType to set
	 */
	@Required
	public void setCurrentAddressType(final String currentAddressType)
	{
		this.currentAddressType = currentAddressType;
	}

}
