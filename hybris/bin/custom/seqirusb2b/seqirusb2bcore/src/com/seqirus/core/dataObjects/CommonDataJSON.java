/**
 *
 */
package com.seqirus.core.dataObjects;

public class CommonDataJSON extends ContactDataJSON {
	
	public String city;
	public String zipCode;
	public String addressLine1;
	public String addressLine2;
	public String state;
	
	public  String orgName;
	public String partnerID;
	
	
	

	public void setAddressLine_1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public void setAddressLine_2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	@Override
	public String toString() {
		return "AddressLookupData [city=" + city + ", zipCode=" + zipCode + ", addressLine1=" + addressLine1
				+ ", addressLine2=" + addressLine2 + ", state=" + state + "]";
	}

}

