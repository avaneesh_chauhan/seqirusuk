package com.seqirus.core.dataObjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ShipmentDetailJSON
{

	public String deliveryNumber;
	public String deliveryStatus;
	public String deliveryDate;
	public String pgiDate;
	public String pgiDocument;
	public String deliveryNoteCreationDate;
	public List<ProductDetailJSON> products;
	public List<TrackingDetailJSON> tracking;
	public List<String> invoices;
	public List<String> debitNotes;
	public List<String> creditNotes;

	public List<String> getInvoiceNumbers()
	{
		final List<String> invoiceList = new ArrayList<String>();
		invoiceList.addAll(invoices == null ? Collections.EMPTY_LIST : invoices);
		//invoiceList.addAll(debitNotes==null?Collections.EMPTY_LIST:debitNotes);
		//invoiceList.addAll(creditNotes==null?Collections.EMPTY_LIST:creditNotes);
		return invoiceList;
	}

	public List<String> getCreditNotes()
	{
		final List<String> creditList = new ArrayList<String>();
		creditList.addAll(creditNotes == null ? Collections.EMPTY_LIST : creditNotes);
		//invoiceList.addAll(debitNotes==null?Collections.EMPTY_LIST:debitNotes);
		//invoiceList.addAll(creditNotes==null?Collections.EMPTY_LIST:creditNotes);
		return creditList;
	}

	public List<TrackingDetailJSON> getTracking()
	{
		return tracking == null ? Collections.EMPTY_LIST : tracking;
	}

	public List<ProductDetailJSON> getProducts()
	{
		return products == null ? Collections.EMPTY_LIST : products;
	}

	@Override
	public String toString()
	{
		return "ShipmentDetailJSON [deliveryNumber=" + deliveryNumber + ", deliveryStatus=" + deliveryStatus + ", deliveryDate="
				+ deliveryDate + ", products=" + products + ", + pgiDate=" + pgiDate + ", + pgiDocument=" + pgiDocument
				+ ",  trackingData=" + tracking + ", invoices="
				+ invoices + "]";
	}



}

