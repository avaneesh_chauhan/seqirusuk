package com.seqirus.core.dataObjects;

/**
 *
 * Holds the order items of an order
 *
 */
public class OrderItems
{
	String materialID;
	int qty;

	public OrderItems(final String materialID, final int qty)
	{
		super();
		this.materialID = materialID;
		this.qty = qty;
	}

	public String getMaterialID()
	{
		return materialID;
	}

	public int getQty()
	{
		return qty;
	}

	@Override
	public String toString()
	{
		return "OrderItems [materialID=" + materialID + ", qty=" + qty + "]";
	}

}
