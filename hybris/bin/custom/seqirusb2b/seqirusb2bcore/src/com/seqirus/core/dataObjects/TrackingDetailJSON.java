package com.seqirus.core.dataObjects;

public class TrackingDetailJSON {

	public String trackingLink;
	public String trackingID;
	@Override
	public String toString() {
		return "TrackingDetailJSON [trackingLink=" + trackingLink + ", trackingID=" + trackingID + "]";
	}


}