/**
 *
 */
package com.seqirus.core.dataObjects;
import java.util.ArrayList;
import java.util.List;
public class OrderCheckoutProducts 
{
	
	public String dose;
	
	public String product ;
   
	public String productCode;
	/**
	 * @return the dose
	 */
	public String getDose()
	{
		return dose;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode()
	{
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * @param dose the dose to set
	 */
	public void setDose(String dose)
	{
		this.dose = dose;
	}

	/**
	 * @return the product
	 */
	public String getProduct()
	{
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(String product)
	{
		this.product = product;
	}
	
}
