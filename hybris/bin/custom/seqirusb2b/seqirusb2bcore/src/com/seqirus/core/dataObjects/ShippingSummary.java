/**
 * Holds the Shipping data for customer
 */
package com.seqirus.core.dataObjects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.seqirus.core.dataObjects.ContactDataJSON;
import com.seqirus.core.dataObjects.PartnerDataJSON;
import com.seqirus.core.dataObjects.ShiptoDataJSON;


public class ShippingSummary extends OrgSummary
{

	String licenseIssueState;
	Date licenseExpiry;
	String nameOnLicense;
	String licenseNumber;
	AddressDetail licenseAddress;
	Map<String, Boolean> nonDeliveryDays;
	String deliveryInstruction;
	boolean govtFedOrg;
	public static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

	public void setLicenseDetails(final String licenseIssueState, final Date licenseExpiry, final String nameOnLicense,
			final String licenseNumber)
	{

		this.licenseIssueState = licenseIssueState;
		this.licenseExpiry = licenseExpiry;
		this.nameOnLicense = nameOnLicense;
		this.licenseNumber = licenseNumber;

	}

	public void setLicenseAddress(final String addressLine1, final String addressLine2, final String city, final String state,
			final String zipCode)
	{
		licenseAddress = new AddressDetail(addressLine1, addressLine2, city, state, zipCode);
	}

	public String getLicenseIssueState()
	{
		return licenseIssueState;
	}

	public Date getLicenseExpiry()
	{
		return licenseExpiry;
	}

	public String getNameOnLicense()
	{
		return nameOnLicense;
	}

	public String getLicenseNumber()
	{
		return licenseNumber;
	}

	public Map<String, Boolean> getNonDeliveryDays()
	{
		return nonDeliveryDays;
	}

	public String getDeliveryInstruction()
	{
		return deliveryInstruction;
	}

	public String getLicenseAddressLine1()
	{
		return licenseAddress.getAddressLine1();
	}

	public String getLicenseAddressLine2()
	{
		return licenseAddress.getAddressLine2();
	}

	public String getLicenseCity()
	{
		return licenseAddress.getCity();
	}

	public String getLicenseState()
	{
		return licenseAddress.getState();
	}

	public String getLicenseZipCode()
	{
		return licenseAddress.getZipCode();
	}

	@Override
	public String toString()
	{
		return "ShippingSummary [licenseIssueState=" + licenseIssueState + ", licenseExpiry=" + licenseExpiry + ", nameOnLicense="
				+ nameOnLicense + ", licenseNumber=" + licenseNumber + ", licenseAddress=" + licenseAddress + ", nonDeliveryDays="
				+ nonDeliveryDays + ", deliveryInstruction=" + deliveryInstruction + ", govtFedOrg=" + govtFedOrg + ", contact="
				+ contact + ", orgName=" + orgName + ", address=" + address + "]";
	}

	@Override
	public void populate(final PartnerDataJSON response)
	{


		final ShiptoDataJSON jsonResponse = (ShiptoDataJSON) response;
		this.nonDeliveryDays = jsonResponse.preference != null ? jsonResponse.preference.getNonDeliveryDays() : null;
		this.deliveryInstruction = jsonResponse.preference != null ? jsonResponse.preference.nonDeliveryComments : null;
		if (jsonResponse.license != null)
		{
			Date expiryDate = null;
			try
			{
				if (jsonResponse.license.expirationDate != null && !jsonResponse.license.expirationDate.isEmpty())
				{
					expiryDate = FORMATTER.parse(jsonResponse.license.expirationDate);
				}
			}
			catch (final ParseException e)
			{
				// TODO Auto-generated catch block
				LOGGER.error("Wrong expiry date");
			}

			setLicenseDetails(jsonResponse.license.stateofIssueLicense, expiryDate, jsonResponse.license.name,
					jsonResponse.license.licenseNumber);
			setLicenseAddress(jsonResponse.license.addressLine1, jsonResponse.license.addressLine2, jsonResponse.license.city,
					jsonResponse.license.state, jsonResponse.license.zipCode);
		}
		if (!jsonResponse.shippingContact.isEmpty())
		{
			final ContactDataJSON contact = jsonResponse.shippingContact.get(0);
			jsonResponse.firstName = contact.firstName;
			jsonResponse.lastName = contact.lastName;
			jsonResponse.jobTitle = contact.jobTitle;
			jsonResponse.phoneExt = contact.phoneExt;
			jsonResponse.phoneNumber = contact.phoneNumber;
			jsonResponse.email = contact.email;
		}
		super.populate(jsonResponse);
	}

	@Override
	public OrgSummary createCopy()
	{
		return new ShippingSummary();
	}


}
