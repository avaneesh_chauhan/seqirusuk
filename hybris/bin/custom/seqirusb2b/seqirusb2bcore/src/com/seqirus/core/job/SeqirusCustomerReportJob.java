/**
 *
 */
package com.seqirus.core.job;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ClearValuesRequest;
import com.google.api.services.sheets.v4.model.ClearValuesResponse;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.seqirus.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.core.enums.AccountStatusEnum;
import com.seqirus.core.enums.AddressType;
import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.core.registration.service.SeqirusCustomerRegistrationService;


/**
 * @author 700196
 *
 */
public class SeqirusCustomerReportJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = Logger.getLogger(SeqirusCustomerReportJob.class);

	private static List<String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);
	private static HttpTransport transport;
	private static JacksonFactory jsonFactory;
	private static final SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;

	@Autowired
	protected ConfigurationService configurationService;

	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		final String valueInputOption = "USER_ENTERED";
		final String range = configurationService.getConfiguration()
				.getString(Seqirusb2bCoreConstants.GOOGLE_SHEET_API_SPREADSHEET_RANGE);
		final String clearRange = configurationService.getConfiguration()
				.getString(Seqirusb2bCoreConstants.GOOGLE_SHEET_API_SPREADSHEET_CLEARRANGE);
		final String spreadSheetId = configurationService.getConfiguration()
				.getString(Seqirusb2bCoreConstants.GOOGLE_SHEET_API_SPREADSHEET_ID);
		final List<B2BCustomerModel> registeredCustomers = seqirusCustomerRegistrationService.getRegisteredCustomersForReport();
		if (null != registeredCustomers && CollectionUtils.isNotEmpty(registeredCustomers))
		{
			final List<Object> reportColumns = new ArrayList<>();
			reportColumns.add(Seqirusb2bCoreConstants.PROFILE_DATE);
			reportColumns.add(Seqirusb2bCoreConstants.PROFILE_FIRST_NAME);
			reportColumns.add(Seqirusb2bCoreConstants.PROFILE_LAST_NAME);
			reportColumns.add(Seqirusb2bCoreConstants.PROFILE_ORG_NAME);
			reportColumns.add(Seqirusb2bCoreConstants.PROFILE_EMAIL);
			reportColumns.add(Seqirusb2bCoreConstants.PROFILE_PHONE_NUMBER);
			reportColumns.add(Seqirusb2bCoreConstants.PROFILE_PHONE_EXT);
			reportColumns.add(Seqirusb2bCoreConstants.SAP_ACCOUNT_CODE);
			reportColumns.add(Seqirusb2bCoreConstants.BUSINESS_ORG_NAME);
			reportColumns.add(Seqirusb2bCoreConstants.BUSINESS_ORG_POSTCODE);
			reportColumns.add(Seqirusb2bCoreConstants.SUCCESSFUL_JOIN_ACCOUNT);
			reportColumns.add(Seqirusb2bCoreConstants.REQUEST_ASSISTANCE);
			reportColumns.add(Seqirusb2bCoreConstants.NEW_REGISTRATION);


			try
			{
				final ValueRange customerList = new ValueRange();
				final List<List<Object>> customerReport = new ArrayList<>();
				customerReport.add(reportColumns);
				for (final B2BCustomerModel customersModel : registeredCustomers)
				{
					if (null != customersModel)
					{
						final List<Object> reportValues = new ArrayList<>();
						final String dateRequested = dateTimeFormatter.format(customersModel.getCreationtime());
						reportValues.add(dateRequested);
						reportValues.add(customersModel.getFirstName() != null ? customersModel.getFirstName() : StringUtils.EMPTY);
						reportValues.add(customersModel.getLastname() != null ? customersModel.getLastname() : StringUtils.EMPTY);
						reportValues.add(
								customersModel.getOrganisationName() != null ? customersModel.getOrganisationName() : StringUtils.EMPTY);
						reportValues.add(customersModel.getEmail() != null ? customersModel.getEmail() : StringUtils.EMPTY);
						reportValues.add(customersModel.getPhone() != null ? customersModel.getPhone() : StringUtils.EMPTY);
						reportValues.add(customersModel.getPhoneExt() != null ? customersModel.getPhoneExt() : StringUtils.EMPTY);
						if (null != customersModel.getDefaultB2BUnit())
						{
							reportValues
									.add(customersModel.getDefaultB2BUnit().getUid() != null
											&& !customersModel.getDefaultB2BUnit().getUid().equals("SeqirusUK")
													? customersModel.getDefaultB2BUnit().getUid()
											: StringUtils.EMPTY);
							if (customersModel.getDefaultB2BUnit().getUid().equals("SeqirusUK"))
							{
								final SeqirusB2BTempCustomerModel customerDataMoel = seqirusCustomerRegistrationService
										.fetchCustModel(customersModel.getUid());
								if (null != customerDataMoel && null != customerDataMoel.getCompanyInfo())
								{
									reportValues.add(customerDataMoel.getCompanyInfo().getCompanyName() != null
											? customerDataMoel.getCompanyInfo().getCompanyName()
											: StringUtils.EMPTY);
									reportValues.add(customerDataMoel.getCompanyInfo().getPostCode() != null
											? customerDataMoel.getCompanyInfo().getPostCode()
											: StringUtils.EMPTY);
								}else {
									reportValues.add(StringUtils.EMPTY);
									reportValues.add(StringUtils.EMPTY);
								}
							}
							else
							{
								reportValues.add(customersModel.getDefaultB2BUnit().getCompanyName() != null
									? customersModel.getDefaultB2BUnit().getCompanyName()
									: StringUtils.EMPTY);
								getAddressByType(customersModel.getDefaultB2BUnit(), reportValues, AddressType.SP.getCode());
							}

						}
						if (null != customersModel.getStatus() && StringUtils.equalsIgnoreCase(AccountStatusEnum.JA_COMPLETE.getCode(),
								customersModel.getStatus().getCode()))
						{
							reportValues.add("Y");
							reportValues.add(StringUtils.EMPTY);
							reportValues.add(StringUtils.EMPTY);
						}

						else if (null != customersModel.getStatus() && StringUtils
								.equalsIgnoreCase(AccountStatusEnum.JA_PENDING_APPROVAL.getCode(), customersModel.getStatus().getCode()))
						{
							reportValues.add(StringUtils.EMPTY);
							reportValues.add("Y");
							reportValues.add(StringUtils.EMPTY);
						}

						else if (null != customersModel.getStatus() && StringUtils
								.equalsIgnoreCase(AccountStatusEnum.CR_COMPLETE.getCode(), customersModel.getStatus().getCode()))
						{
							reportValues.add(StringUtils.EMPTY);
							reportValues.add(StringUtils.EMPTY);
							reportValues.add("Y");
						}

						customerReport.add(reportValues);
				}
			}
			customerList.setValues(customerReport);
			final Sheets sheetsService = createSheetsService();
			final ClearValuesRequest clearRequestBody = new ClearValuesRequest();
			final Sheets.Spreadsheets.Values.Clear clearRequest = sheetsService.spreadsheets().values().clear(spreadSheetId,
					clearRange, clearRequestBody);
			final ClearValuesResponse clearresponse = clearRequest.execute();

			final Sheets.Spreadsheets.Values.Update request = sheetsService.spreadsheets().values().update(spreadSheetId,
					range, customerList);
			request.setValueInputOption(valueInputOption);

			final UpdateValuesResponse response = request.execute();
			System.out.println(response);
			}
			catch (GeneralSecurityException | IOException e)
			{
				e.printStackTrace();
			}
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}


	/**
	 * @param defaultB2BUnit
	 * @param reportValues
	 * @param code
	 * @param reportColumns
	 */
	private void getAddressByShipType(final B2BUnitModel defaultB2BUnit, final List<Object> reportValues, final String addressType)
	{
		if (null != defaultB2BUnit && CollectionUtils.isNotEmpty(defaultB2BUnit.getAddresses()))
		{
			for (final AddressModel address : defaultB2BUnit.getAddresses())
			{
				if (address.getSapCustomerType().contains(addressType))
				{
					reportValues.add(address.getLicenseName());
					reportValues.add(address.getLicenseNumber());
				}
			}
		}
	}

	/**
	 * @param defaultB2BUnit
	 * @param orgValues
	 * @param reportColumns
	 * @param sp
	 */
	private void getAddressByType(final B2BUnitModel defaultB2BUnit, final List<Object> reportValues, final String addressType)
	{
		if (null != defaultB2BUnit && CollectionUtils.isNotEmpty(defaultB2BUnit.getAddresses()))
		{
			for (final AddressModel address : defaultB2BUnit.getAddresses())
			{
				if (address.getSapCustomerType().contains(addressType))
				{
					reportValues.add(address.getPostalcode() != null ? address.getPostalcode() : StringUtils.EMPTY);
					break;
				}

			}
		}
	}

	/**
	 * @return
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	private Sheets createSheetsService() throws GeneralSecurityException, IOException
	{
		transport = GoogleNetHttpTransport.newTrustedTransport();
		jsonFactory = JacksonFactory.getDefaultInstance();
		final Credential credential = authorize();
		return new Sheets.Builder(transport, jsonFactory, credential).setApplicationName("SeqirusTest").build();
	}

	/**
	 * @return
	 * @throws IOException
	 */
	private Credential authorize() throws IOException
	{
		final InputStream credentialsJSON = SeqirusCustomerReportJob.class.getClassLoader()
				.getResourceAsStream(configurationService.getConfiguration()
						.getString(Seqirusb2bCoreConstants.GOOGLE_SHEET_API_SPREADSHEET_SERVICEACCOUNT_JSON));

		final GoogleCredential gcFromJson = GoogleCredential.fromStream(credentialsJSON, transport, jsonFactory)
				.createScoped(scopes);

		return new GoogleCredential.Builder().setTransport(transport).setJsonFactory(jsonFactory)
				.setServiceAccountId(configurationService.getConfiguration()
						.getString(Seqirusb2bCoreConstants.GOOGLE_SHEET_API_SPREADSHEET_SERVICEACCOUNT_ID))
				.setServiceAccountScopes(scopes)
				.setServiceAccountPrivateKey(gcFromJson.getServiceAccountPrivateKey()).build();
	}

	/**
	 * @return the seqirusCustomerRegistrationService
	 */
	protected SeqirusCustomerRegistrationService getSeqirusCustomerRegistrationService()
	{
		return seqirusCustomerRegistrationService;
	}

	/**
	 * @param seqirusCustomerRegistrationService
	 *           the seqirusCustomerRegistrationService to set
	 */

	public void setSeqirusCustomerRegistrationService(final SeqirusCustomerRegistrationService seqirusCustomerRegistrationService)
	{
		this.seqirusCustomerRegistrationService = seqirusCustomerRegistrationService;
	}
}
