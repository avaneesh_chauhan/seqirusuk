/**
 *
 */
package com.seqirus.core.dataObjects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author 700196
 *
 */
public class InvoiceDetailsLineItemsResponse
{
	@JsonProperty(value = "invoiceItem")
	public String invoiceItem;
	@JsonProperty(value = "materialNumber")
	public String materialNumber;
	@JsonProperty(value = "materialDescription")
	public String materialDescription;
	@JsonProperty(value = "batchNo")
	public String batchNo;
	@JsonProperty(value = "unitOfmeasure")
	public String unitOfmeasure;
	@JsonProperty(value = "unitPrice")
	public float unitPrice;
	@JsonProperty(value = "quantity")
	public String quantity;
	@JsonProperty(value = "tax")
	public float tax;
	@JsonProperty(value = "totalCost")
	public float totalCost;
	@JsonProperty(value = "netAmount")
	public float netAmount;
	/**
	 * @return the invoiceItem
	 */
	public String getInvoiceItem()
	{
		return invoiceItem;
	}
	/**
	 * @param invoiceItem the invoiceItem to set
	 */
	public void setInvoiceItem(final String invoiceItem)
	{
		this.invoiceItem = invoiceItem;
	}
	/**
	 * @return the materialNumber
	 */
	public String getMaterialNumber()
	{
		return materialNumber;
	}
	/**
	 * @param materialNumber the materialNumber to set
	 */
	public void setMaterialNumber(final String materialNumber)
	{
		this.materialNumber = materialNumber;
	}
	/**
	 * @return the materialDescription
	 */
	public String getMaterialDescription()
	{
		return materialDescription;
	}
	/**
	 * @param materialDescription the materialDescription to set
	 */
	public void setMaterialDescription(final String materialDescription)
	{
		this.materialDescription = materialDescription;
	}
	/**
	 * @return the batchNo
	 */
	public String getBatchNo()
	{
		return batchNo;
	}
	/**
	 * @param batchNo the batchNo to set
	 */
	public void setBatchNo(final String batchNo)
	{
		this.batchNo = batchNo;
	}
	/**
	 * @return the unitOfmeasure
	 */
	public String getUnitOfmeasure()
	{
		return unitOfmeasure;
	}
	/**
	 * @param unitOfmeasure the unitOfmeasure to set
	 */
	public void setUnitOfmeasure(final String unitOfmeasure)
	{
		this.unitOfmeasure = unitOfmeasure;
	}
	/**
	 * @return the unitPrice
	 */
	public float getUnitPrice()
	{
		return unitPrice;
	}
	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(final float unitPrice)
	{
		this.unitPrice = unitPrice;
	}
	/**
	 * @return the quantity
	 */
	public String getQuantity()
	{
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(final String quantity)
	{
		this.quantity = quantity;
	}
	/**
	 * @return the tax
	 */
	public float getTax()
	{
		return tax;
	}
	/**
	 * @param tax the tax to set
	 */
	public void setTax(final float tax)
	{
		this.tax = tax;
	}
	/**
	 * @return the totalCost
	 */
	public float getTotalCost()
	{
		return totalCost;
	}
	/**
	 * @param totalCost the totalCost to set
	 */
	public void setTotalCost(final float totalCost)
	{
		this.totalCost = totalCost;
	}

	/**
	 * @return the netAmount
	 */
	public float getNetAmount()
	{
		return netAmount;
	}

	/**
	 * @param netAmount
	 *           the netAmount to set
	 */
	public void setNetAmount(final float netAmount)
	{
		this.netAmount = netAmount;
	}


}
