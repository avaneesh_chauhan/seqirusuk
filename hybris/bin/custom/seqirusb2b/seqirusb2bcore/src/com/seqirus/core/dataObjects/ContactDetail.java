/**
 *
 */
package com.seqirus.core.dataObjects;

/**
 * Holds the contact details of Billing /Payer and shipping contact of customer
 *
 */
public class ContactDetail
{
	String firstName;
	String lastName;
	String jobTitle;
	String phone;
	String phoneExt;
	String email;

	public ContactDetail(final String firstName, final String lastName, final String jobTitle, final String phone,
			final String phoneExt, final String email)
	{
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.jobTitle = jobTitle;
		this.phone = phone;
		this.phoneExt = phoneExt;
		this.email = email;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public String getJobTitle()
	{
		return jobTitle;
	}

	public String getPhone()
	{
		return phone;
	}

	public String getPhoneExt()
	{
		return phoneExt;
	}

	public String getEmail()
	{
		return email;
	}

	@Override
	public String toString()
	{
		return "ContactDetail [firstName=" + firstName + ", lastName=" + lastName + ", jobTitle=" + jobTitle + ", phone=" + phone
				+ ", phoneExt=" + phoneExt + ", email=" + email + "]";
	}


}
