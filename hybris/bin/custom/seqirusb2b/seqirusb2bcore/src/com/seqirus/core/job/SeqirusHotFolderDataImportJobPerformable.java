/**
 *
 */
package com.seqirus.core.job;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.core.orders.service.SeqirusOrdersService;
import com.seqirus.core.registration.service.SeqirusCustomerRegistrationService;


/**
 * @author 813784
 *
 */
public class SeqirusHotFolderDataImportJobPerformable extends AbstractJobPerformable<CronJobModel>
{
	/** The seqirus invoice listing request populator. */
	@Resource(name = "seqirusB2BUnitDataSyncPopulator")
	private Populator<B2BUnitModel, SeqirusB2BTempCustomerModel> seqirusB2BUnitDataSyncPopulator;

	private static final Logger LOG = Logger.getLogger(SeqirusHotFolderDataImportJobPerformable.class);

	@Resource(name = "seqirusOrdersService")
	private SeqirusOrdersService seqirusOrdersService;

	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		final SeasonEntryModel season = seqirusOrdersService.getSeasonEntry();

		if (season.getIsDataImported())
		{

			final List<B2BCustomerModel> customers = seqirusCustomerRegistrationService.getAllCustomers();
			for (final B2BCustomerModel customer : customers)
			{
				if (null != customer.getDefaultB2BUnit() && !customer.getDefaultB2BUnit().getUid().equals("SeqirusUK"))
				{
			final SeqirusB2BTempCustomerModel customerDataMoel = seqirusCustomerRegistrationService
					.fetchCustModel(customer.getUid());

			seqirusB2BUnitDataSyncPopulator.populate(customer.getDefaultB2BUnit(), customerDataMoel);
		}
			}
			season.setIsDataImported(false);
			modelService.save(season);
		}

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * @return the seqirusCustomerRegistrationService
	 */
	protected SeqirusCustomerRegistrationService getSeqirusCustomerRegistrationService()
	{
		return seqirusCustomerRegistrationService;
	}

	/**
	 * @param seqirusCustomerRegistrationService
	 *           the seqirusCustomerRegistrationService to set
	 */

	public void setSeqirusCustomerRegistrationService(final SeqirusCustomerRegistrationService seqirusCustomerRegistrationService)
	{
		this.seqirusCustomerRegistrationService = seqirusCustomerRegistrationService;
	}



}
