/**
 *
 */
package com.seqirus.core.dataObjects;

public class PartnerDataJSON extends CommonDataJSON
{
	
	public static final String BILLTO="BillTo";
	public static  final String SHIPTO="ShipTo";
	public  static final String SOLDTO="SoldTo";
	public  static final String PAYER="Payer";
	public String dunns;
	public String loginEmail;
	public PreferenceJSON preference;
	@Override
	public String toString() {
		return "PartnerDataJSON [dunns=" + dunns + ", loginEmail=" + loginEmail + ", city=" + city + ", zipCode="
				+ zipCode + ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", state=" + state
				+ ", orgName=" + orgName + ", partnerID=" + partnerID + ", firstName=" + firstName + ", lastName="
				+ lastName + ", jobTitle=" + jobTitle + ", email=" + email + ", phoneNumber=" + phoneNumber
				+ ", phoneExt=" + phoneExt + "]";
	}

	
	
	
	
	

}
