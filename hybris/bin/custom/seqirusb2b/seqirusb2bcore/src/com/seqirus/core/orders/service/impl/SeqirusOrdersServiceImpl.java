/**
 *
 */
package com.seqirus.core.orders.service.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.seqirus.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.core.dataObjects.BillingSummary;
import com.seqirus.core.dataObjects.CustomerDataJSON;
import com.seqirus.core.dataObjects.CustomerSummary;
import com.seqirus.core.dataObjects.OrderDetailJSON;
import com.seqirus.core.dataObjects.OrderSummary;
import com.seqirus.core.dataObjects.OrgSummary;
import com.seqirus.core.dataObjects.PartnerDataJSON;
import com.seqirus.core.dataObjects.PartnerLookupJSON;
import com.seqirus.core.dataObjects.PartnerSummary;
import com.seqirus.core.dataObjects.PayerSummary;
import com.seqirus.core.dataObjects.PriceRequestParameter;
import com.seqirus.core.dataObjects.ProductDetailJSON;
import com.seqirus.core.dataObjects.RequestParameters;
import com.seqirus.core.dataObjects.ShipmentDetail;
import com.seqirus.core.dataObjects.ShipmentDetailJSON;
import com.seqirus.core.dataObjects.ShippingSummary;
import com.seqirus.core.dataObjects.ShiptoDataJSON;
import com.seqirus.core.dataObjects.SoldToSummary;
import com.seqirus.core.dataObjects.TrackingDetailJSON;
import com.seqirus.core.exceptions.SeqirusCustomException;
import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.model.SeqirusSeasonModel;
import com.seqirus.core.orders.dao.impl.SeqirusOrdersDao;
import com.seqirus.core.orders.service.SeqirusOrdersService;
import com.seqirus.core.services.SeqirusAPIService;


/**
 * @author 614269
 *
 */
public class SeqirusOrdersServiceImpl extends SeqirusAPIService implements SeqirusOrdersService
{
	static Logger logger = Logger.getLogger(SeqirusOrdersServiceImpl.class);
	public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);
	public static final String SALES_ORGANIZATION_KEY = "Sales_Organization";
	public static final String ORDERAPI_URL_PREFIX = "Orders";
	final static String API_LANDINGURL_ALL_CUSTOMER = "allcustomer.api.landing.page.url";
	final static String API_LANDINGURL = "api.landing.page.url";
	final static String API_LANDINGURL_PARTNER = "partner.api.landing.page.url";
	final static String API_LANDINGURL_PARTNER_DETAILS = "partnerDetails.api.landing.page.url";

	public static final String NOT_PROCESSED = "Not yet processed";
	public static final String STATUS_INPROCES = "Order in Process";
	public static final String STATUS_OPEN = "Open";
	public static final String STATUS_CANCELLED = "Closed";
	public static final String ORDER_CANCELLED = "Order Cancelled";
	public static final String STATUS_COMPLETE = "Order Completed";
	public static final String PARTIALLY_PROCESSED = "Partially processed";
	public static final String COMPLETED_PROCESS = "Completely processed";
	public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	public static final String CUSTOMER_REJECTED = "Customer Rejected";
	public static final String PO_TYPE_REPL = "REPL";
	public static final String ACTIVE_ORDER = "Active";
	public static final String CUSTOMER_REQUESTED = "Customer request";

	public static final String STATUS_INPROCES_FLU360 = "Order in Process";
	public static final String STATUS_OPEN_FLU360 = "Order Placed";
	public static final String STATUS_COMPLETE_FLU360 = "Order Completed";
	public static final String STATUS_CANCELLED_FLU360 = "Cancelled";
	public static final String NOT_PROCESSED_FLU360 = "Not yet processed";
	public static final String COMPLETED_PROCESS_FLU360 = "Completely processed";
	public static final String CLOSED_ORDER = "Closed";


	@Resource(name = "seqirusOrdersDao")
	SeqirusOrdersDao seqirusOrdersDao;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;


	@Autowired
	protected ConfigurationService configurationService;

	/** The certificate path. */
	protected String certificatePath;

	@Override
	public List<OrderSummary> getOrders(final String customerId, final String season)
	{
		List<OrderSummary> activeOrders = new ArrayList<OrderSummary>();
		List<OrderSummary> closedOrders = new ArrayList<OrderSummary>();
		if (configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.ASHFIELD_PRESENCE).equals("exist")
				&& configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.ASHFIELD_ALLOWED_SEASONS)
						.contains(season))
		{
			activeOrders = fetchOrders(customerId, season, ACTIVE_ORDER);
		}
		else
		{
			activeOrders = fetchOrders(customerId, season, ACTIVE_ORDER);
			closedOrders = fetchOrders(customerId, season, CLOSED_ORDER);
		}
		final Iterator<OrderSummary> closedOrdItr = closedOrders.iterator();
		while (closedOrdItr.hasNext())
		{
			final OrderSummary summary = closedOrdItr.next();
			if (summary.getStatus().equals(STATUS_COMPLETE) && !summary.isFullyPaid())
			{
				summary.setStatus(STATUS_INPROCES);
				activeOrders.add(summary);
				closedOrdItr.remove();
			}
		}
		final Map<String, List<OrderSummary>> orders = new HashMap<String, List<OrderSummary>>();
		orders.put(ACTIVE_ORDER, activeOrders);
		orders.put(CLOSED_ORDER, closedOrders);
		return mapActiveAndClosedOrders(orders);
	}

	/**
	 * @param allOrders
	 *
	 */
	private List<OrderSummary> mapActiveAndClosedOrders(final Map<String, List<OrderSummary>> allOrders)
	{
		List<OrderSummary> allCustomerOrderlist = new ArrayList<OrderSummary>();
		List<OrderSummary> allCustomerOrderlistClosed = new ArrayList<OrderSummary>();
		final List<OrderSummary> orders = new ArrayList<OrderSummary>();
		for (final Map.Entry<String, List<OrderSummary>> entry : allOrders.entrySet())
		{

			if (entry.getKey().equalsIgnoreCase("Active"))
			{
				allCustomerOrderlist = entry.getValue();
			}
			else
			{

				allCustomerOrderlistClosed = entry.getValue();
			}

		}
		orders.addAll(allCustomerOrderlist);
		orders.addAll(allCustomerOrderlistClosed);
		return orders;
	}

	/**
	 * @param customerId
	 * @param orderStatus
	 * @return
	 */
	private List<OrderSummary> fetchOrders(final String customerId, final String season, final String orderStatus)
			throws SeqirusCustomException
	{

		final List<SeqirusSeasonModel> list = getSeasonEntry().getSeasonList();
		String fromDate = "2019-04-01";
		String toDate = "2020-07-04";
		if (list != null)
		{
			for (final SeqirusSeasonModel model : list)
			{
				final String modelSeason = trim(model.getOrderSeason());

				if (season.trim().equals(modelSeason))
				{
					fromDate = DATE_FORMATTER.format(model.getSeasonStartDate());
					toDate = DATE_FORMATTER.format(model.getSeasonEndDate());
					break;
				}

			}
		}

		final Map<String, String> parameter = new HashMap<>();
		parameter.put(Seqirusb2bCoreConstants.FROM_DATE_KEY, fromDate);
		parameter.put(Seqirusb2bCoreConstants.TO_DATE_KEY, toDate);
		parameter.put(Seqirusb2bCoreConstants.ORDER_STATUS_ALL, orderStatus);


		certificatePath = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.API_CERTIFICATE_LOCATION);

		final OrderDetailJSON jsonResponse = (OrderDetailJSON) processRequest(
				new PriceRequestParameter(parameter, prepareOrdersLandingURL(parameter, customerId), StringUtils.EMPTY,
						Seqirusb2bCoreConstants.REQUEST_GET, certificatePath, Seqirusb2bCoreConstants.API_RESPONSE),
				OrderDetailJSON.class);
		List<OrderSummary> result = new ArrayList<OrderSummary>();
		if (null != jsonResponse)
		{
			result = prepareOrderSummaries(jsonResponse.orders, ORDERAPI_URL_PREFIX);
		}
		return result;


	}

	/**
	 * @param parameter
	 * @return String
	 */
	private String prepareAllCustomerURL(final Map<String, String> parameter, final String customerId)
	{

		final String currentSite = cmsSiteService.getCurrentSite().getUid();
		logger.info("current site : " + currentSite);
		final String endpointURL = configurationService.getConfiguration().getString(API_LANDINGURL_PARTNER);
		final String organizationId = configurationService.getConfiguration()
				.getString(currentSite + "." + Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER);
		final StringBuilder sb = new StringBuilder();
		sb.append(endpointURL).append(customerId).append("/").append(organizationId);
		if (!parameter.isEmpty())
		{
			sb.append(Seqirusb2bCoreConstants.QUESTION_MARK);
		}
		String ampChar = "";
		for (final String s : parameter.keySet())
		{
			sb.append(ampChar).append(s).append(Seqirusb2bCoreConstants.EQUAL_SIGN).append(parameter.get(s));
			ampChar = Seqirusb2bCoreConstants.AMPERSAND;
		}
		return sb.toString();
	}

	private String prepareOrdersLandingURL(final Map<String, String> parameter, final String customerId)
	{

		final String currentSite = cmsSiteService.getCurrentSite().getUid();
		logger.info("current site" + currentSite);
		final String endpointURL = configurationService.getConfiguration().getString(currentSite + "." + API_LANDINGURL);
		final String organizationId = configurationService.getConfiguration()
				.getString(currentSite + "." + Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER);
		final StringBuilder sb = new StringBuilder();
		sb.append(endpointURL).append(customerId).append("/").append(organizationId);
		if (!parameter.isEmpty())
		{
			sb.append(Seqirusb2bCoreConstants.QUESTION_MARK);
		}
		String ampChar = "";
		for (final String s : parameter.keySet())
		{
			sb.append(ampChar).append(s).append(Seqirusb2bCoreConstants.EQUAL_SIGN).append(parameter.get(s));
			ampChar = Seqirusb2bCoreConstants.AMPERSAND;
		}
		return sb.toString();
	}

	private String preparePartnerLookupURL(final Map<String, String> parameter, final String customerId)
	{

		final String currentSite = cmsSiteService.getCurrentSite().getUid();
		logger.info("current site" + currentSite);
		final String endpointURL = configurationService.getConfiguration().getString(API_LANDINGURL_PARTNER);
		final String organizationId = configurationService.getConfiguration()
				.getString(currentSite + "." + Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER);
		final StringBuilder sb = new StringBuilder();
		sb.append(endpointURL).append(customerId).append("/").append("partners");
		if (!parameter.isEmpty())
		{
			sb.append(Seqirusb2bCoreConstants.QUESTION_MARK);
		}
		String ampChar = "";
		for (final String s : parameter.keySet())
		{
			sb.append(ampChar).append(s).append(Seqirusb2bCoreConstants.EQUAL_SIGN).append(parameter.get(s));
			ampChar = Seqirusb2bCoreConstants.AMPERSAND;
		}
		return sb.toString();
	}


	private String preparePartnerDetailsURL(final String partnerType, final String customerId)
	{

		final String currentSite = cmsSiteService.getCurrentSite().getUid();
		logger.info("current site" + currentSite);
		final String endpointURL = configurationService.getConfiguration().getString(API_LANDINGURL_PARTNER_DETAILS);
		final String organizationId = configurationService.getConfiguration()
				.getString(currentSite + "." + Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER);
		final StringBuilder sb = new StringBuilder();
		sb.append(endpointURL).append(customerId).append("/").append("partners").append("/").append(partnerType);

		return sb.toString();
	}

	/**
	 * Prepares list of order summary.
	 *
	 * @param orders
	 * @param status
	 * @return List<OrderSummary>
	 */
	private List<OrderSummary> prepareOrderSummaries(final List<OrderDetailJSON> orders, final String status)
	{
		final List<OrderSummary> orderSummaryList = new ArrayList();
		if (orders != null)
		{
			for (final OrderDetailJSON json : orders)
			{
				orderSummaryList.add(prepareOrderSummary(json, status));
			}
		}

		return orderSummaryList;
	}

	/**
	 * Converts JSON response to POJO for Order API call based on various logics
	 *
	 * @param json
	 * @param requestStatus
	 * @return OrderSummary
	 */
	private OrderSummary prepareOrderSummary(final OrderDetailJSON json, final String requestStatus)
	{
		String status = null;
		final String currentSite = cmsSiteService.getCurrentSite().getUid();
		if (currentSite.equalsIgnoreCase("seqirusb2b-uk"))
		{

			status = STATUS_INPROCES;
			json.orderStatus = trim(json.orderStatus);
			if (NOT_PROCESSED.equalsIgnoreCase(json.orderStatus))
			{
				status = STATUS_OPEN;
			}
			else if (PARTIALLY_PROCESSED.equalsIgnoreCase(json.orderStatus))
			{
				status = STATUS_INPROCES;
			}
			else if (COMPLETED_PROCESS.equalsIgnoreCase(json.orderStatus))
			{
				status = STATUS_COMPLETE;
			}
			else if (ORDER_CANCELLED.equalsIgnoreCase(json.orderStatus))
			{
				status = STATUS_CANCELLED;
			}
		}
		else
		{

			status = STATUS_INPROCES_FLU360;
			json.orderStatus = trim(json.orderStatus);
			if (NOT_PROCESSED_FLU360.equalsIgnoreCase(json.orderStatus))
			{
				status = STATUS_OPEN_FLU360;
			}
			if (COMPLETED_PROCESS_FLU360.equalsIgnoreCase(json.orderStatus) && !ACTIVE_ORDER.equalsIgnoreCase(requestStatus))
			{
				status = STATUS_COMPLETE_FLU360;
			}
		}

		final OrderSummary summary = new OrderSummary(json.orderID, parseDate(json.orderDate), json.totalNetValue,
				removeLeadingZero(json.shipToPartner), removeLeadingZero(json.soldToPartnerID), status, json.poNumber);

		summary.setShipToAddress(json.addressLine1, json.addressLine2, json.city, json.state, json.zipCode);
		summary.setPartnerName(json.partnerName);
		if (json.products != null)
		{

			final Map<String, List<ProductDetailJSON>> productMap = new HashMap<String, List<ProductDetailJSON>>();

			for (final ProductDetailJSON p : json.products)
			{
				if (productMap.containsKey(p.materialID))
				{
					productMap.get(p.materialID).add(p);
				}
				else
				{
					final List<ProductDetailJSON> pList = new ArrayList();
					pList.add(p);
					productMap.put(p.materialID, pList);
				}
			}
			final Map<String, Integer> cancelledQtyMap = new HashMap<String, Integer>();
			int maxItemNumber = 0;
			int validCount = 0;


			//			String anyOneMaterial = null;
			for (final String materialID : productMap.keySet())
			{
				final List<ProductDetailJSON> materialList = productMap.get(materialID);
				int totalQty = 0;
				String itemNumber = null;
				int itemQty = 0;

				//anyOneMaterial = materialID;
				int overallQty = 0;
				for (final ProductDetailJSON prod : materialList)
				{

					prod.reasonForRejection = trim(prod.reasonForRejection);
					final String pOType = prod.poType == null ? "" : trim(prod.poType);
					summary.addPOType(pOType);

					if (CUSTOMER_REJECTED.equalsIgnoreCase(prod.reasonForRejection) || PO_TYPE_REPL.equalsIgnoreCase(pOType)
							|| CUSTOMER_REQUESTED.equalsIgnoreCase(prod.reasonForRejection))
					{
						//totalQty -= prod.quantity;
						//Reducing the total cost in case of rejection
						//summary.setTotalCost(summary.getTotalCost() - prod.netValue);
					}
					else
					{

						totalQty += prod.quantity;
						itemNumber = prod.itemNumber;
						itemQty = prod.quantity;
						validCount++;
						if (itemNumber == null || itemNumber == "")
						{
							itemNumber = "000010";
						}
						// added for handle negative qty and shippingWave data
						summary.addItemProduct(removeLeadingZero(materialID), removeLeadingZero(itemNumber), itemQty, prod.shippingWave,
								parseDate(prod.promisedDate));
					}
					overallQty += prod.quantity;
				}

				if (itemNumber != null && itemNumber != "" && totalQty > 0)
				{
					summary.addProduct(removeLeadingZero(materialID), removeLeadingZero(itemNumber), totalQty, itemQty);
					if (Integer.parseInt(itemNumber) > maxItemNumber)
					{
						maxItemNumber = Integer.parseInt(itemNumber);
					}
				}
				else
				{
					cancelledQtyMap.put(materialID, overallQty);
				}
			}
			summary.setMaxItemNumber(maxItemNumber);
			if (validCount == 0)
			{

				if (currentSite.equalsIgnoreCase("seqirusb2b-uk"))
				{
					summary.setStatus(STATUS_CANCELLED);
				}
				else
				{
					summary.setStatus(STATUS_CANCELLED_FLU360);
				}

				for (final String s : cancelledQtyMap.keySet())
				{
					summary.addProduct(removeLeadingZero(s), "", cancelledQtyMap.get(s));
				}
			}

		}

		//If no Shipment , set the order to open
		if (json.shipments == null || json.shipments.isEmpty())
		{
			if (currentSite.equalsIgnoreCase("seqirusb2b-uk"))
			{
				if (STATUS_INPROCES.equalsIgnoreCase(summary.getStatus()))
				{
					summary.setStatus(STATUS_OPEN);
				}
			}
			else
			{
				if (STATUS_INPROCES_FLU360.equalsIgnoreCase(summary.getStatus()))
				{
					summary.setStatus(STATUS_OPEN_FLU360);
				}
			}
		}
		if (json.shipments != null)
		{
			for (final ShipmentDetailJSON shipment : json.shipments)
			{

				final ShipmentDetail detail = new ShipmentDetail(shipment.deliveryStatus, parseDate(shipment.deliveryDate),
						shipment.deliveryNumber, parseDate(shipment.pgiDate));
				for (final String invoiceNumber : shipment.getInvoiceNumbers())
				{
					detail.addInvoice(invoiceNumber);
				}
				if (!shipment.getCreditNotes().isEmpty())
				{
					detail.setCreditsAvailable(true);
				}

				for (final TrackingDetailJSON trackingURL : shipment.getTracking())
				{
					detail.addTracking(trackingURL.trackingID, trackingURL.trackingLink);
				}

				for (final ProductDetailJSON product : shipment.getProducts())
				{
					detail.addShipmentProduct(removeLeadingZero(product.materialID), "", product.quantity);
				}
				summary.addShippingDetails(detail);

			}
		}
		return summary;

	}


	@Override
	public SeasonEntryModel getSeasonEntry()
	{
		return seqirusOrdersDao.seasonEntry();

	}

	/**
	 * Gets the certificate path.
	 *
	 * @return the certificatePath
	 */
	public String getCertificatePath()
	{
		return certificatePath;
	}

	/**
	 * Sets the certificate path.
	 *
	 * @param certificatePath
	 *           the certificatePath to set
	 */
	public void setCertificatePath(final String certificatePath)
	{
		this.certificatePath = certificatePath;
	}


	@Override
	public CustomerSummary fetchCustomerDetail(final String accountNumber, final String zipCode)
	{
		// XXX Auto-generated method stub
		if (accountNumber == null)
		{
			return null;
		}
		final String currentSite = cmsSiteService.getCurrentSite().getUid();
		logger.info("current site" + currentSite);
		final String orgId = configurationService.getConfiguration()
				.getString(currentSite + "." + Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER);

		final Map<String, String> parameter = new HashMap<>();

		parameter.put("ZipCode", zipCode.replaceAll(" ", "+"));

		parameter.put("ZipCodeValidationRequired", "true");


		certificatePath = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.API_CERTIFICATE_LOCATION);


		if (currentSite.equalsIgnoreCase("seqirusb2b-uk"))
		{

			// place holder for UK code
			return null;
		}
		else
		{
			final CustomerDataJSON jsonResponse = (CustomerDataJSON) processRequest(
					new RequestParameters(parameter, prepareAllCustomerURL(parameter, accountNumber), StringUtils.EMPTY,
							Seqirusb2bCoreConstants.REQUEST_GET, certificatePath, Seqirusb2bCoreConstants.API_RESPONSE),
					CustomerDataJSON.class);

			if (jsonResponse == null)
			{
				return null;
			}
			final CustomerSummary summary = new CustomerSummary();
			summary.populate(jsonResponse);
			return summary;

		}

	}


	@Override
	public PartnerSummary fetchPartnerSummary(final String accountNumber)
	{
		// XXX Auto-generated method stub
		if (accountNumber == null)
		{
			return null;
		}
		final String currentSite = cmsSiteService.getCurrentSite().getUid();
		logger.info("current site" + currentSite);
		final String orgId = configurationService.getConfiguration()
				.getString(currentSite + "." + Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER);

		final Map<String, String> parameter = new HashMap<>();
		parameter.put("organizationId", orgId);


		certificatePath = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.API_CERTIFICATE_LOCATION);


		if (currentSite.equalsIgnoreCase("seqirusb2b-uk"))
		{

			// place holder for UK code
			return null;
		}
		else
		{
			final PartnerLookupJSON jsonResponse = (PartnerLookupJSON) processRequest(
					new RequestParameters(parameter, preparePartnerLookupURL(parameter, accountNumber), StringUtils.EMPTY,
							Seqirusb2bCoreConstants.REQUEST_GET, certificatePath, Seqirusb2bCoreConstants.API_RESPONSE),
					PartnerLookupJSON.class);

			if (jsonResponse == null)
			{
				return null;
			}
			return new PartnerSummary(jsonResponse.getAccounts());

		}

	}

	public static final Map<String, OrgSummary> summaryMap = new HashMap<String, OrgSummary>();

	static
	{
		summaryMap.put(PartnerDataJSON.BILLTO, new BillingSummary());
		summaryMap.put(PartnerDataJSON.SOLDTO, new SoldToSummary());
		summaryMap.put(PartnerDataJSON.SHIPTO, new ShippingSummary());
		summaryMap.put(PartnerDataJSON.PAYER, new PayerSummary());
	}


	@Override
	public OrgSummary fetchPartnerDetails(final String accountNumber, final String partnerType)
	{
		// XXX Auto-generated method stub
		if (accountNumber == null)
		{
			return null;
		}
		final String currentSite = cmsSiteService.getCurrentSite().getUid();
		logger.info("current site" + currentSite);
		final String orgId = configurationService.getConfiguration()
				.getString(currentSite + "." + Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER);

		final Map<String, String> parameter = new HashMap<>();
		parameter.put("organizationId", orgId);


		certificatePath = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.API_CERTIFICATE_LOCATION);


		if (currentSite.equalsIgnoreCase("seqirusb2b-uk"))
		{

			// place holder for UK code
			return null;
		}
		else
		{
			final PartnerDataJSON jsonResponse = (PartnerDataJSON) processRequest(
					new RequestParameters(parameter, preparePartnerDetailsURL(partnerType, accountNumber), StringUtils.EMPTY,
							Seqirusb2bCoreConstants.REQUEST_GET, certificatePath, Seqirusb2bCoreConstants.API_RESPONSE),
					ShiptoDataJSON.class);

			if (jsonResponse == null)
			{
				return null;
			}
			OrgSummary summary = summaryMap.get(partnerType);
			summary = summary.createCopy();
			summary.populate(jsonResponse);
			return summary;

		}

	}



}


