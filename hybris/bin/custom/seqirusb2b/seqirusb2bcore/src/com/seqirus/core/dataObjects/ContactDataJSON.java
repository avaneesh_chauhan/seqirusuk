/**
 *
 */
package com.seqirus.core.dataObjects;

public class ContactDataJSON {
	
	public String firstName;
	public String lastName;
	public String jobTitle;
	public String email;
	public String phoneNumber;
	public String phoneExt;
	
	public void setTelephoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public void setExt(String phoneExt) {
		this.phoneExt = phoneExt;
	}
	@Override
	public String toString() {
		return "ContactDataJSON [firstName=" + firstName + ", lastName=" + lastName + ", jobTitle=" + jobTitle
				+ ", email=" + email + ", phoneNumber=" + phoneNumber + ", phoneExt=" + phoneExt + "]";
	}

	
	
}

