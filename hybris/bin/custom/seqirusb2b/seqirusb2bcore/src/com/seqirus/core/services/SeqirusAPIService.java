/**
 *
 */
package com.seqirus.core.services;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.seqirus.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.core.dataObjects.RequestParameters;
import com.seqirus.core.exceptions.SeqirusCustomException;


/**
 * @author 700196
 *
 */
public abstract class SeqirusAPIService
{
	static Logger logger = Logger.getLogger(SeqirusAPIService.class);
	public static final String DEFAULT_SALES_ORG = "0603";
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

	static final Map<String, ResponseProcessor> responseProcessors = new HashMap<>();
	static final Map<String, ResponseRetriever> responseRetrievers = new HashMap<>();

	static
	{
		responseProcessors.put(Seqirusb2bCoreConstants.JSON, new JSONResponseProcessor());
		responseProcessors.put(Seqirusb2bCoreConstants.BYTE, new ByteResponseProcessor());
		responseRetrievers.put(Seqirusb2bCoreConstants.FILE_RESPONSE, new FileResponseRetriever());
		responseRetrievers.put(Seqirusb2bCoreConstants.API_RESPONSE, new APIResponseRetriever());

	}


	protected Object processRequest(final RequestParameters input, final Class<?> parsingClass) throws SeqirusCustomException
	{
		logger.info("Request Url : " + input.getRequestURL());
		input.setRequestURL(input.getRequestURL());
		input.setParsingClass(parsingClass);
		return processRequest(input);
	}

	private Object processRequest(final RequestParameters input)
	{

		// Gets a particular fetcher based on response Type ( File or API)
		final ResponseRetriever fetcher = responseRetrievers.get(input.getResponseType());
		Object response = null;
		InputStream is = null;
		try
		{
			is = fetcher.retrieve(input);
			if (is == null)
			{
				return null;
			}
			//Get a particular response processor based on format (JSON or Byte)
			final ResponseProcessor processor = responseProcessors.get(input.getResponseFormat());
			response = processor.process(is, input);
		}
		catch (final Exception e)
		{
			logger.error("Error in Process Request for " + input);
			throw new SeqirusCustomException(e);
		}
		finally
		{
			try
			{
				if (is != null)
				{
					is.close();
				}
			}
			catch (final IOException e)
			{
				logger.error("IO Exception in processRequest " + e);
			}
		}
		return response;
	}


	protected String trim(final String s)
	{
		if (s == null)
		{
			return s;
		}
		return s.trim();
	}


	protected String removeLeadingZero(final String s)
	{
		return s == null ? null : s.replaceFirst("^0+(?!$)", "");
	}


	/**
	 * Date parsing logic
	 *
	 * @param argDate
	 * @return
	 */
	protected Date parseDate(final String argDate)
	{
		Date orderDate = null;
		if (argDate == null || argDate.contains("0000"))
		{
			return null;
		}
		try
		{
			orderDate = DATE_FORMATTER.parse(argDate);
		}
		catch (final ParseException e)
		{
			logger.error("Error in parsing date " + argDate);
		}
		return orderDate;
	}


}



abstract class ResponseRetriever
	{
		public abstract InputStream retrieve(RequestParameters input) throws IOException;
	}

	class FileResponseRetriever extends ResponseRetriever
	{
		/**
		 * Fetches the response from file to imitate API response
		 */

		@Override
		public InputStream retrieve(final RequestParameters input) throws IOException
		{
			return new FileInputStream(input.getFileName());
		}
	}


	class APIResponseRetriever extends ResponseRetriever
	{
		/**
		 * Call the API with required parameter to retrieve the response stream from MULEsoft API
		 */
		@Override
		public InputStream retrieve(final RequestParameters input) throws IOException
		{
			final String invokeURL = "";
			try
			{
				final URL myUrl = new URL(input.getRequestURL());
				final long l1 = System.currentTimeMillis();

				final HttpsURLConnection conn = (HttpsURLConnection) myUrl.openConnection();
				//Added for request methods such as Post and Put
				if (input.getRequestMethod() != null)
				{
					conn.setRequestMethod(input.getRequestMethod());
					conn.setRequestProperty(Seqirusb2bCoreConstants.CONTENT_TYPE_KEY, Seqirusb2bCoreConstants.CONTENT_TYPE);
					conn.setRequestProperty(Seqirusb2bCoreConstants.ACCEPT_TYPE_KEY, Seqirusb2bCoreConstants.CONTENT_TYPE);
					conn.setDoOutput(true);
					conn.setDoInput(true);
				}
				conn.setRequestProperty(Seqirusb2bCoreConstants.CLIENT_ID,
						Seqirusb2bCoreConstants.CLIENT_ID_VALUE);
				conn.setRequestProperty(Seqirusb2bCoreConstants.CLIENT_SECRET,
						Seqirusb2bCoreConstants.CLIENT_SECRET_VALUE);
			   conn.setHostnameVerifier(new HostnameVerifier() {
	             @Override
	             public boolean verify(final String hostname, final SSLSession sslSession) {
	                 return true;
	             }
	         });
				final KeyStore trustStore = KeyStore.getInstance(Seqirusb2bCoreConstants.API_JKS);
				trustStore.load(new FileInputStream(this.getClass().getResource(input.getCertPath()).getFile()),
						Seqirusb2bCoreConstants.API_CHANGEIT.toCharArray());
			     final TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			     tmf.init(trustStore);
			     final TrustManager[] tms = tmf.getTrustManagers();
			     SSLContext sslContext = null;
				  sslContext = SSLContext.getInstance(Seqirusb2bCoreConstants.API_TLS);
			     sslContext.init(null, tms, new SecureRandom());
			     conn.setSSLSocketFactory(sslContext.getSocketFactory());
				  SeqirusAPIService.logger.info("Requested API Action1 " + conn.getResponseCode());
				  SeqirusAPIService.logger.info("Requested API Action2 " + conn.getInputStream());
				final InputStream stream = conn.getInputStream();
				final long l2 = System.currentTimeMillis();

				SeqirusAPIService.logger.info("API action completed for " + invokeURL + " time taken " + ((l2 - l1) / 1000));

				return stream;
			}
			catch (final KeyManagementException | KeyStoreException | NoSuchAlgorithmException | CertificateException
					| IOException e)
			{
				SeqirusAPIService.logger.error("API action failed  for " + invokeURL);
				SeqirusAPIService.logger.error("callAPI " + e.getMessage());
				throw new SeqirusCustomException(e);
			}
		}
	}

	abstract class ResponseProcessor
	{
		public abstract Object process(InputStream is, RequestParameters input) throws IOException;
	}

	/**
	 * Implements JSON response process logic
	 */
	class JSONResponseProcessor extends ResponseProcessor
	{
		@Override
		public Object process(final InputStream is, final RequestParameters input) throws IOException
		{
			final String response = readResponse(is);
			SeqirusAPIService.logger.info(
					"Response START---------------------------------------------------------------------------------------------------");

			SeqirusAPIService.logger.info(response);
			SeqirusAPIService.logger.info(
					"Response END---------------------------------------------------------------------------------------------------");

			return parseResponse(response, input.getParsingClass(), input.isParseStrategyApplicable());

		}

		public String readResponse(final InputStream is) throws IOException
		{
			final StringBuilder sb = new StringBuilder(StringUtils.EMPTY);
			final InputStreamReader isr = new InputStreamReader(is);
			final BufferedReader br = new BufferedReader(isr);
			String inputLine;
			while ((inputLine = br.readLine()) != null)
			{
				sb.append(inputLine);
			}
			return sb.toString();
		}

		/**
		 * Implements Parsing logic to convert JSON to POJO based on class
		 *
		 * @param jsonResponse
		 * @param c
		 * @param useStrategy
		 * @return
		 */
		protected Object parseResponse(final String jsonResponse, final Class c, final boolean useStrategy)
		{
			if (jsonResponse == null)
			{
				return null;
			}
			final ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (useStrategy)
			{
				mapper.setPropertyNamingStrategy(new ParseStrategy());
			}
			try
			{
				return mapper.readValue(jsonResponse, c);
			}
			catch (final IOException e)
			{
				SeqirusAPIService.logger.error("JSONParsing Exception");
				throw new SeqirusCustomException(e);
			}
		}
	}

	/**
	 * Implements BYTE response process logic
	 */
	class ByteResponseProcessor extends ResponseProcessor
	{
		@Override
		public byte[] process(final InputStream is, final RequestParameters input) throws IOException
		{
			final ByteArrayOutputStream oos = new ByteArrayOutputStream();
			final byte[] buf = new byte[8192];
			int c = 0;
			while ((c = is.read(buf, 0, buf.length)) > 0)
			{
				oos.write(buf, 0, c);
				oos.flush();
			}
			final byte[] output = oos.toByteArray();
			oos.close();
			return output;
		}
	}

	/**
	 * Parsing strategy to take care of issues related to first letter of attribute in JSON response
	 *
	 */
	class ParseStrategy extends PropertyNamingStrategy
	{
		@Override
		public String nameForField(final MapperConfig config, final AnnotatedField field, final String defaultName)
		{

			return convert(defaultName);

		}

		@Override
		public String nameForSetterMethod(final MapperConfig config, final AnnotatedMethod method, final String defaultName)
		{

			return convert(defaultName);
		}

		@Override
		public String nameForGetterMethod(final MapperConfig config, final AnnotatedMethod method, final String defaultName)
		{

			return convert(defaultName);
		}

		public String convert(final String defaultName)
		{
			final char[] arr = defaultName.toCharArray();
			if (arr.length != 0)
			{
				final char upper = Character.toUpperCase(arr[0]);
				arr[0] = upper;

			}
			return new StringBuilder().append(arr).toString();
		}


	}