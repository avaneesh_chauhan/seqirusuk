/**
 *
 */
package com.seqirus.core.dataObjects;

import java.util.List;


/**
 * @author 614269
 *
 */
public class SeqirusMyLocListData
{
	List<SeqirusMyLocationsData> tableData;

	/**
	 * @return the tableData
	 */
	public List<SeqirusMyLocationsData> getTableData()
	{
		return tableData;
	}

	/**
	 * @param tableData
	 *           the tableData to set
	 */
	public void setTableData(final List<SeqirusMyLocationsData> tableData)
	{
		this.tableData = tableData;
	}


}
