package com.seqirus.core.dataObjects;

/**
 * The Class ReturnsAndCreditsRequest.
 *
 * @author Avaneesh Chauhan
 */
public class ReturnsAndCreditsRequest
{

	/** The organization id. */
	private String organizationId;

	/** The from date. */
	private String fromDate;

	/** The to date. */
	private String toDate;

	/** The customer number. */
	private String customerNumber;

	/** The returns. */
	private String returns;

	/**
	 * Gets the returns.
	 *
	 * @return the returns
	 */
	public String getReturns()
	{
		return returns;
	}

	/**
	 * Sets the returns.
	 *
	 * @param returns
	 *           the new returns
	 */
	public void setReturns(final String returns)
	{
		this.returns = returns;
	}

	/**
	 * Gets the organization id.
	 *
	 * @return the organizationId
	 */
	public String getOrganizationId()
	{
		return organizationId;
	}

	/**
	 * Sets the organization id.
	 *
	 * @param organizationId
	 *           the organizationId to set
	 */
	public void setOrganizationId(final String organizationId)
	{
		this.organizationId = organizationId;
	}

	/**
	 * Gets the from date.
	 *
	 * @return the fromDate
	 */
	public String getFromDate()
	{
		return fromDate;
	}

	/**
	 * Sets the from date.
	 *
	 * @param fromDate
	 *           the fromDate to set
	 */
	public void setFromDate(final String fromDate)
	{
		this.fromDate = fromDate;
	}

	/**
	 * Gets the to date.
	 *
	 * @return the toDate
	 */
	public String getToDate()
	{
		return toDate;
	}

	/**
	 * Sets the to date.
	 *
	 * @param toDate
	 *           the toDate to set
	 */
	public void setToDate(final String toDate)
	{
		this.toDate = toDate;
	}

	/**
	 * Gets the customer number.
	 *
	 * @return the customerNumber
	 */
	public String getCustomerNumber()
	{
		return customerNumber;
	}

	/**
	 * Sets the customer number.
	 *
	 * @param customerNumber
	 *           the customerNumber to set
	 */
	public void setCustomerNumber(final String customerNumber)
	{
		this.customerNumber = customerNumber;
	}


}
