package com.seqirus.core.dataObjects;

public class Partner
{
	private final String name;
	private final String name3;
	private final String name4;
	private final String street;
	private final String zipCode;
	private final String city;

	public Partner(final String name, final String name3, final String name4, final String street, final String zipCode,
			final String city)
	{
		super();
		this.name = name;
		this.name3 = name3;
		this.name4 = name4;
		this.street = street;
		this.zipCode = zipCode;
		this.city = city;
	}

	public String getName()
	{
		return name;
	}

	public String getName3()
	{
		return name3;
	}

	public String getName4()
	{
		return name4;
	}

	public String getStreet()
	{
		return street;
	}

	public String getZipCode()
	{
		return zipCode;
	}

	public String getCity()
	{
		return city;
	}

	@Override
	public String toString()
	{
		return "Partner [name=" + name + ", name3=" + name3 + ", name4=" + name4 + ", street=" + street + ", zipCode=" + zipCode
				+ ", city=" + city + "]";
	}



}
