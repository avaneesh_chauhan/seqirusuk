package com.seqirus.core.dataObjects;

import java.util.List;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author 132793
 *
 */
public class PartnerLookupJSON
{
	public static final String BILLTO="BillTo";
	public static  final String SHIPTO="ShipTo";
	public  static final String SOLDTO="SoldTo";
	public  static final String PAYER="Payer";

	Map<String, List<String>> accounts=new HashMap<String,List<String>>();
	
	public void setBillTo(List<String> accountNumber) {
		accounts.put(BILLTO,accountNumber);
	}
	public void setPayer(List<String> accountNumber) {
		accounts.put(PAYER,accountNumber);
	}
	
	public void setShipTo(List<String> accountNumber) {
		accounts.put(SHIPTO,accountNumber);
	}

	public void setSoldTo(List<String> accountNumber) {
		accounts.put(SOLDTO,accountNumber);
	}

	public Map<String, List<String>> getAccounts() {
		return accounts;
	}

}
