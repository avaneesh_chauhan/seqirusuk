/**
 * Holds the common data for BILLTO,SHipTO,SoldTO and Payer
 */
package com.seqirus.core.dataObjects;
import org.apache.log4j.Logger;


import com.seqirus.core.dataObjects.PartnerDataJSON;

abstract public class OrgSummary
{
	ContactDetail contact;
	String orgName;
	AddressDetail address;
	String id;
	static Logger LOGGER = Logger.getLogger(OrgSummary.class);

	public OrgSummary()
	{
	}

	public OrgSummary(final String orgName)
	{
		super();
		this.orgName = orgName;
	}

	public void setContact(final String firstName, final String lastName, final String jobTitle, final String phone,
			final String phoneExt, final String email)
	{
		contact = new ContactDetail(firstName, lastName, jobTitle, phone, phoneExt, email);
	}

	public void setAddress(final String addressLine1, final String addressLine2, final String city, final String state,
			final String zipCode)
	{
		address = new AddressDetail(addressLine1, addressLine2, city, state, zipCode);
	}

	public String getOrgName()
	{
		return orgName;
	}

	public String getFirstName()
	{
		return contact.getFirstName();
	}

	public String getLastName()
	{
		return contact.getLastName();
	}

	public String getJobTitle()
	{
		return contact.getJobTitle();
	}

	public String getContactPhone()
	{
		return contact.getPhone();
	}

	public String getContactPhoneExt()
	{
		return contact.getPhoneExt();
	}

	public String getContactEmail()
	{
		return contact.getEmail();
	}

	public String getAddressLine1()
	{
		return address.getAddressLine1();
	}

	public String getAddressLine2()
	{
		return address.getAddressLine2();
	}

	public String getCity()
	{
		return address.getCity();
	}

	public String getState()
	{
		return address.getState();
	}

	public String getZipCode()
	{
		return address.getZipCode();
	}


	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	//Maps the JSON response to POJO
	public void populate(final PartnerDataJSON jsonResponse)
	{
		setOrgName(jsonResponse.orgName);
		setAddress(jsonResponse.addressLine1, jsonResponse.addressLine2, jsonResponse.city, jsonResponse.state,
				jsonResponse.zipCode);
		setId(jsonResponse.partnerID);
		setContact(jsonResponse.firstName, jsonResponse.lastName, jsonResponse.jobTitle, jsonResponse.phoneNumber,
				jsonResponse.phoneExt, jsonResponse.email);
	}

	public void setOrgName(final String orgName)
	{
		this.orgName = orgName;
	}

	abstract public OrgSummary createCopy();




}
