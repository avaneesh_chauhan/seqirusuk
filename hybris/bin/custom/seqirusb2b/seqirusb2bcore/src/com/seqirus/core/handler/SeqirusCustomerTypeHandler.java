package com.seqirus.core.handler;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;


/**
 * Sets the customer type based on address type sample address types are
 * address.is.shipping/address.is.billing/address.is.contact
 */
public class SeqirusCustomerTypeHandler extends AbstractDynamicAttributeHandler<String, AddressModel>
{


	@Override
	public String get(final AddressModel model)
	{
		final StringBuilder customerType = new StringBuilder();
		if (null == model)
		{
			return null;
		}

		if (null == model.getTypeQualifier())
		{
			return null;
		}

		if (model.getTypeQualifier().contains("shipping"))
		{
			customerType.append(" Ship To");
		}

		if (model.getTypeQualifier().contains("billing"))
		{
			if (!customerType.toString().isEmpty())
			{
				customerType.append(" / ");
			}
			customerType.append("Bill To");
		}

		if (model.getTypeQualifier().contains("contact"))
		{
			if (!customerType.toString().isEmpty())
			{
				customerType.append(" / ");
			}
			customerType.append("Sold To");
		}
		if (model.getTypeQualifier().contains("payTo"))
		{
			if (!customerType.toString().isEmpty())
			{
				customerType.append(" / ");
			}
			customerType.append("Pay To");
		}

		return customerType.toString();
	}

	/**
	 * Ignore setting the value to the model
	 */
	@Override
	public void set(final AddressModel model, final String value)
	{
		//ignore the data, this field is only used exporting customer data .
	}

}
