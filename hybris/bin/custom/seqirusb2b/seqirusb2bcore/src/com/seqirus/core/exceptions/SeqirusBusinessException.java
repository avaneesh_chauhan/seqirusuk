
package com.seqirus.core.exceptions;

/**
 * @author 657703
 *
 */
public class SeqirusBusinessException extends Exception
{

	private static final String TYPE = "Validation Error";
	private static final String SUBJECT_TYPE = "Business";

	/**
	 * @param message
	 */
	public SeqirusBusinessException(final String message)
	{
		super(message);
	}


}
