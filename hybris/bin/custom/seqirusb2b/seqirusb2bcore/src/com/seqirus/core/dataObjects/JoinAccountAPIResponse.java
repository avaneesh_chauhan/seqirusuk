/**
 *
 */
package com.seqirus.core.dataObjects;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author 700196
 *
 */
public class JoinAccountAPIResponse
{
	@JsonProperty(value = "SoldTo")
	public JoinAccountAPILocation companyDetails;

	@JsonProperty(value = "BillTo")
	public JoinAccountAPILocation billTo;

	@JsonProperty(value = "Payer")
	public JoinAccountAPILocation payer;

	@JsonProperty(value = "ShipTo")
	public List<JoinAccountAPILocation> shipTo;

	/**
	 * @return the companyDetails
	 */
	public JoinAccountAPILocation getCompanyDetails()
	{
		return companyDetails;
	}

	/**
	 * @param companyDetails
	 *           the companyDetails to set
	 */
	public void setCompanyDetails(final JoinAccountAPILocation companyDetails)
	{
		this.companyDetails = companyDetails;
	}

	/**
	 * @return the billTo
	 */
	public JoinAccountAPILocation getBillTo()
	{
		return billTo;
	}

	/**
	 * @param billTo
	 *           the billTo to set
	 */
	public void setBillTo(final JoinAccountAPILocation billTo)
	{
		this.billTo = billTo;
	}

	/**
	 * @return the payer
	 */
	public JoinAccountAPILocation getPayer()
	{
		return payer;
	}

	/**
	 * @param payer
	 *           the payer to set
	 */
	public void setPayer(final JoinAccountAPILocation payer)
	{
		this.payer = payer;
	}

	/**
	 * @return the shipTo
	 */
	public List<JoinAccountAPILocation> getShipTo()
	{
		return shipTo;
	}

	/**
	 * @param shipTo
	 *           the shipTo to set
	 */
	public void setShipTo(final List<JoinAccountAPILocation> shipTo)
	{
		this.shipTo = shipTo;
	}


}
