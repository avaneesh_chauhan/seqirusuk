package com.seqirus.core.dataObjects;



public class ProductDetailJSON {

	public String materialID;
	public String itemNumber;
	public int quantity;
	public String reasonForRejection;
	public double netValue;
	public String poType;
	public String pOItemID;
	public String pONumber;
	public String shippingWave;
	public String promisedDate;
	@Override
	public String toString() {
		return "ProductDetailJSON [materialID=" + materialID + ", itemNumber=" + itemNumber + ", quantity=" + quantity
				+ ", reasonForRejection=" + reasonForRejection + ", netValue=" + netValue + ", poType=" + poType
				+ ", pOItemID=" + pOItemID + ",promisedDate=" + promisedDate + ",shippingWave=" + shippingWave + "]";
	}



}
