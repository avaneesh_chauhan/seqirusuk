/**
 *
 */
package com.seqirus.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;


/**
 * Forgotten password event, implementation of {@link AbstractEmployeeEvent}
 *
 * @author 172553
 *
 */
public class SeqirusEmployeePwdEvent extends AbstractEmployeeEvent<BaseSiteModel>
{
	private String token;

	/**
	 * Default constructor
	 */
	public SeqirusEmployeePwdEvent()
	{
		super();
	}

	/**
	 * Parameterized Constructor
	 *
	 * @param token
	 */
	public SeqirusEmployeePwdEvent(final String token)
	{
		super();
		this.token = token;
	}

	/**
	 * @return the token
	 */
	public String getToken()
	{
		return token;
	}

	/**
	 * @param token
	 *           the token to set
	 */
	public void setToken(final String token)
	{
		this.token = token;
	}
}
