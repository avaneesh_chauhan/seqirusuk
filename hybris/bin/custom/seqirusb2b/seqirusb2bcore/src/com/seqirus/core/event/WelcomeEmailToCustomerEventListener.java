/**
 *
 */
package com.seqirus.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author 700196
 *
 */
public class WelcomeEmailToCustomerEventListener extends AbstractAcceleratorSiteEventListener<WelcomeEmailToCustomerEvent>
{
	private ModelService modelService;
	private BusinessProcessService businessProcessService;


	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener#getSiteChannelForEvent(de.hybris.
	 * platform.servicelayer.event.events.AbstractEvent)
	 */
	@Override
	protected SiteChannel getSiteChannelForEvent(final WelcomeEmailToCustomerEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.commerceservices.event.AbstractSiteEventListener#onSiteEvent(de.hybris.platform.servicelayer.
	 * event.events.AbstractEvent)
	 */
	@Override
	protected void onSiteEvent(final WelcomeEmailToCustomerEvent event)
	{

		final StoreFrontCustomerProcessModel notificationProcessModel = (StoreFrontCustomerProcessModel) getBusinessProcessService()
				.createProcess(
						"welcomeEmailToCustomerProcess-" + event.getCustomer().getUid() + "-" + System.currentTimeMillis(),
						"welcomeEmailToCustomerProcess");

		notificationProcessModel.setCustomer(event.getCustomer());
		notificationProcessModel.setSite(event.getSite());
		notificationProcessModel.setStore(event.getBaseStore());
		notificationProcessModel.setLanguage(event.getLanguage());
		notificationProcessModel.setCurrency(event.getCurrency());

		getModelService().save(notificationProcessModel);
		getBusinessProcessService().startProcess(notificationProcessModel);
	}
}
