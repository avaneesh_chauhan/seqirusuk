package com.seqirus.core.dataObjects;

import java.util.List;

public class ShiptoDataJSON extends PartnerDataJSON 
{

	public LicenseDataJSON license;
	
	public List<ContactDataJSON> shippingContact;
	
	public void setNonDeliveryPreference(PreferenceJSON json) {
		preference=json;
	}

	@Override
	public String toString() {
		return "ShiptoDataJSON [preference=" + preference + ", license=" + license + ", shippingContact="
				+ shippingContact + ", dunns=" + dunns + ", loginEmail=" + loginEmail + ", city=" + city + ", zipCode="
				+ zipCode + ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", state=" + state
				+ ", orgName=" + orgName + ", partnerID=" + partnerID + ", firstName=" + firstName + ", lastName="
				+ lastName + ", jobTitle=" + jobTitle + ", email=" + email + ", phoneNumber=" + phoneNumber
				+ ", phoneExt=" + phoneExt + "]";
	}

	public void setShippingContact(List<ContactDataJSON> shippingContact) {
		this.shippingContact = shippingContact;
	}

	
}