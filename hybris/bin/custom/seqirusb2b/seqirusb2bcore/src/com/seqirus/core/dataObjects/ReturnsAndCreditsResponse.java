package com.seqirus.core.dataObjects;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The Class ReturnsAndCreditsResponse.
 *
 * @author Avaneesh Chauhan
 */
public class ReturnsAndCreditsResponse {

	/** The invoices. */
	@JsonProperty(value = "invoices")
	public List<ReturnsAndCreditsListResponse> invoices;

	/**
	 * Gets the invoices.
	 *
	 * @return the invoices
	 */
	public List<ReturnsAndCreditsListResponse> getInvoices()
	{
		return invoices;
	}

	/**
	 * Sets the invoices.
	 *
	 * @param invoices
	 *           the new invoices
	 */
	public void setInvoices(final List<ReturnsAndCreditsListResponse> invoices)
	{
		this.invoices = invoices;
	}
}
