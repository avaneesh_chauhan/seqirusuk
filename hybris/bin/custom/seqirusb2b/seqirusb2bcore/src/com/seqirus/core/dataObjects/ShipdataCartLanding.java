/**
 *
 */
package com.seqirus.core.dataObjects;

public class ShipdataCartLanding 
{
	
	
	public String locname;
	public String address;
	public String  locID;
	public String checked;
	public String state;
	/**
	 * @return the locname
	 */
	public String getLocname()
	{
		return locname;
	}
	/**
	 * @param locname the locname to set
	 */
	public void setLocname(String locname)
	{
		this.locname = locname;
	}
	/**
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address)
	{
		this.address = address;
	}
	/**
	 * @return the locID
	 */
	public String getLocID()
	{
		return locID;
	}
	/**
	 * @param locID the locID to set
	 */
	public void setLocID(String locID)
	{
		this.locID = locID;
	}
	/**
	 * @return the checked
	 */
	public String getChecked()
	{
		return checked;
	}
	/**
	 * @param checked the checked to set
	 */
	public void setChecked(String checked)
	{
		this.checked = checked;
	}
	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}
	
	
	
	
	
	
	

}
