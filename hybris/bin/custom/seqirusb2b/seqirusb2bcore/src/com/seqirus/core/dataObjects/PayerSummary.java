/**
 *
 */
package com.seqirus.core.dataObjects;

import com.seqirus.core.dataObjects.PartnerDataJSON;


/**
 * @author 132793
 *
 */
public class PayerSummary extends OrgSummary
{

	String dunsNumber;

	public String getDunsNumber()
	{
		return dunsNumber;
	}

	@Override
	public String toString()
	{
		return "PayerSummary [dunsNumber=" + dunsNumber + ", contact=" + contact + ", orgName=" + orgName + ", address=" + address
				+ "]";
	}

	@Override
	public void populate(final PartnerDataJSON jsonResponse)
	{
		this.dunsNumber = jsonResponse.dunns;
		super.populate(jsonResponse);
	}

	@Override
	public OrgSummary createCopy()
	{
		return new PayerSummary();
	}


}
