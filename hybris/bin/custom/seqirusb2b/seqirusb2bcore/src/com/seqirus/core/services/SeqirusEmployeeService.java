/**
 *
 */
package com.seqirus.core.services;

import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.core.model.user.EmployeeModel;


/**
 * @author 172553
 *
 */
public interface SeqirusEmployeeService
{
	/**
	 * Sends a forgotten password event
	 *
	 * @param EmployeeModel
	 *           the user
	 */
	void forgottenPassword(EmployeeModel employeeModel);


	/**
	 * Update the password for the user by decrypting and validating the token.
	 *
	 * @param token
	 *           the password reset token
	 * @param newPassword
	 *           the new plain text password
	 * @throws IllegalArgumentException
	 *            If the new password is empty or the token is invalid or expired
	 * @throws TokenInvalidatedException
	 *            if the token was already used or there is a newer token
	 */
	void updatePassword(final String token, final String newPassword) throws TokenInvalidatedException;


	/**
	 * Returns Employee for a given email id
	 *
	 * @param id
	 * @return EmployeeModel
	 */
	EmployeeModel getEmployee(String id);
}

