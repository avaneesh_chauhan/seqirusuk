
package com.seqirus.core.dataObjects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Holds data of a customer order
 *
 *
 */
public class CustomerOrder implements Comparable
{
	private String customerId;
	private String sapOrderId;
	private String hybrisOrderId;
	private String season;
	private String poNumber;
	private Date orderDate;
	private Date estimatedDeliveryDate;
	private float orderTotal;
	private String orderStatus;
	private Partner billTo;
	private Partner shipTo;
	private Partner payTo;
	private Partner soldTo;
	private final List<OrderItems> items;
	private boolean contract;

	@Override
	public String toString()
	{
		return "CustomerOrder [customerId=" + customerId + ", sapOrderId=" + sapOrderId + ", hybrisOrderId=" + hybrisOrderId
				+ ", season=" + season + ", poNumber=" + poNumber + ", orderDate=" + orderDate + ", estimatedDeliveryDate="
				+ estimatedDeliveryDate + ", orderTotal=" + orderTotal + ", orderStatus=" + orderStatus + ", billTo=" + billTo
				+ ", shipTo=" + shipTo + ", payTo=" + payTo + ", soldTo=" + soldTo + ", items=" + items + "]";
	}

	public CustomerOrder()
	{
		super();
		items = new ArrayList<OrderItems>();
	}

	public String getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(final String customerId)
	{
		this.customerId = customerId;
	}

	public String getSapOrderId()
	{
		return sapOrderId;
	}

	public void setSapOrderId(final String sapOrderId)
	{
		this.sapOrderId = sapOrderId;
	}

	public String getHybrisOrderId()
	{
		return hybrisOrderId;
	}

	public void setHybrisOrderId(final String hybrisOrderId)
	{
		this.hybrisOrderId = hybrisOrderId;
	}

	public String getSeason()
	{
		return season;
	}

	public void setSeason(final String season)
	{
		this.season = season;
	}

	public String getPoNumber()
	{
		return poNumber;
	}

	public void setPoNumber(final String poNumber)
	{
		this.poNumber = poNumber;
	}

	public Date getOrderDate()
	{
		return orderDate;
	}

	public void setOrderDate(final Date orderDate)
	{
		this.orderDate = orderDate;
	}

	public Date getEstimatedDeliveryDate()
	{
		return estimatedDeliveryDate;
	}

	public void setEstimatedDeliveryDate(final Date estimatedDeliveryDate)
	{
		this.estimatedDeliveryDate = estimatedDeliveryDate;
	}

	public float getOrderTotal()
	{
		return orderTotal;
	}

	public void setOrderTotal(final float orderTotal)
	{
		this.orderTotal = orderTotal;
	}

	public String getOrderStatus()
	{
		return orderStatus;
	}

	public void setOrderStatus(final String orderStatus)
	{
		this.orderStatus = orderStatus;
	}

	public Partner getBillTo()
	{
		return billTo;
	}

	public void setBillTo(final Partner billTo)
	{
		this.billTo = billTo;
	}

	public Partner getShipTo()
	{
		return shipTo;
	}

	public void setShipTo(final Partner shipTo)
	{
		this.shipTo = shipTo;
	}

	public Partner getPayTo()
	{
		return payTo;
	}

	public void setPayTo(final Partner payTo)
	{
		this.payTo = payTo;
	}

	public Partner getSoldTo()
	{
		return soldTo;
	}

	public void setSoldTo(final Partner soldTo)
	{
		this.soldTo = soldTo;
	}

	public List<OrderItems> getItems()
	{
		return items;
	}

	public void addItems(final OrderItems item)
	{
		items.add(item);
	}

	@Override
	public int compareTo(final Object obj)
	{
		return (this.orderDate.getTime()) < ((CustomerOrder) (obj)).orderDate.getTime() ? 1 : -1;

	}



	/**
	 * @return the contract
	 */
	public boolean isContract()
	{
		return contract;
	}


	/**
	 * @param contract
	 *           the contract to set
	 */
	public void setContract(final boolean contract)
	{
		this.contract = contract;
	}


	public String getFirstMaterialId()
	{
		return items.isEmpty() ? null : items.get(0).materialID;
	}




}
