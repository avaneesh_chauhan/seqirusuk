package com.seqirus.core.dataObjects;

import java.util.Date;

public class ProductDetailAllItemNumber
{

	private String materialID;
	private String itemNumber;
	private int quantity;
	private String type;
	private int itemQty;
	private String ReasonForRejection;
	private String shippingWave;
	Date promisedDate;

	public ProductDetailAllItemNumber(){


	}

	public ProductDetailAllItemNumber(final String materialID, final String itemNumber, final int qty, final String ReasonForRejection)
	{
		super();
		this.materialID = materialID;
		this.itemNumber = itemNumber;
		this.quantity = qty;
		this.ReasonForRejection = ReasonForRejection;
		type = "Increment";

	}

	public ProductDetailAllItemNumber(final String materialID, final String itemNumber, final int qty, final String shippingWave, final Date promisedDate )
	{
		super();
		this.materialID = materialID;
		this.itemNumber = itemNumber;
		this.itemQty = qty;
		this.shippingWave = shippingWave;
		this.promisedDate= promisedDate;
		type = "Increment";

	}


	/**
	 * @return the ReasonForRejection
	 */
	public String getReasonForRejection()
	{
		return ReasonForRejection;
	}

	/**
	 * @param ReasonForRejection the ReasonForRejection to set
	 */
	public void setReasonForRejection(final String ReasonForRejection)
	{
		this.ReasonForRejection = ReasonForRejection;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(final String type)
	{
		this.type = type;
	}

	public void setModified(final boolean modified)
	{
		this.type = modified == true ? "New" : "Increment";
	}

	public String getMaterialID()
	{
		return materialID;
	}

	public String getItemNumber()
	{
		return itemNumber;
	}

	public String getType()
	{
		return type;
	}

	public int getQuantity()
	{
		return quantity;
	}

	public void setMaterialID(final String materialID)
	{
		this.materialID = materialID;
	}

	public void setItemNumber(final String itemNumber)
	{
		this.itemNumber = itemNumber;
	}


	public int getItemQty()
	{
		return itemQty;
	}

	public void setItemQty(final int itemQty)
	{
		this.itemQty = itemQty;
	}

	/**
	 * @return the shippingWave
	 */
	public String getShippingWave()
	{
		return shippingWave;
	}

	/**
	 * @param shippingWave the shippingWave to set
	 */
	public void setShippingWave(final String shippingWave)
	{
		this.shippingWave = shippingWave;
	}

	/**
	 * @return the promisedDate
	 */
	public Date getPromisedDate()
	{
		return promisedDate;
	}

	/**
	 * @param promisedDate the promisedDate to set
	 */
	public void setPromisedDate(final Date promisedDate)
	{
		this.promisedDate = promisedDate;
	}

	public void setQuantity(final int quantity)
	{
		this.quantity = quantity;
	}


	@Override
	public String toString()
	{
		return "ProductDetail [materialID=" + materialID + ",shippingWave=" + shippingWave + ", promisedDate=" + promisedDate + ", itemNumber=" + itemNumber + ", quantity=" + quantity + ",ReasonForRejection="+ ReasonForRejection +", type="
				+ type + ", itemQty=" + itemQty + "]\n";
	}


}
