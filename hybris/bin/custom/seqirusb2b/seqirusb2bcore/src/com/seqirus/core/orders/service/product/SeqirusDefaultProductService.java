/**
 *
 */
package com.seqirus.core.orders.service.product;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.ProductDao;
import de.hybris.platform.product.impl.DefaultProductService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author 614269
 *
 */
public class SeqirusDefaultProductService extends DefaultProductService implements SeqirusProductService
{
	@Autowired
	private ProductDao productDao;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	private static final String CATALOG_CODE = "seqirusb2b-ukProductCatalog";
	private static final String CATALOG_ONLINE_VERSION = "Online";

	@Override
	public ProductModel getProductDataForCode(final String code)
	{
		ProductModel productModel = null;
		final CatalogVersionModel onlineProductCatalog = catalogVersionService.getCatalogVersion(CATALOG_CODE,
				CATALOG_ONLINE_VERSION);
		final List<ProductModel> products = productDao.findProductsByCode(onlineProductCatalog, code);
		if (CollectionUtils.isNotEmpty(products))
		{
			productModel = products.get(0);
		}
		return productModel;
	}

}
