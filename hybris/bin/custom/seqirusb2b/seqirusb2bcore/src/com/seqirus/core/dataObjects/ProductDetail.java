package com.seqirus.core.dataObjects;

public class ProductDetail
{

	private String materialID;
	private String itemNumber;
	private int quantity;
	private String type;
	private int itemQty;
	private String ReasonForRejection;

	public ProductDetail(){


	}

	public ProductDetail(final String materialID, final String itemNumber, final int qty, final String ReasonForRejection)
	{
		super();
		this.materialID = materialID;
		this.itemNumber = itemNumber;
		this.quantity = qty;
		this.ReasonForRejection = ReasonForRejection;
		type = "Increment";

	}

	public ProductDetail(final String materialID, final String itemNumber, final int qty, final String ReasonForRejection, final String Type)
	{
		super();
		this.materialID = materialID;
		this.itemNumber = itemNumber;
		this.quantity = qty;
		this.ReasonForRejection = ReasonForRejection;
		this.type = Type;

	}

	public ProductDetail(final String materialID, final String itemNumber, final int qty)
	{
		super();
		this.materialID = materialID;
		this.itemNumber = itemNumber;
		this.quantity = qty;
		type = "Increment";

	}


	/**
	 * @return the ReasonForRejection
	 */
	public String getReasonForRejection()
	{
		return ReasonForRejection;
	}

	/**
	 * @param ReasonForRejection the ReasonForRejection to set
	 */
	public void setReasonForRejection(final String ReasonForRejection)
	{
		this.ReasonForRejection = ReasonForRejection;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(final String type)
	{
		this.type = type;
	}

	public void setModified(final boolean modified)
	{
		this.type = modified == true ? "New" : "Increment";
	}

	public String getMaterialID()
	{
		return materialID;
	}

	public String getItemNumber()
	{
		return itemNumber;
	}

	public String getType()
	{
		return type;
	}

	public int getQuantity()
	{
		return quantity;
	}

	public void setMaterialID(final String materialID)
	{
		this.materialID = materialID;
	}

	public void setItemNumber(final String itemNumber)
	{
		this.itemNumber = itemNumber;
	}


	public int getItemQty()
	{
		return itemQty;
	}

	public void setItemQty(final int itemQty)
	{
		this.itemQty = itemQty;
	}

	public void setQuantity(final int quantity)
	{
		this.quantity = quantity;
	}


	@Override
	public String toString()
	{
		return "ProductDetail [materialID=" + materialID + ", itemNumber=" + itemNumber + ", quantity=" + quantity + ",ReasonForRejection="+ ReasonForRejection +", type="
				+ type + ", itemQty=" + itemQty + "]\n";
	}


}
