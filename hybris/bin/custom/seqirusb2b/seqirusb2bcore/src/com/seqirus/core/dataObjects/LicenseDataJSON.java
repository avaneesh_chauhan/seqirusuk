package com.seqirus.core.dataObjects;

public class LicenseDataJSON extends CommonDataJSON {
	
	public String  licenseNumber;
	public String  name;
	public String  stateofIssueLicense;
	public String	expirationDate;
	@Override
	public String toString() {
		return "LicenseLookupData [licenseNumber=" + licenseNumber + ", name=" + name + ", stateofIssuseLicense="
				+ stateofIssueLicense + ", expirationDate=" + expirationDate + ", city=" + city + ", zipCode="
				+ zipCode + ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", state=" + state
				+ "]";
	}
	
	
}