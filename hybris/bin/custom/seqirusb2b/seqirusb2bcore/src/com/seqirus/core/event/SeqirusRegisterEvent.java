/**
 *
 */
package com.seqirus.core.event;

import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.core.model.user.EmployeeModel;


/**
 * @author 172553
 *
 */
public class SeqirusRegisterEvent extends RegisterEvent
{
	private EmployeeModel employee;

	/**
	 * @return the employee
	 */
	public EmployeeModel getEmployee()
	{
		return employee;
	}

	/**
	 * @param employee
	 *           the employee to set
	 */
	public void setEmployee(final EmployeeModel employee)
	{
		this.employee = employee;
	}


}
