/**
 *
 */
package com.seqirus.core.orders.service.product;

import de.hybris.platform.core.model.product.ProductModel;

/**
 * @author 614269
 *
 */
public interface SeqirusProductService
{

	ProductModel getProductDataForCode(final String code);

}
