package com.seqirus.core.dataObjects;

import java.util.Map;

public class PriceRequestParameter extends RequestParameters {

	public PriceRequestParameter(final Map<String, String> requestParam, final String requestURL, final String fileName,
			final String requestMethod, final String certPath, final String responseType)
	{
		super(requestParam, requestURL, fileName, responseType);
		this.requestMethod = requestMethod;
		this.certPath = certPath;
	}

	@Override
	public boolean isParseStrategyApplicable() {
		// TODO Auto-generated method stub
		return false;
	}


}
