/**
 *
 */
package com.seqirus.core.registration.dao.impl;

import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.seqirus.core.registration.dao.SeqirusEmployeeDao;

/**
 * @author 172553
 *
 */
public class SeqirusEmployeeDaoImpl implements SeqirusEmployeeDao
{
	@Autowired
	FlexibleSearchService flexibleSearchService;

	private static final String GET_EMPLOYEE_QUERY = "SELECT {e.pk} FROM {Employee as e} where {uid} = ?id";

	@Override
	public EmployeeModel findEmployeeById(final String id)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(GET_EMPLOYEE_QUERY);
		fQuery.addQueryParameter("id", id);
		final SearchResult<EmployeeModel> resultList = flexibleSearchService.search(fQuery);
		if (null != resultList)
		{
			final List<EmployeeModel> result = resultList.getResult();
			if (null != result && !result.isEmpty())
			{
				return result.get(0);
			}
		}
		return null;
	}
}
