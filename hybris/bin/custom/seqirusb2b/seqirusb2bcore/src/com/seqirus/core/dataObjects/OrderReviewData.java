/**
 *
 */
package com.seqirus.core.dataObjects;
import java.util.ArrayList;
import java.util.List;
import com.seqirus.core.dataObjects.ShipdataCartLanding;
import com.seqirus.core.dataObjects.ShipLoctionQty;

public class OrderReviewData 
{

	

	/**
	 * @return the productCode
	 */
	public long getProductCode()
	{
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(long productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * @return the shipLocationQty
	 */
	public List<ShipLoctionQty> getShipLocationQty()
	{
		return shipLocationQty;
	}

	/**
	 * @param shipLocationQty the shipLocationQty to set
	 */
	public void setShipLocationQty(List<ShipLoctionQty> shipLocationQty)
	{
		this.shipLocationQty = shipLocationQty;
	}

	public long productCode ;
	
	List<ShipLoctionQty> shipLocationQty = new ArrayList<>(); 
 }
