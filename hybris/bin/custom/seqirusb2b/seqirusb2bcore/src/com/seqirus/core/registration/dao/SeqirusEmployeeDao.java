/**
 *
 */
package com.seqirus.core.registration.dao;

import de.hybris.platform.core.model.user.EmployeeModel;

/**
 * @author 172553
 *
 */
public interface SeqirusEmployeeDao
{
	/**
	 * Find employee for a given email id
	 *
	 * @param id
	 * @return
	 */
	EmployeeModel findEmployeeById(String id);
}
