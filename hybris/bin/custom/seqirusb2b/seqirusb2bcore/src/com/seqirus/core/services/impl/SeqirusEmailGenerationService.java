/**
 *
 */
package com.seqirus.core.services.impl;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailGenerationService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * Customized Email Generation Service Class
 *
 * @author 172553
 *
 */
public class SeqirusEmailGenerationService extends DefaultEmailGenerationService
{

	private static final Logger LOGGER = Logger.getLogger(SeqirusEmailGenerationService.class);

	private static final String SEPERATOR = "@";

	private ModelService modelService;

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 *
	 * @param businessProcessModel
	 * @param emailPageModel
	 * @return
	 */
	@Override
	public EmailMessageModel generate(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		//Let's first call OOTB generate method to generate emailMessageModel
		final EmailMessageModel emailMessageModel = super.generate(businessProcessModel, emailPageModel);

		// Now, we retrieve values of fields from businessProcessModel and set them to the emailMessageModel
		final List<EmailAddressModel> ccAddresses = createEmailAddressForEmail(businessProcessModel.getCcAddresses());
		final List<EmailAddressModel> bccAddresses = createEmailAddressForEmail(businessProcessModel.getBccAddresses());

		//Setting CC Email IDs
		if (CollectionUtils.isNotEmpty(ccAddresses))
		{
			emailMessageModel.setCcAddresses(ccAddresses);
		}
		//Setting BCC Email IDs
		if (CollectionUtils.isNotEmpty(bccAddresses))
		{
			emailMessageModel.setBccAddresses(bccAddresses);
		}
		try
		{
			getModelService().save(emailMessageModel);
		}
		catch (final Exception e)
		{
			LOGGER.error("Exception while saving emailMessageModel with CC and BCC" + e.getMessage());
		}
		return emailMessageModel;
	}

	/**
	 * Method to prepare the list of CC and BCC emails
	 *
	 * @param emails
	 * @return
	 */
	private List<EmailAddressModel> createEmailAddressForEmail(final Collection<String> emails)
	{
		final List<EmailAddressModel> emailAddressModelList = new ArrayList<EmailAddressModel>();
		if (CollectionUtils.isNotEmpty(emails))
		{
			for (final String email : emails)
			{
				LOGGER.info("Sending copy or blind copy to : " + email);
				if (StringUtils.isNotEmpty(email))
				{
					final EmailAddressModel emailAddressModel = getEmailService().getOrCreateEmailAddressForEmail(email,
							email.split(SEPERATOR)[0]);
					emailAddressModelList.add(emailAddressModel);
				}
			}
		}
		return emailAddressModelList;
	}

}
