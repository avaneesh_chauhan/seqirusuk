/**
 *
 */
package com.seqirus.core.dataObjects;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author 700196
 *
 */
public class InvoiceDetailsResponse
{
	@JsonProperty(value = "invoiceNumber")
	public String invoiceNumber;
	@JsonProperty(value = "invoiceType")
	public String invoiceType;
	@JsonProperty(value = "invoiceDate")
	public String invoiceDate;
	@JsonProperty(value = "clearingDate")
	public String clearingDate;
	@JsonProperty(value = "paymentDueDate")
	public String paymentDueDate;
	@JsonProperty(value = "amountWithTax")
	public float amountWithTax;
	@JsonProperty(value = "orderNumber")
	public String orderNumber;
	@JsonProperty(value = "salesOrderNumber")
	public String salesOrderNumber;
	@JsonProperty(value = "deliveryNumber")
	public String deliveryNumber;
	@JsonProperty(value = "paymentTerm")
	public String paymentTerm;
	@JsonProperty(value = "paymentTermDescription")
	public String paymentTermDescription;
	@JsonProperty(value = "poNumber")
	public String poNumber;
	@JsonProperty(value = "poDate")
	public String poDate;
	@JsonProperty(value = "shipTo")
	public InvoiceDetailsShipToResponse shipTo;
	@JsonProperty(value = "soldTo")
	public InvoiceDetailsSoldToResponse soldTo;
	@JsonProperty(value = "lineItems")
	public List<InvoiceDetailsLineItemsResponse> lineItems;
	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}
	/**
	 * @param invoiceNumber the invoiceNumber to set
	 */
	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}
	/**
	 * @return the invoiceType
	 */
	public String getInvoiceType()
	{
		return invoiceType;
	}
	/**
	 * @param invoiceType the invoiceType to set
	 */
	public void setInvoiceType(final String invoiceType)
	{
		this.invoiceType = invoiceType;
	}
	/**
	 * @return the invoiceDate
	 */
	public String getInvoiceDate()
	{
		return invoiceDate;
	}
	/**
	 * @param invoiceDate the invoiceDate to set
	 */
	public void setInvoiceDate(final String invoiceDate)
	{
		this.invoiceDate = invoiceDate;
	}
	/**
	 * @return the clearingDate
	 */
	public String getClearingDate()
	{
		return clearingDate;
	}
	/**
	 * @param clearingDate the clearingDate to set
	 */
	public void setClearingDate(final String clearingDate)
	{
		this.clearingDate = clearingDate;
	}
	/**
	 * @return the paymentDueDate
	 */
	public String getPaymentDueDate()
	{
		return paymentDueDate;
	}
	/**
	 * @param paymentDueDate the paymentDueDate to set
	 */
	public void setPaymentDueDate(final String paymentDueDate)
	{
		this.paymentDueDate = paymentDueDate;
	}
	/**
	 * @return the amountWithTax
	 */
	public float getAmountWithTax()
	{
		return amountWithTax;
	}
	/**
	 * @param amountWithTax the amountWithTax to set
	 */
	public void setAmountWithTax(final float amountWithTax)
	{
		this.amountWithTax = amountWithTax;
	}
	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber()
	{
		return orderNumber;
	}
	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(final String orderNumber)
	{
		this.orderNumber = orderNumber;
	}
	/**
	 * @return the salesOrderNumber
	 */
	public String getSalesOrderNumber()
	{
		return salesOrderNumber;
	}
	/**
	 * @param salesOrderNumber the salesOrderNumber to set
	 */
	public void setSalesOrderNumber(final String salesOrderNumber)
	{
		this.salesOrderNumber = salesOrderNumber;
	}
	/**
	 * @return the deliveryNumber
	 */
	public String getDeliveryNumber()
	{
		return deliveryNumber;
	}
	/**
	 * @param deliveryNumber the deliveryNumber to set
	 */
	public void setDeliveryNumber(final String deliveryNumber)
	{
		this.deliveryNumber = deliveryNumber;
	}
	/**
	 * @return the paymentTerm
	 */
	public String getPaymentTerm()
	{
		return paymentTerm;
	}
	/**
	 * @param paymentTerm the paymentTerm to set
	 */
	public void setPaymentTerm(final String paymentTerm)
	{
		this.paymentTerm = paymentTerm;
	}
	/**
	 * @return the paymentTermDescription
	 */
	public String getPaymentTermDescription()
	{
		return paymentTermDescription;
	}
	/**
	 * @param paymentTermDescription the paymentTermDescription to set
	 */
	public void setPaymentTermDescription(final String paymentTermDescription)
	{
		this.paymentTermDescription = paymentTermDescription;
	}
	/**
	 * @return the poNumber
	 */
	public String getPoNumber()
	{
		return poNumber;
	}
	/**
	 * @param poNumber the poNumber to set
	 */
	public void setPoNumber(final String poNumber)
	{
		this.poNumber = poNumber;
	}
	/**
	 * @return the poDate
	 */
	public String getPoDate()
	{
		return poDate;
	}
	/**
	 * @param poDate the poDate to set
	 */
	public void setPoDate(final String poDate)
	{
		this.poDate = poDate;
	}
	/**
	 * @return the shipTo
	 */
	public InvoiceDetailsShipToResponse getShipTo()
	{
		return shipTo;
	}
	/**
	 * @param shipTo the shipTo to set
	 */
	public void setShipTo(final InvoiceDetailsShipToResponse shipTo)
	{
		this.shipTo = shipTo;
	}
	/**
	 * @return the billTo
	 */
	public InvoiceDetailsSoldToResponse getSoldTo()
	{
		return soldTo;
	}
	/**
	 * @param billTo the billTo to set
	 */
	public void setSoldTo(final InvoiceDetailsSoldToResponse soldTo)
	{
		this.soldTo = soldTo;
	}
	/**
	 * @return the lineItems
	 */
	public List<InvoiceDetailsLineItemsResponse> getLineItems()
	{
		return lineItems;
	}
	/**
	 * @param lineItems the lineItems to set
	 */
	public void setLineItems(final List<InvoiceDetailsLineItemsResponse> lineItems)
	{
		this.lineItems = lineItems;
	}


}
