package com.seqirus.core.dataObjects;

import java.util.List;

public class CustomerDataJSON {
	
	public List<ShiptoDataJSON> shipto;
	public PartnerDataJSON billto;
	public PartnerDataJSON payer;
	public PartnerDataJSON soldto;
	public PreferenceJSON preference;
	
	public String errorStatus="";
	public String errorMessage="";
	public void setShipTo(List<ShiptoDataJSON> args) {
		shipto=args;
	}
	public void setBillTo(PartnerDataJSON args) {
		billto=args;
	}
	public void setPayer(PartnerDataJSON args) {
		payer=args;
	}
	public void setSoldTo(PartnerDataJSON args) {
		soldto=args;
	}
	
	public void setWebsitePreferences(PreferenceJSON args) {
		preference=args;
	}

	
	@Override
	public String toString() {
		return "CustomerDataJSON [shipto=" + shipto + "\n, billto=" + billto + "\n, payer=" + payer + ",\n soldto=" + soldto
				+ "]";
	}
	
	

}
