/**
 *Holds the SoldTo data of customer
 */
package com.seqirus.core.dataObjects;

import com.seqirus.core.dataObjects.PartnerDataJSON;


/**
 * Stores the SoldTO details
 *
 */
public class SoldToSummary extends OrgSummary
{
	String loginEmail;
	boolean receiveUpdates;

	public String getLoginEmail()
	{
		return loginEmail;
	}

	public boolean isReceiveUpdates()
	{
		return receiveUpdates;
	}


	@Override
	public OrgSummary createCopy()
	{
		return new SoldToSummary();
	}

	//Maps the JSON response to POJO
	@Override
	public void populate(final PartnerDataJSON jsonResponse)
	{
		if (jsonResponse.loginEmail != null)
		{
			jsonResponse.loginEmail = jsonResponse.loginEmail.toLowerCase();
		}
		this.loginEmail = jsonResponse.loginEmail;
		final String receiveUpdates = jsonResponse.preference == null ? "N" : jsonResponse.preference.anyUpdates;
		this.receiveUpdates = "Y".equalsIgnoreCase(receiveUpdates) ? true : false;
		//jsonResponse.email = jsonResponse.loginEmail;
		super.populate(jsonResponse);
	}

}
