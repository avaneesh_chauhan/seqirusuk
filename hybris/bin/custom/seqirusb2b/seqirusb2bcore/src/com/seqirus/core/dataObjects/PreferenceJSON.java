/**
 *
 */
package com.seqirus.core.dataObjects;

import java.util.HashMap;
import java.util.Map;


public class PreferenceJSON
{

	public String anyUpdates;
	public String marketingPreference;
	public String promotionalPreference;
	public String receiveInvoices;
	public String receiveStatements;
	public String statementEmail;
	public String invoiceEmail;
	public NonDeliveryDays nonDeliveryDays;
	public String nonDeliveryComments;



	public Map<String, Boolean> getNonDeliveryDays()
	{
		return nonDeliveryDays == null ? new HashMap() : nonDeliveryDays.nonDelivery;
	}

	@Override
	public String toString()
	{
		return "WebsitePreferenceJSON [anyUpdates=" + anyUpdates + ", marketingPreference=" + marketingPreference
				+ ", promotionalPreference=" + promotionalPreference + ", receiveinvocies=" + receiveInvoices + ", receiveStatements="
				+ receiveStatements + ", statementEmail=" + statementEmail + ", invoiceEmail=" + invoiceEmail + ", nonDeliveryDays="
				+ getNonDeliveryDays() + ", nonDeliveryComments=" + nonDeliveryComments + "]";
	}


}

class NonDeliveryDays
{
	public static String[] WEEKDAYS =
	{ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };

	public static String STATUS_YES = "Y";

	Map<String, Boolean> nonDelivery = new HashMap<String, Boolean>();
	Boolean isNotDeliveryDay = new Boolean(true);
	Boolean isDeliveryDay = new Boolean(false);

	public void setMonday(final String s)
	{
		setDay(WEEKDAYS[0], s);
	}

	public void setTuesday(final String s)
	{
		setDay(WEEKDAYS[1], s);
	}

	public void setWednesday(final String s)
	{
		setDay(WEEKDAYS[2], s);
	}

	public void setThursday(final String s)
	{
		setDay(WEEKDAYS[3], s);
	}

	public void setFriday(final String s)
	{
		setDay(WEEKDAYS[4], s);
	}

	public void setDay(final String day, final String s)
	{
		if (STATUS_YES.equals(s))
		{
			nonDelivery.put(day, isNotDeliveryDay);
		}
		else
		{
			nonDelivery.put(day, isDeliveryDay);
		}
	}



}

