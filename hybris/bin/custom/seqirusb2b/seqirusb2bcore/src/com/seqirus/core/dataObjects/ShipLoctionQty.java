/**
 *
 */
package com.seqirus.core.dataObjects;
import java.util.List;
import com.seqirus.core.dataObjects.ShipdataCartLanding;

public class ShipLoctionQty 
{
	public String locID;
	/**
	 * @return the locID
	 */
	public String getLocID()
	{
		return locID;
	}
	/**
	 * @return the locQty
	 */
	public long getLocQty()
	{
		return locQty;
	}
	/**
	 * @param locQty the locQty to set
	 */
	public void setLocQty(long locQty)
	{
		this.locQty = locQty;
	}
	/**
	 * @param locID the locID to set
	 */
	public void setLocID(String locID)
	{
		this.locID = locID;
	}
	
	public long locQty;
	
}
