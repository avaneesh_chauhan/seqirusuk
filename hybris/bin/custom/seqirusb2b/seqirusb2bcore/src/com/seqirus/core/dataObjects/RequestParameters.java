package com.seqirus.core.dataObjects;
import java.util.Map;


public class RequestParameters
{

	private String requestURL;
	Map<String, String> requestParam;
	String pathPrefix;
	String fileName = "";
	String requestMethod;
	String certPath;
	String responseFormat = "JSON";
	String responseType;
	Class parsingClass;

	public RequestParameters(final Map<String, String> requestParam, final String requestURL, final String fileName,
			final String responseType)
	{
		super();

		this.requestParam = requestParam;
		this.requestURL = requestURL;
		this.fileName = fileName;
		this.responseType = responseType;
	}


	public RequestParameters(final Map<String, String> requestParam, final String requestURL, final String fileName,
			final String requestMethod, final String certPath, final String responseType)
	{
		this(requestParam, requestURL, fileName, responseType);
		this.requestMethod = requestMethod;
		this.certPath = certPath;

	}


	public String getRequestURL()
	{
		return requestURL;
	}

	public Map<String, String> getRequestParam()
	{
		return requestParam;
	}

	public String getPathPrefix()
	{
		return pathPrefix;
	}

	public String getFileName()
	{
		return fileName;
	}

	public String getRequestMethod()
	{
		return requestMethod;
	}

	public String getCertPath()
	{
		return certPath;
	}


	public String getResponseFormat()
	{
		return responseFormat;
	}


	public void setResponseFormat(final String responseFormat)
	{
		this.responseFormat = responseFormat;
	}

	public void setFileName(final String fileName)
	{
		this.fileName = fileName;
	}

	public void setCertPath(final String certPath)
	{
		this.certPath = certPath;
	}


	public void setRequestURL(final String requestURL)
	{
		this.requestURL = requestURL;
	}



	/**
	 * @return the responseType
	 */
	public String getResponseType()
	{
		return responseType;
	}


	/**
	 * @param responseType the responseType to set
	 */
	public void setResponseType(final String responseType)
	{
		this.responseType = responseType;
	}


	@Override
	public String toString()
	{
		return "RequestParameters [requestURL=" + requestURL + ", requestParam=" + requestParam + ", pathPrefix=" + pathPrefix
				+ ", fileName=" + fileName + ", requestMethod=" + requestMethod + ", certPath=" + certPath + ", responseFormat="
				+ responseFormat + "]";


	}


	public Class getParsingClass()
	{
		return parsingClass;
	}


	public void setParsingClass(final Class parsingClass)
	{
		this.parsingClass = parsingClass;
	}


	public boolean isParseStrategyApplicable() {
		return true;
	}

}
