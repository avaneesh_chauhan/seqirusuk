/**
 *
 */
package com.seqirus.core.dataObjects;

import com.seqirus.core.dataObjects.PartnerDataJSON;


/**
 * Holds billing summary of the customer
 *
 */
public class BillingSummary extends OrgSummary
{

	String invoiceEmail;
	String acctStmtEmail;

	public String getInvoiceEmail()
	{
		return invoiceEmail;
	}

	public String getAcctStmtEmail()
	{
		return acctStmtEmail;
	}

	@Override
	public String toString()
	{
		return "BillingSummary [invoiceEmail=" + invoiceEmail + ", acctStmtEmail=" + acctStmtEmail + ", contact=" + contact
				+ ", orgName=" + orgName + ", address=" + address + "]";
	}

	@Override
	public void populate(final PartnerDataJSON jsonResponse)
	{
		this.invoiceEmail = jsonResponse.preference == null ? "" : jsonResponse.preference.invoiceEmail;
		this.acctStmtEmail = jsonResponse.preference == null ? "" : jsonResponse.preference.statementEmail;
		super.populate(jsonResponse);
	}

	@Override
	public OrgSummary createCopy()
	{
		return new BillingSummary();
	}


}
