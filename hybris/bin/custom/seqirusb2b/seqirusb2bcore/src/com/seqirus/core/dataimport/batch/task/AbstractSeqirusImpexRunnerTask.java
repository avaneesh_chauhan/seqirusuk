package com.seqirus.core.dataimport.batch.task;


import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.AbstractImpexRunnerTask;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

/**
 * Abstract class for Run IMpex TASK which is executed by execute method and insert data from impex
 */
public abstract class AbstractSeqirusImpexRunnerTask extends AbstractImpexRunnerTask
{
	private static final Logger LOG = LogManager.getLogger(AbstractSeqirusImpexRunnerTask.class);
	private String timeStampFormat;

	@Autowired
	private SessionService sessionService;
	@Autowired
	private UserService userService;


	@Override
	public BatchHeader execute(final BatchHeader header) throws FileNotFoundException
	{

		Assert.notNull(header);
		Assert.notNull(header.getEncoding());
		if (CollectionUtils.isNotEmpty(header.getTransformedFiles()))
		{
			final boolean alreadyHasSession = getSessionService().hasCurrentSession();

			final Session localSession;
			if (!alreadyHasSession)
			{
				localSession = getSessionService().createNewSession();
			}
			else
			{
				localSession = null;
			}

			try
			{
				final BatchHeader finalHeader = header;
				for (final File file : finalHeader.getTransformedFiles())
				{
					sessionService.executeInLocalView(new SessionExecutionBody()
					{
						@Override
						public void executeWithoutResult()
						{
							// set the user as admin and process the file then (as an admin)
							userService.setCurrentUser(userService.getAdminUser());
							try
							{
								processFile(file, finalHeader.getEncoding());
							}
							catch (final FileNotFoundException e)
							{
								LOG.error(e.getMessage(), e);
							}
							super.executeWithoutResult();
						}
					});
				}
			}
			finally
			{
				if (!alreadyHasSession)
				{
					getSessionService().closeSession(localSession);
				}
			}
		}
		return header;
	}

	/**
	 * @method setTimeStampFormat
	 * @description set the timeStampFormat
	 * @param timeStampFormat
	 */
	public void setTimeStampFormat(final String timeStampFormat)
	{
		this.timeStampFormat = timeStampFormat;
	}

	/**
	 * @method getTimeStampFormat
	 * @description the timeStampForma
	 */
	protected String getTimeStampFormat()
	{
		return timeStampFormat;
	}

}
