/**
 *
 */
package com.seqirus.core.dataObjects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.seqirus.core.dataObjects.CustomerDataJSON;
import com.seqirus.core.dataObjects.ShiptoDataJSON;

/**
 * Holds overall customer summary including org,billing,payer and shipping details
 */
public class CustomerSummary
{

	SoldToSummary soldTO;
	BillingSummary billing;
	PayerSummary payer;
	List<ShippingSummary> shippingLocations;

	public BillingSummary getBilling()
	{
		return billing;
	}

	public PayerSummary getPayer()
	{
		return payer;
	}

	public List<ShippingSummary> getShippingLocations()
	{
		return shippingLocations;
	}

	public void setBilling(final BillingSummary billing)
	{
		this.billing = billing;
	}

	public void setPayer(final PayerSummary payer)
	{
		this.payer = payer;
	}

	public void addShippingLocations(final ShippingSummary shippingLocation)
	{
		if (this.shippingLocations == null)
		{
			shippingLocations = new ArrayList<ShippingSummary>();
		}
		shippingLocations.add(shippingLocation);
	}

	//Maps the JSON response to POJO
	public void populate(final CustomerDataJSON jsonResponse)
	{
		if (jsonResponse.soldto != null)
		{
			jsonResponse.soldto.preference = jsonResponse.preference;
			soldTO = new SoldToSummary();
			soldTO.populate(jsonResponse.soldto);

		}
		if (jsonResponse.billto != null)
		{
			jsonResponse.billto.preference = jsonResponse.preference;
			billing = new BillingSummary();
			billing.populate(jsonResponse.billto);
		}
		if (jsonResponse.payer != null)
		{
			payer = new PayerSummary();
			payer.populate(jsonResponse.payer);
		}
		if (jsonResponse.shipto != null)
		{
			final Iterator<ShiptoDataJSON> itr = jsonResponse.shipto.iterator();
			while (itr.hasNext())
			{
				final ShippingSummary shipping = new ShippingSummary();
				shipping.populate(itr.next());
				addShippingLocations(shipping);
			}
		}
	}

	public SoldToSummary getSoldTO()
	{
		return soldTO;
	}

	public void setSoldTO(final SoldToSummary soldTO)
	{
		this.soldTO = soldTO;
	}

	@Override
	public String toString()
	{
		return "CustomerSummary [soldTO=" + soldTO + ", billing=" + billing + ", payer=" + payer + ", shippingLocations="
				+ shippingLocations + "]";
	}



}
