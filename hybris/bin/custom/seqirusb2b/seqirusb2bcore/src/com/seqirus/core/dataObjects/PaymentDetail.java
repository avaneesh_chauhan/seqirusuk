package com.seqirus.core.dataObjects;

import java.util.Date;


/**
 *
 * POJO to hold payment details for an invoice
 *
 */

public class PaymentDetail
{

	private final String invoiceNumber;
	private final Date invoiceDate;
	private final float invTax;
	private final float invAmtIncTax; //Total amount without discount
	private float discountedAmt; //Total amount after discount
	private final String invCurrency;
	private float percentDiscount;
	private String status;//OPEN/OVERDUE etc.
	private Date discountExpiryDate; // discount expiry date
	private boolean discountNearExpiry = false;
	private boolean discountApplicable = false;
	private Date paidDate;
	private Date dueDate; // amount due date
	private boolean overCreditLimit = false;
	private boolean creditApplicable = false;

	public PaymentDetail(final String invoiceNumber, final Date invoiceDate,final float invTax,
			final String invCurrency,final float invAmtIncTax)
	{
		super();
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.invTax = invTax;
		this.invCurrency = invCurrency;
		this.invAmtIncTax=invAmtIncTax;
	}



	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	public Date getInvoiceDate()
	{
		return invoiceDate;
	}

	public float getInvTax()
	{
		return invTax;
	}
	public String getInvCurrency()
	{
		return invCurrency;
	}

	public float getPercentDiscount()
	{
		return percentDiscount;
	}

	public Date getDiscountExpiryDate()
	{
		return discountExpiryDate;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(final String status)
	{
		this.status = status;
	}

	public void setDiscountExpiryDate(final Date discountExpiryDate)
	{
		this.discountExpiryDate = discountExpiryDate;
	}

	public boolean isDiscountNearExpiry()
	{
		return discountNearExpiry;
	}



	public void setDiscountNearExpiry(final boolean discountNearExpiry)
	{
		this.discountNearExpiry = discountNearExpiry;
	}



	public boolean isDiscountApplicable()
	{
		return discountApplicable;
	}



	public void setDiscountApplicable(final boolean discountApplicable)
	{
		this.discountApplicable = discountApplicable;
	}


	public Date getPaidDate()
	{
		return paidDate;
	}

	public Date getDueDate()
	{
		return dueDate;
	}



	public void setPaidDate(final Date paidDate)
	{
		this.paidDate = paidDate;
	}



	public void setDueDate(final Date dueDate)
	{
		this.dueDate = dueDate;
	}



	public boolean isOverCreditLimit()
	{
		return overCreditLimit;
	}



	public void setOverCreditLimit(final boolean overCreditLimit)
	{
		this.overCreditLimit = overCreditLimit;
	}



	public boolean isCreditApplicable()
	{
		return creditApplicable;
	}



	public void setCreditApplicable(final boolean creditApplicable)
	{
		this.creditApplicable = creditApplicable;
	}

	public void setPercentDiscount(final float percentDiscount) {
		this.percentDiscount = percentDiscount;
	}

	@Override
	public String toString() {
		return "PaymentDetail [invoiceNumber=" + invoiceNumber + ", invoiceDate=" + invoiceDate + ", invTax=" + invTax
				+ ", discountedAmt=" + discountedAmt + ", invAmtIncTax=" + invAmtIncTax + ",\n percentDiscount="
				+ percentDiscount + ", status=" + status + ", discountExpiryDate=" + discountExpiryDate
				+ ", discountNearExpiry=" + discountNearExpiry + ",\n discountApplicable=" + discountApplicable
				+ ", paidDate=" + paidDate + ", dueDate=" + dueDate + ",\n overCreditLimit=" + overCreditLimit
				+ ", creditApplicable=" + creditApplicable + "]\n";
	}



	public float getDiscountedAmt() {
		return discountedAmt;
	}



	public void setDiscountedAmt(final float discountedAmt) {
		this.discountedAmt = discountedAmt;
	}



	public float getInvAmtIncTax() {
		return invAmtIncTax;
	}

}
