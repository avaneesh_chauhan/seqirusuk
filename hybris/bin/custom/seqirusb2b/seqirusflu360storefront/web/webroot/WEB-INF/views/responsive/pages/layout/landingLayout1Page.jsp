<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">

<div id="page-00" class="container">
    
    <cms:pageSlot position="BreadcrumbSlot" var="feature" element="div" class="breadcrumbs--page-nav">
		<cms:component component="${feature}" />
	</cms:pageSlot>
    <cms:pageSlot position="Section1" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<cms:pageSlot position="Section2A" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<cms:pageSlot position="Section2B" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<cms:pageSlot position="Section2C" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<cms:pageSlot position="Section3" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot> 
    <cms:pageSlot position="Section4" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	 <cms:pageSlot position="Section5" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<cms:pageSlot position="Section6" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
</div>
	
</template:page>