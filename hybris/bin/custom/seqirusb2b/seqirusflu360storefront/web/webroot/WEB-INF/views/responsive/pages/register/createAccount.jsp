<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%> 
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>    
     
<template:page pageTitle="${pageTitle}">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb padLeft79">
		<c:url value="/" var="homeUrl" />
			<li class="breadcrumb-item active"><a href="${homeUrl}" class=""><spring:theme
						code="form.register.homeLabel" /></a></li>
			<li class="breadcrumb-item" aria-current="page"><spring:theme
					code="form.register.createAccount" /></li>
		</ol>
	</nav>
	<div class="container-fluid maincontent col-xs-12">
		<div class="pageHeader col-xs-12">
			<spring:theme code="form.register.createLabel" />
		</div>
		<div class="contentArea col-xs-11">
			<div class="leftcontentArea col-xs-12 col-md-4 createAccountLeft">
				<%-- <cms:pageSlot position="LeftSideContentSlot" var="feature">
					<cms:component component="${feature}" /> --%>

					<div class="leftHeader col-xs-11">
						<spring:theme code="form.register.accountPage" />
					</div>
					<div class="leftsubHeader col-xs-11">
						<spring:theme code="form.register.accountpageInfo" />
					</div>
				<%-- </cms:pageSlot> --%>
			</div>

			<div class="rightcontentArea col-xs-12 col-md-8" id="rightSection_1">
				<div class="section1_header col-xs-12">
					<spring:theme code="form.register.option" />
				</div>
				<div class="section1_subheader col-xs-12">
					<spring:theme code="form.register.optionInfo" />
				</div>
				<a href="${contextPath}/<spring:theme code="header.link.cdcprofileja" text='createprofile?su=ja'/>">
				
					<div class="col-xs-11 section1_joinacc">
						<div class="subsecheader">
							<spring:theme code="form.register.joinAccount" />
						</div>
						<div class="subheaderarrow">&rarr;</div>
						<div class="subsectext">
							<spring:theme code="form.register.joinAccountInfo" />
						</div>
					</div>
				</a>
				<a href="${contextPath}/<spring:theme code="header.link.cdcprofile" text='createprofile?su=cp'/>">
					<div class="col-xs-11 section1_newcust">
	
	
						<div class="subsecheader">
							<spring:theme code="form.register.newCustomer" />
						</div>
						<div class="subheaderarrow">&rarr;</div>
						<div class="subsectext">
							<spring:theme code="form.register.newCustomerInfo" />
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</template:page>