<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer"
	tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%-- <c:if test="${component.visible}">
	<div class="container-fluid">
	    <div class="footer__top">
	        <div class="row">
	            <div class="footer__left col-xs-12 col-sm-12 col-md-9">
	                <div class="row">
	                	<c:forEach items="${component.navigationNode.children}" var="childLevel1">
		                	<c:forEach items="${childLevel1.children}" step="${component.wrapAfter}" varStatus="i">
							   <div class="footer__nav--container col-xs-12 col-sm-3">
		                	       <c:if test="${component.wrapAfter > i.index}">
	                                   <div class="title">${fn:escapeXml(childLevel1.title)}</div>
	                               </c:if>
	                               <ul class="footer__nav--links">
	                                   <c:forEach items="${childLevel1.children}" var="childLevel2" begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
	                                        <c:forEach items="${childLevel2.entries}" var="childlink" >
		                                        <cms:component component="${childlink.item}" evaluateRestriction="true" element="li" class="footer__link"/>
	                                        </c:forEach>
	                                   </c:forEach>
	                               </ul>
	                		   </div>
						    </c:forEach>
	                	</c:forEach>
	               </div>
	           </div>
	           <div class="footer__right col-xs-12 col-md-3">
	               <c:if test="${showLanguageCurrency}">
	                   <div class="row">
	                       <div class="col-xs-6 col-md-6 footer__dropdown">
	                           <footer:languageSelector languages="${languages}" currentLanguage="${currentLanguage}" />
	                       </div>
	                       <div class="col-xs-6 col-md-6 footer__dropdown">
	                           <footer:currencySelector currencies="${currencies}" currentCurrency="${currentCurrency}" />
	                       </div>
	                   </div>
	               </c:if>
	            </div>
	        </div>
	    </div>
	</div>
	
	<div class="footer__bottom">
	    <div class="footer__copyright">
	        <div class="container">${fn:escapeXml(notice)}</div>
	    </div>
	</div>
</c:if> --%>

<div class="col-xs-12 footerdiv">
	<div class="col-xs-12 footertopsection">
		<div class="col-md-3 col-xs-12 footerlogosection">
			<div class="footerlogo"></div>
			<div class="footersiteview">
			<div class="linkbtton"></div>
				<a class="footer-anchorlink" href="https://www.seqirus.com/" target="blank">seqirus.co.uk </a>
			</div>
			<!-- <div class="footerseqlink">Adverse Event Reporting</div> -->
			<!-- <div class="footerseqlink">Portal Usage Agreements</div> -->
		</div>
		<div class="col-md-3 col-xs-12 marTop-30">
               <div class="footertermsheader" style="color: #555555;">Navigate          
               </div>
               <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
	               	<div class="footerterm"><a href="#" class="footerterms">Orders</a></div>
	               <div class="footerterm"><a href="#" class="footerterms">Financials</a></div>
	               <div class="footerterm"><a href="#" class="footerterms">Products & Resources</a></div>
	               <div class="footerterm"><a href="${contextPath}/support" class="footerterms">Support</a></div>
	               <div class="footerterm"><a href="#" class="footerterms">Policies</a></div>
	               <!-- <div class="footerterm"><a href="#" class="footerterms">Notifications</a></div>   -->                  
	               <div class="footerterm"><a href="#" class="footerterms">Profile</a></div>
               </sec:authorize>
               <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
	               <div class="footerterm"><a href="${contextPath}/support" class="footerterms">Support</a></div>
               </sec:authorize>          
        </div>
		<div class="col-md-3 col-xs-12 footerterms">
		<c:forEach items="${component.navigationNode.children}" var="navNode">
				<c:forEach items="${navNode.children}" var="childNavNode"
					varStatus="productImageLoop">
					<c:choose>
						<c:when test="${not empty childNavNode.children}">
							<c:forEach items="${childNavNode.children}" var="childNavNode11">
								<div class="footerterm">
									<a href="${childNavNode11.entries[0].item.url}">${childNavNode11.name}</a><span
										class="footertermsfirst"></span>
								</div>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<c:if test="${not empty childNavNode.entries[0].item.media.url}">
									<c:choose>
										
										<c:when test="${childNavNode.entries[0].item.media.code eq 'AdjuvantedTrivalentLogo'}">
											<div id="footerlogos_3">
									<a href="${childNavNode.entries[0].item.url}" target="blank">
											<img class="adjuvantlogo"
										src="${childNavNode.entries[0].item.media.url}"
										alt="${childNavNode.entries[0].item.media.altText}">
										</a></div>
										</c:when>
										<c:when test="${childNavNode.entries[0].item.media.code eq 'logo-fluad'}">
											<div id="footerlogo_1">
									<a href="${childNavNode.entries[0].item.url}">
											<img class="Fluadlogo"
										src="${childNavNode.entries[0].item.media.url}"
										alt="${childNavNode.entries[0].item.media.altText}">
										</a></div>
										</c:when>
										<c:otherwise>
										<div id="footerlogo_2">
									<a href="${childNavNode.entries[0].item.url}" target="blank">
											<img class="Flucelvaxlogo"
										src="${childNavNode.entries[0].item.media.url}"
										alt="${childNavNode.entries[0].item.media.altText}">
										</a></div>
										</c:otherwise>
									</c:choose>
							</c:if>
							<c:if test="${empty childNavNode.entries[0].item.media.url}">
								<c:choose>
									<c:when
										test="${not empty childNavNode.name && childNavNode.name eq 'Product Site'}">
										<div class="footertermsheader">
											<div class="linkbtton"></div>
											<div class="productlinks" ><a href="${childNavNode.entries[0].item.url}"
												 class="footerterms" style="color: #555555;"><b>${childNavNode.name}</b></a></div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="footerterm">
											<div class="linkbtton"></div>
											<div class="productlinks"><a href="${childNavNode.entries[0].item.url}"
												class="footerterms">${childNavNode.name}</a></div>
										</div>
									</c:otherwise>
								</c:choose>

							</c:if>
						</c:otherwise>
					</c:choose>
				</c:forEach>
		</c:forEach>
		</div>
		<div class="col-md-3 col-xs-12 footerterms">
			<div
				style="color: #555555; font-family: campton-bold; font-size: 14px; margin-bottom: 20px;">
				<spring:theme code="footer.text1" />
			</div>
			<div>
				<p class="footerpara-1">
					<spring:theme code="footer.text2" />
					<a href="https://www.mhra.gov.uk/yellowcard" class="footerredlink" target="blank"><spring:theme
							code="footer.link1" /> </a>
					<spring:theme code="footer.text3" />
				</p>
				<p class="footerpara-2">
					<spring:theme code="footer.text4" />
					<a href="#" class="footerredlink"><spring:theme
							code="footer.link2" /></a>
					<spring:theme code="footer.text5" />
				</p>
			</div>
		</div>
	</div>
	<%-- <div class="copyrighttext col-xs-12 col-md-11">
		<spring:theme code="footer.copyright.text.line1" />
		<spring:theme code="footer.copyright.text.line2" />
		<spring:theme code="footer.copyright.text.line3" />
		<br>
		<spring:theme code="footer.copyright.text.line4" />
		<br>
	</div> --%>
	<div class="copyrighttext col-xs-12 col-md-11">
          <!-- <div class="col-md-9 textLeft"> -->
         	<spring:theme code="footer.copyright.text.line1" />
			<br>
			<spring:theme code="footer.copyright.text.line4" />
			<br>
         <!-- </div> -->
         <!-- <div class="col-md-3 footer-policies">
            <a href="#">Terms of Use</a>
            <a href="#">Privacy Policy</a>
            <a href="#">Cookie Policy</a>
         </div> -->
    </div>
</div>