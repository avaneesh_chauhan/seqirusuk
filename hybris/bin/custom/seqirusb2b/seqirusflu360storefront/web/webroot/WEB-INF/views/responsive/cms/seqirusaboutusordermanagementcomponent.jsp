<div class="row">
	<div class="container mar-left161">
		<div class="row">
			<div class="col-md-6 col-xs-12 aboutfirstsec">
				<h3 class="aboutsecondary-header">${feature.text1}</h3>
				<p class="aboutheaderpara">${feature.text2}</p>
				<ul class="circle">${feature.text3}</ul>
			</div>
			<div class="col-md-6 col-xs-12 abtgreyboxMar secondsection">
				<div>
					<img class="imgwidth aboutimg"
						src="${feature.orderManagementImage.url}" alt="Resource" />
				</div>
			</div>
		</div>
	</div>
</div>
