<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<c:set value="${fn:escapeXml(component.styleClass)}"
	var="navigationClassHtml" />
<c:if test="${component.visible}">
	<c:if test="${component.uid == 'LeftNavigationComponent'}">
		<ul class="nav nav-sidebar">
			<c:forEach items="${component.navigationNode.children}" var="navNode"
				varStatus="loop">
				<c:choose>
					<c:when test="${empty navNode.children}">
						<c:choose>
							<c:when test="${navNode.uid == 'StartOrderNavNode'}">
								<li class=""><a class="create-account margin-T30 "
									href="${navNode.entries[0].item.url}"><span
										class="startorder">${navNode.name}</span></a></li>
							</c:when>
							<c:otherwise>
								<li class="active"><a href="${navNode.entries[0].item.url}">${navNode.name}</a></li>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<li><a href="#" data-toggle="collapse"
							data-target="#submenu-${loop.index}">${navNode.name} <i
								class="fa fa-fw fa-angle-down pull-right"></i></a>
							<ul id="submenu-${loop.index}"
								class="collapse dashborad_sub_menu">
								<c:forEach items="${navNode.children}" var="childNavNode">
									<li><a href="${childNavNode.entries[0].item.url}">${childNavNode.name}</a></li>
								</c:forEach>
							</ul></li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul>
	</c:if>
	<c:choose>
		<c:when
			test="${(not anonymous) && (component.uid == 'MainHeaderComponent')}">
			<div class="header hidden-xs hidden-sm visible-md-* visible-lg-*">
				<div class="container-fluid container-lg">
					<div class="row">
						<div class="for-healthcare-profe">${component.name}</div>
						<div class="header-nav">
							<ul class="login-links marginBottom">
								<c:forEach items="${component.navigationNode.children}"
									var="navNode">
									<c:set value="${navNode.name}" var="name"></c:set>
									<li><a href="${navNode.entries[0].item.url}">${navNode.name} <c:if test="${fn:contains(navNode.name, 'Prescribing') || fn:contains(navNode.name, 'Important')}"><span class="glyphicon glyphicon-menu-down" style="top: 3px; left: 3px;"></span></c:if></a></li>
								</c:forEach>

								<a href="${navNode.entries[0].item.url}"><span
									class="glyphicon glyphicon-log-in"
									style="top: 3px; margin-left: -14px; color: #2A3237 !important;"></span></a>


							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="header hidden-md hidden-lg visible-xs-* visible-sm-*">
				<div class="for-healthcare-profe-mob col-xs-12 col-sm-12">${component.name}</div>
			</div>
		</c:when>
		<c:otherwise>
			<c:if test="${component.uid ne 'LeftNavigationComponent'}">
				<header
					class="headerbar hidden-xs hidden-sm visible-md-* visible-lg-*">
					<nav class="container-fluid container-lg">
						<div class="row headerSection">
							<div class="col-xs-2 col-md-4 col-lg-4 padding-L40 margin-T10">
								<a rel="home" href="#"> <cms:pageSlot position="SiteLogo"
										var="logo" limit="1">
										<cms:component component="${logo}" class="logoimg"
											alt="Seqirus A CSL Company" />
									</cms:pageSlot>
								</a>
							</div>
							<div class="col-xs-8 navcolumn">
								<nav class="primary-nav">
									<ul class="">
										<c:forEach items="${component.navigationNode.children}"
											var="navNode">
											<c:choose>
												<c:when test="${not empty navNode.children}">
													<li class="nav-item dropdown has-megamenu"><a
														class="nav-link dropdown-toggle under_arrow"
														data-toggle="dropdown" href="#">${navNode.name}</a>
														<div class="indicator_arrow">
															<i class="fa fa-angle-up" aria-hidden="true"></i>
														</div>
														<ul class="dropdown-menu megamenu pro_menu">


															<c:forEach items="${navNode.children}" var="childNavNode">
																<c:if test="${childNavNode.name ne 'Clinical Support'}">
																	<li class="dropdown_first_level"><a
																		href="${childNavNode.entries[0].item.url}"
																		class="nav-link">${childNavNode.name}</a></li>
																</c:if>
																<c:if test="${childNavNode.name eq 'Clinical Support'}">
																	<li class="dropdown_first_level"><a
																		class="nav-link" href="#">${childNavNode.name}</a>
																		<ul class="second_level_menu">
																			<c:forEach items="${childNavNode.children}"
																				var="clinicalNavNode">
																				<li><a class="nav-link"
																					href="${clinicalNavNode.entries[0].item.url}">${clinicalNavNode.name}</a>
																				</li>
																			</c:forEach>

																		</ul></li>
																</c:if>
															</c:forEach>
														</ul></li>
												</c:when>
												<c:otherwise>

													<c:choose>
														<c:when test="${fn:contains(navNode.name, 'Account')}">
															<li class="redborder"><a class="create-account"
																href="${navNode.entries[0].item.url}">${navNode.name}</a></li>
														</c:when>
														<c:otherwise>
															<li><a class="" href="${navNode.entries[0].item.url}">${navNode.name}</a></li>
														</c:otherwise>
													</c:choose>



												</c:otherwise>
											</c:choose>



										</c:forEach>
									</ul>
								</nav>
							</div>
						</div>
					</nav>
				</header>
				<div class="hidden-md hidden-lg visible-xs-* visible-sm-*">
					<div class="mob-logo-wrap">
						<div class="col-xs-6 col-sm-6 padding-T20 padding-B20">
							<a rel="home" href="#"> <cms:pageSlot position="SiteLogo"
									var="logo" limit="1">
									<cms:component component="${logo}" class="logoimg"
										alt="Seqirus A CSL Company" width="120" />
								</cms:pageSlot>
							</a>
						</div>
						<div class="col-xs-6 col-sm-6 mob-menu">
							<i class="fa fa-bars mob_nav_click" aria-hidden="true"></i>
						</div>
					</div>
					<div class="mob_exp_menu">

						<!-- Accordion Menu START -->
						<div class="panel-group" id="accordion">
							<c:forEach items="${component.navigationNode.children}"
								var="navNode">
								<c:choose>
									<c:when test="${navNode.name eq 'flu360 Overview'}">
										<c:set var="navigations" value="collapseOne"></c:set>
									</c:when>
									<c:when test="${navNode.name eq 'Products'}">
										<c:set var="navigations" value="collapseTwo"></c:set>
									</c:when>
									<c:when test="${navNode.name eq 'Tools & Resources'}">
										<c:set var="navigations" value="collapseThree"></c:set>
									</c:when>
								</c:choose>
								<c:if test="${navNode.name ne 'Create Account'}">
									<div class="panel panel-default">
										<div class="panel-heading accordion-toggle collapsed"
											data-toggle="collapse" data-parent="#accordion"
											data-target="#${navigations}">
											<h4 class="panel-title">${navNode.name}</h4>
										</div>
										<div id="${navigations}" class="panel-collapse collapse">
											<div class="panel-body">
												<ul class="mob_sub_menu">
													<c:forEach items="${navNode.children}" var="childNavNode">
														<li><a href="${childNavNode.entries[0].item.url}"
															target="_self" title="${childNavNode.name}">${childNavNode.name}</a>
															<c:forEach items="${childNavNode.children}"
																var="clinicalNavNode">
																<ul class="second_level_mob_sub">
																	<li><a
																		href="${clinicalNavNode.entries[0].item.url}"
																		target="_self" title="${clinicalNavNode.name}">${clinicalNavNode.name}</a></li>
																</ul>
															</c:forEach></li>
													</c:forEach>
												</ul>
											</div>
										</div>
									</div>
								</c:if>
							</c:forEach>

							<div class="panel-heading">
								<h4 class="panel-title-default">
									<a href="#" target="_self" title="About">About</a>
								</h4>
							</div>

							<div class="panel-heading">
								<h4 class="panel-title-default">
									<a href="#" target="_self" title="Support">Support</a>
								</h4>
							</div>

						</div>
						<!-- Accordion menu END -->

						<div class="default-mob-menu-item">
							<div class="col-xs-6">
								<div class="redborder">
									<a class="create-account" href="#">Create Account</a>
								</div>

							</div>
							<div class="col-xs-6 mob-login-icon">
								<a class="log-in" href="">Log In <span
									class="glyphicon glyphicon-log-in" style="top: 3px;"></span></a>
							</div>
						</div>
					</div>
				</div>
			</c:if>
		</c:otherwise>
	</c:choose>
</c:if>