<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
    <div id="influenza-burden">

        <!-- HERO SECTION STARTS -->
        <div class="hero--overview">
            <div class="breadcrumbs--page-nav">
                <p>Home > flu360 Overview > Clinical Support > <strong>Influenza Burden 6+ months </strong></p>
            </div>
            <div class="hero--overview-content">
                <img class="rectangle-down" src="${themeResourcePath}/cms/assets/images/rectangle-down.svg" >
                <h1>Influenza impacts millions of people in the US<sup>1</sup></h1>
                <p>Seasonal influenza can affect everyone—children and adults. <span>flu360<sup>&trade;</sup></span> keeps
                    you informed of what that influenza burden means to your patients and your practice.</p>
            </div>

        </div>

        <div class="row-flex center-xs">
            <div class="hero--overview-content-mobile hide-desktop">
                <img class="rectangle-down" src="${themeResourcePath}/cms/assets/images/rectangle-down.svg">
                <h1>Influenza impacts millions of people in the US<sup>1</sup></h1>
                <p>Seasonal influenza can affect everyone—children and adults. <span>flu360<sup>&trade;</sup></span> keeps
                    you informed of what that influenza burden means to your patients and your practice.</p>
            </div>

        </div>
        <!-- HERO SECTION ENDS -->

        <!-- INFLUENZA RESULTS SECTION STARTS -->
        <div class="row-flex bg--grey-0">
            <div id="influenza-results-header"
                class="col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-sm-12 text-center">
                <h2>Each year in the US, influenza results in an estimated<sup>1</sup>:</h2>
            </div>

        </div>

        <div id="facts-card-row" class="row-flex bg--grey-0 carousel--three-card-mobile">
            <div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12">
                <div class="bg--white facts-card">
                    <div class="text-center card-content">
                        <img src="${themeResourcePath}/cms/assets/images/illness-icon.svg">
                        <p><span>9 million-45 million</span><br> illnesses*</p>
                    </div>
                </div>
            </div>
            <div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12">
                <div class="bg--white facts-card">
                    <div class="text-center card-content">
                        <img src="${themeResourcePath}/cms/assets/images/hospital-icon.svg">
                        <p><span>140,000-810,000</span><br> hospitalizations*</p>
                    </div>
                </div>
            </div>
            <div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12">
                <div class="bg--white facts-card">
                    <div class="text-center card-content">
                        <img src="${themeResourcePath}/cms/assets/images/death-icon.svg">
                        <p><span>12,000-61,000</span><br> deaths*</p>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-xs-12 bg--grey-0">
            <p class="fact-card-footnote">*CDC estimates from 2010-2011 through 2019-2020 US influenza seasons</p>
        </div>
        <!-- INFLUENZA RESULTS SECTION ENDS -->

        <!--  INFLUENZA SEASON SECTION STARTS -->
        <div class="container">
            <div class="row-flex">
                <div id="influnenza-season-header"
                    class="col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-sm-12 text-center">
                    <h2>In the 2019-2020 influenza season, the CDC reported<sup>2</sup></h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row-flex">

                <div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
                    <div class="two-row--card">
                        <div class="two-row--card-content">
                            <div class="two-row--card-top">
                                <p>Approximately</p>
                                <p class="card-callout">405,000<br class="show-br-sm-mobile">hospitalizations</p>
                                <p>influenza-related, all age groups</p>
                            </div>
                            <hr>
                            <div class="two-row--card-bottom">
                                <p>Approximately</p>
                                <p class="card-callout">52,500</p>
                                <p>were children*</p>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
                    <div class="two-row--card">
                        <div class="two-row--card-content">
                            <div class="two-row--card-top">
                                <p>Nearly</p>
                                <p class="card-callout">22,000 deaths</p>
                                <p>influenza-related, all age groups</p>
                            </div>
                            <hr>
                            <div class="two-row--card-bottom">
                                <p>At least</p>
                                <p class="card-callout">434</p>
                                <p>were children*&dagger;</p>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-xs-12">
                    <div class="influenza-season-footnotes">
                        <p>*Under 18 years</p>
                        <p>&dagger;Combines data on hospitalization rates, influenza testing practices, and<br>
                            the frequency of death in and out of the hospital from death certificates </p>
                    </div>
                </div>
            </div>

        </div>

        <!--  INFLUENZA SEASON SECTION ENDS -->
        <!-- INFLUENZA VACCINE HERO STARTS -->
        <div id="influenza-hero" class="row-flex">
            <div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
                <img class="influenza-hero-image" src="${themeResourcePath}/cms/assets/images/influenza-vaccine-hero.png">
            </div>

            <div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12 influenza-callout-desktop">
                <div class="influenza-hero-container">
                    <img class="rectangle-down" src="${themeResourcePath}/cms/assets/images/rectangle-down.svg">
                    <p class="callout">Influenza vaccine effectiveness has varied considerably over<br
                            class="hide-br-mobile"> the last 10 years</p>
                    <img class="rectangle-up" src="${themeResourcePath}/cms/assets/images/rectangle-up.svg">
                    <p class="small-callout">
                        Vaccine effectiveness in the US has ranged from <strong>as low as 19% in the 2014-2015 season to 60%
                            in the 2010-2011 season</strong><sup>3</sup>
                    </p>
                </div>
            </div>

        </div>

        <!-- INFLUENZA VACCINE HERO ENDS-->

        <!-- INFLUENZA BURDEN HERO STARTS  -->
        <div id="influenza-burden-hero" class="row-flex bg--grey-100">
            <div class="col-flex-lg-8 col-flex-md-8 col-flex-sm-12 col-flex-xs-12">
              <h3>
                    We're committed to helping reduce the influenza burden through vaccine innovation designed to improve vaccine effectiveness
                </h3>
                <a href="#">Discover our cell-based technology <img src="${themeResourcePath}/cms/assets/images/arrow-right-white.svg"></a>
            </div>
            <div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12">
                  <img class="cells-360-bg" src="${themeResourcePath}/cms/assets/images/influenza-vaccine-hero-bg.svg">
            </div>

        </div>    <!-- INFLUENZA BURDEN HERO ENDS  -->

        <!-- VACCINE PORTFOLIO STARTS -->
        <div class="container">
            <div id="vaccine-header-row" class="row-flex">
                    <div class="col-flex-12 vacccine-portfolio-header--row">
                         <div class="col-flex-sm-12 header--container">
                            <h2>Explore the Seqirus Vaccine Portfolio<div class="header-line"></div></h2>
                        </div>
                    </div>

                </div>

                <div class="row-flex vaccine-portfolio-row">
                    <div class="col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                        <div class="logo--card center-xs">
                            <div class="logo--card-container">
                            <img class="fluad-logo" src="${themeResourcePath}/cms/assets/images/logos/fluad-logo.svg">
                            <a href="#" class="no-underline">
                            <p class="text-dark-gray cta ml-20" style="margin-top: 25px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </p>
                            </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                        <div class="logo--card center-xs">
                            <div class="logo--card-container">
                            <img class="flucelvax-logo" src="${themeResourcePath}/cms/assets/images/logos/flucelvax-logo.svg">
                            <a href="#" class="no-underline">
                            <p class="text-dark-gray cta" style="margin-top: 30px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </p>
                            </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-flex-md-4 col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                        <div class="logo--card center-xs">
                            <div class="logo--card-container">
                            <img class="affluria-logo mt-10" src="${themeResourcePath}/cms/assets/images/logos/affluria-logo.svg">
                            <a href="#" class="no-underline">
                            <p class="text-dark-gray cta ml-10" style="margin-top: 35px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </p>
                            </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-flex-xl-12 col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-xs-12">
                        <p class="disclaimer--paragraph">Please see Important Safety Information and full US Prescribing Information
                            on each vaccine's respective product page.</p>
                    </div>
                </div>    </div>
        <!-- VACCINE PORTFOLIO ENDS  -->

        <!-- REFERENCES START -->
        <div class="container">
            <div class="row-flex page-references">
                <div class="col-flex-lg=12 col-flex-md-12 col-flex-sm-12 col-flex-xs-12 px-sm-20">
                    <p>
                        <strong>References:</strong> <strong>1.</strong> Centers for Disease Control and Prevention. Disease burden of influenza. Accessed October 20,
                        2020. https://www.cdc.gov/flu/ about/burden/index.html <strong>2.</strong> Centers for Disease Control and Prevention.
                        Estimated influenza illnesses, medical visits, hospitalizations, and deaths in the United States — 2019-2020
                        influenza season. Accessed January 6, 2021. https://www.cdc.gov/flu/about/burden/2019-2020.html <strong>3.</strong> Centers
                        for Disease Control and Prevention. Past seasons vaccine effectiveness estimates. Accessed September 25,
                        2020. https://www.cdc.gov/flu/vaccines-work/past-seasons-estimates.html
                    </p>
                </div>
            </div>
        </div>
        <!-- REFERENCES END  -->

    </div>
</template:page>
