<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<template:page pageTitle="${pageTitle}">
<div id="prod-landing">
	<!-- begin hero -->
	<div class="hero--grad" style="background-image: url('_ui/responsive/theme-lambda/images/prod-hero.png')">
	
		<div class="breadcrumbs--page-nav">
			<div class="container">
				<p>
					<a href="#">Home</a><span> > </span>
					<strong>Products</strong>
				</p>
			</div>
		</div>
		<div class="row-flex">
			<div class="col-flex-12 hidden-sm hidden-md hidden-lg hero--grad__mobile">
				<img src="_ui/responsive/theme-lambda/images/prod-hero.png" role="presentation">
			</div>
			<div class="container">
				<div class="col-flex-12 col-flex-md-8">
					<div class="hero--grad__content">
						<div class="content-container content-container--has-corner text-left">
							<h1>A flu vaccine portfolio both innovative and dependable</h1>
							<p class="text--grey-100">Whether it's adjuvant technology, innovative cell-based manufacturing, or traditional egg-based manufacturing, the Seqirus vaccine portfolio is dedicated to both innovative and enduring influenza technoligies for your patients' needs.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end hero -->
	
	<div class="product-3col container">
	    <div class="product-3col__body">
				<div class="product-3col__item">
					<img alt="Fluad Quadrivalent logo" src="_ui/responsive/theme-lambda/images/fluad-logo.svg">
					<h3>Fluad Quadrivalent</h3>
					<p>The first-and-only adjuvanted quadrivalent seasonal influenza vaccine approved for patients <strong>65+ years<sup>1</sup></strong></p>
					<a href="${contextPath}/products/fluad" class="no-underline">Learn More</a>
				</div>
				<div class="product-3col__item">
					<img alt="Flucelvax Quadrivalent logo" src="_ui/responsive/theme-lambda/images/flucelvax-logo.svg">
					<h3>Flucelvax Quadrivalent</h3>
					<p>The first-and-only cell-based quadrivalent seasonal influenza vaccine in the US approved for patients <strong>2+ years<sup>2,3</sup></strong></p>
					<a href="${contextPath}/products/flucelvax" class="no-underline">Learn More</a>
				</div>
				<div class="product-3col__item">
					<img alt="Afluria Quadrivalent logo" src="_ui/responsive/theme-lambda/images/affluria-logo.svg">
					<h3>Afluria Quadrivalent</h3>
					<p>A quadrivalent influenza vaccine for the prevention of seasonal influenza in patients <strong>6+ months<sup>4</sup></strong></p>
					<a href="${contextPath}/products/afluria" class="no-underline">Learn More</a>
				</div>
	    </div>
		<div class="product-3col__footer">
			<p class="disclaimer--paragraph">Please see Important Safety Information on each vaccine&#x27;s respective product page.</p>
		</div>
	</div>	<!-- begin hero -->
	<div class="hero--grad hero--grad-alt" style="background-image: url('_ui/responsive/theme-lambda/images/prod-cont-hero.png')">
	
		<div class="row-flex">
			<div class="col-flex-12 hidden-sm hidden-md hidden-lg hero--grad__mobile">
				<img src="_ui/responsive/theme-lambda/images/prod-cont-hero-mob.png" role="presentation">
			</div>
			<div class="container">
				<div class="col-flex-12 col-flex-md-6">
					<div class="hero--grad__content">
						<div class="content-container content-container--has-corner text-left">
								<h2>Order. Track. Manage.<br /><span class='text--red-100'>flu360<sup>TM</sup></span> simplifies at every step.</h2>
							<a class="no-underline redborder" href="#">Order Now</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end hero -->
	
	<!-- begin 50 percent speedbump -->
	<section class="bg--grey-100 speedbump speedbump--50">
		<div class="container">
			<div class="speedbump__content">
				<h3 class="header--3">Influenza can have a devastating impact on adults 65 years and older<sup>5</sup></h3>
					<a href="#" class="text--white cta">Learn about the burden of influenza<img src="_ui/responsive/theme-lambda/images/arrow-right-white.svg">
					</a>
			</div>
		</div>
	</section>
	<!-- end 50 percent speedbump -->	<div id="safetyInfoAnchor"></div>
		<section id="safetyInfo" class="safety-info sbs is-sticky">
			<div class="container">
				<div class="safety-info__header">
					<button class="safety-info__btn">MORE <img role="presentation" src="_ui/responsive/theme-lambda/images/plus.svg"></button>
				</div>
				<div class="sbs__body">
					<div class="sbs__left">
						<p><strong>IMPORTANT SAFETY INFORMATION for FLUAD� (Influenza Vaccine, Adjuvanted), FLUAD� QUADRIVALENT (Influenza Vaccine, Adjuvanted), AFLURIA� QUADRIVALENT (Influenza Vaccine), and FLUCELVAX� QUADRIVALENT (Influenza Vaccine)</strong></p>
	
						<p><strong>CONTRAINDICATIONS</strong></p>
	
						<p>Do not administer FLUAD, FLUAD QUADRIVALENT, or AFLURIA QUADRIVALENT to anyone with a history of severe allergic reaction (e.g. anaphylaxis) to any component of the vaccine, including egg protein, or to a previous influenza vaccine. Do not administer FLUCELVAX QUADRIVALENT to anyone with a history of severe allergic reactions (e.g. anaphylaxis) to any component of the vaccine.</p>
	
						<p><strong>WARNINGS AND PRECAUTIONS</strong></p>
	
						<p>If Guillain-Barr� syndrome (GBS) has occurred within 6 weeks of receipt of prior influenza vaccine, the decision to give FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT or FLUCELVAX QUADRIVALENT should be based on careful consideration of the potential benefits and risks.</p>
						
						<p>Appropriate medical treatment and supervision must be available to manage possible anaphylactic reactions following administration of the vaccine.</p>
						
						<p>Syncope (fainting) may occur in association with administration of injectable vaccines including FLUAD, FLUAD QUADRIVALENT, and FLUCELVAX QUADRIVALENT. Syncope can be accompanied by transient neurological signs such as visual disturbance, paresthesia, and tonic-clonic limb movements. Ensure procedures are in place to avoid falling injury and to restore cerebral perfusion following syncope by maintaining a supine or Trendelenburg position.</p>
						
						<p>The immune response to FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT and FLUCELVAX QUADRIVALENT in immunocompromised persons, including individuals receiving immunosuppressive therapy, may be lower than in immunocompetent individuals.</p>
						
						<p>Vaccination with FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT and FLUCELVAX QUADRIVALENT may not protect all vaccine recipients against influenza disease.</p>
	
						<p><strong>ADVERSE REACTIONS</strong></p>
	
						<p>FLUAD:</p>
	
						<p>The most common (&ge; 10%) local (injection site) adverse reactions observed in clinical studies with FLUAD were injection site pain (25%) and tenderness (21%). The most common (&ge; 10%) systemic adverse reactions observed in clinical studies with FLUAD were myalgia (15%), headache (13%) and fatigue (13%).</p>
	
						<p>FLUAD QUADRIVALENT:</p>
	
						<p>The most common (&ge; 10%) local and systemic reactions with FLUAD QUADRIVALENT in elderly subjects 65 years of age and older were injection site pain (16.3%), headache (10.8%) and fatigue (10.5%).</p>
	
						<p>AFLURIA QUADRIVALENT:</p>
	
						<p>AFLURIA QUADRIVALENT administered by needle and syringe:</p>
	
						<p>In adults 18 through 64 years, the most commonly reported injection-site adverse reaction was pain (&ge; 40%). The most common systemic adverse events were myalgia and headache (&ge; 20%).</p>
	
						<p>In adults 65 years of age and older, the most commonly reported injection-site adverse reaction was pain (&ge; 20%).  The most common systemic adverse event was myalgia (&ge; 10%).</p>
	
						<p>In children 5 through 8 years, the most commonly reported injection-site adverse reactions were pain (&ge; 50%), redness and swelling (&ge; 10%).  The most common systemic adverse event was headache (&ge; 10%).</p>
	
						<p>In children 9 through 17 years, the most commonly reported injection-site adverse reactions were pain (&ge; 50%), redness and swelling (&ge; 10%).  The most common systemic adverse events were headache, myalgia, and malaise and fatigue (&ge; 10%).</p>
	
						<p>In children 6 months through 35 months of age, the most commonly reported injection-site reactions were pain and redness (&ge; 20%). The most common systemic adverse events were irritability (&ge; 30%), diarrhea and loss of appetite (&ge; 20%).</p> 
	
						<p>In children 36 through 59 months of age, the most commonly reported injection site reactions were pain (&ge; 30%) and redness (&ge; 20%).  The most commonly reported systemic adverse events were malaise and fatigue, and diarrhea (&ge; 10%).</p>
	
						<p>The safety experience with AFLURIA (trivalent formulation) is relevant to AFLURIA QUADRIVALENT because both vaccines are manufactured using the same process and have overlapping compositions:</p>
	
						<p>In adults 18 through 64 years of age, the most commonly reported injection-site adverse reactions with AFLURIA (trivalent formulation) when administered by the PharmaJet Stratis Needle-Free Injection System were tenderness (&ge; 80%), swelling, pain, redness (&ge; 60%), itching (&ge; 20%) and bruising (&ge; 10%). The most common systemic adverse events were myalgia, malaise (&ge; 30%), and headache (&ge; 20%).</p>
	
						<p>FLUCELVAX QUADRIVALENT:</p>
	
						<p>In adults 18 through 64 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were pain (&ge; 40%), erythema and induration (&ge; 10%). The most common systemic adverse events were headache, fatigue and myalgia (&ge; 10%).</p>
	
						<p>In adults &ge; 65 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were pain (&ge; 20%) and erythema (&ge; 10%).</p>
	
						<p>In children 2 through 8 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were tenderness (28.7%), pain (27.9%) and erythema (21.3%), induration (14.9%) and ecchymosis (10.0%). The most common systemic adverse events were sleepiness (14.9%), headache (13.8%), fatigue (13.8%), irritability (13.8%) and loss of appetite (10.6%).</p>
	
						<p>In children and adolescents 9 through 17 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were injection site pain (21.7%), erythema (17.2%) and induration (10.5%).  The most common systemic adverse events were headache (18.1%) and fatigue (17.0%).</p>
	
						<p>To report SUSPECTED ADVERSE REACTIONS, contact Seqirus USA Inc. at <strong>1-855-358-8966</strong> or <strong>VAERS</strong> at <strong>1-800-822-7967</strong> or <a target="_blank" href="http://www.vaers.hhs.gov"><strong>www.vaers.hhs.gov</strong></a>.</p>
	
						<p>Before administration, please see the full US Prescribing Information for FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT and FLUCELVAX QUADRIVALENT.</p>
	
						<p>FLUAD�, FLUAD� QUADRIVALENT, AFLURIA� QUADRIVALENT and FLUCELVAX� QUADRIVALENT are registered trademarks of Seqirus UK Limited or its affiliates.</p>
					</div>
					<div class="sbs__right">
						<p><strong>INDICATIONS AND USAGE</strong></p>
	
						<p>FLUAD� (Influenza Vaccine, Adjuvanted) is an inactivated influenza vaccine indicated for active immunization against influenza disease caused by influenza virus subtypes A and type B contained in the vaccine. FLUAD� QUADRIVALENT (Influenza Vaccine, Adjuvanted) is an inactivated influenza vaccine indicated for active immunization against influenza disease caused by influenza virus subtypes A and types B contained in the vaccine.  FLUAD and FLUAD QUADRIVALENT are approved for use in persons 65 years of age and older.  These indications are approved under accelerated approval based on the immune response elicited by FLUAD QUADRIVALENT.</p>
	
						<p>AFLURIA� QUADRIVALENT (Influenza Vaccine) is an inactivated influenza vaccine indicated for active immunization against influenza disease caused by influenza A subtype viruses and type B viruses contained in the vaccine.  AFLURIA QUADRIVALENT is approved for use in persons 6 months of age and older.</p>
	
						<p>FLUCELVAX� QUADRIVALENT (Influenza Vaccine) is an inactivated vaccine indicated for active immunization for the prevention of influenza disease caused by influenza virus subtypes A and types B contained in the vaccine. FLUCELVAX QUADRIVALENT is approved for use in persons 2 years of age and older.</p>
					</div>
				</div>
			</div>
		 </section>	 <section class="references">
		<div class="container">
			<p><strong>References:</strong><br /><br /><strong>1.</strong> FLUAD QUADRIVALENT. Package insert. Seqirus Inc; 2020. <strong>2.</strong> FLUCELVAX QUADRIVALENT. Package insert. Seqirus Inc; 2021. <strong>3.</strong> Centers for Disease Control and Prevention. Cell-based flu vaccines. Accessed February 11, 2021. https://www.cdc.gov/flu/prevent/cell-based.htm <strong>4.</strong> AFLURIA QUADRIVALENT. Package insert. Seqirus Inc; 2020. <strong>5.</strong> Centers for Disease Control and Prevention. Flu & people 65 years and older. Accessed February 12, 2021. https://www.cdc.gov/flu/highrisk/65over.htm</p>
		</div>
	</section>
</div>


</template:page>
