<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="col-xs-12 col-md-6">
	<div class="row orders_rowsection ">
		<div class="col-xs-12 orders_graphtitle">
			Current Season Status <span class="orders_graphsubtitle">${seasonEntry.currentSeason}</span>
		</div>
		<div id="ordergraphcontainer">		
			<div id="totalShippedQty" style="display:none">${Total_Shipped_Qty}</div>
			<div id="delivered" style="display:none">${Delivered}</div>
			<div id="inTransit" style="display:none">${inTransit}</div>
			<div id="processing" style="display:none">${processing}</div>
		</div>
		<div class=" row">
			<div class="col-md-2 "></div>
			<div class="col-md-8 orders_rightbottomborder"></div>
			<div class="col-md-2 "></div>
		</div>
	</div>
	<div class="row orders_rowsection ">
		<div class="col-xs-12 orders_newordertitle">New Order</div>
		<div class="col-xs-12 orders_neworderbox1">
			<a href="#">
				<div class="orders_boxtext1">Reserve for</div>
				<div class="orders_boxtext2">
					2022 - 2023 Season <i class="orders_global_blackarrow pull-right"></i>
				</div>
			</a>
		</div>
		<div class="col-xs-12 orders_neworderbox2">
			<a href="#">
				<div class="orders_boxtext1">Order for</div>
				<div class="orders_boxtext2">
					${seasonEntry.currentSeason} Season <i class="orders_global_blackarrow pull-right"></i>
				</div>
			</a>
		</div>
		<div class="row">
			<div class="col-md-2 "></div>
			<div class="col-md-8 orders_rightbottomborder"></div>
			<div class="col-md-2 "></div>
		</div>
	</div>
	<div class="row orders_rowsection ">
		<div class="row">
			<div class="col-xs-12 col-md-5 orders_producttitle">Product
				Availability</div>
			<div class="col-xs-12 col-md-5 orders_productyear">${seasonEntry.currentSeason}</div>
		</div>
		<div class="col-xs-12 orders_prodbox">
			<div class="orders_prodtext1">FLUAD� QUADRIVALENT</div>
			<div class="orders_prodtext2">
				0.5-mL pre-filled syringe <span
					class="orders_avilabletxt pull-right">AVAILABLE</span>
			</div>
		</div>
		<div class="col-xs-12 orders_prodbox">
			<div class="orders_prodtext1">FLUCELVAX� QUADRIVALENT</div>
			<div class="orders_prodtext2">
				0.5-mL pre-filled syringe <span
					class="orders_avilabletxt pull-right">AVAILABLE</span>
			</div>
			<div class="orders_prodtext2">
				5-mL multi-dose vial <span class="orders_soldouttxt pull-right">Sold
					Out</span>
			</div>
		</div>
		<div class="col-xs-12 orders_prodboxlast">
			<div class="orders_prodtext1">AFLURIA� QUADRIVALENT</div>
			<div class="orders_prodtext2">
				0.25-mL pre-filled syringe<span
					class="orders_avilabletxt pull-right">AVAILABLE</span>
			</div>
			<div class="orders_prodtext2">
				0.5-mL pre-filled syringe <span
					class="orders_avilabletxt pull-right">AVAILABLE</span>
			</div>
			<div class="orders_prodtext2">
				5-mL multi-dose vial <span class="orders_avilabletxt pull-right">AVAILABLE</span>
			</div>
		</div>
	</div>
</div>
<div class="col-xs-12 col-md-6">
	<div class="row">
		<div class="row">
			<div class="col-xs-12 orders_graphshiptitle">Shipping
				Visibility</div>
			<div class="col-xs-12 orders_nextdeliverble">Next Scheduled
				Delivery</div>
			<div class="col-xs-12 orders_monthdelverytxt">October 15</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-6 margin-T20">
			<div class="row order_deliverytxt">
				<div class="orders_estimatetxt">Estimated delivery window(s):</div>
				<div class="orders_monthtxt">October 15 - October 19</div>
			</div>
			<div class="row orders_locrowbroder">
				<div class="orders_locationtxt">Location Name</div>
				<div class="orders_locationprodtxt">500 units FLUAD�
					QUADRIVALENT</div>
				<div class="orders_locationprodtxt">400 units FLUCELVAX�
					QUADRIVALENT</div>
				<div class="orders_locationprodtxt">300 units AFLURIA�
					QUADRIVALENT</div>
			</div>
			<div class="row orders_locrowbroder">
				<div class="orders_locationtxt">Location Name</div>
				<div class="orders_locationprodtxt">500 units FLUAD�
					QUADRIVALENT</div>
				<div class="orders_locationprodtxt">400 units FLUCELVAX�
					QUADRIVALENT</div>
				<div class="orders_locationprodtxt">300 units AFLURIA�
					QUADRIVALENT</div>
			</div>
			<div class="row ">
				<div class="orders_locationprodtxt">+ 5 more locations</div>
				<div class="orders_viewall">
					View all locations <i class="global_blackarrow"></i>
				</div>

			</div>
		</div>
		<div class="col-xs-12 col-md-6 margin-T20">
			<div id="orderlanding_calendar" class="col-xs-12"></div>
		</div>
	</div>
	<div class="row orders_bordertop">
		<div
			class="col-xs-12 col-md-6 orders_latesshipment no-padding margin-T30">
			Latest Shipments</div>
		<div class="col-xs-12 col-md-6 margin-T30">
			<a href="#" class="orders_viewalltxt">View all open orders<i
				class="global_blackarrow"></i></a>
		</div>
		<div class="col-xs-12 margin-T30">
			<div class="row orderlandingshipment">
				<table id="orderlandingshipmenttabel" class="display">
					<thead>
						<tr>
							<th>Order #</th>
							<th>Shipment Unit Qty</th>
							<th>Shipped to</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					</tbody>

				</table>
			</div>
		</div>
	</div>
</div>