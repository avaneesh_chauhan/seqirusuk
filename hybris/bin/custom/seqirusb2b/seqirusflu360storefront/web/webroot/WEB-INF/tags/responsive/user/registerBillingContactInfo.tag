<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<div id="BusinessInformation" class="tabcontent">
               <div class="section1_header col-xs-12">
                  <spring:theme code="form.register.billing.header" />
               </div>
               <div class="section1_subheader col-xs-12">
                  <spring:theme code="form.register.billing.subHeader" /><br>
                  <input type="radio" id="payinginfo1" name="payinginfo" value="<spring:theme code="form.register.billing.radioText" />">
                  <label class="radioLabel" for="payinginfo1"><spring:theme code="form.register.billing.radioText" /></label>
               </div>

            
                  <div class="col-xs-12 businessformContent">
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step2FirstName"><spring:theme code="form.register.billing.firstName" /></label><br/>
                              <input  class="form-control textonly" id="step2FirstName" name="billingContactInfo.firstName" placeholder="Edward"  type="text"/>                              
                           </div>
                           <div class="form-group col-md-4">
                              <label for="step2LastName"><spring:theme code="form.register.billing.lastName" /></label><br/>
                              <input  class="form-control textonly" id="step2LastName" name="billingContactInfo.lastName" placeholder="Newgate"  type="text"/> 
                           </div>
                        </div>
                     </div> 

                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step2Email"><spring:theme code="form.register.billing.email" /></label><br/>
                              <input class="form-control emailcheck_step1 spcl-fld" autocomplete="no" id="step2Email" name="billingContactInfo.email" data-msg-email="Please enter valid Email" placeholder="email@example.co.uk"  type="email"/>
                              <div class="help-block"></div>                             
                           </div>
                           <div class="form-group col-md-4">
                              <label for="step2phoneNumber"><spring:theme code="form.register.billing.phoneNum" /></label><br/>
                              <input  class="form-control phone-not-man spcl-fld" autocomplete="no" id="step2phoneNumber" name="billingContactInfo.phone" maxlength="11"  type="text"/> 
                           	  <div class="help-block"></div>  
                           </div>
                           <div class="form-group col-md-2">
                              <label for="step2phoneext"><spring:theme code="form.register.billing.phoneExt" /></label><br/>
                              <input  class="form-control numberonly" id="step2phoneext" name="billingContactInfo.phoneExt"  maxlength="4" type="text"/>  
                           </div>
                        </div>
                     </div> 
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step2jobtitle"><spring:theme code="form.register.billing.jobTitle" /></label><br/>
                              <input  class="form-control textonly" id="step2jobtitle" name="billingContactInfo.jobTitle" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.business.jobtitleDesc" />" placeholder="E.g. Vaccine Buyer"  type="text"/>                             
                           </div>
                           <div class="form-group col-md-4"> 
                              <label for="step2organisationname"><spring:theme code="form.register.billing.orgName" /></label><br/>
                              <input  class="form-control with_space" id="step2organisationname" name="billingContactInfo.organizationName" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.business.orgName" />" placeholder="Practice, Pharmacy or Business Name"  type="text"/>   
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-5 col-xs-12">
                           <div class="form-group col-md-12">
                              <label for="Address">Address Lookup</label>
                              <div class="input-group spcl-addon">  
                              <div class="input-group-addon"><i class="fa fa-search"></i></div>
                              <input class="form-control" style="margin-bottom:0px;" id="addresslookupBilling" name="billingContactInfo.addressLookUp" placeholder="Search" type="text">
                              </div>
                             
                           </div>
                        </div>
                     </div>
                     
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step2address"><spring:theme code="form.register.billing.addr" /></label><br/>
                              <input  class="form-control" id="step2address" name="billingContactInfo.addressLine1" placeholder="Building Number & Street"  type="text"/>
                           </div>
                           <div class="form-group col-md-4">
                              <label for="step2street"><spring:theme code="form.register.billing.additionalStreet" /></label><br/>
                              <input  class="form-control textonly" id="step2street" name="billingContactInfo.addressLine2"  type="text"/> 
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step2City"><spring:theme code="form.register.billing.city" /></label><br/>
                              <input  class="form-control textonly" id="step2City" name="billingContactInfo.city" placeholder="City"  type="text"/>
                           </div>
                           <div class="form-group col-md-2">
                              <label for="step2post"><spring:theme code="form.register.billing.postCode" /></label><br/>
                              <input  class="form-control post_not_man spcl-fld" autocomplete="no" id="step2post" name="billingContactInfo.postalCode" placeholder="SL16 8AA"  type="text"/>   
                           	  <div class="help-block"></div>  
                           </div>
                           <div class="form-group col-md-3">
                              <label for="step2country"><spring:theme code="form.register.billing.country" /></label><br/>
                              <input  class="form-control textonly" id="step2country" name="billingContactInfo.country" placeholder="Country"  type="text"/>   
                           </div>
                        </div>
                     </div>  
                  </div>
           
               
               <div class="col-xs-12 previosnmail">                  
                  <div class="prevStepbtn pagi-link sub-3" data-section-id="section-2" data-img-id="img-2">< <spring:theme code="form.register.prev" /></div>
                  <button type="button" class="step3 Nextbutton_without_validation pagi-link sub-5" data-section-id="section-4" data-img-id="img-3" id="billingNext"><spring:theme code="form.register.next" /></button>
                  <span class="skiptext"><a class="step3 skiplink pagi-link sub-5" href="javaScript:void(0);" data-section-id="section-4" data-img-id="img-2" id="billingSkipForNow"><spring:theme code="form.register.skip" /></a></span>
               </div>
            </div>
