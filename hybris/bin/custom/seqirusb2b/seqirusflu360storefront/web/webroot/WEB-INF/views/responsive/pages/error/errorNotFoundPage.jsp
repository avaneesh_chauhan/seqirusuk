<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}">

<cms:pageSlot position="SideContent" var="feature" element="div"
			class="col-xs-12 col-md-2 col-lg-2  sidebar hidden-xs hidden-sm visible-md-* visible-lg-*">
			<cms:component component="${feature}" />
		</cms:pageSlot>
<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">

<div class="col-xs-12 col-md-10 col-lg-10  main">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 LI_errorTextParent">
                    <div class="LI_errorText1">Something went wrong.</div>
                    <div class="LI_errorText2">Let's get you back on track.</div>
                    <div class="LI_errorbtnParent">
                        <button class="LI_errorReturnDashboardBtn">Return to Dashboard</button>
                        <button class="LI_errorViewBtn">View FAQs</button>
                    </div>
                    
                </div>
            </div>
    </sec:authorize>    
    <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">   
     
                    <!--Logged Out Error code starts -->
        <main role="main" class="loggedOutError col-xs-12 no-padding" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 LO_errorTextParent">
                <div class="LO_errorText1">Something went wrong.</div>
                <div class="LO_errorText2">Let's get you back on track.</div>
                <button class="LO_returnHomeBtn">Return to Home</button>
            </div>
        </main>
         </sec:authorize> 

        <!--Logged Out Error code ends -->

</template:page>


