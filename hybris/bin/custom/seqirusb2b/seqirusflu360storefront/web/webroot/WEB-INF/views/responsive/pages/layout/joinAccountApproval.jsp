<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<template:page pageTitle="${pageTitle}">
<div class="container-fluid maincontent col-xs-12">
<nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumbfont padLeft79">
            <c:url value="/" var="homeUrl" />
               <li class="breadcrumbs-item active"><a href="${homeUrl}" class=""><spring:theme
						code="form.register.homeLabel" /></a></li>
               <li class="breadcrumbs-item" aria-current="page">Access Your Account</li>
            </ol>
         </nav>

  <div class="pageHeader col-xs-12"><cms:pageSlot position="Section1DA" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot></div>
  <div class="contentArea col-xs-11">
    <div class="leftcontentArea col-xs-12 col-md-4">
      <div class="leftHeader col-xs-11">
        <cms:pageSlot position="Section2DA" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
      </div>
      <div class="leftsubHeader col-xs-11">
        <cms:pageSlot position="Section2DB" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
      </div>
    </div>
    <div class="rightcontentArea col-xs-12 col-md-8" id="rightSection_1">
      <div class="section1_helpSection col-xs-12"><span class="troubleText">Having Trouble? </span><span class="helpText"> <a href="${contextPath}/support" style="color: #DF323F;">Get Help</a></span></div>
      <div class="section1_header col-xs-12">
        <cms:pageSlot position="Section3DB" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
      </div> 
      <div class="section1_subheader col-xs-12">
       <cms:pageSlot position="Section3DC" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
      </div> 
      <!--<div class="col-xs-12 approvalHeader">
        <cms:pageSlot position="Section3DD" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
      </div>-->
      <div class="col-xs-12 approvalText">
        <cms:pageSlot position="Section3DE" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
      </div>
      <div class="col-xs-12 approvalButtons">
        <div class="fluadbtn"><a href="https://www.adjuvanted-tiv.co.uk/" target="_blank" style="color: #DF323F;">View Adjuvanted Trivalent Site</a></div>
        <div class="flucelvaxbtn"><a href="https://www.flucelvax-tetra.co.uk/" target="_blank" style="color: #DF323F;">View Flucelvax Tetra Site</a></div>
      </div>
    </div>
  </div>
</div>
</template:page>