<%-- <%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="userProfileInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userCompanyInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userAccountInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userLocationsInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>



<template:page pageTitle="${pageTitle}">
	<div class="post_login">

		<div class="container-fluid body-section">
			<div class="container">
				<nav aria-label="supportbreadcrumb">
					<ol class="supportbreadcrumb">
						<c:url value="/" var="homeUrl" />
						<li class="supportbreadcrumb-item active"><a
							href="${homeUrl}" class=""><spring:theme
									code="breadcrumb.home" /></a></li>
						<li class="supportbreadcrumb-item" aria-current="page"><spring:theme
								code="breadcrumb.profile" /></li>
					</ol>
				</nav>


				<div class="row">
					<div class="col-md-12">
						<div class="col-md-5 white-bg">
							<table class="table dashboard-heading-table">
								<tr>
									<td class="heading-txt">Michelle Lawson</td>
									<td class="lst-login"><span class="profile-lebel">Last
											Login</span><br />December 5, 2020</td>
								</tr>
							</table>

						</div>
					</div>
				</div>

				<div class="container dashboard-body">


					<userProfileInfo:userProfileInfo />

					<userCompanyInfo:userCompanyInfo />

					<userAccountInfo:userAccountInfo />

					<userLocationsInfo:userLocationsInfo />

					<div class="col-md-12 no-pad">
						<div class="blkfirst">
							<div class="col-md-12 no-pad">
								<cms:pageSlot position="Section1" var="feature">
									<span class="heading-txt">${feature.h2content}</span>
									<div
										class="col-md-12 white-background-profile white-background-profile-consent body-txt">

										${feature.paragraphcontent}
								</cms:pageSlot>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</template:page> --%>