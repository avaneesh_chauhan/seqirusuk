  <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
 
  <div class="header">
         <div class="header-nav">
            <ul class="login-links marginBottom">
             <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
               <%-- <li>
                  <a class="loginbutton" href="${contextPath}/signup">Notifications</a>
               </li> --%>
             	<li>
                  <a class="login-link" href="${contextPath}/profile">My Profile</a>
               </li>
               <li>
                  <a class="loginbutton" href="${contextPath}/logout">LOG OUT</a>
               </li>
             </sec:authorize>
             <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
             	 <li>
                  <a class="createbutton" href="${contextPath}/signup">${feature.signup}</a>
               </li>
               <li>
                  <a class="loginbutton" href="${contextPath}/login">${feature.login}</a>
               </li>
             </sec:authorize>
            </ul>
         </div>
      </div>