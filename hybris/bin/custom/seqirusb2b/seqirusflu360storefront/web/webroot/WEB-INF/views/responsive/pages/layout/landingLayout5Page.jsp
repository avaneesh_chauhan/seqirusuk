<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<template:page pageTitle="${pageTitle}">
<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
	  <main role="main" class="Support_container col-xs-12 " >
            <div class="Support_border"></div>
            <div class="container-fluid container-lg padding-L40">
                <nav class="nav_breadCrumb" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    <c:url value="/" var="homeUrl" />
                        <li class="breadcrumb-item"><a href="${homeUrl}"><spring:theme code="breadcrumb.home" /></a></li>
                        <li class="breadcrumb-item"><spring:theme code="breadcrumb.support" /></li>
                        <li class="breadcrumb-item"><a class="active" href="#"><spring:theme code="breadcrumb.contact"/></a></li>
                    </ol>
                </nav>
            
                <div class="Support_content  margin-T30">
                    <div class="global_Lshape">
                        <div class="global_horizontalL"></div>
                        <div class="global_verticalL"></div>                       
                    </div>
                    <cms:pageSlot position="ContactandConnectSlot" var="feature">
                    <div class="row">
                        <div class="Support_contentbox col-lg-6 col-md-6 col-sm-9 col-xs-11">
                            <h1 class="Support_header">${feature.title}</h1>
                            <div class="Support_text">${feature.content}</div>
                            <div class="redborder">
                                <a class="Support_faqButton" href="${feature.urlLink}">${feature.subcontent}</a>
                            </div>
                        </div>
                    </div>
                    </cms:pageSlot>
                </div>
            </div>
            <div class="Support_lowerboxs">
                <div class="container-fluid container-lg padding-L40">
                <cms:pageSlot position="Flu360SupportSlot" var="feature">
                    <div class="Support_flu360 col-lg-4 col-md-4 col-sm-12 col-xs-12 no-padding padding-R20">
                        <h4 class="Support_flu360header boxHeader">${feature.title}</h4>
                        <div class="Support_flu360Content boxContent">${feature.content}</div>
                        ${feature.subcontent}
                    </div>
                    </cms:pageSlot>
                    <cms:pageSlot position="AdverseEventSupportSlot" var="feature">
                    <div class="Support_adverseReporting col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <h4 class="Support_adverseReportingheader boxHeader">${feature.title}</h4>
                        <div class="Support_adverseReportingContent boxContent">${feature.content}</div>
                        ${feature.subcontent}
                    </div>
                    </cms:pageSlot>
                    <cms:pageSlot position="MedicalCommSupportSlot" var="feature">
                    <div class="Support_medicalCommunication col-lg-4 col-md-4 col-sm-12 col-xs-12 padding-R10">
                        <h4 class="Support_medicalCommunicationheader boxHeader">${feature.title}</h4>
                        <div class="Support_medicalCommunicationContent boxContent">${feature.content}</div>
                        ${feature.subcontent}
                    </div>
                    </cms:pageSlot>
                    <div class="clearboth"></div>
                </div>
            </div>
        </main>
		</sec:authorize>
		</template:page>