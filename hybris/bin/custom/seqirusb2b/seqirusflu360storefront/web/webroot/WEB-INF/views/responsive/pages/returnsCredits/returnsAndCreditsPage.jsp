<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="breadcrumb"
	tagdir="/WEB-INF/tags/responsive/nav/breadcrumb"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<template:page pageTitle="${pageTitle}">
	<%-- <div id="breadcrumb" style="padding: 10px 80px !important;margin-bottom: 0px !important;background-color: #F4F7FA !important;"/>
			<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}" />
		</div> --%>
	<div class="post_login">
		<div class="container-fluid body-returns">
			<div class="container">
				<nav aria-label="returnsbreadcrumb">
					<ol class="returnsbreadcrumb">
						<li class="returnsbreadcrumb-item active"><a href="${contextPath}/dashboard"
							class=""><spring:theme code="breadcrumb.home" text="Home" /></a></li>
						<li class="returnsbreadcrumb-item" aria-current="page"><spring:theme
								code="breadcrumb.financial" text="Financial" /></li>
						<li class="returnsbreadcrumb-item" aria-current="page"><spring:theme
								code="breadcrumb.returnsandcredits" text="Returns & Credits" /></li>
					</ol>
				</nav>
				<div class="row">
					<div class="col-md-12">
						<h2 class="returnsheader">
							<spring:theme code="returnsandcredits.header.text"
								text="Returns & Credits" />
						</h2>
					</div>
					<div class="col-md-12">
						<h2 class="returnssubheader1">
							<spring:theme code="returns.subheader1.text" text="Returns" />
						</h2>
						<h2 class="returnssubheader2">
							<spring:theme code="returns.subheader2.text"
								text="Current Returns" />
						</h2>
					</div>
				</div>
				<div class="row greywhitesec">
					<div class="col-md-12">
						<div class="returnsgreybg">
							<div class="greysectxt">
								<spring:theme code="returns.postseason.text"
									text="Post-season returns for 2020/2021" />
							</div>

						</div>
						<div class="returnswhitebg">

							<div>
								<a class="whitesecredtxt" href="${contextPath}/support"><spring:theme
										code="returns.whoiseligible.text"
										text="Who is eligible for Post-season returns?" /></a>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="returnssubheader2">
							<spring:theme code="returns.season.text" text="Spring 2021" />
						</div>
						<div class="returnssubheader3">
							<spring:theme code="returns.timeline.text"
								text="Rough estimate of timeline. Specific dates will be determined at a later date." />
						</div>
						<div class="seasonsection">
							<!-- <ul class="seasonlist">
                        <li class="tab seasonlisting">Invitation
                           <p>Invitations sent to eligible customers to facilitate return of unused products from 2020/2021 season</p>
                        </li>
                        <li class="seasonlisting">
                           Return Form
                           <p>Invitations sent to eligible customers to facilitate return of unused products from 2020/2021 season</p>
                        </li>
                        <li class="seasonlisting">
                           Review
                           <p>Invitations sent to eligible customers to facilitate return of unused products from 2020/2021 season</p>
                        </li>
                        
                     </ul>  -->
							<ul class="seasonlists">
								<li><spring:theme code="returns.invitiation.text"
										text="Invitation" />
									<p>
										<spring:theme code="returns.invitiationsent.text"
											text="Invitations sent to eligible customers once return period is open." />
									</p></li>
								<li><spring:theme code="returns.returnform.text"
										text="Return Form" />
									<p>
										<spring:theme code="returns.returnform.subtitle"
											text="Your organisation completes the returns form and submits this to Seqirus" />
									</p></li>
								<li><spring:theme code="returns.review.text" text="Review" />
									<p>
										<spring:theme code="returns.review.subtitle"
											text="Seqirus will review and, once approved, process the return credit to your account." />
									</p></li>
							</ul>
						</div>
					</div>
					<div class="col-md-6">
						<div class="returnsheader2">
							<spring:theme code="returns.unusedproduct.text"
								text="Unused Product Returns (End of Season)" />
						</div>
						<div
							class="returns-detailssection returns-seasontxt col-xs-12 col-md-12">
							<cms:pageSlot position="RightSlotForReturn" var="feature"
								element="div"
								class="account-section-content col-xs-12 col-md-12">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</div>
				</div>
				<div class="row returnsmartop">
					<div class="col-md-12">
						<h2 class="returnssubheader1">
							<spring:theme code="credits.header.text" text="Credits" />
						</h2>
						<div>
							<div class="returnssubheader2">
								<spring:theme code="credits.subheader.text"
									text="Credits History" />
								<span><a class="returnsredtxt" href="${contextPath}/support"><spring:theme
											code="credits.howtoapply.text"
											text="How do I apply a credit?" /></a></span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 returnsBody">
						<div class="col-md-6 credittextmar">
							<div class="returnssubheader5">
								<spring:theme code="credits.account.title"
									text="Customer Account" />
							</div>
							<c:choose>
								<c:when test="${not empty creditsData}">
									<div class="returnssubheader1">${creditsData.accountName}</div>
								</c:when>
								<c:otherwise>
									<div class="returnssubheader1"></div>
								</c:otherwise>
							</c:choose>
						</div>
						<div class="col-md-6 credittextmar">
							<div class="returnssubheader5">
								<spring:theme code="credits.availability.title"
									text="Available Credits" />
							</div>
							<div class="returnssubheader1">
								<c:if test="${not empty creditsData}">
                           	�${creditsData.totalAvailableCredit}
                           </c:if>
							</div>
						</div>
						<!-- Table starts -->
						<div class="returnstablecontainer returnsTablecontainer">
							<table id="returnsTable" class="display">
								<thead>
									<tr>
										<th><spring:theme code="credits.table.heading1"
												text="Credit Note #" /></th>
										<th><spring:theme code="credits.table.heading2"
												text="Credit Amount" /></th>
										<th><spring:theme code="credits.table.heading3"
												text="Date Issued" /></th>
										<th><spring:theme code="credits.table.heading4"
												text="Status" /></th>
									</tr>
								</thead>
							</table>
						</div>
						<!-- Table Ends -->
					</div>
				</div>

			</div>
		</div>
	</div>
</template:page>
