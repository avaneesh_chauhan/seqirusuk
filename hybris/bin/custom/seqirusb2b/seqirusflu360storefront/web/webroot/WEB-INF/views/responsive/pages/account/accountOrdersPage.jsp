<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

 <div class="row dashboard-rightheadersection">
	<div class="col-xs-6 col-md-6 dashboard-orderheades no-padding">
		<span>Orders</span>
	</div>
	<div class="col-xs-6 col-md-6 dashboard-viewall">
		<a href="#" class="dashboard-viewall">View All Orders <i
			class="global_blackarrow"></i></a>
	</div>
</div>
 <c:forEach items="${orders}" var="activeOrder" varStatus="activeOrderLoop">
<c:set var="count" value="${activeOrderLoop.index}"/>
<c:if test="${count <= 3 }" >
<div class="row dashboard-ordersborder">
	<div class="col-xs-6 col-md-6 no-padding">
		<div class="dashboard-orderdetials-subheader">SEASON ${presentSeason}</div>
		<div class="dashboard-orderdetials-header">Order #${activeOrder.orderID}</div>
		<div class="row">
			<div class="col-xs-6 col-md-6">
				<div class="dashboard-orderdetials-text">Unit Qty Total:</div>
				<div class="dashboard-orderdetials-text">Shipping Location:</div>
			</div>
			<div class="col-xs-6 col-md-6">
				<div class="dashboard-orderdetials-text">${activeOrder.totalFluad+activeOrder.totalAfluria+activeOrder.totalFlucelvax}</div>
				<div class="dashboard-orderdetials-text">${activeOrder.address.addressLine1},&nbsp;${activeOrder.address.addressLine2}</br>${activeOrder.address.city}&nbsp;${activeOrder.address.state}&nbsp;${activeOrder.address.zipCode}
					Chicago IL 93456</div>
			</div>
		</div>
	</div>
	<div class="col-xs-6 col-md-6">
		<div class="dashboard-orderdetials-status">${activeOrder.status}</div>
	</div>
</div>
</c:if>
 </c:forEach> 

