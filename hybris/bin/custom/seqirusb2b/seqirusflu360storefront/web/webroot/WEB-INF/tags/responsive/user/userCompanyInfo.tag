<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div class="blklast ">
                    <div class="heading-txt"><spring:theme code="edit.profile.company.myCompany" /> | <span class="heading-txt-bold">${customerData.companyName}</span></div> 
                    <div class="col-md-12 white-background-profile">
                        <div class="col-md-12 no-pad">
                            <table class="table dashboard-heading-table">
                                 <tr>
                                    <td> 
                                    <span class="status_active"><c:choose><c:when test="${customerData.businessType ne null}"><spring:theme code="edit.profile.company.completed" /></c:when><c:otherwise><spring:theme code="edit.profile.company.pending" /></c:otherwise></c:choose></span></td>
                                    <td class="profile-heading-width">
                                     <c:if test="${customerData.status eq 'Completed'}">
                                    <a href="javascript:void(0)" class="view_order_link edit-company"><spring:theme code="edit.profile.company.editInfo" /></a></c:if></td>
                                </tr>
                            </table>
                        </div>
                        
                   <div class="company-details">
                   <div class="row">
                        <div class="col-md-3 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.compReg" /></div>
                            <div class="profile-field" id="company_reg">${customerData.companyRegNumber}</div>
                        </div>
                        <div class="col-md-3 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.businessType" /></div>
                            <div class="profile-field" id="company_business_type">${customerData.businessType}</div>
                        </div>
						
                        <div class="col-md-3 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.nhscode" /></div>
                            <div class="profile-field" id="company_nhs_num">${customerData.nhcNumber}</div>
                        </div>
                        <div class="col-md-3 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.vat" /></div>
                            <div class="profile-field" id="company_vat">${customerData.vatNumber}</div>
                        </div>
					</div>	
					<div class="row">                       
                        <div class="col-md-3 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.mainContact" /></div>
                            <div class="profile-field"><span id="company_main_contact">${customerData.firstName} ${customerData.lastName}</span><br/></div>
                        </div>
                        
                        <div class="col-md-3 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.jobTitle" /></div>
                            <div class="profile-field"><span id="company_job_title">${customerData.jobTitle}</span></div>
                        </div>
                        
                        <div class="col-md-6 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.address" /></div>
                            <div class="profile-field"><span id="company_address">${customerData.buildingStreet}<br/><c:if test="${null!=customerData.additionalStreet}"> ${customerData.additionalStreet},</c:if> ${customerData.city}</span><br/><span id="company_zip"><c:if test="${null ne customerData.postCode}"> ${customerData.postCode},</c:if> ${customerData.country.name}</span></div>
                        </div>
                        </div>
                        <div class="row">
<div class="col-md-6 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.contactInfo" /></div>
                            <div class="profile-field"><span id="company_address">${customerData.email}</span><br/><span id="company_zip">${customerData.phoneNumber} ext. ${customerData.phoneExt}</span></div>
                        </div>
                     </div>
                     </div>

                <form:form role="form" modelAttribute="customerRegistrationForm" id="edit_mycompany" data-toggle="validator" method="POST">
              
                     <div class="edit-company-details shippingformContent-profile greycolor">
                        <div class="col-md-4 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.compReg" /></div>
                            <div class="profile-field"><input type="text" readonly name="companyRegNumber" id="edit_company_reg" value="${customerData.companyRegNumber}" placeholder=""  ></div>
                            
                        </div>

                        <div class="col-md-4 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.businessType" /></div>
                            <div class="profile-field"><input type="text" readonly name="businessType" id="edit_company_business_type" value="${customerData.businessType}" placeholder=""></div>
                        </div>

                        <%-- <div class="col-md-4 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company." />Other</div>
                            <div class="profile-field"><input type="text" readonly name="businessType" id="edit_company_other" value="${customerData.businessType}" placeholder=""></div>
                        </div> --%>
                        <div class="col-md-4 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.nhscode" /></div>
                            <div class="profile-field"><input type="text" readonly name="nhcNumber" id="edit_company_vat" value="${customerData.vatNumber}" placeholder=""></div>
                        </div>
                        <div class="col-md-4 profile-field-margin">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.vat" /></div>
                            <div class="profile-field"><input type="text" readonly name="vatNumber" id="edit_company_vat" value="${customerData.vatNumber}" placeholder=""></div>
                        </div>
                        <div class="col-md-12"></div>
                        <div class="row">
                        <div class="col-md-6 profile-field-margin form-group">
                        
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.address" /><span class="asteriskred">*</span></div>
                            <div class="profile-field">
                            <input type="text" class="rqrd_edit_fld_company" autocomplete='no' data-error="Please enter Address" name="buildingStreet" id="edit_company_address" value="${customerData.buildingStreet}" placeholder="" required>
                            <div class="help-block with-errors"></div>
                            </div>
                          
                            
                        </div>
                        <div class="col-md-6 profile-field-margin form-group">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.additionalStreet" /></div>
                            <div class="profile-field"><input type="text" autocomplete='no'  name="additionalStreet" id="edit_company_address2" value="${customerData.additionalStreet}" placeholder="" >
                            
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-md-4 profile-field-margin form-group">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.city" /><span class="asteriskred">*</span></div>
                            <div class="profile-field"><input type="text" autocomplete='no' class="rqrd_edit_fld_company" data-error="Please enter City" name="city" id="edit_company_city" value="${customerData.city}" placeholder="" required>
                            <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        
                        <div class="col-md-4 profile-field-margin form-group">
                           <div class="profile-lebel"><spring:theme code="edit.profile.company.zipcode" /><span class="asteriskred">*</span></div>
                           <div class="profile-field"><input type="text" autocomplete='no' class="rqrd_edit_fld_company" data-error="Please enter zipcode" name="postalCode" id="edit_company_zip" value="${customerData.postCode}" placeholder="" required>
                           <div class="help-block with-errors postcodeerror_edit"></div>
                           </div>
                       </div>
                        <div class="col-md-4 profile-field-margin form-group">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.country" /><span class="asteriskred">*</span></div>
                            <div class="profile-field"><input type="text" autocomplete='no' class="rqrd_edit_fld_company" data-error="Please enter country" name="country" id="edit_company_country" value="${customerData.country.name}" placeholder="" required>
                            <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-md-6 profile-field-margin form-group">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.mainContact" /><span class="asteriskred">*</span></div>
                            <div class="profile-field"><input type="text" autocomplete='no' class="rqrd_edit_fld_company" data-error="Please enter Main Contact" name="BussinessfirstName" id="edit_company_contact" value="${customerData.firstName} ${customerData.lastName}" placeholder="" required>
                            <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-md-6 profile-field-margin form-group">
                            <div class="profile-lebel"><spring:theme code="edit.profile.company.jobTitle" /><span class="asteriskred">*</span></div>
                            <div class="profile-field"><input type="text" autocomplete='no' class="rqrd_edit_fld_company" data-error="Please enter Job title" name="jobTitle" id="edit_job_title" value="${customerData.jobTitle}" placeholder="" required>
                            <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        </div>

                       <div class="col-md-6 profile-field-margin form-group">
                           <div class="profile-lebel"><spring:theme code="edit.profile.company.email" /><span class="asteriskred">*</span></div>
                           <div class="profile-field"><input type="email" autocomplete='no' class="rqrd_edit_fld_company" data-error="Please enter valid Email" pattern=".*@[\w.-]+\.[a-z0-9.-]{2,6}" name="email" id="edit_company_email_contact" value="${customerData.email}" placeholder="" required>
                           <div class="help-block with-errors"></div>
                           </div>
                       </div>
						<div style="clear:both"></div>
                       <div class="col-md-4 profile-field-margin form-group">
                        <div class="profile-lebel"><spring:theme code="edit.profile.company.phone" /><span class="asteriskred">*</span></div>
                        <div class="profile-field"><input type="text" autocomplete='no' class="rqrd_edit_fld_company numberonly" data-error="Please enter Phone Number" name="phoneNumber" class="phonemask" id="edit_company_phone_contact" value="${customerData.phoneNumber}" placeholder="" maxlength="11" required>
                        <div class="help-block with-errors" id="message_phn"></div>
                        </div>
                       </div>
                       <div class="col-md-3 profile-field-margin form-group">
                        <div class="profile-lebel"><spring:theme code="edit.profile.company.ext" /></div>
                        <div class="profile-field"><input class="numberonly" autocomplete='no' type="text" name="phoneExt" maxlength="4" id="edit_company_phone_ext" value="${customerData.phoneExt}" placeholder="" >
                        
                        </div>
                       </div>
                       <div style="clear:both"></div>
                       <div class="row">
                        <input type="hidden" name="updateCompanyInfoFlag" id="companyInfoFlag" value="">
                       <div class="col-xs-12 col-md-2"></div>
                        <div class="col-xs-12 col-md-4 middle-align">
                        <a href="javascript:void(0)" class="cancel-btn close-company-edit"><spring:theme code="edit.profile.company.cancel" /></a>
                        </div>
                        <div class="col-xs-12 col-md-6 middle-align">
                         <button type="submit" class="Nextbutton-profile update-company-info"><spring:theme code="edit.profile.company.update" /> </button> 
                        </div>
                       </div> 
                       
                     </div>
                </form:form>
                     

                      </div>
          				
                      <div class="col-md-12 marBottom40" style="background: #fff;" >
                      <div class="col-md-7"></div>
                      <div class="col-md-5 no-pad">
                          <div class="profile-progress">
                              <div class="profile-field margin-top-field"><c:choose><c:when test="${customerData.businessType ne null}">100% Complete</c:when><c:otherwise>0% Complete</c:otherwise> </c:choose></div>
                             <c:choose>
                            
                              <c:when test="${customerData.businessType ne null}">
                        <div class="progress-circle over50 p100">
                        </c:when>
                        <c:otherwise>
                            <div class="progress-circle p0">
                        </c:otherwise>
                              </c:choose>
                                  <div class="left-half-clipper">
                                     <div class="first50-bar"></div>
                                     <div class="value-bar"></div>
                                  </div>
                              </div>
                          </div>
   
                      </div>
                     </div>

                </div>

<div class="clearfix1"></div>