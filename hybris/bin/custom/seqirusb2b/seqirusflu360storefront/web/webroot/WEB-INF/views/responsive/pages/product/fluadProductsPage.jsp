<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<template:page pageTitle="${pageTitle}">
<div id="fluad">
	<div class="hero--prod" style="background-image: url('../_ui/responsive/theme-lambda/images/fluad-hero.png')">
		<div class="breadcrumbs--page-nav">
			<div class="container">
				<p>
					<a href="#">Home</a><span> > </span>
					<a href="#">Products</a><span> > </span>
					<strong>FLUAD QUADRIVALENT</strong>
				</p>
			</div>
		</div>	<div class="container">
			<div class="hero--prod__body">
				<div class="hero--prod__logo">
					<img src="../_ui/responsive/theme-lambda/images/fluad-hero-logo.png" alt="product logo">
				</div>
				<div class="hero--prod__info">
					<h1 class="hero--prod__header header--2">Choose an adjuvanted vaccine for the prevention of seasonal influenza</h1>
						<p class="hero--prod__subheader">FLUAD� QUADRIVALENT is the first-and-only adjuvanted quadrivalent seasonal influenza vaccine<sup>1</sup></p>
					<p class="hero--prod__approval hero--prod__approval-green"><span>APPROVED FOR PATIENTS</span><span>65+ YEARS<sup>1</sup></span></p>
					<p class="hero--prod__content">155 million doses distributed over 20+ years*<sup>2</sup></p>
					<a class="hero--prod__link hero--prod__link-blue" href="#">Order Today</a>
						<p class="hero--prod__info-disclaimer">*Doses distributed globally as of September 2020 and includes both FLUAD and FLUAD QUADRIVALENT.</p>
				</div>
			</div>
		</div>
		<div class="hero--prod__footer">
			<div class="container">
					<div class="hero--prod__cpt-content">
						<p class="hero--prod__footer-header">Reimbursed through CPT code: </p>
							<p class="hero--prod__cpt-code"><span class="hero--prod__cpt-code-green">90694</span></p>
					</div>
					<p class="hero--prod__cpt-subtitle">Covered by Medicare Part B and by most Medicare Advantage Plans with no copay<sup>+3</sup></p>
					<p class="hero--prod__cpt-disclaimer"><sup>+</sup>This information does not constitute a guarantee or warranty of coverage benefits or reimbursement</p>
			</div>
		</div>
	</div>	<div class="prod-sbs">
		<div class="container">
			<div class="prod-sbs__left">
				<h3 class="prod-sbs__header header--3">Adding MF59� Adjuvant to an influenza vaccine is designed to strengthen, broaden, and lengthen the duration of the immune response.<sup>4-6</sup></h3>
				<a href="#" class="prod-sbs__link text--grey-110 cta"><span>Learn more about MF59� Adjuvant technology</span><img src="../_ui/responsive/theme-lambda/images/arrow-right.svg"></a>
				<div class="prod-sbs__key-features">
					<span class="prod-sbs__key-features-header">Key Features</span>
					<ul class="prod-sbs__key-features-list">
							<li>Made with MF59� Adjuvant<sup>1</sup></li>
							<li>Preservative free<sup>1</sup></li>
							<li>For intramuscular injection<sup>1</sup></li>
							<li>0.5-mL pre-filled syringe<sup>1</sup></li>
							<li>10 syringes per carton<sup>1</sup></li>
							<li>Syringe, plunger, and tip cap are not made from natural rubber latex<sup>1</sup></li>
					</ul>
				</div>
			</div>
			<div class="prod-sbs__right">
				<p class="prod-sbs__img"><img alt="product image" src="../_ui/responsive/theme-lambda/images/fluad-box.png"><span class="prod-sbs__fpo-watermark">FPO</span></p>
			</div>
		</div>
	</div>	<section class="prod-tabs">
		<div class="container">
		    <div class="prod-tabs__tab-nav">
		        <ul>
						<li class="active">
							<a data-tab="#tab-0" href="">Clinical Data
								<div class="prod-tabs__arrow">
									<img role="presentation" src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
									<span class="sr-text">Click to expand or contract content</span>
								</div>
							</a>
						</li>
						<li class="">
							<a data-tab="#tab-1" href="">Safety Data
								<div class="prod-tabs__arrow">
									<img role="presentation" src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
									<span class="sr-text">Click to expand or contract content</span>
								</div>
							</a>
						</li>
						<li class="">
							<a data-tab="#tab-2" href="">Dosing and Administration
								<div class="prod-tabs__arrow">
									<img role="presentation" src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
									<span class="sr-text">Click to expand or contract content</span>
								</div>
							</a>
						</li>
						<li class="">
							<a data-tab="#tab-3" href="">Storage and Handling
								<div class="prod-tabs__arrow">
									<img role="presentation" src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
									<span class="sr-text">Click to expand or contract content</span>
								</div>
							</a>
						</li>
		        </ul>
		    </div>
		    <div class="prod-tabs__tab-content">
					<div class="prod-tabs__tab active" id="tab-0">
						<div class="prod-tabs__header">
							<h4>
								<a href="">
									Clinical Data
									<div class="up-down">
										<img role="presentation" src="../_ui/responsive/theme-lambda/images/chevron-up.svg">
										<span class="sr-text">Click to expand or contract content</span>
									</div>
								</a>
							</h4>
						</div>
						<div class="prod-tabs__body">
							<p>FLUAD QUADRIVALENT with MF59� Adjuvant produces robust immune responses.</p><p>Compared to FLUAD, FLUAD QUADRIVALENT includes an additional B strain to help prevent disease caused by all 4 influenza strains represented in the vaccine.<sup>1</sup></p><p class='centered-text'><img src='../_ui/responsive/theme-lambda/images/fluad-sales-aid.png' alt='fluad sales aid chart'></p><p>For the non-influenza comparator vaccine, the proportion of subjects with HI titers greater than or equal to 1:40 at Day 21 were 46.7% for the A/H1N1 strain, 41.7% for A/H3N2, 21.5% for B/Yamagata, and 18.4% for B/Victoria. The seroconversion rates for the non-influenza comparator vaccine were 2.1% for A/H1N1, 3.9% for A/H3N2, 3.6% for B/Yamagata, and 2.1% for B/Victoria.</p><p>Study 1 evaluated the immunogenicity of FLUAD QUADRIVALENT in a randomized, observer-blind, non-influenza comparator-controlled, multicenter efficacy study. Adult subjects 65 years of age and older received 1 dose of either FLUAD QUADRIVALENT (N=3379) or a US-licensed non-influenza comparator vaccine (N=3382).</p><div class='prod-tabs__references'><p><sup>a</sup>Success criterion: lower bound of the 95% CI for the % of subjects with HI titer &ge;1:40 must be &ge;60%</p><p><sup>b</sup>Seroconversion is defined as a prevaccination HI titer &lt;1:10 and postvaccination HI titer &ge;1:40 or at least a 4-fold increase in HI from prevaccination HI titer &ge;1:10. Success criterion: lower bound of the 95% CI for the seroconversion rate must be &ge;30%.</p><p><sup>c</sup>Non-influenza comparator vaccine=combined tetanus toxoid, reduced diphtheria toxoid, and acellular pertussis vaccine, Boostrix� (GlaxoSmithKline Biologicals)</p></div>
						</div>
					</div>
					<div class="prod-tabs__tab" id="tab-1">
						<div class="prod-tabs__header">
							<h4>
								<a href="">
									Safety Data
									<div class="up-down">
										<img role="presentation" src="../_ui/responsive/theme-lambda/images/chevron-up.svg">
										<span class="sr-text">Click to expand or contract content</span>
									</div>
								</a>
							</h4>
						</div>
						<div class="prod-tabs__body">
							<p>The safety of FLUAD QUADRIVALENT was evaluated in 2 multicenter, randomized controlled trials in 4269 adults 65 years and older.<sup>1</sup></p><p>In Study 1, most common (&ge;10%) local and systemic adverse reactions were observed within 7 days of vaccination with FLUAD QUADRIVALENT or a non-influenza comparator vaccine.<sup>1</sup></p><p class='centered-text'><img src='../_ui/responsive/theme-lambda/images/fluad-safety-data-chart.png' alt='fluad sales aid chart'></p><div class='prod-tabs__references'><p>N=number of subjects with solicited safety data</p><p><sup>a</sup>Solicited safety population: all subjects in the exposed population who received a study vaccine and provided postvaccination solicited safety data. <sup>b</sup>Severe reactions of each type were reported in 1.1% or fewer subjects receiving FLUAD QUADRIVALENT; severe reactions of each type were also reported in the comparator group at similar percentages. Severe definitions: erythema, induration, and ecchymosis=>100 mm diameter; injection-site pain, nausea, fatigue, myalgia, arthralgia, headache, and chills=prevents daily activity; loss of appetite=not eating at all; vomiting=6 or more times in 24 hours or requires intravenous hydration; diarrhea=6 or more loose stools in 24 hours or requires intravenous hydration; fever=&ge;102.2 �F (39 �C)</p></div><p>In Study 2, FLUAD QUADRIVALENT demonstrated a similar safety profile to that of FLUAD.<sup>1</sup></p><p>Solicited local and systemic adverse reactions reported were similar to those reported for Study 1.</p>
						</div>
					</div>
					<div class="prod-tabs__tab" id="tab-2">
						<div class="prod-tabs__header">
							<h4>
								<a href="">
									Dosing and Administration
									<div class="up-down">
										<img role="presentation" src="../_ui/responsive/theme-lambda/images/chevron-up.svg">
										<span class="sr-text">Click to expand or contract content</span>
									</div>
								</a>
							</h4>
						</div>
						<div class="prod-tabs__body">
							<p>For intramuscular injection only.<sup>1</sup></p><p>FLUAD QUADRIVALENT is supplied as a package of ten 0.5-mL prefilled needleless syringes.<sup>1</sup></p><p>Administer FLUAD QUADRIVALENT as a single 0.5-mL intramuscular injection in adults 65 years of age and older.<sup>1</sup></p><p>Each 0.5-mL dose of FLUAD QUADRIVALENT does not contain a preservative. The syringe, plunger, and tip cap are not made with natural rubber latex.<sup>1</sup></p>
						</div>
					</div>
					<div class="prod-tabs__tab" id="tab-3">
						<div class="prod-tabs__header">
							<h4>
								<a href="">
									Storage and Handling
									<div class="up-down">
										<img role="presentation" src="../_ui/responsive/theme-lambda/images/chevron-up.svg">
										<span class="sr-text">Click to expand or contract content</span>
									</div>
								</a>
							</h4>
						</div>
						<div class="prod-tabs__body">
							<p>Store FLUAD QUADRIVALENT at 2�C to 8�C (36�F to 46�F). Protect from light. Do not freeze. Discard if the vaccine has been frozen. Do not use after expiration date.<sup>1</sup></p>
						</div>
					</div>
			</div>
		</div>
	</section>	<section class="prod-sb" style="background-color: #4ECFF8;">
		<div class="container prod-sb__body">
			<div class="prod-sb__left">
				<img class="prod-sb__img" alt="product logo" src="../_ui/responsive/theme-lambda/images/fluad-logo-white.svg">
			</div>
			<div class="prod-sb__right prod-sb__right--dark">
				<h2>Choose FLUAD QUADRIVALENT with MF59� Adjuvant for your eligible patients 65+ years<sup>1</sup></h2>
				<a class="prod-sb__link" href="#">Order Now</a>
			</div>
		</div>
	</section>	<div class="resources">
		<div class="container">
			<div class="row-flex carousel-container-row">
				 <div class="row-flex access-financial-resources-header--row">
					 <div class="col-flex-sm-12 header--container">
						<h2>Featured Resources<div class="header-line"></div></h2>
					</div>
			   </div>
			</div>
	
			<!-- 5. CLINICAL RESOURCES CARDS STARTS  DESKTOP-->
			<div id="clinical-resources-financial" class="row-flex carousel--three-card resources-card--row">
					<div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12 cards-pl-0-mobile">
						<div class="resources--card">
							<img src="../_ui/responsive/theme-lambda/images/fluad-patient-brochure.png">
							<div class="resources--card-container">
								<p class="resources--card-eyebrow">FLUAD QUADRIVALENT | CLINICAL</p>
								<p class="resources--card-title">FLUAD� QUADRIVALENT Patient Brochure</p>
								<p class="resources--card-paragraph">Educate patients about vaccination with FLUAD QUADRIVALENT (Influenza Vaccine, Adjuvanted).</p>  
							</div>
	
							<button class="resources--card-btn">
							<span>Access Resource</span> <img src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
							</button>
						</div>
					</div>
					<div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12 cards-pl-0-mobile">
						<div class="resources--card">
							<img src="../_ui/responsive/theme-lambda/images/fluad-flashcard.png">
							<div class="resources--card-container">
								<p class="resources--card-eyebrow">FLUAD QUADRIVALENT | CLINICAL</p>
								<p class="resources--card-title">FLUAD� QUADRIVALENT Informational Flashcard</p>
								<p class="resources--card-paragraph">Informative flashcard that details why FLUAD QUADRIVALENT (Influenza Vaccine, Adjuvanted), an MF59� adjuvanted flu vaccine, is an appropriate option for patients 65+.</p>  
							</div>
	
							<button class="resources--card-btn">
							<span>Access Resource</span> <img src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
							</button>
						</div>
					</div>
					<div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12 cards-pl-0-mobile">
						<div class="resources--card">
							<img src="../_ui/responsive/theme-lambda/images/fluad-coding-billing.png">
							<div class="resources--card-container">
								<p class="resources--card-eyebrow">FLUAD QUADRIVALENT | FINANCIAL</p>
								<p class="resources--card-title">FLUAD� QUADRIVALENT Coding and Billing Guide</p>
								<p class="resources--card-paragraph">Quick reference guide for coding and billing information specific to FLUAD QUADRIVALENT (Influenza Vaccine, Adjuvanted).</p>  
							</div>
	
							<button class="resources--card-btn">
							<span>Access Resource</span> <img src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
							</button>
						</div>
					</div>
			</div>
	
			<!-- 5. CLINICAL RESOURCES CARDS ENDS  -->
			<!-- VIEW ALL RESOURCES ROW STARTS -->
			<div class="row-flex ">
				<div class="col-flex-xs-12 text-center view-resources--row">
					 <button class="button--view-resources-outline-grey">
								View All Resources
							</button>
				</div>
	
			</div>
			 <!--  VIEW ALL RESOURCES ROW ENDS -->
		</div>
	</div>	<!-- begin 50 percent speedbump -->
	<section class="bg--grey-100 speedbump speedbump--50">
		<div class="container">
			<div class="speedbump__content">
				<h3 class="header--3">Influenza can have a devastating impact on adults 65 years and older<sup>7</sup></h3>
					<a href="#" class="text--white cta">Learn more about the burden of influenza among this age group<img src="../_ui/responsive/theme-lambda/images/arrow-right-white.svg">
					</a>
			</div>
		</div>
	</section>
	<!-- end 50 percent speedbump -->	<div class="prod-portfolio">
		<div class="container">
			<h2 class="prod-portfolio__heading">Explore Other Flu Vaccines in the Seqirus Portfolio<div class="header-line"></div></h2>
			<div class="prod-portfolio__body">
					<div class="prod-portfolio__card">
						<img src="../_ui/responsive/theme-lambda/images/flucelvax-logo.svg">
						<a href="${contextPath}/products/flucelvax" class="no-underline">
							<p class="text-dark-gray cta ml-20">Explore <img src="../_ui/responsive/theme-lambda/images/arrow-right.svg"></p>
						</a>
					</div>
					<div class="prod-portfolio__card">
						<img src="../_ui/responsive/theme-lambda/images/affluria-logo.svg">
						<a href="${contextPath}/products/afluria" class="no-underline">
							<p class="text-dark-gray cta ml-20">Explore <img src="../_ui/responsive/theme-lambda/images/arrow-right.svg"></p>
						</a>
					</div>
			</div>
			<div class="prod-portfolio__disclaimer">
				<p class="disclaimer--paragraph">Please see Important Safety Information and full US Prescribing Information on each vaccine&#x27;s respective product page.</p>
			</div>
		</div>
	</div>	<div id="safetyInfoAnchor"></div>
		<section id="safetyInfo" class="safety-info sbs is-sticky">
			<div class="container">
				<div class="safety-info__header">
					<button class="safety-info__btn">MORE <img role="presentation" src="../_ui/responsive/theme-lambda/images/plus.svg"></button>
				</div>
				<div class="sbs__body">
					<div class="sbs__left">
						<p><strong>IMPORTANT SAFETY INFORMATION for FLUAD� (Influenza Vaccine, Adjuvanted), FLUAD� QUADRIVALENT (Influenza Vaccine, Adjuvanted), AFLURIA� QUADRIVALENT (Influenza Vaccine), and FLUCELVAX� QUADRIVALENT (Influenza Vaccine)</strong></p>
	
						<p><strong>CONTRAINDICATIONS</strong></p>
	
						<p>Do not administer FLUAD, FLUAD QUADRIVALENT, or AFLURIA QUADRIVALENT to anyone with a history of severe allergic reaction (e.g. anaphylaxis) to any component of the vaccine, including egg protein, or to a previous influenza vaccine. Do not administer FLUCELVAX QUADRIVALENT to anyone with a history of severe allergic reactions (e.g. anaphylaxis) to any component of the vaccine.</p>
	
						<p><strong>WARNINGS AND PRECAUTIONS</strong></p>
	
						<p>If Guillain-Barr� syndrome (GBS) has occurred within 6 weeks of receipt of prior influenza vaccine, the decision to give FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT or FLUCELVAX QUADRIVALENT should be based on careful consideration of the potential benefits and risks.</p>
						
						<p>Appropriate medical treatment and supervision must be available to manage possible anaphylactic reactions following administration of the vaccine.</p>
						
						<p>Syncope (fainting) may occur in association with administration of injectable vaccines including FLUAD, FLUAD QUADRIVALENT, and FLUCELVAX QUADRIVALENT. Syncope can be accompanied by transient neurological signs such as visual disturbance, paresthesia, and tonic-clonic limb movements. Ensure procedures are in place to avoid falling injury and to restore cerebral perfusion following syncope by maintaining a supine or Trendelenburg position.</p>
						
						<p>The immune response to FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT and FLUCELVAX QUADRIVALENT in immunocompromised persons, including individuals receiving immunosuppressive therapy, may be lower than in immunocompetent individuals.</p>
						
						<p>Vaccination with FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT and FLUCELVAX QUADRIVALENT may not protect all vaccine recipients against influenza disease.</p>
	
						<p><strong>ADVERSE REACTIONS</strong></p>
	
						<p>FLUAD:</p>
	
						<p>The most common (&ge; 10%) local (injection site) adverse reactions observed in clinical studies with FLUAD were injection site pain (25%) and tenderness (21%). The most common (&ge; 10%) systemic adverse reactions observed in clinical studies with FLUAD were myalgia (15%), headache (13%) and fatigue (13%).</p>
	
						<p>FLUAD QUADRIVALENT:</p>
	
						<p>The most common (&ge; 10%) local and systemic reactions with FLUAD QUADRIVALENT in elderly subjects 65 years of age and older were injection site pain (16.3%), headache (10.8%) and fatigue (10.5%).</p>
	
						<p>AFLURIA QUADRIVALENT:</p>
	
						<p>AFLURIA QUADRIVALENT administered by needle and syringe:</p>
	
						<p>In adults 18 through 64 years, the most commonly reported injection-site adverse reaction was pain (&ge; 40%). The most common systemic adverse events were myalgia and headache (&ge; 20%).</p>
	
						<p>In adults 65 years of age and older, the most commonly reported injection-site adverse reaction was pain (&ge; 20%).  The most common systemic adverse event was myalgia (&ge; 10%).</p>
	
						<p>In children 5 through 8 years, the most commonly reported injection-site adverse reactions were pain (&ge; 50%), redness and swelling (&ge; 10%).  The most common systemic adverse event was headache (&ge; 10%).</p>
	
						<p>In children 9 through 17 years, the most commonly reported injection-site adverse reactions were pain (&ge; 50%), redness and swelling (&ge; 10%).  The most common systemic adverse events were headache, myalgia, and malaise and fatigue (&ge; 10%).</p>
	
						<p>In children 6 months through 35 months of age, the most commonly reported injection-site reactions were pain and redness (&ge; 20%). The most common systemic adverse events were irritability (&ge; 30%), diarrhea and loss of appetite (&ge; 20%).</p> 
	
						<p>In children 36 through 59 months of age, the most commonly reported injection site reactions were pain (&ge; 30%) and redness (&ge; 20%).  The most commonly reported systemic adverse events were malaise and fatigue, and diarrhea (&ge; 10%).</p>
	
						<p>The safety experience with AFLURIA (trivalent formulation) is relevant to AFLURIA QUADRIVALENT because both vaccines are manufactured using the same process and have overlapping compositions:</p>
	
						<p>In adults 18 through 64 years of age, the most commonly reported injection-site adverse reactions with AFLURIA (trivalent formulation) when administered by the PharmaJet Stratis Needle-Free Injection System were tenderness (&ge; 80%), swelling, pain, redness (&ge; 60%), itching (&ge; 20%) and bruising (&ge; 10%). The most common systemic adverse events were myalgia, malaise (&ge; 30%), and headache (&ge; 20%).</p>
	
						<p>FLUCELVAX QUADRIVALENT:</p>
	
						<p>In adults 18 through 64 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were pain (&ge; 40%), erythema and induration (&ge; 10%). The most common systemic adverse events were headache, fatigue and myalgia (&ge; 10%).</p>
	
						<p>In adults &ge; 65 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were pain (&ge; 20%) and erythema (&ge; 10%).</p>
	
						<p>In children 2 through 8 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were tenderness (28.7%), pain (27.9%) and erythema (21.3%), induration (14.9%) and ecchymosis (10.0%). The most common systemic adverse events were sleepiness (14.9%), headache (13.8%), fatigue (13.8%), irritability (13.8%) and loss of appetite (10.6%).</p>
	
						<p>In children and adolescents 9 through 17 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were injection site pain (21.7%), erythema (17.2%) and induration (10.5%).  The most common systemic adverse events were headache (18.1%) and fatigue (17.0%).</p>
	
						<p>To report SUSPECTED ADVERSE REACTIONS, contact Seqirus USA Inc. at <strong>1-855-358-8966</strong> or <strong>VAERS</strong> at <strong>1-800-822-7967</strong> or <a target="_blank" href="http://www.vaers.hhs.gov"><strong>www.vaers.hhs.gov</strong></a>.</p>
	
						<p>Before administration, please see the full US Prescribing Information for FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT and FLUCELVAX QUADRIVALENT.</p>
	
						<p>FLUAD�, FLUAD� QUADRIVALENT, AFLURIA� QUADRIVALENT and FLUCELVAX� QUADRIVALENT are registered trademarks of Seqirus UK Limited or its affiliates.</p>
					</div>
					<div class="sbs__right">
						<p><strong>INDICATIONS AND USAGE</strong></p>
	
						<p>FLUAD� (Influenza Vaccine, Adjuvanted) is an inactivated influenza vaccine indicated for active immunization against influenza disease caused by influenza virus subtypes A and type B contained in the vaccine. FLUAD� QUADRIVALENT (Influenza Vaccine, Adjuvanted) is an inactivated influenza vaccine indicated for active immunization against influenza disease caused by influenza virus subtypes A and types B contained in the vaccine.  FLUAD and FLUAD QUADRIVALENT are approved for use in persons 65 years of age and older.  These indications are approved under accelerated approval based on the immune response elicited by FLUAD QUADRIVALENT.</p>
	
						<p>AFLURIA� QUADRIVALENT (Influenza Vaccine) is an inactivated influenza vaccine indicated for active immunization against influenza disease caused by influenza A subtype viruses and type B viruses contained in the vaccine.  AFLURIA QUADRIVALENT is approved for use in persons 6 months of age and older.</p>
	
						<p>FLUCELVAX� QUADRIVALENT (Influenza Vaccine) is an inactivated vaccine indicated for active immunization for the prevention of influenza disease caused by influenza virus subtypes A and types B contained in the vaccine. FLUCELVAX QUADRIVALENT is approved for use in persons 2 years of age and older.</p>
					</div>
				</div>
			</div>
		 </section>	<section class="references">
		<div class="container">
			<p><strong>References:</strong><br /><br /><strong>1.</strong> FLUAD QUADRIVALENT. Package insert. Seqirus Inc; 2020. <strong>2.</strong> Data on file. Seqirus Inc; 2020. <strong>3.</strong> Medicare.gov. Flu shots. Accessed February 12, 2021. https://www.medicare.gov/coverage/flushots. <strong>4.</strong> O'Hagan DT, Ott GS, De Gregorio E, Seubert A. The mechanism of action of MF59-an innately attractive adjuvant formulation. Vaccine. 2012;30(29):4341-4348.doi:10.1016/j.vaccine.2011.09.061. <strong>5.</strong> O'Hagan DT, Ott GS, Nest GV, Rappuoli R, Del Giudice G. The history of MF59� adjuvant: a phoenix that arose from the ashes. Expert Rev Vaccines. 2013;12(1):13-30. doi:10.1586/erv.12.140 <strong>6.</strong> Banzhoff A, Pellegrini M, Del Giudice G, Fragapane E, Groth N, Podda A. MF59-adjuvanted vaccines for seasonal and pandemic influenza prophylaxis. Influenza Other Respir Viruses. 2008;2(6):243-249. doi:10.1111/j.1750-2659.2008.00059.x <strong>7.</strong> Centers for Disease Control and Prevention. Flu &amp; people 65 years and older. Accessed February 12, 2021. https://www.cdc.gov/flu/highrisk/65over.htm</p>
		</div>
	</section>
</div>

</template:page>