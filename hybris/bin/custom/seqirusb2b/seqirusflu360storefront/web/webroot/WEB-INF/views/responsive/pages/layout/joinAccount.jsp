<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<template:page pageTitle="${pageTitle}">
  <div class="container-fluid joincontent col-xs-12">
         <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumbfont padLeft79">
            <c:url value="/" var="homeUrl" />
               <li class="breadcrumbs-item active"><a href="${homeUrl}" class=""><spring:theme
						code="form.register.homeLabel" /></a></li>
               <li class="breadcrumbs-item" aria-current="page">Access Your Account</li>
            </ol>
         </nav>
	<div class="joinpageHeader col-xs-12">
		<cms:pageSlot position="Section1CA" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
	</div>
         <div class="joincontentArea col-xs-11">
            <div class="leftcontentArea col-xs-12 col-md-4">
            <div class="leftHeader col-xs-11">
                  <cms:pageSlot position="Section2CA" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
               </div>
               <div class="leftsubHeader col-xs-11">
                   <cms:pageSlot position="Section2CB" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
               </div>
               
            </div>
            <div class="rightcontentArea col-xs-12 col-md-8" id="rightSection_1">
               <div class="join_helpSection col-xs-12"> 
               <span class="troubleTexts">Having Trouble? </span><span class="helpTexts"> <a href="${contextPath}/support" style="color: #DF323F;">Get Help</a></span>
               		<!--<cms:pageSlot position="Section3CA" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>-->
					</div>
               <div class="join_header col-xs-12">
                 <cms:pageSlot position="Section3CB" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
               </div>
               <div class="join_subheader col-xs-12">
                  <cms:pageSlot position="Section3CC" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
               </div>
           
		<form action="javascript:void(0)"  role="form" id="joinform" autocomplete="no">
                  <div class="col-xs-12 formContent joinformContent">
                     <div class="row">
                        <div class="form-group col-md-5 col-xs-12">
                           <label for="accountnumber"><img style="display:none;" class="failureimg" id="failureIcon1" src="${fn:escapeXml(themeResourcePath)}/images/failure.png"><img style="display:none;" id="successIcon1" src="${fn:escapeXml(themeResourcePath)}/images/success-icon.svg">
                           Seqirus Customer Account Number<span class="asteriskred">*</span></label><br/>
                           <input class="form-control" id="accountnumber" type="text" name="accountnumber" maxlength="10" data-error="Please enter Account number" placeholder="60 XXX XXX" required/>
                           <div class="help-block with-errors lengtherror"></div>
                        </div>
                        <!-- <div class="form-group col-xs-12 col-md-7">
                           <label for="orgemail"><img style="display:none;" class="failureimg" id="failureIcon2" src="${fn:escapeXml(themeResourcePath)}/images/failure.png"><img style="display:none;" id="successIcon2" src="${fn:escapeXml(themeResourcePath)}/images/success-icon.svg">
                           Organisation Referral Email</label><br/>
                           <input class="form-control joininput emailcheckaccount"  type="email" name="orgemail" id="orgemail" data-error="Please enter a valid email address." placeholder="email@example.com" />
                           <div class="help-block with-errors" id="emailerr"></div>
                        </div>-->
                        <div class="form-group col-xs-12 col-md-5">
                        <label for="postcode"><img style="display:none;" class="failureimg" id="failureIcon3" src="${fn:escapeXml(themeResourcePath)}/images/failure.png"><img style="display:none;" id="successIcon3" src="${fn:escapeXml(themeResourcePath)}/images/success-icon.svg">
                        Postal Code<span class="asteriskred">*</span></label><br/>
                        <input class="form-control joininput selectiveselect"  type="text" name="postcode" id="postcode"  data-error="Please enter postal code" placeholder="SL16 8AA" required/>
                        <div class="help-block with-errors  jointcodeerror1"></div>
                     </div>
                     </div>
                     <!-- <div class="row">
                     <div class="form-group col-xs-12 col-md-5">
                        <label for="postcode"><img style="display:none;" class="failureimg" id="failureIcon3" src="${fn:escapeXml(themeResourcePath)}/images/failure.png"><img style="display:none;" id="successIcon3" src="${fn:escapeXml(themeResourcePath)}/images/success-icon.svg">
                        Postal Code<span class="asteriskred">*</span></label><br/>
                        <input class="form-control joininput selectiveselect"  type="text" name="postcode" id="postcode"  data-error="Please enter postal code" placeholder="SL16 8AA" required/>
                        <div class="help-block with-errors  jointcodeerror1"></div>
                     </div>
                      </div>-->
                     <div class="row">
                        <div class="col-md-7 col-xs-12">
                           <button id="Search1" class="searchbutton" onclick="isAccountExists('accountnumber')">Search</button>
                        </div>
                     </div>
                     </div>
                     </form>
                     <div class="col-xs-12 xhLine">xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</div>
                     <div class="joinmarLeft joinformContent">
                     <form action="javascript:void(0)"  role="form" id="joinform1">
                        <div class="form-group col-md-5 col-xs-12">
                           <label for="jobTitle" style="text-transform:none;color: #5D5D5D;font-family: campton-bold;font-size: 12px;letter-spacing: 0;line-height: 18px;margin-bottom: 3%;"><img style="display:none;" class="failureimg" id="failureIcon5" src="${fn:escapeXml(themeResourcePath)}/images/failure.png"><img style="display:none;" id="successIcon5" src="${fn:escapeXml(themeResourcePath)}/images/success-icon.svg">
                              Access Code<span class="asteriskred">*</span>
                              <!-- Tooltip code starts -->                         
								<div class="myiconInformation">
								<div>
								<div class="tooltipjointtext">
								 <div class="tooltiptoptext">Tool Tip Title</div>
								<div class="tooltipbottomtext">Body text to the tool tip at varying lengths depending on the content</div>
								</div>
								</div>
								 <div class="myicon floatLeft" id="myicon1">
								<div class="smallInfo">i</div>
								</div>
								<div class="clear"></div>
								</div>

                              <!-- Tooltip code ends -->
                              <!-- <div class="myiconInformation ">
                                 <div class="myiconInfo" id="myiconinfo1">
                                    <div class="myiconinfotext"></div>
                                 </div>
                                 <div class="myicon floatLeft" id="myicon1">
                                    <div class="smallInfo">  i</div>
                                 </div>
                                 <div class="clear"></div>
                              </div> -->
                           </label>
                           <br/>
                           <input class="form-control numberonly" type="text" name="accesscode" id="accesscode" maxlength="10" data-error="Enter your unique access code" placeholder="000000000" required>
                           <div class="help-block with-errors lengtherroraccess"></div>
                        </div>
                        <div class="col-md-7 col-xs-12 acesscolumn">
                           <button id="Search2" class="searchbutton" onclick="isAccountExists('accesscode')">Search</button>
                        </div>
                        </form>
                     </div>
                     
                   <div id="successDiv" style="display:none;" class="col-md-10 greentext"><i>Your organisation's &copy;Seqirus account was successfully located</i></div>
                     <div id="successSubDiv" style="display:none;" class="col-md-12" >
                        <div class="col-md-5">
                        
                           <p class="joinconfirmtxt">Please confirm this is your organisation</p>
                           <p class="join_headeracct" id="accountNameTxt"></p>
                        </div>
                        <div class="col-md-7">
                           <input class="col-md-7" type="checkbox" id="privacyPolicy" name="privacyPolicy" style="margin-top: 12px;">
                           <label class="joinradioButton" for="privacyPolicy">
                           I acknowledge that I am employee at
                           </label>
                           <label class="joinradioButton"  for="privacyPolicy" id="accountNameText" ></label>
                        </div>
                        <div class="col-md-10 labelsubtext"><i>Click the &copy;Link Account &copy;button below to link your user profile to
                           your Organisation's &copy;account and access the portal.
                           </i>
                        </div>
                     </div>
                     
                     <div id="failureDiv" style="display:none;" class="col-md-10 failuretext"><i>Your organisation's &copy;Seqirus account was not found</i></div>
                     <div id="failureSubDiv" style="display:none;" class="join_subheader col-xs-12" >
                        <p>Please check the details you have provided above against your records and retry your search.</p>
                        <p>If you are unable to link your account,and require further assistance,click below.</p>
                        
                     </div>
                     
                     <div id="joinActDiv" style="display:none;" class="col-xs-12 prevnmailjoin" >
                     <!--<cms:pageSlot position="Section4CE" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>-->
					<!--<form:form method="post"  action="${contextPath}/joinActRegister"  data-toggle="validator" id="form_id">
					
					
   <!--  <input type="submit" name="button1" value="Button 1" /> -->
    <!--<button id="joinActBtn"  type="submit" class="joinbutton" disabled="disabled">Join Account</button>

 </form:form>-->
<button id="joinActBtn"  type="button" class="joinbutton" ></button>
					
                  </div>
                  </div>
                  
              
                  
            </div>
           
         </div>



</template:page>