<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${cmsPage.uid == 'flu360-overview'}">
	<c:if test="${component.uid == 'flu360-solution-component'}">
		<div id="solutions-header-row" class="row-flex center-xs">
			<div class="center-xs">
				<h2>${feature.content}</h2>
				<p>${feature.paragraphcontent}</p>
			</div>
		</div>
	</c:if>
	<c:if test="${component.uid == 'multi-solution-component'}">
		<div id="multiple-needs-header" class="row-flex center-xs">
			<h2>${feature.content}</h2>
		</div>
	</c:if>
</c:if>
