<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<template:page pageTitle="${pageTitle}">
	
   
        <!--Registration code starts -->
        <main role="main" class="registration col-xs-12 no-padding" >
        <body class="page-registrationpage">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 registration_leftside no-padding-right">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 registration_nagivation no-padding">
                    <div class="registration_nagivationPart" id="registration_nagivationPart1">
                        <div class="registration_nagivationNo">
                            <div class="registration_nagivationtick" style="display:block;"><img src="_ui\responsive\theme-lambda\images\greentick.png" alt=""></div>
                        </div>
                        <div class="registration_nagivationName">User Profile</div>
                    </div>

                    <div class="registration_nagivationPart" id="registration_nagivationPart2">
                        <div class="registration_nagivationNo">
                            <div class="registration_nagivationNumeric">2</div>
                            <div class="registration_nagivationtick" ><img src="_ui\responsive\theme-lambda\images\greentick.png" alt=""></div>
                        </div>
                        <div class="registration_nagivationName active">Business Details</div>
                    </div>
                    <div class="registration_nagivationPart" id="registration_nagivationPart3">
                        <div class="registration_nagivationNo">
                            <div class="registration_nagivationNumeric">3</div>
                            <div class="registration_nagivationtick"><img src="_ui\responsive\theme-lambda\images\greentick.png" alt=""></div>
                        </div>
                        <div class="registration_nagivationName inactive">Contacts & Addresses
                            <ul>
                                <li>Billing Information</li>
                                <li>Paying Information</li>
                                <li>Shipping Location(s)</li>
                            </ul>
                        </div>
                    </div>
                    <div class="registration_nagivationPart" id="registration_nagivationPart4">
                        <div class="registration_nagivationNo">
                            <div class="registration_nagivationNumeric">4</div>
                            <div class="registration_nagivationtick"><img src="_ui\responsive\theme-lambda\images\greentick.png" alt=""></div>
                        </div>
                        <div class="registration_nagivationName inactive">Review & Confirm</div>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 registration_rightside no-padding-left">
                <div class=" registration_help">
                    Having trouble? &nbsp;<a href="" target="_self"><span>We Can Help</span>
                        <div class="global_blackarrow"></div></a>
                </div>
                <section class="registration_business registrationFlow" id="registration_section1">
                    <form id="businessForm" modelAttribute="customerRegisterForm" action="/register/saveRegData" method="post">
                      
                        <input type="hidden" id="loginemail" name ="hiddenEmail" value ="${account.uid}" />
                      <input type="hidden" id="fname" name ="firstName" value ="${account.firstName}" />
                      <input type="hidden" id="lname" name ="lastName" value ="${account.lastname}" />
                       <input type="hidden" id="position" name ="position" value ="${account.jobTitle}" />
                        <input type="hidden" id="phone" name ="phoneNumber" value ="${account.phone}" />
                         <input type="hidden" id="ext" name ="phoneExt" value ="${account.phoneExt}" />
                        <div class="registration_label padding-B10">Business Details</div>
                        <div class="registration_text1 padding-B10">Tell us about your business.</div>
                        <div class="registration_text2">All fields marked with an asterisk (*) are required.</div>
                        <div class="registration_existing padding-B20"><a href="" target="_self">Join an existing business profile instead <div class="global_blackarrow"></div></a></div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="business-orgname">Organization Name*</label>
                                <input  class="registration_input form-control"  id="business-orgname" name="orgAddressForm.billingOrgName" autocomplete="no"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="business-dunnumber">Dun & Bradstreet (DUNS) Number</label>
                                <input class="registration_input form-control" id="business-dunnumber" name="orgAddressForm.duns" autocomplete="off" />
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="registration_text1  margin-B20">If your organization is a member of 1 or more Group Purchasing Organization (GPO), you do not need to provide that information at this time. Our team will follow up to confirm your membership.</div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding margin-B20 registration_address form-group">                        
                            <label class="registration_inputLabel " for="business-lookup">Address Lookup</label>
                            <input class="registration_input form-control" id="business-lookup" name="business-lookup" />
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel " for="business-address1">Address Line 1*</label>
                                <input class="registration_input form-control" id="business-address1" name="orgAddressForm.Line1"  autocomplete="off"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel " for="business-address2">Address Line 2</label>
                                 <input class="registration_input form-control" id="business-address2" name="orgAddressForm.line2" autocomplete="off"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel " for="business-city">City*</label>
                                <input class="registration_input form-control" id="business-city" name="orgAddressForm.city" />
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 no-padding-right margin-B20 registration_mob form-group">                        
                                <label class="registration_inputLabel " for="business-state">State*</label>
                                 <select class="registration_state form-control" id="business-state" name="orgAddressForm.billingState">
                                    <option value="" selected="selected"></option>
                                    <option value="AL">AL</option>
                                    <option value="AK">AK</option>
                                    <option value="AR">AR</option>
                                    <option value="AZ">AZ</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DE">DE</option>
                                    <option value="DC">DC</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="GU">GU</option>
                                    <option value="HI">HI</option>
                                    <option value="IA">IA</option>
                                    <option value="ID">ID</option>
                                    <option value="IL">IL</option>
                                    <option value='IN'>IN</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="ME">ME</option>
                                    <option value="MD">MD</option>
                                    <option value="MA">MA</option>
                                    <option value="MI">MI</option>
                                    <option value="MN">MN</option>
                                    <option value="MS">MS</option>
                                    <option value="MO">MO</option>
                                    <option value="MT">MT</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="NE">NE</option>
                                    <option value="NV">NV</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NY">NY</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PA">PA</option>
                                    <option value="PR">PR</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VA">VA</option>
                                    <option value="VI">VI</option>
                                    <option value="VT">VT</option>
                                    <option value="WA">WA</option>
                                    <option value="WI">WI</option>
                                    <option value="WV">WV</option>
                                    <option value="WY">WY</option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 no-padding-right margin-B20 form-group">                        
                                <label class="registration_inputLabel" for="business-zipcode">ZIP Code*</label>
                               <input class="registration_input form-control" id="business-zipcode" name="orgAddressForm.postalCode" autocomplete="off"/>                                                       
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <button type="button" class="registration_save active" id="registration_save">Save and Continue</button>
                    </form>
                </section>
                <section class="registration_billing registrationFlow" id="registration_section2">
                    <form id="billingForm">
                     <input type="hidden" id="loginemail" name ="hiddenEmail" value ="${account.uid}" />
                        <div class="registration_label padding-B10">Billing Information</div>
                        <div class="registration_text1 padding-B10">Complete this section with the contact information of the person or group who should receive all financial documents from Seqirus.</div>
                        <div class="registration_text2">All fields marked with an asterisk (*) are required.</div>
                        <div class="registration_contact padding-B20">Billing Contact</div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="billing-firstname">First Name*</label>
                                <input class="registration_input form-control" id="billing-firstname" name="billAndShippAddressForm.firstName" autocomplete="off"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="billing-lastname">Last Name*</label>
                                <input class="registration_input form-control" id="billing-lastname" name="billAndShippAddressForm.lastName" autocomplete="off"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                            <label class="registration_inputLabel" for="billing-email">Email Address*</label>
                            <input class="registration_input form-control" id="billing-email" name="billAndShippAddressForm.email"  autocomplete="off"/>
                        </div>
                        <div class="clearboth"></div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="billing-phone">Phone Number*</label>
                                <input class="registration_input form-control" id="billing-phone" name="billAndShippAddressForm.phone"  autocomplete="off"/>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="billing-extn">Ext.</label>
                                <input class="registration_input form-control" id="billing-extn" name="billAndShippAddressForm.phoneExt"  autocomplete="off"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="registration_contact padding-B20">Billing Address</div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                            <label class="registration_inputLabel" for="billing-orgname">Organization Name</label>
                            <input class="registration_input form-control" id="billing-orgname" name="billAndShippAddressForm.billingOrgName" autocomplete="off"/>
                        </div>
                        <div class="clearboth"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-left margin-B30 registration_address form-group">                        
                            <label class="registration_inputLabel" for="billing-lookup">Address Lookup</label>
                            <input class="registration_input form-control" id="billing-lookup" name="billing-lookup"  />
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="billing-address1">Address Line 1*</label>
                                <input class="registration_input form-control" id="billing-address1" name="billAndShippAddressForm.Line1"  autocomplete="off"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="billing-address2">Address Line 2</label>
                                <input class="registration_input form-control" id="billing-address2" name="billAndShippAddressForm.Line2"  autocomplete="off"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="billing-city">City*</label>
                                <input class="registration_input form-control" id="billing-city" name="billAndShippAddressForm.city" autocomplete="off"/>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 no-padding-right margin-B20 registration_mob form-group">                        
                                <label class="registration_inputLabel "  for="billing-state">State*</label>
                                <select class="registration_state form-control" id="billing-state" name="billAndShippAddressForm.billingState">
                                    <option value="" selected="selected"></option>
                                    <option value="AL">AL</option>
                                    <option value="AK">AK</option>
                                    <option value="AR">AR</option>
                                    <option value="AZ">AZ</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DE">DE</option>
                                    <option value="DC">DC</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="GU">GU</option>
                                    <option value="HI">HI</option>
                                    <option value="IA">IA</option>
                                    <option value="ID">ID</option>
                                    <option value="IL">IL</option>
                                    <option value='IN'>IN</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="ME">ME</option>
                                    <option value="MD">MD</option>
                                    <option value="MA">MA</option>
                                    <option value="MI">MI</option>
                                    <option value="MN">MN</option>
                                    <option value="MS">MS</option>
                                    <option value="MO">MO</option>
                                    <option value="MT">MT</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="NE">NE</option>
                                    <option value="NV">NV</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NY">NY</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PA">PA</option>
                                    <option value="PR">PR</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VA">VA</option>
                                    <option value="VI">VI</option>
                                    <option value="VT">VT</option>
                                    <option value="WA">WA</option>
                                    <option value="WI">WI</option>
                                    <option value="WV">WV</option>
                                    <option value="WY">WY</option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 no-padding-right margin-B20 form-group">                        
                                <label class="registration_inputLabel" for="billing-zipcode">ZIP Code*</label>
                                <input class="registration_input form-control" id="billing-zipcode" name="billAndShippAddressForm.postalCode"  autocomplete="off"/>                              
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="registration_contact padding-B20">Communication Preferences</div>
                        <div class="padding-R15">
                            <div class="margin-B20 registration_reviewCheckbox registration_parent registration_checkparent">                                              
                            <label class="checkbox-button">
                                <input type="checkbox" class="registration_checkbox" id="bill-payer_check" name="bill-payer_check"/>
                                <span class="checkbox-button__control"></span>
                            </label>
                            <label class="registration_checkboxLabel">Use this contact and address for payer contact as well &nbsp;&nbsp;<i class="fa fa-question-circle-o" data-toggle="tooltip" title="Help"></i></label>
                            <div class="clearboth"></div>
                        </div>
                            <div class="margin-B20 registration_parent registration_reviewCheckbox">                        
                            <label class="checkbox-button">
                                <input type="checkbox" class="registration_checkbox" id="bill-ship_check" name="bill-ship_check"/>
                                <span class="checkbox-button__control"></span>
                            </label>
                            <label class="registration_checkboxLabel">Ship to this location &nbsp;&nbsp;<i class="fa fa-question-circle-o" data-toggle="tooltip" title="Help"></i></label>
                            <div class="clearboth"></div>
                        </div>
                        </div>
                        <div class="padding-R15 padding-B20 registration_checkQues">How would you like to receive account statements?</div>
                        <div class="padding-R15 ">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 no-padding-left margin-B10 registration_parent">                                              
                            <label class="radio-button">
                                <input type="radio" class="registration_radio" name="bill-accountType" value="1" id="bill-accountType_E" checked="checked"/>
                                <span class="radio-button__control"></span>
                            </label>
                            <label class="registration_checkboxLabel">Email only</label>
                            <div class="clearboth"></div>
                        </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 no-padding-right margin-B10 registration_parent">                        
                            <label class="radio-button">
                                <input type="radio" class="registration_radio" name="bill-accountType" value="0" id="bill-accountType_M"/>
                                <span class="radio-button__control"></span>
                            </label>
                            <label class="registration_checkboxLabel">By mail</label>
                            <div class="clearboth"></div>
                        </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding-left margin-B20 registration_parent form-group" id="bill-accemail">                        
                            <label class="registration_inputLabel " for="billing-accountemail">Email Address*</label>
                            <input class="registration_input form-control" id="billing-accountemail" name="billAndShippAddressForm.acctStmtEmail"  autocomplete="off" />
                        </div>
                        <div class="clearboth"></div>
                        <div class="padding-R15 padding-B20 registration_checkQues">How would you like to receive invoices?</div>
                        <div class="padding-R15 ">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 no-padding-left margin-B10 registration_parent">                                              
                            <label class="radio-button">
                                <input type="radio" class="registration_radio" name="bill-invoiceType" value="1" id="bill-invoiceType_E" checked="checked"/>
                                <span class="radio-button__control"></span>
                            </label>
                            <label class="registration_checkboxLabel">Email only</label>
                            <div class="clearboth"></div>
                        </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 no-padding-right margin-B10 registration_parent">                        
                            <label class="radio-button">
                                <input type="radio" class="registration_radio" name="bill-invoiceType" value="0" id="bill-invoiceType_M"/>
                                <span class="radio-button__control"></span>
                            </label>
                            <label class="registration_checkboxLabel">By mail</label>
                            <div class="clearboth"></div>
                        </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding-left margin-B20 registration_parent form-group" id="bill-inemail">                        
                            <label class="registration_inputLabel " for="billing-invoiceemail">Email Address*</label>
                            <input class="registration_input form-control" id="billing-invoiceemail" name="billAndShippAddressForm.invoiceEmail"  autocomplete="off"/>
                        </div>
                        <div class="clearboth"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding margin-T20 margin-B20 registration_button">
                            <div class="registration_back active">
                                <div class="global_blackarrow"></div>
                                <span>Back</span>
                                <div class="clearboth"></div>
                            </div>
                            <button type="button" class="registration_save active" id="registration_save">Save and Continue</button>
                            <div class="clearboth"></div>
                        </div>    
                    </form>
                </section>
                <section class="registration_paying registrationFlow" id="registration_section3">
                    <form id="payingForm">
                        <div class="registration_label padding-B10">Paying Information</div>
                        <div class="registration_text1 padding-B10">Complete this section with the contact information of the person or group who will be paying invoices that come from Seqirus.</div>
                        <div class="registration_text2">All fields marked with an asterisk (*) are required.</div>
                        <div class="registration_contact padding-B20">Paying Contact</div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel " for="pay-firstname">First Name*</label>
                                <input class="registration_input form-control" id="pay-firstname" name="payerAddressForm.firstName"  autocomplete="off"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel " for="pay-lastname">Last Name*</label>
                                <input class="registration_input form-control" id="pay-lastname" name="payerAddressForm.lastName"  autocomplete="off"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                            <label class="registration_inputLabel " for="pay-email">Email Address*</label>
                            <input class="registration_input form-control" id="pay-email" name="payerAddressForm.email"  autocomplete="off"/>
                        </div>
                        <div class="clearboth"></div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel " for="pay-phone">Phone Number*</label>
                                <input class="registration_input form-control" id="pay-phone" name="payerAddressForm.phone"  autocomplete="off"/>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel " for="pay-ext">Ext.</label>
                                <input class="registration_input form-control" id="pay-ext" name="payerAddressForm.phoneExt"  autocomplete="off"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="registration_contact padding-B20">Paying Address</div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                            <label class="registration_inputLabel " for="pay-orgname">Organization Name</label>
                            <input class="registration_input form-control" id="pay-orgname" name="payerAddressForm.billingOrgName"  autocomplete="off" />
                        </div>
                        <div class="clearboth"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-left margin-B30 registration_address form-group">                        
                            <label class="registration_inputLabel " for="pay-lookup">Address Lookup</label>
                            <input class="registration_input form-control" id="pay-lookup" name="pay-lookup"  />
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel " for="pay-address1">Address Line 1*</label>
                                <input class="registration_input form-control" id="pay-address1" name="payerAddressForm.Line1"  autocomplete="off"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel " for="pay-address2">Address Line 2</label>
                                <input class="registration_input form-control" id="pay-address2" name="payerAddressForm.Line2"  autocomplete="off" />
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel " for="pay-city">City*</label>
                                <input class="registration_input form-control" id="pay-city" name="payerAddressForm.city"  autocomplete="off"/>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 no-padding-right margin-B20 registration_mob form-group">                        
                                <label class="registration_inputLabel " for="pay-state">State*</label>
                                <select class="registration_state form-control" id="pay-state" name="payerAddressForm.billingState">
                                    <option value="" selected="selected"></option>
                                    <option value="AL">AL</option>
                                    <option value="AK">AK</option>
                                    <option value="AR">AR</option>
                                    <option value="AZ">AZ</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DE">DE</option>
                                    <option value="DC">DC</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="GU">GU</option>
                                    <option value="HI">HI</option>
                                    <option value="IA">IA</option>
                                    <option value="ID">ID</option>
                                    <option value="IL">IL</option>
                                    <option value='IN'>IN</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="ME">ME</option>
                                    <option value="MD">MD</option>
                                    <option value="MA">MA</option>
                                    <option value="MI">MI</option>
                                    <option value="MN">MN</option>
                                    <option value="MS">MS</option>
                                    <option value="MO">MO</option>
                                    <option value="MT">MT</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="NE">NE</option>
                                    <option value="NV">NV</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NY">NY</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PA">PA</option>
                                    <option value="PR">PR</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VA">VA</option>
                                    <option value="VI">VI</option>
                                    <option value="VT">VT</option>
                                    <option value="WA">WA</option>
                                    <option value="WI">WI</option>
                                    <option value="WV">WV</option>
                                    <option value="WY">WY</option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 no-padding-right margin-B20 form-group">                        
                                <label class="registration_inputLabel " for="pay-zipcode">ZIP Code*</label>
                                <input class="registration_input form-control" id="pay-zipcode" name="payerAddressForm.postalCode"  autocomplete="off"/>                            
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding margin-T20 margin-B20 registration_button">
                            <div class="registration_back active">
                                <div class="global_blackarrow"></div>
                                <span>Back</span>
                                <div class="clearboth"></div>
                            </div>
                            <button type="button" class="registration_save active" id="registration_save">Save and Continue</button>
                            <div class="clearboth"></div>
                        </div>
                    </form>
                </section>
                <section class="registration_shipping registrationFlow" id="registration_section4">
                   <form id="shippingForm">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-left registration_label padding-B10">
                        <input type="hidden" id="loginemail" name ="hiddenEmail" value ="${account.uid}" />
                            <div>Shipping Locations</div>
                            <div>
                                <div class="registration_download"><a href="" target="_blank" download>Download Template &nbsp;<span class="glyphicon glyphicon-download-alt"></span></a></div>
                                <div class="registration_upload" id="registration_upload">Upload Shipping Locations</div>
                            </div>
                        </div>
                        <div class="registration_text1 padding-B10">Complete this section with shipping location(s) used for receiving vaccine shipments, or save time by uploading shipping location documents via the link above. You can always add more locations.</div>
                        <div class="registration_text1 padding-B10">Please note, if information is incomplete or incorrect, we will contact you within 5 business days. This may affect verification, which could delay any orders or shipments to that location.</div>
                        <div class="registration_text2">All fields marked with an asterisk (*) are required.</div>
						
						<!---Add Shipping Locations starts----->
						<div class="print_shipping">
						
						</div>
						<!---Add Shipping Locations ends----->
		<div class="shipping-block">				
						<div class="registration_contact padding-B20">Shipping Location Contact &nbsp;&nbsp;<i class="fa fa-question-circle-o" data-toggle="tooltip" title="Help"></i><div class='registration_edit add_ship_del'>Remove <i class='fa fa-trash' aria-hidden='true'></i></div></div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-firstname">First Name*</label>
                                <input class="registration_input form-control field_mandatory" id="shipping-firstname" name="shippingAddressForm[0].firstName" autocomplete="no"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-lastname">Last Name*</label>
                                <input class="registration_input form-control field_mandatory" id="shipping-lastname" name="shippingAddressForm[0].lastName" autocomplete="no"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                            <label class="registration_inputLabel" for="shipping-email">Email Address*</label>
                            <input class="registration_input form-control field_mandatory" id="shipping-email" name="shippingAddressForm[0].email"  autocomplete="no"/>
                        </div>
                        <div class="clearboth"></div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-phno">Phone Number*</label>
                                <input class="registration_input form-control field_mandatory" id="shipping-phno" name="shippingAddressForm[0].phone" autocomplete="no"/>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-extension">Ext.</label>
                                <input class="registration_input form-control" id="shipping-extension" name="shippingAddressForm[0].phoneExt"  autocomplete="no"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="registration_contact padding-B20">Shipping Address</div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                            <label class="registration_inputLabel" for="shipping-orgname">Organization Name*</label>
                            <input class="registration_input form-control field_mandatory" id="shipping-orgname" name="shippingAddressForm[0].billingOrgName" autocomplete="no"/>
                        </div>
                        <div class="clearboth"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-left margin-B30 registration_address form-group">                        
                            <label class="registration_inputLabel" for="shipping-lookup1">Address Lookup</label>
                            <input class="registration_input form-control" id="shipping-lookup1" name="shipping-lookup1"  autocomplete="no"/>
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-address1">Address Line 1*</label>
                                <input class="registration_input form-control field_mandatory" id="shipping-address1" name="shippingAddressForm[0].Line1"  autocomplete="no"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-address2">Address Line 2</label>
                                <input class="registration_input form-control" id="shipping-address2" name="shippingAddressForm[0].Line2"  autocomplete="no"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-city1">City*</label>
                                <input class="registration_input form-control field_mandatory" id="shipping-city1" name="shippingAddressForm[0].city"  autocomplete="no"/>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 no-padding-right margin-B20 registration_mob form-group">                        
                                <label class="registration_inputLabel" for="shipping-state1">State*</label>
                                <select class="registration_state form-control field_mandatory" id="shipping-state1" name="shippingAddressForm[0].billingState">
                                    <option value="" selected="selected"></option>
                                    <option value="AL">AL</option>
                                    <option value="AK">AK</option>
                                    <option value="AR">AR</option>
                                    <option value="AZ">AZ</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DE">DE</option>
                                    <option value="DC">DC</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="GU">GU</option>
                                    <option value="HI">HI</option>
                                    <option value="IA">IA</option>
                                    <option value="ID">ID</option>
                                    <option value="IL">IL</option>
                                    <option value='IN'>IN</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="ME">ME</option>
                                    <option value="MD">MD</option>
                                    <option value="MA">MA</option>
                                    <option value="MI">MI</option>
                                    <option value="MN">MN</option>
                                    <option value="MS">MS</option>
                                    <option value="MO">MO</option>
                                    <option value="MT">MT</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="NE">NE</option>
                                    <option value="NV">NV</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NY">NY</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PA">PA</option>
                                    <option value="PR">PR</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VA">VA</option>
                                    <option value="VI">VI</option>
                                    <option value="VT">VT</option>
                                    <option value="WA">WA</option>
                                    <option value="WI">WI</option>
                                    <option value="WV">WV</option>
                                    <option value="WY">WY</option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 no-padding-right margin-B20 form-group">                        
                                <label class="registration_inputLabel" for="shipping-zip1">ZIP Code*</label>
                                <input class="registration_input form-control field_mandatory" id="shipping-zip1" name="shippingAddressForm[0].postalCode"  autocomplete="no"/>                            
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="registration_contact padding-B20">License</div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-licensename">Name on License*</label>
                                <input class="registration_input form-group field_mandatory" id="shipping-licensename" name="shippingAddressForm[0].licenseDetailsForm.licenseName"  autocomplete="no"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-licenseno">State License Number*</label>
                                <input class="registration_input form-group field_mandatory" id="shipping-licenseno" name="shippingAddressForm[0].licenseDetailsForm.licenseNum" autocomplete="no"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding-left margin-B20 shipping_check registration_parent form-group">                                              
                            <label class="checkbox-button">                           
                            	<input type="checkbox" class="registration_checkbox form-group" id="shipping-checkbox" name="shipping-checkbox"/>
                            	<span class="checkbox-button__control"></span>
                        	</label>
                            <label class="registration_checkboxLabel"  for="shipping-checkbox">Same address for license as shipping address</label>
                            <div class="clearboth"></div>
                        </div>
                        <div class="clearboth"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-left margin-B30 registration_address form-group">                        
                            <label class="registration_inputLabel" for="shipping-lookup2">Address Lookup</label>
                            <input class="registration_input form-group" id="shipping-lookup2" name="shipping-lookup2"  autocomplete="no"/>
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-address3">Address Line 1*</label>
                                <input class="registration_input form-group field_mandatory" id="shipping-address3" name="shippingAddressForm[0].licenseDetailsForm.licAddressLine1"  autocomplete="no"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-address4">Address Line 2</label>
                                <input class="registration_input form-group" id="shipping-address4" name="shippingAddressForm[0].licenseDetailsForm.licAddressLine2"  autocomplete="no"/>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="padding-R15">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group">                        
                                <label class="registration_inputLabel" for="shipping-city2">City*</label>
                                <input class="registration_input form-group field_mandatory" id="shipping-city2" name="shippingAddressForm[0].licenseDetailsForm.licenseCity"  autocomplete="no"/>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 no-padding-right margin-B20 registration_mob form-group">                        
                                <label class="registration_inputLabel" for="shipping-state2">State*</label>
                                <select class="registration_state form-group field_mandatory" id="shipping-state2" name="shippingAddressForm[0].licenseDetailsForm.licenseState">
                                    <option value="" selected="selected"></option>
                                    <option value="AL">AL</option>
                                    <option value="AK">AK</option>
                                    <option value="AR">AR</option>
                                    <option value="AZ">AZ</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DE">DE</option>
                                    <option value="DC">DC</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="GU">GU</option>
                                    <option value="HI">HI</option>
                                    <option value="IA">IA</option>
                                    <option value="ID">ID</option>
                                    <option value="IL">IL</option>
                                    <option value='IN'>IN</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="ME">ME</option>
                                    <option value="MD">MD</option>
                                    <option value="MA">MA</option>
                                    <option value="MI">MI</option>
                                    <option value="MN">MN</option>
                                    <option value="MS">MS</option>
                                    <option value="MO">MO</option>
                                    <option value="MT">MT</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="NE">NE</option>
                                    <option value="NV">NV</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NY">NY</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PA">PA</option>
                                    <option value="PR">PR</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VA">VA</option>
                                    <option value="VI">VI</option>
                                    <option value="VT">VT</option>
                                    <option value="WA">WA</option>
                                    <option value="WI">WI</option>
                                    <option value="WV">WV</option>
                                    <option value="WY">WY</option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 no-padding-right margin-B20 form-group">                        
                                <label class="registration_inputLabel " for="shipping-zip2">ZIP Code*</label>
                                <input class="registration_input form-group field_mandatory" id="shipping-zip2" name="shippingAddressForm[0].licenseDetailsForm.zipcode"  autocomplete="no"/>                            
                            </div>
		</div>	
		</div>
							<div class="clearboth"></div>
                            <div class="registration_addbutton padding-B20 registration_addbutton_disable">Add Another Location &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></div>
							<div class="padding-B20 only_show_field">Add Another Location &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding margin-T20 margin-B20 registration_button">
                                <div class="registration_back active">
                                    <div class="global_blackarrow"></div>
                                    <span>Back</span>
                                    <div class="clearboth"></div>
                                </div>
                                <button type="button" class="registration_save active" id="registration_save">Save and Continue</button>
                                <div class="clearboth"></div>
                            </div>
                    </form>
                </section>
                <section class="registration_review registrationFlow" id="registration_section5">
                    <form id="reviewForm">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-left registration_label padding-B10">Review Information</div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-left registration_text1 padding-B20">Please confirm everything looks correct. Once you've submitted your information, you'll have access to your account where you can pay invoices, access helpful resources, and place orders. We won't be able to process any orders until we confirm your information, which usually takes 3-5 business days.</div>
                        <div class="clearboth"></div>
                        <div class="registration_reviewContainer">
                            <div class="registration_reviewProfile padding-B20">
                                <div class="registration_reviewlabel1">Profile</div>
                                <div class="clearboth"></div>
                                <div class="registration_reviewlabel2 margin-B20">Login Info</div>
                                <div class="registration_populate registration_reviewemail1" >${account.uid}</div>
                                <div class="registration_populate registration_reviewpassword" id="reviewLoginPassword">***********</div>
                                <div class="registration_reviewlabel2 margin-T25 margin-B20">Contact Info</div>
                                <div class="registration_populate registration_reviewname" id="reviewName">${account.firstName}&nbsp;${account.lastname}</div>
                                <div class="registration_populate registration_reviewBuyer" id="reviewJob">${account.jobTitle}</div>
                                <div class="registration_populate registration_reviewemail" id="reviewContactEmail">${account.uid}</div>
                                <div class="registration_populate registration_reviewPh" id="reviewPhone">${account.phone} ext. ${account.phoneExt}</div>
                            </div>
                            <div class="registration_reviewBusiness padding-B20">
                                <div class="registration_reviewparent">
                                    <div class="registration_reviewlabel1">Business Details</div>
                                    <div class="registration_edit" id="registration_Businessedit">Edit <img src="_ui/responsive/theme-lambda/images/edit.png" width="20" alt="Edit"></div>
                                    <div class="clearboth"></div>
                                </div>
                                <div class="registration_reviewlabel2 margin-B20" >Company Info</div>
                                <div class="registration_populate registration_companyName" id="review-orgname">Main Pharmacy</div>
                               <!-- <div class="registration_populate registration_GPOname">GPO Name</div>
                                <div class="registration_populate registration_GPOname">GPO #</div>-->
                                <div class="registration_populate registration_duns" id="review-duns">DUNS #</div>
                                <div class="registration_reviewlabel2 margin-T25 margin-B20" >Address</div>
                                <div class="registration_populate registration_reviewaddress1" id="review-bussiness-address1">123 Main Street</div>
                                <div class="registration_populate registration_reviewaddress2" id="review-bussiness-address2">Suite 210</div>
                                <div class="registration_populate registration_reviewaddress3" id="review-bussiness-city-zip">Boston, MA 02115</div>
                            </div>
                        </div>
                        <div class="registration_reviewContainer">
                            <div class="registration_reviewBilling padding-B20">
                                <div class="registration_reviewparent">
                                    <div class="registration_reviewlabel1">Billing Details</div>
                                    <div class="registration_edit" id="registration_Billingedit">Edit <img src="_ui/responsive/theme-lambda/images/edit.png" width="20" alt="Edit"></div>
                                    <div class="clearboth"></div>
                                </div>
                                <div class="registration_reviewlabel2 margin-B20">Contact Info</div>
                                <div class="registration_populate registration_reviewname" id="review-billing-name">Akilah Hughes</div>
                                <div class="registration_populate registration_reviewemail" id="review-billing-email">email@email.com</div>
                                <div class="registration_populate registration_reviewPh" id="review-billing-phone">(555) 555-5555 ext. 1234</div>
                                <div class="registration_reviewlabel2 margin-T25 margin-B20">Address</div>
                                <div class="registration_populate registration_reviewaddress1" id="review-billing-address1" >123 Main Street</div>
                                <div class="registration_populate registration_reviewaddress2" id="review-billing-address2">Suite 210</div>
                                <div class="registration_populate registration_reviewaddress3" id="review-billing-city-zip">Boston, MA 02115</div>
                                <div class="registration_reviewlabel2 registration_reviewCheckbox margin-T25">
                                    <label class="checkbox-button">
                                        <input type="checkbox" class="registration_checkbox" disabled name="review-billing-invoiceType" value="0" id="review-billing-invoiceType_E"/>
                                        <span class="checkbox-button__control"></span>
                                    </label>
                                    <label class="registration_checkboxLabel">Receiving invoices by email</label>
                                    <div class="clearboth"></div>
                                </div>
                                <div class="registration_populate registration_reviewemail" id="review-billing-invoice-email">email@email.com</div>
                                <div class="registration_reviewlabel2 registration_reviewCheckbox margin-T25">
                                    <label class="checkbox-button">
                                        <input type="checkbox" class="registration_checkbox" disabled name="review-billing-accountType" value="0" id="review-billing-accountType_E"/>
                                        <span class="checkbox-button__control"></span>
                                    </label>
                                    <label class="registration_checkboxLabel">Receiving account statements by email</label>
                                    <div class="clearboth"></div>
                                </div>
                                <div class="registration_populate registration_reviewemail" id="review-billing-account-email">email@email.com</div>
                            </div>
                            <div class="registration_reviewpaying padding-B20">
                                <div class="registration_reviewparent">
                                    <div class="registration_reviewlabel1">Paying Details</div>
                                    <div class="registration_edit"id="registration_Payingedit">Edit <img src="_ui/responsive/theme-lambda/images/edit.png" width="20" alt="Edit"></div>
                                    <div class="clearboth"></div>
                                </div>
                                <div class="registration_reviewlabel2 margin-B20">Contact Info</div>
                                <div class="registration_populate registration_reviewname" id="review-paying-name">Akilah Hughes</div>
                                <div class="registration_populate registration_reviewemail" id="review-paying-email">email@email.com</div>
                                <div class="registration_populate registration_reviewPh" id="review-paying-phone">(555) 555-5555 ext. 1234</div>
                                <div class="registration_reviewlabel2 margin-T25 margin-B20">Address</div>
                                <div class="registration_populate registration_reviewaddress1" id="review-paying-adrress1">123 Main Street</div>
                                <div class="registration_populate registration_reviewaddress2" id="review-paying-adrress2">Suite 210</div>
                                <div class="registration_populate registration_reviewaddress3" id="review-paying-city-zip">Boston, MA 02115</div>
                            </div>
                        </div>
                        <div class="registration_reviewShipping margin-B20">
                            <div class="registration_reviewparent">
                                <div class="registration_reviewlabel1">Shipping Locations</div>
                                <div class="registration_edit" id="registration_Shippingedit">Edit <img src="_ui/responsive/theme-lambda/images/edit.png" width="20" alt="Edit"></div>
                                <div class="clearboth"></div>
                            </div>
                            <div class="registration_reviewlabel2"><span class="total_ship_location"></span> shipping locations</div>
                        </div>                      
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding margin-T20 margin-B20 registration_button">
                            <div class="registration_back active">
                                <div class="global_blackarrow"></div>
                                <span>Back</span>
                                <div class="clearboth"></div>
                            </div>
                            <button class="registration_createAccount active" type="submit">Create Account</button>
                            <div class="clearboth"></div>
                        </div>

                    </form>
                </section>
				<section class="registration_uploadConfirm registrationFlow" id="registration_uploadConfirm">
                    <!--<form id="uploadForm" method="post" class="" action="" autocomplete="off">-->
                    <div class="registration_label padding-B10">Shipping Locations</div>
                    <div class="registration_shippingUploadtext2 margin-B10">Complete this section with shipping location(s) used for receiving vaccine shipments, or save time by uploading shipping location documents via the link below. You can always add more locations whenever you need.
                        <p>Please note, if information is incomplete or incorrect, we will contact you within 5 business days. This may affect verification, which could delay any orders or shipments to that location.</p>
                    </div>
                    <div class=" margin-B10">
                        <div class="registration_shippingUploadtext2 uploadformat"><span style="color:#2A3237;">Accepted File Formats:</span> .xls, .xlsx</div>
                        <div class="registration_popcont">
                            <div class="registration_download"><a href="" target="_blank" download>Download Template &nbsp;<span class="glyphicon glyphicon-download-alt"></span></a></div>
                            <div class="registration_upload" id="registration_uploadlocation1">Upload Location Documents</div>                            
                            <div class="clearboth"></div>
                            <input class="form-group" id="registration_uploadInput" type="file" name="registration_uploadfile"/>
                        </div>
                        <div class="clearboth"></div>
                    </div>
                    <div class="registration_uploadFile">
                        <p></p>
                        <div></div>
                        <button class="registration_uploadclose">X</button>
                    </div>
                    <div class="registration_shippingUploadtext2 margin-B10">Please ensure that these are the correct documents before continuing, complete with <span>main contact</span> (name, email, and phone number) for each location, as well as the <span>full address</span> and <span>license information</span> If your upload is missing important information, we will contact you within 5 business days. This may affect location verification, which could delay any orders or shipments. You can also upload more than 1 document, or choose to <span class="registration_manualLocation">manually upload locations</span> instead. </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding margin-T20 margin-B20 registration_button">
                        <div class="registration_back active">
                            <div class="global_blackarrow"></div>
                            <span>Back</span>
                            <div class="clearboth"></div>
                        </div>
                        <button class="registration_save active">Save and Continue</button>
                        <div class="clearboth"></div>
                    </div>
                    <!--</form>-->
                </section>		
            </div>
             <div class="registration_shippingUploadpop modal" data-backdrop="static" id="registration_shippingUploadpop">
            <div class="registration_shippingUploadContent">
                <div class="registration_shippingUploadheader">
                    <div class="registration_shippingUploadtext1 margin-B10">Prefer to upload location information?</div>
                    <div class="registration_shippingUploadclose margin-B10">X</div>
                    <div class="clearboth"></div>
                </div>
                <div class="registration_shippingUploadtext2 margin-B10">If you have multiple shipping locations, you can save time by uploading that information.
                    Please make sure you download the template and enter the necessary information including the <span>main contact</span> (name, email, and phone number) for each location, as well as the <span>full address</span> and <span>license information</span>.
                    <p>If your upload is missing important information, we will contact you within 5 business
                        days. This may affect location verification, which could delay any orders or shipments.</p>
                </div>
                <div class=" margin-B10">
                    <div class="registration_shippingUploadtext2 uploadformat"><span style="color:#2A3237;">Accepted File Formats:</span> .xls, .xlsx</div>
                    <div class="registration_popcont">
                        <div class="registration_download"><a href="" target="_blank" download>Download Template &nbsp;<span class="glyphicon glyphicon-download-alt"></span></a></div>
                        <div class="registration_upload" id="registration_uploadlocation">Upload Location Documents</div>                            
                        <div class="clearboth"></div>
                        <input class="form-group" id="registration_uploadInput" type="file" name="registration_uploadfile" />
                    </div>
                    <div class="clearboth"></div>
                </div>
                <div class="registration_uploadFile">
                    <p></p>
                    <div></div>
                    <button class="registration_uploadclose">X</button>
                </div>
                <button class="registration_shippingSave inactive">Save</button>
            </div>
        </div>
        <div class="registration_helppop modal" data-backdrop="static" id="registration_helppop">
            <div class="registration_helpContent">
                <div class="registration_helpheader">
                    <div class="registration_shippingUploadtext1">Are you sure you want to leave this page?</div>
                    <div class="registration_helpclose ">X</div>
                    <div class="clearboth"></div>
                </div>
                <div class="registration_shippingUploadtext2">If you leave now, your registration will be incomplete, and the information you have entered so far will not be saved. It also means you will not be able to start orders or view important information about your account.</p>
                </div>
                <div class="registration_popcont">
                    <button class="registration_helpLeave">Leave and Lose Progress</button>
                    <button class="registration_helpContinue">Continue Registration</button>
                </div>

            </div>
        </div>
        </body>
        </main>
        <!--registration code ends -->

        

</template:page>
<script type="text/javascript" src="${themeResourcePath}/js/validate/jquery.validate.js"></script>
				<script type="text/javascript" src="${themeResourcePath}/js/registration.js"></script>
