<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer"
	tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<div class="footer_container">
		<div class="container-fluid container-lg">
			<div class="row">
				<div class="col-md-4">
					<div class="Logo_footer">
					<c:forEach items="${navigationNodes[3].children[0].links}" var="logo">
					<c:if test="${logo.media.altText eq 'flu360'}">
					<img src="${logo.media.url}" alt="${logo.media.altText}" >
					</c:if>
					</c:forEach>
					</div>
					<div class="hidden-xs visible-md-* visible-lg-* hidden-sm">
					<div class="logo_seqirus">
					
					<c:forEach items="${navigationNodes[3].children[0].links}" var="logo">
					<c:if test="${logo.media.altText eq 'seqirus'}">
					<img src="${logo.media.url}" alt="${logo.media.altText}" >
					</c:if>
					</c:forEach>
					
					</div>
					<div class="fotter_txt"><a href="#" target="_blank">Visit Our Corporate Site<br/>at seqirus.us</a></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="footer_heading">Navigate</div>
					<ul class="footer_menu_list navigate_menu footer_menu_list_nav">
					<c:forEach items="${navigationNodes[2].children[0].links}" var="navSiteNode">
					<li><a href="${navSiteNode.url}">${navSiteNode.name}</a></li>
					</c:forEach>
					</ul>
				</div>
				<div class="col-md-5">
					<div class="footer_heading">Website Terms of Use</div>
					<ul class="footer_menu_list">
					<c:forEach items="${navigationNodes[0].children}" var="navNode">
										<li><a href="${navNode.entries[0].item.url}">${navNode.name}</a></li>
										</c:forEach>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="hidden-md hidden-lg visible-xs-* visible-sm-* margin-T20 margin-B20">
					<div class="logo_seqirus">
					<c:forEach items="${navigationNodes[3].children[0].links}" var="logo">
					<c:if test="${logo.media.altText eq 'seqirus'}">
					<img alt="${logo.media.altText}" src="${logo.media.url}">
					</c:if>
					</c:forEach>
					</div>
					<div class="fotter_txt"><a href="#" target="_blank">Visit Our Corporate Site<br/>at seqirus.us</a></div>
				</div>
			</div>
			</div>
			<div class="row">
					<div class="col-md-12 footer_txt_disclaimer">
					<spring:theme code="footer.copyright.text.line1" />
					   
					</div>
			</div>
			<div class="row">
					<div class="col-md-12 fotter_bottom_logo">
					
					<c:forEach items="${navigationNodes[1].children[0].links}" var="eachProduct" varStatus="productImageLoop">
												<c:choose>
													<c:when test="${empty eachProduct.url}"><img  src="${eachProduct.media.url}" alt="${eachProduct.media.altText}"> </c:when>
													<c:otherwise>
														<a href="${eachProduct.url}"><img src="${eachProduct.media.url}" alt="${eachProduct.media.altText}" /></a>
													</c:otherwise>
						</c:choose>
					</c:forEach>
				
					</div>
			</div>
		</div>
	</div>
