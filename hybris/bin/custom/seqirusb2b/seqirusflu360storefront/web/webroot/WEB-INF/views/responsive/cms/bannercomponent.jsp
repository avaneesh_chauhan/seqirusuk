<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${component.uid == 'homepage-banner-Component'}">
	<section class="col-xs-12" id="home_loginsection"
		style="background-image: url('${feature.media.url}');">
		<div class="col-xs-12 col-md-7" id="home_loginleftsection">
			<div id="home_leftcontent">
				<div id="home_leftheader">
					<div class="global_Lshape">
						<div class="global_horizontalL"></div>
						<div class="global_verticalL"></div>
					</div>${feature.headline}

				</div>
				<div id="home_leftcontenttext">${feature.h2content}</div>
				<button id="home_leftoverviewbtn">${feature.content}</button>
				<div id="home_leftoverviewtext">${feature.paragraphcontent}</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-5" id="home_loginrightsection">
			<div id="home_rightcontent">
				<form>
					<div class="col-xs-12" id="home_loginheader">Log in to your
						flu360 account</div>
					<div class="col-xs-12">
						<input type="email" id="home_emailfield" /> <label
							for="home_emailfield" class="home_loginlabel">Email
							Address</label>

					</div>
					<div class="col-xs-12">
						<input type="password" id="home_passwordfield" /> <label
							for="home_passwordfield" class="home_loginlabel">Password</label>

					</div>
					<div class="col-xs-12 global_checkboxsection">
						<div class="col-xs-6">
							<label class="checkbox-label"><input type="checkbox"><span
								class="checkbox-custom"></span></label><span class="global_checkboxtext">Remember
								Me</span>
						</div>
						<div class="col-xs-6 global_forgotpasstext">Forgot Password?</div>
					</div>
					<div class="col-xs-12 global_termstext">
						Seqirus Terms of Use have been updated. <span
							class="global_darktext">You must read and agree to the new
							terms</span> before using this portal.
					</div>
					<div class="col-xs-12" id="home_consenttext">
						<label class="checkbox-label"><input type="checkbox"><span
							class="checkbox-custom"></span></label><span class="global_checkboxtext">I
							have read and consent to the terms of use.</span>
					</div>
					<div class="col-xs-12">
						<div class="col-xs-12 col-md-4" id="home_loginbtn">
							<button class="global_redbtn" id="home_loginpadding">Log
								In</button>
						</div>
						<div class="col-xs-12 col-md-8" id="home_logintext">
							Don't have a flu360 account? It's easy to sign up and see the
							benefits.<br> <span class="global_darktext">Create
								Account</span>
						</div>

					</div>
				</form>
			</div>
		</div>
	</section>
</c:if>
<!-- Flu360 Overview Page Components Start -->
<c:if test="${component.uid == 'overview-hero-component'}">
	<div class="hero--overview"
		style="background-image: url('${feature.media.url}');">
		<div class="hero--overview-content">
			<img class="rectangle-down"
				src="${fn:escapeXml(themeResourcePath)}/images/rectangle-down.svg">
			<h1>${feature.headline}</h1>
			<p>${feature.content}</p>
			<a href="${feature.urlLink}"><button
					class="button--hero-solid-red">${feature.h2content}</button></a>
		</div>
	</div>
	<div class="row-flex center-xs">
		<div class="hero--overview-content-mobile hide-desktop">
			<img class="rectangle-down" src="${fn:escapeXml(themeResourcePath)}/images/rectangle-down.svg">
			<h1>${feature.headline}</h1>
			<p>${feature.content}</p>
			<a href="${feature.urlLink}"><button
					class="button--hero-solid-red" onclick="${feature.urlLink}">${feature.h2content}</button></a>
		</div>
	</div>
</c:if>
<c:if test="${component.uid == 'overview-flu-vaccine-component'}">
	<div id="vaccine-row" class="row-flex bg--grey-0 reverse-xs">
		<div class="col-flex-xl-6  hide-desktop">
			<div class="flu-vaccine--callout">
				<img class="rectangle-down" src="${fn:escapeXml(themeResourcePath)}/images/rectangle-down.svg">
				<p>${feature.headline}</p>
				<img class="rectangle-up" src="${fn:escapeXml(themeResourcePath)}/images/rectangle-up.svg">
			</div>
		</div>
		<div class="col-flex-xl-6">
			<img class="flu-vaccine--image" src="${feature.media.url}">
		</div>
		<div class="col-flex-xl-6 hide-flu-desktop">
			<div class="flu-vaccine--callout">
				<img class="rectangle-down" src="${fn:escapeXml(themeResourcePath)}/images/rectangle-down.svg">
				<p>${feature.headline}</p>
				<img class="rectangle-up" src="${fn:escapeXml(themeResourcePath)}/images/rectangle-up.svg">
			</div>
		</div>
	</div>
</c:if>
<c:if test="${component.uid == 'overview-laptop-component'}">
	<div id="laptop-row" class="row-flex">
		<div class="col-flex-xs-6 hide-laptop-desktop">
			<img class="laptop-image-mobile" src="${feature.media.url}">
		</div>
		<div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
			<div class="laptop-paragraph-content">
				<h2>${feature.headline}</h2>
				<p>${feature.content}</p>
				<ul>${feature.paragraphcontent}</ul>
			</div>
		</div>

		<div class="col-flex-xs-6 hide-laptop-mobile">
			<img class="laptop-image" src="${feature.media.url}">
		</div>
	</div>
</c:if>
<c:if test="${component.uid == 'overview-end-hero-component'}">
	<div id="end-hero-row" class="row-flex">
		<div
			class="col-flex-xl-6 col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
			<img class="end-hero--image" src="${feature.media.url}">
		</div>
		<div
			class="col-flex-xl-6 col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
			<div class="end-hero--callout">
				<img class="rectangle-down" src="${fn:escapeXml(themeResourcePath)}/images/rectangle-down.svg">
				<p>${feature.headline}</p>
				<img class="rectangle-up" src="${fn:escapeXml(themeResourcePath)}/images/rectangle-up.svg">
				<a href="${feature.urlLink}"><button onclick="#"
						class="button--solid-red" type="button">
						${feature.h2content}</button></a>
			</div>
		</div>
	</div>
</c:if>
<!-- Flu360 Overview Page Components End -->
<c:if test="${component.uid == 'News-Announcement-Component'}">
	<div class="col-xs-12 dashborad_anoucementssection ">${feature.headline}</div>
	<div class="col-xs-12 margin-T25 dashborad_announceleftcontent ">${feature.paragraphcontent}</div>
	<div class="col-xs-12 margin-T25 dashborad_readmore">
		${feature.content} <i class="global_blackarrow"></i>
	</div>
</c:if>

