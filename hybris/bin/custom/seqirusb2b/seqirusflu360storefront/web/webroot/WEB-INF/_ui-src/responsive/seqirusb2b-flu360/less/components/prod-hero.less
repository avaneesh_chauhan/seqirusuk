.hero--prod{
	background-size: cover;
	background-position: center left;
	background-repeat: no-repeat;
	
	.container{
		background: transparent;
	}
	
	.breadcrumbs--page-nav{
		position: relative;
		background: transparent;
		margin-bottom: 30px;
		@media(min-width:@desktop){
			top: 0;
			left: 0;
			height: auto;
			margin: 0;
		}
		
		p{
			margin-left: 0;
			@media(min-width:@desktop){
				margin: 0;
				padding: 40px 0;
			}
		}
		
		a{
			color: @three-sixty--white;
			
			&:hover{
				color: @three-sixty--white;
			}
		}
		
		span{
			color: @three-sixty--white;
		}
		
		strong{
			color: @three-sixty--white;
			font-family: @site-font;
		}
	}
	
	&__body{
		@media(min-width:@desktop){
			display: flex;
			flex-direction: row-reverse;
		}
	}
	
	&__logo{
		margin-bottom: 50px;
		@media(min-width:@desktop){
			flex: 0 0 50%;
			text-align: right;
		}
	}
	
	&__header{
		font-family: @campton-medium;
		font-size: 40px;
		color: @three-sixty--white;
		letter-spacing: 0;
		text-shadow: 0 2px 24px rgba(0,0,0,0.71);
		margin-bottom: 80px;
		@media(min-width:@desktop){
			margin-bottom: 20px;
		}
	}
	
	&__subheader{
		font-family: @campton-medium;
		font-size: 18px;
		color: #72CDF4;
		letter-spacing: 0;
		line-height: 27px;
		margin-bottom: 24px;
	}
	
	&__approval{
		margin-bottom: 30px;
		
		&.hero--prod__approval-green{
			span{
				&:last-child{
					background-color: #659628;
					border: 1px solid #659628;
					color: @three-sixty--white;
				}
			}
		}
		
		&.hero--prod__approval-orange{
			span{
				&:last-child{
					background-color: #DC7F06;
					border: 1px solid #DC7F06;
					color: @three-sixty--white;
				}
			}
		}
		
		&.hero--prod__approval-white{
			span{
				&:last-child{
					background-color: @three-sixty--white;
					border: 1px solid @three-sixty--white;
					color: #33637A;
				}
			}
		}
		
		span{
			display: inline-block;
			font-family: @campton-semibold;
			font-size: 20px;
			color: @three-sixty--white;
			letter-spacing: 0;
			text-align: center;
			line-height: 1;
			padding: 10px 10px 5px;
			
			&:first-child{
				border: 1px solid @three-sixty--white;
				
				@media(min-width:@desktop){
					border-right: none;
				}
			}
		}
	}
	
	&__content{
		font-family: @campton-semibold;
		font-size: 18px;
		color: @three-sixty--white;
		letter-spacing: 0;
		line-height: 27px;
		margin-bottom: 40px;
	}
	
	&__link{
		display: inline-block;
		font-family: @campton-semibold;
		font-size: 16px;
		letter-spacing: 0;
		text-align: center;
		line-height: 24px;
		padding: 15px 30px;
		
		&:hover{
			text-decoration: none;
		}
		
		&.hero--prod__link-blue{
			color: @grey--110;
			background-color: #72CDF4;
		}
		
		&.hero--prod__link-green{
			color: @three-sixty--white;
			background-color: #659628;
		}
	}
	
	&__info-disclaimer{
		font-family: @site-font;
		font-size: 14px;
		color: @three-sixty--white;
		letter-spacing: 0;
		margin-top: 30px;
		@media(min-width:@desktop){
			max-width: 440px;
		}
	}
	
	&__footer{
		padding-top: 20px;
		padding-bottom: 20px;
		margin-top: 72px;
		color: @three-sixty--white;
		position: relative;
		
		&:before{
			content: "";
			width: 100%;
			height: 100%;
			position: absolute;
			left: 0;
			top: 0;
			background-color: rgba(42, 50, 55, 0.75);
			mix-blend-mode: multiply;
		}
		
		p{
			color: @three-sixty--white;
			@media(min-width:@desktop){
				margin-bottom: 0;
			}
		}
	} 
	
	&__footer-header{
		display: inline-block;
		font-family: @campton-semibold;
	}
	
	&__cpt-content--alt{
		@media(min-width:@desktop){
			display: flex;
		}
		
		.hero--prod__footer-header{
			@media(min-width:@desktop){
				flex: 0 0 285px;
			}
		}
		
		.hero--prod__cpt-twocol{
			@media(min-width:@desktop){
				columns: 2;
			}
		}
	}
	
	.hero-prod__cpt-disclaimers{
		@media(min-width:@desktop){
			display: flex;
			padding-top: 10px;
		}
		
		.hero--prod__cpt-subtitle{
			@media(min-width:@desktop){
				flex: 0 0 61%;
			}
		}
		
		.hero--prod__cpt-disclaimer{
			@media(min-width:@desktop){
				padding-top: 0;
			}
		}
	}
	
	&__cpt-code{
		display: inline-block;
		font-family: @campton-semibold;
		
		&-green{
			color: #659628;
		}
		
		&-orange{
			color: #DC7F06;
		}
		
		&-blue{
			color: #72CDF4;
		}
	}
	
	&__cpt-subtitle{
		font-family: @campton-medium;
		font-size: 14px;
		letter-spacing: 0;
		line-height: 22px;
	}
	
	&__cpt-disclaimer{
		font-family: @campton-medium;
		font-size: 14px;
		letter-spacing: 0;
		line-height: 22px;
		padding-top: 8px;
	}
}