<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
    <div id="influenza-burden-65plus">
        <div class="breadcrumbs--page-nav">
            <div class="container">
                <p>
                    <a href="#">Home</a><span> &gt; </span>
                    <a href="#">flu360 Overview</a><span> &gt; </span>
                    <a href="#">Clinical Support</a><span> &gt; </span>
                    <strong>Influenza Burden 65+</strong>
                </p>
            </div>
        </div>
        <!-- HERO SECTION STARTS -->
        <div class="hero--overview">
            <div class="hero--overview-content">
                <img class="rectangle-down" src="${themeResourcePath}/cms/assets/images/rectangle-down.svg">
                <h1>Influenza can have a devastating impact on adults 65 years and older<sup>1</sup></h1>
                <p>While influenza impacts millions of people in the US,<sup>2</sup> it disproportionately affects adults 65 years and older.<sup>1</sup> <span>flu360<sup>&trade;</sup> </span>fcan you help address this vulnerability among your older patients.</p>
            </div>

        </div>

        <div class="row-flex center-xs">
            <div class="hero--overview-content-mobile hide-desktop">
                <img class="rectangle-down" src="${themeResourcePath}/cms/assets/images/rectangle-down.svg">
                <h1>Influenza impacts millions of people in the US<sup>1</sup></h1>
                <p>While influenza impacts millions of people in the US,<sup>2</sup> it disproportionately affects adults 65 years and older.<sup>1</sup> <span>flu360<sup>&trade;</sup> </span>fcan you help address this vulnerability among your older patients.</p>
            </div>

        </div>
     <!-- HERO SECTION ENDS -->

    <!-- INFLUENZA RESULTS SECTION STARTS -->
    <div class="bg--grey-0 w-100">
        <div class="container">
            <div class="row-flex bg--grey-0">
                <div id="influenza-impacts-header"
                    class="col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-sm-12 text-center">
                    <h2>In the US, influenza impacts adults 65+ with high hospitalization and death rates.<sup>1</sup></h2>
                    <p>The CDC estimates that each year, adults 65+ account for approximately<sup>1</sup>:</p>
                </div>
            </div>
        </div>

         <div class="container">
            <div id="facts-card-row" class="row-flex bg--grey-0">
                <div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
                    <div class="bg--white facts-card">
                        <div class="text-center card-content">
                            <img src="${themeResourcePath}/cms/assets/images/hospital-icon.svg">
                            <p><span>50%-70%</span><br>of influenza-related hospitalizations</p>
                        </div>
                    </div>
                </div>

                <div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
                    <div class="bg--white facts-card">
                        <div class="text-center card-content">
                            <img src="${themeResourcePath}/cms/assets/images/death-icon.svg">
                            <p><span>70%-85%</span><br>of influenza-related deaths</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>




         <div class="col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-xs-12 bg--grey-0">
                <p class="fact-card-footnote">*CDC estimates from 2010-2011 through 2019-2020 US influenza seasons</p>
            </div>
    <!-- INFLUENZA RESULTS SECTION ENDS -->

    <!--  INFLUENZA SEASON SECTION STARTS -->
    <div class="container">
        <div class="row-flex">

            <div id="influnenza-complications-header"
                class="col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-sm-12 text-center">
                <h2>What are the influenza-related complications?</h2>
            </div>
            <div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
                <div class="two-row--card">
                    <div class="two-row--card-content">
                        <div class="two-row--card-top">
                            <p class="lined-header">up to</p>
                            <p class="card-callout">80%</p>
                        </div>
                         <div class="two-row--card-bottom">
                           <p>of <strong>adults 65+</strong> live with <span>multiple chronic health conditions</span>, many of which are associated with increased risk of influenza-related complications.<sup>*3,4</sup> </p>
                        </div>

                    </div>

                </div>
            </div>

            <div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
                <div class="two-row--card bg--white">
                    <div class="two-row--card-content">
                        <div class="two-row--card-top">
                          <p class="text--grey-110"> In the first weeks after influenza infection, adults 65+ have an increased risk of:</p>
                        </div>
                        <hr>
                        <div class="two-row--card-bottom">
                            <p>At least</p>
                            <p class="card-callout">434</p>
                            <p>were children*&dagger;</p>
                        </div>

                    </div>

                </div>
            </div>

            <div class="col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-xs-12 influenza-season-footnotes">
                <p>*2010 Medical Expenditure Panel Survey Data (MEPS) from the Agency for Healthcare Research and Quality (AHRQ)</p>
            </div>
        </div>

    </div>

    <!--  INFLUENZA SEASON SECTION ENDS -->
    <!-- INFLUENZA VACCINE HERO STARTS -->
    <div id="influenza-hero" class="row-flex">
        <div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
            <img  class="influenza-hero-image"src="${themeResourcePath}/cms/assets/images/influenza-vaccine-65-hero.png">
        </div>

         <div class="col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12 influenza-callout-desktop">
             <div class="influenza-hero-container">
                 <img class="rectangle-down" src="${themeResourcePath}/cms/assets/images/rectangle-down.svg">
                 <p class="callout">In many recent seasons, vaccine effectiveness has been suboptimal in adults 65+ compared to younger adults.&dagger;7-10</p>
                 <img class="rectangle-up" src="${themeResourcePath}/cms/assets/images/rectangle-up.svg">
                 <p class="small-callout">
                   <sup>&dagger;</sup>Data estimates from the Centers for Disease Control and Prevention from 2016-2020
                 </p>
             </div>
        </div>

    </div>

    <!-- INFLUENZA VACCINE HERO ENDS-->

    <!-- INFLUENZA BURDEN HERO STARTS  -->
        <div id="influenza-burden-hero" class="row-flex bg--grey-100">
            <div class="col-flex-lg-8 col-flex-md-8 col-flex-sm-12 col-flex-xs-12">
              <h3>
                    We're committed to helping reduce the influenza burden through vaccine innovation designed to improve vaccine effectiveness
                </h3>
                <a href="#">Discover our cell-based technology <img src="${themeResourcePath}/cms/assets/images/arrow-right-white.svg"></a>
            </div>
            <div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12">
                  <img class="cells-360-bg" src="${themeResourcePath}/cms/assets/images/influenza-vaccine-hero-bg.svg">
            </div>

        </div><!-- INFLUENZA BURDEN HERO ENDS  -->

    <!-- VACCINE PORTFOLIO STARTS -->
    <div class="container">
     <div id="vaccine-header-row" class="row-flex">
             <div class="col-flex-12 vacccine-portfolio-header--row">
                  <div class="col-flex-sm-12 header--container">
                     <h2>Explore the Seqirus Vaccine Portfolio<div class="header-line"></div></h2>
                 </div>
             </div>

         </div>

         <div class="row-flex vaccine-portfolio-row">
             <div class="col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                 <div class="logo--card center-xs">
                     <div class="logo--card-container">
                     <img class="fluad-logo" src="${themeResourcePath}/cms/assets/images/logos/fluad-logo.svg">
                     <a href="#" class="no-underline">
                     <p class="text-dark-gray cta ml-20" style="margin-top: 25px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                     </p>
                     </a>
                     </div>
                 </div>
             </div>

             <div class="col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                 <div class="logo--card center-xs">
                     <div class="logo--card-container">
                     <img class="flucelvax-logo" src="${themeResourcePath}/cms/assets/images/logos/flucelvax-logo.svg">
                     <a href="#" class="no-underline">
                     <p class="text-dark-gray cta" style="margin-top: 30px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                     </p>
                     </a>
                     </div>
                 </div>
             </div>

             <div class="col-flex-md-4 col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                 <div class="logo--card center-xs">
                     <div class="logo--card-container">
                     <img class="affluria-logo mt-10" src="${themeResourcePath}/cms/assets/images/logos/affluria-logo.svg">
                     <a href="#" class="no-underline">
                     <p class="text-dark-gray cta ml-10" style="margin-top: 35px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                     </p>
                     </a>
                     </div>
                 </div>
             </div>

             <div class="col-flex-xl-12 col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-xs-12">
                 <p class="disclaimer--paragraph">Please see Important Safety Information and full US Prescribing Information
                     on each vaccine's respective product page.</p>
             </div>
         </div> </div>
    <!-- VACCINE PORTFOLIO ENDS  -->

    <!-- REFERENCES START -->
    <div class="container">
        <div class="row-flex page-references">
            <div class="col-flex-lg=12 col-flex-md-12 col-flex-sm-12 col-flex-xs-12 px-sm-20">
                <p>
                    <strong>References:</strong> <strong>1.</strong> Centers for Disease Control and Prevention. Flu &amp; people 65 years and older. Accessed February 12, 2021. https://www.cdc.gov/flu/highrisk/65over.htm <strong>2.</strong> Centers for Disease Control and Prevention. Disease burden of influenza. Accessed October 20, 2020. https://www.cdc.gov/flu/ about/burden/index.html <strong>3.</strong> Agency for Healthcare Research and Quality. Multiple chronic conditions chartbook: 2010 medical expenditure panel survey data. Published April 2014. Accessed September 12, 2020. https://www.ahrq.gov/sites/default/files/wysiwyg/professionals/prevention-chronic-care/decision/mcc/mccchartbook.pdf <strong>4.</strong> McElhaney JE, Kuchel GA, Zhou X, Swain SL, Haynes L. T-cell immunity to influenza in older adults: a pathophysiological framework for development of more effective vaccines. <i>ront Immunol</i>F. 2016;7:41. doi:10.3389/fimmu.2016.00041 <strong>5.</strong> National Foundation for Infectious Diseases. Call to action: reinvigorating influenza prevention in US adults age 65 and older. Published September 2016. Accessed September 12, 2020. https://www.nfid.org/wp-content/uploads/2019/08/flu-older-adults.pdf <strong>6.</strong> Rothberg MB, Haessler SD, Brown RB. Complications of viral influenza. <i>Am J Med</i>. 2008;121(4):258-264. doi:10.1016/j.amjmed.2007.10.0  <strong>7.</strong> Centers for Disease Control and Prevention. Seasonal influenza vaccine effectiveness, 2016-2017. Accessed October 1, 2019. https://www.cdc.gov/flu/vaccines-work/2016-2017.html <strong>8.</strong> Centers for Disease Control and Prevention. Seasonal influenza vaccine effectiveness, 2017-2018. Accessed October 1, 2019. https://www.cdc.gov/flu/vaccines-work/2017-2018.html <strong>9.</strong> Centers for Disease Control and Prevention. US flu VE data for 2018-2019. Accessed August 19, 2020. https://www.cdc.gov/flu/vaccines-work/2018-2019.html <strong>10.</strong> Centers for Disease Control and Prevention. US flu VE data for 2019-2020. Accessed March 4, 2021. https://www.cdc.gov/flu/vaccines-work/2019-2020.html
                </p>
            </div>
        </div>
    </div>
    <!-- REFERENCES END  -->

    </div>

</template:page>
