<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:if test="${(component.uid == 'overview-whatif-component')}">
	<div id="what-if-row" class="row-flex center-xs">
	<c:forEach items="${banners}" var="banner" varStatus="count">
		<div
			class="col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12 center-content">
			<div class="whatif--card">
				<div class="whatif--card-content">
					<c:if test="${(banner.uid == 'overview-first-whatif-component')}">
						<p class="text--red-100 whatif--card-header">${banner.headline}</p>
						<p class="content">${banner.paragraphcontent}</p>
						<p class="text--red-100 callout">${banner.content}</p>
					</c:if>
					<c:if test="${(banner.uid == 'overview-second-whatif-component')}">
						<p class="text--teal-110 whatif--card-header">${banner.headline}</p>
						<p class="content">${banner.paragraphcontent}</p>
						<p class="text--teal-110 callout">${banner.content}</p>
					</c:if>
					<c:if test="${(banner.uid == 'overview-third-whatif-component')}">
						<p class="text--grey-110 whatif--card-header">${banner.headline}</p>
						<p class="content width--250">${banner.paragraphcontent}</p>
						<p class="text--grey-110 callout">${banner.content}</p>
					</c:if>
					<a href="${banner.urlLink}" class="text-dark-gray cta">${banner.h2content}
						<img src="${fn:escapeXml(themeResourcePath)}/images/arrow-right.svg">
					</a>
				</div>
			</div>
		</div>
	</c:forEach>
	</div>
</c:if>
<c:if test="${(component.uid == 'overview-solution-component')}">
	<div id="solutions-cards-row" class="row-flex center-xs">
		<c:forEach items="${banners}" var="banner" varStatus="count">
			<div
				class="col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-xs-12 center-content">
				<div id="card-${count.index}" class="solutions--card">
					<div class="solutions--btn">
						<div class="solutions--hitarea"></div>
						<img class="expand--btn" src="${fn:escapeXml(themeResourcePath)}/images/card-expand-btn.svg">
						<img class="collapse--btn" src="${fn:escapeXml(themeResourcePath)}/images/collapse-btn.svg">
					</div>
					<div class="solutions--card-container">
						<img class="solutions--card-icon" src="${banner.media.url}">
						<c:if test="${(banner.uid == 'overview-clinical-support-card-component')}">
							<p class="header text--red-100">${banner.headline}</p>
						</c:if>
						<c:if test="${(banner.uid == 'overview-financial-guidance-card-component')}">
							<p class="header text--teal-110">${banner.headline}</p>
						</c:if>	
						<c:if test="${(banner.uid == 'overview-operational-efficiency-card-component')}">
							<p class="header text--grey-110">${banner.headline}</p>
						</c:if>							
						<div class="content">
							<p>${banner.content}</p>
							<div class="content--expand">${banner.paragraphcontent}</div>
							<a href="${banner.urlLink}" class="solutions--card-cta">${banner.h2content}<img
								src="${fn:escapeXml(themeResourcePath)}/images/arrow-right.svg"></a>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</c:if>
<c:if test="${(component.uid == 'Flu360Solutions-CarouselComponent')}">
	<section class="col-xs-12" id="home_solutionssection">
		${component.h2content}
		<div id="home_solutiontabs">

			<c:forEach items="${banners}" var="banner" varStatus="count">
				<div class="home_tabs col-xs-12 col-md-4">
					<div class="home_tabsimg" id="home_tabimg${count.index}"
						style="background-image: url('${banner.media.url}');"></div>
					<div class="home_tabsheader" id="home_tabheader${count.index}">${banner.h2content}</div>
					<div class="home_tabscontent">${banner.paragraphcontent}</div>
				</div>

			</c:forEach>
		</div>
		<div class="col-xs-12 global_greybtncontainer">
			<button class="global_greybtn">${component.paragraphcontent}</button>
		</div>
	</section>
</c:if>
<c:if test="${(component.uid == 'VaccinePortfolio-CarouselComponent')}">
	<section class="col-xs-12" id="home_productssection">
		<div class="col-xs-12 global_headerwithdash">
			<div class="global_dashheader">${component.h2content}</div>
			<div class="global_line"></div>
		</div>
		<div class="col-xs-12" id="home_vaccineportfolio">

			<c:forEach items="${banners}" var="banner" varStatus="count">
				<div class="home_fluad col-xs-12 col-md-4">
					<div class="home_vaccineimg col-xs-12" id="home_img${count.index}"
						style="background-image: url('${banner.media.url}');"></div>
					<div class="home_vaccineheader col-xs-12" id="home_fluadheader">${banner.h2content}<div
							class="global_blackarrow"></div>
					</div>
				</div>
			</c:forEach>

		</div>
		<div class="col-xs-12" id="home_safetyinfo">${component.paragraphcontent}</div>
	</section>

</c:if>

<c:if test="${(component.uid == 'FeaturedResources-CarouselComponent')}">
	<section class="col-xs-12" id="home_resourcessection">
		<div class="col-xs-12 global_headerwithdash" id="home_dashleftpadding">
			<div class="global_dashheader">${component.h2content}</div>
			<div class="global_line"></div>
		</div>
		<div class="col-xs-12 carousel slide " id="home_resourcetabs"
			data-interval="false">
			<ol
				class="carousel-indicators visible-xs visible-sm hidden-md hidden-lg">
				<li data-target="#home_resourcetabs" data-slide-to="0"
					class="active"></li>
				<li data-target="#home_resourcetabs" data-slide-to="1"></li>
				<li data-target="#home_resourcetabs" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner global_mobilecarousel">
				<c:forEach items="${banners}" var="banner" varStatus="count">

					<c:if test="${count.index eq 0}">
						<c:set var="product" value="clinical"></c:set>
					</c:if>
					<c:if test="${count.index eq 1}">
						<c:set var="product" value="financial"></c:set>
					</c:if>
					<c:if test="${count.index eq 2}">
						<c:set var="product" value="clinical2"></c:set>
					</c:if>
					<div class="home_resources item">
						<div class="item__third">
							<div class="home_resourcescontent">
								<div class="home_resourceimg col-xs-12" id="home_${product}img"
									style="background-image: url('${banner.media.url}');"></div>
								<div class="home_resourceheader col-xs-12"
									id="home_${product}header">${banner.headline}</div>
								<div class="home_resourcesubheader col-xs-12"
									id="home_${product}subheader">${banner.h2content}</div>
								<div class="home_resourcetext col-xs-12"
									id="home_${product}text">${banner.paragraphcontent}</div>
								<div class="home_resourcelink col-xs-12"
									id="home_${product}link">${banner.content}<div
										class="global_blackarrow"></div>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<a
				class="left carousel-control visible-xs visible-sm hidden-md hidden-lg"
				href="#home_resourcetabs" data-slide="prev"> </a> <a
				class="right carousel-control visible-xs visible-sm hidden-md hidden-lg"
				href="#home_resourcetabs" data-slide="next"> </a>
		</div>
		<div class="col-xs-12 global_greybtncontainer">
			<button class="global_greybtn">${component.paragraphcontent}</button>
		</div>
	</section>

</c:if>