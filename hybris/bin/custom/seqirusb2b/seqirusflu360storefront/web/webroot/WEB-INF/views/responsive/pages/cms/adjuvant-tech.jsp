<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
    <div id="adjuvant-tech">
        <div class="row-flex hero--section" style="background-image: url('${themeResourcePath}/cms/assets/images/hero-bg-adjuvant-tech.png');">
            <div class="container">
                <div class="breadcrumbs--page-nav">
                    <p>Home > flu360 Overview <strong> > Adjuvant Technology </strong></p>
                </div>
            </div>
            <div class="hero-mobile-img" style="background-image: url('${themeResourcePath}/cms/assets/images/hero-bg-adjuvant-tech.png');"></div>
            <div class="container">
                <div class="col-flex-xl-6 col-flex-lg-6 col-flex-md-6 col-flex-sm-12 col-flex-xs-12">
                    <div class="hero--section-content">
                        <img class="rectangle-down" src="${themeResourcePath}/cms/assets/images/rectangle-down.svg">
                        <div class="content-container text-left">
                            <h1>Adjuvanted influenza vaccine technology</h1>
                            <p><span class="text--red-100">flu360<sup>TM</sup></span> connects you with flu vaccine innovation to address the challenges of the flu that can disproportionately affect adults 65 years and older.1</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row factor-section bg--grey-0">
            <div class="container">
                <h2>Low vaccine effectiveness in adults 65+ is driven largely by 2 factors:</h2>
                <div class="col-md-6">
                    <div class="factor-block">
                        <h3>Weakened Immune System</h3>
                        <p><strong>Adults 65+ experience age-related decline of the immune system</strong> and thus may be less able to mount a sufficient immune response to vaccination, leaving them more vulnerable to influenza infection and its complications.2</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="factor-block">
                        <h3>Strain Mismatch</h3>
                        <p><strong>This occurs when circulating influenza strains do not match the WHO-selected strains</strong> contained in the vaccine. Most often, it is due to mutations from antigenic drift or egg adaptation. It can also occur when the prevalent circulating influenza strains are different from WHO predictions.3,4</p>
                        <div class="note">WHO=World Health Organization</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row senior-section" style="background-image: url('${themeResourcePath}/cms/assets/images/senior-section-bg.png');">
            <div class="senior-mobile-img" style="background-image: url('${themeResourcePath}/cms/assets/images/senior-section-bg.png');"></div>
            <div class="container">
                <div class="col-md-5 col-md-offset-5">
                    <h2>An adjuvanted vaccine is designed to help address challenges of influenza in adults 65+ years</h2>
                    <p>An adjuvant is a substance added to a vaccine to boost the immune response.5</p>
                </div>
            </div>
        </div>

        <div class="row-flex heading-section">
            <div class="container">
                <h2>Adding MF59® Adjuvant to an influenza vaccine is designed to <strong>strengthen, broaden,</strong> and <strong>lengthen</strong> the duration of the immune response6-8</h2>
                <p>This is a conceptualization of the mechanism of action (MOA) of an MF59® adjuvanted influenza vaccine and a standard-dose, non-adjuvanted influenza vaccine.</p>
            </div>
        </div>

        <div class="row diagram-section">
            <div class="container">
                <div class="diagram-heading">MF59® Adjuvanted influenza vaccine6-8</div>
                <div class="slick-wrap">
                    <div class="col-md-4">
                        <div class="diagram-block">
                            <img src="${themeResourcePath}/cms/assets/images/diagram-1a.png"/>
                            <div class="img-caption">Antigen + MF59® Adjuvant</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="diagram-block">
                            <img src="${themeResourcePath}/cms/assets/images/diagram-1b.png"/>
                            <div class="img-caption">Strengthens the immune response by stimulating more immune cells to create more antibodies</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="diagram-block">
                            <img src="${themeResourcePath}/cms/assets/images/diagram-1c.png"/>
                            <div class="img-caption">Broadens the immune response by creating more diverse, cross-reactive antibodies. This may be important if there is a mismatch between the influenza virus strains in the vaccine and the circulating influenza strains</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row diagram-section">
            <div class="container">
                <div class="diagram-heading">Non-adjuvanted, standard-dose influenza vaccine6-8</div>
                <div class="slick-wrap">
                    <div class="col-md-4">
                        <div class="diagram-block">
                            <img src="${themeResourcePath}/cms/assets/images/diagram-2a.png"/>
                            <div class="img-caption">Antigen only</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="diagram-block">
                            <img src="${themeResourcePath}/cms/assets/images/diagram-2b.png"/>
                            <div class="img-caption">Stimulates immune cells</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="diagram-block">
                            <img src="${themeResourcePath}/cms/assets/images/diagram-2c.png"/>
                            <div class="img-caption">Creates fewer antibodies</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row video-section bg--grey-0">
            <div class="container">
                <div class="col-md-5">
                    <h2>An adjuvanted vaccine for seasonal influenza prevention</h2>
                    <p>Adding an adjuvant can make a difference for those patients 65 years and older.1</p>
                    <a href="#">Learn more about our adjuvanted influenza vaccine <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg"/></a>
                </div>
                <div class="col-md-7">
                    <div class="video-caption">Watch how our MF59® Adjuvant works</div>
                    <div class="video-wrap">
                        <img src="${themeResourcePath}/cms/assets/images/video-placeholder.png"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row grey-cell-section">
            <img class="cells-bg" src="${themeResourcePath}/cms/assets/images/cells-bg.png">
            <div class="container">
                <div class="col-md-6">
                    <h2>What’s the impact of influenza on adults 65 years and older?</h2>
                    <p>Influenza vaccine effectiveness continues to be challenging as influenza disproportionately affects adults 65 years and older.1</p>
                    <a href="#">Learn more about the burden of influenza <img src="${themeResourcePath}/cms/assets/images/arrow-right-white.svg"/></a>
                </div>
            </div>
        </div>

        <div class="resource-section">
            <div class="container">
                <div class="row-flex access-financial-resources-header--row">
                    <div class="col-flex-sm-12 header--container clinical-resources-header">
                        <h2>Featured Resources<div class="header-line"></div></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container slick-wrap">
                    <div class="col-md-4">
                        <div class="resources-card">
                            <img class="card-img" src="${themeResourcePath}/cms/assets/images/resource-1.png">
                            <div class="resources-card-container">
                                <p class="resources-card-eyebrow">FLUAD QUADRIVALENT | CLINICAL</p>
                                <p class="resources-card-title">FLUAD QUADRIVALENT Patient Brochure</p>
                                <p class="resources-card-paragraph">Download this brochure to help educate your patients about FLUAD QUADRIVALENT for those 65+ years.</p>
                            </div>
                            <button class="resources-card-btn">
                                <span>Access Resource</span> <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </button>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="resources-card">
                            <img class="card-img" src="${themeResourcePath}/cms/assets/images/resource-2.png">
                            <div class="resources-card-container">
                                <p class="resources-card-eyebrow">FLUAD QUADRIVALENT | CLINICAL</p>
                                <p class="resources-card-title">FLUAD QUADRIVALENT Flashcard</p>
                                <p class="resources-card-paragraph">Download this flashcard to learn more about why FLUAD QUADRIVALENT, with MF59® Adjuvant, is an appropriate option for adults 65+ years.</p>
                            </div>
                            <button class="resources-card-btn">
                                <span>Access Resource</span> <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </button>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="resources-card">
                            <img class="card-img" src="${themeResourcePath}/cms/assets/images/resource-3.png">
                            <div class="resources-card-container">
                                <p class="resources-card-eyebrow">FLUAD QUADRIVALENT | FINANCIAL</p>
                                <p class="resources-card-title">FLUAD QUADRIVALENT Coding and Billing Guide</p>
                                <p class="resources-card-paragraph">Ensure you're up to date with the latest FLUAD QUADRIVALENT coding and billing information.</p>
                            </div>
                            <button class="resources-card-btn">
                                <span>Access Resource</span> <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-flex">
                <div class="col-flex-xs-12 text-center view-resources--row">
                    <button class="button--view-resources-outline-grey">
                        View All Resources
                    </button>
                </div>
            </div>
        </div>

        <div class="container">
            <div id="vaccine-header-row" class="row-flex">
                    <div class="col-flex-12 vacccine-portfolio-header--row">
                         <div class="col-flex-sm-12 header--container">
                            <h2>Explore the Seqirus Vaccine Portfolio<div class="header-line"></div></h2>
                        </div>
                    </div>

                </div>

                <div class="row-flex vaccine-portfolio-row">
                    <div class="col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                        <div class="logo--card center-xs">
                            <div class="logo--card-container">
                            <img class="fluad-logo" src="${themeResourcePath}/cms/assets/images/logos/fluad-logo.svg">
                            <a href="#" class="no-underline">
                            <p class="text-dark-gray cta ml-20" style="margin-top: 25px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </p>
                            </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                        <div class="logo--card center-xs">
                            <div class="logo--card-container">
                            <img class="flucelvax-logo" src="${themeResourcePath}/cms/assets/images/logos/flucelvax-logo.svg">
                            <a href="#" class="no-underline">
                            <p class="text-dark-gray cta" style="margin-top: 30px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </p>
                            </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-flex-md-4 col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                        <div class="logo--card center-xs">
                            <div class="logo--card-container">
                            <img class="affluria-logo mt-10" src="${themeResourcePath}/cms/assets/images/logos/affluria-logo.svg">
                            <a href="#" class="no-underline">
                            <p class="text-dark-gray cta ml-10" style="margin-top: 35px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </p>
                            </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-flex-xl-12 col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-xs-12">
                        <p class="disclaimer--paragraph">Please see Important Safety Information and full US Prescribing Information
                            on each vaccine's respective product page.</p>
                    </div>
                </div>    </div>

        <div class="references-section container">
            <div class="references-content">
                <strong>References.</strong> <strong>1.</strong> Centers for Disease Control and Prevention. Flu & people 65 years and older. Accessed February 12, 2021. https://www.cdc.gov/flu/highrisk/65over.htm <strong>2.</strong> Monto AS, Ansaldi F, Aspinall R, et al. Influenza control in the 21st century: optimizing protection of older adults. Vaccine. 2009;27(37):5043-5053. doi:10.1016/j.vaccine.2009.06.032 <strong>3.</strong> Paules CI, Sullivan SG, Subbarao K, Fauci AS. Chasing seasonal influenza - the need for a universal influenza vaccine. N Engl J Med. 2018;378(1):7-9. doi:10.1056/NEJMp1714916 <strong>4.</strong> Zost SJ, Parkhouse K, Gumina ME, et al. Contemporary H3N2 influenza viruses have a glycosylation site that alters binding of antibodies elicited by egg-adapted vaccine strains. Proc Natl Acad Sci USA. 2017;114(47):12578-12583. doi:10.1073/pnas.1712377114 <strong>5.</strong> Garçon N, Leroux-Roels G, Cheng WF. Vaccine adjuvants. Understanding modern vaccines: Perspectives in vaccinology. 2011;1(1):89-113. <strong>6.</strong> O’Hagan DT, Ott GS, De Gregorio E, Seubert A. The mechanism of action of MF59—an innately attractive adjuvant formulation. Vaccine. 2012;30(29):4341-4348. doi:10.1016/j.vaccine.2011.09.061 <strong>7.</strong> O’Hagan DT, Ott GS, Nest GV, Rappuoli R, Del Giudice G. The history of MF59® adjuvant: a phoenix that arose from the ashes. Expert Rev Vaccines. 2013;12(1):13-30. doi:10.1586/erv.12.140 <strong>8.</strong> Banzhoff A, Pellegrini M, Del Giudice G, Fragapane E, Groth N, Podda A. MF59-adjuvanted vaccines for seasonal and pandemic influenza prophylaxis. Influenza Other Respir Viruses. 2008;2(6):243-249. doi:10.1111/j.1750-2659.2008.00059.x
            </div>
        </div>

    </div>
</template:page>

