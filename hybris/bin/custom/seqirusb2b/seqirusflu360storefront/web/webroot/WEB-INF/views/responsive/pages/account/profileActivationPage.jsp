<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="breadcrumb"
	tagdir="/WEB-INF/tags/responsive/nav/breadcrumb"%>

<template:page pageTitle="${pageTitle}">
	
	<div class="container-fluid maincontent createprofile_container col-xs-12">
		<%-- <div class="pageHeader col-xs-12">
			<spring:theme code="text.headline.createaccount"
				text="CREATE A NEW PROFILE" />
		</div> --%>
		<div class="account-section createprofile_content">
			<div class="createprofile_contentArea col-xs-12">
				<cms:pageSlot position="LeftContentSlot" var="component"
					element="div" class="col-xs-12 col-md-4 createprofile_leftimagecontainer">
					<%-- <div class="loginleftimage"
						style="background-image: url(${component.media.url})"> --%>
						<h1 class="createprofile_leftHeader col-xs-12 col-md-8 col-sm-9">${component.headline}</h1>
						<p class="createprofile_leftsubHeader col-xs-12 col-md-8 col-sm-9">${component.content}</p>
					
					<cms:component component="${feature}" />
				</cms:pageSlot>

				<cms:pageSlot position="BodyContent" var="feature" element="div"
					class="account-section-content createprofile_cdccontent col-xs-12 col-md-8">
					<div id="breadcrumb" class="col-xs-12 col-md-6">
						<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}" />
					</div>
					<div class="col-xs-12 col-md-6 createprofile_helptext">
					
						<span class="createprofile_helplighttext"><spring:theme code="createprofilepage.support.text1"/></span>
						<a href="/seqirusflu360storefront/support"><span class="createprofile_helpdarktext"><spring:theme code="createprofilepage.support.text2"/></span><div class="global_blackarrow"></div></a>
					</div>
					<cms:component component="${feature}" />
				</cms:pageSlot>
				<cms:pageSlot position="BottomContent" var="feature" element="div"
					class="accountPageBottomContent">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
</template:page>