<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<template:page pageTitle="${pageTitle}">
<cms:pageSlot position="SideContent" var="feature" element="div"
			class="col-xs-12 col-md-2 col-lg-2  sidebar hidden-xs hidden-sm visible-md-* visible-lg-*">
			<cms:component component="${feature}" />
		</cms:pageSlot>
		
		<main>
		<body>
		<div class="col-xs-12 col-md-10 col-lg-10  main">
                <section class="col-xs-12 cart_titlesection">
                    <div class="col-xs-12 col-md-8">
                        <div class="cart_header">My Cart</div>
                        <div class="cart_subheader">One or more items in <span class="global_underline">your account</span> are pending confirmation.</div>
                    </div>
                    <div class="col-xs-12 col-md-4 cart_dateheader">2021-2022 Season Begins<br/><span>October 1, 2021</span></div>
                </section>
                <section class="col-xs-12 cart_neworder">
                    <div class="col-xs-12 cart_neworderheader">New Order</div>
                    <div class="col-xs-12 cart_neworderbtns">
                        <div class="col-xs-4 cart_neworderbtn" id="cart_neworderbtn-1"><div class="col-xs-12 cart_neworderbtnheader">Reserve for</div><div class="col-xs-12 cart_neworderbtndate">2022 - 2023 Season</div></div>
                        <div class="col-xs-4 cart_neworderbtn" id="cart_neworderbtn-2"><div class="col-xs-12 cart_neworderbtnheader">Order for</div><div class="col-xs-12 cart_neworderbtndate">2021 - 2022 Season</div></div>
                        <div class="col-xs-4 cart_neworderbtn" id="cart_neworderbtn-3"><div class="col-xs-12 cart_neworderbtnheader">Order via</div><div class="col-xs-12 cart_neworderbtndate">File Upload</div></div>
                    </div>
                    <div class="global_selectdivmob">	
                        <select name="cart_neworderselect" id="cart_neworderselect">
                            <option value="Select New Order Type" id="cart_neworderselect-0">Select New Order Type</option>
                            <option value="2022 - 2023 Season" id="cart_neworderselect-1">2022 - 2023 Season</option>
                            <option value="2021 - 2022 Season" id="cart_neworderselect-2">2021 - 2022 Season</option>
                            <option value="File Upload" id="cart_neworderselect-3">File Upload</option>
                        </select>
                    </div>
                    <div class="col-xs-12 cart_neworderreorder">Reorder previous season quantities<div class="global_blackarrow"></div></div>
                </section>
                <section class="col-xs-12 col-md-11 cart_selectlocations" id="cart_selectlocation-1">
                    <div class="col-xs-12 col-md-6 cart_selectheader">Select Locations</div>
                    <div class="col-xs-12 col-md-6 cart_selectcount">Ordering to: <span class="cart_loccount">0</span> <span>Locations</span></div>
                    <div class="col-xs-12 cart_selectline"></div>
                    <div class="col-xs-12 cart_selectoptions">
                        <div class="cart_customsearch">
                            <i class="global_searchicon"></i><input type="search" placeholder="Search for location" id="cart_searchbox"><button>Search</button>
                        </div>
                        <div class="cart_selectunselect">
                            <div class="cart_selectoption" id="cart_selectall">Select All</div><div class="cart_selectdivision"></div><div class="cart_selectoption" id="cart_clearall">Clear Selections</div>		
                        </div>
                    </div>
                    <!-- Table starts -->
                    <div class="tablecontainer"> 
                        <table id="cart_landingTable-1" class="display cart_landingtable">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th ></th>
                                </tr>
                            </thead> 
                        </table>
                    </div>
                    <!-- Table Ends -->
                    <div class="col-xs-12 cart_startorderbtnsection">
                        <button class="cart_startorderbtn" disabled>Start Order</button>
                    </div>
                </section>
                <section class="col-xs-12 col-md-11 cart_selectlocations" id="cart_selectlocation-2">
                    <div class="col-xs-12 col-md-6 cart_selectheader">Select Locations</div>
                    <div class="col-xs-12 col-md-6 cart_selectcount">Ordering to: <span class="cart_loccount"> 0</span> <span>Locations</span></div>
                    <div class="col-xs-12 cart_selectline"></div>
                    <div class="col-xs-12 cart_selectoptions">
                        <div class="cart_customsearch">
                            <i class="global_searchicon"></i><input type="search" placeholder="Search for location" id="cart_searchbox"><button>Search</button>
                        </div>
                        <div class="cart_selectunselect">
                            <div class="cart_selectoption" id="cart_selectall">Select All</div><div class="cart_selectdivision"></div><div class="cart_selectoption" id="cart_clearall">Clear Selections</div>		
                        </div>
                    </div>
                    <!-- Table starts -->
                    <div class="tablecontainer"> 
                        <table id="cart_landingTable-2" class="display cart_landingtable">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                     <th></th>
                                </tr>
                            </thead> 
                        </table>
                    </div>
                    <div class="col-xs-12 cart_startorderbtnsection">
                        <button class="cart_startorderbtn" disabled>Start Order</button>
                    </div>
                    <!-- Table Ends -->
                </section>

                <!-- Add product DIV starts -->
                <div class="col-xs-12" id="cart_section">
                    <div class="col-xs-12 col-md-12 add_cart_wrapper" id="cart-wrapper-main">
                        <div class="row">
                            <div class="col-md-12 border_cart">
                                <div class="col-md-12 back_txt"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;<a href="#" title="Back to location selection">Back to location selection</a></div>

                                <div class="col-md-12 order-listing nopad_L_R">
 
                                   <!--Add Products list in the cart starts here -->
                                   <c:url value="/addtoCart" var="reviewcart" />
		<form:form action="${reviewcart}" modelAttribute="orderReviewForm"	method="post" id="reviewform" onkeypress="return event.keyCode != 13;" autocomplete="off">
	
                                    <c:forEach items="${products}" var="vari" varStatus="prod">
								<div class="product_according nopad_L_R" id="product-${vari.code}">
									<div class="col-md-11 nopad_L_R left-pro-list">
										<div class="col-md-2 pro_desc hidden-xs hidden-sm visible-md-* visible-lg-*"><img alt="FLUCELVAX Logo" src="ui\responsive\theme-seqirusb2b-us\images\FLUCELVAXLogoCart.png" width="90%"></div>
										<div class="col-md-4 col-xs-10 pro_desc"> QUADRIVALENT</div>
										<div class="col-xs-2 nopad_L_R del_section hidden-md hidden-lg visible-xs-* visible-sm-*"></div>
										<div class="col-md-3 pro_desc col-xs-9">${vari.presentationName}</div>
										<div class="col-md-2 pro_desc pro_qty hidden-xs hidden-sm visible-md-* visible-lg-* notvisible-ipad"><strong>Total Quantity: <span class="count_qty">0</span></strong></div>
										<div class="col-md-2 col-xs-3 pro_desc pro_qty hidden-md hidden-lg visible-xs-* visible-sm-* visible-ipad"><strong>Qty: <span class="count_qty">0</span></strong></div>
										
										<div class="col-md-1 pro_desc col-xs-12 hidden-xs hidden-sm visible-md-* visible-lg-*"><i class="fa fa-angle-down show_pro_details" aria-hidden="true" ></i></div>
									</div>
									<div class="col-md-1 nopad_L_R del_section "><i class="fa fa-trash-o" aria-hidden="true" data-button-id="addBtn-${vari.code}"></i></div>
									<div class="col-md-12 pro_calc_wrapper">
									  <div class="row">
										<div class="col-md-3 spcl_3"><a href="#" class="cart_pdf_link hidden-xs hidden-sm visible-md-* visible-lg-*">Prescribing Information</a></div>
										<div class="col-md-3"><a href="#" class="cart_pdf_link hidden-xs hidden-sm visible-md-* visible-lg-*">Important Safety Information</a></div>
										<div class="col-md-3"><a href="#" class="cart_pdf_link hidden-xs hidden-sm visible-md-* visible-lg-*">Coding & Billing Guide</a></div>
										<div class="col-md-3 col-xs-12 cart_doses_link">${vari.priceData.formattedValue} per unit&nbsp;&nbsp;|&nbsp;&nbsp;10 doses per unit</div>
									  </div>
									<form:hidden  path ="orderReviewdata[${prod.index}].productCode"  id="product_${vari.code}" value="${vari.code}" />
									
									 <c:forEach items="${shippingData}" var="ship" varStatus="shipping">
									
									  <div class="col-md-12 product_calc locationrow${ship.locID} hide" >
										<div class="col-md-2">${ship.locname}</div>
										<div class="col-md-4">${ship.address}</div>
										<div class="col-md-4">${ship.state}</div>
										<div class="col-md-2">
										<form:input path ="orderReviewdata[${prod.index}].shipLocationQty[${shipping.index}].locQty" type="text" class="form-control qty-messure" id="qty_product${vari.code}${ship.locID}" placeholder="0"/></div>
									    <form:hidden  path ="orderReviewdata[${prod.index}].shipLocationQty[${shipping.index}].locID"  id="ship_${vari.code}${ship.locID}" value="${ship.locID}"/>
									  </div>
									  
									
									  <div class="col-md-12 total_count locationrow${ship.locID} hide">
									    <div class="col-md-10 col-xs-8 no-padding-left total-order-txt"><strong>Total Cost :</strong></div>
										<div class="col-md-2 col-xs-4 no-padding-right total_cost_item"><strong>$&nbsp<span class="total_amnt">0</span></strong></div>
									  </div>
									  
									    </c:forEach>
									 
										<div class="clearfix"></div>
									  <div class="row hidden-md hidden-lg visible-xs-* visible-sm-* mob-prescribe">
										<div class="col-md-3 spcl_3"><a href="#" class="cart_pdf_link">Prescribing Information</a></div>
										<div class="col-md-3"><a href="#" class="cart_pdf_link">Important Safety Information</a></div>
										<div class="col-md-3"><a href="#" class="cart_pdf_link">Coding & Billing Guide</a></div>
									  </div>
									  
									</div>
									<div class="col-md-1 pro_desc col-xs-12 hidden-md hidden-lg visible-xs-* visible-sm-* notvisible-ipad mobile-according"><span><i class="fa fa-angle-down show_pro_details" aria-hidden="true" ></i></span></div>
								</div>
								
								
								</c:forEach>
								
								
								
								<!--Add Products list in the cart ends here -->

                                </div>


                               <div class="col-md-12 top-b"></div>
							
							<div class="col-md-10 nopad_L_R">
								<div class="add_product_btn add_product_btn12">Add Product <i class="fa fa-plus-circle" aria-hidden="true"></i></div>
								<div class="col-md-12 cart_list_wrap">
							
								 <c:forEach items="${products}" var="vari" varStatus="prod">
								<div class="col-md-12 prod_list active_stage" id="addBtn-${vari.code}">
									<div class="col-md-4 nopad_L_R"> QUADRIVALENT</div>
									<div class="col-md-6 nopad_L_R">${vari.presentationName}</div>
									<div class="col-md-2 nopad_L_R"><span class="add_pro_cart" data-product-id="product-${vari.code}" >ADD&nbsp;&nbsp;<i class="fa fa-plus-circle" aria-hidden="true"></i></span></div>
								</div>
								</c:forEach>
							
							</div>
							</div>
							<div class="col-md-2 nopad_L_R mob_border">
								<div class="btn-confirm rvw_btn col-xs-7 col-md-12">Review & Place Order</div>
							</div>
							
						</div>

                        </div>



                    </div>
                     </form:form>
                    <!-- Add product DIV ends -->
                </div>
                <!-- summary DIV starts -->
                <section class="col-xs-12 col-md-11 cart_selectlocations" id="cart_summaryLocation">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart_summary">                       
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart_summaryContentbox ">
                            <div class="cart_summaryProductselection"><div class="global_blackarrow"></div>Back to Product Selection</div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart_summarytable no-padding">
                                <table id="cart_summaryProductTable" style="width:100%;">
                                    <thead>
                                        <tr>                                           
                                        </tr>
                                    </thead>
                                    <tbody>                                       
                                    </tbody>
                                </table>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart_summaryProductTableMob">

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart_summaryborder"></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart_summaryContentbox ">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cart_summarybilling no-padding-left">
                                <div class="cart_summarybillingHeader">Billing Location</div>
                                <div class="cart_summarybillingaddressline" id="cart_summarybillingaddressline1"></div>
                                <div class="cart_summarybillingaddressline" id="cart_summarybillingaddressline2"></div>
                                <div class="cart_summarybillingtext">Your billing location is where invoices will be sent. Any billing location edits may take several business days to verify and may impact ability to place orders.</div>                               
                                <div class="cart_summaryupdatebilling">Update Billing Location<div class="global_blackarrow"></div></div>                                
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cart_summarypayer no-padding-right">
                                <div class="cart_summarypayerHeader">Payer Location</div>
                                <div class="cart_summarypayeraddressline" id="cart_summarypayeraddressline1"></div>
                                <div class="cart_summarypayeraddressline" id="cart_summarypayeraddressline2"></div>
                                <div class="cart_summarypayertext">Your payer location is where all account statements will be sent, and can be the same as your billing location. Any payer location edits may take several business days to verify and may impact ability to place orders.</div>
                                <div class="cart_summaryupdatepayer">Update Payer Location<div class="global_blackarrow"></div></div>                               
                            </div>
                            <div class="clearboth"></div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cart_summarybilling no-padding-left">
                                <div class="cart_summaryPO">Enter PO Numbers</div>
                                <div class="cart_summaryPOlabel">PO NUMBER</div>
                                <div class="cart_summaryPOinput">
                                    <input type="text" class="cart_summaryPOinputbox"/>
                                    <div class="cart_summaryPOautogenerate">Autogenerate PO</div>
                                </div>
                                <!--<div class="cart_summaryAdd">Add<i class="fa fa-plus-circle"></i></div>-->
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cart_summarypayer no-padding-right">
                                <div class="cart_summaryorderTotal">Order Total</div>
                                <div class="cart_summarysubTotal">
                                    <div class="cart_summarysubTotallabel cart_summaryLabel">Subtotal:</div>
                                    <div class="cart_summarysubTotalno cart_summarySum"></div>
                                </div>
                                <div class="cart_summaryexcise">
                                    <div class="cart_summaryexciselabel cart_summaryLabel">Federal Excise Tax:</div>
                                    <div class="cart_summaryexciseno cart_summarySum"></div>
                                </div>
                                <div class="cart_summarytotal">
                                    <div class="cart_summarytotallabel cart_summaryLabel">Total Cost:</div>
                                    <div class="cart_summarytotalno cart_summarySum"></div>
                                </div>
                            </div>
                            <div class="clearboth"></div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cart_summarybilling no-padding-left">
                                <div class="cart_summaryshipping">Shipping Timeframes</div>
                                <div class="cart_summaryshippingtext">Your <a href="">Orders</a> page offers the most recent shipping information as shipping dates approach.</div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cart_summarypayer no-padding-right">
                                <div class="cart_summaryexpected">Expected Shipping for <span>2021-2022</span></div>
                                <div class="cart_summaryexpectedtext">Shipping for the <span>2021-2022</span> season is expected to begin <span>July 2021</span>. Deliveries are prioritized based on when they were ordered. Please refer to your <a href="">Orders</a> page for the most recent shipping information and shipping dates.</div>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart_summaryborder"></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart_summaryContentbox ">
                            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 cart_summarybilling no-padding-left">
                                <div class="cart_summarycheckbox">
                                    <input type="checkbox" class="cart_summarycheckedbox" id="cart_summarycheckbox1">
                                    <label> By placing this order, I am confirming that I have read and agree to the <a href="" target="_blank">Terms and Conditions of Sale</a>.</label>
                                </div>
                                <div class="cart_summarycheckbox">
                                    <input type="checkbox" class="cart_summarycheckedbox" id="cart_summarycheckbox2" >
                                    <label> By placing this order, I am confirming that I have read and agree to the <a href="" target="_blank">Terms and Conditions of Returnability</a>.</label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 cart_summarypayer no-padding-right">
                                <button class="cart_summaryPlaceorder inactive">Place Order</button>
                            </div>
                            <div class="clearboth"></div>
                        </div>
                    </div>
                </section>
                <section class="col-xs-12 col-md-11 cart_thankyousection">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart_thankyou">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart_thankyouContentbox ">
                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 cart_Thankyouheader">Thank You for Submitting Your Order</div>
                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 cart_Thankyoutext"><p>Your order is confirmed! Please note, if you placed an order immediately after creating your flu360 account or after adding a new location, the status of your order will not be available until your account information is verified, which takes 5-7 business days.</p>
                                <p>Once verified, you will be able to see your order information in your account on your <a href="">Orders</a> page, as well as further shipping details and exact shipping dates as the shipment window approaches. Thank you for your partnership in the fight against influenza!</p>
                            </div>  
                            <button class="cart_thankyouButton">View Orders</button>
                        </div>                       
                    </div>
                </section>
                <!-- summary DIV starts -->
                </body>
        </main>
		
</template:page>
