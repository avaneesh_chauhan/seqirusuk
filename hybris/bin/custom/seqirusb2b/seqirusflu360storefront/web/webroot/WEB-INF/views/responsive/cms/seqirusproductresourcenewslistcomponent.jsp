<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:forEach items="${component.resourceContentList}" var="comp">
	<c:if test="${empty comp.media.url}">
		<div class="content-section marBottom40">
                  <div class="content-head">${comp.h2content}</div>
                  <div class="row marBottom40">
                  <div class="col-md-7">
                 ${comp.paragraphcontent}
                  </div>
                  <div class="col-md-3 pos-mid"><a href="${comp.mediaForpdf.downloadurl}" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                  <div class="col-md-2 pos-mid"><a href="${comp.seqMediaForpdf.url}" class="download_txt">View PDF</a></div>
                  </div> 
        </div>
	</c:if>
</c:forEach>

 