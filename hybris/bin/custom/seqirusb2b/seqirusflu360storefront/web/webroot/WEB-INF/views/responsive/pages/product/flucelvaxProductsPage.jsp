<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<template:page pageTitle="${pageTitle}">

<div id="flucelvax">
	<div class="hero--prod" style="background-image: url(../_ui/responsive/theme-lambda/images/flucelvax-hero.png)">
		<div class="breadcrumbs--page-nav">
			<div class="container">
				<p>
					<a href="#">Home</a><span> > </span>
					<a href="#">Products</a><span> > </span>
					<strong>FLUCELVAX QUADRIVALENT</strong>
				</p>
			</div>
		</div>	<div class="container">
			<div class="hero--prod__body">
				<div class="hero--prod__logo">
					<img src="../_ui/responsive/theme-lambda/images/flucelvax-logo-white.svg" alt="product logo">
				</div>
				<div class="hero--prod__info">
					<h1 class="hero--prod__header header--2">Choose a cell-based vaccine for prevention of seasonal influenza</h1>
					<p class="hero--prod__approval hero--prod__approval-orange"><span>APPROVED FOR PATIENTS</span><span>2+ years<sup>1</sup></span></p>
					<p class="hero--prod__content">100+ million estimated doses distributed over 5 US influenza seasons<sup>2</sup></p>
					<a class="hero--prod__link hero--prod__link-green" href="#">Order Today</a>
				</div>
			</div>
		</div>
		<div class="hero--prod__footer">
			<div class="container">
					<div class="hero--prod__cpt-content">
						<p class="hero--prod__footer-header">Reimbursed through CPT codes: </p>
							<p class="hero--prod__cpt-code"><span class="hero--prod__cpt-code-orange">90674</span> - Single-Dose Syringe | </p>
							<p class="hero--prod__cpt-code"><span class="hero--prod__cpt-code-orange">90756</span> - Multi-Dose Vial</p>
					</div>
					<p class="hero--prod__cpt-subtitle">Covered by Medicare Part B, Vaccines for Children (VFC) program, and by most major health plans.<sup>*3</sup></p>
					<p class="hero--prod__cpt-disclaimer"><sup>*</sup>This information does not constitute a guarantee or warranty of coverage benefits or reimbursement</p>
			</div>
		</div>
	</div>	<div class="prod-sbs">
		<div class="container">
			<div class="prod-sbs__left">
				<h3 class="prod-sbs__header header--3">The first-and-only FDA-approved cell-based influenza vaccine in the US<sup>1,4</sup></h3>
				<a href="#" class="prod-sbs__link text--grey-110 cta"><span>Learn more about cell-based technology</span><img src="../_ui/responsive/theme-lambda/images/arrow-right.svg"></a>
				<div class="prod-sbs__key-features">
					<span class="prod-sbs__key-features-header">Key Features</span>
					<ul class="prod-sbs__key-features-list">
							<li>Proven effective in patients 2 through 17 years<sup>1</sup></li>
							<li>Demonstrated safety profile in patients 2 years and older<sup>1</sup></li>
							<li>Proven noninferior to FLUCELVAX� (Influenza Vaccine) based on immunogenicity and seroconversion for patients 4 years and older<sup>1</sup></li>
							<li>Contains no egg protein and no antibiotics<sup>1</sup></li>
							<li>0.5-mL pre-filled syringes contain no preservative; 5-mL multi-dose vials contain thimerosal, a mercury derivative, added as a preservative<sup>1</sup></li>
					</ul>
				</div>
			</div>
			<div class="prod-sbs__right">
				<p class="prod-sbs__img"><img alt="product image" src="../_ui/responsive/theme-lambda/images/flucelvax-box.png"></p>
			</div>
		</div>
	</div>	<section class="prod-tabs">
		<div class="container">
		    <div class="prod-tabs__tab-nav">
		        <ul>
						<li class="active">
							<a data-tab="#tab-0" href="">Safety Data
								<div class="prod-tabs__arrow">
									<img role="presentation" src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
									<span class="sr-text">Click to expand or contract content</span>
								</div>
							</a>
						</li>
						<li class="">
							<a data-tab="#tab-1" href="">Dosing and Administration
								<div class="prod-tabs__arrow">
									<img role="presentation" src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
									<span class="sr-text">Click to expand or contract content</span>
								</div>
							</a>
						</li>
						<li class="">
							<a data-tab="#tab-2" href="">Storage and Handling
								<div class="prod-tabs__arrow">
									<img role="presentation" src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
									<span class="sr-text">Click to expand or contract content</span>
								</div>
							</a>
						</li>
		        </ul>
		    </div>
		    <div class="prod-tabs__tab-content">
					<div class="prod-tabs__tab active" id="tab-0">
						<div class="prod-tabs__header">
							<h4>
								<a href="">
									Safety Data
									<div class="up-down">
										<img role="presentation" src="../_ui/responsive/theme-lambda/images/chevron-up.svg">
										<span class="sr-text">Click to expand or contract content</span>
									</div>
								</a>
							</h4>
						</div>
						<div class="prod-tabs__body">
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p><p>Epsum factorial non deposit quid pro quo hic escorol. Olypian quarrels et gorilla congolium sic ad nauseum. Souvlaki ignitus carborundum e pluribus unum. Defacto lingo est igpay atinlay. Marquee selectus non provisio incongruous feline nolo contendre. Gratuitous octopus niacin, sodium glutimate. Quote meon an estimate et non interruptus stadium. Sic tempus fugit esperanto hiccup estrogen. Glorious baklava ex librus hup hey ad infinitum. Non sequitur condominium facile et geranium incognito. Epsum factorial non deposit quid pro quo hic escorol. Marquee selectus non provisio incongruous feline nolo contendre Olypian quarrels et gorilla congolium sic ad nauseum. Souvlaki ignitus carborundum e pluribus unum.</p>
						</div>
					</div>
					<div class="prod-tabs__tab" id="tab-1">
						<div class="prod-tabs__header">
							<h4>
								<a href="">
									Dosing and Administration
									<div class="up-down">
										<img role="presentation" src="../_ui/responsive/theme-lambda/images/chevron-up.svg">
										<span class="sr-text">Click to expand or contract content</span>
									</div>
								</a>
							</h4>
						</div>
						<div class="prod-tabs__body">
							<p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, li tot Europa usa li sam vocabularium. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilita; de un nov lingua franca: on refusa continuar payar custosi traductores. It solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles.</p>
						</div>
					</div>
					<div class="prod-tabs__tab" id="tab-2">
						<div class="prod-tabs__header">
							<h4>
								<a href="">
									Storage and Handling
									<div class="up-down">
										<img role="presentation" src="../_ui/responsive/theme-lambda/images/chevron-up.svg">
										<span class="sr-text">Click to expand or contract content</span>
									</div>
								</a>
							</h4>
						</div>
						<div class="prod-tabs__body">
							Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.
						</div>
					</div>
			</div>
		</div>
	</section>	<section class="prod-sb" style="background-color: #1D8912;">
		<div class="container prod-sb__body">
			<div class="prod-sb__left">
				<img class="prod-sb__img" alt="product logo" src="../_ui/responsive/theme-lambda/images/affluria-logo-white.svg">
			</div>
			<div class="prod-sb__right prod-sb__right--light">
				<h2>Choose cell-based FLUCELVAX QUADRIVALENT for your eligible patients 2+ years<sup>1</sup></h2>
				<a class="prod-sb__link" href="#">Order Now</a>
			</div>
		</div>
	</section>	<div class="resources">
		<div class="container">
			<div class="row-flex carousel-container-row">
				 <div class="row-flex access-financial-resources-header--row">
					 <div class="col-flex-sm-12 header--container">
						<h2>Featured Resources<div class="header-line"></div></h2>
					</div>
			   </div>
			</div>
	
			<!-- 5. CLINICAL RESOURCES CARDS STARTS  DESKTOP-->
			<div id="clinical-resources-financial" class="row-flex carousel--three-card resources-card--row">
					<div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12 cards-pl-0-mobile">
						<div class="resources--card">
							<img src="../_ui/responsive/theme-lambda/images/flucelvax-patient-brochure.png">
							<div class="resources--card-container">
								<p class="resources--card-eyebrow">FLUCELVAX QUADRIVALENT | CLINICAL</p>
								<p class="resources--card-title">FLUCELVAX� QUADRIVALENT Patient Brochure</p>
								<p class="resources--card-paragraph">Educate patients about vaccination with FLUCELVAX QUADRIVALENT (Influenza Vaccine).</p>  
							</div>
	
							<button class="resources--card-btn">
							<span>Access Resource</span> <img src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
							</button>
						</div>
					</div>
					<div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12 cards-pl-0-mobile">
						<div class="resources--card">
							<img src="../_ui/responsive/theme-lambda/images/flucelvax-flashcard.png">
							<div class="resources--card-container">
								<p class="resources--card-eyebrow">FLUCELVAX QUADRIVALENT | CLINICAL</p>
								<p class="resources--card-title">FLUCELVAX� QUADRIVALENT Informational Flashcard</p>
								<p class="resources--card-paragraph">Key information highlighting the features of FLUCELVAX QUADRIVALENT (Influenza Vaccine).</p>  
							</div>
	
							<button class="resources--card-btn">
							<span>Access Resource</span> <img src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
							</button>
						</div>
					</div>
					<div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12 cards-pl-0-mobile">
						<div class="resources--card">
							<img src="../_ui/responsive/theme-lambda/images/flucelvax-coding-billing.png">
							<div class="resources--card-container">
								<p class="resources--card-eyebrow">FLUCELVAX QUADRIVALENT | FINANCIAL</p>
								<p class="resources--card-title">FLUCELVAX� QUADRIVALENT Coding and Billing Guide</p>
								<p class="resources--card-paragraph">Quick reference guide for coding and billing information specific to FLUCELVAX QUADRIVALENT (Influenza Vaccine).</p>  
							</div>
	
							<button class="resources--card-btn">
							<span>Access Resource</span> <img src="../_ui/responsive/theme-lambda/images/arrow-right.svg">
							</button>
						</div>
					</div>
			</div>
	
			<!-- 5. CLINICAL RESOURCES CARDS ENDS  -->
			<!-- VIEW ALL RESOURCES ROW STARTS -->
			<div class="row-flex ">
				<div class="col-flex-xs-12 text-center view-resources--row">
					 <button class="button--view-resources-outline-grey">
								View All Resources
							</button>
				</div>
	
			</div>
			 <!--  VIEW ALL RESOURCES ROW ENDS -->
		</div>
	</div>	<!-- begin 50 percent speedbump -->
	<section class="bg--grey-100 speedbump speedbump--50">
		<div class="container">
			<div class="speedbump__content">
				<h3 class="header--3">Influenza impacts millions of people in the US<sup>5</sup></h3>
					<a href="#" class="text--white cta">Learn about the burden of influenza<img src="../_ui/responsive/theme-lambda/images/arrow-right-white.svg">
					</a>
			</div>
		</div>
	</section>
	<!-- end 50 percent speedbump -->	<div class="prod-portfolio">
		<div class="container">
			<h2 class="prod-portfolio__heading">Explore Other Flu Vaccines in the Seqirus Portfolio<div class="header-line"></div></h2>
			<div class="prod-portfolio__body">
					<div class="prod-portfolio__card">
						<img src="../_ui/responsive/theme-lambda/images/affluria-logo.svg">
						<a href="${contextPath}/products/afluria" class="no-underline">
							<p class="text-dark-gray cta ml-20">Explore <img src="../_ui/responsive/theme-lambda/images/arrow-right.svg"></p>
						</a>
					</div>
					<div class="prod-portfolio__card">
						<img src="../_ui/responsive/theme-lambda/images/fluad-logo.svg">
						<a href="${contextPath}/products/fluad" class="no-underline">
							<p class="text-dark-gray cta ml-20">Explore <img src="../_ui/responsive/theme-lambda/images/arrow-right.svg"></p>
						</a>
					</div>
			</div>
			<div class="prod-portfolio__disclaimer">
				<p class="disclaimer--paragraph">Please see Important Safety Information and full US Prescribing Information on each vaccine&#x27;s respective product page.</p>
			</div>
		</div>
	</div>	<div id="safetyInfoAnchor"></div>
		<section id="safetyInfo" class="safety-info sbs is-sticky">
			<div class="container">
				<div class="safety-info__header">
					<button class="safety-info__btn">MORE <img role="presentation" src="../_ui/responsive/theme-lambda/images/plus.svg"></button>
				</div>
				<div class="sbs__body">
					<div class="sbs__left">
						<p><strong>IMPORTANT SAFETY INFORMATION for FLUAD� (Influenza Vaccine, Adjuvanted), FLUAD� QUADRIVALENT (Influenza Vaccine, Adjuvanted), AFLURIA� QUADRIVALENT (Influenza Vaccine), and FLUCELVAX� QUADRIVALENT (Influenza Vaccine)</strong></p>
	
						<p><strong>CONTRAINDICATIONS</strong></p>
	
						<p>Do not administer FLUAD, FLUAD QUADRIVALENT, or AFLURIA QUADRIVALENT to anyone with a history of severe allergic reaction (e.g. anaphylaxis) to any component of the vaccine, including egg protein, or to a previous influenza vaccine. Do not administer FLUCELVAX QUADRIVALENT to anyone with a history of severe allergic reactions (e.g. anaphylaxis) to any component of the vaccine.</p>
	
						<p><strong>WARNINGS AND PRECAUTIONS</strong></p>
	
						<p>If Guillain-Barr� syndrome (GBS) has occurred within 6 weeks of receipt of prior influenza vaccine, the decision to give FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT or FLUCELVAX QUADRIVALENT should be based on careful consideration of the potential benefits and risks.</p>
						
						<p>Appropriate medical treatment and supervision must be available to manage possible anaphylactic reactions following administration of the vaccine.</p>
						
						<p>Syncope (fainting) may occur in association with administration of injectable vaccines including FLUAD, FLUAD QUADRIVALENT, and FLUCELVAX QUADRIVALENT. Syncope can be accompanied by transient neurological signs such as visual disturbance, paresthesia, and tonic-clonic limb movements. Ensure procedures are in place to avoid falling injury and to restore cerebral perfusion following syncope by maintaining a supine or Trendelenburg position.</p>
						
						<p>The immune response to FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT and FLUCELVAX QUADRIVALENT in immunocompromised persons, including individuals receiving immunosuppressive therapy, may be lower than in immunocompetent individuals.</p>
						
						<p>Vaccination with FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT and FLUCELVAX QUADRIVALENT may not protect all vaccine recipients against influenza disease.</p>
	
						<p><strong>ADVERSE REACTIONS</strong></p>
	
						<p>FLUAD:</p>
	
						<p>The most common (&ge; 10%) local (injection site) adverse reactions observed in clinical studies with FLUAD were injection site pain (25%) and tenderness (21%). The most common (&ge; 10%) systemic adverse reactions observed in clinical studies with FLUAD were myalgia (15%), headache (13%) and fatigue (13%).</p>
	
						<p>FLUAD QUADRIVALENT:</p>
	
						<p>The most common (&ge; 10%) local and systemic reactions with FLUAD QUADRIVALENT in elderly subjects 65 years of age and older were injection site pain (16.3%), headache (10.8%) and fatigue (10.5%).</p>
	
						<p>AFLURIA QUADRIVALENT:</p>
	
						<p>AFLURIA QUADRIVALENT administered by needle and syringe:</p>
	
						<p>In adults 18 through 64 years, the most commonly reported injection-site adverse reaction was pain (&ge; 40%). The most common systemic adverse events were myalgia and headache (&ge; 20%).</p>
	
						<p>In adults 65 years of age and older, the most commonly reported injection-site adverse reaction was pain (&ge; 20%).  The most common systemic adverse event was myalgia (&ge; 10%).</p>
	
						<p>In children 5 through 8 years, the most commonly reported injection-site adverse reactions were pain (&ge; 50%), redness and swelling (&ge; 10%).  The most common systemic adverse event was headache (&ge; 10%).</p>
	
						<p>In children 9 through 17 years, the most commonly reported injection-site adverse reactions were pain (&ge; 50%), redness and swelling (&ge; 10%).  The most common systemic adverse events were headache, myalgia, and malaise and fatigue (&ge; 10%).</p>
	
						<p>In children 6 months through 35 months of age, the most commonly reported injection-site reactions were pain and redness (&ge; 20%). The most common systemic adverse events were irritability (&ge; 30%), diarrhea and loss of appetite (&ge; 20%).</p> 
	
						<p>In children 36 through 59 months of age, the most commonly reported injection site reactions were pain (&ge; 30%) and redness (&ge; 20%).  The most commonly reported systemic adverse events were malaise and fatigue, and diarrhea (&ge; 10%).</p>
	
						<p>The safety experience with AFLURIA (trivalent formulation) is relevant to AFLURIA QUADRIVALENT because both vaccines are manufactured using the same process and have overlapping compositions:</p>
	
						<p>In adults 18 through 64 years of age, the most commonly reported injection-site adverse reactions with AFLURIA (trivalent formulation) when administered by the PharmaJet Stratis Needle-Free Injection System were tenderness (&ge; 80%), swelling, pain, redness (&ge; 60%), itching (&ge; 20%) and bruising (&ge; 10%). The most common systemic adverse events were myalgia, malaise (&ge; 30%), and headache (&ge; 20%).</p>
	
						<p>FLUCELVAX QUADRIVALENT:</p>
	
						<p>In adults 18 through 64 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were pain (&ge; 40%), erythema and induration (&ge; 10%). The most common systemic adverse events were headache, fatigue and myalgia (&ge; 10%).</p>
	
						<p>In adults &ge; 65 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were pain (&ge; 20%) and erythema (&ge; 10%).</p>
	
						<p>In children 2 through 8 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were tenderness (28.7%), pain (27.9%) and erythema (21.3%), induration (14.9%) and ecchymosis (10.0%). The most common systemic adverse events were sleepiness (14.9%), headache (13.8%), fatigue (13.8%), irritability (13.8%) and loss of appetite (10.6%).</p>
	
						<p>In children and adolescents 9 through 17 years of age who received FLUCELVAX QUADRIVALENT, the most commonly reported injection-site adverse reactions were injection site pain (21.7%), erythema (17.2%) and induration (10.5%).  The most common systemic adverse events were headache (18.1%) and fatigue (17.0%).</p>
	
						<p>To report SUSPECTED ADVERSE REACTIONS, contact Seqirus USA Inc. at <strong>1-855-358-8966</strong> or <strong>VAERS</strong> at <strong>1-800-822-7967</strong> or <a target="_blank" href="http://www.vaers.hhs.gov"><strong>www.vaers.hhs.gov</strong></a>.</p>
	
						<p>Before administration, please see the full US Prescribing Information for FLUAD, FLUAD QUADRIVALENT, AFLURIA QUADRIVALENT and FLUCELVAX QUADRIVALENT.</p>
	
						<p>FLUAD�, FLUAD� QUADRIVALENT, AFLURIA� QUADRIVALENT and FLUCELVAX� QUADRIVALENT are registered trademarks of Seqirus UK Limited or its affiliates.</p>
					</div>
					<div class="sbs__right">
						<p><strong>INDICATIONS AND USAGE</strong></p>
	
						<p>FLUAD� (Influenza Vaccine, Adjuvanted) is an inactivated influenza vaccine indicated for active immunization against influenza disease caused by influenza virus subtypes A and type B contained in the vaccine. FLUAD� QUADRIVALENT (Influenza Vaccine, Adjuvanted) is an inactivated influenza vaccine indicated for active immunization against influenza disease caused by influenza virus subtypes A and types B contained in the vaccine.  FLUAD and FLUAD QUADRIVALENT are approved for use in persons 65 years of age and older.  These indications are approved under accelerated approval based on the immune response elicited by FLUAD QUADRIVALENT.</p>
	
						<p>AFLURIA� QUADRIVALENT (Influenza Vaccine) is an inactivated influenza vaccine indicated for active immunization against influenza disease caused by influenza A subtype viruses and type B viruses contained in the vaccine.  AFLURIA QUADRIVALENT is approved for use in persons 6 months of age and older.</p>
	
						<p>FLUCELVAX� QUADRIVALENT (Influenza Vaccine) is an inactivated vaccine indicated for active immunization for the prevention of influenza disease caused by influenza virus subtypes A and types B contained in the vaccine. FLUCELVAX QUADRIVALENT is approved for use in persons 2 years of age and older.</p>
					</div>
				</div>
			</div>
		 </section>	<section class="references">
		<div class="container">
			<p><strong>References:</strong><br /><br /><strong>1.</strong> FLUCELVAX QUADRIVALENT. Package insert. Seqirus Inc; 2021. <strong>2.</strong> Data on file. Seqirus Inc; 2021. <strong>3.</strong> Medicare.gov. Flu shots. Accessed February 12, 2021. https://www.medicare.gov/coverage/flushots. <strong>4.</strong> Centers for Disease Control and Prevention. Cell-based flu vaccines. Accessed February 11, 2021. https://www.cdc.gov/flu/prevent/cell-based.htm </p>
		</div>
	</section>
</div>


</template:page>