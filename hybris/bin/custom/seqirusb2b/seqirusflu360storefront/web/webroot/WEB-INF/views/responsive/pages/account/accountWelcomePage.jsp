<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>


<c:set var="pageId" value="${cmsPage.uid}" />
<c:choose>
	<c:when test="${pageId == 'account'}">
		<div class="col-xs-12" id="dashborad_topcontent">
			<div class="row dashboard-welcomesection">
				<div class="col-xs-12 col-md-6" id="dashborad_topcontentleftcontent">
					<div class="col-xs-12">
						Welcome back, <span>${customer.firstName}</span>
					</div>
					<div class="col-xs-12 margin-T20 currently-booking">Currently
						booking:</div>
					<div class="col-xs-12 padding-T5 currently-booking-seson">${seasonEntry.currentSeason}
						In-Season Orders</div>
				</div>
				<div class="col-xs-12 col-md-6 "
					id="dashborad_topcontentrightcontent">
					<div class="row dashboard-ordervalues">
						<div class="col-xs-5 col-md-6 dashborad_openorders">
							<div class="col-xs-12 dashborad_opennumbers">${openOrders}</div>
							<div class="col-xs-12">Open Orders</div>
						</div>
						<div class="col-xs-1 col-md-1 dashborad_sperator"></div>
						<div class="col-xs-6 col-md-5 dashborad_openInvoices">
							<div class="col-xs-12 dashborad_opennumbers">${invoice.openInvoiceCount}</div>
							<div class="col-xs-12 no-padding">Open Invoices</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<div class="col-xs-12" id="orders_topcontent">
			<div class="col-xs-12 col-md-8" id="orders_topcontentleftcontent">
				<div class="col-xs-12 orders_titeltop">My Orders</div>
				<div class="col-xs-12 orders_titeltop_sub">
					One or more items in <a href="#">your account</a> are pending
					confirmation.
				</div>
			</div>
			<div class="col-xs-12 col-md-4" id="orders_topcontentleftcontent">
				<div class="row">
					<div class="col-xs-12 orders_righttitle">
						Current Season : <span class="orders_titleyear">${seasonEntry.currentSeason}</span>
					</div>
					<div class="col-xs-12"></div>
				</div>
				<div class="row">
					<div class="col-xs-12 orders_titlsubtext">2022-2023
						Reservation Window Opens:</div>
					<div class="col-xs-12 orders_titlsubtext_year">XX, 2021</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>

<!-- <div class="card">
	<div class="card-body">
		<h5 class="card-title orders_popupdelverydate">October 10, 2021</h5>
		<h6 class="card-subtitle mb-2 text-muted">
			<a href="#" class="card-link orders_popviewtracking">View
				tracking <i class="global_blackarrow"></i>
			</a>
		</h6>
		<div class="orders_popupordersinfo">
			<p class="orders_popuporderinfotitle">Order #5463536346246</p>
			<p class="orders_popupsubtitle">123 Main St, Atlanta GA 12313</p>
		</div>
		<div class="orders_popupordersprodinfo">
			<p class="orders_popuporderinfotitle">500 units FLUAD�
				QUADRIVALENT</p>
			<p class="orders_popupsubtitle">0.5-mL pre-filled syringe</p>
			<p class="orders_popupsubtitle">0.25-mL pre-filled syringe</p>
			<p class="orders_popuporderinfotitle">500 units FLUCELVAX�
				QUADRIVALENT</p>
			<p class="orders_popupsubtitle">0.5-mL pre-filled syringe</p>
			<p class="orders_popuporderinfotitle">500 units AFLURIA�
				QUADRIVALENT</p>
			<p class="orders_popupsubtitle">5-mL multi-dose vial</p>
		</div>
	</div>
</div> -->