<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
    <div class="home_container container col-xs-12">

        <div id="home_floatingbanner" class="col-xs-12">
            <span id="home_floattext" class="col-xs-10">Lorem ipsum dolor
                sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua.</span> <span id="home_closebtn" class="col-xs-2">X</span>
        </div>
        <cms:pageSlot position="homepageBanner" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>
        <div class="col-xs-12" id="home_planningcontent">
            <cms:pageSlot position="planningSection" var="feature">
                <div class="col-xs-12 col-md-6" id="home_planningleftcontent">
                    ${feature.content}
                </div>
                <div class="col-xs-12 col-md-6" id="home_planningrightcontent">
                    <div class="col-xs-12" id="home_planningrighttext">${feature.h2content}</div>
                    <div class="col-xs-12" id="home_planninglearn">${feature.paragraphcontent} <i class="global_whitearrow"></i></div>
                </div>
            </cms:pageSlot>
        </div>
        <cms:pageSlot position="solutionsSection" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>
        <cms:pageSlot position="strengthenSection" var="feature">
            <section class="col-xs-12 global_createaccsection" id="home_flu360overview" style="background-image: url('${feature.media.url}')">
                <div class="col-xs-12 col-md-offset-6 col-md-6">
                    <div class="col-xs-12 global_createaccheader">${feature.headline}</div>
                    <div class="col-xs-12 global_createaccsubheader">${feature.h2content}</div>
                    <div class="col-xs-12 global_greyredbtncontainer">
                        <button class="global_redbtn">${feature.paragraphcontent}</button>
                        <button class="global_greybtn">${feature.content}</button>
                    </div>
                </div>
            </section>
        </cms:pageSlot>
        <cms:pageSlot position="vaccinePortfolioSection" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>
        <cms:pageSlot position="featuredResourcesSection" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>
       <cms:pageSlot position="BroughtybSection" var="feature">
        <section class="col-xs-12 global_createaccsection" id="home_learnseqirus" style="background-image: url('${feature.media.url}')">
            <div class="col-xs-12 col-md-offset-6 col-md-6">
                <div class="col-xs-12 global_createaccheader">${feature.headline}</div>
                <div class="col-xs-12 global_createaccsubheader">${feature.h2content}</div>
                <div class="col-xs-12 global_greyredbtncontainer">
                    <button class="global_redbtn">${feature.paragraphcontent}</button>
                    <button class="global_greybtn">${feature.content}</button>
                </div>
            </div>
        </section>
        </cms:pageSlot>
    </div>
</template:page>