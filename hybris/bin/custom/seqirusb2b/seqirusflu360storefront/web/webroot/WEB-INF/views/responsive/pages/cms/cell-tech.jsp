<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
    <div id="cell-tech">
        <!-- begin hero -->
        <div class="hero--grad" style="background-image: url('${themeResourcePath}/cms/assets/images/cell-based-tech.png')">

            <div class="breadcrumbs--page-nav">
                <div class="container">
                    <p>
                        <a href="#">Home</a><span> > </span>
                        <a href="#">flu360 Overview</a><span> > </span>
                        <a href="#">Clinical Support</a><span> > </span>
                        <strong>Cell Technology</strong>
                    </p>
                </div>
            </div>
            <div class="row-flex">
                <div class="col-flex-12 hidden-sm hidden-md hidden-lg hero--grad__mobile">
                    <img src="${themeResourcePath}/cms/assets/images/cell-based-tech.png" role="presentation">
                </div>
                <div class="container">
                    <div class="col-flex-12 col-flex-md-8">
                        <div class="hero--grad__content">
                            <div class="content-container content-container--has-corner text-left">
                                <h1>Cell-based technology</h1>
                                <p class="text--grey-100"><span class='text--red-100'>flu360<sup>TM</sup></span> helps you understand and navigate the benefits of a cell-based baccine as an alternative to traditional, egg-based vaccines.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end hero -->

        <!-- begin sbs section width gray bg-->
        <section class="sbs bg--grey-0">
            <div class="container">
                <!-- begin sbs content -->
                <div class="sbs__content text-center">
                    <h3 class="header--3">A cell-based option is an alternative to traditional, egg-based vaccines</h3>
                    <p>Several factors may impact vaccine effectiveness. Two that may play an important role include<sup>1</sup>:</p>
                </div>
                <div class="sbs__cards">
                    <div class="sbs__card">
                        <h4 class="header--4-medium">Antigenic Drift</h4>
                        <p>After strain selection, <strong>circulating influenza virus strains have the potential to mutate,</strong> which can impact vaccine effectiveness.<sup>2</sup></p>
                    </div>
                    <div class="sbs__card">
                        <h4 class="header--4-medium">Egg Adaptation</h4>
                        <p><strong>Mutations can also be introduced during egg-based manufacturing.</strong> In addition to injecting the WHO*-selected strains into the egg, a growth-inducing strain is required to ensure the virus can grow successfully in eggs. This process can cause mutations, resulting in an influenza virus that can be different from the intended strain.<sup>3-5</sup></p>
                    </div>
                </div>
                <!-- end sbs content -->
            </div>
        </section>
        <!-- end sbs section width gray bg -->
        <!-- begin charts content-->
        <section class="charts-content">
            <div class="container">
                <div class="centered-header centered-header--smaller">
                    <h4 class="header--4-medium text--grey-100">Egg-based manufacturing requires the addition of a growth-inducing strain along with the WHO-selected strain, which can cause egg adaptation.<sup>3,4</sup></h4>
                    <p>This is a conceptualization of the traditional, egg-based manufacturing process</p>
                </div>

                <div class="charts-content__chart-container">
                    <img src="${themeResourcePath}/cms/assets/images/egg-cell1.png">
                </div>

                <div class="centered-header">
                    <h4 class="header--4-medium text--grey-100">A strain mism\atch occurred in 6 of the last 10 influenza seasons in the US (2010-2011 through 2019-2020<sup>†</sup>), half of which were caused by egg adaptation in the vaccine strains during manufacturing.<sup>4-14</sup></h4>
                    <p class="citations">
                    <sup>*</sup>World Health Organization<br />
                    <sup>†</sup>Preliminary end of season estimates for the 2019-2020 influenza season by the Centers for Disease Control and Prevention</p>
                </div>

                <div class="centered-header">
                    <h4 class="header--4-medium text--grey-100">Cell-based manufacturing may produce a truer match to the WHO-selected strains.<sup>3,15</sup></h4>
                    <p>This is a conceptualization of the cell-based manufacturing process</p>
                </div>

                <div class="charts-content__chart-container">
                    <img src="${themeResourcePath}/cms/assets/images/egg-cell2.png">
                </div>

                <div class="centered-header">
                    <h4 class="header--4-medium text--grey-100">By avoiding egg adaptations, seasonal influenza vaccine effectiveness may be improved.<sup>15,16</sup></h4>
                </div>
            </div>
        </section>
        <!-- end charts content-->
        <!-- begin sbs section width gray bg-->
        <section class="sbs bg--grey-0">
            <div class="container">
                <!-- begin sbs content -->
                <div class="sbs__content text-center">
                    <h3 class="header--3">A cell-based vaccine for prevention of seasonal influenza</h3>
                </div>
                <div class="sbs__cards sbs__cards--nobg">
                    <div class="sbs__card sbs__card--nobg">
                        <p>Cell-based manufacturing is a different production process compared to traditional egg-based manufacturing, and may produce a truer match to the WHO-selected strains.<sup>3,15</sup></p>
                    </div>
                    <div class="sbs__card sbs__card--nobg">
                        <p>Implementation of cell-grown virus seeds for the 4 WHO-selected strains avoids egg-adapted changes that can sometimes occur in the egg-based manufacturing process.<sup>3,15</sup></p>
                    </div>
                </div>
                <div class="sbs__footer">
                    <a href="#" class="text-dark-gray cta">Learn more about our cell-based influenza vaccine<img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                    </a>
                </div>
                <!-- end sbs content -->
            </div>
        </section>
        <!-- end sbs section width gray bg -->
        <!-- begin 50 percent speedbump -->
        <section class="bg--grey-100 speedbump speedbump--50">
            <div class="container">
                <div class="speedbump__content">
                    <h3 class="header--3">What’s behind the influenza burden and vaccine effectiveness?</h3>
                        <p>Influenza impacts the lives of millions, and vaccine effectiveness in the US has varied considerably over the 10 most recent flu seasons.<sup>17,18</sup></p>
                        <a href="#" class="text--white cta">Learn more about our cell-based influenza vaccine<img src="${themeResourcePath}/cms/assets/images/arrow-right-white.svg">
                        </a>
                </div>
            </div>
        </section>
        <!-- end 50 percent speedbump -->		<!--  CAROUSEL MOBILE LAYOUT STARTS -->
        <div class="resources">
            <div class="container">
                <div class="row-flex carousel-container-row">
                        <div class="row-flex access-financial-resources-header--row">

                            <div class="col-flex-sm-12 header--container">
                            <h2>Featured Resources<div class="header-line"></div></h2>
                        </div>
                    </div>
                </div>

                <!-- 5. CLINICAL RESOURCES CARDS STARTS  DESKTOP-->
                <div id="clinical-resources-financial" class="row-flex carousel--three-card resources-card--row">

                    <div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12 cards-pl-0-mobile">
                        <div class="resources--card">
                            <img src="${themeResourcePath}/cms/assets/images/flucelvax-brochure.png">
                            <div class="resources--card-container">
                                <p class="resources--card-eyebrow">FLUCELVAX QUADRIVALENT | CLINICAL</p>
                                <p class="resources--card-title">FLUCELVAX QUADRIVALENT Patient Brochure</p>
                                <p class="resources--card-paragraph">Download this brochure to help educate your patients about FLUCELVAX QUADRIVALENT for those 2+ years.</p>
                            </div>

                            <button class="resources--card-btn">
                            <span>Access Resource</span> <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </button>

                        </div>
                    </div>

                    <div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12 cards-pl-0-mobile">
                        <div class="resources--card">
                            <img src="${themeResourcePath}/cms/assets/images/flucelvax-flashcard.png">
                            <div class="resources--card-container">
                                <p class="resources--card-eyebrow">FLUCELVAX QUADRIVALENT | CLINICAL</p>
                                <p class="resources--card-title">FLUCELVAX QUADRIVALENT Flashcard</p>
                                <p class="resources--card-paragraph">Download this flashcard to learn more about why FLUCELVAX QUADRIVALENT, a cell-based vaccine, is an appropriate option for patients 2+ years.</p>
                            </div>

                            <button class="resources--card-btn">
                            <span>Access Resource</span> <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </button>
                        </div>
                    </div>

                    <div class="col-flex-lg-4 col-flex-md-4 col-flex-sm-12 col-flex-xs-12 cards-pl-0-mobile">
                        <div class="resources--card">
                            <img src="${themeResourcePath}/cms/assets/images/flucelvax-coding.png">
                            <div class="resources--card-container">
                                <p class="resources--card-eyebrow">FLUCELVAX QUADRIVALENT | CLINICAL</p>
                                <p class="resources--card-title">FLUCELVAX QUADRIVALENT Coding and Billing Guide</p>
                                <p class="resources--card-paragraph">Ensure you're up to date with the latest FLUCELVAX QUADRIVALENT coding and billing information.</p>
                            </div>

                            <button class="resources--card-btn">
                            <span>Access Resource</span> <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </button>
                        </div>
                    </div>

                </div>

                <!-- 5. CLINICAL RESOURCES CARDS ENDS  -->
                <!-- VIEW ALL RESOURCES ROW STARTS -->
                <div class="row-flex ">
                    <div class="col-flex-xs-12 text-center view-resources--row">
                            <button class="button--view-resources-outline-grey">
                                    View All Resources
                                </button>
                    </div>

                </div>
                    <!--  VIEW ALL RESOURCES ROW ENDS -->
            </div>
        </div>
        <!-- resources end -->
        <!--4. VACCINE PORTFOLIO SECTION STARTS -->
        <div class="container">
            <div id="vaccine-header-row" class="row-flex">
                    <div class="col-flex-12 vacccine-portfolio-header--row">
                            <div class="col-flex-sm-12 header--container">
                            <h2>Explore the Seqirus Vaccine Portfolio<div class="header-line"></div></h2>
                        </div>
                    </div>

                </div>

                <div class="row-flex vaccine-portfolio-row">
                    <div class="col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                        <div class="logo--card center-xs">
                            <div class="logo--card-container">
                            <img class="fluad-logo" src="${themeResourcePath}/cms/assets/images/logos/fluad-logo.svg">
                            <a href="#" class="no-underline">
                            <p class="text-dark-gray cta ml-20" style="margin-top: 25px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </p>
                            </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                        <div class="logo--card center-xs">
                            <div class="logo--card-container">
                            <img class="flucelvax-logo" src="${themeResourcePath}/cms/assets/images/logos/flucelvax-logo.svg">
                            <a href="#" class="no-underline">
                            <p class="text-dark-gray cta" style="margin-top: 30px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </p>
                            </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-flex-md-4 col-flex-xl-4 col-flex-lg-4 col-flex-md-4 col-flex-sm-12">
                        <div class="logo--card center-xs">
                            <div class="logo--card-container">
                            <img class="affluria-logo mt-10" src="${themeResourcePath}/cms/assets/images/logos/affluria-logo.svg">
                            <a href="#" class="no-underline">
                            <p class="text-dark-gray cta ml-10" style="margin-top: 35px;">Explore <img src="${themeResourcePath}/cms/assets/images/arrow-right.svg">
                            </p>
                            </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-flex-xl-12 col-flex-lg-12 col-flex-md-12 col-flex-sm-12 col-flex-xs-12">
                        <p class="disclaimer--paragraph">Please see Important Safety Information and full US Prescribing Information
                            on each vaccine's respective product page.</p>
                    </div>
                </div>		</div>
        <!-- VACCINE PORTFOLIO SECTION ENDS -->
        <section class="references">
            <div class="container">
                <p><strong>References:</strong> <strong>1.</strong> Mameli C, Cocchi I, Fumagalli M and Zuccotti G. Influenza vaccination: effectiveness, indications, and limits in the pediatric population. Front Pediatr. 2019. doi.org/10.3389/fped.2019.00317 <strong>2.</strong> Paules CI, Sullivan SG, Subbarao K, Fauci AS. Chasing seasonal influenza—the need for a universal influenza vaccine. N Engl J Med. 2018;378(1):7-9. doi:10.1056/ NEJMp1714916 <strong>3.</strong> Rajaram S, Boikos C, Gelone DK, Gandhi A. Influenza vaccines: the potential benefits of cell-culture isolation and manufacturing. Ther Adv Vaccines Immunother. 2020;8:2515135520908121. doi:10.1177/2515135520908121 <strong>4.</strong> Skowronski DM, Janjua NZ, De Serres G, et al. Low 2012-13 influenza vaccine effectiveness associated with mutation in the egg-adapted H3N2 vaccine strain not antigenic drift in circulating viruses. PLoS One. 2014;9(3):e92153. doi:10.1371/journal.pone.0092153 <strong>5.</strong> Zost SJ, Parkhouse K, Gumina ME, et al. Contemporary H3N2 influenza viruses have a glycosylation site that alters binding of antibodies elicited by egg-adapted vaccine strains. Proc Natl Acad Sci USA. 2017;114(47):12578-12583. doi:10.1073/pnas.1712377114 <strong>6.</strong> Centers for Disease Control and Prevention. Update: Influenza activity—United States, 2010-11 season, and composition of the 2011-12 influenza vaccine. MMWR Morb Mortal Wkly Rep. 2011;60(21):705-712. <strong>7.</strong> Ohmit SE, Thompson MG, Petrie JG, et al. Influenza vaccine effectiveness in the 2011-2012 season: protection against each circulating virus and the effect of prior vaccination on estimates. Clin Infect Dis. 2014;58(3):319-327. doi:10.1093/cid/cit736. <strong>8.</strong> McLean HQ, Thompson MG, Sundaram ME, et al. Influenza vaccine effectiveness in the United States during 2012-2013: variable protection by age and virus type. J Infect Dis. 2015;211(10):1529-1540. doi:10.1093/ infdis/jiu647 <strong>9.</strong> Gaglani M, Pruszynski J, Murthy K, et al. Influenza vaccine effectiveness against 2009 pandemic influenza A(H1N1) virus differed by vaccine type during 2013-2014 in the United States. J Infect Dis. 2016;213(10):1546-1556. doi:10.1093/infdis/jiv577 <strong>10.</strong> Zimmerman RK, Nowalk MP, Chung J, et al. 2014-2015 influenza vaccine effectiveness in the United States by vaccine type. Clin Infect Dis. 2016;63(12):1564-1573. doi:10.1093/cid/ciw635 <strong>11.</strong> Jackson ML, Chung JR, Jackson LA, et al. Influenza vaccine effectiveness in the United States during the 2015-2016 season. N Engl J Med. 2017;377(6):534-543. doi:10.1056/NEJMoa1700153 <strong>12.</strong> Flannery B, Chung JR, Belongia EA, et al. Interim estimates of 2017-18 seasonal influenza vaccine effectiveness - United States, February 2018. MMWR Morb Mortal Wkly Rep. 2018;67(6):180-185. doi:10.15585/mmwr.mm6706a2 <strong>13.</strong> Flannery B, Kondor RJG, Chung JR, et al. Spread of antigenically drifted influenza A(H3N2) viruses and vaccine effectiveness in the United States during the 2018-2019 season. J Infect Dis. 2020;221(1):8-15. doi:10.1093/infdis/jiz543 <strong>14.</strong> Dawood FS, Chung JR, Kim SS, et al. Interim estimates of 2019-20 seasonal influenza vaccine effectiveness — United States, February 2020. MMWR Morb Mortal Wkly Rep. 2020;69(7):177-182. <strong>15.</strong> Centers for Disease Control and Prevention. Cell-based flu vaccines. Accessed February 11, 2021. https://www.cdc.gov/flu/prevent/cell-based.htm <strong>16.</strong> Mabrouk T, Ellis RW. Influenza vaccine technologies and the use of the cell-culture process (cell-culture influenza vaccine). Dev Biol. 2002;110:125-134. <strong>17.</strong> Centers for Disease Control and Prevention. Disease burden of influenza. Accessed October 20, 2020. https://www.cdc.gov/flu/about/burden/index.html <strong>18.</strong> Centers for Disease Control and Prevention. Past seasons vaccine effectiveness estimates. Accessed September 25, 2020. https://www.cdc.gov/flu/vaccines-work/past-seasons-estimates.html</p>
            </div>
        </section>
    </div>
</template:page>

