<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">

	<div class="post_login">
<%-- No Order Dashboard--%>
	<div class="container-fluid body-section" >
      <div class="">
         <nav aria-label="supportbreadcrumb" class="no-show">
            <ol class="supportbreadcrumb">
               <li class="supportbreadcrumb-item active"><a href="#" class="">Home</a></li>
               <li class="supportbreadcrumb-item" aria-current="page">Support</li>
            </ol>
         </nav>
         <div class="row">
            <div class="col-md-12 marBottom40">
               <h2 class="contactheader">
							<%-- <cms:pageSlot position="Section4A" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot> --%>
							<span>WELCOME, </span>${userName}
						</h2>
            </div>
         </div>

         <div class="container dashboard-body">
         
            <div class="col-md-6 ">
              <div class="blklast">
			    <h2 class="no-order-title-text">What`s next</h2>
				<p class="no-order-title-para">
								<cms:pageSlot position="Section4B" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</p>
			  </div>
			   <%-- <div class="blklast">
                    <div class="col-md-12 no-pad marginTop30">
                        <table class="table dashboard-heading-table">
                            <tr>
                                <td class="heading-txt">Notifications <span>4</span></td>
                                <td><a href="#" class="view_order_link">View All Notifications</a></td>
                            </tr>
                        </table>
                    </div>  
  
                    <div class="col-md-11 dashboard-notification no-pad">
			          <div class="notificationblk">
                  <div class="notification-col">
                     <div class="notificationtxt notification-col1"><span class="notification-colspan">3</span><span class="">Seqirus announcements</span></div>
                     <div class="notificationtxt"><span class="notification-colspan">0</span>Order Alert</div>
                     <div class="notificationtxt"><span class="notification-colspan">1</span>Account Alert</div>
                  </div>
               </div>

                    </div>
					
					 <div class="col-md-12 dashboard-notification-accordion">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h3 class="panel-title new-product-announce">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
												aria-controls="collapseOne"> New Product Announcement | 25th May 2021 </a>
												
												<button type="button" class="pull-right close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<a href="#" class="pull-right linkcolor" > View Product </a>
										</h3>
										
										
									</div>
									<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body lorem-ipsum-optional">
												<cms:pageSlot position="Section4C" var="feature">
													<cms:component component="${feature}" />
												</cms:pageSlot>
											</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTwo">
										<h4 class="panel-title new-product-announce">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
												aria-expanded="false" aria-controls="collapseTwo"> New Product Announcement | 25th May 2021 </a>
												<button type="button" class="pull-right close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<a href="#" class="pull-right linkcolor" > View Page <span class="linkbtton"></span></a>
										</h4>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
										<div class="panel-body lorem-ipsum-optional">
												<cms:pageSlot position="Section4C" var="feature">
													<cms:component component="${feature}" />
												</cms:pageSlot>
											</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingThree">
										<h4 class="panel-title new-product-announce">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
												aria-expanded="false" aria-controls="collapseThree"> New Product Announcement | 25th May 2021 </a>
													<button type="button" class="pull-right close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<a href="#" class="pull-right linkcolor " > View Page <span class="linkbtton"></span></a>
										</h4>
									</div>
									<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
										<div class="panel-body lorem-ipsum-optional">
												<cms:pageSlot position="Section4C" var="feature">
													<cms:component component="${feature}" />
												</cms:pageSlot>
											</div>
									</div>
								</div>
							</div>

					 </div>
                </div> --%>
            </div>

            <div class="col-md-6 ">
                <div class="blklast">
                    <div class="col-md-12 no-pad">
                        <table class="table dashboard-heading-table">
                            <tr>
                                <td class="heading-txt">Products & Resources</td>
                                <td><a href="${contextPath}/productresources" class="view_order_link">More Products & Resources</a></td>
                            </tr>
                        </table>
						<div class="no-order-resource">
						    <!-- <div class="categories-title">
							   Categories
							</div>
						    <div class="row">
								<div class=" categories-btn">Clinic Planning</div>
								<div class=" categories-btn">Coding & Billing</div>
								<div class="categories-btn">Products</div>
								<div class=" categories-btn">View More</div>
							</div> -->
							
							<div class="categories-title">
							  Getting started
							</div>
							<div class="row">
								 <div class="col-md-6">
                        <div class="Dgrey-Blocks">
                           <div class="Dgrey-Labels">
                              <div class="Dred-labls">Flucelvax</div>
                           </div>
                           <div class="Dinner-Content">
                              <div class="Dproducts-blocks">
                                 <p class="Dboxheading">aTIV Administration Guide</p>
                                 <p class="Dresource-para">
															<cms:pageSlot position="Section4D" var="feature">
																<cms:component component="${feature}" />
															</cms:pageSlot>
                             </p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="Dgrey-Blocks">
                           <div class="Dgrey-Labels">
                              <div class="Dred-labls">Flucelvax</div>
                           </div>
                           <div class="Dinner-Content">
                              <div class="Dproducts-blocks">
                                 <p class="Dboxheading">aTIV Administration Guide</p>
                                 <p class="Dresource-para">
															<cms:pageSlot position="Section4D" var="feature">
																<cms:component component="${feature}" />
															</cms:pageSlot></p>
                             
                              </div>
                           </div>
                        </div>
                     </div>
								 
							</div>
							<!-- <div class="row">
							  <div class="recomedtion">More recommended for you</div>
								 <div class="col-xs-12 col-md-6">
									  <ul>
										<li>Fluad Important Safety Info</li>
										<li>Flucelvax Important Safety Info</li>
										<li>2019-2020 Coding & Billing</li>
									  </ul>
								</div>
								 <div class="col-xs-12 col-md-6">
									  <ul>
										<li>Lorem Ipsum dolor</li>
										<li>Lorem Ipsum dolor</li>
										<li>Lorem Ipsum dolor</li>
									  </ul>
								</div>
							</div> -->
						</div>
                    </div>  
  
                
                </div>
        
            </div>
		
           

         </div>
		 
		        <div class="col-md-12 no-pad">
                <div class="blklast">
                    <div class="col-md-12 no-pad">
                        <table class="table dashboard-heading-table">
                            <tr>
                                <td class="heading-txt">Support</td>
                                <td><a href="${contextPath}/support" class="view_order_link">More Support</a></td>
                            </tr>
                        </table>
                    </div>  
  
                    <div class="col-md-12 no-pad dashboard-support">

                        <div class="col-md-4 col-sm-3 col-xs-12 support-left" style="text-align:center;"><span >Contact Seqirus<br>Customer Service</span></div>
                        <div class="col-md-8 col-sm-9 col-xs-12 support-right">
						<div class="col-md-4" style="text-align:center;">
						  <p> Call</p>
                          <p > <a href="tel:555555555" class="phoneno">+44 (0) 8457 451500</a></p> 
						</div>	
						<div class="col-md-4" style="text-align:center;">
						
						  <span>*</span> <br>
                          <span>Or</span> <br>
						  <span>*</span> 
						</div>	
						<div class="col-md-4" style="text-align:center;">
						  <p> Email</p>
                          <p><a href="mailto:Seqirus@custome.com">service.uk@seqirus.com</a></p>
						</div>	
                          
                        </div>
                      
                    </div>
                </div>
        
            </div>
         
      </div>
      </div>

		<%-- Order Dashboard--%>
		<div class="container-fluid body-section" style="display: none;">
			<div class="container">
				
				<div class="row">
					<div class="col-md-12 marBottom40">
						<h2 class="contactheader">
							<cms:pageSlot position="Section1" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</h2>
					</div>
				</div>

				<div class="container dashboard-body">

					<div class="col-md-6 no-pad">
						<div class="blkfirst">
							<div class="col-md-12 no-pad">
								<table class="table dashboard-heading-table">
									<tr>
										<td class="heading-txt">Orders</td>
										<td><a href="#" class="view_order_link">View All
												Orders</a></td>
									</tr>
								</table>
							</div>

							<div class="col-md-12 white-background">
								<div class="box-heading-dashboard">You have 2 order(s)
									pending</div>
								<div class="dashboard-table-wrapper">
									<table class="table dashboard-table">
										<tr>
											<td>Order#123456789</td>
											<td><a href="#" class="view_order_link">View Order</a></td>
										</tr>
										<tr>
											<td>Order#123456789</td>
											<td><a href="#" class="view_order_link">View Order</a></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6 no-pad">
						<div class="blklast">
							<div class="col-md-12 no-pad">
								<table class="table dashboard-heading-table">
									<tr>
										<td class="heading-txt">Invoices</td>
										<td><a href="#" class="view_order_link">View All
												Invoices</a></td>
									</tr>
								</table>
							</div>

							<div class="col-md-12 white-background">
								<div class="box-heading-dashboard">You have 3 invoice(s)
									pending</div>
								<div class="dashboard-table-wrapper">
									<table class="table dashboard-table">
										<tr>
											<td>Invoice#123456789</td>
											<td><a href="#" class="view_order_link">View Order</a></td>
										</tr>
										<tr>
											<td>Invoice#123456789</td>
											<td><a href="#" class="view_order_link">View Order</a></td>
										</tr>
										<tr>
											<td>Invoice#123456789</td>
											<td><a href="#" class="view_order_link">View Order</a></td>
										</tr>
									</table>

								</div>
							</div>
						</div>

					</div>


					<%-- <div class="col-md-6 no-pad">
						<div class="blklast">
							<div class="col-md-12 no-pad">
								<table class="table dashboard-heading-table">
									<tr>
										<td class="heading-txt">Notifications <span>2</span></td>
										<td><a href="#" class="view_order_link">View All
												Notifications</a></td>
									</tr>
								</table>
							</div>

							<div class="col-md-12 dashboard-notification no-pad">

								<div class="notification-block">
									<cms:pageSlot position="Section3A" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
								<div class="notification-block">
									<cms:pageSlot position="Section3B" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
								<div class="notification-block">
									<cms:pageSlot position="Section3C" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
								<div class="notification-block">
									<cms:pageSlot position="Section3D" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>

							</div>
						</div>

					</div> --%>
					
					
					<!-- <div class="col-md-6 no-pad">
                <div class="blklast">
                    <div class="col-md-12 no-pad">
                        <table class="table dashboard-heading-table">
                            <tr>
                                <td class="heading-txt">Notifications <span>4</span></td>
                                <td><a href="#" class="view_order_link">View All Notifications</a></td>
                            </tr>
                        </table>
                    </div>  
  
                    <div class="col-md-11 dashboard-notification no-pad">
			          <div class="dashboard-notification-alert">
						<ul class="nav nav-pills" role="tablist">
						  <li role="presentation" class="active otherlinks"><a href="#"><span>3</span>Seqirus Announcement(s) </a></li>
						  <li role="presentation" class="otherlinks"><a href="#"><span>1</span>Order Alert(s)</a></li>
						  <li role="presentation" class="otherlinks"><a href="#"><span>0</span>Account Alert(s) </a></li>
						</ul>
                      </div>
                    </div>
					
					 <div class="col-md-11 dashboard-notification-accordion">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h3 class="panel-title new-product-announce">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
												aria-controls="collapseOne"> New Product Announcement | 25th May 2021 </a>
												
												<button type="button" class="pull-right close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<a href="#" class="pull-right linkcolor" > View Product </a>
										</h3>
										
										
									</div>
									<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body lorem-ipsum-optional">Lorem ipsum optional description copy about new product or a new product update.
											This is optional; the content author can choose to omit a description.</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTwo">
										<h4 class="panel-title new-product-announce">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
												aria-expanded="false" aria-controls="collapseTwo"> New Product Announcement | 25th May 2021 </a>
												<button type="button" class="pull-right close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<a href="#" class="pull-right linkcolor" > View Page <span class="linkbtton"></span></a>
										</h4>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
										<div class="panel-body lorem-ipsum-optional">Lorem ipsum optional description copy about new product or a new product update.
											This is optional; the content author can choose to omit a description.</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingThree">
										<h4 class="panel-title new-product-announce">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
												aria-expanded="false" aria-controls="collapseThree"> New Product Announcement | 25th May 2021 </a>
													<button type="button" class="pull-right close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<a href="#" class="pull-right linkcolor " > View Page <span class="linkbtton"></span></a>
										</h4>
									</div>
									<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
										<div class="panel-body lorem-ipsum-optional">Lorem ipsum optional description copy about new product or a new product update.
											This is optional; the content author can choose to omit a description. </div>
									</div>
								</div>
							</div>

					 </div>
                </div>
        
            </div> -->


					<div class="col-md-6 no-pad">
						<div class="blklast">
							<div class="col-md-12 no-pad">
								<table class="table dashboard-heading-table">
									<tr>
										<td class="heading-txt">Support</td>
										<td><a href="${contextPath}/support"
											class="view_order_link">More Support</a></td>
									</tr>
								</table>
							</div>

							<div class="col-md-12 no-pad dashboard-support">

								<div class="col-md-6 col-sm-6 col-xs-12 support-left">
									<cms:pageSlot position="Section2B" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12 support-right">
									<cms:pageSlot position="Section2C" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>

							</div>
						</div>

					</div>

				</div>

			</div>
		</div>

	</div>
</template:page>