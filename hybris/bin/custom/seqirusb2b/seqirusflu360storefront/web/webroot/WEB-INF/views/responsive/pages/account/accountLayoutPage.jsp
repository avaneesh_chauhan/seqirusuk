<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<spring:url value="/my-account/update-profile" var="updateProfileUrl" />
<spring:url value="/my-account/update-password" var="updatePasswordUrl" />
<spring:url value="/my-account/update-email" var="updateEmailUrl" />
<spring:url value="/my-account/address-book" var="addressBookUrl" />
<spring:url value="/my-account/payment-details" var="paymentDetailsUrl" />
<spring:url value="/my-account/orders" var="ordersUrl" />

<template:page pageTitle="${pageTitle}">
	<!--Body code starts -->
	<main role="main" class=" container col-xs-12">
		<cms:pageSlot position="SideContent" var="feature" element="div"
			class="col-xs-12 col-md-2 col-lg-2  sidebar hidden-xs hidden-sm visible-md-* visible-lg-*">
			<cms:component component="${feature}" />
		</cms:pageSlot>
		<div class="col-xs-12 col-md-10 col-lg-10  main">
			<cms:pageSlot position="TopContent" var="feature" element="div"
				class="row">
				<cms:component component="${feature}" />
			</cms:pageSlot>
			<c:set var="pageId" value="${cmsPage.uid}" />
			<c:choose>
				<c:when test="${pageId == 'account'}">
					<div class="row">
						<div class="col-xs-12" id="dashborad_announcment">
							<div class="col-xs-12 col-md-6  dashborad_announceleftheader  ">
								<cms:pageSlot position="LeftContent" var="feature" element="div"
									class="row">
									<cms:component component="${feature}" />
								</cms:pageSlot>
								<cms:pageSlot position="BodyContent" var="feature" element="div"
									class="row dashboard-splitscetion">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div>
							<cms:pageSlot position="RightContent" var="feature" element="div"
								class="col-xs-12 col-md-6 dashborad_announceleftheader">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</div>
					<cms:pageSlot position="BottomContent" var="feature" element="div"
						class="row dashboard-support">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</c:when>
				<c:otherwise>
					<cms:pageSlot position="PlaceholderContentSlot" var="feature" element="div" class="row">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</c:otherwise>
			</c:choose>
		</div>
	</main>
	<!--Body code Ends -->

</template:page>