<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div class="col-xs-12 col-md-6 dashboard-invoicesection">
	<div class="dashboard-invoicesection-header">Invoices</div>
	<div class="dashboard-invoicesection-text">
		You have <span class="dashboard-pricetxt">${invoice.openInvoiceCount} open invoices</span> <br>
		applied to your account.
	</div>
	<div class="dashboard-invoicesection-text">
		Total Balance Due: <br> <span class="dashboard-pricetxt">
			<p>$<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${invoice.openStatusInvoiceAmount}" /></p></span>
	</div>
	<div class="dashboard-viewall dashboard-viewinvociemargin">
		View Invoices<i class="global_blackarrow"></i>
	</div>
</div>
<div class="col-xs-12 col-md-6 dashboard-creditsection">
	<div class="dashboard-invoicesection-header">Credits</div>
	<div class="dashboard-invoicesection-text">
		You have <span class="dashboard-pricetxt">$XX,XXX </span>in credit
		available based on your returns.
	</div>
	<div class="dashboard-invoicesection-text">Have questions about
		using your credit? Visit Help & FAQs.</div>
	<div class="dashboard-viewall margin-T20">
		View Credits<i class="global_blackarrow"></i>
	</div>
</div>