<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
	<cms:pageSlot position="TopHeaderSlot" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<cms:pageSlot position="BottomHeaderSlot" var="component" element="div">
		<cms:component component="${component}" />
	</cms:pageSlot>
</sec:authorize>
<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
	<!--Dashboard Header Desktop code starts -->
	<header class="headerbar hidden-xs hidden-sm visible-md-* visible-lg-*">
		<nav class="container-fluid container-lg">
			<div class="row headerSection">
				<div
					class="col-xs-2 col-lg-2 col-md-3 padding-L40 margin-T10 margin-B20">
					<cms:pageSlot position="SiteLogo" var="logo" limit="1">
						<cms:component component="${logo}" class="logoimg"
							alt="Seqirus A CSL Company" />
					</cms:pageSlot>
				</div>
				<div class="col-lg-7 col-md-6 dashboard_search"></div>
				<div class="col-md-1 dashboard_cart">
					<i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp; Cart
				</div>
				<div class="col-md-2 dashboard_profile">
					<i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;
					${customer.name}
				</div>
			</div>
		</nav>
	</header>
	<!--Dasboard Header Desktop ends -->
	<!--Dashboard Header Mobile code starts -->

	<div class="hidden-md hidden-lg visible-xs-* visible-sm-*">
		<div class="mob-logo-wrap">
			<div class="col-xs-6 col-sm-6 padding-T20 padding-B20">
				<a rel="home" href="#"> <!-- <img src="images/flu360.png" class="logoimg" alt="Seqirus A CSL Company" width="120"></a> -->
					<cms:pageSlot position="SiteLogo" var="logo" limit="1">
						<cms:component component="${logo}" class="logoimg"
							alt="Seqirus A CSL Company" />
					</cms:pageSlot>
				</a>
			</div>
			<div class="col-xs-6 col-sm-6 mob-menu">
				<i class="fa fa-shopping-cart" aria-hidden="true"></i> <i
					class="fa fa-user-circle" aria-hidden="true"></i> <i
					class="fa fa-bars mob_nav_click" aria-hidden="true"></i>
			</div>
		</div>
		<div class="mob_exp_menu">
			<!-- Accordion Menu START -->
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<h4 class="dashboard-title">My Dashboard</h4>
					<div class="panel-heading accordion-toggle collapsed"
						data-toggle="collapse" data-parent="#accordion"
						data-target="#collapseOne">

						<h4 class="panel-title">Orders</h4>

					</div>
					<div id="collapseOne" class="panel-collapse collapse">
						<div class="panel-body">
							<ul class="mob_sub_menu">

								<li><a href="#" target="_self" title="Order Summary">Order
										Summary</a>
									<ul class="second_level_mob_sub">
										<li><a href="#" target="_self" title="My Orders">My
												Orders</a></li>
										<li><a href="#" target="_self" title="Returns">Returns</a></li>
									</ul></li>

							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading accordion-toggle collapsed"
						data-toggle="collapse" data-parent="#accordion"
						data-target="#collapseTwo">
						<h4 class="panel-title">Invoices</h4>

					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">

							<ul class="mob_sub_menu">
								<li><a href="#" target="_self" title="Financial Dashboard">Financial
										Dashboard</a></li>
								<li><a href="#" target="_self" title="All Invoices">All
										Invoices</a></li>
							</ul>

						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading accordion-toggle collapsed"
						data-toggle="collapse" data-parent="#accordion"
						data-target="#collapseThree">
						<h4 class="panel-title">Products & Resources</h4>

					</div>
					<div id="collapseThree" class="panel-collapse collapse">
						<div class="panel-body">
							<ul class="mob_sub_menu">

								<li><a href="#" target="_self" title="All Products">All
										Products</a>
									<ul class="second_level_mob_sub">
										<li><a href="#" target="_self" title="FLUAD">FLUAD</a></li>
										<li><a href="#" target="_self"
											title="Operational Efficiency">AFLURIA</a></li>
										<li><a href="#" target="_self" title="FLUCELVAX">FLUCELVAX</a></li>
									</ul></li>

								<li><a href="#" target="_self" title="Resources">Resources</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading accordion-toggle collapsed"
						data-toggle="collapse" data-parent="#accordion"
						data-target="#collapseFour">
						<h4 class="panel-title">Account Management</h4>

					</div>
					<div id="collapseFour" class="panel-collapse collapse">
						<div class="panel-body">
							<ul class="mob_sub_menu">
								<li><a href="#" target="_self" title="My Profile">My
										Profile</a></li>
								<!-- <li><a href="#" target="_self" title="User Directory">User
										Directory</a></li> -->
								<li><a href="#" target="_self"
									title="Organization Locations">Organization Locations</a></li>

							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading accordion-toggle collapsed"
						data-toggle="collapse" data-parent="#accordion"
						data-target="#collapseFive">
						<h4 class="panel-title">Help & FAQs</h4>

					</div>
					<div id="collapseFive" class="panel-collapse collapse">
						<div class="panel-body">
							<ul class="mob_sub_menu">
								<li><a href="#" target="_self" title="FAQ">FAQ</a></li>
								<li><a href="#" target="_self"
									title="Organization Locations">Contact Information</a></li>

							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- Accordion menu END -->

			<div class="default-mob-menu-item">
				<div class="col-xs-6">
					<div class="redborder">
						<a class="dashborad-create-account" href="#">Start Order</a>
					</div>

				</div>
				<!-- <div class="col-xs-6 mob-login-icon"><a class="log-in" href="">Log In  <span class="glyphicon glyphicon-log-in" style="top: 3px;"></span></a></div> -->
				<!-- </div> -->
			</div>
		</div>
	</div>

	<!--Dashboard Header Mobile code Ends -->
	<cms:pageSlot position="HeaderLinks" var="component" element="div">
		<cms:component component="${component}" />
	</cms:pageSlot>
</sec:authorize>



