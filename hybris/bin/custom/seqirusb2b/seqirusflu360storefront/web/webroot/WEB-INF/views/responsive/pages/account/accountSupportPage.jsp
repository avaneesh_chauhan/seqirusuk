<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="col-xs-12">
	<div class="col-xs-12 col-md-6 dashboard-supportsection">
		<div class="dashboard-support-text">Personal Support</div>
		<div class="dashboard-support-title">Contact your Vaccine
			Solution Specialist:</div>
		<div class="dashboard-support-name">Karen Joyce</div>
		<div class="dashboard-support-emailtext">
			Email: <a href="mailto:support@seqirus.com"
				class="dashboard-support-emaillink">support@seqirus.com</a>
		</div>
		<div class="dashboard-support-emailtext">
			Call: <a href="tel:678.908.8493" class="dashboard-support-emaillink">678.908.8493</a>
		</div>
	</div>
	<div class="col-xs-12 col-md-6">
		<div class="dashboard-support-text">flu360 | Support</div>
		<div class="dashboard-support-emailtext">
			Email: <a href="mailto:customerservice.US@seqirus.com"
				class="dashboard-support-emaillink">
				customerservice.US@seqirus.com</a>
		</div>
		<div class="dashboard-support-emailtext">
			Call: <a href="tel:855.358.8966" class="dashboard-support-emaillink">
				855.358.8966</a>
		</div>
	</div>
</div>