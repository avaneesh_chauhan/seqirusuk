<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<div id="PayingContact" class="tabcontent">
               <div class="section1_header col-xs-12">
                  <spring:theme code="form.register.paying.header" />
               </div>
               <div class="section1_subheader col-xs-12">
                  <spring:theme code="form.register.paying.subHeader" />
                  <br>   <input type="radio" id="payInfo1" name="payinginfo" value="<spring:theme code="form.register.shipping.radioText" />">
                  <label class="radioLabel" for="payInfo1"><spring:theme code="form.register.shipping.radioText" /></label>
               </div>
            
                  <div class="col-xs-12 businessformContent">
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step1FirstName"><spring:theme code="form.register.paying.firstName" /></label><br/>
                              <input  class="form-control textonly step1first" id="step1FirstName" name="payingContactInfo.firstName" autocomplete="no" placeholder="Edward"  type="text"/>                              
                           </div>
                           <div class="form-group col-md-4">
                              <label for="step1LastName"><spring:theme code="form.register.paying.lastName" /></label><br/>
                              <input class="form-control textonly" id="step1LastName" name="payingContactInfo.lastName" autocomplete="no" placeholder="Newgate"  type="text"/> 
                           </div>
                        </div>
                     </div> 

                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step1Email"><spring:theme code="form.register.paying.email" /><span class="asteriskred">*</span></label><br/>
                              <input class="form-control required-fld-form2 email_input emailcheck2" autocomplete="no" name="payingContactInfo.email" id="step1Email" data-error="Please enter valid Email" placeholder="email@example.co.uk"  type="email" required/>
                              <div class="help-block" id='emailerr2'></div>                            
                           </div>
                           <div class="form-group col-md-4">
                              <label for="step1phoneNumber"><spring:theme code="form.register.paying.phoneNum" /></label><br/>
                              <input  class="form-control phone-not-man" id="step1phoneNumber" autocomplete="no" name ="payingContactInfo.phone" maxlength="11"  type="text"/> 
                           <div class="help-block" id="message2"></div>  
                           </div>
                           <div class="form-group col-md-2">
                              <label for="step1phoneext"><spring:theme code="form.register.paying.phoneExt" /></label><br/>
                              <input  class="form-control numberonly" id="step1phoneext" name="payingContactInfo.phoneExt" autocomplete="no" placeholder="1234" maxlength="4" type="text"/>  
                           </div>
                        </div>
                     </div> 
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step1jobtitle"><spring:theme code="form.register.paying.jobTitle" /></label><br/>
                              <input  class="form-control textonly" id="step1jobtitle" name="payingContactInfo.jobTitle" autocomplete="no" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.business.jobtitleDesc" />" placeholder="E.g. Vaccine Buyer"  type="text"/>                             
                           </div>
                           <div class="form-group col-md-4"> 
                              <label for="step1organisationname"><spring:theme code="form.register.paying.orgName" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form2 with_space" id="step1organisationname" name="payingContactInfo.organizationName" autocomplete="no" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.business.orgName" />"  placeholder="Practice, Pharmacy or Business Name"  data-error="Please enter Organisation Name" type="text" required/>   
                           	  <div class="help-block with-errors"></div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-5 col-xs-12">
                           <div class="form-group col-md-12">
                              <label for="Address">Address Lookup</label>
                              <div class="input-group spcl-addon">  
                              <div class="input-group-addon"><i class="fa fa-search"></i></div>
                              <input class="form-control" style="margin-bottom:0px;" id="addresslookupPaying" name="payingContactInfo.addressLookUp" placeholder="Search" type="text">
                              </div>
                             
                           </div>
                        </div>
                     </div>
                     
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step1address"><spring:theme code="form.register.paying.addr" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form2" id="step1address" name="payingContactInfo.addressLine1" data-error="Please enter Address" placeholder="Building Number & Street"  type="text" required/>
                           	  <div class="help-block with-errors"></div>
                           </div>
                           <div class="form-group col-md-4">
                              <label for="step1street"><spring:theme code="form.register.paying.additionalStreet" /></label><br/>
                              <input  class="form-control textonly" id="step1street" name="payingContactInfo.addressLine2" placeholder="Building or Office name/number"  type="text"/> 
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step1City"><spring:theme code="form.register.paying.city" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form2 textonly" id="step1City" placeholder="City" name="payingContactInfo.city" data-error="Please enter City" type="text" required/>
                              <div class="help-block with-errors"></div>
                           </div>
                           <div class="form-group col-md-2">
                              <label for="step1post"><spring:theme code="form.register.paying.postCode" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form2 post_code post_not_man" autocomplete="no" id="step1post" placeholder="SL16 8AA" name="payingContactInfo.postalCode" data-error="Please enter valid Post Code"  type="text" required/>   
                           	 <div class="help-block with-errors"></div>
                           </div>
                           <div class="form-group col-md-3">
                              <label for="step1country"><spring:theme code="form.register.paying.country" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form2 textonly" id="step1country" placeholder="Country" name="payingContactInfo.country" data-error="Please enter Country" type="text" required/>   
                              <div class="help-block with-errors"></div>
                           </div>
                        </div>
                     </div>  
                  </div>
            
               <div class="col-xs-12 previosnmail">
               <div class="prevStepbtn pagi-link sub-remove tab-2" data-section-id="section-1" data-img-id="img-1">< <spring:theme code="form.register.prev" /></div>
               <button type="submit"  class="step2 nxt2_2 Nextbutton2 sub-3" id="gotosection_4"><spring:theme code="form.register.next" /></button>
               <%-- <span class="skiptext"><a  class="skiplink pagi-link sub-2" href="javaScript:void(0);" data-section-id="section-4" data-img-id="img-2" id="payerSkipForNow"><spring:theme code="form.register.skip" /></a></span> --%>
               </div>
               </div>
