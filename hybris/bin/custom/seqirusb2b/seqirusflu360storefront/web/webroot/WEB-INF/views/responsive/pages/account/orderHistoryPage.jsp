<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="userProfileInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userCompanyInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userAccountInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userLocationsInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<template:page pageTitle="${pageTitle}">
<div class="container-fluid body-section">

<div class="hiddenYear" id="${presentSeason}"></div>
<div class="totalorderflucelvax" id="${totalOrderFlucelvax}"></div>
		<div class="totalOrderFluad" id="${totalOrderFluad}"></div>
		<div class="totalOrderAfluria" id="${totalOrderAfluria}"></div>
		<div class="totalOrder" id="${totalOrder}"></div>
				
		<div class="totalShipflucelvax" id="${totalShipFlucelvax}"></div>
		<div class="totalShipFluad" id="${totalShipFluad}"></div>
		<div class="totalShipAfluria" id="${totalShipAfluria}"></div>
		<div class="totalShip" id="${totalShip}"></div>
		
		
      <div class="container" style="width:1170px">
         <nav aria-label="supportbreadcrumb" >
            <ol class="supportbreadcrumb">
               <li class="supportbreadcrumb-item active"><a href="#" class="">Home</a></li>
               <li class="supportbreadcrumb-item" aria-current="page">Orders</li>
            </ol>
         </nav>
         <div class="row">
            <div class="col-md-12 marBottom40">
               <h2 class="contactheader">ORDERS</h2>  
            </div>
            <div class="col-md-3 left-order-section">
               <div class="search-sec">
               <select class="form-control selectpicker" id="ordersDropdown" >
               <c:forEach items="${seasonList}" var="seasons" varStatus="seasonLoop">
                   <option id="season_${seasons.orderSeason}">${seasons.orderSeason}</option>
                 </c:forEach>
                 </select>
               </div>
               <div class="order-left-sec">
                  <div class="order-title"><i class="fa fa-angle-right" aria-hidden="true"></i> <span>All Orders</span></div>
                  <div class="search-order-sec">
                     <div class="form-group">
                        <div class="input-group spcl-addon">  
                        <div class="input-group-addon"><span class="fa-search fa"></span></div>
                        <input class="form-control" placeholder="Search Orders" id="search-orders" type="text">
                        </div>
                        
                     </div>
                  </div>

                  <div class="left-navigation-order">
                     <div class="customer-scroll" id="custom-scrollbar">
                     <c:forEach items="${shipAddressIds}" var="shipAddressId" varStatus="ship">
                     <div class="menu_container">

                        <!--Location 1-->
                        <div class="menu-title">

 <c:forEach items="${orders}" var="activeOrder" varStatus="activeOrderLoop">
		 <c:if test="${activeOrder.shipToID == shipAddressId}">
		 <c:set var= "locationName" value="${activeOrder.partnerName}"/>
		 </c:if>
		 </c:forEach>
          Location ${ship.index+1} - ${locationName}

	<i class="fa fa-chevron-up" aria-hidden="true"></i></div>
                       <c:forEach items="${orders}" var="activeOrder" varStatus="activeOrderLoop">
                       <c:if test="${activeOrder.shipToID == shipAddressId}">
                       <input type="hidden" id="orderStatus${activeOrderLoop.index}" value="${activeOrder.status}"/>
                        <div class="expand-menu ordertoOpen" id="expandOrder_${activeOrderLoop.index}">
                           <div class="menu-box">
                           <div class="left-gap"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                           <div class="right-area">
                              <div class="order-status">${activeOrder.status}</div>
                              <c:choose>
                              <c:when test="${activeOrder.status eq 'Cancelled'}">
                             <div class="order-no" id="${activeOrder.orderID}">Order #${activeOrder.orderID}</div>
                              
                              </c:when>
                              <c:otherwise>
                               <div class="order-no error-ord" id="${activeOrder.orderID}">Order #${activeOrder.orderID} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
                              </c:otherwise>
                              
                              </c:choose>
                              <div class="order-date">Placed <fmt:formatDate pattern = "MM/dd/yyyy" value = "${activeOrder.orderDate}"/></div>
                           </div>
                           </div>
                        </div>
                        </c:if>
                        </c:forEach>
                        <!--Location 2-->
                       <!-- <div class="menu-title">Location 2 - Hampshire<i class="fa fa-chevron-up" aria-hidden="true"></i></div>
                         <div class="expand-menu">
                           <div class="menu-box">
                           <div class="left-gap"></div>
                           <div class="right-area">
                              <div class="order-status">OPEN</div>
                              <div class="order-no error-ord">Order #45605645 <i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
                              <div class="order-date">Placed 29/06/2020</div>

                           </div>
                           </div>
                           <div class="menu-box">
                              <div class="left-gap"></div>
                              <div class="right-area">
                                 <div class="order-status">CLOSED</div>
                                 <div class="order-no">Order #45605645</div>
                                 <div class="order-date">Placed 29/06/2020</div>
   
                              </div>
                              </div>
                        </div> -->

                        <!--Location 3-->
                      <!--  <div class="menu-title">Location 3 - Amstartdam<i class="fa fa-chevron-up" aria-hidden="true"></i></div>
                        <div class="expand-menu">
                           <div class="menu-box">
                           <div class="left-gap"></div>
                           <div class="right-area">
                              <div class="order-status">OPEN</div>
                              <div class="order-no error-ord">Order #45605645 <i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
                              <div class="order-date">Placed 29/06/2020</div>

                           </div>
                           </div>
                           <div class="menu-box">
                              <div class="left-gap"></div>
                              <div class="right-area">
                                 <div class="order-status">CLOSED</div>
                                 <div class="order-no">Order #45605645</div>
                                 <div class="order-date">Placed 29/06/2020</div>
   
                              </div>
                              </div>
                        </div>-->


                     </div>
                     </c:forEach>
                  </div>

                  </div>

               </div>
            </div>
            
            
            <div class="col-md-9 order-right-section contentarea marBottom40">
            <div class="order-landing order-right-section-bg">
          <c:if test="${totalOrders==0}">
        <div class="container" >
    <div class="row marBottom40 margin-T20" >
           <div class="col-md-6 page-title ">YOY HAVE NO ORDERS PLACED <br/>IN THIS SEASON.</div>
           <div class="col-md-3 updated-status">Last Updated 2nd August  at 3:02am </div>
           
        <div class="row">
                  <div class="col-md-8">Can't find your order? It might be in another season. Otherwise, <a class="emaillink" href="mailto:service.uk@seqirus.com">contact support.</a></div>
               </div>
 </div>
 </div>
 </c:if>
             <c:if test="${totalOrder>0}"> 
               <div class="row">
                  <div class="col-md-5 page-title">ORDER STATUS</div>
                  <div class="col-md-7 updated-status">Last Updated 2nd August  at 3:02am <span>Refresh Content <a href="#"><i class="fa fa-repeat" aria-hidden="true"></i></a></span></div>
               </div>
               <div class="row">
                  <div class="col-md-8">This is a summary of all orders placed for this season. Use the filters to view fulfillment status by order and product. </div>
               </div>
               <div class="row order-filter">
                  <!-- <div class="col-md-2 filter-order-lebel">Filter By</div>
                  <div class="col-md-10 order-filter-drop">
                     <div class="order-drop-down">
                      <div class="col-md-3 no-pad order-status-filter"> 
                       <select class="form-control selectpicker" id="forcast-status">
                        <option style="pointer-events: none;">Orders</option>
                        <option>Filter Value1</option>
                        <option>Filter Value2</option>
                        <option>Filter Value3</option>
                        <option>Filter Value4</option>
                        </select>  
                     </div> 
                     <div class="col-md-9 clear-filter-order no-pad" style="display:none;">
                        <div class="form-group clr-fil">
                           <a href="#/" id="statusclearButton" class="btn btn-md btn-default">Clear All</a>
                       </div>
                     </div>
                     <div class="clear"></div>
                     </div>
                     <div class="clear"></div>
                     <div class="row" id="forcastordervalues" style="display:none;">
                        <div class="filter-title floatLeft margin-T20 marginPL10" id="filterdisplay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                       
                        
                        --Multiple Filter Value Populated Start Area-
                        <div id="forcastordersfilter1" class="filter-value-wrap"></div> 
                        --Multiple Filter Value Populated Ends Area-                       
                        
                       
                        <div class="clear "></div>
                        
                    </div>

                  </div>  -->
                  
               </div>

               <div class="row marBottom40">
                  <div class="col-md-5 col-sm-3 col-xs-12">
                     <div class="circleGraphic "></div>
                 </div>
                 <div class="col-md-7 col-sm-9 col-xs-12">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra<span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">0/${totalOrderFlucelvax}</div>
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra<span class="prodcuttitle-Subtext">- 0.5mL
                                 prefilled syringe 10 pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical1'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">0/0</div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad Tetra <span class="prodcuttitle-Subtext">- 0.5mL
                                 prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical2'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">0/${totalOrderFluad}</div>
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad Tetra<span class="prodcuttitle-Subtext">- 0.5mL
                                 prefilled syringe 10 pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical3'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">0/0</div>
                 </div>
               </div>

                <div class="row marBottom40">
                  <div class="col-md-12">
                     <div class="invoiceHeader col-xs-12 allOrderTableHeader">${presentSeason}</div>
                        <!-- Table starts -->
                        <div class="tablecontainer ordersTablecontainer"> 
                            <table id="ordersTable" class="display">
                                <thead>
                                    <tr>
                                        <th>Orders</th>
                                        <th>Location</th>
                                       <!--  <th>Status</th>
                                        <th>Est. Delivery</th>
                                        <th></th> -->
                                    </tr>
                                </thead> 
                            </table>
                        </div>
                        <!-- Table Ends --> 
                    </div>

                  </div>

               
               <!-- 
               <div class="row">
                <div class="col-md-4 order-sub-title">Delivery Schedule</div>  
                <div class="col-md-8">This is a summary of all orders placed for this season.This calendar reflects the requested delivery weeks at time of order placement. The schedule will be updated once a named delivery date is confirmed.</div>
               </div>

               <div class="row order-filter">
                  <div class="col-md-2 filter-order-lebel">Filter By</div>
                  <div class="col-md-10">
                     <div class="order-drop-down">
                        <div class="col-md-3 no-pad forcast-select product-forcast-filter"> 
                           <select class="form-control selectpicker" id="product_filter">
                              <option style="pointer-events: none;">(2) Product</option>
                              <option>Filter Value1</option>
                              <option>Filter Value2</option>
                              <option>Filter Value3</option>
                              <option>Filter Value4</option>
                           </select> 
                        </div>
                        <div class="col-md-3 no-pad forcast-select order-forcast-filter"> 
                           <select class="form-control selectpicker" id="order_deliver_filter">
                              <option style="pointer-events: none;">Orders</option>
                              <option>Filter Value1</option>
                              <option>Filter Value2</option>
                              <option>Filter Value3</option>
                              <option>Filter Value4</option>
                           </select>
                        </div>
                       <div class="col-md-6 clear-filter-order-shipping no-pad" style="display:none;">
                        <div class="form-group floatLeft greyLine clr_botn">
                           <a href="#/" id="clearButton" class="btn btn-md btn-default">Clear All</a>
                       </div>
                       </div>
                       <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                       <div class="row" id="filteredvalues" style="display:none;">
                          <div class="filter-title floatLeft margin-T20 marginPL10" id="filterdisplay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                         
                          
                         <!-----This is the three Filter Buttons will be started-
                        
                         <div id="forprimaryfilter" class="filter-block"></div>
                         <div id="forcityfilter" class="filter-block"></div>
                         <div id="forstatefilter" class="filter-block"></div>
                         
                          <!-----This is the three Filter Buttons will be ended                     
                          
                         
                          <div class="clear "></div>
                          
                      </div>

                  </div> 
                  
               </div>

               <div class="row">
                  <div class="col-md-12 col-xs-12 calender-order-page no-pad">
                  <div id="calendar">
                  <div class="delivery-info">
                    <table class="table table-condensed delivery-table">
                       <tr>
                          <td ><span>2/2</span> Shipments Delivered</td>
                          <td ><i class="fa fa-arrow-circle-o-right" style="color:green;"></i> Shipment Scheduled</td>
                          <td ><i class="fa fa-check-circle-o" style="color:red;"></i> Delivery Made</td>
                       </tr>
                     </table>
    
                   </div>
                  </div>
                  
                  </div>
                
                 
                </div> -->
            </div>
             </c:if>  
            <div class="order-details">
            
<c:forEach items="${orders}" var="activeOrder" varStatus="activeOrderLoop">
              <div class="orderHeader orderHeader${activeOrderLoop.index}">
               <div class="row marBottom40">
                  <div class="col-md-4 order-details-box">
                     <div class="title">Order #</div>
                     <div class="white-box" id="${activeOrder.orderID}">${activeOrder.orderID}<span>Placed On: <fmt:formatDate pattern = "MM/dd/yyyy" value = "${activeOrder.orderDate}" /> </span></div>

                  </div>
                  <div class="col-md-4 order-details-box">
                     <div class="title">Total Units</div>
                     <div class="white-box-table">
                        <table class="table table-condensed">
                           <tr>
                              <td>Flucelvax</td>
                              <td id="${activeOrder.totalFlucelvax}" >${activeOrder.totalFlucelvax}</td>
                           </tr>
                           <tr>
                              <td>Fluad</td>
                              <td id="${activeOrder.totalFluad}" >${activeOrder.totalFluad}</td>
                           </tr>
                         </table>
                     </div>

                  </div>
                  <div class="col-md-4 order-details-box">

                     <div class="title">Total Cost</div>
                     <div class="white-box">&#163;${activeOrder.totalCost}<span><a href="#">See invoices for<br/> this Order</a></span></div>

                  </div>
               </div>

               <div class="row marBottom40">
                  <div class="col-md-4 order-white-rect">
                     <div class="title">P.O Number</div>
                     <div class="white-area">${activeOrder.poNumber}</div>

                  </div>
                  <div class="col-md-8 order-white-rect">
                     <div class="title">Shipping Location</div>
                     <div class="shipping-loc">
                     <div class="col-md-5 grey">${activeOrder.partnerName}</div>   
                     <div class="col-md-7 shipp-address"> <c:if test="${not empty activeOrder.address.addressLine2 && not empty activeOrder.address.addressLine1 }">${activeOrder.address.addressLine2},&nbsp;${activeOrder.address.addressLine1},&nbsp;${activeOrder.address.city},&nbsp;${activeOrder.address.state}<br/>${activeOrder.address.zipCode}
                              </c:if>
                              <c:if test="${ empty activeOrder.address.addressLine2}">${activeOrder.address.addressLine1},&nbsp;${activeOrder.address.city},&nbsp;${activeOrder.address.state}<br/>${activeOrder.address.zipCode}
                              </c:if>   
                              <c:if test="${ empty activeOrder.address.addressLine1}">${activeOrder.address.addressLine2},&nbsp;${activeOrder.address.city},&nbsp;${activeOrder.address.state}<br/>${activeOrder.address.zipCode}
                              </c:if></div>    
                     </div>

                  </div>
               </div>
               </div>
              </c:forEach>
               <div class="order-right-section-bg">

               <div class="row">
                  <div class="col-md-5 page-title">ORDER STATUS</div>
                  <div class="col-md-7 updated-status"></div>
               </div>
               <div class="row marBottom40">
                  <div class="col-md-8">This is a summary of all orders placed for this season. Use the filters to view fulfillment status by order and product. </div>
               </div>
               

               <div class="row marBottom40">
                  <div class="col-md-5 col-sm-3 col-xs-12">
                     <div class="circleGraphic1 "></div>
                 </div>
                 <div class="col-md-7 col-sm-9 col-xs-12">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">100/100</div>
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra <span class="prodcuttitle-Subtext">- 0.5mL
                                 prefilled syringe 10 pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical1'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">78/100</div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad Tetra <span class="prodcuttitle-Subtext">- 0.5mL
                                 prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical2'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">15/100</div>
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad Tetra<span class="prodcuttitle-Subtext">- 0.5mL
                                 prefilled syringe 10 pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical3'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">52/100</div>
                 </div>
               </div>

                <div class="row marBottom40">
                  <div class="col-md-12">
                     <div class="invoiceHeader col-xs-12">${presentSeason}</div>
                        <!-- Table starts -->
                        <div class=" tablecontainer2 ordersTablecontainer"> 
                            <table id="ordersTable1" class="display">
                                <thead>
                                    <tr>
                                        <th>Orders</th>
                                        <th>Location</th>
                                        <!-- <th>Status</th>
                                        <th>Est. Delivery</th>
                                        <th></th> -->
                                    </tr>
                                </thead> 
                            </table>
                        </div>
                        <!-- Table Ends --> 
                    </div>

                  </div>

               
<!-- 
               <div class="row">
                <div class="col-md-4 order-sub-title">Delivery Schedule</div>  
                <div class="col-md-8">This is a summary of all orders placed for this season.This calendar reflects the requested delivery weeks at time of order placement. The schedule will be updated once a named delivery date is confirmed.</div>
               </div>

               <div class="row order-filter order-filter-details">
                  <div class="col-md-2 filter-order-lebel">Filter By</div>
                  <div class="col-md-10">
                     <div class="order-drop-down">
                        <div class="col-md-3 no-pad forcast-select product-forcast-filter"> 
                           <select class="form-control selectpicker" id="product_filter_order_details">
                              <option style="pointer-events: none;">(2) Product</option>
                              <option>Filter Value1</option>
                              <option>Filter Value2</option>
                              <option>Filter Value3</option>
                              <option>Filter Value4</option>
                           </select> 
                        </div>
                        <div class="col-md-3 no-pad forcast-select order-forcast-filter"> 
                           <select class="form-control selectpicker" id="order_deliver_filter_order_details">
                              <option style="pointer-events: none;">Orders</option>
                              <option>Filter Value1</option>
                              <option>Filter Value2</option>
                              <option>Filter Value3</option>
                              <option>Filter Value4</option>
                           </select>
                        </div>
                       <div class="col-md-6 clear-filter-order-shipping no-pad" style="display:none;">
                        <div class="form-group floatLeft greyLine clr_botn">
                           <a href="#/" id="clearButton" class="btn btn-md btn-default">Clear All</a>
                       </div>
                       </div>
                       <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                       <div class="row" id="filteredvalues" style="display:none;">
                          <div class="filter-title floatLeft margin-T20 marginPL10" id="filterdisplay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                         
                          
                         ---This is the three Filter Buttons will be started--
                        
                         <div id="forprimaryfilter" class="filter-block"></div>
                         <div id="forcityfilter" class="filter-block"></div>
                         <div id="forstatefilter" class="filter-block"></div>
                         
                          ---This is the three Filter Buttons will be ended--                     
                          
                         
                          <div class="clear "></div>
                          
                      </div>

                  </div> 
                  
               </div> -->

               <!-- <div class="row">
                  <div class="col-md-12 col-xs-12 calender-order-page no-pad">
                  <div id="order-details-calendar">
                  <div class="delivery-info">
                    <table class="table table-condensed delivery-table">
                       <tr>
                          <td ><span>2/2</span> Shipments Delivered</td>
                          <td ><i class="fa fa-arrow-circle-o-right" style="color:green;"></i> Shipment Scheduled</td>
                          <td ><i class="fa fa-check-circle-o" style="color:red;"></i> Delivery Made</td>
                       </tr>
                     </table>
    
                   </div>
                  </div>
                  
                  </div>
                
                 
                </div> -->
               </div>
            </div>
            </div> 
            </div>
            
         </div>

         
         
      </div>
</template:page>