<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<template:page pageTitle="${pageTitle}">


<div class="container-fluid faqsbody-section" id="faqId">
         <div class="container">
            <nav aria-label="supportbreadcrumb">
               <ol class="supportbreadcrumb">
               <c:url value="/" var="homeUrl" />
				  <li class="supportbreadcrumb-item active"><a href="${homeUrl}"
					class="blacktext"><spring:theme code="breadcrumb.home" /></a></li>
				 
                  <li class="supportbreadcrumb-item active" aria-current="page"><a href="${contextPath}/support">Support</a></li>
                  <li class="supportbreadcrumb-item" aria-current="page">FAQ</li>
               </ol>
            </nav>
            <div class="row marBottom30">
               <div class="col-md-12">
                  <h2 class="faqheader">
                  	<cms:pageSlot position="Section1BA" var="feature">
					   <cms:component component="${feature}" />
					</cms:pageSlot>
				</h2>
               </div>
            </div>	
            <div class="container faq-body">
               <div class="col-md-3 faqleftcolumn">
                  <ul class="faqslist">
                  <li class="faglists sidemenuselected"><a class="link-1 faqlinks sublinks-active" id="faqsection-1">General</a></li><li class="faglists"><a class="link-2 faqlinks" id="faqsection-2">Orders</a></li><li class="faglists"><a class="link-3 faqlinks" id="faqsection-3" href="#">Shipping</a></li><li class="faglists"><a class="link-4 faqlinks" id="faqsection-4" href="#">Invoices</a></li><li class="faglists"><a class="link-5 faqlinks" id="faqsection-5" href="#">Account</a></li><li class="faglists"><a class="link-6 faqlinks" id="faqsection-6" href="#">Org. Affiliation</a></li><li class="faglists"><a class="link-7 faqlinks" id="faqsection-7" href="#">Returns</a></li><li class="faglists"><a class="link-8 faqlinks" id="faqsection-8" href="#">Other</a></li>


                     <%-- <cms:pageSlot position="Section2BB" var="feature">
					   <cms:component component="${feature}" />
					</cms:pageSlot> --%>
                  </ul>
               </div>
                <div class="col-md-8 faq-section faqwhite-background" id="faq-section-1">
                  <div class="faqheading-txt">
                  	<cms:pageSlot position="Section3BC" var="feature">
					   <cms:component component="${feature}" />
					</cms:pageSlot>
                  </div>
                  
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-2" style="display: none;">
                  	<cms:pageSlot position="Section4BC" var="feature">
					   	<cms:component component="${feature}" />
					</cms:pageSlot>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-3" style="display: none;">
                  	<cms:pageSlot position="Section5BC" var="feature">
					   	<cms:component component="${feature}" />
					</cms:pageSlot>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-4" style="display: none;">
                   <cms:pageSlot position="Section6BC" var="feature">
					   	<cms:component component="${feature}" />
					</cms:pageSlot>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-5" style="display: none;">
                  <cms:pageSlot position="Section7BC" var="feature">
					   	<cms:component component="${feature}" />
					</cms:pageSlot>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               
               
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-6" style="display: none;">
                  <div class="faqheading-txt">
                  	<cms:pageSlot position="Section9BC" var="feature">
					   <cms:component component="${feature}" />
					</cms:pageSlot>
                  </div>
                  
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               
               
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-7" style="display: none;">
                  <div class="faqheading-txt">
                  	<cms:pageSlot position="SectionRBC" var="feature">
					   <cms:component component="${feature}" />
					</cms:pageSlot>
                  </div>
                  
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
              
              
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-8" style="display: none;">
                  <cms:pageSlot position="Section8BC" var="feature">
					   	<cms:component component="${feature}" />
					</cms:pageSlot>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


</template:page>