$(window).on('load',function(){

    $('.large--carousel-slider').slick({
        dots:false,
        focusOnSelect: true,
        appendArrows: '.large--carousel-btn-container',
        prevArrow:'.prev-btn',
        nextArrow:'.next-btn'
    });


    $('.large--carousel-slider-mobile').slick({
        dots:true,
        focusOnSelect: true,
        appendDots:'.slick-slider-dots',
        appendArrows: '.large--carousel-mobile-btn-container',
        prevArrow:'.prev-btn-mobile',
        nextArrow:'.next-btn-mobile'
    });

    // $('.card-carousel-mobile').slick({dots:true,arrows:false,focusOnSelect: true});
   
   
   
   });