$(window).on('load',function(){

    $('.large--carousel-slider').slick({
        dots:false,
        focusOnSelect: true,
        appendArrows: '.large--carousel-btn-container',
        prevArrow:'.prev-btn',
        nextArrow:'.next-btn'
    });

    $('.large--carousel-slider-mobile').slick({
        dots:true,
        focusOnSelect: true,
        appendArrows: '.large--carousel-mobile-btn-container',
        prevArrow:'.prev-btn-mobile',
        nextArrow:'.next-btn-mobile'
    });

    $('.carousel--three-card').slick({dots:true,arrows:false,
        mobileFirst: true,
		centerMode: true,
        responsive: [
            {
                breakpoint: 1023,
                settings: "unslick"
            }
        ]
        
    })
	
});