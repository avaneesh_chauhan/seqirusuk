$(document).ready(function(){
    var uri = window.location.toString();
	if (window.location.pathname!='/invoicesDetails' && uri.length > 0 && uri.indexOf('/returnsandcredit')>=0) {
	
    var orderTable=$('#returnsTable').dataTable({
    	"ajax": {
			"url": ACC.config.contextPath + "/returnsandcredit/getCredits",
			"type": "GET",
			 "dataSrc": "creditList",
			 error: function (xhr, error, code)
            {
                console.log(error);
                console.log(code);
             }
		    },
		    "oLanguage": {
		        "sEmptyTable": "Credits not available"
		    },
        "columns": [
          { "data": "invoiceNumber"},
		  { "data": "amoutWithTax" , "render": function ( data, type, row, meta ) {return data.toLocaleString('en-GB', {style:'currency', currency:'GBP'}) }},
          { "data": "invoiceDate" },
          { "data": "status" },
        ],
          "pagingType":"full_numbers",
          "pageLength": 10,
          "lengthChange": false,
          "info":false,
          "autoWidth":false,
          "language": {
          "search": "",
          "paginate": {
            "first": "<<",
            "previous": "<",
            "next": ">",
            "last": ">>",
          }
      },
        "initComplete": function(settings, json) {
        	$("#returnsTable thead tr th:nth-child(even)").css("background","none");

        }
      });
      orderTable= $("#returnsTable_filter input").attr("placeholder", "Search by order #, etc");
      }
 });
  



  
  


