$(document).ready(function () {
	var urlLink1;
abandonPopup();
    var shippingTemplate = false;
    $(".registration_business").show();
    $('[data-toggle="tooltip"]').tooltip();
    $(".registration_nagivationPart2 .registration_nagivationName").addClass("active");
    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
    $("#billAndShippAddressForm.phone").inputmask({
        "mask": "(999) 999-9999"
    });
    $("#pay-phone").inputmask({
        "mask": "(999) 999-9999"
    });
    $("#shipping-phno").inputmask({
        "mask": "(999) 999-9999"
    });
    /* email radio function  Start*/
    $(function () {
        $("input[name='bill-accountType']").click(function () {
            if ($("#bill-accountType_E").is(":checked")) {
                $("#bill-accemail").show();
            } else {
                $("#bill-accemail").hide();
            }
        });
    });
    $(function () {
        $("input[name='bill-invoiceType']").click(function () {
            if ($("#bill-invoiceType_E").is(":checked")) {
                $("#bill-inemail").show();
            } else {
                $("#bill-inemail").hide();
            }
        });
    });
    /* billing Radio button function */
    $('input[name="bill-invoiceType"]').on('click', function () {
        if ($(this).val() === '0') {
            $('#billAndShippAddressForm.invoiceEmail').val('').hide();
        } else {
            $('#billAndShippAddressForm.invoiceEmail').val('').show().focus();
        }
    });
    $('input[name="bill-accountType"]').on('click', function () {
        if ($(this).val() === '0') {
            $('#billAndShippAddressForm.acctStmtEmail').val('').hide();
        } else {
            $('#billAndShippAddressForm.acctStmtEmail').val('').show().focus();
        }
    });
    /* email radio function  end*/
    /* Bussiness form Rules start*/
    $("#businessForm").validate({
        rules: {
            "orgAddressForm.billingOrgName": {
                required: true,
                alphabetsnspace: true
            },
            "business-dunnumber": {
                number: true,
                minlength: 10,
                maxlength: 10
            },
            "orgAddressForm.Line1": {
                required: true,
                alphanumeric: true
            },
            "orgAddressForm.line2": {
                required: false,
                alphanumeric: true
            },
            "orgAddressForm.city": {
                required: true,
                alpha: true
            },
            "orgAddressForm.billingState": {
                required: true,
            },
            "orgAddressForm.postalCode": {
                required: true,
                zipcodeUS: true
            }
        },
        messages: {
            "orgAddressForm.billingOrgName": {
                required: "Please enter organization name for billing.",
                alphabetsnspace: "Please enter Only Letters"
            },
            "business-dunnumber": {
                number: "Please enter your Valid DUNS Number",
                minlength: "Please enter your Valid DUNS Number Min is 10",
            },
            "orgAddressForm.Line1": {
                required: "Please enter an address.",
                alphanumeric: "Please enter Only Letters"
            },
            "orgAddressForm.line2": {
                required: "Please enter your Address2",
                alphanumeric: "Please enter Only Letters"
            },
            "orgAddressForm.city": {
                required: "Please enter a city.",
                alpha: "Please enter Only Letters"
            },
            "orgAddressForm.billingState": "Please select a state.",
            "orgAddressForm.postalCode": {
                required: "Please enter a valid US zip code.",
                zipcodeUS: "Please enter a valid US zip code."
            }
        },
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            // Add `has-feedback` class to the parent div.form-group
            // in order to add icons to inputs
            element.parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-feedback");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!element.next("span")[0]) {
                $("<span class='glyphicon glyphicon-exclamation-sign form-control-feedback'></span>").insertAfter(element);
            }
        },
        success: function (label, element) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!$(element).next("span")[0]) {
                //$("<span class='glyphicon form-control-feedback'></span>").insertAfter($(element));
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-error").removeClass("has-success");
            $(element).next("span").addClass("glyphicon-exclamation-sign").removeClass("glyphicon-ok");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-success").removeClass("has-error");
            $(element).next("span").removeClass("glyphicon-exclamation-sign");
        }
    });
    $(".registration_business .registration_save").click(function () {
     
        if ($('#businessForm').valid()) {
          
          // save to backend
          var str = $("#businessForm").serialize();
       var status = "failed"
      $.ajax({
    type:"post",
    data:str,
    url:ACC.config.contextPath+ "/register/saveRegData",
    async: false,
    dataType: "json",
    success: function(data){
      status  = "success";
      alert("success")
    },
                error: function() { 
                    alert("error"); 
                }
         });
          
          //save to backend end
       
            $(".registrationFlow").hide();
            $(".registration_billing").show();
            $("#registration_nagivationPart2 .registration_nagivationName").removeClass("active");
            $("#registration_nagivationPart3 .registration_nagivationName").removeClass("inactive").addClass("active");
            $("#registration_nagivationPart3 .registration_nagivationName li").show();
            $("#registration_nagivationPart3 .registration_nagivationName li:first-child").addClass("active");
            $("#registration_nagivationPart2 .registration_nagivationNumeric").hide();
            $("#registration_nagivationPart2 .registration_nagivationtick").show();
            topFunction();
        }
    });
    /* Bussiness form Rules end*/
    /* Billing form Rules Start*/
    $("#billingForm").validate({
        rules: {
            "billAndShippAddressForm.firstName": {
                required: true,
                alpha: true
            },
            "billAndShippAddressForm.lastName": {
                required: true,
                alpha: true
            },
            "billAndShippAddressForm.email": {
                required: true,
                emailCustom: true
            },
            "billAndShippAddressForm.phone": {
                required: true,
                phoneUS: true
            },
            "billAndShippAddressForm.phoneExt": {
                number: true,
                maxlength: 10,
            },
            "billAndShippAddressForm.billingOrgName": {
                required: true,
                alphabetsnspace: true
            },
            "billAndShippAddressForm.Line1": {
                required: true,
                alphanumeric: true
            },
            "billAndShippAddressForm.Line2": {
                required: false,
                alphanumeric: true
            },
            "billAndShippAddressForm.city": {
                required: true,
                alpha: true
            },
            "billAndShippAddressForm.billingState": {
                required: true,
            },
            "billAndShippAddressForm.postalCode": {
                required: true,
                zipcodeUS: true
            },
            "billAndShippAddressForm.acctStmtEmail": {
                required: true,
                emailCustom: true
            },
            "billAndShippAddressForm.invoiceEmail": {
                required: true,
                emailCustom: true
            }
        },
        messages: {
            "billAndShippAddressForm.firstName": {
                required: "Please enter a first name.",
                alpha: "Please enter Only Letters"
            },
            "billAndShippAddressForm.lastName": {
                required: "Please enter a last name.",
                alpha: "Please enter Only Letters"
            },
            "billAndShippAddressForm.email": {
                required: "Please enter a valid email address.",
                emailCustom: "Please enter a valid email address.",
            },
            "billAndShippAddressForm.phone": {
                required: "Please enter a valid phone number.",
                phoneUS: "Please specify a valid phone number"
            },
            "billAndShippAddressForm.phoneExt": {
                number: "Please enter your Valid Extn Number",
                maxlength: "Please enter your Valid Extn Number Max is 10",
            },
            "billAndShippAddressForm.billingOrgName": {
                required: "Please enter your Organization Name",
                alphabetsnspace: "Please enter Only Letters"
            },
            "billAndShippAddressForm.Line1": {
                required: "Please enter an address.",
                alphanumeric: "Please enter Only Letters"
            },
            "billAndShippAddressForm.Line2": {
                required: "Please enter your Address2",
                alphanumeric: "Please enter Only Letters"
            },
            "billAndShippAddressForm.city": {
                required: "Please enter a city.",
                alpha: "Please enter Only Letters"
            },
            "billAndShippAddressForm.billingState": "Please select a state.",
            "billAndShippAddressForm.postalCode": {
                required: "Please enter a valid US zip code.",
                zipcodeUS: "Please enter a valid US zip code."
            },
            "billAndShippAddressForm.acctStmtEmail": {
                required: "Please enter a valid email address.",
                emailCustom: "Please enter a valid email address.",
            },
            "billAndShippAddressForm.invoiceEmail": {
                required: "Please enter a valid email address.",
                emailCustom: "Please enter a valid email address.",
            }
        },
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            // Add `has-feedback` class to the parent div.form-group
            // in order to add icons to inputs
            element.parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-feedback");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!element.next("span")[0]) {
                $("<span class='glyphicon glyphicon-exclamation-sign form-control-feedback'></span>").insertAfter(element);
            }
        },
        success: function (label, element) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!$(element).next("span")[0]) {
                //$("<span class='glyphicon form-control-feedback'></span>").insertAfter($(element));
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-error").removeClass("has-success");
            $(element).next("span").addClass("glyphicon-exclamation-sign").removeClass("glyphicon-ok");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-success").removeClass("has-error");
            $(element).next("span").removeClass("glyphicon-exclamation-sign");
        }
    });
    $(".registration_billing .registration_save").click(function () {
        if ($('#billingForm').valid()) {
        
         // save to backend
          var str = $("#billingForm").serialize();
       var status = "failed"
      $.ajax({
    type:"post",
    data:str,
    url:ACC.config.contextPath+ "/register/saveRegData",
    async: false,
    dataType: "json",
    success: function(data){
      status  = "success";
      alert("success")
    },
                error: function() { 
                    alert("error"); 
                }
         });
          
          //save to backend end
        
            $(".registrationFlow").hide();
            $(".registration_paying").show();
            $("#registration_nagivationPart3 .registration_nagivationName li:first-child").removeClass("active");
            $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(2)").addClass("active");
            topFunction();
        }
    });
    /* Billing form Rules end*/
    /* Paying form Rules Start*/
    $("#payingForm").validate({
        rules: {
            "payerAddressForm.firstName": {
                required: true,
                alpha: true
            },
            "payerAddressForm.lastName": {
                required: true,
                alpha: true
            },
            "payerAddressForm.email": {
                required: true,
                emailCustom: true
            },
            "payerAddressForm.phone": {
                required: true,
                phoneUS: true
            },
            "payerAddressForm.phoneExt": {
                number: true,
                maxlength: 10,
            },
            "payerAddressForm.billingOrgName": {
                required: true,
                alphabetsnspace: true
            },
            "payerAddressForm.Line1": {
                required: true,
                alphanumeric: true
            },
            "payerAddressForm.Line2": {
                required: false,
                alphanumeric: true
            },
            "payerAddressForm.city": {
                required: true,
                alpha: true
            },
            "payerAddressForm.billingState": {
                required: true,
            },
            "payerAddressForm.postalCode": {
                required: true,
                zipcodeUS: true
            }
        },
        messages: {
            "payerAddressForm.firstName": {
                required: "Please enter a first name.",
                alpha: "Please enter Only Letters"
            },
            "payerAddressForm.firstName": {
                required: "Please enter a last name.",
                alpha: "Please enter Only Letters"
            },
            "payerAddressForm.email": {
                required: "Please enter a valid email address.",
                emailCustom: "Please enter a valid email address.",
            },
            "payerAddressForm.phone": {
                required: "Please enter a valid phone number.",
                phoneUS: "Please specify a valid phone number"
            },
            "payerAddressForm.phoneExt": {
                number: "Please enter your Valid Extn Number",
                maxlength: "Please enter your Valid Extn Number Max is 10",
            },
            "payerAddressForm.billingOrgName": {
                required: "Please enter organization name for payer.",
                alphabetsnspace: "Please enter Only Letters"
            },
            "payerAddressForm.Line1": {
                required: "Please enter an address.",
                alphanumeric: "Please enter Only Letters"
            },
            "payerAddressForm.Line1": {
                required: "Please enter your Address2",
                alphanumeric: "Please enter Only Letters"
            },
            "payerAddressForm.city": {
                required: "Please enter a city.",
                alpha: "Please enter Only Letters"
            },
            "payerAddressForm.billingState": "Please select a state.",
            "payerAddressForm.postalCode": {
                required: "Please enter a valid US zip code.",
                zipcodeUS: "Please enter a valid US zip code."
            }
        },
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            // Add `has-feedback` class to the parent div.form-group
            // in order to add icons to inputs
            element.parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-feedback");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!element.next("span")[0]) {
                $("<span class='glyphicon glyphicon-exclamation-sign form-control-feedback'></span>").insertAfter(element);
            }
        },
        success: function (label, element) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!$(element).next("span")[0]) {
                //$("<span class='glyphicon form-control-feedback'></span>").insertAfter($(element));
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-error").removeClass("has-success");
            $(element).next("span").addClass("glyphicon-exclamation-sign").removeClass("glyphicon-ok");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-success").removeClass("has-error");
            $(element).next("span").removeClass("glyphicon-exclamation-sign");
        }
    });
    $(".registration_paying .registration_save").click(function () {
        if ($('#payingForm').valid()) {
        
                 // save to backend
          var str = $("#businessForm").serialize();
       var status = "failed"
      $.ajax({
    type:"post",
    data:str,
    url:ACC.config.contextPath+ "/register/saveRegData",
    async: false,
    dataType: "json",
    success: function(data){
      status  = "success";
      alert("success")
    },
                error: function() { 
                    alert("error"); 
                }
         });
          
          //save to backend end
        
            $(".registrationFlow").hide();
            $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(2)").removeClass("active");
            $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(3)").addClass("active");
            topFunction();
            if (shippingTemplate == true) {
                $(".registration_uploadConfirm").show();
            } else {
                $(".registration_shipping").show();
            }
        }
    });
    /* Paying form Rules End*/
    /* Shipping form Rules Start*/
    $("#shippingForm").validate({
        rules: {
            "shippingAddressForm[0].firstName": {
                required: true,
                alpha: true
            },
            "shippingAddressForm[0].lastName": {
                required: true,
                alpha: true
            },
            "shippingAddressForm[0].email": {
                required: true,
                emailCustom: true
            },
            "shippingAddressForm[0].phone": {
                required: true,
                phoneUS: true
            },
            "shippingAddressForm[0].phoneExt": {
                number: true,
                maxlength: 10,
            },
            "shippingAddressForm[0].billingOrgName": {
                required: true,
                alphabetsnspace: true
            },
            "shippingAddressForm[0].Line1": {
                required: true,
                alphanumeric: true
            },
            "shippingAddressForm[0].city": {
                required: true,
                alpha: true
            },
            "shippingAddressForm[0].billingState": {
                required: true,
            },
            "shippingAddressForm[0].postalCode": {
                required: true,
                zipcodeUS: true
            },
            "shippingAddressForm[0].licenseDetailsForm.licenseName": {
                required: true,
            },
            "shippingAddressForm[0].licenseDetailsForm.licenseNum": {
                required: true,
                stateLicense: true
            },
            "shippingAddressForm[0].licenseDetailsForm.licAddressLine1": {
                required: true,
                alphanumeric: true
            },
            "shippingAddressForm[0].licenseDetailsForm.licenseCity": {
                required: true,
                alpha: true
            },
            "shippingAddressForm[0].licenseDetailsForm.licenseState": {
                required: true,
            },
            "shippingAddressForm[0].licenseDetailsForm.zipcode": {
                required: true,
                zipcodeUS: true
            }
        },
        messages: {
            "shippingAddressForm[0].firstName": {
                required: "Please enter a first name.",
                alpha: "Please enter Only Letters"
            },
            "shippingAddressForm[0].lastName": {
                required: "Please enter a last name.",
                alpha: "Please enter Only Letters"
            },
            "shippingAddressForm[0].email": {
                required: "Please enter a valid email address.",
                emailCustom: "Please enter a valid email address.",
            },
            "shippingAddressForm[0].phone": {
                required: "Please enter a valid phone number.",
                phoneUS: "Please specify a valid phone number"
            },
            "shippingAddressForm[0].phoneExt": {
                number: "Please enter your Valid Extn Number",
                maxlength: "Please enter your Valid Extn Number Max is 10",
            },
            "shippingAddressForm[0].billingOrgName": {
                required: "Please enter organization name for shipping.",
                alphabetsnspace: "Please enter Only Letters"
            },
            "shippingAddressForm[0].Line1": {
                required: "Please enter an address.",
                alphanumeric: "Please enter Only Letters"
            },
            "shippingAddressForm[0].city": {
                required: "Please enter a city.",
                alpha: "Please enter Only Letters"
            },
            "shippingAddressForm[0].billingState": "Please select a state.",
            "shippingAddressForm[0].postalCode": {
                required: "Please enter a valid US zip code.",
                zipcodeUS: "Please enter a valid US zip code."
            },
            "shippingAddressForm[0].licenseDetailsForm.licAddressLine1": {
                required: "Please enter an address.",
                alphanumeric: "Please enter Only Letters"
            },
            "shippingAddressForm[0].licenseDetailsForm.licenseCity": {
                required: "Please enter a city.",
                alpha: "Please enter Only Letters"
            },
            "shippingAddressForm[0].licenseDetailsForm.licenseState": "Please select a state.",
            "shippingAddressForm[0].licenseDetailsForm.zipcode": {
                required: "Please enter a valid US zip code.",
                zipcodeUS: "Please enter a valid US zip code."
            },
            "shippingAddressForm[0].licenseDetailsForm.licenseName": {
                required: "Please enter a License name",
            },
            "shippingAddressForm[0].licenseDetailsForm.licenseNum": {
                required: "Please enter a License Number",
                stateLicense: "Please enter a valid state license number"
            },
        },
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            // Add `has-feedback` class to the parent div.form-group
            // in order to add icons to inputs
            element.parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-feedback");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!element.next("span")[0]) {
                $("<span class='glyphicon glyphicon-exclamation-sign form-control-feedback'></span>").insertAfter(element);
            }
        },
        success: function (label, element) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!$(element).next("span")[0]) {
                //$("<span class='glyphicon form-control-feedback'></span>").insertAfter($(element));
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-error").removeClass("has-success");
            $(element).next("span").addClass("glyphicon-exclamation-sign").removeClass("glyphicon-ok");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-6, .col-md-4, .col-md-2").addClass("has-success").removeClass("has-error");
            $(element).next("span").removeClass("glyphicon-exclamation-sign");
        }
    });
    $(".registration_shipping .registration_save").click(function () {
        if ($('#shippingForm').valid()) {
        
                 // save to backend
          var str = $("#shippingForm").serialize();
       var status = "failed"
      $.ajax({
    type:"post",
    data:str,
    url:ACC.config.contextPath+ "/register/saveRegData",
    async: false,
    dataType: "json",
    success: function(data){
      status  = "success";
      alert("success")
    },
                error: function() { 
                    alert("error"); 
                }
         });
          
          //save to backend end
          
            
            var shipCount = $('.registration_locations').length;
            var totalcount = shipCount;
            if(""!=$('#shipping-orgname').val()){
            alert("notnull");
            totalcount = totalcount + 1;
            }
            $(".total_ship_location").html(totalcount);
            $(".registrationFlow").hide();
            $(".registration_review").show();
            reviewDatashow();
            $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(3)").removeClass("active");
            $("#registration_nagivationPart3 .registration_nagivationName").removeClass("active");
            $("#registration_nagivationPart4 .registration_nagivationName").removeClass("inactive").addClass("active");
            $("#registration_nagivationPart3 .registration_nagivationNumeric").hide();
            $("#registration_nagivationPart3 .registration_nagivationtick").show();

 

            topFunction();
        }
    });
    /* Shipping form Rules end*/
    $('#reviewForm').validate({
        // rules,
        submitHandler: function (form) {
            // serialize and join data for all forms
            // ajax submit
            console.log($("#businessForm, #billingForm ,#payingForm, #shippingForm").serializeArray());
            event.preventDefault();
            alert('go ajax');
            return false;
        }
    });

    function reviewDatashow() {
        /* bussiness data */
        $('#review-orgname').html($('#business-orgname').val());
        $('#review-duns').html($('#business-dunnumber').val());
        $('#review-bussiness-address1').html($('#business-address1').val());
        $('#review-bussiness-address2').html($('#business-address2').val());
        $('#review-bussiness-city-zip').html($('#business-city').val() + ', ' + $('#business-state').val() + ' ' + $('#business-zipcode').val());
        /* bussiness data */
         /* billing data */
        $('#review-billing-name').html($('#billing-firstname').val() + ' ' + $('#billing-lastname').val());
        $('#review-billing-email').html($('#billing-email').val());
        $('#review-billing-phone').html($('#billing-phone').val() + ' ext. ' + $('#billing-extn').val());
        $('#review-billing-address1').html($('#billing-address1').val());
        $('#review-billing-address2').html($('#billing-address2').val());
        $('#review-billing-city-zip').html($('#billing-city').val() + ', ' + $('#billing-state').val() + ' ' + $('#billing-zipcode').val());
        $('#review-billing-invoice-email').html($('#billing-invoiceemail').val());
        $('#review-billing-account-email').html($('#billing-accountemail').val());
        if ($("#billing-invoiceemail").val() == '') {
            $("#review-billing-invoiceType_E").prop("checked", false);
        } else {
            $("#review-billing-invoiceType_E").prop("checked", true);
        }
        if ($("#billing-accountemail").val() == '') {
            $("#review-billing-accountType_E").prop("checked", false);
        } else {
            $("#review-billing-accountType_E").prop("checked", true);
        }
        /* billing data */
        /* paying data */
        $('#review-paying-name').html($('#pay-firstname').val() + ' ' + $('#pay-lastname').val());
        $('#review-paying-email').html($('#pay-email').val());
        $('#review-paying-phone').html($('#pay-phone').val() + ' ext. ' + $('#pay-ext').val());
        $('#review-paying-adrress1').html($('#pay-address1').val());
        $('#review-paying-adrress2').html($('#pay-address2').val());
        $('#review-paying-city-zip').html($('#pay-city').val() + ', ' + $('#pay-state').val() + ' ' + $('#pay-zipcode').val());
        /* Paying data */
    }
    /* registration back starts */
    $(".registration_billing .registration_back,#registration_Businessedit").click(function () {
        $(".registrationFlow").hide();
        $(".registration_business").show();
        $(".registration_nagivationName").removeClass("active");
        $("#registration_nagivationPart2 .registration_nagivationName").addClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName li").hide();
        $("#registration_nagivationPart3 .registration_nagivationName li:first-child").removeClass("active");
        topFunction();
    });
    $(".registration_paying .registration_back,#registration_Billingedit").click(function () {
        $(".registrationFlow").hide();
        $(".registration_billing").show();
        $(".registration_nagivationName").removeClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName").addClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName li:first-child").addClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(2)").removeClass("active");
        topFunction();
    });
    $(".registration_shipping .registration_back,#registration_Payingedit").click(function () {
        $(".registrationFlow").hide();
        $(".registration_paying").show();
        $(".registration_nagivationName").removeClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName").addClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(2)").addClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(3)").removeClass("active");
        topFunction();
    });
    $(".registration_review .registration_back,#registration_Shippingedit").click(function () {
        $(".registrationFlow").hide();
        $(".registration_nagivationName").removeClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName").addClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(3)").addClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName").addClass("active");
        topFunction();
        if (shippingTemplate == true) {
            $(".registration_uploadConfirm").show();
        } else {
            $(".registration_shipping").show();
        }
    });
    $(".registration_uploadConfirm .registration_back").click(function () {
        $(".registrationFlow").hide();
        $(".registration_paying").show();
        $('.registration_uploadFile p').empty();
        $('.registration_uploadFile p').css("display", "none");
        $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(2)").addClass("active");
        $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(3)").removeClass("active");
        topFunction();
    });

    /* registration back ends */
    /* billing contact copy to payer code start */
    var same = false; //set simple Boolean
    function samepayer() {
        var i = parseInt($('#billing-state').get(0).selectedIndex); //find selectedIndex of dropdown
        if (!same) {
            $('#pay-orgname').val($('#billing-orgname').val()); //copy values
            $('#pay-address1').val($('#billing-address1').val());
            $('#pay-address2').val($('#billing-address2').val());
            $('#pay-city').val($('#billing-city').val());
            $('#pay-zipcode').val($('#billing-zipcode').val());
            $('#pay-state').prop('selectedIndex', i); //copy whatever the selectedIndex value is
            same = true;
        } else {
            $('#pay-orgname').val('');
            $('#pay-address1').val('');
            $('#pay-address2').val('');
            $('#pay-city').val('');
            $('#pay-zipcode').val('');
            $('#pay-state').prop('selectedIndex', 0); //selectedIndex 47 = Washington in this example
            same = false;
        }
    }
    $('#bill-payer_check').on('change', function () { //listen for the checkbox change event
        if ($(this).prop('checked')) { //is the #payercheck checkbox checked?
            samepayer(); //copy the values over
        } else {
            samepayer(); //clear the values when unchecked
        }
    });
    /* billing contact copy to payer code end */
    /* billing contact copy to shipping code start */
    var sameship = false; //set simple Boolean
    function sameshipping() {
        var i = parseInt($('#billing-state').get(0).selectedIndex); //find selectedIndex of dropdown
        if (!sameship) {
            $('#shipping-orgname').val($('#billing-orgname').val()); //copy values
            $('#shipping-address1').val($('#billing-address1').val());
            $('#shipping-address2').val($('#billing-address2').val());
            $('#shipping-city1').val($('#billing-city').val());
            $('#shipping-zip1').val($('#billing-zipcode').val());
            $('#shipping-state1').prop('selectedIndex', i); //copy whatever the selectedIndex value is
            sameship = true;
        } else {
            $('#shipping-orgname').val('');
            $('#shipping-address1').val('');
            $('#shipping-address2').val('');
            $('#shipping-city1').val('');
            $('#shipping-zip1').val('');
            $('#shipping-state').prop('selectedIndex', 0);
            sameship = false;
        }
    }
    $('#bill-ship_check').on('change', function () { //listen for the checkbox change event
        if ($(this).prop('checked')) { //is the #payercheck checkbox checked?
            sameshipping(); //copy the values over
        } else {
            sameshipping(); //clear the values when unchecked
        }
    });
    /*billing contact copy to shipping code  end */
    /* Shipping contact copy to shipping licence code start */
    var sameshiplic = false; //set simple Boolean
    function sameshippinglicence() {
        var i = parseInt($('#shipping-state1').get(0).selectedIndex); //find selectedIndex of dropdown
        if (!sameshiplic) {
            $('#shipping-address4').val($('#shipping-address1').val()); //copy values
            $('#shipping-address3').val($('#shipping-address2').val());
            $('#shipping-city2').val($('#shipping-city1').val());
            $('#shipping-zip2').val($('#shipping-zip1').val());
            $('#shipping-state2').prop('selectedIndex', i); //copy whatever the selectedIndex value is
            sameshiplic = true;
        } else {
            $('#shipping-address3').val('');
            $('#shipping-address4').val('');
            $('#shipping-city2').val('');
            $('#shipping-zip2').val('');
            $('#shipping-state2').prop('selectedIndex', 0);
            sameshiplic = false;
        }
    }
    $('#shipping-checkbox').on('change', function () { //listen for the checkbox change event
        if ($(this).prop('checked')) { //is the #payercheck checkbox checked?
            sameshippinglicence(); //copy the values over
        } else {
            sameshippinglicence(); //clear the values when unchecked
        }
    });
    /*shipping contact copy shippingto  licence code  end */
    /* shipping template starts */
    $(".registration_uploadConfirm .registration_save").click(function () {
        if (!($(this).hasClass("inactive"))) {
            $(".registrationFlow").hide();
            $(".registration_review").show();
            $("#registration_nagivationPart3 .registration_nagivationName li:nth-child(3)").removeClass("active");
            $("#registration_nagivationPart3 .registration_nagivationName").removeClass("active");
            $("#registration_nagivationPart4 .registration_nagivationName").removeClass("inactive").addClass("active");
            $("#registration_nagivationPart3 .registration_nagivationNumeric").hide();
            $("#registration_nagivationPart3 .registration_nagivationtick").show();
            topFunction();
            shippingTemplate = true;
        }
    });
    $("#registration_upload").click(function () {
        $("#registration_shippingUploadpop").show();
        $("html").css("overflow", "hidden");
    });
    $(".registration_shippingUploadclose").click(function () {
        $("#registration_shippingUploadpop").hide();
        $("html").css("overflow", "auto");
        $('#registration_uploadlocation, #registration_uploadlocation1').removeClass("inactive");
        $(".registration_shippingSave").addClass("inactive");
        $('.registration_uploadFile').css("visibility", "hidden");
        $('.registration_uploadFile div').empty();
        $('#registration_uploadInput').val("");
    });
    $('#registration_uploadlocation, #registration_uploadlocation1').on('click', function () {
        if (!($(this).hasClass("inactive"))) {
            $('#registration_uploadInput').trigger('click');
        }
    });
    $('#registration_uploadInput').change(function () {
        var ext = $('#registration_uploadInput').val().split('.').pop().toLowerCase();
        var filename = $('#registration_uploadInput').val().replace(/.*(\/|\\)/, '');
        var maxletter = $('.registration_uploadFile div');
        if ($.inArray(ext, ['xls', 'xlsx']) == 1) {
            $('.registration_uploadFile div').html(filename);
            maxletter.text(maxletter.text().substring(0, 20) + '...' + ext);
            $('.registration_uploadFile').css("visibility", "visible");
            $('.registration_uploadclose').css("display", "block");
            $('.registration_uploadFile p').css("display", "none");
            $('#registration_uploadlocation, #registration_uploadlocation1').addClass("inactive");
            $(".registration_shippingSave,.registration_uploadConfirm .registration_save").removeClass("inactive");
        } else {
            $('.registration_uploadclose').css("display", "none");
            $('.registration_uploadFile p').css("display", "block");
            $('.registration_uploadFile p').html("Please upload file with .xls/xlsx extension");
            $('.registration_uploadFile').css("visibility", "visible");
            $('#registration_uploadlocation, #registration_uploadlocation1').removeClass("inactive");
            $(".registration_shippingSave,.registration_uploadConfirm .registration_save").addClass("inactive");
            $('.registration_uploadFile div').empty();
            $('#registration_uploadInput').val("");
        }
    });
    $(".registration_uploadclose").click(function () {
        $('#registration_uploadlocation, #registration_uploadlocation1').removeClass("inactive");
        $(".registration_shippingSave,.registration_uploadConfirm .registration_save").addClass("inactive");
        $('.registration_uploadFile').css("visibility", "hidden");
        $('.registration_uploadFile div').empty();
        $('#registration_uploadInput').val("");
    });
    $(".registration_shippingSave").click(function () {
        if (!($(this).hasClass("inactive"))) {
            $('#registration_shippingUploadpop').hide();
            $(".registration_shipping").hide();
            $(".registration_uploadConfirm").show();
            $("html").css("overflow", "auto");
        }
    });
    $(".registration_manualLocation").click(function () {
        $(".registration_uploadConfirm").hide();
        $(".registration_shipping").show();
        $('#registration_uploadlocation, #registration_uploadlocation1').removeClass("inactive");
        $('.registration_uploadFile').css("visibility", "hidden");
        $('.registration_uploadFile div').empty();
        $('#registration_uploadInput').val("");
        shippingTemplate = false;
    });
	$(".registration_addbutton").click(function () {
        $(".registration_locations").show();
        topFunction();
    });

    /* shipping template ends */

    $(".registration_helpclose, .registration_helpLeave, .registration_helpContinue").click(function () {
        $("#registration_helppop").hide();
        $("html").css("overflow", "auto");
    });
    $(".registration_helpLeave,.registration_helpContinue").hover(function () {
        $(".registration_helpContinue").removeClass("active");
    });

$('.registration_helpLeave').click(function (e) {
window.location.href = urlLink1;
$("#registration_helppop").hide();
});

 function abandonPopup() {
if ($("body").hasClass("page-registrationpage")) {
$("a").addClass('showPopupclick');
$("a.showPopupclick").click(function (e) {
urlLink1 = $(this).attr("href");
e.preventDefault();
$("#registration_helppop").show();
topFunction();
$(".registration_helpContinue").addClass("active");
$("html").css("overflow", "hidden");
});
}
}


});
/* Registration Custom methods */

$.validator.addMethod("alphabetsnspace", function (value, element) {
    return this.optional(element) || /^(?!\s)(?!.*\s$)[A-Za-z][A-Za-z\-\,\s]*$/g.test(value);
});
$.validator.addMethod("alpha", function (value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
});
$.validator.addMethod("alphanumeric", function (value, element) {
    return this.optional(element) || /^[a-z0-9\-\,\#\s]+$/i.test(value);
});
$.validator.addMethod("zipcodeUS", function (value, element) {
    return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
});
$.validator.addMethod("emailCustom", function (email, element) {
    const filter = /^(?=(.{1,64}@.{1,255}))([!#$%&'*+\-\/=?\^_`{|}~a-zA-Z0-9}]{1,64}(\.[!#$%&'*+\-\/=?\^_`{|}~a-zA-Z0-9]{0,}){0,})@((\[(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}\])|([a-zA-Z0-9-]{1,63}(\.[a-zA-Z0-9-]{2,63}){1,}))$/;
    return this.optional(element) || filter.test(email)
});
$.validator.addMethod("phoneUS", function (phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    //phone_number = phone_number.replace(/(\d{3})(\d{3})(\d{4})/, '($1)-$2-$3');
    return this.optional(element) || phone_number.length > 9 && phone_number.match(
            /^(\([2-9]([02-9]\d|1[02-9])\)|[2-9]([02-9]\d|1[02-9])) ?[2-9]\d{2}-?\d{4}$/);
}, "Please specify a valid phone number");
$.validator.addMethod("stateLicense", function (value, element) {
    return this.optional(element) || /^[a-z0-9\-_><={,\]\/\+}\[\|\.\?\\\s]+$/i.test(value);
}, "Please enter a valid state license number.");

 $(".downloadTemplate").on("click", function(e) {
       window.location.href ="_ui/responsive/theme-lambda/images/eCommerce-Excel-Customer-Spreadsheet.xls";
       });
       
              	/* Add shipping location starts here */
			var count = 0;
			
		$(".registration_addbutton").click(function () {
			var shipCount = $('.registration_locations').length;
            var finalcount = shipCount + 1;
            console.log(finalcount);
			count++;
			
			var shippingfirstname = $('#shipping-firstname').val();
			var shippinglastname = $('#shipping-lastname').val();
			var shippingemail = $('#shipping-email').val();
			var shippingphno = $('#shipping-phno').val();
			var shippingextension = $('#shipping-extension').val();
			var shippingorgname = $('#shipping-orgname').val();
			var shippingaddress1 = $('#shipping-address1').val();
			var shippingaddress2 = $('#shipping-address2').val();
			var shippingcity1 = $('#shipping-city1').val();
			var shippingstate1 = $('#shipping-state1').val();
			var shippingzip1 = $('#shipping-zip1').val();
			var shippinglicensename = $('#shipping-licensename').val();
			var shippinglicenseno = $('#shipping-licenseno').val();
			var shippingaddress3 = $('#shipping-address3').val();
			var shippingaddress4 = $('#shipping-address4').val();
			var shippingcity2 = $('#shipping-city2').val();
			var shippingstate2 = $('#shipping-state2').val();
			var shippingzip2 = $('#shipping-zip2').val();
			
			
			$(".print_shipping").append("<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 registration_locations'><div class='registration_locationparent'><div class='registration_locationLine1'><div class='registration_locationName'>Location <span class='locationno'>"+finalcount+"</span></div><div class='registration_locationOrg'>"+shippingorgname+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].billingOrgName' value='" + shippingorgname +"'></div><div class='clearboth'></div></div><div class='registration_edit ship_del'>Remove <i class='fa fa-trash' aria-hidden='true'></i></div><div class='registration_edit only_edit_ship'>Edit <img src='_ui/responsive/theme-lambda/images/edit.png' width='20' alt='Edit'></div><div class='clearboth'></div></div><div class='registration_locationLine2'><div class='registration_locationcontact'><div class='registration_locationcontactline1'>Contact Info</div><div class='registration_locationcontactname'><span>"+shippingfirstname+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].firstName' value='" + shippingfirstname +"'></span>&nbsp;<span>"+shippinglastname+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].lastName' value='" + shippinglastname +"'></span></div><div class='registration_locationcontactemail'>"+shippingemail+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].email' value='" + shippingemail +"'></div><div class='registration_locationcontactphn'><span>"+shippingphno+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].phone' value='" + shippingphno +"'></span> ext. <span>"+shippingextension+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].phoneExt' value='" + shippingextension +"'></span></div></div><div class='registration_locationaddress'><div class='registration_locationcontactline1'>Address</div><div class='registration_locationaddress1'>"+shippingaddress1+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].Line1' value='" + shippingaddress1 +"'></div><div class='registration_locationaddress2'>"+shippingaddress2+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].Line2' value='" + shippingaddress2 +"'></div><div class='registration_locationaddress4'><span>"+shippingcity1+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].city' value='" + shippingcity1 +"'></span>, <span>"+shippingstate1+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].billingState' value='" + shippingstate1 +"'></span> <span>"+shippingzip1+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].postalCode' value='" + shippingzip1 +"'></span></div></div><div class='registration_locationLic'><div class='registration_locationcontactline1'>License</div><div class='registration_locationlic1'><span>"+shippinglicensename+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].licenseDetailsForm.licenseName' value='" + shippinglicensename +"'></span> - <span>"+shippinglicenseno+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].licenseDetailsForm.licenseNum' value='" + shippinglicenseno +"'></span></div><div class='registration_locationlic3'>"+shippingaddress3+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].licenseDetailsForm.licAddressLine1' value='" + shippingaddress3 +"'></div><div class='registration_locationlic4'>"+shippingaddress4+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].licenseDetailsForm.licAddressLine2' value='" + shippingaddress4 +"'></div><div class='registration_locationlic5'><span>"+shippingcity2+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].licenseDetailsForm.licenseCity' value='" + shippingcity2 +"'></span>, <span>"+shippingstate2+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].licenseDetailsForm.licenseState' value='" + shippingstate2 +"'></span> <span>"+shippingzip2+" <input type='hidden' name='shippingAddressForm[" + finalcount +"].licenseDetailsForm.zipcode' value='" + shippingzip2 +"'></span></div></div><div class='clearboth'></div></div></div>");
			setTimeout(function(){
               
                $("#registration_section4 .form-control").val('');
				$("#registration_section4 .registration_input").val('');
				$("#registration_section4 .form-group").val('');
               //$('#registration_section4 .registration_checkbox').prop('checked', false);
               

            }, 1000);
			
			$(".registration_locations").show();
			$(".shipping-block").hide();
			$(".registration_addbutton").hide();
			$(".only_show_field").show();
			
			$(this).addClass("registration_addbutton_disable");
			
			topFunction();
			
		});
		
		    
		$(".only_show_field").click(function () {
			
			var shipCount = $('.registration_locations').length;
			if(shipCount > 0){
				$('.add_ship_del').show();
			}
			$(".shipping-block").show();
			$(".registration_addbutton").show();
			$(this).hide();
			$('#registration_section4 .registration_checkbox').prop('checked', false).removeAttr('checked');
			
			
		$('#shipping-checkbox').on('change', function () { //listen for the checkbox change event
			var i = parseInt($('#shipping-state1').get(0).selectedIndex); //find selectedIndex of dropdown
			if ($("#shipping-address3, #shipping-address4, #shipping-city2, #shipping-zip2, #shipping-state2").val()== '') {
            $('#shipping-address4').val($('#shipping-address1').val()); //copy values
            $('#shipping-address3').val($('#shipping-address2').val());
            $('#shipping-city2').val($('#shipping-city1').val());
            $('#shipping-zip2').val($('#shipping-zip1').val());
            $('#shipping-state2').prop('selectedIndex', i); //copy whatever the selectedIndex value is
            
			} else {
            $('#shipping-address3').val('');
            $('#shipping-address4').val('');
            $('#shipping-city2').val('');
            $('#shipping-zip2').val('');
            $('#shipping-state2').prop('selectedIndex', 0);
            
			}
		});
			
			
			
            
    
		});	
		
		 
		$('.registration_shipping .field_mandatory').on('keyup change', function() {
 
			if($('#shippingForm').valid() && $('.registration_shipping .field_mandatory').val()!=''){
				 $(".registration_shipping .registration_addbutton").removeClass("registration_addbutton_disable");
				 console.log('good');
			 }
			 else{
				 
				  $('#shippingForm .form-control-feedback').removeClass('glyphicon-exclamation-sign');
				  $('#shippingForm .form-group').removeClass('has-feedback');
				  $('#shippingForm .form-group').removeClass('has-error');
				  $('#shippingForm .error').html(''); 
				  $(".registration_shipping .registration_addbutton").addClass("registration_addbutton_disable");
				 console.log('Bad');
			 }
			 
		 
		 
		 
		});
		
		$('#shipping-checkbox').on('change', function () { //listen for the checkbox change event
		setTimeout(function(){
			if ($('#shippingForm').valid()) { //is the #payercheck checkbox checked?
            $(".registration_addbutton").removeClass("registration_addbutton_disable");
				 console.log('click good');
			} else {
            
				  $('#shippingForm .form-control-feedback').removeClass('glyphicon-exclamation-sign');
				  $('#shippingForm .form-group').removeClass('has-feedback');
				  $('#shippingForm .form-group').removeClass('has-error');
				  $('#shippingForm .error').html(''); 
				  $(".registration_shipping .registration_addbutton").addClass("registration_addbutton_disable");
				 console.log('click Bad');
			}
			}, 100);
        });
	
	$('.print_shipping').on('click', '.ship_del', function() {
           $(this).parent().parent().remove();
           $('.registration_locationName .locationno').each(function(index) {
           $(this).html(index + 1);
           console.log('count' + index);
           });
		   
		   var shipCount = $('.print_shipping .registration_locations').length;
			if(shipCount < 1){
				$('#registration_section4 .registration_checkbox').prop('checked', false);
				$('.registration_addbutton').show();
				$(".only_show_field").hide();
				$(".shipping-block").show();
				$(".add_ship_del").hide();
				$(".registration_shipping .registration_addbutton").addClass("registration_addbutton_disable");
				
			$('#shipping-checkbox').on('change', function () { //listen for the checkbox change event
			setTimeout(function(){
			var i = parseInt($('#shipping-state1').get(0).selectedIndex); //find selectedIndex of dropdown
			if ($("#shipping-address3, #shipping-address4, #shipping-city2, #shipping-zip2, #shipping-state2").val()== '') {
            $('#shipping-address4').val($('#shipping-address1').val()); //copy values
            $('#shipping-address3').val($('#shipping-address2').val());
            $('#shipping-city2').val($('#shipping-city1').val());
            $('#shipping-zip2').val($('#shipping-zip1').val());
            $('#shipping-state2').prop('selectedIndex', i); //copy whatever the selectedIndex value is
			$(".registration_shipping .registration_addbutton").removeClass("registration_addbutton_disable");
            
			} else {
            $('#shipping-address3').val('');
            $('#shipping-address4').val('');
            $('#shipping-city2').val('');
            $('#shipping-zip2').val('');
            $('#shipping-state2').prop('selectedIndex', 0);
			$(".registration_shipping .registration_addbutton").addClass("registration_addbutton_disable");
            
			}
			}, 100);
		    });
				
			}
			
			
			
    });
	
	$(".add_ship_del").click(function () {
		$(".registration_addbutton").hide();
		$(".only_show_field").show();
		$(this).parent().parent().hide();
		
		var shipCount = $('.print_shipping .registration_locations').length;
			if(shipCount < 1){
				$('.shipping-block').show();
				$('.shipping-block .registration_checkbox').prop('checked', false);
			}
		  
		 $('#shipping-checkbox').on('change', function () { //listen for the checkbox change event
			setTimeout(function(){
			var i = parseInt($('#shipping-state1').get(0).selectedIndex); //find selectedIndex of dropdown
			if ($("#shipping-address3, #shipping-address4, #shipping-city2, #shipping-zip2, #shipping-state2").val()== '') {
            $('#shipping-address4').val($('#shipping-address1').val()); //copy values
            $('#shipping-address3').val($('#shipping-address2').val());
            $('#shipping-city2').val($('#shipping-city1').val());
            $('#shipping-zip2').val($('#shipping-zip1').val());
            $('#shipping-state2').prop('selectedIndex', i); //copy whatever the selectedIndex value is
			$(".registration_shipping .registration_addbutton").removeClass("registration_addbutton_disable");
            
			} else {
            $('#shipping-address3').val('');
            $('#shipping-address4').val('');
            $('#shipping-city2').val('');
            $('#shipping-zip2').val('');
            $('#shipping-state2').prop('selectedIndex', 0);
			$(".registration_shipping .registration_addbutton").addClass("registration_addbutton_disable");
            
			}
			}, 100);
		    });
		
	});	
	
	
	$('.print_shipping').on('keyup change', '.field_mandatory', function() {
 
			if($('#shippingForm').valid() && $(this).parent().parent().parent().find('.location_edit_container .field_mandatory').val()!=''){
				
				$(this).parent().parent().parent().find(".shipping_edit_save").addClass("active");
				$(this).parent().parent().parent().find(".shipping_edit_save").removeClass("click_disable");
				$(this).parent().removeClass('has-feedback');
				$(this).parent().removeClass('has-error');
				console.log('good');
			 }
			 else{
				$(this).parent().addClass('has-feedback');
				$(this).parent().addClass('has-error');
				$(this).parent().parent().parent().find(".shipping_edit_save").removeClass("active"); 
				$(this).parent().parent().parent().find(".shipping_edit_save").addClass("click_disable");
				$(this).parent().append("<span class='glyphicon form-control-feedback glyphicon-exclamation-sign'></span><label id='shipping-email-error' class='error help-block'>Please enter valid input data</label>")
				 console.log('Bad');
			 }
			 
		 
		 
		 
		});
	
	
	
	
	$('.print_shipping').on('click', '.only_edit_ship', function() {
	
		
		
		var finalcount = $(this).parent().parent().find(".locationno").text();	
		   
		var shippingfirstname_edit = $(this).parent().parent().find(".registration_locationcontactname span:first-child").text();   
		var shippinglastname_edit = $(this).parent().parent().find(".registration_locationcontactname span:last-child").text();   
		var shippingemail_edit = $(this).parent().parent().find(".registration_locationcontactemail").text();   
		var shippingphno_edit = $(this).parent().parent().find(".registration_locationcontactphn span:first-child").text();   
		var shippingextension_edit = $(this).parent().parent().find(".registration_locationcontactphn span:last-child").text(); 
		var shippingorgname_edit = $(this).parent().parent().find(".registration_locationOrg").text();  
		var shippingaddress1_edit = $(this).parent().parent().find(".registration_locationaddress1").text();   
		var shippingaddress2_edit = $(this).parent().parent().find(".registration_locationaddress2").text();   
		var shippingcity1_edit = $(this).parent().parent().find(".registration_locationaddress4 span:first-child").text();   
		var shippingstate1_edit = $(this).parent().parent().find(".registration_locationaddress4 span:nth-child(2)").text();
		var shippingzip1_edit = $(this).parent().parent().find(".registration_locationaddress4 span:last-child").text();   
		var shippinglicensename_edit = $(this).parent().parent().find(".registration_locationlic1 span:first-child").text();   
		var shippinglicenseno_edit = $(this).parent().parent().find(".registration_locationlic1 span:last-child").text();  
		var shippingaddress3_edit = $(this).parent().parent().find(".registration_locationlic3").text(); 
		var shippingaddress4_edit = $(this).parent().parent().find(".registration_locationlic4").text(); 
		var shippingcity2_edit = $(this).parent().parent().find(".registration_locationlic5 span:first-child").text(); 
		var shippingstate2_edit = $(this).parent().parent().find(".registration_locationlic5 span:nth-child(2)").text(); 	
		var shippingzip2_edit = $(this).parent().parent().find(".registration_locationlic5 span:last-child").text(); 
		
		console.log(shippingstate1_edit);
		console.log(shippingstate2_edit);
		
		//$(this).parent().parent().append("<input type='text' value='"+shippingphno_edit+"' />");
		$(this).parent().parent().find("#shipping-state1").val(shippingstate1_edit);
		$(this).parent().parent().find("#shipping-state2").val(shippingstate2_edit);
		
		$(this).parent().parent().append("<div class='location_edit_container'><div class='row'><div class='col-md-12'><div class='registration_locationName no-right-border'>Location <span class='locationno'>"+finalcount+"</span></div></div></div><div class='registration_contact padding-B20'>Shipping Location Contact &nbsp;&nbsp;<i class='fa fa-question-circle-o' data-toggle='tooltip' title='Help'></i><div class='registration_edit edit_ship_del'>Remove <i class='fa fa-trash' aria-hidden='true'></i></div></div><div class='padding-R15'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-firstname'>First Name*</label> <input class='registration_input form-control field_mandatory' id='shipping-firstname' name='shipping-firstname' autocomplete='no' value='"+shippingfirstname_edit+"' /></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-lastname'>Last Name*</label> <input class='registration_input form-control field_mandatory' id='shipping-lastname' name='shipping-lastname' autocomplete='no' value='"+shippinglastname_edit+"' /></div><div class='clearboth'></div></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-email'>Email Address*</label> <input class='registration_input form-control field_mandatory' id='shipping-email' name='shipping-email' autocomplete='no' value='"+shippingemail_edit+"' /></div><div class='clearboth'></div><div class='padding-R15'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-phno'>Phone Number*</label> <input class='registration_input form-control field_mandatory phn-mask' id='shipping-phno' name='shipping-phno' autocomplete='no' value='"+shippingphno_edit+"' /></div><div class='col-lg-2 col-md-2 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-extension'>Ext.</label> <input class='registration_input form-control' id='shipping-extension' name='shipping-extension' autocomplete='no' value='"+shippingextension_edit+"' /></div><div class='clearboth'></div></div><div class='registration_contact padding-B20'>Shipping Address</div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-orgname'>Organization Name*</label> <input class='registration_input form-control field_mandatory' id='shipping-orgname' name='shipping-orgname' autocomplete='no' value='"+shippingorgname_edit+"' /></div><div class='clearboth'></div><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-left margin-B30 registration_address form-group'> <label class='registration_inputLabel' for='shipping-lookup1'>Address Lookup</label> <input class='registration_input form-control' id='shipping-lookup1' name='shipping-lookup1' autocomplete='no' /> <span class='glyphicon glyphicon-search form-control-feedback'></span></div><div class='padding-R15'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-address1'>Address Line 1*</label> <input class='registration_input form-control field_mandatory' id='shipping-address1' name='shipping-address1' autocomplete='no' value='"+shippingaddress1_edit+"' /></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-address2'>Address Line 2*</label> <input class='registration_input form-control field_mandatory' id='shipping-address2' name='shipping-address2' autocomplete='no' value='"+shippingaddress2_edit+"' /></div><div class='clearboth'></div></div><div class='padding-R15'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-city1'>City*</label> <input class='registration_input form-control field_mandatory' id='shipping-city1' name='shipping-city1' autocomplete='no' value='"+shippingcity1_edit+"' /></div><div class='col-lg-2 col-md-2 col-sm-6 col-xs-6 no-padding-right margin-B20 registration_mob form-group'> <label class='registration_inputLabel' for='shipping-state1'>State*</label> <select class='registration_state form-control field_mandatory' id='shipping-state1' name='shipping-state1' value='"+shippingstate1_edit+"'><option value='' selected='selected'></option><option value='AL'>AL</option><option value='AK'>AK</option><option value='AR'>AR</option><option value='AZ'>AZ</option><option value='CA'>CA</option><option value='CO'>CO</option><option value='CT'>CT</option><option value='DE'>DE</option><option value='DC'>DC</option><option value='FL'>FL</option><option value='GA'>GA</option><option value='GU'>GU</option><option value='HI'>HI</option><option value='IA'>IA</option><option value='ID'>ID</option><option value='IL'>IL</option><option value='IN'>IN</option><option value='KS'>KS</option><option value='KY'>KY</option><option value='LA'>LA</option><option value='ME'>ME</option><option value='MD'>MD</option><option value='MA'>MA</option><option value='MI'>MI</option><option value='MN'>MN</option><option value='MS'>MS</option><option value='MO'>MO</option><option value='MT'>MT</option><option value='NC'>NC</option><option value='ND'>ND</option><option value='NE'>NE</option><option value='NV'>NV</option><option value='NH'>NH</option><option value='NJ'>NJ</option><option value='NM'>NM</option><option value='NY'>NY</option><option value='OH'>OH</option><option value='OK'>OK</option><option value='OR'>OR</option><option value='PA'>PA</option><option value='PR'>PR</option><option value='RI'>RI</option><option value='SC'>SC</option><option value='SD'>SD</option><option value='TN'>TN</option><option value='TX'>TX</option><option value='UT'>UT</option><option value='VA'>VA</option><option value='VI'>VI</option><option value='VT'>VT</option><option value='WA'>WA</option><option value='WI'>WI</option><option value='WV'>WV</option><option value='WY'>WY</option> </select></div><div class='col-lg-4 col-md-4 col-sm-6 col-xs-6 no-padding-right margin-B20 form-group'> <label class='registration_inputLabel' for='shipping-zip1'>ZIP Code*</label> <input class='registration_input form-control field_mandatory' id='shipping-zip1' name='shipping-zip1' autocomplete='no' value='"+shippingzip1_edit+"' /></div><div class='clearboth'></div></div><div class='registration_contact padding-B20'>License</div><div class='padding-R15'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-licensename'>Name on License*</label> <input class='registration_input form-group field_mandatory' id='shipping-licensename' name='shipping-licensename' autocomplete='no' value='"+shippinglicensename_edit+"' /></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-licenseno'>State License Number*</label> <input class='registration_input form-group field_mandatory' id='shipping-licenseno' name='shipping-licenseno' autocomplete='no' value='"+shippinglicenseno_edit+"' /></div><div class='clearboth'></div></div><div class='col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding-left margin-B20 shipping_check registration_parent form-group'> <label class='checkbox-button'> <input type='checkbox' class='registration_checkbox form-group' id='shipping-checkbox' name='shipping-checkbox' /> <span class='checkbox-button__control'></span> </label> <label class='registration_checkboxLabel' for='shipping-checkbox'>Same address for license as shipping address</label><div class='clearboth'></div></div><div class='clearboth'></div><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-left margin-B30 registration_address form-group'> <label class='registration_inputLabel' for='shipping-lookup2'>Address Lookup</label> <input class='registration_input form-group' id='shipping-lookup2' name='shipping-lookup2' autocomplete='no' /> <span class='glyphicon glyphicon-search form-control-feedback'></span></div><div class='padding-R15'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-address3'>Address Line 1*</label> <input class='registration_input form-group field_mandatory' id='shipping-address3' name='shipping-address3' autocomplete='no' value='"+shippingaddress3_edit+"' /></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-right margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-address4'>Address Line 2*</label> <input class='registration_input form-group field_mandatory' id='shipping-address4' name='shipping-address4' autocomplete='no' value='"+shippingaddress4_edit+"' /></div><div class='clearboth'></div></div><div class='padding-R15'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding-left margin-B20 registration_parent form-group'> <label class='registration_inputLabel' for='shipping-city2'>City*</label> <input class='registration_input form-group field_mandatory' id='shipping-city2' name='shipping-city2' autocomplete='no' value='"+shippingcity2_edit+"' /></div><div class='col-lg-2 col-md-2 col-sm-6 col-xs-6 no-padding-right margin-B20 registration_mob form-group'> <label class='registration_inputLabel' for='shipping-state2'>State*</label> <select class='registration_state form-group field_mandatory' id='shipping-state2' name='shipping-state2' value='"+shippingstate2_edit+"'><option value='' selected='selected'></option><option value='AL'>AL</option><option value='AK'>AK</option><option value='AR'>AR</option><option value='AZ'>AZ</option><option value='CA'>CA</option><option value='CO'>CO</option><option value='CT'>CT</option><option value='DE'>DE</option><option value='DC'>DC</option><option value='FL'>FL</option><option value='GA'>GA</option><option value='GU'>GU</option><option value='HI'>HI</option><option value='IA'>IA</option><option value='ID'>ID</option><option value='IL'>IL</option><option value='IN'>IN</option><option value='KS'>KS</option><option value='KY'>KY</option><option value='LA'>LA</option><option value='ME'>ME</option><option value='MD'>MD</option><option value='MA'>MA</option><option value='MI'>MI</option><option value='MN'>MN</option><option value='MS'>MS</option><option value='MO'>MO</option><option value='MT'>MT</option><option value='NC'>NC</option><option value='ND'>ND</option><option value='NE'>NE</option><option value='NV'>NV</option><option value='NH'>NH</option><option value='NJ'>NJ</option><option value='NM'>NM</option><option value='NY'>NY</option><option value='OH'>OH</option><option value='OK'>OK</option><option value='OR'>OR</option><option value='PA'>PA</option><option value='PR'>PR</option><option value='RI'>RI</option><option value='SC'>SC</option><option value='SD'>SD</option><option value='TN'>TN</option><option value='TX'>TX</option><option value='UT'>UT</option><option value='VA'>VA</option><option value='VI'>VI</option><option value='VT'>VT</option><option value='WA'>WA</option><option value='WI'>WI</option><option value='WV'>WV</option><option value='WY'>WY</option> </select></div><div class='col-lg-4 col-md-4 col-sm-6 col-xs-6 no-padding-right margin-B20 form-group'> <label class='registration_inputLabel ' for='shipping-zip2'>ZIP Code*</label> <input class='registration_input form-group field_mandatory' id='shipping-zip2' name='shipping-zip2' autocomplete='no' value='"+shippingzip2_edit+"' /></div><div class='col-md-12'><button type='button' class='registration_save shipping_edit_save'>Save</button></div></div>");
		
		$(this).parent().parent().find("#shipping-state1").val(shippingstate1_edit);
		$(this).parent().parent().find("#shipping-state2").val(shippingstate2_edit);
		$(this).parent().parent().find(".registration_checkbox").prop('checked', true);
		$(this).parent().parent().find('.registration_locationLine2').empty();
		$(this).parent().parent().find('.registration_locationparent').empty();
		setTimeout(function(){
		$(".print_shipping .phn-mask").inputmask({"mask": "(999) 999-9999"});
		}, 100);
	});	
	
	$('.print_shipping').on('click', '.shipping_edit_save', function() {
		
		var finalcount = $(this).parent().parent().parent().parent().find(".locationno").text();	
			
			var shippingfirstname_update = $(this).parent().parent().parent().parent().find("#shipping-firstname").val(); 
			var shippinglastname_update = $(this).parent().parent().parent().parent().find('#shipping-lastname').val();
			var shippingemail_update = $(this).parent().parent().parent().parent().find('#shipping-email').val();
			var shippingphno_update = $(this).parent().parent().parent().parent().find('#shipping-phno').val();
			var shippingextension_update = $(this).parent().parent().parent().parent().find('#shipping-extension').val();
			var shippingorgname_update = $(this).parent().parent().parent().parent().find('#shipping-orgname').val();
			var shippingaddress1_update = $(this).parent().parent().parent().parent().find('#shipping-address1').val();
			var shippingaddress2_update = $(this).parent().parent().parent().parent().find('#shipping-address2').val();
			var shippingcity1_update = $(this).parent().parent().parent().parent().find('#shipping-city1').val();
			var shippingstate1_update = $(this).parent().parent().parent().parent().find('#shipping-state1').val();
			var shippingzip1_update = $(this).parent().parent().parent().parent().find('#shipping-zip1').val();
			var shippinglicensename_update = $(this).parent().parent().parent().parent().find('#shipping-licensename').val();
			var shippinglicenseno_update = $(this).parent().parent().parent().parent().find('#shipping-licenseno').val();
			var shippingaddress3_update = $(this).parent().parent().parent().parent().find('#shipping-address3').val();
			var shippingaddress4_update = $(this).parent().parent().parent().parent().find('#shipping-address4').val();
			var shippingcity2_update = $(this).parent().parent().parent().parent().find('#shipping-city2').val();
			var shippingstate2_update = $(this).parent().parent().parent().parent().find('#shipping-state2').val();
			var shippingzip2_update = $(this).parent().parent().parent().parent().find('#shipping-zip2').val();
		
	
		
		
		$(this).parent().parent().parent().parent().find('.registration_locationparent').append("<div class='registration_locationLine1'><div class='registration_locationName'>Location <span class='locationno'>"+finalcount+"</span></div><div class='registration_locationOrg'>"+shippingorgname_update+"</div><div class='clearboth'></div></div><div class='registration_edit ship_del'>Remove <i class='fa fa-trash' aria-hidden='true'></i></div><div class='registration_edit only_edit_ship'>Edit <img src='_ui/responsive/theme-lambda/images/edit.png' width='20' alt='Edit'></div><div class='clearboth'></div>");
		
		$(this).parent().parent().parent().parent().find('.registration_locationLine2').append("<div class='registration_locationcontact'><div class='registration_locationcontactline1'>Contact Info</div><div class='registration_locationcontactname'><span>"+shippingfirstname_update+"</span>&nbsp;<span>"+shippinglastname_update+"</span></div><div class='registration_locationcontactemail'><span>"+shippingemail_update+"</span></div><div class='registration_locationcontactphn'><span>"+shippingphno_update+"</span> ext. <span>"+shippingextension_update+"</span></div></div><div class='registration_locationaddress'><div class='registration_locationcontactline1'>Address</div><div class='registration_locationaddress1'><span>"+shippingaddress1_update+"</span></div><div class='registration_locationaddress2'><span>"+shippingaddress2_update+"</span></div><div class='registration_locationaddress4'><span>"+shippingcity1_update+"</span>, <span>"+shippingstate1_update+"</span> <span>"+shippingzip1_update+"</span></div></div><div class='registration_locationLic'><div class='registration_locationcontactline1'>License</div><div class='registration_locationlic1'><span>"+shippinglicensename_update+"</span> - <span>"+shippinglicenseno_update+"</span></div><div class='registration_locationlic3'><span>"+shippingaddress3_update+"</span></div><div class='registration_locationlic4'><span>"+shippingaddress4_update+"</span></div><div class='registration_locationlic5'><span>"+shippingcity2_update+"</span>, <span>"+shippingstate2_update+"</span> <span>"+shippingzip2_update+"</span></div></div><div class='clearboth'></div>");
		
		
				
		
		$(this).parent().parent().parent().parent().find('.location_edit_container').empty();
		
		
		
		
	});	
	
	$('.print_shipping').on('click', '.edit_ship_del', function() {
		$(this).parent().parent().parent().remove();
		 $('.registration_locationName .locationno').each(function(index) {
           $(this).html(index + 1);
           console.log('count' + index);
           });
		    var shipCount = $('.print_shipping .registration_locations').length;
			if(shipCount < 1){
				$(".shipping-block").show();
				$('.shipping-block .registration_checkbox').prop('checked', false);
				$('#shipping-checkbox').on('change', function () { //listen for the checkbox change event
			setTimeout(function(){
			var i = parseInt($('#shipping-state1').get(0).selectedIndex); //find selectedIndex of dropdown
			if ($("#shipping-address3, #shipping-address4, #shipping-city2, #shipping-zip2, #shipping-state2").val()== '') {
            $('#shipping-address4').val($('#shipping-address1').val()); //copy values
            $('#shipping-address3').val($('#shipping-address2').val());
            $('#shipping-city2').val($('#shipping-city1').val());
            $('#shipping-zip2').val($('#shipping-zip1').val());
            $('#shipping-state2').prop('selectedIndex', i); //copy whatever the selectedIndex value is
			$(".registration_shipping .registration_addbutton").removeClass("registration_addbutton_disable");
            
			} else {
            $('#shipping-address3').val('');
            $('#shipping-address4').val('');
            $('#shipping-city2').val('');
            $('#shipping-zip2').val('');
            $('#shipping-state2').prop('selectedIndex', 0);
			$(".registration_shipping .registration_addbutton").addClass("registration_addbutton_disable");
            
			}
			}, 100);
		    });
			}
			else{
				$(".add_ship_del").hide();
				
			}
		
	});
	
	$('.print_shipping').on('change', '#shipping-checkbox', function() {
	
			setTimeout(function(){
			var i = parseInt($('#shipping-state1').get(0).selectedIndex); //find selectedIndex of dropdown
			if ($("#shipping-address3, #shipping-address4, #shipping-city2, #shipping-zip2, #shipping-state2").val()== '') {
            $('#shipping-address3').val($('#shipping-address1').val()); //copy values
            $('#shipping-address4').val($('#shipping-address2').val());
            $('#shipping-city2').val($('#shipping-city1').val());
            $('#shipping-zip2').val($('#shipping-zip1').val());
            $('#shipping-state2').prop('selectedIndex', i); //copy whatever the selectedIndex value is
			$(".registration_shipping .registration_addbutton").removeClass("registration_addbutton_disable");
            
			} else {
            $('#shipping-address3').val('');
            $('#shipping-address4').val('');
            $('#shipping-city2').val('');
            $('#shipping-zip2').val('');
            $('#shipping-state2').prop('selectedIndex', 0);
			$(".registration_shipping .registration_addbutton").addClass("registration_addbutton_disable");
            
			}
			}, 100);
		    });
	
	/* Add shipping location ends here */
	
       