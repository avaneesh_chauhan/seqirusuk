$(function() {
    evenHeight('.factor-block');
    evenHeight('.resources-card-container');

    $('.slick-wrap').slick({
        dots: true,
        arrows: false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 1023,
                settings: "unslick"
            }
        ]
    });

    $('.carousel--three-card').slick({
        dots: true,
        arrows: false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 1023,
                settings: "unslick"
            }
        ]
    });
});

function evenHeight(selector) {
    let maxH = 0;
    let test = 0;
    $(selector).each(function(idx) {
        test = jQuery(this).innerHeight();
        if (maxH < test) {
            maxH = test;
        }
    });
    $(selector).css('height', maxH);
}