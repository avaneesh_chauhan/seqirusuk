$(window).on('load',function(){

  
    $('.large--carousel-slider').slick({
        dots:false,
        focusOnSelect: true,
        appendArrows: '.large--carousel-btn-container',
        prevArrow:'.prev-btn',
        nextArrow:'.next-btn'
    });


    $('.large--carousel-slider-mobile').slick({
        dots:true,
        focusOnSelect: true,
        appendArrows: '.large--carousel-mobile-btn-container',
        prevArrow:'.prev-btn-mobile',
        nextArrow:'.next-btn-mobile'
    });

  
    var tabBtnContainer = $('.tabs--container-nav');
    var tabBtns = $('.tabs--container-btn');
    var tabsContent = $('.tabs--container-content');
    var tabsContainerHolder = $('.tabs--container--holder');
    var map = {};
    selectTab(0);

    tabBtns.on('click', function () {
        num = tabBtns.index(this);
        selectTab(num);
   
    })

    function selectTab(num) {

        tabBtnContainer.children().each(function (index, element) {
            if (index == num) {
                $(this).css({ 'background': '#ffffff' ,'border-bottom-color':'#ffffff'});
            //    alert(num +' - num')
            $("p",this).css({'border-bottom':'3px solid #EA181B'});
            } else {
                $(this).css({ 'background': '#F9FAFA','border-bottom-color':'#AEB7BD' });
             
                $("p",this).css({'border-bottom':'none'});
            }
        })

        tabsContainerHolder.children().each(function (index, element) {

            if (index == num) {
                $(this).show();
                $(this).css('background', '#ffffff');
                // var test = $(this);
                $("p",this).css({'border-bottom':'3px solid #EA181B'});
            } else {
                $(this).hide();
                $(this).css('background', '#F9FAFA');
                $("p",this).css({'border-bottom':'none'});
            }
        })
    }


   
 
var acc = document.getElementsByClassName("accordion");
var i;
var myArr =[];
for (i = 0; i < acc.length; i++) {
    myArr.push(acc[i].id);
  acc[i].addEventListener("click", function(evt) {
    // var index = acc.indexOf(this.target);
   
    // this.style.background="yellow";
   
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    var currClassList = evt.target.classList;
    var test = evt.target.classList
    var arr = evt.target.id
    var index = myArr.indexOf(evt.target.id)

   
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
      
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
   
      
    } 

    
  });
}

var settings =  {
    dots:true,arrows:false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 1023,
                settings: "unslick"
            }
        ]
}

$('.carousel--three-card').not('.slick-initialized').slick(settings)  
$(window).on('resize', function() {
   if ($(window).width() < 1024) {
    
   
    $('.carousel--three-card').not('.slick-initialized').slick(settings)  
   } 


});



$("#billing-accordion").accordionjs({activeIndex:false});
//  alert($('.accordion-footnote-container').children().length)

   
   });