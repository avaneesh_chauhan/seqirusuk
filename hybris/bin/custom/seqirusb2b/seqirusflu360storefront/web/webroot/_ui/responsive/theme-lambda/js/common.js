$(document).ready(function () {
	$(document).on('click', '.mega-dropdown', function(e) {
	  e.stopPropagation()
	});
	$("#home_closebtn").click(function(){
		$("#home_floatingbanner").hide();
	});
	$(".has-megamenu").on("mouseover",function(){
		var divWidth = ($(this).width())/2;
		var leftPos = $(this).position().left;
		$(".indicator_arrow").show().css("left",(leftPos+divWidth+20)+"px");
	});
	$(".has-megamenu").on("mouseout",function(){
		$(".indicator_arrow").hide();
	});
	if ($(window).width() < 992)
	{
		$(".global_mobilecarousel").addClass("carousel-inner");
		$(".carousel").addClass("multi-item-carousel");
		$('.multi-item-carousel .item').each(function(){
			var next = $(this).next();
			if (!next.length) next = $(this).siblings(':first');
			next.children(':first-child').clone().appendTo($(this));
		});
		$('.multi-item-carousel .item').each(function(){
			var prev = $(this).prev();
			if (!prev.length) prev = $(this).siblings(':last');
			prev.children(':nth-last-child(2)').clone().prependTo($(this));
		});
		
	}
	else{
		$(".global_mobilecarousel").removeClass("carousel-inner");
		$(".carousel").removeClass("multi-item-carousel");
	}
	
	
	
	$(".mob_nav_click").click(function(){
	 $(this).toggleClass("fa-bars fa-times"); 	
     $(".mob_exp_menu").slideToggle();
    });

	
});

/* Dashboard menu expand and collapse  Starts */
$(function(){
	$('.collapse').on('hide.bs.collapse',function(){
		$(this).parent().find('.fa-angle-up').removeClass('.fas fa-angle-up').addClass('.fas fa-angle-down');
	});
	$('.collapse').on('show.bs.collapse',function(){
		$(this).parent().find('.fa-angle-down').removeClass('.fas fa-angle-down').addClass('.fas fa-angle-up');
	});
});   
/* Dashboard menu expand and collapse  ends */ 