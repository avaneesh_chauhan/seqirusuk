$(function(){
    // $('.carousel--three-card-mobile').slick({dots:true,arrows:false,
    //     mobileFirst: true,
    //     responsive: [
    //         {
    //             breakpoint: 1023,
    //             settings: "unslick"
    //         }
    //     ]
        
    // })

    var settings =  {
        dots:true,arrows:false,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 1023,
                    settings: "unslick"
                }
            ]
    }
    
    $('.carousel--three-card-mobile').not('.slick-initialized').slick(settings)  
    $(window).on('resize', function() {
       if ($(window).width() < 1024) {
        
       
        $('.carousel--three-card-mobile').not('.slick-initialized').slick(settings)  
       } 
    
    
    });
    
})