$(window).on('load',function(){

    $('.large--carousel-slider').slick({
        dots:false,
        focusOnSelect: true,
        appendArrows: '.large--carousel-btn-container',
        prevArrow:'.prev-btn',
        nextArrow:'.next-btn'
    });


    $('.large--carousel-slider-mobile').slick({
        dots:true,
        // customPaging: function(slider,i){
        //     return '<div class="my-slickdots"></div>'
        // },
        // appendDots:'.slick-slider-dots',
        focusOnSelect: true,
        appendArrows: '.large--carousel-mobile-btn-container',
        prevArrow:'.prev-btn-mobile',
        nextArrow:'.next-btn-mobile'
    });

    // $('.card-carousel-mobile').slick({dots:true,arrows:false,focusOnSelect: true});

    // $('.carousel--three-card').slick({dots:true,arrows:false,
    //     mobileFirst: true,
    //     responsive: [
    //         {
    //             breakpoint: 1023,
    //             settings: "unslick"
    //         }
    //     ]
        
    // })

    var settings =  {
        dots:true,arrows:false,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 1023,
                    settings: "unslick"
                }
            ]
    }
    
    $('.carousel--three-card').not('.slick-initialized').slick(settings)  
    $(window).on('resize', function() {
       if ($(window).width() < 1024) {
        
       
        $('.carousel--three-card').not('.slick-initialized').slick(settings)  
       } 
    
    
    });
    
   
  
   
   });