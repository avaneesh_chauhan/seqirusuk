$(document).ready(function () {
	/* graph code start  here*/
	var deliver = $("#delivered").text();
	var intrantsit = $("#inTransit").text();
	var process = $("#processing").text();	
	var totalShipped = $('#totalShippedQty').text();
	var percDelivered = ((deliver/totalShipped) * 100).toFixed(0);
	var percInTransit = ((intrantsit/totalShipped) * 100).toFixed(0);
	var percDelivered = ((process/totalShipped) * 100).toFixed(0);	
	
	Highcharts.chart('ordergraphcontainer', {
		title: {
			text: ''
		},
		chart: {
			backgroundColor: 'transparent',
		},
		showInLegend: true,
		colors: ["#AEB7BD ", "#3E464B ", "#CFD8E1 "],
		credits: {
			enabled: false
		},
		legend: {
			enabled: true,
			layout: 'vertical',
			backgroundColor: 'transparent',
			padding: 5,
			itemMarginTop: 10,
			itemMarginBottom: 10,
			useHTML: true,
			labelFormatter: function () {
				return '<div id="orderslabelname">' + this.name + '&nbsp;</div><div id="orderpercnetage">' + this.y + '%</div>';
			},
			//width: 400
		},
		series: [{
			showInLegend: true,
			type: 'pie',
			center: ['50%', '50%'],
			borderColor: '#AEB7BD',
			enableMouseTracking: false,
			innerSize: '80%',
			dataLabels: {
				enabled: false,
			},
			data: [{
				y: parseInt(deliver),
				borderColor: '#AEB7BD',
				name: 'Delivered',
			}, {
				y: parseInt(intrantsit),
				borderColor: '#AEB7BD',
				name: 'In Transit',
			}, {
				y: parseInt(process),
				name: 'Processing',
				color: {
					patternIndex: 1,
					borderColor: '#AEB7BD',
				}
				/* color: {
					pattern: {
						image: '../images/line.jpg',
						aspectRatio: 9 / 4
					}
            }*/
			}]
		}]
	});
	/* graph code end  here*/
	/* shipment datatabel code start here*/
	var shipTable = $('#orderlandingshipmenttabel').dataTable({
		"ajax": {
			"url": ACC.config.contextPath + "/orders/getChartandTableData",
			"type": "GET",
			"dataSrc": "tableData"
			
		},
		"columns": [{
			"data": "orders"
		}, {
			"data": "qty"
		}, {
			"data": "location"
		}, {
			"data": "viewdetails",
			className: "never",
			"fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
				$(nTd).html('<a href="#" target="_blank" >View order <i class="global_blackarrow"></i></a>');
			}
		}],
		"autoWidth": false,
		"searching": false,
		"ordering": false,
		"paging": false,
		"pageLength":4,
		"info": false,
		 aoColumnDefs: [ { "sClass": "orders_shipmentcol", "aTargets": [ 2 ] } ],
         responsive: true
	});
	$('#orderlandingshipmenttabel').removeClass("no-footer");
	/* shipment datatabel code end here*/
	
	  moment().format();


    
    $("#orderlanding_calendar").fullCalendar({
        header: {
            left: 'prev ',
            center: 'title',
            right: 'next'
        },
        
        defaultDate: moment(),
		dayOfMonthFormat: 'dd DD/MM',
		 columnFormat: {
            month: 'dd', // Monday, Wednesday, etc
        },
	    events: [{
            title: '<span class="orders_eventtitle">Shipment Sent</span>',
            start: '2021-04-05',
			className: "orders_cal-delivered",
		    description: '<div class="card" ><div class="card-body"> <h5 class="card-title orders_popupdelverydate">October 05, 2021</h5> <h6 class="card-subtitle mb-2 text-muted"><a href="#" class="card-link orders_popviewtracking">View tracking <i class="global_blackarrow"></i></a></h6> <div class="orders_popupordersinfo"><p class="orders_popuporderinfotitle">Order #5463536346246</p> <p class="orders_popupsubtitle">123 Main St, Atlanta GA 12313</p></div> <div class="orders_popupordersprodinfo"><p class="orders_popuporderinfotitle">500 units FLUAD® QUADRIVALENT</p> <p class="orders_popupsubtitle">0.5-mL pre-filled syringe</p><p class="orders_popuporderinfotitle">500 units FLUCELVAX® QUADRIVALENT</p> <p class="orders_popupsubtitle">0.5-mL pre-filled syringe</p><p class="orders_popuporderinfotitle">500 units AFLURIA® QUADRIVALENT</p> <p class="orders_popupsubtitle">5-mL multi-dose vial</p></div> </div></div>'
        },{
            title: '<span class="orders_eventtitle">Shipment Sent</span>',
            start: '2021-04-10',
			className: "orders_cal-delivered",
			description: '<div class="card" ><div class="card-body"> <h5 class="card-title orders_popupdelverydate">October 10, 2021</h5> <h6 class="card-subtitle mb-2 text-muted"><a href="#" class="card-link orders_popviewtracking">View tracking <i class="global_blackarrow"></i></a></h6> <div class="orders_popupordersinfo"><p class="orders_popuporderinfotitle">Order #5463536346246</p> <p class="orders_popupsubtitle">123 Main St, Atlanta GA 12313</p></div> <div class="orders_popupordersprodinfo"><p class="orders_popuporderinfotitle">500 units FLUAD® QUADRIVALENT</p> <p class="orders_popupsubtitle">0.5-mL pre-filled syringe</p><p class="orders_popuporderinfotitle">500 units FLUCELVAX® QUADRIVALENT</p> <p class="orders_popupsubtitle">0.5-mL pre-filled syringe</p><p class="orders_popuporderinfotitle">500 units AFLURIA® QUADRIVALENT</p> <p class="orders_popupsubtitle">5-mL multi-dose vial</p></div> </div></div>'
		
        },{
           // title: 'Conference',
            start: '2021-04-22',
            end: '2021-04-27',
	    }, {
           // title: 'Click for Google',
              start: '2021-05-11',
			  end: '2021-05-15',
        }],
		eventRender: function (event, element) {
			var eventStart = moment(event.start);
			var eventEnd = event._end === null ? eventStart : moment(event.end);
			var diffInDays = eventEnd.diff(eventStart, 'days');
			// var totaldays= diffInDays + 1;
			//console.log('>>>>>>>>>>>>difdate:'+diffInDays+'>>>>>>>>>>>>>>>>Startdate'+eventStart+'>>>>>> enddate'+ eventStart+'>>>>>>>>>total'+totaldays);
		    for(var i = 0; i <= diffInDays; i++) {
				if (i == 0){
					$("td[data-date='" + eventStart.format('YYYY-MM-DD') + "']").css('border-radius','25px 0px 0px 25px ');
					$("td[data-date='" + eventStart.format('YYYY-MM-DD') + "']").css('background-color','#5D6F7B');
					$("td[data-date='" + eventStart.format('YYYY-MM-DD') + "']").css('color','#fff');
				} 
				else {
					$("td[data-date='" + eventStart.format('YYYY-MM-DD') + "']").css('background-color','#5D6F7B');
				    $("td[data-date='" + eventStart.format('YYYY-MM-DD') + "']").css('color','#fff');
				}
				$("td[data-date='" + eventEnd.format('YYYY-MM-DD') + "']").css('border-radius','0px 25px 25px 0px ');
				eventStart.add(1,'day');
			  
				
			}
			 $(element).popover({
                title: function () {
                    return event.title  ;
                },
                placement: 'bottom',
                html: true,
                container: 'body',
                /* optional */
                trigger: 'click',
                animation: 'true',
                content: function () {
                    return  event.description  ;
                },
                container: 'body'
            }).click(function (e) {
                $(this).addClass("fc-event-select");
				$(this).toggleClass("orders_cal-delivered-click");
                $(this).removeClass("fc-event-default fc-normal");
                $('.fc-event').not(this).addClass("fc-event-default");
                $('.fc-event').not(this).removeClass("fc-event-select");
                //$('.popover').not(this).hide(); /* optional, hide other popovers */
                $(this).popover('show'); /* show popover now it's setup */
                // e.preventDefault();
				if ($(window).width() < 767){
                   
				}
                
                
           
            });
		}

		
   
     });
	 $('body').on('click', '.fc-prev-button', function() {
		 $('.popover').hide();
   	});

	$('body').on('click', '.fc-next-button', function() {
	   $('.popover').hide();
   
	});
	  $(document).on('click', function (e) {
        $('[data-toggle="popover"],[data-original-title]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).addClass("fc-normal");
                $(this).removeClass("fc-event-select");
				$(this).removeClass("orders_cal-delivered-click");
                (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false // fix for BS 3.3.6
            }
        });
    });
});
