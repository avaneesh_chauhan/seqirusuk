$(document).ready(function () {
    /* Cart Landing JS */
    var locationcount;
    $(".cart_neworderbtn").on("click", function () {
        var cartnum = $(this).attr("id").split("-")[1];
        $(".cart_neworderbtn").removeClass("cart_active");
        $("#cart_neworderbtn-" + cartnum).addClass("cart_active");
        $(".cart_selectlocations").hide();
        $("#cart_selectlocation-" + cartnum).show();

        $('#cart_landingTable-2').DataTable().clear();
        $('#cart_landingTable-2').DataTable().destroy();
        $('#cart_landingTable-1').DataTable().clear();
        $('#cart_landingTable-1').DataTable().destroy();
        locationcount = 0;
        createcart(cartnum);
        $("#cart_section").hide();
        $(".back_txt a").click(function () {
            $(".cart_selectlocations").hide();
            $("#cart_selectlocation-" + cartnum).show();
            $("#cart_section").hide();
        });
    });

    $("#cart_neworderselect").on("change", function () {
        var selectnum = $("#cart_neworderselect option:selected").attr("id").split("-")[1];
        $(".cart_selectlocations").hide();
        $("#cart_selectlocation-" + selectnum).show();
        $('#cart_landingTable-2').DataTable().clear();
        $('#cart_landingTable-2').DataTable().destroy();
        $('#cart_landingTable-1').DataTable().clear();
        $('#cart_landingTable-1').DataTable().destroy();
        locationcount = 0;
        createcart(selectnum);
        $("#cart_section").hide();
        $(".back_txt a").click(function () {
            $(".cart_selectlocations").hide();
            $("#cart_selectlocation-" + selectnum).show();
            $("#cart_section").hide();
        });
    });

    function createcart(createdcart)
    {

        var test = $('#cart_landingTable-' + createdcart).DataTable({
            "ajax": {
            "url": ACC.config.contextPath + "/shipLocationData",
            "type": "GET",
             "dataSrc": "data",    
            },
            
      //  var test = $('#cart_landingTable-' + createdcart).DataTable({
        //    "ajax": {
          //      "url": "_ui/responsive/theme-lambda/json/dataGrid" + createdcart + ".json",
            //    "type": "GET"
            //},
            "columns": [
                {"data": "locname"},
                {"data": "address"},
                {"data": "state"},
                {"data": "checked"},
                {"data": "locID"}
            ],
            "searching": false,
            "paging": false,
            "bSort": false,
            "columnDefs": [],
            "scrollY": "360px",
            "scrollCollapse": true,
            "lengthChange": false,
            "info": false,
            "autoWidth": false,
            "destroy": true,
            "initComplete": function (settings, json) {
                
                $('#cart_selectlocation-'+createdcart+' button').click(function() {
					var value = $('#cart_selectlocation-'+createdcart+' input').val().toLowerCase();
					$("#cart_landingTable-"+createdcart+" tr").filter(function() {
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
					});
			  	});

                // var table = $('#cart_landingTable-'+createdcart).DataTable();
                
                
                $('#cart_landingTable-' + createdcart + ' tbody tr').on('click', function () {
                   var selectedloc ;
                    $(this).toggleClass('selected');
                    locationcount = test.rows('#cart_selectlocation-' + createdcart + ' .selected').data().length;

                    $("#cart_selectlocation-" + createdcart + " .cart_loccount").empty().append(locationcount);
                    if ($(this).hasClass("selected"))
                    {
                        $(this).find("td:nth-of-type(4)").addClass("selectedtick");
                        
                       selectedloc =   $(this).find("td:nth-of-type(5)").text();
                
                       $(".locationrow"+selectedloc).addClass("show");
                        $(".locationrow"+selectedloc).removeClass("hie");
                       
                    } else
                    {
                        $(this).find("td:nth-of-type(4)").removeClass("selectedtick");
                        
                         selectedloc =   $(this).find("td:nth-of-type(5)").text();
                       
                       $(".locationrow"+selectedloc).addClass("hide");
                        $(".locationrow"+selectedloc).removeClass("show");

                    }
                   
                    if (locationcount > 0)
                    {
                        $("#cart_selectlocation-" + createdcart + " .cart_startorderbtn").prop("disabled", false);
                    } else
                    {
                        $("#cart_selectlocation-" + createdcart + " .cart_startorderbtn").prop("disabled", "disabled");
                    }
                  
                });

                $("#cart_selectlocation-" + createdcart + " #cart_selectall").on("click", function () {
                    $('#cart_landingTable-' + createdcart + ' tbody tr').filter(':visible').addClass("selected");
                    /*console.log($('#cart_landingTable tbody tr').filter(function() {
                     return $(this).css('display') !== 'none';
                     }).length);*/
                    $("#cart_landingTable-" + createdcart + " tbody tr td:nth-of-type(4)").filter(':visible').addClass("selectedtick");
                    locationcount = test.rows('#cart_selectlocation-' + createdcart + ' .selected').data().length;
                    $("#cart_selectlocation-" + createdcart + " .cart_loccount").empty().append(locationcount);
                    $("#cart_selectlocation-" + createdcart + " .cart_startorderbtn").prop("disabled", false);
                });

                $("#cart_selectlocation-" + createdcart + " #cart_clearall").on("click", function () {
                    $('#cart_landingTable-' + createdcart + ' tbody tr').filter(':visible').removeClass("selected");
                    $("#cart_landingTable-" + createdcart + " tbody tr td:nth-of-type(4)").filter(':visible').removeClass("selectedtick");
                    locationcount = test.rows('#cart_selectlocation-' + createdcart + ' .selected').data().length;
                    $("#cart_selectlocation-" + createdcart + " .cart_loccount").empty().append(locationcount);
                    if (locationcount == 0)
                    {
                        $("#cart_selectlocation-" + createdcart + " .cart_startorderbtn").prop("disabled", "disabled");
                    } else
                    {
                        $("#cart_selectlocation-" + createdcart + " .cart_startorderbtn").prop("disabled", false);
                    }

                });
                $("#cart_selectlocation-" + createdcart + " .cart_loccount").empty().append(0);
                $("#cart_selectlocation-" + createdcart + " #cart_locations").val($("#cart_selectlocation-" + createdcart + " .cart_locations option:first").val());
                $("#cart_selectlocation-" + createdcart + " .cart_startorderbtn").prop("disabled", "disabled");
            }
        });

    }
    /* Cart Landing JS end*/

    /* Add Cart JS Starts here*/
    $(".cart_startorderbtn").click(function () {
        $(".cart_selectlocations").hide();
        $("#cart_section").show();
    });
	$(".add_product_btn").click(function(){
	
		$(this).toggleClass("border-add");
		$('.cart_list_wrap').toggle();
	});
   $('.add_pro_cart').on('click', function () {
		$('#' + $(this).data('product-id')).show();
		$(this).parent().parent().hide();
		if($('.del_section .fa').parent().hasClass('border_del_select')){
			autoincrementHeightAccordin();
		}
		else{
			autoincrementHeight();
		}
		
	});
	
	$('.del_section .fa').on('click', function () {
	
		$(this).parent().parent().hide();
		$('#' + $(this).data('button-id')).show();
		
			
			
			
		
		if($(this).parent().hasClass('border_del_select')){
			autodecrementHeightAccordin();
			$(this).parent().parent().find('.pro_calc_wrapper').hide();
			$(this).parent().parent().find('.left-pro-list').removeClass("border_cart_select");
			$(this).parent().removeClass('border_del_select');
			$(this).parent().parent().find('.show_pro_details').removeClass("fa-angle-up").addClass("fa-angle-down");
			$(this).parent().parent().find(".count_qty").text("0");
		    $(this).parent().parent().find(".total_amnt").text("0");
			$(this).parent().parent().find(".qty-messure").val("");
		}
		else{
		     $(this).parent().parent().find(".count_qty").text("0");
		    $(this).parent().parent().find(".total_amnt").text("0");
			$(this).parent().parent().find(".qty-messure").val("");
	
			autodecrementHeight();
		}
		
		setTimeout(function () {
			if($('.qty-messure').val() == ''){
				
				$('.btn-confirm').removeClass('rvw_btn_enable');
				$('.btn-confirm').addClass('rvw_btn');	
				
			}
			else{
				$('.btn-confirm').addClass('rvw_btn_enable');
				$('.btn-confirm').removeClass('rvw_btn');	
			}
			}, 1000);
	});
	
	   
	$('.show_pro_details').on('click', function () {
		$(this).toggleClass("fa-angle-up fa-angle-down");
		
		$(this).parent().parent().parent().find('.pro_calc_wrapper').slideToggle("fast", function(){
			
        });
		
		$(this).parent().parent().parent().find('.del_section').toggleClass("border_del_select");
		$(this).parent().parent().toggleClass("border_cart_select");
		 var el = this;
		if ($(window).width() < 1025) {
        
        return (el.t = !el.t) ? mobautoincrementHeightAccordin(el) : mobautodecrementHeightAccordin(el);
        }
        else {
        
        return (el.t = !el.t) ? autoincrementHeightAccordin(el) : autodecrementHeightAccordin(el);
        }
		
		
	   
	   
	});	
	
	
	
	$('.qty-messure').keyup(function () {
		var sum = 0;
		var total = 0;
		var val= 0;
		$(this).parent().parent().parent().find('.qty-messure').each(function() {
        val = $(this).val();
        sum += Number($(this).val());
		total = parseFloat(21.54 * sum,10).toFixed(2);
		});
		$(this).parent().parent().parent().parent().find(".count_qty").text(sum);
		$(this).parent().parent().parent().find(".total_amnt").text(total);
		
		$(this).parent().parent().parent().parent().parent().find('.count_qty').each(function() {
			if($('.count_qty').text() > 0){
				$('.btn-confirm').addClass('rvw_btn_enable');
				$('.btn-confirm').removeClass('rvw_btn');	
			}
			else{
				$('.btn-confirm').removeClass('rvw_btn_enable');
				$('.btn-confirm').addClass('rvw_btn');		
			}
		});
			
	});
	
	
	
	
	$(".qty-messure").keypress(function (e) {
		if (e.which != 8 && e.which != 0  && (e.which < 48 || e.which > 57)) {
            return false;
		}
	});
	
	function autoincrementHeight() {
    var el = document.getElementById("cart-wrapper-main");
    var height = el.offsetHeight;
    var newHeight = height + 100;
    el.style.height = newHeight + 'px';
	}
	
	function autodecrementHeight() {
    var el = document.getElementById("cart-wrapper-main");
    var height = el.offsetHeight;
    var newHeight = height - 100;
    el.style.height = newHeight + 'px';
	}
	
	function autoincrementHeightAccordin(el) {
    var el = document.getElementById("cart-wrapper-main");
    var height = el.offsetHeight;
    var newHeight = height + 400;
    el.style.height = newHeight + 'px';
	}
	
	function autodecrementHeightAccordin(el) {
    var el = document.getElementById("cart-wrapper-main");
    var height = el.offsetHeight;
    var newHeight = height - 400;
    el.style.height = newHeight + 'px';
	}
	
	function mobautoincrementHeightAccordin(el) {
    var el = document.getElementById("cart-wrapper-main");
    var height = el.offsetHeight;
    var newHeight = height + 700;
    el.style.height = newHeight + 'px';
	}
	
	function mobautodecrementHeightAccordin(el) {
    var el = document.getElementById("cart-wrapper-main");
    var height = el.offsetHeight;
    var newHeight = height - 700;
    el.style.height = newHeight + 'px';
	}

    /* Add Cart JS Ends here*/

    /* Cart Summary JS */

    $(".rvw_btn").on("click", function () {
     
      var str = $("#reviewform").serialize();
       var status = "failed"
      $.ajax({
    type:"post",
    data:str,
    url:ACC.config.contextPath+ "/addtoCart",
    async: false,
    dataType: "json",
    success: function(data){
      status  = "success";
    },
                error: function() { 
                    alert("error"); 
                }
         });
      if(status == "success") {
        $("#cart_section").hide();
        $("#cart_summaryLocation").show();
        $(".cart_neworderreorder").css("visibility","hidden");
        topFunction();

         $.getJSON("_ui/responsive/theme-lambda/json/productTable.json", function (productJson) {
                        for (i = 0; i < productJson.products.length; i++) {
                var tableHeaders = '';
                var productname = productJson.products[i].product;
                var dose = productJson.products[i].dose;
                var th = "<th>" + productname + "<br><span>" + dose + "</th>";
                tableHeaders += th;
                $("#cart_summaryProductTable thead tr").append(tableHeaders);
            }
            for (i = 0; i < productJson.locations.length; i++) {
                var tRow = "<tr>";
                $.each(productJson.locations[i], function (key, value) {
                    if (key != "locationProducts") {
                        var tData = "<td>" + value + "</td>";
                    }else{
                        var tData = "";
                        var pCode="";
                        for (k = 0; k < productJson.products.length; k++) {
                         pCode = productJson.products[k].productCode;
                        for (j = 0; j < productJson.locations[i].locationProducts.length; j++) {                            
                          if(pCode == productJson.locations[i].locationProducts[j].locID ){
                            var qntyData = "<td>" + productJson.locations[i].locationProducts[j].locQty + "</td>";
                            tData += qntyData;
                            }else{
                            
                            }
                        }
                        }
                    }
                    tRow += tData;
                });

                tRow += "</tr>";
                $("#cart_summaryProductTable tbody").append(tRow);
            }


//            $('#cart_summaryProductTable').DataTable({
//                "searching": false,
//                "paging": false,
//                "bSort": false,
//                "destroy": true,
//                "info": false
//            });
            $('#cart_summarybillingaddressline1').append(productJson.billing.Addressline1);
            $('#cart_summarybillingaddressline2').append(productJson.billing.Addressline2);
            $('#cart_summarypayeraddressline1').append(productJson.paying.Addressline1);
            $('#cart_summarypayeraddressline2').append(productJson.paying.Addressline2);

            $('.cart_summarysubTotalno').append(productJson.cost.Subtotal);
            $('.cart_summaryexciseno').append(productJson.cost.FederalTax);
            $('.cart_summarytotalno').append(productJson.cost.TotalCost);
        });

        $.getJSON("json/productTablemob.json", function (productJsonmob) {
            for (i = 0; i < productJsonmob.data.length; i++) {
                var divRow = "<div class='cart_summaryProductname'>";
                $.each(productJsonmob.data[i], function (key, value) {
                    var divData = "<div>" + value + "</div>";
                    divRow += divData;
                });
                divRow += "<div class=''><span class='glyphicon glyphicon-menu-down'></span></div></div>";

                $(".cart_summaryProductTableMob").append(divRow);
            }
            var quantitySpan = "<span>QTY: </span>"
            $(".cart_summaryProductname div:nth-child(3)").prepend(quantitySpan);
        });
        
        }

    });
    $(".cart_summaryProductselection").on("click", function () {
        resetData();
        $("#cart_section").show();
        $("#cart_summaryLocation").hide();
    });


    $(".cart_summarycheckedbox").on("click", function () {
        if ($('.cart_summarycheckedbox:checked').length == $(".cart_summarycheckedbox").length)
        {
            $(".cart_summaryPlaceorder").removeClass("inactive").addClass("active");
        } else
        {
            $(".cart_summaryPlaceorder").removeClass("active").addClass("inactive");
        }
    });
    $(".cart_summaryPlaceorder").click(function () {
        if ($(this).hasClass("active")) {
            $("#cart_summaryLocation,.cart_neworder").hide();
            $(".cart_thankyousection").show();
            topFunction();
        }
    });
    function resetData() {
        $('.cart_summarypayeraddressline').empty();
        $('.cart_summarybillingaddressline').empty();
        $('.cart_summarySum').empty();
        $("#cart_summaryProductTable thead tr").empty();
        $("#cart_summaryProductTable tbody").empty();
        $(".cart_summaryProductTableMob").empty();
    }
    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
    /* Cart Summary JS end*/
});
