/* Dashboard menu expand and collapse  Starts */
$(function(){
	$('.collapse').on('hide.bs.collapse',function(){
		$(this).parent().find('.fa-angle-up').removeClass('.fas fa-angle-up').addClass('.fas fa-angle-down');
	});
	$('.collapse').on('show.bs.collapse',function(){
		$(this).parent().find('.fa-angle-down').removeClass('.fas fa-angle-down').addClass('.fas fa-angle-up');
	});
});   
/* Dashboard menu expand and collapse  ends */ 