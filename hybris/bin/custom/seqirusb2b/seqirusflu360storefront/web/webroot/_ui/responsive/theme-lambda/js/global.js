// product pages tabs
function tabToAccordion(){
	setTimeout(function () {
		var tabItems = $('.prod-tabs__tab-nav li > a');
		var wrap = $('<div />');
		wrap.addClass('tab-nav-select-wrap');

		var select = $('<select />');
		wrap.append(select);

		tabItems.each(function (i, el) {
			var opt = $('<option />');
			opt.val($(el).attr('href'));
			opt.text($(el).text());
			select.append(opt);
		});
		$('.prod-tabs.tabs-only .wrapper').append(wrap);

		select.on('change', function () {
			var href = $(this).find('option:selected').val();
		});
	}, 250);

	// tabs desktop
	$('body').on('click', '.prod-tabs .prod-tabs__tab-nav li a', function (event) {
		//
		event.preventDefault();
		if ($(window).width() < 960) return;
		var tabsOnly = $(this).parents('.prod-tabs-single').length,
			parentLi = $(this).parent('li'),
			prevActiveLi = $('.prod-tabs .prod-tabs__tab-nav li.active'),
			tab = $($(this).data('tab')),
			prevActiveTab = $('.prod-tabs .prod-tabs__tab.active'),
			allBodies = $('.prod-tabs .prod-tabs__tab .prod-tabs__body');

		setTimeout(function () {
			prevActiveLi.removeClass('active');
			parentLi.addClass('active');
			if (!tabsOnly) {
				prevActiveTab.removeClass('active');
			}
		}, 50);

		setTimeout(function () {
			if (!tabsOnly) {
				tab.addClass('active');
			}
		}, 50);


		setTimeout(function() {
			allBodies.addClass('is-animated');
		}, 250);
	});
	// /tabs desktop

	/* tabs mobile */
	$('body').on('click', '.prod-tabs .prod-tabs__tab .prod-tabs__header a', function (e) {
		var allTabs = $('.prod-tabs .prod-tabs__tab'),
			allPlusMinus = $('.prod-tabs .prod-tabs__tab .plus-minus');

		e.preventDefault();
		allTabs.removeClass('active');
		allPlusMinus.removeClass('is-active');

		var currentTab = $(this).parents('.prod-tabs__tab'),
			plusMinus = $(this).children('.plus-minus');

		currentTab.addClass('active');
		plusMinus.addClass('is-active');

		setTimeout(function() {
			$('html, body').animate({
				scrollTop: $('.prod-tabs .prod-tabs__tab').offset().top - 15
			}, 1000);
		}, 500)
	});
	$('body').on('click', '.prod-tabs .prod-tabs__tab.active .prod-tabs__header a', function (e) {

		var currentTab = $(this).parents('.prod-tabs__tab'),
			plusMinus = $(this).children('.plus-minus');

		currentTab.removeClass('active');
		plusMinus.removeClass('is-active');

		setTimeout(function() {
			$('html, body').animate({
				scrollTop: $('.prod-tabs .prod-tabs__tab').offset().top - 15
			}, 1000);
		}, 500)
	});
	/* /tabs mobile */
}
// end product pages tabs



$(function(){
	// ISI panel thingy
	if ($('#safetyInfo').length > 0){
		var isi = $('#safetyInfo');
		var anchor = $('#safetyInfoAnchor')
		var heightThreshold = anchor.offset().top - window.innerHeight ;

		$(".safety-info__btn").click(function() {
			$('html, body').animate({
			scrollTop: anchor.offset().top
			}, 500);
		});

		$(window).scroll(function() {
			var scroll = $(window).scrollTop();

			if (scroll >= heightThreshold && scroll ) {
				isi.removeClass('is-sticky');
			} else {
				isi.addClass('is-sticky');
			}
		});
	}
	// end ISI panel thingy
	// product pages tabs call
	if ($('.prod-tabs').length > 0){
		tabToAccordion();
	}
	// end product pages tabs call
});