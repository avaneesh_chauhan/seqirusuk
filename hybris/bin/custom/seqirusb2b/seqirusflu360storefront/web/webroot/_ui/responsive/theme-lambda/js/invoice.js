$(document).ready(function(){
	
	var uri = window.location.toString();
    if (window.location.pathname!='/invoicesDetails' && uri.length > 0 && uri.indexOf('/invoices')>=0) {
	/* graph code start  here*/
	var paidinvoice=0;
	var openinvoice=0;
	var totalinvoice =0
	var resultedAmount=0;
	var zeroinvoice1= 1;
	var currentSeason;
	setTimeout(function() {
	    currentSeason=$('#currentSeason').val();
		paidinvoice=parseFloat($('#paidInvAmount').val())
		openinvoice=parseFloat($('#openInvAmount').val())
		totalinvoice = paidinvoice+openinvoice;
		if(totalinvoice=='0'){
			resultedAmount='&#163;'+totalinvoice.toString();
		}else{
			resultedAmount=totalinvoice.toLocaleString('en-GB', {style:'currency', currency:'GBP'})
		}
	
	Highcharts.chart('invoicegraphcontainer', {
	  title: {
		text: ''
	  },
	 // colors: ["#DF323F ", "#35818A ", "#CFD8E1 "],
	  colors: ["#DF323F ", "#35818A ", "#CFD8E1 ", "#fafbec" ],
	  credits: {
		enabled: false
	  },
	  subtitle: {
		text: '<div style="font-size: 24px; font-family: Campton-bold;">Total Invoices<br/>' + currentSeason + '</div><div style="color: #5C6E7C; font-size: 20px; padding: 15px;font-weight: bold;">' + resultedAmount + '</div> ',
		align: "center",
		verticalAlign: "middle",
		style: {
		  "textAlign": "center",
		  "color": "red", 
		  "font-size": "1.5rem" ,  
		  "font-family": "Campton-bold", 
		  "font-weight":"600",
		  "word-break":"break-word"
		},
		x: 0,
		y: -2,
		useHTML: true
	  },
	  series: [{
		type: 'pie',
		center: ['50%', '50%'],
        borderWidth: 5,
        enableMouseTracking: false,
		innerSize: '86%',
		dataLabels: {
		  enabled: false,
		},
		//data: [ parseFloat(paidinvoice), parseFloat(openinvoice)]
		data: [ parseFloat(paidinvoice), parseFloat(openinvoice), parseInt(zeroinvoice1)]
	  }]
	});
	}, 700);
	/* graph code end  here*/	
	
	
	//invoice landing data
	
    var orderTable=$('#invoiceTable').dataTable({
    	"ajax": {
			"url": ACC.config.contextPath + "/getInvoices",
			"type": "GET",
			 "dataSrc": "invoices",
		    },
        "columns": [
          { "data": "salesOrderNumber"},
         // { "data": "invoiceNumber"},
          { "data": "invoiceNumber" , "render": function ( data, type, row, meta ) { if(data=='N/A'){ return data }else{ return '<a class="invoicetbleredtxt" id="'+data+'" href="'+ACC.config.contextPath+'/invoicesDetails?invoiceNumber='+data+'">'+data+'</a>';} }},
         // { "data": "invoiceNumber" , "render": function ( data, type, row, meta ) {if(data=='N/A'){ return data }else{ return  '<a class="invoicetbleredtxt" id="'+data+'" href="#">'+data+'</a>';} }},
          { "data": "amount" , "render": function ( data, type, row, meta ) {if(data=='0'){ return 'N/A' }else{ return data.toLocaleString('en-GB', {style:'currency', currency:'GBP'}) }}},
          { "data": "invoiceDate" },
          { "data": "dueDate" },
          { "data": "status" },
        ],
          "pagingType":"full_numbers",
          "pageLength": 5,
          "lengthChange": false,
          "info":false,
          "autoWidth":false,
          "language": {
          "search": "",
          "paginate": {
            "first": "&#8592;",
            "previous": "<",
            "next": ">",
            "last": "&#8594;",
          }
      },
        "initComplete": function(settings, json) {
        	$("#invoiceTable thead tr th:nth-child(even)").css("background","none");
        }
      });
      orderTable= $("#invoiceTable_filter input").attr("placeholder", "Search by order #, etc");
    }
});
  
$("#invoicedrop").change(function(){
var season=$("#invoicedrop").children("option:selected").val();
	var uri = ACC.config.contextPath+"/invoices?season="+season;
	console.log("hello.."+uri)
	   // if (uri.indexOf("?") > 0) 
	    //{
	        var clean_uri = uri.substring(0, uri.indexOf("?")); 
	        window.history.replaceState({}, document.title, uri);
	        window.location.replace(uri)
	    //}
	
});

/*function getInvoicesList(){
	  $.ajax({
	      url: ACC.config.contextPath+'/invoices1',
	      async:true,
	      data: {"fromDate": "2020-01-10","toDate": "2020-12-17","customerNumber":"0060098765"},
	      type: "GET",
	      error: function(er) 
	      {
	    	  console.log("er.."+er)
	      },
	      success: function(data) 
	      {
		      $('#oldInvoiceLandingReponse').css('display','none');
		      $('#invoiceLandingPanelId').html($(data).filter("#newInvoiceLanding").html());
	      }
	  });
}*/
  
  


