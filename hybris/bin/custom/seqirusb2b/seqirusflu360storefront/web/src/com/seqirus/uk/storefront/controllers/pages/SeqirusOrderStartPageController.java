/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.storefront.controllers.pages;


import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;
import com.seqirus.storefront.controllers.ControllerConstants;
import com.seqirus.storefront.controllers.pages.AccountPageController.ProductWrapperData;
import com.seqirus.storefront.util.SeqirusUtils;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import com.seqirus.core.orders.service.SeqirusOrdersService;
import com.seqirus.flu360.facades.cutomer.data.CustomerRegistrationAddressData;
import org.springframework.web.bind.annotation.ResponseBody;
import com.seqirus.core.dataObjects.ShipdataCartLanding;
import com.seqirus.core.dataObjects.CartLandingShipmentData;
import com.seqirus.flu360.facades.cutomer.data.SeqirusCustomerShippingAddressData;
import org.springframework.beans.factory.annotation.Autowired;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import com.seqirus.facades.orders.SeqirusOrdersFacade;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.user.UserService;


import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.dataObjects.OrderReviewForm;




/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller
public class SeqirusOrderStartPageController extends AbstractPageController
{
	private static final String QUANTITY_ATTR = "quantity";
	private static final String SHIPTO = "shipTo";
	private static final String SOLD_TO = "SoldTo";
	private static final String NO_FORMULATION = "NoFormulation";
	private static final String TYPE_MISMATCH_ERROR_CODE = "typeMismatch";
	private static final String ERROR_MSG_TYPE = "errorMsg";
	private static final String QUANTITY_INVALID_BINDING_MESSAGE_KEY = "basket.error.quantity.invalid.binding";
	private static final String SHOWN_PRODUCT_COUNT = "seqirusflu360storefront.storefront.minicart.shownProductCount";

	private static final Logger LOG = Logger.getLogger(SeqirusOrderStartPageController.class);

	
	
	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;
	
	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "seqirusOrdersFacade")
	private SeqirusOrdersFacade seqirusOrdersFacade;

	@Resource(name = "userService")
	private UserService userService;
	
	@Resource
	private SessionService sessionService;
	
	@Resource(name = "seqirusOrdersService")
	private SeqirusOrdersService seqirusOrdersService;
	
	private static final String PAGETYPE = "pageType";
	
	



	@RequestMapping(value = "/start-order", method ={ RequestMethod.GET, RequestMethod.POST })
	public final String addQuickOrderToCart( final Model model) throws CMSItemNotFoundException
	{
	//	CategoryPageModel categoryPage = null;
		//CategoryModel category = null;
		//category = commerceCategoryService.getCategoryForCode("Seqirus_InSeason");
		//categoryPage = getCategoryPage(category);
		final B2BCustomerModel account = (B2BCustomerModel) userService.getCurrentUser();
		
		CartLandingShipmentData seassiondata = sessionService.getAttribute("data");
		if(null==seassiondata) {
		SeqirusB2BCustomerRegistrationData partnerData = seqirusOrdersFacade.getPartnerDetails(account.getDefaultB2BUnit().getUid(), null);
		
		final SeqirusB2BCustomerRegistrationData partner = seqirusOrdersFacade
				.getPartnerDetails(partnerData.getSoldToId(), SOLD_TO);
		
		CustomerRegistrationAddressData orgAddress = new CustomerRegistrationAddressData();
		String soldtoZip = null;
		List<SeqirusCustomerShippingAddressData> shippingAddresses = new ArrayList<SeqirusCustomerShippingAddressData>();
		
		List<ShipdataCartLanding> shipList = new ArrayList<ShipdataCartLanding>();
		orgAddress = partner.getOrgAddress();
		if (null != orgAddress)
		{	
			
			soldtoZip = orgAddress.getZipCode();
			
			final SeqirusB2BCustomerRegistrationData currentCustomer = seqirusOrdersFacade.getCustomerDetailsFromSAP(account.getDefaultB2BUnit().getUid(),
					soldtoZip);
			
			if(null!=currentCustomer) {
				
				shippingAddresses = currentCustomer.getShippingDetailsList();
				
				if(null!=shippingAddresses) {
					
					sessionService.setAttribute("shippingAddress", shippingAddresses);
					
					for(SeqirusCustomerShippingAddressData address : shippingAddresses) {
						ShipdataCartLanding ship = new ShipdataCartLanding();
						
						ship.setLocname(address.getShippingOrganizationName());
					
						if(null != address.getShippingAddressLine1() && null != address.getShippingAddressLine2())
						{
							ship.setAddress(address.getShippingAddressLine1() +", "+ address.getShippingAddressLine2() );
						}else if(null != address.getShippingAddressLine2())
						{
							ship.setAddress(address.getShippingAddressLine2());
							
						}else if(null != address.getShippingAddressLine1())
						{
							ship.setAddress(address.getShippingAddressLine1());
							
						}
						
						ship.setLocID(address.getAddressId());
						ship.setChecked("");
						ship.setState(address.getTown() + ", " + address.getShippingState()+ "  " + address.getPostalCode());
						shipList.add(ship);
					}
				}
			}
		}
		
		CartLandingShipmentData data = new CartLandingShipmentData();
		data.setData(shipList);
		sessionService.setAttribute("data", data);
		model.addAttribute("shippingData", shipList);
		}
		
		CartLandingShipmentData ship = sessionService.getAttribute("data");
		model.addAttribute("shippingData", ship.getData());
		OrderReviewForm orderReviewForm = new OrderReviewForm();
		model.addAttribute("orderReviewForm", orderReviewForm);
		
		final Collection<VariantOptionData> varientdataPreSeason = new ArrayList<VariantOptionData>();
		final List<VariantOptionData> varientdataSortedPreSeason = new ArrayList<VariantOptionData>();
		final Collection<VariantOptionData> varientdataOrderablePreSeason = new ArrayList<VariantOptionData>();
		final List<VariantOptionData> varientdataOrderableListPreSeason = new ArrayList<VariantOptionData>();

		getProductsForSeason("Seqirus_PreSeason", varientdataPreSeason, varientdataSortedPreSeason, varientdataOrderablePreSeason,
				varientdataOrderableListPreSeason);
		model.addAttribute("products", varientdataOrderableListPreSeason);
		final ContentPageModel cartPage = getContentPageForLabelOrId("cart");
		storeCmsPageInModel(model, cartPage);
		setUpMetaDataForContentPage(model, cartPage);
		model.addAttribute("pageType", PageType.CART.name());
		storeContentPageTitleInModel(model, "order pages");
		return ControllerConstants.Views.Pages.Cart.CartPage;
	}
	
	
	@RequestMapping(value = "/shipLocationData", method ={ RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public final CartLandingShipmentData shipLocationData( final Model model) throws CMSItemNotFoundException
	{
	
		CartLandingShipmentData data = sessionService.getAttribute("data");
		return data;
	}
	
	/**
	 * @param category
	 * @return
	 */
	private void getProductsForSeason(final String season, final Collection<VariantOptionData> varientdata,
			final List<VariantOptionData> varientdataSorted, final Collection<VariantOptionData> varientdataOrderable,
			final List<VariantOptionData> varientdataOrderableList)
	{


		// all orderable products
		final CategoryModel category = commerceCategoryService.getCategoryForCode(season);
		//			final CategoryPageModel categoryPage = getCategoryPage(category);
		final List<ProductOption> options = new ArrayList<>(
				Arrays.asList(ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_MEDIA,
						ProductOption.BASIC, ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
						ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS,
						ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
						ProductOption.PRICE_RANGE, ProductOption.DELIVERY_MODE_AVAILABILITY));


		final List<ProductModel> products = category.getProducts();

		final List<ProductWrapperData> productDataList = new ArrayList<>();
		final List<Map<String, Collection<VariantOptionData>>> productFormulationsList = new ArrayList<Map<String, Collection<VariantOptionData>>>();

		for (final ProductModel productModel : products)
		{
			final ProductWrapperData pwd = new ProductWrapperData();
			ProductData productData = productFacade.getProductForCodeAndOptions(productModel.getCode(), options);
			pwd.setProductFormulations(groupProductsByFormulationOrder(productData));
			pwd.setMinPrice(minProductPrice(productData));
			pwd.setProductData(productData);
			productDataList.add(pwd);
			productFormulationsList.add(groupProductsByFormulationOrder(productData));
			productData = null;
		}



		for (final Map<String, Collection<VariantOptionData>> productFormulations : productFormulationsList)
		{
			for (final Map.Entry<String, Collection<VariantOptionData>> entry : productFormulations.entrySet())
			{

				varientdata.addAll(entry.getValue());
				varientdataSorted.addAll(entry.getValue());

			}

		}

		for (final VariantOptionData varient : varientdata)
		{
			if (varient.getOrderable())
			{
				varientdataOrderable.add(varient);
				varientdataOrderableList.add(varient);
			}
		}

		Collections.sort(varientdataSorted, new Comparator<VariantOptionData>()
		{

			public int compare(final VariantOptionData u1, final VariantOptionData u2)
			{
				if ((null == u1.getSequenceId()) || (null == u2.getSequenceId()))
				{
					return 0;
				}
				else
				{
					return u1.getSequenceId().compareTo(u2.getSequenceId());
				}
			}
		});


	}
	
	private Map<String, Collection<VariantOptionData>> groupProductsByFormulationOrder(final ProductData productData)
	{
		final List<VariantOptionData> variants = productData.getVariantOptions();

		final Map<String, Collection<VariantOptionData>> productsGroupByFormulation = new HashMap<>();

		if (null == variants || variants.isEmpty())
		{
			return productsGroupByFormulation;
		}

		for (final VariantOptionData variantOptionData : variants)
		{

			if (CollectionUtils.isNotEmpty(variantOptionData.getVariantOptionQualifiers()))
			{
				udpateproductAndFormulationMap(productsGroupByFormulation, SeqirusUtils.getFormulationCategories(variantOptionData));

			}
			else if (productsGroupByFormulation.containsKey(NO_FORMULATION))
			{
				productsGroupByFormulation.get(NO_FORMULATION).add(variantOptionData);
			}
			else
			{
				final List<VariantOptionData> vodList = new ArrayList<>();
				vodList.add(variantOptionData);
				productsGroupByFormulation.put(NO_FORMULATION, vodList);
			}
		}

		SeqirusUtils.sortBySequenceId(productsGroupByFormulation);
		final TreeMap<String, Collection<VariantOptionData>> sorted = new TreeMap<>();
		sorted.putAll(productsGroupByFormulation);
		return sorted;
	}
	
	
	private void udpateproductAndFormulationMap(final Map<String, Collection<VariantOptionData>> productsGroupByFormulation,
			final Map<String, Collection<VariantOptionData>> formulationData)
	{

		//Map<BlogKey, Blog> map = new HashMap<BlogKey, Blog>();
		for (final String formulation : formulationData.keySet())
		{
			if (productsGroupByFormulation.containsKey(formulation))
			{
				productsGroupByFormulation.get(formulation).addAll(formulationData.get(formulation));
				SeqirusUtils.removeDuplicate(productsGroupByFormulation.get(formulation));
			}
			else
			{
				productsGroupByFormulation.put(formulation, formulationData.get(formulation));
			}
		}

	}
	
	private BigDecimal minProductPrice(final ProductData productData)
	{
		final List<VariantOptionData> variants = productData.getVariantOptions();
		if (null != variants)
		{
			final List<BigDecimal> allVariantsPrices = new ArrayList<BigDecimal>();
			for (final VariantOptionData variantOptionData : variants)
			{
				if (null != variantOptionData.getPriceData())
				{
					allVariantsPrices.add(variantOptionData.getPriceData().getValue());
				}

			}
			if (allVariantsPrices.isEmpty())
			{
				return BigDecimal.ZERO;
			}
			return Collections.min(allVariantsPrices);
		}
		else
		{
			return BigDecimal.ZERO;
		}
	}
	
	
	public class ProductWrapperData
	{
		private ProductData productData;
		private Map<String, Collection<VariantOptionData>> productFormulations;
		private BigDecimal minPrice;

		/**
		 * @return the productData
		 */
		public ProductData getProductData()
		{
			return productData;
		}

		/**
		 * @param productData
		 *           the productData to set
		 */
		public void setProductData(final ProductData productData)
		{
			this.productData = productData;
		}

		/**
		 * @return the minPrice
		 */
		public BigDecimal getMinPrice()
		{
			return minPrice;
		}

		/**
		 * @param minPrice
		 *           the minPrice to set
		 */
		public void setMinPrice(final BigDecimal minPrice)
		{
			this.minPrice = minPrice;
		}

		/**
		 * @return the productFormulations
		 */
		public Map<String, Collection<VariantOptionData>> getProductFormulations()
		{
			return productFormulations;
		}

		/**
		 * @param productFormulations
		 *           the productFormulations to set
		 */
		public void setProductFormulations(final Map<String, Collection<VariantOptionData>> productFormulations)
		{
			this.productFormulations = productFormulations;
		}

	}
	
	private SeasonEntryModel fetchSeasonEntry()
	{
		SeasonEntryModel entry = null;
		if (null != seqirusOrdersService)
		{
			entry = seqirusOrdersService.getSeasonEntry();
		}
		return entry;
	}
	


	}
