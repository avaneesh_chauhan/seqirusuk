/**
 * 
 */
package com.seqirus.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;

/**
 * @author 172553
 *
 */
public class SeqirusAddressForm extends AddressForm
{
	private String orgName;
	private String emailAddress;
	private String phoneExt;
	/**
	 * @return the orgName
	 */
	public String getOrgName()
	{
		return orgName;
	}
	/**
	 * @param orgName the orgName to set
	 */
	public void setOrgName(String orgName)
	{
		this.orgName = orgName;
	}
	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress()
	{
		return emailAddress;
	}
	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}
	/**
	 * @return the phoneExt
	 */
	public String getPhoneExt()
	{
		return phoneExt;
	}
	/**
	 * @param phoneExt the phoneExt to set
	 */
	public void setPhoneExt(String phoneExt)
	{
		this.phoneExt = phoneExt;
	}
	
	
}
