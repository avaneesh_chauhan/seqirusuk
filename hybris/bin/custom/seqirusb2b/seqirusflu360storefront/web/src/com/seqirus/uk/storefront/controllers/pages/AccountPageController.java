/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorfacades.ordergridform.OrderGridFormFacade;
import de.hybris.platform.acceleratorfacades.product.data.ReadOnlyOrderGridData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateEmailForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePasswordForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.AddressValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.EmailValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.PasswordValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ProfileValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.verification.AddressVerificationResultHandler;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CustomerConsentDataStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.address.AddressVerificationFacade;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commercefacades.user.exceptions.PasswordMismatchException;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.consent.exceptions.CommerceConsentGivenException;
import de.hybris.platform.commerceservices.consent.exceptions.CommerceConsentWithdrawnException;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CountryType;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.dataObjects.OrderSummary;
import com.seqirus.core.dataObjects.ProductDetail;
import com.seqirus.core.orders.service.SeqirusOrdersService;
import com.seqirus.core.orders.service.product.SeqirusProductService;
import com.seqirus.core.registration.service.SeqirusCustomerRegistrationService;
import com.seqirus.facades.constants.Seqirusb2bFacadesConstants;
import com.seqirus.facades.invoice.SeqirusInvoiceFacade;
import com.seqirus.facades.invoice.data.SeqirusInvoiceLandingListResponse;
import com.seqirus.facades.invoice.data.SeqirusInvoiceLandingResponseData;
import com.seqirus.facades.orders.SeqirusOrdersFacade;
import com.seqirus.storefront.controllers.ControllerConstants;
import com.seqirus.storefront.util.SeqirusUtils;




/**
 * Controller for home page
 */
@Controller
@RequestMapping("/my-account")
public class AccountPageController extends AbstractSearchPageController
{
	private static final String TEXT_ACCOUNT_ADDRESS_BOOK = "text.account.addressBook";
	private static final String NO_FORMULATION = "NoFormulation";
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";
	private static final String IS_DEFAULT_ADDRESS_ATTR = "isDefaultAddress";
	private static final String COUNTRY_DATA_ATTR = "countryData";
	private static final String ADDRESS_BOOK_EMPTY_ATTR = "addressBookEmpty";
	private static final String TITLE_DATA_ATTR = "titleData";
	private static final String FORM_GLOBAL_ERROR = "form.global.error";
	private static final String PROFILE_CURRENT_PASSWORD_INVALID = "profile.currentPassword.invalid";
	private static final String TEXT_ACCOUNT_PROFILE = "text.account.profile";
	private static final String ADDRESS_DATA_ATTR = "addressData";
	private static final String ADDRESS_FORM_ATTR = "addressForm";
	private static final String COUNTRY_ATTR = "country";
	private static final String REGIONS_ATTR = "regions";
	private static final String MY_ACCOUNT_ADDRESS_BOOK_URL = "/my-account/address-book";
	private static final String TEXT_ACCOUNT_CONSENT_MANAGEMENT = "text.account.consent.consentManagement";
	private static final String TEXT_ACCOUNT_CONSENT_GIVEN = "text.account.consent.given";
	private static final String TEXT_ACCOUNT_CONSENT_WITHDRAWN = "text.account.consent.withdrawn";
	private static final String TEXT_ACCOUNT_CONSENT_NOT_FOUND = "text.account.consent.notFound";
	private static final String TEXT_ACCOUNT_CONSENT_TEMPLATE_NOT_FOUND = "text.account.consent.template.notFound";
	private static final String TEXT_ACCOUNT_CLOSE = "text.account.close";
	private static final String TEXT_ACCOUNT_CONSENT_ALREADY_GIVEN = "text.account.consent.already.given";
	private static final String TEXT_ACCOUNT_CONSENT_ALREADY_WITHDRAWN = "text.account.consent.already.withdrawn";

	// Internal Redirects
	private static final String REDIRECT_TO_ADDRESS_BOOK_PAGE = REDIRECT_PREFIX + MY_ACCOUNT_ADDRESS_BOOK_URL;
	private static final String REDIRECT_TO_PAYMENT_INFO_PAGE = REDIRECT_PREFIX + "/my-account/payment-details";
	private static final String REDIRECT_TO_EDIT_ADDRESS_PAGE = REDIRECT_PREFIX + "/my-account/edit-address/";
	private static final String REDIRECT_TO_UPDATE_EMAIL_PAGE = REDIRECT_PREFIX + "/my-account/update-email";
	private static final String REDIRECT_TO_UPDATE_PROFILE = REDIRECT_PREFIX + "/my-account/update-profile";
	private static final String REDIRECT_TO_PASSWORD_UPDATE_PAGE = REDIRECT_PREFIX + "/my-account/update-password";
	private static final String REDIRECT_TO_ORDER_HISTORY_PAGE = REDIRECT_PREFIX + "/my-account/orders";
	private static final String REDIRECT_TO_CONSENT_MANAGEMENT = REDIRECT_PREFIX + "/my-account/consents";
	private static final String BASE_FLUAD = "FLUAD";
	private static final String BASE_FLUCELVAX = "FLUCELVAX";
	private static final String BASE_AFLURIA = "AFLURIA";
	private static final String BASE_PRODUCT = "baseProduct";

	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";
	private static final String ADDRESS_CODE_PATH_VARIABLE_PATTERN = "{addressCode:.*}";

	// CMS Pages
	private static final String ACCOUNT_CMS_PAGE = "account";
	private static final String PROFILE_CMS_PAGE = "profile";
	private static final String UPDATE_PASSWORD_CMS_PAGE = "updatePassword";
	private static final String UPDATE_PROFILE_CMS_PAGE = "update-profile";
	private static final String UPDATE_EMAIL_CMS_PAGE = "update-email";
	private static final String ADDRESS_BOOK_CMS_PAGE = "address-book";
	private static final String ADD_EDIT_ADDRESS_CMS_PAGE = "add-edit-address";
	private static final String PAYMENT_DETAILS_CMS_PAGE = "payment-details";
	private static final String ORDER_HISTORY_CMS_PAGE = "orders";
	private static final String ORDER_DETAIL_CMS_PAGE = "order";
	private static final String CONSENT_MANAGEMENT_CMS_PAGE = "consents";
	private static final String CLOSE_ACCOUNT_CMS_PAGE = "close-account";

	private static final Logger LOG = Logger.getLogger(AccountPageController.class);

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;
	

	@Autowired
	private SeqirusProductService seqirusProductService;
	
	@Resource(name = "seqirusOrdersFacade")
	private SeqirusOrdersFacade seqirusOrdersFacade;
	
	@Resource(name = "seqirusInvoiceFacade")
	private SeqirusInvoiceFacade seqirusInvoiceFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "passwordValidator")
	private PasswordValidator passwordValidator;

	@Resource(name = "addressValidator")
	private AddressValidator addressValidator;

	@Resource(name = "profileValidator")
	private ProfileValidator profileValidator;

	@Resource(name = "emailValidator")
	private EmailValidator emailValidator;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "addressVerificationFacade")
	private AddressVerificationFacade addressVerificationFacade;

	@Resource(name = "addressVerificationResultHandler")
	private AddressVerificationResultHandler addressVerificationResultHandler;

	@Resource(name = "orderGridFormFacade")
	private OrderGridFormFacade orderGridFormFacade;

	@Resource(name = "customerConsentDataStrategy")
	protected CustomerConsentDataStrategy customerConsentDataStrategy;

	@Resource(name = "addressDataUtil")
	private AddressDataUtil addressDataUtil;
	
	@Resource(name = "userService")
	UserService userService;
	
	@Resource(name = "seqirusOrdersService")
	private SeqirusOrdersService seqirusOrdersService;
	
	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;
	
	@Resource(name = "productService")
	private ProductService productService;
	
	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;
	
	@Autowired
	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;
	
	@Resource
	private SessionService sessionService;

	protected PasswordValidator getPasswordValidator()
	{
		return passwordValidator;
	}

	protected AddressValidator getAddressValidator()
	{
		return addressValidator;
	}

	protected ProfileValidator getProfileValidator()
	{
		return profileValidator;
	}

	protected EmailValidator getEmailValidator()
	{
		return emailValidator;
	}

	protected I18NFacade getI18NFacade()
	{
		return i18NFacade;
	}

	protected AddressVerificationFacade getAddressVerificationFacade()
	{
		return addressVerificationFacade;
	}

	protected AddressVerificationResultHandler getAddressVerificationResultHandler()
	{
		return addressVerificationResultHandler;
	}

	@ModelAttribute("countries")
	public Collection<CountryData> getCountries()
	{
		return checkoutFacade.getCountries(CountryType.SHIPPING);
	}

	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return userFacade.getTitles();
	}

	@ModelAttribute("countryDataMap")
	public Map<String, CountryData> getCountryDataMap()
	{
		final Map<String, CountryData> countryDataMap = new HashMap<>();
		for (final CountryData countryData : getCountries())
		{
			countryDataMap.put(countryData.getIsocode(), countryData);
		}
		return countryDataMap;
	}


	@RequestMapping(value = "/addressform", method = RequestMethod.GET)
	public String getCountryAddressForm(@RequestParam("addressCode") final String addressCode,
			@RequestParam("countryIsoCode") final String countryIsoCode, final Model model)
	{
		model.addAttribute("supportedCountries", getCountries());
		populateModelRegionAndCountry(model, countryIsoCode);

		final AddressForm addressForm = new AddressForm();
		model.addAttribute(ADDRESS_FORM_ATTR, addressForm);
		for (final AddressData addressData : userFacade.getAddressBook())
		{
			if (addressData.getId() != null && addressData.getId().equals(addressCode)
					&& countryIsoCode.equals(addressData.getCountry().getIsocode()))
			{
				model.addAttribute(ADDRESS_DATA_ATTR, addressData);
				addressDataUtil.convert(addressData, addressForm);
				break;
			}
		}
		return ControllerConstants.Views.Fragments.Account.CountryAddressForm;
	}

	protected void populateModelRegionAndCountry(final Model model, final String countryIsoCode)
	{
		model.addAttribute(REGIONS_ATTR, getI18NFacade().getRegionsForCountryIso(countryIsoCode));
		model.addAttribute(COUNTRY_ATTR, countryIsoCode);
	}

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String account(final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		model.addAttribute("customer",customerFacade.getCurrentCustomer());
		final SeasonEntryModel seasonEntry = seqirusCustomerRegistrationService.getSeasonEntry();
		model.addAttribute("seasonEntry",seasonEntry);		
		SeqirusInvoiceLandingResponseData invoiceLandingResponseData = seqirusInvoiceFacade.retrieveInvoiceList(seasonEntry.getCurrentSeason());
		model.addAttribute("invoice",invoiceLandingResponseData);
		sessionService.setAttribute("invoiceLandingResponse", invoiceLandingResponseData);
		//dashboardOrderData(model);
		
		final ContentPageModel accountPage = getContentPageForLabelOrId(ACCOUNT_CMS_PAGE);
		storeCmsPageInModel(model, accountPage);
		setUpMetaDataForContentPage(model, accountPage);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	
	public void dashboardOrderData(final Model model) {
		final UserModel currentUser = userService.getCurrentUser();
		final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
		if (null != customer.getDefaultB2BUnit())
		{
			final B2BUnitModel b2bUnit = customer.getDefaultB2BUnit();
            String season =null;
			final SeasonEntryModel entry = fetchSeasonEntry();
			if (null == season)
			{
				season = entry.getCurrentSeason();
			}
			model.addAttribute("presentSeason", season);
			final List<OrderSummary> customerOrderlist = new ArrayList<OrderSummary>();
			final List<OrderSummary> orders = seqirusOrdersFacade.getOrders(b2bUnit.getUid(), season);
			removeJunkOrder(orders);
			
			//final Map<Date, List<CustomerOrder>> map = new TreeMap<Date, List<CustomerOrder>>();
			//final Map<Date, List<CustomerOrder>> newMap = new TreeMap(Collections.reverseOrder());
			//final Map<String, List<CustomerOrder>> seasonOrderMap = new HashMap<String, List<CustomerOrder>>();
			//final HashSet<Date> dates = new HashSet<Date>();
			final TreeSet<Date> dates = new TreeSet<Date>(Collections.reverseOrder());
			//final HashSet<String> seasons = new HashSet<String>();

			// prepare base product list
			final List<String> baseProduct = new ArrayList<String>();
			baseProduct.add("FLUAD");
			baseProduct.add("FLUCELVAX");
			baseProduct.add("AFLURIA");
			model.addAttribute("baseProduct", baseProduct);
			final List<String> baseProductQTIV = new ArrayList<String>();
			baseProductQTIV.add("FLUADQUADRIVALENT");
			baseProductQTIV.add("FLUADTRIVALENT");
			baseProductQTIV.add("FLUCELVAX");
			baseProductQTIV.add("AFLURIA");
			model.addAttribute("baseProductQTIV", baseProductQTIV);

			final Collection<VariantOptionData> varientdataInSeason = new ArrayList<VariantOptionData>();
			final List<VariantOptionData> varientdataSortedInSeason = new ArrayList<VariantOptionData>();
			final Collection<VariantOptionData> varientdataOrderableInSeason = new ArrayList<VariantOptionData>();
			final List<VariantOptionData> varientdataOrderableListInSeason = new ArrayList<VariantOptionData>();

			getProductsForSeason("Seqirus_InSeason", varientdataInSeason, varientdataSortedInSeason, varientdataOrderableInSeason,
					varientdataOrderableListInSeason);

			final Collection<VariantOptionData> varientdataPreSeason = new ArrayList<VariantOptionData>();
			final List<VariantOptionData> varientdataSortedPreSeason = new ArrayList<VariantOptionData>();
			final Collection<VariantOptionData> varientdataOrderablePreSeason = new ArrayList<VariantOptionData>();
			final List<VariantOptionData> varientdataOrderableListPreSeason = new ArrayList<VariantOptionData>();
			
			getProductsForSeason("Seqirus_PreSeason", varientdataPreSeason, varientdataSortedPreSeason,
					varientdataOrderablePreSeason, varientdataOrderableListPreSeason);
			
			// filter selected season order from API response returned for date range
						for (final OrderSummary co : orders)
						{

							final String varCode = co.getProducts().get(0).getMaterialID();

							// set in/pre season

							for (final VariantOptionData variantOptionData : varientdataPreSeason)
							{
								if (varCode.equalsIgnoreCase(variantOptionData.getCode()))
								{
									co.setSeasonInPre("Seqirus_PreSeason");
									break;
								}
							}
							for (final VariantOptionData variantOptionData : varientdataInSeason)
							{
								if (varCode.equalsIgnoreCase(variantOptionData.getCode()))
								{
									co.setSeasonInPre("Seqirus_InSeason");
									break;
								}
							}

							// set season  like 2019 2020
							final VariantProductModel product = (VariantProductModel) productService.getProductForCode(varCode);
							final String productSeason = product.getSeason();
							if (StringUtils.isNotEmpty(productSeason) && productSeason.equalsIgnoreCase(season))
							{
								customerOrderlist.add(co);
							}

							// set contract price
							if (null != co.getSeasonInPre() && co.getSeasonInPre().equalsIgnoreCase("Seqirus_InSeason"))
							{
								BigDecimal total = new BigDecimal(0);
								for (final ProductDetail productforPrice : co.getProducts())
								{

									for (final VariantOptionData variantOptionData : varientdataInSeason)
									{
										if (productforPrice.getMaterialID().equalsIgnoreCase(variantOptionData.getCode())
												&& null != variantOptionData.getPriceData() && null != variantOptionData.getPriceData().getValue())
										{
											total = total.add(variantOptionData.getPriceData().getValue()
													.multiply(BigDecimal.valueOf(productforPrice.getQuantity())));

										}
									}

								}
								co.setEstimatedCost(total);

								// set total quantity for each base product

								for (final String base : baseProduct)
								{
									int totalQty = 0;
									for (final VariantOptionData variantOptionData : varientdataInSeason)
									{

										VariantProductModel productModel;
										productModel = (VariantProductModel) seqirusProductService
												.getProductDataForCode(variantOptionData.getCode());

										if (base.equalsIgnoreCase(productModel.getBaseName()))
										{

											for (final ProductDetail productData : co.getProducts())
											{

												if (productModel.getCode().equalsIgnoreCase(productData.getMaterialID()))
												{

													totalQty = totalQty + productData.getQuantity();
												}
											}
										}
									}
									if (base.equalsIgnoreCase(BASE_FLUAD))
									{

										co.setTotalFluad(totalQty);
									}
									if (base.equalsIgnoreCase(BASE_FLUCELVAX))
									{

										co.setTotalFlucelvax(totalQty);
									}
									if (base.equalsIgnoreCase(BASE_AFLURIA))
									{

										co.setTotalAfluria(totalQty);
									}
								}
								// set quantity end
							}

							if (null != co.getSeasonInPre() && co.getSeasonInPre().equalsIgnoreCase("Seqirus_PreSeason"))
							{
								BigDecimal total = new BigDecimal(0);
								for (final ProductDetail productforPrice : co.getProducts())
								{

									for (final VariantOptionData variantOptionData : varientdataPreSeason)
									{
										if (productforPrice.getMaterialID().equalsIgnoreCase(variantOptionData.getCode())
												&& null != variantOptionData.getPriceData() && null != variantOptionData.getPriceData().getValue())
										{
											total = total.add(variantOptionData.getPriceData().getValue()
													.multiply(BigDecimal.valueOf(productforPrice.getQuantity())));

										}
									}

								}
								co.setEstimatedCost(total);


								// set total quantity for each base product

								for (final String base : baseProduct)
								{
									int totalBaseQty = 0;
									for (final VariantOptionData variantOptionData : varientdataPreSeason)
									{

										VariantProductModel productModel;
										productModel = (VariantProductModel) seqirusProductService
												.getProductDataForCode(variantOptionData.getCode());

										if (base.equalsIgnoreCase(productModel.getBaseName()))
										{

											for (final ProductDetail productdata : co.getProducts())
											{

												if (productModel.getCode().equalsIgnoreCase(productdata.getMaterialID()))
												{

													totalBaseQty = totalBaseQty + productdata.getQuantity();
												}
											}
										}
									}
									if (base.equalsIgnoreCase(BASE_FLUAD))
									{

										co.setTotalFluad(totalBaseQty);
									}
									if (base.equalsIgnoreCase(BASE_FLUCELVAX))
									{

										co.setTotalFlucelvax(totalBaseQty);
									}
									if (base.equalsIgnoreCase(BASE_AFLURIA))
									{

										co.setTotalAfluria(totalBaseQty);
									}
								}

							}


						}

						model.addAttribute("orders", customerOrderlist);
						
						int openOrderCount = 0;
						for (final OrderSummary co : orders) {
							if(co.getStatus().equalsIgnoreCase("Order Placed") || co.getStatus().equalsIgnoreCase("Order in Process"))
								openOrderCount++;
						}
						model.addAttribute("openOrders", openOrderCount);
		}
	}
	
	private void getProductsForSeason(final String season, final Collection<VariantOptionData> varientdata,
			final List<VariantOptionData> varientdataSorted, final Collection<VariantOptionData> varientdataOrderable,
			final List<VariantOptionData> varientdataOrderableList)
	{


		// all orderable products
		final CategoryModel category = commerceCategoryService.getCategoryForCode(season);
		//			final CategoryPageModel categoryPage = getCategoryPage(category);
		final List<ProductOption> options = new ArrayList<>(
				Arrays.asList(ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_MEDIA,
						ProductOption.BASIC, ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
						ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS,
						ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
						ProductOption.PRICE_RANGE, ProductOption.DELIVERY_MODE_AVAILABILITY));


		final List<ProductModel> products = category.getProducts();

		final List<ProductWrapperData> productDataList = new ArrayList<>();
		final List<Map<String, Collection<VariantOptionData>>> productFormulationsList = new ArrayList<Map<String, Collection<VariantOptionData>>>();

		for (final ProductModel productModel : products)
		{
			final ProductWrapperData pwd = new ProductWrapperData();
			ProductData productData = productFacade.getProductForCodeAndOptions(productModel.getCode(), options);
			pwd.setProductFormulations(groupProductsByFormulationOrder(productData));
			pwd.setMinPrice(minProductPrice(productData));
			pwd.setProductData(productData);
			productDataList.add(pwd);
			productFormulationsList.add(groupProductsByFormulationOrder(productData));
			productData = null;
		}



		for (final Map<String, Collection<VariantOptionData>> productFormulations : productFormulationsList)
		{
			for (final Map.Entry<String, Collection<VariantOptionData>> entry : productFormulations.entrySet())
			{

				varientdata.addAll(entry.getValue());
				varientdataSorted.addAll(entry.getValue());

			}

		}

		for (final VariantOptionData varient : varientdata)
		{
			if (varient.getOrderable())
			{
				varientdataOrderable.add(varient);
				varientdataOrderableList.add(varient);
			}
		}

		Collections.sort(varientdataSorted, new Comparator<VariantOptionData>()
		{

			public int compare(final VariantOptionData u1, final VariantOptionData u2)
			{
				if ((null == u1.getSequenceId()) || (null == u2.getSequenceId()))
				{
					return 0;
				}
				else
				{
					return u1.getSequenceId().compareTo(u2.getSequenceId());
				}
			}
		});


	}

	private Map<String, Collection<VariantOptionData>> groupProductsByFormulationOrder(final ProductData productData)
	{
		final List<VariantOptionData> variants = productData.getVariantOptions();

		final Map<String, Collection<VariantOptionData>> productsGroupByFormulation = new HashMap<>();

		if (null == variants || variants.isEmpty())
		{
			return productsGroupByFormulation;
		}

		for (final VariantOptionData variantOptionData : variants)
		{

			if (CollectionUtils.isNotEmpty(variantOptionData.getVariantOptionQualifiers()))
			{
				udpateproductAndFormulationMap(productsGroupByFormulation, SeqirusUtils.getFormulationCategories(variantOptionData));

			}
			else if (productsGroupByFormulation.containsKey(NO_FORMULATION))
			{
				productsGroupByFormulation.get(NO_FORMULATION).add(variantOptionData);
			}
			else
			{
				final List<VariantOptionData> vodList = new ArrayList<>();
				vodList.add(variantOptionData);
				productsGroupByFormulation.put(NO_FORMULATION, vodList);
			}
		}

		SeqirusUtils.sortBySequenceId(productsGroupByFormulation);
		final TreeMap<String, Collection<VariantOptionData>> sorted = new TreeMap<>();
		sorted.putAll(productsGroupByFormulation);
		return sorted;
	}
	
	
	private void udpateproductAndFormulationMap(final Map<String, Collection<VariantOptionData>> productsGroupByFormulation,
			final Map<String, Collection<VariantOptionData>> formulationData)
	{

		//Map<BlogKey, Blog> map = new HashMap<BlogKey, Blog>();
		for (final String formulation : formulationData.keySet())
		{
			if (productsGroupByFormulation.containsKey(formulation))
			{
				productsGroupByFormulation.get(formulation).addAll(formulationData.get(formulation));
				SeqirusUtils.removeDuplicate(productsGroupByFormulation.get(formulation));
			}
			else
			{
				productsGroupByFormulation.put(formulation, formulationData.get(formulation));
			}
		}

	}
	
	private BigDecimal minProductPrice(final ProductData productData)
	{
		final List<VariantOptionData> variants = productData.getVariantOptions();
		if (null != variants)
		{
			final List<BigDecimal> allVariantsPrices = new ArrayList<BigDecimal>();
			for (final VariantOptionData variantOptionData : variants)
			{
				if (null != variantOptionData.getPriceData())
				{
					allVariantsPrices.add(variantOptionData.getPriceData().getValue());
				}

			}
			if (allVariantsPrices.isEmpty())
			{
				return BigDecimal.ZERO;
			}
			return Collections.min(allVariantsPrices);
		}
		else
		{
			return BigDecimal.ZERO;
		}
	}
	
	
	public class ProductWrapperData
	{
		private ProductData productData;
		private Map<String, Collection<VariantOptionData>> productFormulations;
		private BigDecimal minPrice;

		/**
		 * @return the productData
		 */
		public ProductData getProductData()
		{
			return productData;
		}

		/**
		 * @param productData
		 *           the productData to set
		 */
		public void setProductData(final ProductData productData)
		{
			this.productData = productData;
		}

		/**
		 * @return the minPrice
		 */
		public BigDecimal getMinPrice()
		{
			return minPrice;
		}

		/**
		 * @param minPrice
		 *           the minPrice to set
		 */
		public void setMinPrice(final BigDecimal minPrice)
		{
			this.minPrice = minPrice;
		}

		/**
		 * @return the productFormulations
		 */
		public Map<String, Collection<VariantOptionData>> getProductFormulations()
		{
			return productFormulations;
		}

		/**
		 * @param productFormulations
		 *           the productFormulations to set
		 */
		public void setProductFormulations(final Map<String, Collection<VariantOptionData>> productFormulations)
		{
			this.productFormulations = productFormulations;
		}

	}
	
	

	
	private void removeJunkOrder(final List<OrderSummary> customerOrderlist)
	{

		final Set<String> materialSet = new HashSet<String>();
		//Get All Unique codes available in all order
		for (final OrderSummary co : customerOrderlist)
		{
			for (final ProductDetail d : co.getProducts())
			{
				materialSet.add(d.getMaterialID());
			}
		}
		final List<String> availableProductCode = new ArrayList<String>();
		//Prepare Available Code list
		for (final String code : materialSet)
		{
			try
			{
				final VariantProductModel product = (VariantProductModel) productService.getProductForCode(code);
				availableProductCode.add(code);
			}
			catch (final Exception e)
			{
				LOG.error("exception while fetching product details", e);
			}
		}
		//Remove Unwanted orders
		final Iterator<OrderSummary> custItr = customerOrderlist.iterator();
		while (custItr.hasNext())
		{
			final OrderSummary co = custItr.next();
			final Iterator<ProductDetail> orderItr = co.getProducts().iterator();
			while (orderItr.hasNext())
			{
				final ProductDetail d = orderItr.next();
				if (!availableProductCode.contains(d.getMaterialID()))
				{
					orderItr.remove();
				}
			}
			if (co.getProducts().isEmpty())
			{
				custItr.remove();
			}
		}

	}
	/**
	 * @param entry
	 * @return
	 */
	private SeasonEntryModel fetchSeasonEntry()
	{
		SeasonEntryModel entry = null;
		if (null != seqirusOrdersService)
		{
			entry = seqirusOrdersService.getSeasonEntry();
		}
		return entry;
	}


	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	@RequireHardLogIn
	public String orders(@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model) throws CMSItemNotFoundException
	{
		// Handle paged search results
		final PageableData pageableData = createPageableData(page, 5, sortCode, showMode);
		final SearchPageData<OrderHistoryData> searchPageData = orderFacade.getPagedOrderHistoryForStatuses(pageableData);
		populateModel(model, searchPageData, showMode);
		final ContentPageModel orderHistoryPage = getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE);
		storeCmsPageInModel(model, orderHistoryPage);
		setUpMetaDataForContentPage(model, orderHistoryPage);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("text.account.orderHistory"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/order/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String order(@PathVariable("orderCode") final String orderCode, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		try
		{
			final OrderData orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
			model.addAttribute("orderData", orderDetails);

			final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
			breadcrumbs.add(new Breadcrumb("/my-account/orders",
					getMessageSource().getMessage("text.account.orderHistory", null, getI18nService().getCurrentLocale()), null));
			breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.order.orderBreadcrumb", new Object[]
			{ orderDetails.getCode() }, "Order {0}", getI18nService().getCurrentLocale()), null));
			model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);

		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn("Attempted to load a order that does not exist or is not visible", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found", null);
			return REDIRECT_TO_ORDER_HISTORY_PAGE;
		}
		final ContentPageModel orderDetailPage = getContentPageForLabelOrId(ORDER_DETAIL_CMS_PAGE);
		storeCmsPageInModel(model, orderDetailPage);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		setUpMetaDataForContentPage(model, orderDetailPage);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/order/" + ORDER_CODE_PATH_VARIABLE_PATTERN
			+ "/getReadOnlyProductVariantMatrix", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getProductVariantMatrixForResponsive(@PathVariable("orderCode") final String orderCode,
			@RequestParam("productCode") final String productCode, final Model model)
	{
		final OrderData orderData = orderFacade.getOrderDetailsForCodeWithoutUser(orderCode);

		final Map<String, ReadOnlyOrderGridData> readOnlyMultiDMap = orderGridFormFacade.getReadOnlyOrderGridForProductInOrder(
				productCode, Arrays.asList(ProductOption.BASIC, ProductOption.CATEGORIES), orderData);
		model.addAttribute("readOnlyMultiDMap", readOnlyMultiDMap);

		return ControllerConstants.Views.Fragments.Checkout.ReadOnlyExpandedOrderForm;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String profile(final Model model) throws CMSItemNotFoundException
	{
		final List<TitleData> titles = userFacade.getTitles();

		final CustomerData customerData = customerFacade.getCurrentCustomer();
		if (customerData.getTitleCode() != null)
		{
			model.addAttribute("title", findTitleForCode(titles, customerData.getTitleCode()));
		}

		model.addAttribute("customerData", customerData);

		final ContentPageModel profilePage = getContentPageForLabelOrId(PROFILE_CMS_PAGE);
		storeCmsPageInModel(model, profilePage);
		setUpMetaDataForContentPage(model, profilePage);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	protected TitleData findTitleForCode(final List<TitleData> titles, final String code)
	{
		if (code != null && !code.isEmpty() && titles != null && !titles.isEmpty())
		{
			for (final TitleData title : titles)
			{
				if (code.equals(title.getCode()))
				{
					return title;
				}
			}
		}
		return null;
	}

	@RequestMapping(value = "/update-email", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editEmail(final Model model) throws CMSItemNotFoundException
	{
		final CustomerData customerData = customerFacade.getCurrentCustomer();
		final UpdateEmailForm updateEmailForm = new UpdateEmailForm();

		updateEmailForm.setEmail(customerData.getDisplayUid());

		model.addAttribute("updateEmailForm", updateEmailForm);
		final ContentPageModel updateEmailPage = getContentPageForLabelOrId(UPDATE_EMAIL_CMS_PAGE);
		storeCmsPageInModel(model, updateEmailPage);
		setUpMetaDataForContentPage(model, updateEmailPage);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-email", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateEmail(final UpdateEmailForm updateEmailForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getEmailValidator().validate(updateEmailForm, bindingResult);
		String returnAction = REDIRECT_TO_UPDATE_EMAIL_PAGE;

		if (!bindingResult.hasErrors() && !updateEmailForm.getEmail().equals(updateEmailForm.getChkEmail()))
		{
			bindingResult.rejectValue("chkEmail", "validation.checkEmail.equals", new Object[] {}, "validation.checkEmail.equals");
		}

		if (bindingResult.hasErrors())
		{
			returnAction = setErrorMessagesAndCMSPage(model, UPDATE_EMAIL_CMS_PAGE);
		}
		else
		{
			try
			{
				customerFacade.changeUid(updateEmailForm.getEmail(), updateEmailForm.getPassword());
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);

				// Replace the spring security authentication with the new UID
				final String newUid = customerFacade.getCurrentCustomer().getUid().toLowerCase();
				final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
				final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(newUid, null,
						oldAuthentication.getAuthorities());
				newAuthentication.setDetails(oldAuthentication.getDetails());
				SecurityContextHolder.getContext().setAuthentication(newAuthentication);
			}
			catch (final DuplicateUidException e)
			{
				bindingResult.rejectValue("email", "profile.email.unique");
				returnAction = setErrorMessagesAndCMSPage(model, UPDATE_EMAIL_CMS_PAGE);
			}
			catch (final PasswordMismatchException passwordMismatchException)
			{
				bindingResult.rejectValue("password", PROFILE_CURRENT_PASSWORD_INVALID);
				returnAction = setErrorMessagesAndCMSPage(model, UPDATE_EMAIL_CMS_PAGE);
			}
		}

		return returnAction;
	}

	protected String setErrorMessagesAndCMSPage(final Model model, final String cmsPageLabelOrId) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
		final ContentPageModel cmsPage = getContentPageForLabelOrId(cmsPageLabelOrId);
		storeCmsPageInModel(model, cmsPage);
		setUpMetaDataForContentPage(model, cmsPage);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		return getViewForPage(model);
	}


	@RequestMapping(value = "/update-profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editProfile(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(TITLE_DATA_ATTR, userFacade.getTitles());

		final CustomerData customerData = customerFacade.getCurrentCustomer();
		final UpdateProfileForm updateProfileForm = new UpdateProfileForm();

		updateProfileForm.setTitleCode(customerData.getTitleCode());
		updateProfileForm.setFirstName(customerData.getFirstName());
		updateProfileForm.setLastName(customerData.getLastName());

		model.addAttribute("updateProfileForm", updateProfileForm);

		final ContentPageModel updateProfilePage = getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE);
		storeCmsPageInModel(model, updateProfilePage);
		setUpMetaDataForContentPage(model, updateProfilePage);

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-profile", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateProfile(final UpdateProfileForm updateProfileForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getProfileValidator().validate(updateProfileForm, bindingResult);

		String returnAction = REDIRECT_TO_UPDATE_PROFILE;
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		final CustomerData customerData = new CustomerData();
		customerData.setTitleCode(updateProfileForm.getTitleCode());
		customerData.setFirstName(updateProfileForm.getFirstName());
		customerData.setLastName(updateProfileForm.getLastName());
		customerData.setUid(currentCustomerData.getUid());
		customerData.setDisplayUid(currentCustomerData.getDisplayUid());

		model.addAttribute(TITLE_DATA_ATTR, userFacade.getTitles());

		final ContentPageModel updateProfilePage = getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE);
		storeCmsPageInModel(model, updateProfilePage);
		setUpMetaDataForContentPage(model, updateProfilePage);

		if (bindingResult.hasErrors())
		{
			returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE);
		}
		else
		{
			try
			{
				customerFacade.updateProfile(customerData);
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);

			}
			catch (final DuplicateUidException e)
			{
				bindingResult.rejectValue("email", "registration.error.account.exists.title");
				returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE);
			}
		}


		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		return returnAction;
	}

	@RequestMapping(value = "/update-password", method = RequestMethod.GET)
	@RequireHardLogIn
	public String updatePassword(final Model model) throws CMSItemNotFoundException
	{
		final UpdatePasswordForm updatePasswordForm = new UpdatePasswordForm();

		model.addAttribute("updatePasswordForm", updatePasswordForm);

		final ContentPageModel updatePasswordPage = getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE);
		storeCmsPageInModel(model, updatePasswordPage);
		setUpMetaDataForContentPage(model, updatePasswordPage);

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-password", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updatePassword(final UpdatePasswordForm updatePasswordForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getPasswordValidator().validate(updatePasswordForm, bindingResult);
		if (!bindingResult.hasErrors())
		{
			if (updatePasswordForm.getNewPassword().equals(updatePasswordForm.getCheckNewPassword()))
			{
				try
				{
					customerFacade.changePassword(updatePasswordForm.getCurrentPassword(), updatePasswordForm.getNewPassword());
				}
				catch (final PasswordMismatchException localException)
				{
					bindingResult.rejectValue("currentPassword", PROFILE_CURRENT_PASSWORD_INVALID, new Object[] {},
							PROFILE_CURRENT_PASSWORD_INVALID);
				}
			}
			else
			{
				bindingResult.rejectValue("checkNewPassword", "validation.checkPwd.equals", new Object[] {},
						"validation.checkPwd.equals");
			}
		}

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			final ContentPageModel updatePasswordPage = getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE);
			storeCmsPageInModel(model, updatePasswordPage);
			setUpMetaDataForContentPage(model, updatePasswordPage);

			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
			return getViewForPage(model);
		}
		else
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.account.confirmation.password.updated", null);
			return REDIRECT_TO_PASSWORD_UPDATE_PAGE;
		}
	}

	@RequestMapping(value = "/address-book", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getAddressBook(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(ADDRESS_DATA_ATTR, userFacade.getAddressBook());
		final ContentPageModel addressBookPage = getContentPageForLabelOrId(ADDRESS_BOOK_CMS_PAGE);
		storeCmsPageInModel(model, addressBookPage);
		setUpMetaDataForContentPage(model, addressBookPage);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_ADDRESS_BOOK));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/add-address", method = RequestMethod.GET)
	@RequireHardLogIn
	public String addAddress(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(COUNTRY_DATA_ATTR, checkoutFacade.getCountries(CountryType.SHIPPING));
		model.addAttribute(TITLE_DATA_ATTR, userFacade.getTitles());
		final AddressForm addressForm = getPreparedAddressForm();
		model.addAttribute(ADDRESS_FORM_ATTR, addressForm);
		model.addAttribute(ADDRESS_BOOK_EMPTY_ATTR, Boolean.valueOf(CollectionUtils.isEmpty(userFacade.getAddressBook())));
		model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.FALSE);
		final ContentPageModel addEditAddressPage = getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE);
		storeCmsPageInModel(model, addEditAddressPage);
		setUpMetaDataForContentPage(model, addEditAddressPage);

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_ADDRESS_BOOK_URL,
				getMessageSource().getMessage(TEXT_ACCOUNT_ADDRESS_BOOK, null, getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("#",
				getMessageSource().getMessage("text.account.addressBook.addEditAddress", null, getI18nService().getCurrentLocale()),
				null));
		model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	protected AddressForm getPreparedAddressForm()
	{
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		final AddressForm addressForm = new AddressForm();
		addressForm.setFirstName(currentCustomerData.getFirstName());
		addressForm.setLastName(currentCustomerData.getLastName());
		addressForm.setTitleCode(currentCustomerData.getTitleCode());
		return addressForm;
	}

	@RequestMapping(value = "/add-address", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addAddress(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getAddressValidator().validate(addressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			final ContentPageModel addEditAddressPage = getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE);
			storeCmsPageInModel(model, addEditAddressPage);
			setUpMetaDataForContentPage(model, addEditAddressPage);
			setUpAddressFormAfterError(addressForm, model);
			return getViewForPage(model);
		}

		final AddressData newAddress = addressDataUtil.convertToVisibleAddressData(addressForm);

		if (CollectionUtils.isEmpty(userFacade.getAddressBook()))
		{
			newAddress.setDefaultAddress(true);
		}
		else
		{
			newAddress.setDefaultAddress(addressForm.getDefaultAddress() != null && addressForm.getDefaultAddress().booleanValue());
		}

		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.added");

		populateModelRegionAndCountry(model, addressForm.getCountryIso());
		model.addAttribute("edit", Boolean.FALSE);
		model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.valueOf(userFacade.isDefaultAddress(addressForm.getAddressId())));

		if (addressRequiresReview)
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			return getViewForPage(model);
		}

		userFacade.addAddress(newAddress);


		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.added",
				null);

		return REDIRECT_TO_EDIT_ADDRESS_PAGE + newAddress.getId();
	}

	protected void setUpAddressFormAfterError(final AddressForm addressForm, final Model model)
	{
		model.addAttribute(COUNTRY_DATA_ATTR, checkoutFacade.getCountries(CountryType.SHIPPING));
		model.addAttribute(TITLE_DATA_ATTR, userFacade.getTitles());
		model.addAttribute(ADDRESS_BOOK_EMPTY_ATTR, Boolean.valueOf(CollectionUtils.isEmpty(userFacade.getAddressBook())));
		model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.valueOf(userFacade.isDefaultAddress(addressForm.getAddressId())));
		if (addressForm.getCountryIso() != null)
		{
			populateModelRegionAndCountry(model, addressForm.getCountryIso());
		}
	}

	@RequestMapping(value = "/edit-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddress(@PathVariable("addressCode") final String addressCode, final Model model)
			throws CMSItemNotFoundException
	{
		final AddressForm addressForm = new AddressForm();
		model.addAttribute(COUNTRY_DATA_ATTR, checkoutFacade.getCountries(CountryType.SHIPPING));
		model.addAttribute(TITLE_DATA_ATTR, userFacade.getTitles());
		model.addAttribute(ADDRESS_FORM_ATTR, addressForm);
		final List<AddressData> addressBook = userFacade.getAddressBook();
		model.addAttribute(ADDRESS_BOOK_EMPTY_ATTR, Boolean.valueOf(CollectionUtils.isEmpty(addressBook)));


		for (final AddressData addressData : addressBook)
		{
			if (addressData.getId() != null && addressData.getId().equals(addressCode))
			{
				model.addAttribute(REGIONS_ATTR, getI18NFacade().getRegionsForCountryIso(addressData.getCountry().getIsocode()));
				model.addAttribute(COUNTRY_ATTR, addressData.getCountry().getIsocode());
				model.addAttribute(ADDRESS_DATA_ATTR, addressData);
				addressDataUtil.convert(addressData, addressForm);

				if (userFacade.isDefaultAddress(addressData.getId()))
				{
					addressForm.setDefaultAddress(Boolean.TRUE);
					model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.TRUE);
				}
				else
				{
					addressForm.setDefaultAddress(Boolean.FALSE);
					model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.FALSE);
				}
				break;
			}
		}
		final ContentPageModel addEditAddressPage = getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE);
		storeCmsPageInModel(model, addEditAddressPage);
		setUpMetaDataForContentPage(model, addEditAddressPage);

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_ADDRESS_BOOK_URL,
				getMessageSource().getMessage(TEXT_ACCOUNT_ADDRESS_BOOK, null, getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("#",
				getMessageSource().getMessage("text.account.addressBook.addEditAddress", null, getI18nService().getCurrentLocale()),
				null));
		model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute("edit", Boolean.TRUE);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/edit-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	@RequireHardLogIn
	public String editAddress(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getAddressValidator().validate(addressForm, bindingResult);
		final ContentPageModel addEditAddressPage = getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			storeCmsPageInModel(model, addEditAddressPage);
			setUpMetaDataForContentPage(model, addEditAddressPage);
			setUpAddressFormAfterError(addressForm, model);
			return getViewForPage(model);
		}

		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		final AddressData newAddress = addressDataUtil.convertToVisibleAddressData(addressForm);

		if (Boolean.TRUE.equals(addressForm.getDefaultAddress()) || userFacade.getAddressBook().size() <= 1)
		{
			newAddress.setDefaultAddress(true);
		}

		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.updated");

		model.addAttribute(REGIONS_ATTR, getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
		model.addAttribute(COUNTRY_ATTR, addressForm.getCountryIso());
		model.addAttribute("edit", Boolean.TRUE);
		model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.valueOf(userFacade.isDefaultAddress(addressForm.getAddressId())));

		if (addressRequiresReview)
		{
			storeCmsPageInModel(model, addEditAddressPage);
			setUpMetaDataForContentPage(model, addEditAddressPage);
			return getViewForPage(model);
		}

		userFacade.editAddress(newAddress);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.updated",
				null);
		return REDIRECT_TO_EDIT_ADDRESS_PAGE + newAddress.getId();
	}

	@RequestMapping(value = "/select-suggested-address", method = RequestMethod.POST)
	public String doSelectSuggestedAddress(final AddressForm addressForm, final RedirectAttributes redirectModel)
	{
		final Set<String> resolveCountryRegions = org.springframework.util.StringUtils
				.commaDelimitedListToSet(Config.getParameter("resolve.country.regions"));

		final AddressData selectedAddress = addressDataUtil.convertToVisibleAddressData(addressForm);

		final CountryData countryData = selectedAddress.getCountry();

		if (!resolveCountryRegions.contains(countryData.getIsocode()))
		{
			selectedAddress.setRegion(null);
		}

		if (Boolean.TRUE.equals(addressForm.getEditAddress()))
		{
			userFacade.editAddress(selectedAddress);
		}
		else
		{
			userFacade.addAddress(selectedAddress);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.added");

		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	@RequestMapping(value = "/remove-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressCode);
		userFacade.removeAddress(addressData);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.removed");
		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	@RequestMapping(value = "/set-default-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String setDefaultAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setDefaultAddress(true);
		addressData.setVisibleInAddressBook(true);
		addressData.setId(addressCode);
		userFacade.setDefaultAddress(addressData);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
				"account.confirmation.default.address.changed");
		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	@RequestMapping(value = "/payment-details", method = RequestMethod.GET)
	@RequireHardLogIn
	public String paymentDetails(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("customerData", customerFacade.getCurrentCustomer());
		model.addAttribute("paymentInfoData", userFacade.getCCPaymentInfos(true));
		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("text.account.paymentDetails"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/set-default-payment-details", method = RequestMethod.POST)
	@RequireHardLogIn
	public String setDefaultPaymentDetails(@RequestParam final String paymentInfoId)
	{
		CCPaymentInfoData paymentInfoData = null;
		if (StringUtils.isNotBlank(paymentInfoId))
		{
			paymentInfoData = userFacade.getCCPaymentInfoForCode(paymentInfoId);
		}
		userFacade.setDefaultPaymentInfo(paymentInfoData);
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	@RequestMapping(value = "/remove-payment-method", method = RequestMethod.POST)
	@RequireHardLogIn
	public String removePaymentMethod(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		userFacade.unlinkCCPaymentInfo(paymentMethodId);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
				"text.account.profile.paymentCart.removed");
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	@RequestMapping(value = "/consents", method = RequestMethod.GET)
	@RequireHardLogIn
	public String consentManagement(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("consentTemplateDataList", getConsentFacade().getConsentTemplatesWithConsents());
		final ContentPageModel consentManagementPage = getContentPageForLabelOrId(CONSENT_MANAGEMENT_CMS_PAGE);
		storeCmsPageInModel(model, consentManagementPage);
		setUpMetaDataForContentPage(model, consentManagementPage);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_CONSENT_MANAGEMENT));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/consents/give/{consentTemplateId}/{version}", method = RequestMethod.POST)
	@RequireHardLogIn
	public String giveConsent(@PathVariable final String consentTemplateId, @PathVariable final Integer version,
			final RedirectAttributes redirectModel)
	{
		try
		{
			getConsentFacade().giveConsent(consentTemplateId, version);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, TEXT_ACCOUNT_CONSENT_GIVEN);
		}
		catch (final ModelNotFoundException | AmbiguousIdentifierException e)
		{
			LOG.warn(String.format("ConsentTemplate with code [%s] and version [%s] was not found", consentTemplateId, version), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					TEXT_ACCOUNT_CONSENT_TEMPLATE_NOT_FOUND, null);
		}
		catch (final CommerceConsentGivenException e)
		{
			LOG.warn(String.format("ConsentTemplate with code [%s] and version [%s] already has a given consent", consentTemplateId,
					version), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, TEXT_ACCOUNT_CONSENT_ALREADY_GIVEN,
					null);
		}
		customerConsentDataStrategy.populateCustomerConsentDataInSession();
		return REDIRECT_TO_CONSENT_MANAGEMENT;
	}

	@RequestMapping(value = "/consents/withdraw/{consentCode}", method = RequestMethod.POST)
	@RequireHardLogIn
	public String withdrawConsent(@PathVariable final String consentCode, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		try
		{
			getConsentFacade().withdrawConsent(consentCode);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, TEXT_ACCOUNT_CONSENT_WITHDRAWN);
		}
		catch (final ModelNotFoundException e)
		{
			LOG.warn(String.format("Consent with code [%s] was not found", consentCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, TEXT_ACCOUNT_CONSENT_NOT_FOUND,
					null);
		}
		catch (final CommerceConsentWithdrawnException e)
		{
			LOG.error(String.format("Consent with code [%s] is already withdrawn", consentCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					TEXT_ACCOUNT_CONSENT_ALREADY_WITHDRAWN, null);
		}
		customerConsentDataStrategy.populateCustomerConsentDataInSession();
		return REDIRECT_TO_CONSENT_MANAGEMENT;
	}

	@RequestMapping(value = "/close-account", method = RequestMethod.GET)
	@RequireHardLogIn
	public String showCloseAccountPage(final Model model) throws CMSItemNotFoundException
	{
		final ContentPageModel closeAccountPage = getContentPageForLabelOrId(CLOSE_ACCOUNT_CMS_PAGE);
		storeCmsPageInModel(model, closeAccountPage);
		setUpMetaDataForContentPage(model, closeAccountPage);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_CLOSE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/close-account", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	@RequireHardLogIn
	public void closeAccount(final HttpServletRequest request) throws CMSItemNotFoundException, ServletException
	{
		customerFacade.closeAccount();
		request.logout();
	}
}
