/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractRegisterPageController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.seqirus.core.exceptions.SeqirusBusinessException;
import com.seqirus.facades.customer.CustomerRegistrationFacade;
import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;
import com.seqirus.storefront.forms.CustomerRegisterForm;
import com.seqirus.storefront.forms.utils.PopulateSeqirusFormToSeqirusDataUtil;
import com.seqirus.core.enums.AccountStatusEnum;
import com.seqirus.core.exceptions.SeqirusCustomException;
import com.seqirus.facades.customer.CustomerAddressFacade;
import com.seqirus.facades.customer.SeqirusCustomerRegistrationFacade;
import com.seqirus.facades.cutomer.data.SeqirusCustomerRegistrationAddressData;
import com.seqirus.facades.cutomer.data.SeqirusCustomerRegistrationData;
import com.seqirus.facades.orders.SeqirusOrdersFacade;
import com.seqirus.storefront.controllers.ControllerConstants;
import com.seqirus.storefront.forms.SeqirusAddressForm;
import com.seqirus.storefront.forms.SeqirusBusinessDetailsForm;
/**
 * Register Controller for mobile. Handles login and register for the account flow.
 */
@Controller
@RequestMapping(value = "/register")
public class RegisterPageController extends AbstractRegisterPageController
{
	private HttpSessionRequestCache httpSessionRequestCache;

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("register");
	}
	
	@Resource(name = "userService")
	private UserService userService;
	
	@Autowired
	private CustomerRegistrationFacade customerRegistrationFacade;

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (httpSessionRequestCache.getRequest(request, response) != null)
		{
			return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
		}
		return "/";
	}

	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.Account.AccountRegisterPage;
	}

	@Resource(name = "httpSessionRequestCache")
	public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}

	
	
	/**
	 * @param custRegistrationForm
	 * @param model
	 * @param bindingResult
	 * @param request
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	
	
	@RequestMapping(value = "/newcustomer", method = RequestMethod.POST)
	public String doRegister(final RegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		getRegistrationValidator().validate(form, bindingResult);
		return processRegisterUserRequest(null, form, bindingResult, model, request, response, redirectModel);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String register(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		
	/*	if (userService.isAnonymousUser(userService.getCurrentUser()))
		{
			return REDIRECT_PREFIX + "/createprofile";
		}
		else
		{
			model.addAttribute("regCustomer", REG_CUSTOMER);
			return goToRegisterPage(model);
		} */
		
		final B2BCustomerModel account = (B2BCustomerModel) userService.getCurrentUser();
		
		final CustomerRegisterForm customerRegisterForm = new CustomerRegisterForm();
		model.addAttribute("customerRegisterForm", customerRegisterForm);
		model.addAttribute("account", account);
		storeCmsPageInModel(model, getContentPageForLabelOrId("createAccount"));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId("createAccount"));
		return getViewForPage(model);
	}
	
	@RequestMapping(value = "/saveRegData", method ={ RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public final String saveRegData(@ModelAttribute("customerRegisterForm")
	final CustomerRegisterForm customerRegisterForm, final Model model) throws CMSItemNotFoundException
	{
	 
		final PopulateSeqirusFormToSeqirusDataUtil populateSeqirusFormToSeqirusDataUtil = new PopulateSeqirusFormToSeqirusDataUtil();


		final SeqirusB2BCustomerRegistrationData seqirusB2BCustomerRegistrationData = populateSeqirusFormToSeqirusDataUtil.populateCustRegiFormToData(customerRegisterForm);
		
		
		try
		{
			customerRegistrationFacade.registerCustomerRequest(seqirusB2BCustomerRegistrationData);
		}
		catch (final SeqirusBusinessException e)
		{
			model.addAttribute("emailalradyExists", e.getMessage());
			
		}
		return "ok";
	}
	
}
