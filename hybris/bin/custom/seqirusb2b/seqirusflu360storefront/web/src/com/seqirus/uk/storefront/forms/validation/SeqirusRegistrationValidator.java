/**
 * 
 */
package com.seqirus.storefront.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import org.springframework.validation.Errors;

/**
 * @author 172553
 *
 */
public class SeqirusRegistrationValidator extends RegistrationValidator
{
	
	@Override
	public void validate(final Object object, final Errors errors)
	{
		//
	}
}
