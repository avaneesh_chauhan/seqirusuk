package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SeqirusCMSPageController extends AbstractPageController {

    @GetMapping("/influenza")
    public String influenzaPage(Model model) throws CMSItemNotFoundException {
        final ContentPageModel contentPage = getContentPageForLabelOrId("influenzaPage");
        storeCmsPageInModel(model, contentPage);
        model.addAttribute("mccPage",true);

       return "pages/cms/influenza";
    }

    @GetMapping("/influenza-65plus")
    public String influenza65PlusPage(Model model) throws CMSItemNotFoundException {
        final ContentPageModel contentPage = getContentPageForLabelOrId("influenza-65Plus");
        storeCmsPageInModel(model, contentPage);
        return "pages/cms/influenza-65plus";
    }

    @GetMapping("/adjuvant-technology")
    public String adjuvantTechnology (Model model) throws CMSItemNotFoundException {
        final ContentPageModel contentPage = getContentPageForLabelOrId("adjuvant-technology");
        storeCmsPageInModel(model, contentPage);
        return "pages/cms/adjuvant-tech";
    }

    @GetMapping("/cell-technology")
    public String cellTechnology(Model model) throws CMSItemNotFoundException {
        final ContentPageModel contentPage = getContentPageForLabelOrId("cell-technology");
        storeCmsPageInModel(model, contentPage);
        return "pages/cms/cell-tech";
    }
}
