/**
 *
 */
package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.Collections;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seqirus.storefront.controllers.ControllerConstants;


/**
 * The Class SeqirusCDCProfilePageController.
 */
@Controller
@RequestMapping("/resetpassword")
public class SeqirusCDCPasswordResetPageController extends AbstractPageController
{

	/** The Constant BREADCRUMBS. */
	private static final String BREADCRUMBS = "breadcrumbs";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(SeqirusCDCPasswordResetPageController.class);

	/** The Constant METADATA_CONTENT. */
	private static final String METADATA_CONTENT = "By creating an account with Seqirus, you can see specific product pricing, make a purchase, and manage your shipments easily.";

	/** The Constant CREATE_PROFILE_CMS_PAGE. */
	private static final String PASSWORD_RESET_CMS_PAGE = "resetpassword";


	/**
	 * Reset password.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String resetPassword(final Model model) throws CMSItemNotFoundException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("SeqirusCDCResetPasswordPageController:resetPassword() ");
		}
		storeCmsPageInModel(model, getResetPasswordPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getResetPasswordPage());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, METADATA_CONTENT);
		final Breadcrumb registerBreadcrumbEntry = new Breadcrumb("#", getMessageSource().getMessage("header.link.resetpassword",
				null, getI18nService().getCurrentLocale()), null);
		model.addAttribute(BREADCRUMBS, Collections.singletonList(registerBreadcrumbEntry));
		return getView();
	}


	/**
	 * Gets the reset password page.
	 *
	 * @return the reset password page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected AbstractPageModel getResetPasswordPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId(PASSWORD_RESET_CMS_PAGE);
	}

	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	protected String getView()
	{
		return ControllerConstants.Views.Pages.Password.CDCPasswordResetPage;
	}

}
