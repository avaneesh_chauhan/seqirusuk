/**
 * 
 */
package com.seqirus.storefront.forms;

/**
 * @author 172553
 *
 */
public class SeqirusShippingLocationForm
{
	private SeqirusAddressForm shippingAddressForm;
	private boolean license;
	private SeqirusAddressForm licenseAddressForm;
	private String nameOnLicense;
	private String licenseNum;
	/**
	 * @return the shippingAddressForm
	 */
	public SeqirusAddressForm getShippingAddressForm()
	{
		return shippingAddressForm;
	}
	/**
	 * @param shippingAddressForm the shippingAddressForm to set
	 */
	public void setShippingAddressForm(SeqirusAddressForm shippingAddressForm)
	{
		this.shippingAddressForm = shippingAddressForm;
	}
	/**
	 * @return the license
	 */
	public boolean isLicense()
	{
		return license;
	}
	/**
	 * @param license the license to set
	 */
	public void setLicense(boolean license)
	{
		this.license = license;
	}
	/**
	 * @return the licenseAddressForm
	 */
	public SeqirusAddressForm getLicenseAddressForm()
	{
		return licenseAddressForm;
	}
	/**
	 * @param licenseAddressForm the licenseAddressForm to set
	 */
	public void setLicenseAddressForm(SeqirusAddressForm licenseAddressForm)
	{
		this.licenseAddressForm = licenseAddressForm;
	}
	/**
	 * @return the nameOnLicense
	 */
	public String getNameOnLicense()
	{
		return nameOnLicense;
	}
	/**
	 * @param nameOnLicense the nameOnLicense to set
	 */
	public void setNameOnLicense(String nameOnLicense)
	{
		this.nameOnLicense = nameOnLicense;
	}
	/**
	 * @return the licenseNum
	 */
	public String getLicenseNum()
	{
		return licenseNum;
	}
	/**
	 * @param licenseNum the licenseNum to set
	 */
	public void setLicenseNum(String licenseNum)
	{
		this.licenseNum = licenseNum;
	}
	
	
}
