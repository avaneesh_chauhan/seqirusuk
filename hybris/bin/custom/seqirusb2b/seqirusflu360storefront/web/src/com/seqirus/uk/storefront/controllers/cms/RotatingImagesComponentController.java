/**
 *
 */
package com.seqirus.storefront.controllers.cms;

import de.hybris.platform.cms2lib.model.components.RotatingImagesComponentModel;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.seqirus.storefront.controllers.ControllerConstants;

/**
 * @author 172553
 *
 */
@Controller("RotatingImagesComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.RotatingImagesComponent)
public class RotatingImagesComponentController extends AbstractAcceleratorCMSComponentController<RotatingImagesComponentModel>
{

	private static final Logger LOGGER = Logger.getLogger(RotatingImagesComponentController.class);

	private static final String BANNERS = "banners";
	private static final String COMPONENTS = "components";


	/**
	 * CMS Controller For Rotating Images Component
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final RotatingImagesComponentModel component)
	{

				model.addAttribute(BANNERS, component.getBanners());
				model.addAttribute(COMPONENTS, component);
	}

}
