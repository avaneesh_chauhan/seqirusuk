/**
 *
 */
package com.seqirus.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;

import java.util.List;


/**
 * @author nesingh
 *
 */
public class CustomerRegisterForm extends RegisterForm
{

	private String asmAgentId;
	private String asmAgentName;
	private String asmAgentEmail;
	private String hiddenEmail;
	private String confEmail;
	private String position;
	private boolean optNotify;
	private String orgName;
	private String phoneNumber;
	private String phoneExt;
	private LicenseDetailsForm licenseDetailsForm;
	private BillingAndShippingAddressForm billAndShippAddressForm;
	private BillingAndShippingAddressForm orgAddressForm;

	private List<ShippingAddressForm> shippingAddressForm;

	private BillingAndShippingAddressForm payerAddressForm;

	/**
	 * @return the asmAgentId
	 */
	public String getAsmAgentId()
	{
		return asmAgentId;
	}

	/**
	 * @param asmAgentId
	 *           the asmAgentId to set
	 */
	public void setAsmAgentId(final String asmAgentId)
	{
		this.asmAgentId = asmAgentId;
	}

	/**
	 * @return the asmAgenName
	 */
	public String getAsmAgentName()
	{
		return asmAgentName;
	}

	/**
	 * @param asmAgenName
	 *           the asmAgenName to set
	 */
	public void setAsmAgentName(final String asmAgentName)
	{
		this.asmAgentName = asmAgentName;
	}

	/**
	 * @return the asmAgentEmail
	 */
	public String getAsmAgentEmail()
	{
		return asmAgentEmail;
	}

	/**
	 * @param asmAgentEmail
	 *           the asmAgentEmail to set
	 */
	public void setAsmAgentEmail(final String asmAgentEmail)
	{
		this.asmAgentEmail = asmAgentEmail;
	}

	/**
	 * @return the hiddenEmail
	 */
	public String getHiddenEmail()
	{
		return hiddenEmail;
	}

	/**
	 * @param hiddenEmail
	 *           the hiddenEmail to set
	 */
	public void setHiddenEmail(final String hiddenEmail)
	{
		this.hiddenEmail = hiddenEmail;
	}

	private boolean isGovFederalOrg;

	/**
	 * @return the isGovFederalOrg
	 */
	public boolean isGovFederalOrg()
	{
		return isGovFederalOrg;
	}

	/**
	 * @param isGovFederalOrg
	 *           the isGovFederalOrg to set
	 */
	public void setGovFederalOrg(final boolean isGovFederalOrg)
	{
		this.isGovFederalOrg = isGovFederalOrg;
	}


	/**
	 * @return the shippingAddressForm
	 */
	public List<ShippingAddressForm> getShippingAddressForm()
	{
		return shippingAddressForm;
	}

	/**
	 * @param shippingAddressForm
	 *           the shippingAddressForm to set
	 */
	public void setShippingAddressForm(final List<ShippingAddressForm> shippingAddressForm)
	{
		this.shippingAddressForm = shippingAddressForm;
	}

	/**
	 * @return the confEmail
	 */
	public String getConfEmail()
	{
		return confEmail;
	}

	/**
	 * @param confEmail
	 *           the confEmail to set
	 */
	public void setConfEmail(final String confEmail)
	{
		this.confEmail = confEmail;
	}

	/**
	 * @return the position
	 */
	public String getPosition()
	{
		return position;
	}

	/**
	 * @param position
	 *           the position to set
	 */
	public void setPosition(final String position)
	{
		this.position = position;
	}


	/**
	 * @return the licenseDetailsForm
	 */
	public LicenseDetailsForm getLicenseDetailsForm()
	{
		return licenseDetailsForm;
	}

	/**
	 * @param licenseDetailsForm
	 *           the licenseDetailsForm to set
	 */
	public void setLicenseDetailsForm(final LicenseDetailsForm licenseDetailsForm)
	{
		this.licenseDetailsForm = licenseDetailsForm;
	}

	/**
	 * @return the billAndShippAddressForm
	 */
	public BillingAndShippingAddressForm getBillAndShippAddressForm()
	{
		return billAndShippAddressForm;
	}

	/**
	 * @param billAndShippAddressForm
	 *           the billAndShippAddressForm to set
	 */
	public void setBillAndShippAddressForm(final BillingAndShippingAddressForm billAndShippAddressForm)
	{
		this.billAndShippAddressForm = billAndShippAddressForm;
	}

	/**
	 * @return the orgName
	 */
	public String getOrgName()
	{
		return orgName;
	}

	/**
	 * @param orgName
	 *           the orgName to set
	 */
	public void setOrgName(final String orgName)
	{
		this.orgName = orgName;
	}

	/**
	 * @return the optNotify
	 */
	public boolean isOptNotify()
	{
		return optNotify;
	}

	/**
	 * @param optNotify
	 *           the optNotify to set
	 */
	public void setOptNotify(final boolean optNotify)
	{
		this.optNotify = optNotify;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *           the phoneNumber to set
	 */
	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}


	/**
	 * @return the phoneExt
	 */
	public String getPhoneExt()
	{
		return phoneExt;
	}

	/**
	 * @param phoneExt
	 *           the phoneExt to set
	 */
	public void setPhoneExt(final String phoneExt)
	{
		this.phoneExt = phoneExt;
	}


	/**
	 * @return the orgAddressForm
	 */
	public BillingAndShippingAddressForm getOrgAddressForm()
	{
		return orgAddressForm;
	}

	/**
	 * @param orgAddressForm
	 *           the orgAddressForm to set
	 */
	public void setOrgAddressForm(final BillingAndShippingAddressForm orgAddressForm)
	{
		this.orgAddressForm = orgAddressForm;
	}

	/**
	 * @return the payerAddressForm
	 */
	public BillingAndShippingAddressForm getPayerAddressForm()
	{
		return payerAddressForm;
	}

	/**
	 * @param payerAddressForm
	 *           the payerAddressForm to set
	 */
	public void setPayerAddressForm(final BillingAndShippingAddressForm payerAddressForm)
	{
		this.payerAddressForm = payerAddressForm;
	}




}
