/**
 * 
 */
package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.util.Collections;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.seqirus.storefront.controllers.ControllerConstants;

/**
 * The Class SeqirusProfileActivationPageController.
 *
 * @author Avaneesh Chauhan
 */
@Controller
@RequestMapping(value = "/profileactivation")
public class SeqirusProfileActivationPageController extends AbstractPageController
{
	

	/**
	 * Creates the user profiler.
	 *
	 * @param model the model
	 * @return the string
	 * @throws CMSItemNotFoundException the CMS item not found exception
	 */
	@GetMapping
	public String getProfileActivationPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.PROFILE_ACTIVATTION_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.PROFILE_ACTIVATTION_CMS_PAGE));
		final Breadcrumb profileActivationBreadcrumbEntry = new Breadcrumb("#",
				getMessageSource().getMessage("header.profileactivation.text", null, getI18nService().getCurrentLocale()), null);
		model.addAttribute("breadcrumbs", Collections.singletonList(profileActivationBreadcrumbEntry));
		return ControllerConstants.Views.Pages.Account.ProfileActivationPage;
	}

}
