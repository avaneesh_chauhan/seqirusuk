/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.storefront.controllers.misc;

import de.hybris.platform.acceleratorfacades.product.data.ProductWrapperData;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartOrderForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToEntryGroupForm;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.converters.populator.GroupCartModificationListPopulator;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.Config;
import com.seqirus.storefront.controllers.ControllerConstants;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seqirus.core.dataObjects.OrderReviewData;
import com.seqirus.core.dataObjects.OrderReviewForm;
import com.seqirus.core.dataObjects.ShipLoctionQty;
import org.springframework.web.bind.annotation.ModelAttribute;
import javax.servlet.http.HttpServletRequest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.servicelayer.session.SessionService;
import com.seqirus.facades.cart.SeqirusCheckoutFacade;

/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller
public class AddToCartController extends AbstractController
{
	private static final String QUANTITY_ATTR = "quantity";
	private static final String TYPE_MISMATCH_ERROR_CODE = "typeMismatch";
	private static final String ERROR_MSG_TYPE = "errorMsg";
	private static final String QUANTITY_INVALID_BINDING_MESSAGE_KEY = "basket.error.quantity.invalid.binding";
	private static final String SHOWN_PRODUCT_COUNT = "seqirusflu360storefront.storefront.minicart.shownProductCount";

	private static final Logger LOG = Logger.getLogger(AddToCartController.class);

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;
	
	@Resource
	private SessionService sessionService;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "groupCartModificationListPopulator")
	private GroupCartModificationListPopulator groupCartModificationListPopulator;
	
	@Resource(name = "seqirusCheckoutFacade")
	private SeqirusCheckoutFacade seqirusCheckoutFacade;


	protected String getViewWithBindingErrorMessages(final Model model, final BindingResult bindingErrors)
	{
		for (final ObjectError error : bindingErrors.getAllErrors())
		{
			if (isTypeMismatchError(error))
			{
				model.addAttribute(ERROR_MSG_TYPE, QUANTITY_INVALID_BINDING_MESSAGE_KEY);
			}
			else
			{
				model.addAttribute(ERROR_MSG_TYPE, error.getDefaultMessage());
			}
		}
		return ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
	}

	protected boolean isTypeMismatchError(final ObjectError error)
	{
		return error.getCode().equals(TYPE_MISMATCH_ERROR_CODE);
	}

	
	
	protected ProductWrapperData createProductWrapperData(final String sku, final String errorMsg)
	{
		final ProductWrapperData productWrapperData = new ProductWrapperData();
		final ProductData productData = new ProductData();
		productData.setCode(sku);
		productWrapperData.setProductData(productData);
		productWrapperData.setErrorMsg(errorMsg);
		return productWrapperData;
	}

	protected void logDebugException(final Exception ex)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug(ex);
		}
	}

	

	protected boolean isValidProductEntry(final OrderEntryData cartEntry)
	{
		return cartEntry.getProduct() != null && StringUtils.isNotBlank(cartEntry.getProduct().getCode());
	}

	protected boolean isValidQuantity(final OrderEntryData cartEntry)
	{
		return cartEntry.getQuantity() != null && cartEntry.getQuantity().longValue() >= 1L;
	}
	
	@RequestMapping(value = "/addtoCart" , method = RequestMethod.POST)
	@ResponseBody
	public String addtoCart(@ModelAttribute("orderReviewForm") final OrderReviewForm orderReviewForm , final HttpServletRequest request , final Model model)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{

		//String  rollOver = request.getParameter("rollOver");
		final boolean isCart = cartFacade.hasSessionCart();
		if (isCart && CollectionUtils.isNotEmpty(cartFacade.getSessionCart().getEntries()))
		{
			cartFacade.removeSessionCart();
			cartFacade.getSessionCart();
		}
		
		List<AddressData> dataList = new ArrayList<AddressData>();
		
		dataList = sessionService.getAttribute("shippingAddress");
		
		List<OrderReviewData> orderReviewdata = orderReviewForm.getOrderReviewdata();
		
		
		for(OrderReviewData orderData : orderReviewdata) {
			
			long prodcode = orderData.getProductCode();
			
			
			
				for(ShipLoctionQty locData : orderData.getShipLocationQty()) {
					if(locData.getLocQty() > 0) {
				AddressData cartAddressData = new AddressData();
				for(AddressData address : dataList) {
				
					if(address.getAddressId().equals(locData.getLocID())) {
						
						cartAddressData = address;
						seqirusCheckoutFacade.addToSeqCart(prodcode, locData.getLocQty(), cartAddressData);
					}
					
				}
				}
			}
		
			
		}
		
		
		return "ok";
	}
}