package com.seqirus.storefront.forms.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.seqirus.flu360.facades.cutomer.data.CustomerRegistrationAddressData;
import com.seqirus.flu360.facades.cutomer.data.CustomerRegistrationLicenseDetailData;
import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;
import com.seqirus.flu360.facades.cutomer.data.SeqirusCustomerShippingAddressData;
import com.seqirus.storefront.forms.CustomerRegisterForm;
import com.seqirus.storefront.forms.ShippingAddressForm;


/**
 *
 * PopulateSeqirusFormToSeqirusDataUtil to populate form class data to Data class
 */

public class PopulateSeqirusFormToSeqirusDataUtil
{

	private static final Logger LOGGER = Logger.getLogger(PopulateSeqirusFormToSeqirusDataUtil.class);


	public SeqirusB2BCustomerRegistrationData populateCustRegiFormToData(final CustomerRegisterForm form)
	{
		SeqirusB2BCustomerRegistrationData seqData = null;

		if (form != null)
		{
			seqData = new SeqirusB2BCustomerRegistrationData();
			if (form.getFirstName() != null)
			{
				seqData.setFirstName(form.getFirstName());
			}
			if (form.getLastName() != null)
			{
				seqData.setLastName(form.getLastName());
			}
			if (StringUtils.isNotBlank(form.getEmail()))
			{

				seqData.setEmailId(form.getEmail().toLowerCase());
			}
			else if (StringUtils.isNotBlank(form.getHiddenEmail()))
			{

				seqData.setEmailId(form.getHiddenEmail());
			}

			if (form.getPosition() != null)
			{
				seqData.setPosition(form.getPosition());
			}

			/*
			 * if (StringUtils.isNotBlank(form.getAsmAgentId())) { seqData.setAgentId(form.getAsmAgentId()); }
			 *
			 * if (StringUtils.isNotBlank(form.getAsmAgentName())) { seqData.setAgentName(form.getAsmAgentName()); }
			 *
			 * if (StringUtils.isNotBlank(form.getAsmAgentEmail())) { seqData.setAgentEmail(form.getAsmAgentEmail()); }
			 */


			if (form.getOrgAddressForm() != null)
			{
				final CustomerRegistrationAddressData orgAddress = new CustomerRegistrationAddressData();

				if (form.getOrgAddressForm().getAddressId() != null)
				{
					orgAddress.setAddressId(form.getOrgAddressForm().getAddressId());
				}
				if (form.getOrgAddressForm().getEmail() != null)
				{
					orgAddress.setEmailId(form.getOrgAddressForm().getEmail().toLowerCase());
				}
				if (form.getOrgAddressForm().getBillingOrgName() != null)
				{
					orgAddress.setOrganizationName(form.getOrgAddressForm().getBillingOrgName());
				}
				if (form.getOrgAddressForm().getGroupOrganization() != null)
				{
					seqData.setGroupOrganization(form.getOrgAddressForm().getGroupOrganization());
				}
				if (form.getOrgAddressForm().getMemberId() != null)
				{
					seqData.setMemberId((form.getOrgAddressForm().getMemberId()));
				}
				if (form.getOrgAddressForm().getLine1() != null)
				{
					orgAddress.setAddressLine1(form.getOrgAddressForm().getLine1());
				}
				if (form.getOrgAddressForm().getLine2() != null)
				{
					orgAddress.setAddressLine2(form.getOrgAddressForm().getLine2());
				}
				if (form.getOrgAddressForm().getCity() != null)
				{
					orgAddress.setCity(form.getOrgAddressForm().getCity());
				}
				if (form.getOrgAddressForm().getBillingState() != null)
				{
					orgAddress.setState(form.getOrgAddressForm().getBillingState());
				}
				if (form.getOrgAddressForm().getPostalCode() != null)
				{
					orgAddress.setZipCode(form.getOrgAddressForm().getPostalCode());
				}
				if (form.getPhoneNumber() != null)
				{
					orgAddress.setPhone(form.getPhoneNumber());
				}
				if (form.getPhoneExt() != null)
				{
					orgAddress.setPhoneExt(form.getPhoneExt());
				}
				if (form.getOrgAddressForm().getAltEmail() != null)
				{
					orgAddress.setAltEmail(form.getOrgAddressForm().getAltEmail());
				}
				if (form.getOrgAddressForm().getDuns() != null)
				{
					orgAddress.setDuns(form.getOrgAddressForm().getDuns());
				}
				seqData.setOrgAddress(orgAddress);

			}


			seqData.setOptNotify(form.isOptNotify());
			seqData.setIsFederalOrganization(form.isGovFederalOrg());

			/* Shipping address details */

			if (form.getShippingAddressForm() != null)
			{

				final List<SeqirusCustomerShippingAddressData> seqirusCustomerShippingAddress = new ArrayList<>();

				final List<ShippingAddressForm> shipAddForm = form.getShippingAddressForm();

				for (final ShippingAddressForm shippingAddressForm : shipAddForm)
				{
					if (null != shippingAddressForm.getBillingOrgName() && !shippingAddressForm.getBillingOrgName().isEmpty())
					{
						LOGGER.info("ShipTo ID for " + form.getEmail() + ":" + shippingAddressForm.getAddressId());
						final SeqirusCustomerShippingAddressData shippinAddressData = new SeqirusCustomerShippingAddressData();

						if (StringUtils.isNotEmpty(shippingAddressForm.getAddressId()))
						{
							shippinAddressData.setAddressId(shippingAddressForm.getAddressId());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getBillingOrgName()))
						{
							shippinAddressData.setShippingOrganizationName(shippingAddressForm.getBillingOrgName());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLine1()))
						{
							shippinAddressData.setShippingAddressLine1(shippingAddressForm.getLine1());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLine2()))
						{
							shippinAddressData.setShippingAddressLine2(shippingAddressForm.getLine2());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getCity()))
						{
							shippinAddressData.setTown(shippingAddressForm.getCity());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getBillingState()))
						{
							shippinAddressData.setShippingState(shippingAddressForm.getBillingState());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getPostalCode()))
						{
							shippinAddressData.setPostalCode(shippingAddressForm.getPostalCode());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getPhone()))
						{
							shippinAddressData.setPhone(shippingAddressForm.getPhone());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getPhoneExt()))
						{
							shippinAddressData.setPhoneExt(shippingAddressForm.getPhoneExt());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getOperationsHours()))
						{
							shippinAddressData.setHoursOfOperation(shippingAddressForm.getOperationsHours());
						}

						//Shipping Contact Details
						if (StringUtils.isNotEmpty(shippingAddressForm.getFirstName()))
						{
							shippinAddressData.setFirstName(shippingAddressForm.getFirstName());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLastName()))
						{
							shippinAddressData.setLastName(shippingAddressForm.getLastName());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getEmail()))
						{
							shippinAddressData.setEmail(shippingAddressForm.getEmail());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getJobTitle()))
						{
							shippinAddressData.setShippingPosition(shippingAddressForm.getJobTitle());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getContactPhone()))
						{
							shippinAddressData.setShipToContactPhone(shippingAddressForm.getContactPhone());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getContactPhoneExt()))
						{
							shippinAddressData.setShipToContactPhoneExt(shippingAddressForm.getContactPhoneExt());
						}
						if (ArrayUtils.isNotEmpty(shippingAddressForm.getNonDeliveryDays()))
						{
							shippinAddressData.setNonDeliveryDays(Arrays.asList(shippingAddressForm.getNonDeliveryDays()));
						}

						final CustomerRegistrationLicenseDetailData customerLicData = new CustomerRegistrationLicenseDetailData();
						//customerLicData.setIsFederalOrganization(form.isGovFederalOrgCheck());

						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getLicenseName()))
						{
							customerLicData.setLicenseName(shippingAddressForm.getLicenseDetailsForm().getLicenseName());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getLicenseNum()))
						{
							customerLicData.setLicenseStateNumber(shippingAddressForm.getLicenseDetailsForm().getLicenseNum());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getLicenseExpiryDate()))
						{
							customerLicData.setLicexpiryDate(shippingAddressForm.getLicenseDetailsForm().getLicenseExpiryDate());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getLicAddressLine1()))
						{
							customerLicData.setLicenceAddressLine1(shippingAddressForm.getLicenseDetailsForm().getLicAddressLine1());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getLicAddressLine2()))
						{
							customerLicData.setLicenceAddressLine2(shippingAddressForm.getLicenseDetailsForm().getLicAddressLine2());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getLicenseCity()))
						{
							customerLicData.setCity(shippingAddressForm.getLicenseDetailsForm().getLicenseCity());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getLicenseState()))
						{
							customerLicData.setTerritory(shippingAddressForm.getLicenseDetailsForm().getLicenseState());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getZipcode()))
						{
							customerLicData.setPostalCode(shippingAddressForm.getLicenseDetailsForm().getZipcode());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getPhone()))
						{
							customerLicData.setPhoneNumber(shippingAddressForm.getLicenseDetailsForm().getPhone());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getPhoneExt()))
						{
							customerLicData.setPhoneExt(shippingAddressForm.getLicenseDetailsForm().getPhoneExt());
						}
						if (StringUtils.isNotEmpty(shippingAddressForm.getLicenseDetailsForm().getStateIssuingLicence()))
						{
							customerLicData.setStateIssuingLicence(shippingAddressForm.getLicenseDetailsForm().getStateIssuingLicence());
						}

						shippinAddressData.setLicDetils(customerLicData);


						seqirusCustomerShippingAddress.add(shippinAddressData);
					}
				}
				seqData.setShippingDetailsList(seqirusCustomerShippingAddress);
			}

			/* Billing address details */

			if (form.getBillAndShippAddressForm() != null)
			{
				final CustomerRegistrationAddressData billingAddressData = new CustomerRegistrationAddressData();

				if (form.getBillAndShippAddressForm().getAddressId() != null)
				{
					billingAddressData.setAddressId(form.getBillAndShippAddressForm().getAddressId());
				}

				if (form.getBillAndShippAddressForm().getEmail() != null)
				{
					billingAddressData.setEmailId(form.getBillAndShippAddressForm().getEmail().toLowerCase());
				}

				if (form.getBillAndShippAddressForm().getBillingOrgName() != null)
				{
					billingAddressData.setOrganizationName(form.getBillAndShippAddressForm().getBillingOrgName());
				}
				if (form.getBillAndShippAddressForm().getLine1() != null)
				{
					billingAddressData.setLine1(form.getBillAndShippAddressForm().getLine1());
				}
				if (form.getBillAndShippAddressForm().getLine2() != null)
				{
					billingAddressData.setLine2(form.getBillAndShippAddressForm().getLine2());
				}
				if (form.getBillAndShippAddressForm().getCity() != null)
				{
					billingAddressData.setTown(form.getBillAndShippAddressForm().getCity());
				}
				if (form.getBillAndShippAddressForm().getBillingState() != null)
				{
					billingAddressData.setState(form.getBillAndShippAddressForm().getBillingState());
				}
				if (form.getBillAndShippAddressForm().getPostalCode() != null)
				{
					billingAddressData.setPostalCode(form.getBillAndShippAddressForm().getPostalCode());
				}
				if (form.getBillAndShippAddressForm().getPhone() != null)
				{
					billingAddressData.setPhone(form.getBillAndShippAddressForm().getPhone());
				}
				if (form.getBillAndShippAddressForm().getPhoneExt() != null)
				{
					billingAddressData.setPhoneExt(form.getBillAndShippAddressForm().getPhoneExt());
				}
				if (form.getBillAndShippAddressForm().getFirstName() != null)
				{
					billingAddressData.setFirstName(form.getBillAndShippAddressForm().getFirstName());
				}
				if (form.getBillAndShippAddressForm().getLastName() != null)
				{
					billingAddressData.setLastName(form.getBillAndShippAddressForm().getLastName());
				}
				if (form.getBillAndShippAddressForm().getEmail() != null)
				{
					billingAddressData.setEmailId(form.getBillAndShippAddressForm().getEmail().toLowerCase());
				}
				if (StringUtils.isNotBlank(form.getBillAndShippAddressForm().getInvoiceEmail()))
				{
					billingAddressData.setInvoiceEmailId(form.getBillAndShippAddressForm().getInvoiceEmail().toLowerCase());
					billingAddressData.setOptNotifyInvoiceEmail(Boolean.TRUE);
				}
				else
				{
					billingAddressData.setOptNotifyInvoiceEmail(Boolean.FALSE);
				}
				if (StringUtils.isNotBlank(form.getBillAndShippAddressForm().getAcctStmtEmail()))
				{
					billingAddressData.setAcctStmtEmailId(form.getBillAndShippAddressForm().getAcctStmtEmail().toLowerCase());
					billingAddressData.setOptNotifyAcctStmtEmail(Boolean.TRUE);
				}
				else
				{
					billingAddressData.setOptNotifyAcctStmtEmail(Boolean.FALSE);
				}

				if (form.getBillAndShippAddressForm().getJobTitle() != null)
				{
					billingAddressData.setPosition(form.getBillAndShippAddressForm().getJobTitle());
				}
				if (form.getBillAndShippAddressForm().getAltEmail() != null)
				{
					billingAddressData.setAltEmail(form.getBillAndShippAddressForm().getAltEmail());
				}
				seqData.setBillingAddress(billingAddressData);

			}

			/* Payer Address Starts */

			if (form.getPayerAddressForm() != null)
			{
				final CustomerRegistrationAddressData payerAddressData = new CustomerRegistrationAddressData();

				if (form.getPayerAddressForm().getAddressId() != null)
				{
					payerAddressData.setAddressId(form.getPayerAddressForm().getAddressId());
				}

				if (form.getPayerAddressForm().getEmail() != null)
				{
					payerAddressData.setEmailId(form.getPayerAddressForm().getEmail().toLowerCase());
				}

				if (form.getPayerAddressForm().getBillingOrgName() != null)
				{
					payerAddressData.setOrganizationName(form.getPayerAddressForm().getBillingOrgName());
				}
				if (form.getPayerAddressForm().getLine1() != null)
				{
					payerAddressData.setLine1(form.getPayerAddressForm().getLine1());
				}
				if (form.getPayerAddressForm().getLine2() != null)
				{
					payerAddressData.setLine2(form.getPayerAddressForm().getLine2());
				}
				if (form.getPayerAddressForm().getCity() != null)
				{
					payerAddressData.setTown(form.getPayerAddressForm().getCity());
				}
				if (form.getPayerAddressForm().getBillingState() != null)
				{
					payerAddressData.setState(form.getPayerAddressForm().getBillingState());
				}

				if (form.getPayerAddressForm().getPostalCode() != null)
				{
					payerAddressData.setPostalCode(form.getPayerAddressForm().getPostalCode());
				}
				if (form.getPayerAddressForm().getPhone() != null)
				{
					payerAddressData.setPhone(form.getPayerAddressForm().getPhone());
				}

				if (form.getPayerAddressForm().getPhoneExt() != null)
				{
					payerAddressData.setPhoneExt(form.getPayerAddressForm().getPhoneExt());
				}

				if (form.getPayerAddressForm().getFirstName() != null)
				{
					payerAddressData.setFirstName(form.getPayerAddressForm().getFirstName());
				}
				if (form.getPayerAddressForm().getLastName() != null)
				{
					payerAddressData.setLastName(form.getPayerAddressForm().getLastName());
				}
				if (form.getPayerAddressForm().getEmail() != null)
				{
					payerAddressData.setEmailId(form.getPayerAddressForm().getEmail().toLowerCase());
				}
				if (form.getPayerAddressForm().getJobTitle() != null)
				{
					payerAddressData.setPosition(form.getPayerAddressForm().getJobTitle());
				}
				if (form.getPayerAddressForm().getDuns() != null)
				{
					payerAddressData.setDuns(form.getPayerAddressForm().getDuns());
				}
				if (form.getPayerAddressForm().getAltEmail() != null)
				{
					payerAddressData.setAltEmail(form.getPayerAddressForm().getAltEmail());
				}
				seqData.setPayerAddress(payerAddressData);
			}

			/* Payer Address Ends */

		}
		return seqData;
	}
}

