/**
 *
 */
package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.consent.data.ConsentData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seqirus.constants.Seqirusb2bworkflowConstants;
import com.seqirus.core.dataObjects.SeqirusMyLocListData;
import com.seqirus.core.dataObjects.SeqirusMyLocationsData;
import com.seqirus.core.enums.AccountStatusEnum;
import com.seqirus.core.enums.BusinessTypeEnum;
import com.seqirus.core.enums.CompanyTypeEnum;
import com.seqirus.core.exceptions.SeqirusCustomException;
import com.seqirus.data.B2BRegistrationData;
import com.seqirus.exceptions.CustomerAlreadyExistsException;
import com.seqirus.facades.B2BRegistrationFacade;
import com.seqirus.facades.customer.SeqirusB2BCustomerFacade;
import com.seqirus.facades.customer.SeqirusCustomerRegistrationFacade;
import com.seqirus.facades.cutomer.data.SeqirusCustomerRegistrationAddressData;
import com.seqirus.facades.cutomer.data.SeqirusCustomerRegistrationData;
import com.seqirus.storefront.controllers.ControllerConstants;
import com.seqirus.storefront.forms.CustomerRegistrationForm;
import com.seqirus.storefront.forms.JoinAccountForm;
import com.seqirus.storefront.util.SeqirusCustomerRequestDataProcessor;


/**
 * Register Controller for Seqirus. Handles register for create account flow.
 */
@Controller
public class SeqirusCustomerController extends AbstractSearchPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(SeqirusCustomerController.class);

	private static final String CREATE_ACCOUNT_PAGE = "createAccount";
	private static final String USER_DASHBOARD_PAGE = "userDashboard";
	private static final String PRODUCTS_RESOUCES_PAGE = "productResources";
	private static final String USER_PROFILE_PAGE = "userProfile";
	private static final String USER_SUPPORT_PAGE = "userSupportLogged";
	private static final String USER_JOINACCOUNT_PAGE = "joinAccount";
	private static final String USER_JOINACCOUNTAPPROVAL_PAGE = "joinAccountApproval";
	private static final String B2B_UNIT = "b2bUnit";
	private static final String PORTAL_AGREEMENT_PAGE = "portalAgreement";
	private static final String USER_RESOURCELANDING_PAGE = "resourceLanding";
	private static final String USER_FAQS_PAGE = "faqs";

	@Resource(name = "seqirusCustomerRegistrationFacade")
	private SeqirusCustomerRegistrationFacade seqirusCustomerRegistrationFacade;

	/** enumeration service bean */
	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "userService")
	UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/** The seqirus customer facade. */
	@Resource(name = "seqirusCustomerFacade")
	private SeqirusB2BCustomerFacade seqirusCustomerFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Autowired
	private B2BRegistrationFacade b2bRegistrationFacade;


	/**
	 * @param custRegistrationForm
	 * @param model
	 * @param bindingResult
	 * @param request
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/businessDetails", method = RequestMethod.GET)
	@RequireHardLogIn
	public String register(@ModelAttribute
	final CustomerRegistrationForm custRegistrationForm, final Model model, final BindingResult bindingResult,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final UserModel currentUser = userService.getCurrentUser();
		if (currentUser != null && !userService.isAnonymousUser(currentUser))
		{
			final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
			if (null != customer)
			{
				customer.setStatus(AccountStatusEnum.CR_PENDING);
				modelService.save(customer);
				modelService.refresh(customer);
				model.addAttribute("userId", customer.getUid());
				model.addAttribute("userName", customer.getName());
				model.addAttribute("jobTitle", customer.getJobTitle());
				model.addAttribute("phoneNum", customer.getPhone());
			}
		}
		final CustomerRegistrationForm customerRegistrationForm = new CustomerRegistrationForm();
		model.addAttribute("customerRegistrationForm", customerRegistrationForm);
		model.addAttribute("businessTypes", enumerationService.getEnumerationValues(BusinessTypeEnum.class));
		model.addAttribute("companyTypes", enumerationService.getEnumerationValues(CompanyTypeEnum.class));
		model.addAttribute("tradingSinceYears", getTradingSinceYears());
		storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.REGISTER_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.REGISTER_CMS_PAGE));
		return getViewForPage(model);
	}


	/**
	 * @param customerRegistrationForm
	 * @return String
	 */
	@PostMapping(value = "/createuser")
	public String doRegister(@ModelAttribute("customerRegistrationForm")
	final CustomerRegistrationForm customerRegistrationForm)
	{
		String view=null;
		try
		{
			final SeqirusCustomerRequestDataProcessor seqirusCustomerRequestDataProcessor = new SeqirusCustomerRequestDataProcessor();
			seqirusCustomerRegistrationFacade
					.register(seqirusCustomerRequestDataProcessor.populateFormData(customerRegistrationForm));
			view= REDIRECT_PREFIX + "/dashboard";
			final UserModel currentUser = userService.getCurrentUser();
			if (currentUser != null && !userService.isAnonymousUser(currentUser))
			{
				final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
				customer.setStatus(AccountStatusEnum.CR_COMPLETE);
				modelService.save(customer);
				modelService.refresh(customer);
			}
		}
		catch (final SeqirusCustomException e)
		{
			LOG.error("ErrorDuringRegistration : ", e);
		}
		return view;
	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String createAccount(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(CREATE_ACCOUNT_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CREATE_ACCOUNT_PAGE));
		return ControllerConstants.Views.Pages.Registration.CREATE_ACCOUNT_CMS_PAGE;
	}

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	@RequireHardLogIn
	public String userDashboard(final Model model) throws CMSItemNotFoundException
	{
		String status = "";
		final UserModel currentUser = userService.getCurrentUser();
		final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
		if (null != customer)
		{
			model.addAttribute("userName", customer.getName());
			status = customer.getStatus().getCode();
		}
		LOG.info("Registration status of " + customer.getUid() + " : " + status);
		if (StringUtils.equalsIgnoreCase(AccountStatusEnum.CR_PENDING.getCode(), status))
		{
			return REDIRECT_PREFIX + "/businessDetails";
		}
		else if (StringUtils.equalsIgnoreCase(AccountStatusEnum.JA_PENDING.getCode(), status))
		{
			return REDIRECT_PREFIX + "/joinaccount";
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(USER_DASHBOARD_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(USER_DASHBOARD_PAGE));
		return ControllerConstants.Views.Pages.Registration.USER_DASHBOARD_CMS_PAGE;
	}

	@RequestMapping(value = "/getRedirectUrl", method = RequestMethod.GET)
	public String getRedirectUrlByRegStatus()
	{
		String status = "";
		String pageUrl = "";
		final UserModel currentUser = userService.getCurrentUser();
		final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
		if (null != customer)
		{
			status = customer.getStatus().getCode();
		}
		LOG.info("Registration status of " + customer.getUid() + " : " + status);
		if (StringUtils.equalsIgnoreCase(AccountStatusEnum.CR_PENDING.getCode(), status))
		{
			pageUrl = "businessDetails";
		}
		else if (StringUtils.equalsIgnoreCase(AccountStatusEnum.JA_PENDING.getCode(), status))
		{
			pageUrl = "joinaccount";
		}
		return pageUrl;
	}

	@RequestMapping(value = "/productresources", method = RequestMethod.GET)
	public String userProducts(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(PRODUCTS_RESOUCES_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PRODUCTS_RESOUCES_PAGE));
		return ControllerConstants.Views.Pages.Registration.PRODUCT_RESOURCES_CMS_PAGE;


	}

	@RequestMapping(value = "/portalagreement", method = RequestMethod.GET)
	@RequireHardLogIn
	public String portalAgreement(final Model model) throws CMSItemNotFoundException
	{

		final UserModel currentUser = userService.getCurrentUser();
		if (currentUser != null && !userService.isAnonymousUser(currentUser))
		{
			final B2BCustomerModel customer = (B2BCustomerModel) currentUser;

			final List<ConsentData> constData = seqirusCustomerRegistrationFacade.getConsentsForCustomer(customer);
			model.addAttribute("language", commonI18NService.getCurrentLanguage());
			model.addAttribute("constData", constData);
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(PORTAL_AGREEMENT_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PORTAL_AGREEMENT_PAGE));
		return ControllerConstants.Views.Pages.Registration.PORTAL_AGREEMENT_CMS_PAGE;


	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String userProfile(final Model model) throws CMSItemNotFoundException
	{

		final UserModel currentUser = userService.getCurrentUser();
		if (currentUser != null && !userService.isAnonymousUser(currentUser))
		{
			final B2BCustomerModel customer = (B2BCustomerModel) currentUser;

			model.addAttribute("userName", customer.getName());

			final SeqirusCustomerRegistrationData customerData = seqirusCustomerRegistrationFacade.fetchCustData(customer, false);
			model.addAttribute("customerData", customerData);

			model.addAttribute("customerProfileData", seqirusCustomerFacade.getCustomerProfileData(customer));


			int percentageCalculation = 0;
			if (null != customer.getDefaultB2BUnit())
			{

				if (null != customer.getDefaultB2BUnit().getAddresses())
				{
				for (final AddressModel address : customer.getDefaultB2BUnit().getAddresses())
				{
					if (address.getSapCustomerType().contains("PY"))
					{
						percentageCalculation = percentageCalculation + 33;

					}
					if (address.getSapCustomerType().contains("SH"))
					{
						percentageCalculation = percentageCalculation + 33;

					}
					if (address.getSapCustomerType().contains("BP"))
					{
						percentageCalculation = percentageCalculation + 33;

					}
				}
			}


				model.addAttribute("soldToAcc", customer.getDefaultB2BUnit().getUid());
				model.addAttribute("soldToPh", customer.getDefaultB2BUnit().getPhoneNumber());
				model.addAttribute("soldToExt", customer.getDefaultB2BUnit().getPhoneExt());
				model.addAttribute("soldToEmail", customer.getDefaultB2BUnit().getEmail());

			}
			else{
			if (customerData != null && null != customerData.getPayingContactData())
			{
				percentageCalculation = percentageCalculation + 33;

			}
			if (customerData != null && null != customerData.getInvoiceContractData())
			{
				percentageCalculation = percentageCalculation + 33;

			}
			if (customerData != null && null != customerData.getShippingLocationsData())
			{
				percentageCalculation = percentageCalculation + 33;

			}
			}
			model.addAttribute("percentageCalculation", percentageCalculation);

		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(USER_PROFILE_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(USER_PROFILE_PAGE));
		return ControllerConstants.Views.Pages.Registration.USER_PROFILE_CMS_PAGE;

	}


	@RequestMapping(value = "/joinaccount", method = RequestMethod.GET)
	@RequireHardLogIn
	public String userJoinAccount(final Model model) throws CMSItemNotFoundException
	{
		final UserModel currentUser = userService.getCurrentUser();
		if (currentUser != null && !userService.isAnonymousUser(currentUser))
		{
			final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
			if (null != customer)
			{
				customer.setStatus(AccountStatusEnum.JA_PENDING);
				modelService.save(customer);
				modelService.refresh(customer);
			}
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(USER_JOINACCOUNT_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(USER_JOINACCOUNT_PAGE));
		return ControllerConstants.Views.Pages.Registration.USER_JOINACCOUNT_CMS_PAGE;
	}

	@RequestMapping(value = "/joinaccountapproval", method = RequestMethod.GET)
	@RequireHardLogIn
	public String userJoinAccountApproval(final Model model) throws CMSItemNotFoundException
	{
		final UserModel currentUser = userService.getCurrentUser();
		if (currentUser != null && !userService.isAnonymousUser(currentUser))
		{
			final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
			if (null != customer)
			{
				customer.setStatus(AccountStatusEnum.JA_PENDING_APPROVAL);
				modelService.save(customer);
				modelService.refresh(customer);
			}
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(USER_JOINACCOUNTAPPROVAL_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(USER_JOINACCOUNTAPPROVAL_PAGE));
		return ControllerConstants.Views.Pages.Registration.USER_JOINACCOUNTAPPROVAL_CMS_PAGE;
	}

	@RequestMapping(value = "/resourcelanding", method = RequestMethod.GET)
	@RequireHardLogIn
	public String userResourceLandingApproval(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(USER_RESOURCELANDING_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(USER_RESOURCELANDING_PAGE));
		return ControllerConstants.Views.Pages.Registration.USER_RESOURCELANDING_CMS_PAGE;


	}

	@RequestMapping(value = "/faqs", method = RequestMethod.GET)
	@RequireHardLogIn
	public String userFaq(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(USER_FAQS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(USER_FAQS_PAGE));
		return ControllerConstants.Views.Pages.Registration.USER_FAQS_CMS_PAGE;


	}
	/*
	 * @RequestMapping(value = "/supportlogged", method = RequestMethod.GET) public String userSupportLogged(final Model
	 * model) throws CMSItemNotFoundException {
	 *
	 * storeCmsPageInModel(model, getContentPageForLabelOrId(USER_SUPPORT_PAGE)); setUpMetaDataForContentPage(model,
	 * getContentPageForLabelOrId(USER_SUPPORT_PAGE)); return
	 * ControllerConstants.Views.Pages.Registration.USER_SUPPORT_CMS_PAGE;
	 *
	 *
	 * }
	 */

	@RequestMapping(value = "/searchAccount", method = RequestMethod.GET)
	@ResponseBody
	public String searchAccount(@RequestParam(value = "account", required = true)
	final String account, @RequestParam(value = "postcode", required = false)
	final String postcode, @RequestParam(value = "accesscodeflag", required = true)
	final String accesscodeflag, final Model model) throws CMSItemNotFoundException

	{
		String formattedAccnt = "";
		if (account.length() == 8 || account.length() == 9)
		{

			formattedAccnt = "0000000000".substring(account.length())
					+ account.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\\-", "").replaceAll("\\s", "");
		}
		else
		{
			formattedAccnt = account.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\\-", "").replaceAll("\\s", "");

		}
		String formattedPostCode = "";
		if (StringUtils.isNotBlank(postcode))
		{
			formattedPostCode = postcode.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\\-", "");
		}
		LOG.info("searchAccount : formatted act is " + formattedAccnt + ": Postcode : " + formattedPostCode);

		String existingAccount = null;
		final B2BUnitModel b2bUnit = seqirusCustomerRegistrationFacade.getOrgNameByAccount(formattedAccnt);
		if (null != b2bUnit)
		{
			sessionService.setAttribute(B2B_UNIT, b2bUnit);
			final String b2bUnitOrgName = b2bUnit.getName();
			LOG.info("searchAccount : Org Name from B2B Unit " + b2bUnitOrgName);
			if (StringUtils.equalsIgnoreCase(formattedPostCode, getPostCodeOfAccount(b2bUnit))
					&& (StringUtils.equalsIgnoreCase(formattedAccnt, b2bUnit.getUid()))
					&& StringUtils.equalsIgnoreCase("false", accesscodeflag))
			{
				existingAccount = b2bUnitOrgName;

			}
			else if ((StringUtils.isBlank(postcode))
					|| (StringUtils.isNotBlank(formattedPostCode) && StringUtils.equalsIgnoreCase(formattedAccnt, b2bUnit.getUid()))
							&& StringUtils.equalsIgnoreCase("true", accesscodeflag))
			{
				existingAccount = b2bUnitOrgName;

			}
		}
		return existingAccount;
	}


	/**
	 * Find PostCode of SAP Account of Customer
	 *
	 * @param b2bUnit
	 * @return
	 */
	private String getPostCodeOfAccount(final B2BUnitModel b2bUnit)
	{
		final Collection<AddressModel> addresses = b2bUnit.getAddresses();
		if (CollectionUtils.isNotEmpty(addresses))
		{
			for (final AddressModel address : addresses)
			{
				if (address.getContactAddress())
				{
					return address.getPostalcode();
				}
			}
		}
		return "";
	}

	@RequestMapping(value = "/joinActRegister", method = RequestMethod.POST)
	@ResponseBody
	public String joinActRegister(@RequestBody
	final JoinAccountForm joinAccountForm, final Model model, final BindingResult bindingResult, final HttpServletRequest request)
			throws CMSItemNotFoundException
	{
		final UserModel currentUser = userService.getCurrentUser();
		B2BCustomerModel customer = null;
		if (currentUser != null && !userService.isAnonymousUser(currentUser))
		{
			customer = (B2BCustomerModel) currentUser;
		}
		final String account = joinAccountForm.getAccountNumber();
		final String accesscode = joinAccountForm.getAccessCode();
		final String searchStatus = joinAccountForm.getSearchStatus();
		LOG.info("searchStatus obtained on the click of search btn " + searchStatus);
		if (StringUtils.equalsIgnoreCase("success", searchStatus) && null != customer)
		{
			LOG.info("uid " + customer.getUid());
			LOG.info("name " + customer.getName());
			LOG.info("before setting-search1" + customer.getDefaultB2BUnit().getUid());
			final B2BUnitModel b2bUnit = (B2BUnitModel) sessionService.getAttribute(B2B_UNIT);
			customer.setDefaultB2BUnit(b2bUnit);
			customer.setStatus(AccountStatusEnum.JA_COMPLETE);
			customer.setApproved(Boolean.TRUE);
			modelService.save(customer);
			modelService.refresh(customer);
			LOG.info("after setting-search1 " + customer.getDefaultB2BUnit().getUid());
			seqirusCustomerRegistrationFacade.sendEmail(currentUser);
		}
		else if (StringUtils.equalsIgnoreCase("failure", searchStatus))
		{
			LOG.info(" searchstatus inside else block  " + searchStatus);
			String formattedAccnt = "";
			String formattedAccessCode = "";
			if (StringUtils.isNotBlank(account))
			{
				formattedAccnt = account.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\\-", "").replaceAll("\\s", "");
			}
			if (StringUtils.isNotBlank(accesscode))
			{
				formattedAccessCode = accesscode.trim();
			}
			final B2BRegistrationData data = new B2BRegistrationData();
			data.setAccountNumber(formattedAccnt);
			//data.setReferralEmail(joinAccountForm.getOrgReferralEmail());
			data.setCompanyName(joinAccountForm.getOrgName());
			data.setCompanyAddressPostalCode(joinAccountForm.getZipCode());
			data.setAccessCode(formattedAccessCode);
			data.setWorkflowName(Seqirusb2bworkflowConstants.Workflows.JOIN_ACCOUNT_WORKFLOW);
			try
			{
				b2bRegistrationFacade.register(data);
			}
			catch (final CustomerAlreadyExistsException ce)
			{
				ce.printStackTrace();
			}
		}
		return searchStatus;
	}
	/*
	 * if (request.getParameter("accountnumber") != null) {
	 *
	 * final B2BUnitModel orgName = seqirusCustomerRegistrationFacade
	 * .getOrgNameByAccount(joinAccountForm.getAccountnumber()); customer.setDefaultB2BUnit(orgName); }
	 *
	 * else { final B2BUnitModel orgNameByAccess = seqirusCustomerRegistrationFacade
	 * .getOrgNameByAccount(joinAccountForm.getSapAccessCode()); customer.setDefaultB2BUnit(orgNameByAccess); }
	 *
	 */




	/*
	 * final SeqirusB2BTempCustomerModel customerTempObj = modelService.create(SeqirusB2BTempCustomerModel.class);
	 * customerTempObj.setUserId(customer.getUid()); customerTempObj.setUserName(customer.getDisplayName());
	 * customerTempObj.setCompanyInfo(customer.); customerTempObj.setBillingAddress(customer.get);
	 * customerTempObj.setPayingAddress(customer.get); customerTempObj.setInvoiceAddress(value);
	 * customerTempObj.setShippingAddress(value);
	 *
	 *
	 * modelService.save(customerTempObj); modelService.refresh(customerTempObj);
	 */

	/*
	 * final String accesscode = null; if (accesscode != null) {
	 *
	 * final B2BUnitModel orgName =
	 * seqirusCustomerRegistrationFacade.getOrgNameByAccount(custRegistrationForm.getAccountnumber());
	 * customer.setDefaultB2BUnit(orgName); } else { final B2BUnitModel orgNameByAccess =
	 * seqirusCustomerRegistrationFacade .getOrgNameByAccount(custRegistrationForm.getSapAccessCode());
	 * customer.setDefaultB2BUnit(orgNameByAccess); }
	 */





	//final SeqirusCustomerRequestDataProcessor seqirusCustomerRequestDataProcessor = new SeqirusCustomerRequestDataProcessor();
	//seqirusCustomerRegistrationFacade.register(seqirusCustomerRequestDataProcessor.populateFormData(customerRegistrationForm));







	/**
	 * @return List<String>
	 */
	private List<String> getTradingSinceYears()
	{
		final SimpleDateFormat format = new SimpleDateFormat("yyyy");
		final List<String> yearsList = new ArrayList<>();
		for (int i = 0; i < 12; i++)
		{
			final Calendar calendarEnd = Calendar.getInstance();
			calendarEnd.add(Calendar.YEAR, -i);
			yearsList.add(format.format(calendarEnd.getTime()));
		}
		return yearsList;
	}

	@RequestMapping(value = "/getMyLocTableData", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public SeqirusMyLocListData getMyLocTableData(final HttpServletRequest request, final Model model, final HttpSession session)
	{

		final SeqirusMyLocListData locListData = new SeqirusMyLocListData();

		final UserModel currentUser = userService.getCurrentUser();
		if (currentUser != null && !userService.isAnonymousUser(currentUser))
		{
			final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
			final B2BUnitModel b2bUnit = customer.getDefaultB2BUnit();

			final SeqirusCustomerRegistrationData customerData = seqirusCustomerRegistrationFacade.fetchCustData(customer, true);
			if (null != customerData)
			{
				final List<SeqirusMyLocationsData> tableDataList = new ArrayList<SeqirusMyLocationsData>();

				if (null != customerData.getPayingContactData()
						&& (customerData.getPayingContactData().getStatus().equalsIgnoreCase("NEW")
								|| customerData.getPayingContactData().getStatus().equalsIgnoreCase("CHANGE")))
				{
					final SeqirusMyLocationsData locationDataPaying = new SeqirusMyLocationsData();
					locationDataPaying.setAddressId(customerData.getPayingContactData().getAddressID());
					locationDataPaying.setName(customerData.getPayingContactData().getCompanyName());
					locationDataPaying.setStatus("Processing");
					locationDataPaying.setType("Paying");
					locationDataPaying.setAddress(customerData.getPayingContactData().getFirstName() + " "
							+ customerData.getPayingContactData().getLastName() + ", " + customerData.getPayingContactData().getEmail());
					tableDataList.add(locationDataPaying);
				}
				else
				{
					if (null != b2bUnit.getAddresses())
					{
						for (final AddressModel address : b2bUnit.getAddresses())
						{
							if (address.getSapCustomerType().contains("PY")
									&& address.getAddressId().equals(customerData.getPayingContactData().getAddressID()))
							{
								final SeqirusMyLocationsData locationDataPaying = new SeqirusMyLocationsData();
								locationDataPaying.setAddressId(address.getAddressId());
								locationDataPaying.setName(address.getCompany());
								locationDataPaying.setStatus("Confirmed");
								locationDataPaying.setType("Paying");
								locationDataPaying
										.setAddress(address.getFirstname() + " " + address.getLastname() + ", " + address.getEmail());
								tableDataList.add(locationDataPaying);
							}
						}
					}
				}
				if (null != customerData.getInvoiceContractData()
						&& (customerData.getInvoiceContractData().getStatus().equalsIgnoreCase("NEW")
								|| customerData.getInvoiceContractData().getStatus().equalsIgnoreCase("CHANGE")))
				{
					final SeqirusMyLocationsData locationDataInvoicing = new SeqirusMyLocationsData();
					locationDataInvoicing.setAddressId(customerData.getInvoiceContractData().getAddressID());
					locationDataInvoicing.setName(customerData.getInvoiceContractData().getCompanyName());
					locationDataInvoicing.setStatus("Processing");
					locationDataInvoicing.setType("Invoicing");
					locationDataInvoicing.setAddress(customerData.getInvoiceContractData().getFirstName() + " "
							+ customerData.getInvoiceContractData().getLastName() + ", "
							+ customerData.getInvoiceContractData().getEmail());
					tableDataList.add(locationDataInvoicing);
				}
				else
				{
					if (null != b2bUnit.getAddresses())
					{
						for (final AddressModel address : b2bUnit.getAddresses())
						{
							if (address.getSapCustomerType().contains("BP")
									&& address.getAddressId().equals(customerData.getInvoiceContractData().getAddressID()))
							{
								final SeqirusMyLocationsData locationDataInvoicing = new SeqirusMyLocationsData();
								locationDataInvoicing.setAddressId(address.getAddressId());
								locationDataInvoicing.setName(address.getCompany());
								locationDataInvoicing.setStatus("Confirmed");
								locationDataInvoicing.setType("Invoicing");
								locationDataInvoicing
										.setAddress(address.getFirstname() + ", " + address.getLastname() + ", " + address.getEmail());
								tableDataList.add(locationDataInvoicing);
							}
						}
					}
				}
				if (null != customerData.getShippingLocationsData())
				{
					for (final SeqirusCustomerRegistrationAddressData shippingData : customerData.getShippingLocationsData())
					{
						if (shippingData.getStatus().equalsIgnoreCase("NEW") || shippingData.getStatus().equalsIgnoreCase("CHANGE"))
						{
							final SeqirusMyLocationsData locationDataShipping = new SeqirusMyLocationsData();
							locationDataShipping.setAddressId(shippingData.getAddressID());
							locationDataShipping.setName(shippingData.getCompanyName());
							locationDataShipping.setStatus("Processing");
							locationDataShipping.setType("Shipping");
							locationDataShipping.setAddress(shippingData.getLine1() + ", " + shippingData.getLine2() + ", "
									+ shippingData.getCity() + ", " + shippingData.getPostalCode());
							tableDataList.add(locationDataShipping);
						}
						else
						{
							if (null != b2bUnit.getAddresses())
							{
								for (final AddressModel shippingAddr : b2bUnit.getAddresses())
								{
									if (shippingAddr.getSapCustomerType().contains("SH")
											&& shippingData.getAddressID().equalsIgnoreCase(shippingAddr.getAddressId()))
									{
										final SeqirusMyLocationsData locationDataShipping = new SeqirusMyLocationsData();
										locationDataShipping.setAddressId(shippingAddr.getAddressId());
										locationDataShipping.setName(shippingAddr.getCompany());
										locationDataShipping.setStatus("Confirmed");
										locationDataShipping.setType("Shipping");
										locationDataShipping.setAddress(shippingAddr.getLine1() + ", " + shippingAddr.getLine2() + ", "
												+ shippingAddr.getTown() + ", " + shippingAddr.getPostalcode());
										tableDataList.add(locationDataShipping);
									}
								}
							}
						}
					}
				}
				locListData.setTableData(tableDataList);
				model.addAttribute("tableDataList", tableDataList);
				sessionService.setAttribute("tableDataList", customerData);
				int percentageCalculation = 0;
				for(final SeqirusMyLocationsData tableData : tableDataList) {

				if (tableData.getType().equalsIgnoreCase("Shipping"))
				{
					percentageCalculation = percentageCalculation + 33;

				}
				if (tableData.getType().equalsIgnoreCase("Paying"))
				{
					percentageCalculation = percentageCalculation + 33;

				}
				if (tableData.getType().equalsIgnoreCase("Invoicing"))
				{
					percentageCalculation = percentageCalculation + 33;

				}
				}
				model.addAttribute("percentageCalculation", percentageCalculation);

			}
		}
		return locListData;

	}

	@RequestMapping(value = "/getEditLocData", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public SeqirusCustomerRegistrationAddressData getChartDataFilter(@RequestParam("addressID")
	final String addressID, final HttpServletRequest request, final Model model, final HttpSession session)
	{
		final SeqirusCustomerRegistrationData tableDataList = sessionService.getAttribute("tableDataList");

		if (addressID.equalsIgnoreCase("Paying"))
		{

			return tableDataList.getPayingContactData();
		}
		if (addressID.equalsIgnoreCase("Invoicing"))
		{

			return tableDataList.getInvoiceContractData();
		}
		for (final SeqirusCustomerRegistrationAddressData shippingData : tableDataList.getShippingLocationsData())
		{
			if (addressID.equalsIgnoreCase("Shipping"))
			{

				return shippingData;
			}
		}

		return null;
	}

	@RequestMapping(value = "/update-profile", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public SeqirusCustomerRegistrationData updateProfileData(@ModelAttribute("customerRegistrationForm")
	final CustomerRegistrationForm customerRegistrationForm, final HttpServletRequest request, final Model model,
			final HttpSession session)
	{
		final SeqirusCustomerRequestDataProcessor seqirusCustomerRequestDataProcessor = new SeqirusCustomerRequestDataProcessor();
		final SeqirusCustomerRegistrationData custUpdatedData = seqirusCustomerRequestDataProcessor
				.populateFormData(customerRegistrationForm);
		seqirusCustomerRegistrationFacade.updateProfile(custUpdatedData);

		return custUpdatedData;
	}
}

