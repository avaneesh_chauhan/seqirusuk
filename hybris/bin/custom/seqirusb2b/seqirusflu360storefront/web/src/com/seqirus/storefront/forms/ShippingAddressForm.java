
package com.seqirus.storefront.forms;

/**
 * BillingAndShippingAddress are using Customer Registration process
 *
 */
public class ShippingAddressForm
{

	private String addressId;
	private boolean isGovtFederal;
	private String Line1;
	private String Line2;
	private String postalCode;
	private String city;
	private String billingState;
	private String billingOrgName;
	private String phone;
	private String phoneExt;
	private String groupOrganization;
	private String firstName;
	private String lastName;
	private String email;
	private String confEmail;
	private String jobTitle;
	private String operationsHours;
	private String[] nonDeliveryDays;
	private LicenseDetailsForm licenseDetailsForm;
	private String govtFed;
	private String sapCustomerId;
	private String contactPhone;
	private String contactPhoneExt;



	/**
	 * @return the govtFed
	 */
	public String getGovtFed()
	{
		return govtFed;
	}

	/**
	 * @param govtFed
	 *           the govtFed to set
	 */
	public void setGovtFed(final String govtFed)
	{
		this.govtFed = govtFed;
	}



	/**
	 * @return the addressId
	 */
	public String getAddressId()
	{
		return addressId;
	}

	/**
	 * @param addressId
	 *           the addressId to set
	 */
	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}

	/**
	 * @return the isGovtFederal
	 */
	public boolean isGovtFederal()
	{
		return isGovtFederal;
	}

	/**
	 * @param isGovtFederal
	 *           the isGovtFederal to set
	 */
	public void setGovtFederal(final boolean isGovtFederal)
	{
		this.isGovtFederal = isGovtFederal;
	}

	/**
	 * @return the line1
	 */
	public String getLine1()
	{
		return Line1;
	}

	/**
	 * @param line1
	 *           the line1 to set
	 */
	public void setLine1(final String line1)
	{
		Line1 = line1;
	}

	/**
	 * @return the line2
	 */
	public String getLine2()
	{
		return Line2;
	}

	/**
	 * @param line2
	 *           the line2 to set
	 */
	public void setLine2(final String line2)
	{
		Line2 = line2;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *           the postalCode to set
	 */
	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the billingOrgName
	 */
	public String getBillingOrgName()
	{
		return billingOrgName;
	}

	/**
	 * @param billingOrgName
	 *           the billingOrgName to set
	 */
	public void setBillingOrgName(final String billingOrgName)
	{
		this.billingOrgName = billingOrgName;
	}

	/**
	 * @return the billingState
	 */
	public String getBillingState()
	{
		return billingState;
	}

	/**
	 * @param billingState
	 *           the billingState to set
	 */
	public void setBillingState(final String billingState)
	{
		this.billingState = billingState;
	}

	/**
	 * @return the phone
	 */
	public String getPhone()
	{
		return phone;
	}

	/**
	 * @param phone
	 *           the phone to set
	 */
	public void setPhone(final String phone)
	{
		this.phone = phone;
	}

	/**
	 * @return the phoneExt
	 */
	public String getPhoneExt()
	{
		return phoneExt;
	}

	/**
	 * @param phoneExt
	 *           the phoneExt to set
	 */
	public void setPhoneExt(final String phoneExt)
	{
		this.phoneExt = phoneExt;
	}

	/**
	 * @return the groupOrganization
	 */
	public String getGroupOrganization()
	{
		return groupOrganization;
	}

	/**
	 * @param groupOrganization
	 *           the groupOrganization to set
	 */
	public void setGroupOrganization(final String groupOrganization)
	{
		this.groupOrganization = groupOrganization;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the jobTitle
	 */
	public String getJobTitle()
	{
		return jobTitle;
	}

	/**
	 * @param jobTitle
	 *           the jobTitle to set
	 */
	public void setJobTitle(final String jobTitle)
	{
		this.jobTitle = jobTitle;
	}



	/**
	 * @return the confEmail
	 */
	public String getConfEmail()
	{
		return confEmail;
	}

	/**
	 * @param confEmail
	 *           the confEmail to set
	 */
	public void setConfEmail(final String confEmail)
	{
		this.confEmail = confEmail;
	}

	/**
	 * @return the licenseDetailsForm
	 */
	public LicenseDetailsForm getLicenseDetailsForm()
	{
		return licenseDetailsForm;
	}

	/**
	 * @param licenseDetailsForm
	 *           the licenseDetailsForm to set
	 */
	public void setLicenseDetailsForm(final LicenseDetailsForm licenseDetailsForm)
	{
		this.licenseDetailsForm = licenseDetailsForm;
	}



	/**
	 * @return the sapCustomerId
	 */
	public String getSapCustomerId()
	{
		return sapCustomerId;
	}

	/**
	 * @param sapCustomerId
	 *           the sapCustomerId to set
	 */
	public void setSapCustomerId(final String sapCustomerId)
	{
		this.sapCustomerId = sapCustomerId;
	}

	/**
	 * @return the operationsHours
	 */
	public String getOperationsHours()
	{
		return operationsHours;
	}

	/**
	 * @param operationsHours
	 *           the operationsHours to set
	 */
	public void setOperationsHours(final String operationsHours)
	{
		this.operationsHours = operationsHours;
	}

	/**
	 * @return the nonDeliveryDays
	 */
	public String[] getNonDeliveryDays()
	{
		return nonDeliveryDays;
	}

	/**
	 * @param nonDeliveryDays
	 *           the nonDeliveryDays to set
	 */
	public void setNonDeliveryDays(final String[] nonDeliveryDays)
	{
		this.nonDeliveryDays = nonDeliveryDays;
	}

	/**
	 * @return the contactPhone
	 */
	public String getContactPhone()
	{
		return contactPhone;
	}

	/**
	 * @param contactPhone
	 *           the contactPhone to set
	 */
	public void setContactPhone(final String contactPhone)
	{
		this.contactPhone = contactPhone;
	}

	/**
	 * @return the contactPhoneExt
	 */
	public String getContactPhoneExt()
	{
		return contactPhoneExt;
	}

	/**
	 * @param contactPhoneExt
	 *           the contactPhoneExt to set
	 */
	public void setContactPhoneExt(final String contactPhoneExt)
	{
		this.contactPhoneExt = contactPhoneExt;
	}


}
