
package com.seqirus.storefront.forms;

/**
 * LicenseDetailsForm are using Customer Registration process
 *
 */


public class LicenseDetailsForm
{
	private String licenseName;
	private String licenseNum;
	private String licenseExpiryDate;
	private String licAddressLine1;
	private String stateIssuingLicence;
	private String shippingAddressLine1;
	private String licensePostalCode;
	private String licenseCity;
	private String licenseState;
	private String zipcode;
	private String shippingState;
	private String shippingOrgName;
	private String phone;
	private String phoneExt;
	/* private String operationsHours; */
	private String licAddressLine2;
	private String hoursOfOperation;
	private BillingAndShippingAddressForm billAndShippAddressForm;


	/**
	 * @return the licenseName
	 */
	public String getLicenseName()
	{
		return licenseName;
	}

	/**
	 * @param licenseName
	 *           the licenseName to set
	 */
	public void setLicenseName(final String licenseName)
	{
		this.licenseName = licenseName;
	}

	/**
	 * @return the licenseNum
	 */
	public String getLicenseNum()
	{
		return licenseNum;
	}

	/**
	 * @param licenseNum
	 *           the licenseNum to set
	 */
	public void setLicenseNum(final String licenseNum)
	{
		this.licenseNum = licenseNum;
	}

	/**
	 * @return the licensePostalCode
	 */
	public String getLicensePostalCode()
	{
		return licensePostalCode;
	}

	/**
	 * @param licensePostalCode
	 *           the licensePostalCode to set
	 */
	public void setLicensePostalCode(final String licensePostalCode)
	{
		this.licensePostalCode = licensePostalCode;
	}

	/**
	 * @return the licenseCity
	 */
	public String getLicenseCity()
	{
		return licenseCity;
	}

	/**
	 * @param licenseCity
	 *           the licenseCity to set
	 */
	public void setLicenseCity(final String licenseCity)
	{
		this.licenseCity = licenseCity;
	}

	/**
	 * @return the shippingOrgName
	 */
	public String getShippingOrgName()
	{
		return shippingOrgName;
	}

	/**
	 * @param shippingOrgName
	 *           the shippingOrgName to set
	 */
	public void setShippingOrgName(final String shippingOrgName)
	{
		this.shippingOrgName = shippingOrgName;
	}

	/**
	 * @return the phone
	 */
	public String getPhone()
	{
		return phone;
	}

	/**
	 * @param phone
	 *           the phone to set
	 */
	public void setPhone(final String phone)
	{
		this.phone = phone;
	}

	/**
	 * @return the phoneExt
	 */
	public String getPhoneExt()
	{
		return phoneExt;
	}

	/**
	 * @param phoneExt
	 *           the phoneExt to set
	 */
	public void setPhoneExt(final String phoneExt)
	{
		this.phoneExt = phoneExt;
	}

	/**
	 * @return the shippingState
	 */
	public String getShippingState()
	{
		return shippingState;
	}

	/**
	 * @param shippingState
	 *           the shippingState to set
	 */
	public void setShippingState(final String shippingState)
	{
		this.shippingState = shippingState;
	}

	/**
	 * @return the licenseExpiryDate
	 */
	public String getLicenseExpiryDate()
	{
		return licenseExpiryDate;
	}

	/**
	 * @param licenseExpiryDate
	 *           the licenseExpiryDate to set
	 */
	public void setLicenseExpiryDate(final String licenseExpiryDate)
	{
		this.licenseExpiryDate = licenseExpiryDate;
	}

	/**
	 * @return the licAddressLine1
	 */
	public String getLicAddressLine1()
	{
		return licAddressLine1;
	}

	/**
	 * @param licAddressLine1
	 *           the licAddressLine1 to set
	 */
	public void setLicAddressLine1(final String licAddressLine1)
	{
		this.licAddressLine1 = licAddressLine1;
	}

	/**
	 * @return the stateIssuingLicence
	 */
	public String getStateIssuingLicence()
	{
		return stateIssuingLicence;
	}

	/**
	 * @param stateIssuingLicence
	 *           the stateIssuingLicence to set
	 */
	public void setStateIssuingLicence(final String stateIssuingLicence)
	{
		this.stateIssuingLicence = stateIssuingLicence;
	}

	/**
	 * @return the shippingAddressLine1
	 */
	public String getShippingAddressLine1()
	{
		return shippingAddressLine1;
	}

	/**
	 * @param shippingAddressLine1
	 *           the shippingAddressLine1 to set
	 */
	public void setShippingAddressLine1(final String shippingAddressLine1)
	{
		this.shippingAddressLine1 = shippingAddressLine1;
	}

	/**
	 * @return the licAddressLine2
	 */
	public String getLicAddressLine2()
	{
		return licAddressLine2;
	}

	/**
	 * @param licAddressLine2
	 *           the licAddressLine2 to set
	 */
	public void setLicAddressLine2(final String licAddressLine2)
	{
		this.licAddressLine2 = licAddressLine2;
	}

	/**
	 * @return the hoursOfOperation
	 */
	public String getHoursOfOperation()
	{
		return hoursOfOperation;
	}

	/**
	 * @param hoursOfOperation
	 *           the hoursOfOperation to set
	 */
	public void setHoursOfOperation(final String hoursOfOperation)
	{
		this.hoursOfOperation = hoursOfOperation;
	}

	/**
	 * @return the billAndShippAddressForm
	 */
	public BillingAndShippingAddressForm getBillAndShippAddressForm()
	{
		return billAndShippAddressForm;
	}

	/**
	 * @param billAndShippAddressForm
	 *           the billAndShippAddressForm to set
	 */
	public void setBillAndShippAddressForm(final BillingAndShippingAddressForm billAndShippAddressForm)
	{
		this.billAndShippAddressForm = billAndShippAddressForm;
	}

	/**
	 * @return the licenseState
	 */
	public String getLicenseState()
	{
		return licenseState;
	}

	/**
	 * @param licenseState
	 *           the licenseState to set
	 */
	public void setLicenseState(final String licenseState)
	{
		this.licenseState = licenseState;
	}

	/**
	 * @return the zipcode
	 */
	public String getZipcode()
	{
		return zipcode;
	}

	/**
	 * @param zipcode
	 *           the zipcode to set
	 */
	public void setZipcode(final String zipcode)
	{
		this.zipcode = zipcode;
	}


}
