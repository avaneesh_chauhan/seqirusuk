/**
 *
 */
package com.seqirus.storefront.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.facades.cart.SeqirusCheckoutFacade;
import com.seqirus.flu360.facades.order.data.SeqirusLocationOrderData;
import com.seqirus.core.dataObjects.ChartandTableData;
import com.seqirus.core.dataObjects.OrderSummary;
import com.seqirus.core.dataObjects.ProductDetail;
import com.seqirus.core.dataObjects.ProductDetailAllItemNumber;
import com.seqirus.core.dataObjects.ShipmentDetail;
import com.seqirus.core.dataObjects.ShipmentTableData;
import com.seqirus.core.orders.service.SeqirusOrdersService;
import com.seqirus.core.orders.service.product.SeqirusProductService;
import com.seqirus.core.registration.service.SeqirusCustomerRegistrationService;
import com.seqirus.facades.customer.CustomerAddressFacade;
import com.seqirus.facades.orders.SeqirusOrdersFacade;
import com.seqirus.storefront.util.SeqirusUtils;


/**
 * Controller for Orders Summary, My Orders
 * 
 * @author 172553
 *
 */
@Controller
@RequestMapping(value = "/orders")
@RequireHardLogIn
public class SeqirusOrdersPageController extends AbstractSearchPageController
{
	private static final Logger LOGGER = Logger.getLogger(SeqirusOrdersPageController.class);


	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "customerAddressFacade")
	CustomerAddressFacade customerAddressFacade;

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;

	@Resource(name = "productService")
	private ProductService productService;

	@Autowired
	private SeqirusProductService seqirusProductService;

	@Resource(name = "seqirusOrdersFacade")
	private SeqirusOrdersFacade seqirusOrdersFacade;

	@Resource(name = "userService")
	UserService userService;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "seqirusCheckoutFacade")
	private SeqirusCheckoutFacade seqirusCheckoutFacade;

	@Resource(name = "seqirusOrdersService")
	private SeqirusOrdersService seqirusOrdersService;

	@Resource(name = "seqirusCustomerRegistrationService")
	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;

	@Resource
	private SessionService sessionService;

	private static final String ORDER_HISTORY_CMS_PAGE = "orders";

	private static final String BASE_FLUAD = "FLUAD";
	private static final String BASE_FLUCELVAX = "FLUCELVAX";
	private static final String BASE_AFLURIA = "AFLURIA";
	private static final String TOTAL_SHIP_FLUAD = "totalShipFluad";
	private static final String TOTAL_SHIP_FLUCELVAX = "totalShipFlucelvax";
	private static final String TOTAL_SHIP_AFLURIA = "totalShipAfluria";
	private static final String TOTAL_SHIP = "totalShip";
	private static final String TOTAL_ORDER_FLUAD = "totalOrderFluad";
	private static final String TOTAL_ORDER_FLUCELVAX = "totalOrderFlucelvax";
	private static final String TOTAL_ORDER_AFLURIA = "totalOrderAfluria";
	private static final String TOTAL_ORDER_QTY = "totalOrder";
	private static final String NO_FORMULATION = "NoFormulation";
	private static final String CONTACT_ADDRESS_DATA_ATTR = "contactAddressData";
	private static final String BILLING_ADDRESS_DATA_ATTR = "billingAddressData";
	private static final String SHIPPING_ADDRESS_DATA_ATTR = "shippingAddressData";
	private static final String PAYTO_ADDRESS_DATA_ATTR = "payToAddressData";
	private static final String QUANTITIES_BY_LOC = "qtyByLoc";

	private static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);

	/**
	 * Controller Mapping for Order Summary
	 * 
	 * @param model
	 * @param redirectModel
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String orders(final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		model.addAttribute("customer", customerFacade.getCurrentCustomer());
		final SeasonEntryModel seasonEntry = seqirusOrdersService.getSeasonEntry();
		model.addAttribute("seasonEntry", seasonEntry);
		return "";
	}

	private Set<String> findShipToPartnerIds(List<OrderSummary> orders)
	{
		Set<String> partnerIds = new HashSet<>();
		for (final OrderSummary co : orders)
		{
			LOGGER.info("Ship To ID : " + co.getShipToID());
			partnerIds.add(co.getShipToID());
		}
		return partnerIds;
	}


	/**
	 * Controller Mapping for My Orders
	 * 
	 * @param model
	 * @param redirectModel
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/my-orders", method = RequestMethod.GET)
	public String myOrders(@RequestParam(value = "sortDate", required = false)
	final String sortDate, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{

		final B2BCustomerModel account = (B2BCustomerModel) userService.getCurrentUser();
		final String customerId = account.getDefaultB2BUnit().getUid();
		SeasonEntryModel entry = fetchSeasonEntry();
		model.addAttribute("seasonEntry", entry);
		final String season = entry.getCurrentSeason();
		final List<OrderSummary> orders = seqirusOrdersFacade.getOrders(customerId, season);
		LOGGER.info("Number of Orders from Service : " + orders.size());
		removeJunkOrder(orders);
		LOGGER.info("Number of Orders post removal of junk orders : " + orders.size());
		//Uncomment when contract logic implemented
		//setContract(orders); 

		// Set Location specific Orders Start
		int totalShip = 0;
		int totalShipFluad = 0;
		int totalShipFlucelvax = 0;
		int totalShipAfluria = 0;
		int totalOrder = 0;

		List<SeqirusLocationOrderData> locations = new ArrayList<>();
		for (String shipToId : findShipToPartnerIds(orders))
		{
			int totalOrderFluad = 0;
			int totalOrderFlucelvax = 0;
			int totalOrderAfluria = 0;

			for (final OrderSummary co : orders)
			{
				if (StringUtils.equals(shipToId, co.getShipToID()))
				{
					List<ProductDetailAllItemNumber> products = co.getProductsAllItem();
					for (ProductDetailAllItemNumber product : products)
					{
						VariantProductModel productModel = (VariantProductModel) seqirusProductService
								.getProductDataForCode(product.getMaterialID());

						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUAD))
						{
							totalOrderFluad = totalOrderFluad + co.getMaterialOrderedQty(product.getMaterialID());
						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX))
						{
							totalOrderFlucelvax = totalOrderFlucelvax + co.getMaterialOrderedQty(product.getMaterialID());
						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_AFLURIA))
						{
							totalOrderAfluria = totalOrderAfluria + co.getMaterialOrderedQty(product.getMaterialID());
						}
					}
					LOGGER.info("Location ID : " + shipToId + " Name :" + co.getPartnerName() + " Fluad :" + totalOrderFluad
							+ " Flucelvax : " + totalOrderFlucelvax + " Afluria : " + totalOrderAfluria);
					SeqirusLocationOrderData location = new SeqirusLocationOrderData();
					location.setPartnerId(shipToId);
					location.setPartnerName(co.getPartnerName());
					location.setFluadOrderQty(totalOrderFluad);
					location.setFlucelvaxOrderQty(totalOrderFlucelvax);
					location.setAfluriaOrderQty(totalOrderAfluria);

					locations.add(location);
				}
			}
		}

		model.addAttribute(QUANTITIES_BY_LOC, locations);

		// Set Location specific Orders Start
		/*
		 * final List<OrderSummary> customerOrderlist = new ArrayList<OrderSummary>(); final Map<Date,
		 * List<CustomerOrder>> map = new TreeMap<Date, List<CustomerOrder>>(); final Map<Date, List<CustomerOrder>>
		 * newMap = new TreeMap(Collections.reverseOrder()); final Map<String, List<CustomerOrder>> seasonOrderMap = new
		 * HashMap<String, List<CustomerOrder>>(); final TreeSet<Date> dates = new
		 * TreeSet<Date>(Collections.reverseOrder());
		 * 
		 * // prepare base product list final List<String> baseProduct = new ArrayList<String>();
		 * baseProduct.add("FLUAD"); baseProduct.add("FLUCELVAX"); baseProduct.add("AFLURIA");
		 * model.addAttribute("baseProduct", baseProduct); final List<String> baseProductQTIV = new ArrayList<String>();
		 * baseProductQTIV.add("FLUADQUADRIVALENT"); baseProductQTIV.add("FLUADTRIVALENT");
		 * baseProductQTIV.add("FLUCELVAX"); baseProductQTIV.add("AFLURIA"); model.addAttribute("baseProductQTIV",
		 * baseProductQTIV);
		 * 
		 * final Collection<VariantOptionData> varientdataInSeason = new ArrayList<VariantOptionData>(); final
		 * List<VariantOptionData> varientdataSortedInSeason = new ArrayList<VariantOptionData>(); final
		 * Collection<VariantOptionData> varientdataOrderableInSeason = new ArrayList<VariantOptionData>(); final
		 * List<VariantOptionData> varientdataOrderableListInSeason = new ArrayList<VariantOptionData>();
		 * 
		 * getProductsForSeason("Seqirus_InSeason", varientdataInSeason, varientdataSortedInSeason,
		 * varientdataOrderableInSeason, varientdataOrderableListInSeason);
		 * 
		 * final Collection<VariantOptionData> varientdataPreSeason = new ArrayList<VariantOptionData>(); final
		 * List<VariantOptionData> varientdataSortedPreSeason = new ArrayList<VariantOptionData>(); final
		 * Collection<VariantOptionData> varientdataOrderablePreSeason = new ArrayList<VariantOptionData>(); final
		 * List<VariantOptionData> varientdataOrderableListPreSeason = new ArrayList<VariantOptionData>();
		 * 
		 * getProductsForSeason("Seqirus_PreSeason", varientdataPreSeason, varientdataSortedPreSeason,
		 * varientdataOrderablePreSeason, varientdataOrderableListPreSeason);
		 * 
		 * // filter selected season order from API response returned for date range for (final OrderSummary co : orders)
		 * {
		 * 
		 * final String varCode = co.getProducts().get(0).getMaterialID();
		 * 
		 * // set in/pre season
		 * 
		 * for (final VariantOptionData variantOptionData : varientdataPreSeason) { if
		 * (varCode.equalsIgnoreCase(variantOptionData.getCode())) { co.setSeasonInPre("Seqirus_PreSeason"); break; } }
		 * for (final VariantOptionData variantOptionData : varientdataInSeason) { if
		 * (varCode.equalsIgnoreCase(variantOptionData.getCode())) { co.setSeasonInPre("Seqirus_InSeason"); break; } }
		 * 
		 * // set season like 2019 2020 final VariantProductModel product = (VariantProductModel)
		 * productService.getProductForCode(varCode); final String productSeason = product.getSeason(); if
		 * (StringUtils.isNotEmpty(productSeason) && productSeason.equalsIgnoreCase(season)) { customerOrderlist.add(co);
		 * }
		 * 
		 * 
		 * }
		 * 
		 * 
		 * //set ship state final List<AddressData> allAddressData =
		 * seqirusCheckoutFacade.populateSoldToLinkedAddresses(); final List<AddressData> shippingAddressData =
		 * seqirusCheckoutFacade.populateLinkedAddressesForGivenType("shippingAddress", allAddressData); final Set<String>
		 * shipAddressIds = new HashSet<String>();
		 * 
		 * 
		 * for (final OrderSummary co : customerOrderlist) {
		 * 
		 * for (final AddressData shippingAddress : shippingAddressData) { if
		 * (shippingAddress.getSapCustomerId().equals(co.getShipToID())) { //this.fetchShippingParams(seqCart); if (null
		 * != shippingAddress.getRegion() && null != shippingAddress.getRegion().getIsocodeShort()) {
		 * co.getAddress().setState(shippingAddress.getRegion().getIsocodeShort()); }
		 * 
		 * } } shipAddressIds.add(co.getShipToID());
		 * 
		 * // set total order quantities
		 * 
		 * if (!co.getStatus().equalsIgnoreCase("Cancelled")) { totalOrder = totalOrder + co.getTotalOrderedQty(); for
		 * (final String orderMaterial : co.getAllOrderedMaterials()) {
		 * 
		 * 
		 * VariantProductModel productModel; productModel = (VariantProductModel)
		 * seqirusProductService.getProductDataForCode(orderMaterial);
		 * 
		 * if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUAD)) {
		 * 
		 * totalOrderFluad = totalOrderFluad + co.getMaterialOrderedQty(orderMaterial);
		 * 
		 * } if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX)) {
		 * 
		 * totalOrderFlucelvax = totalOrderFlucelvax + co.getMaterialOrderedQty(orderMaterial);
		 * 
		 * } if (productModel.getBaseName().equalsIgnoreCase(BASE_AFLURIA)) {
		 * 
		 * totalOrderAfluria = totalOrderAfluria + co.getMaterialOrderedQty(orderMaterial);
		 * 
		 * }
		 * 
		 * 
		 * 
		 * }
		 * 
		 * 
		 * // set total shipment quantities
		 * 
		 * for (final ShipmentDetail ship : co.getShipments()) {
		 * 
		 * totalShip = totalShip + ship.getTotalShippedQty(); final List<String> shipMaterials =
		 * ship.getShippedMaterials();
		 * 
		 * final Set<String> shipMaterialsSet = new HashSet<String>(); for (final String material : shipMaterials) {
		 * shipMaterialsSet.add(material); } for (final String material : shipMaterialsSet)
		 * 
		 * {
		 * 
		 * VariantProductModel productModel; productModel = (VariantProductModel)
		 * seqirusProductService.getProductDataForCode(material);
		 * 
		 * if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUAD)) {
		 * 
		 * totalShipFluad = totalShipFluad + ship.getMaterialShippedQty(material);
		 * 
		 * } if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX)) {
		 * 
		 * totalShipFlucelvax = totalShipFlucelvax + ship.getMaterialShippedQty(material);
		 * 
		 * } if (productModel.getBaseName().equalsIgnoreCase(BASE_AFLURIA)) {
		 * 
		 * totalShipAfluria = totalShipAfluria + ship.getMaterialShippedQty(material);
		 * 
		 * }
		 * 
		 * }
		 * 
		 * }
		 * 
		 * }
		 * 
		 * }
		 * 
		 * //set ship end
		 * 
		 * model.addAttribute(TOTAL_SHIP_FLUAD, totalShipFluad); model.addAttribute(TOTAL_SHIP_FLUCELVAX,
		 * totalShipFlucelvax); model.addAttribute(TOTAL_SHIP_AFLURIA, totalShipAfluria); model.addAttribute(TOTAL_SHIP,
		 * totalShip); model.addAttribute(TOTAL_ORDER_QTY, totalOrder); model.addAttribute("orders", customerOrderlist);
		 * model.addAttribute("shipAddressIds", shipAddressIds); final List<OrderSummary> customerOrderlistSorted = new
		 * ArrayList<OrderSummary>(); final List<OrderSummary> customerOrderlistClosedSorted = new
		 * ArrayList<OrderSummary>(); if (CollectionUtils.isNotEmpty(customerOrderlist)) {
		 * 
		 * for (final OrderSummary order : customerOrderlist) { LOGGER.info("order id" + order.getOrderID() +
		 * "order seasoninpre" + order.getSeasonInPre()); } }
		 * 
		 * model.addAttribute("varientdataInSeasonOrderable", varientdataOrderableListInSeason);
		 * model.addAttribute("varientdataPreSeasonOrderable", varientdataOrderableListPreSeason);
		 * 
		 * final Collection<VariantOptionData> varientdataInCurrentSeason = new ArrayList<VariantOptionData>(); final
		 * Collection<VariantOptionData> varientdataPreCurrentSeason = new ArrayList<VariantOptionData>();
		 * 
		 * for (final VariantOptionData variantOptionData : varientdataInSeason) {
		 * 
		 * final VariantProductModel product = (VariantProductModel)
		 * productService.getProductForCode(variantOptionData.getCode()); final String productSeason =
		 * product.getSeason(); if (season.equalsIgnoreCase(productSeason)) {
		 * varientdataInCurrentSeason.add(variantOptionData); } }
		 * 
		 * for (final VariantOptionData variantOptionData : varientdataPreSeason) {
		 * 
		 * final VariantProductModel product = (VariantProductModel)
		 * productService.getProductForCode(variantOptionData.getCode()); final String productSeason =
		 * product.getSeason(); if (season.equalsIgnoreCase(productSeason)) {
		 * varientdataPreCurrentSeason.add(variantOptionData); } } model.addAttribute("varientdataInSeason",
		 * varientdataInCurrentSeason); model.addAttribute("varientdataPreSeason", varientdataPreCurrentSeason);
		 * model.addAttribute("dates", dates); model.addAttribute("totalOrders", customerOrderlist.size());
		 */
		sessionService.setAttribute("allOrders", orders);
		prepareGraph(orders, model);
		final ContentPageModel orderHistoryPage = getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE);
		storeCmsPageInModel(model, orderHistoryPage);
		setUpMetaDataForContentPage(model, orderHistoryPage);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);

	}

	class OrderDateSort implements Comparator<OrderSummary>
	{
		@Override
		public int compare(final OrderSummary o1, final OrderSummary o2)
		{
			// TODO Auto-generated method stub
			return o2.getOrderDate().compareTo(o1.getOrderDate());
		}
	}

	/**
	 * Sort Products for a Season
	 * 
	 * @param season
	 * @param varientdata
	 * @param varientdataSorted
	 * @param varientdataOrderable
	 * @param varientdataOrderableList
	 */
	private void getProductsForSeason(final String season, final Collection<VariantOptionData> varientdata,
			final List<VariantOptionData> varientdataSorted, final Collection<VariantOptionData> varientdataOrderable,
			final List<VariantOptionData> varientdataOrderableList)
	{


		// all orderable products
		final CategoryModel category = commerceCategoryService.getCategoryForCode(season);
		//			final CategoryPageModel categoryPage = getCategoryPage(category);
		final List<ProductOption> options = new ArrayList<>(
				Arrays.asList(ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_MEDIA,
						ProductOption.BASIC, ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
						ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS,
						ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
						ProductOption.PRICE_RANGE, ProductOption.DELIVERY_MODE_AVAILABILITY));


		final List<ProductModel> products = category.getProducts();

		final List<ProductWrapperData> productDataList = new ArrayList<>();
		final List<Map<String, Collection<VariantOptionData>>> productFormulationsList = new ArrayList<Map<String, Collection<VariantOptionData>>>();

		for (final ProductModel productModel : products)
		{
			final ProductWrapperData pwd = new ProductWrapperData();
			ProductData productData = productFacade.getProductForCodeAndOptions(productModel.getCode(), options);
			pwd.setProductFormulations(groupProductsByFormulationOrder(productData));
			pwd.setMinPrice(minProductPrice(productData));
			pwd.setProductData(productData);
			productDataList.add(pwd);
			productFormulationsList.add(groupProductsByFormulationOrder(productData));
			productData = null;
		}



		for (final Map<String, Collection<VariantOptionData>> productFormulations : productFormulationsList)
		{
			for (final Map.Entry<String, Collection<VariantOptionData>> entry : productFormulations.entrySet())
			{

				varientdata.addAll(entry.getValue());
				varientdataSorted.addAll(entry.getValue());

			}

		}

		for (final VariantOptionData varient : varientdata)
		{
			if (varient.getOrderable())
			{
				varientdataOrderable.add(varient);
				varientdataOrderableList.add(varient);
			}
		}

		Collections.sort(varientdataSorted, new Comparator<VariantOptionData>()
		{

			public int compare(final VariantOptionData u1, final VariantOptionData u2)
			{
				if ((null == u1.getSequenceId()) || (null == u2.getSequenceId()))
				{
					return 0;
				}
				else
				{
					return u1.getSequenceId().compareTo(u2.getSequenceId());
				}
			}
		});


	}

	/**
	 * @param productData
	 * @return
	 */
	private Map<String, Collection<VariantOptionData>> groupProductsByFormulationOrder(final ProductData productData)
	{
		final List<VariantOptionData> variants = productData.getVariantOptions();

		final Map<String, Collection<VariantOptionData>> productsGroupByFormulation = new HashMap<>();

		if (null == variants || variants.isEmpty())
		{
			return productsGroupByFormulation;
		}

		for (final VariantOptionData variantOptionData : variants)
		{

			if (CollectionUtils.isNotEmpty(variantOptionData.getVariantOptionQualifiers()))
			{
				updateproductAndFormulationMap(productsGroupByFormulation, SeqirusUtils.getFormulationCategories(variantOptionData));

			}
			else if (productsGroupByFormulation.containsKey(NO_FORMULATION))
			{
				productsGroupByFormulation.get(NO_FORMULATION).add(variantOptionData);
			}
			else
			{
				final List<VariantOptionData> vodList = new ArrayList<>();
				vodList.add(variantOptionData);
				productsGroupByFormulation.put(NO_FORMULATION, vodList);
			}
		}

		SeqirusUtils.sortBySequenceId(productsGroupByFormulation);
		final TreeMap<String, Collection<VariantOptionData>> sorted = new TreeMap<>();
		sorted.putAll(productsGroupByFormulation);
		return sorted;
	}



	/**
	 * @param productsGroupByFormulation
	 * @param formulationData
	 */
	private void updateproductAndFormulationMap(final Map<String, Collection<VariantOptionData>> productsGroupByFormulation,
			final Map<String, Collection<VariantOptionData>> formulationData)
	{
		for (final String formulation : formulationData.keySet())
		{
			if (productsGroupByFormulation.containsKey(formulation))
			{
				productsGroupByFormulation.get(formulation).addAll(formulationData.get(formulation));
				SeqirusUtils.removeDuplicate(productsGroupByFormulation.get(formulation));
			}
			else
			{
				productsGroupByFormulation.put(formulation, formulationData.get(formulation));
			}
		}
	}

	/**
	 * @param productData
	 * @return
	 */
	private BigDecimal minProductPrice(final ProductData productData)
	{
		final List<VariantOptionData> variants = productData.getVariantOptions();
		if (null != variants)
		{
			final List<BigDecimal> allVariantsPrices = new ArrayList<BigDecimal>();
			for (final VariantOptionData variantOptionData : variants)
			{
				if (null != variantOptionData.getPriceData())
				{
					allVariantsPrices.add(variantOptionData.getPriceData().getValue());
				}

			}
			if (allVariantsPrices.isEmpty())
			{
				return BigDecimal.ZERO;
			}
			return Collections.min(allVariantsPrices);
		}
		else
		{
			return BigDecimal.ZERO;
		}
	}

	/**
	 * @param entry
	 * @return
	 */
	private SeasonEntryModel fetchSeasonEntry()
	{
		SeasonEntryModel entry = null;
		if (null != seqirusOrdersService)
		{
			entry = seqirusOrdersService.getSeasonEntry();
		}
		return entry;
	}

	public class ProductWrapperData
	{
		private ProductData productData;
		private Map<String, Collection<VariantOptionData>> productFormulations;
		private BigDecimal minPrice;

		/**
		 * @return the productData
		 */
		public ProductData getProductData()
		{
			return productData;
		}

		/**
		 * @param productData
		 *           the productData to set
		 */
		public void setProductData(final ProductData productData)
		{
			this.productData = productData;
		}

		/**
		 * @return the minPrice
		 */
		public BigDecimal getMinPrice()
		{
			return minPrice;
		}

		/**
		 * @param minPrice
		 *           the minPrice to set
		 */
		public void setMinPrice(final BigDecimal minPrice)
		{
			this.minPrice = minPrice;
		}

		/**
		 * @return the productFormulations
		 */
		public Map<String, Collection<VariantOptionData>> getProductFormulations()
		{
			return productFormulations;
		}

		/**
		 * @param productFormulations
		 *           the productFormulations to set
		 */
		public void setProductFormulations(final Map<String, Collection<VariantOptionData>> productFormulations)
		{
			this.productFormulations = productFormulations;
		}

	}

	/**
	 * Remove Unwanted Orders from the total orders received from webservice
	 * 
	 * @param customerOrderlist
	 */
	private void removeJunkOrder(final List<OrderSummary> customerOrderlist)
	{

		final Set<String> materialSet = new HashSet<String>();
		//Get All Unique codes available in all order
		for (final OrderSummary co : customerOrderlist)
		{
			for (final ProductDetail d : co.getProducts())
			{
				materialSet.add(d.getMaterialID());
			}
		}
		final List<String> availableProductCode = new ArrayList<String>();
		//Prepare Available Code list
		for (final String code : materialSet)
		{
			try
			{
				final VariantProductModel product = (VariantProductModel) productService.getProductForCode(code);
				availableProductCode.add(code);
			}
			catch (final Exception e)
			{
				LOGGER.error("exception while fetching product details", e);
			}
		}
		//Remove Unwanted orders
		final Iterator<OrderSummary> custItr = customerOrderlist.iterator();
		while (custItr.hasNext())
		{
			final OrderSummary co = custItr.next();
			final Iterator<ProductDetail> orderItr = co.getProducts().iterator();
			while (orderItr.hasNext())
			{
				final ProductDetail d = orderItr.next();
				if (!availableProductCode.contains(d.getMaterialID()))
				{
					orderItr.remove();
				}
			}
			if (co.getProducts().isEmpty())
			{
				custItr.remove();
			}
		}

	}

	private Map<String, Object> getAddressTypes(final List<AddressData> addressData)
	{
		final List<AddressData> shippingAddresses = new ArrayList<>();

		final Map addressMap = new HashMap<>();

		for (final AddressData address : addressData)
		{
			if (address.isDefaultAddress())
			{
				addressMap.put(CONTACT_ADDRESS_DATA_ATTR, address);
			}
			if (address.isBillingAddress())
			{
				addressMap.put(BILLING_ADDRESS_DATA_ATTR, address);
			}
			if (address.isPayToAddress())
			{
				addressMap.put(PAYTO_ADDRESS_DATA_ATTR, address);
			}
			if (address.isShippingAddress())
			{
				shippingAddresses.add(address);
				addressMap.put(SHIPPING_ADDRESS_DATA_ATTR, shippingAddresses);
			}
		}
		return addressMap;
	}

	/**
	 * Returns Calendar View for All Orders in JSON Format
	 *
	 * @param season
	 * @param session
	 * @return List<SeqirusShipmentCalendarData>
	 */
	/*
	 * @RequestMapping(value = "/orders/calendar", method = RequestMethod.GET)
	 * 
	 * @ResponseBody public List<SeqirusShipmentCalendarData> getAllWeekCalendarView(@RequestParam(value = "season",
	 * required = true) final String season, final HttpSession session) { final List<SeqirusShipmentCalendarData>
	 * shipmentList = new ArrayList<>(); final String orderSeason = season; final List<OrderSummary> activeOrders =
	 * (List<OrderSummary>) sessionService.getAttribute("allOrders"); if (CollectionUtils.isNotEmpty(activeOrders)) {
	 * LOGGER.info("Active Order Count :: " + activeOrders.size()); prepareWaveCalendar(activeOrders, shipmentList); }
	 * else { LOGGER.info("No Orders for Wave Range"); } LOGGER.info("Size of Shipping Items in Calendar : " +
	 * shipmentList.size()); return shipmentList; }
	 * 
	 *//**
		 * Preparing Calendar for Shipping Season
		 *
		 * @param activeOrders
		 * @param shipmentList
		 */
	/*
	 * private void prepareWaveCalendar(final List<OrderSummary> activeOrders, final List<SeqirusShipmentCalendarData>
	 * shipmentList) { final Set<String> pgiDates = new HashSet<>(); final Set<String> promisedDates = new HashSet<>();
	 * final List<SeqirusOrderQuantityData> listofOrdersQty = new ArrayList<>(); final B2BCustomerModel customer =
	 * (B2BCustomerModel) userService.getCurrentUser(); for (final OrderSummary order : activeOrders) { //Prepare a list
	 * of Unique PGI Dates if (CollectionUtils.isNotEmpty(order.getShipments())) {
	 * prepareAllPGIDates(order.getShipments(), pgiDates); groupByPGIDate(pgiDates,order); }
	 * 
	 * //Prepare a list of Unique Promised Dates if (CollectionUtils.isNotEmpty(order.getProductsAllItem())) {
	 * preparePromisedDates(order.getProductsAllItem(), promisedDates); } } LOGGER.info("Total " + promisedDates.size() +
	 * " Promised Dates for All Orders");
	 * 
	 * //Prepare a list of Product Items for Unique Promised Dates prepareAllProductsByPromiseDay(promisedDates,
	 * activeOrders, listofOrdersQty); //Find Same Week Delivery and Combine all shipments
	 * prepareShipments(listofOrdersQty, shipmentList);
	 * 
	 * //Set PGI Dates setPGIDatesForCalendar(pgiDates, shipmentList); }
	 * 
	 * 
	 *//**
		 * Combine Multiple Promise Dates for Same Week and Prepare Shipment data for Calendar View
		 *
		 * @param listofOrdersQty
		 * @param shipmentList
		 */
	/*
	 * private void prepareShipments(final List<SeqirusOrderQuantityData> listofOrdersQty, final
	 * List<SeqirusShipmentCalendarData> shipmentList) { final Map<String, SeqirusOrderQuantityData> shipmentWeekMap =
	 * new HashMap<>(); for (int i = 0; i < listofOrdersQty.size(); i++) { String promiseiSunday = ""; String
	 * promisejSunday = ""; final SeqirusOrderQuantityData orderQtyi = listofOrdersQty.get(i); promiseiSunday =
	 * findSunday(orderQtyi.getPromiseDate()); int totalFluad = orderQtyi.getFluadOrderQuantity(); int totalflucelvax =
	 * orderQtyi.getFlucelvaxOrderQuantity(); int totalAfluria = orderQtyi.getAfluriaOrderQuantity(); int totalUnit =
	 * orderQtyi.getTotalOrderQty(); //LOGGER.info("For each i : Sunday : " + promiseiSunday + " for Promise Date : " +
	 * orderQtyi.getPromiseDate()); for (int j = i + 1; j < listofOrdersQty.size(); j++) { final SeqirusOrderQuantityData
	 * orderQtyj = listofOrdersQty.get(j); promisejSunday = findSunday(orderQtyj.getPromiseDate());
	 * //LOGGER.info("For each j : Sunday : " + promisejSunday + " for Promise Date : " + orderQtyj.getPromiseDate()); if
	 * (promiseiSunday.equals(promisejSunday)) { //LOGGER.info("Matched : Sunday : " + promisejSunday + " Total Units : "
	 * + totalUnit); totalFluad = totalFluad + orderQtyj.getFluadOrderQuantity(); totalflucelvax = totalflucelvax +
	 * orderQtyj.getFlucelvaxOrderQuantity(); totalAfluria = totalAfluria + orderQtyj.getAfluriaOrderQuantity();
	 * totalUnit = totalUnit + orderQtyj.getTotalOrderQty(); //LOGGER.info("Matched : Total Units After Adding: " +
	 * totalUnit); } } LOGGER.info("Total " + totalUnit + EXPECTED_SHIPPED_UNIT + " for the week beginning on " +
	 * promiseiSunday + "with Fluad : " + totalFluad + " : Flucelvax : " + totalflucelvax + " : Afluria : " +
	 * totalAfluria); //Preparing a new data set final SeqirusOrderQuantityData newOrderQty = new
	 * SeqirusOrderQuantityData(); newOrderQty.setPromiseDate(promiseiSunday);
	 * newOrderQty.setFluadOrderQuantity(totalFluad); newOrderQty.setFlucelvaxOrderQuantity(totalflucelvax);
	 * newOrderQty.setAfluriaOrderQuantity(totalAfluria); newOrderQty.setTotalOrderQty(totalUnit); if
	 * (doesPromiseDateExist(shipmentWeekMap, newOrderQty)) { shipmentWeekMap.put(promiseiSunday, newOrderQty); } }
	 * LOGGER.info("Total No of Shipment Weeks for Calendar View : " + shipmentWeekMap.size()); //Iterate through the Map
	 * and Populate the Weekly Shipment List for (final Map.Entry<String, SeqirusOrderQuantityData> entry :
	 * shipmentWeekMap.entrySet()) { final SeqirusOrderQuantityData orderQty = entry.getValue(); final
	 * SeqirusShipmentCalendarData expectedShipment = new SeqirusShipmentCalendarData();
	 * expectedShipment.setStart(entry.getKey() + FIRST_HOUR); expectedShipment.setEnd(findSaturday(entry.getKey()) +
	 * LAST_HOUR); expectedShipment.setTitle(orderQty.getTotalOrderQty() + EXPECTED_SHIPPED_UNIT); final String html =
	 * "<table class='table table-striped table-condensed'><tr><td>Flucelvax</td><td>" +
	 * orderQty.getFlucelvaxOrderQuantity() + "</td></tr><tr><td>Fluad</td><td>" + orderQty.getFluadOrderQuantity() +
	 * "</td></tr><tr><td>Afluria</td><td>" + orderQty.getAfluriaOrderQuantity() + "</td></tr></table>";
	 * expectedShipment.setDescription(html); expectedShipment.setClassName(" "); expectedShipment.setIcon(" ");
	 * shipmentList.add(expectedShipment); } }
	 * 
	 *//**
		 * Checks whether New Set of Product Quantity needs to be added
		 *
		 * @param shipmentWeekMap
		 * @param newOrderQty
		 * @return boolean
		 */
	/*
	 * private boolean doesPromiseDateExist(final Map<String, SeqirusOrderQuantityData> shipmentWeekMap, final
	 * SeqirusOrderQuantityData newOrderQty) { boolean toRemove = false; if (shipmentWeekMap.size() == 0) { return true;
	 * } for (final Map.Entry<String, SeqirusOrderQuantityData> entry : shipmentWeekMap.entrySet()) { if
	 * (StringUtils.equals(entry.getKey(), newOrderQty.getPromiseDate())) { if (entry.getValue().getTotalOrderQty() <
	 * newOrderQty.getTotalOrderQty()) { toRemove = true; } } else { toRemove = true; } } return toRemove; }
	 * 
	 *//**
		 * Returns Date of Beginning (Sunday) of the Week
		 *
		 * @param promiseDate
		 * @return String
		 */
	/*
	 * private String findSunday(final String promiseDate) { String sunday = ""; final Calendar promiseCal =
	 * Calendar.getInstance(); try { promiseCal.setTime(formatter.parse(promiseDate)); } catch (final ParseException pe)
	 * { LOGGER.error("Exception during Promise Date Parsing" + pe.getMessage()); } sunday =
	 * SeqirusUtils.getDateForSunday(promiseCal); return sunday; }
	 * 
	 *//**
		 * Returns Date of End (Saturday) of the Week
		 *
		 * @param promiseDate
		 * @return String
		 */
	/*
	 * private String findSaturday(final String promiseDate) { String saturday = ""; final Calendar promiseCal =
	 * Calendar.getInstance(); try { promiseCal.setTime(formatter.parse(promiseDate)); } catch (final ParseException pe)
	 * { LOGGER.error("Exception during Promise Date Parsing" + pe.getMessage()); } saturday =
	 * SeqirusUtils.getDateForNextSaturday(promiseCal); return saturday; }
	 * 
	 *//**
		 * Preparing a list of PGI Dates for a single order
		 *
		 * @param shipments
		 * @param pgiDates
		 */
	/*
	 * private void prepareAllPGIDates(final List<ShipmentDetail> shipments, final Set<String> pgiDates) { for (final
	 * ShipmentDetail shipment : shipments) { final Date pgiDate = shipment.getPGIDate(); if (null != pgiDate) { final
	 * String strPGIdate = formatter.format(shipment.getPGIDate()); pgiDates.add(strPGIdate); } } }
	 * 
	 *//**
		 * Preparing a list of product items for each order grouped by PGI Dates
		 *
		 * @param pgiDates
		 * @param orders
		 */
	/*
	 * private void groupByPGIDate(final Set<String> pgiDates, final List<OrderSummary> orders) {
	 * 
	 * for (final String pgiDate : pgiDates) { int totalShip = 0; int totalShipFluad = 0; int totalShipFlucelvax = 0; int
	 * totalShipAfluria = 0;
	 * 
	 * for (final OrderSummary order : orders) { List<ShipmentDetail> shipments = order.getShipments();
	 * if(CollectionUtils.isNotEmpty(shipments)) { for(ShipmentDetail shipment : shipments) {
	 * if(StringUtils.equals(pgiDate, formatter.format(shipment.getPGIDate()))) { List<ProductDetail> products =
	 * shipment.getShippedIems(); if(CollectionUtils.isNotEmpty(products)) { for(ProductDetail product : products) {
	 * String materialId = product.getMaterialID(); final VariantProductModel productModel = (VariantProductModel)
	 * seqirusProductService .getProductDataForCode(materialId); if (null != productModel) { final String productBaseName
	 * = productModel.getBaseName();
	 * 
	 * if (StringUtils.equalsIgnoreCase(productBaseName, BASE_FLUAD)) { //LOGGER.info(productAllItem.getItemQty() +
	 * "Fluad for promise date : " + promisedDate); totalShipFluad = totalShipFluad + product.getItemQty(); } if
	 * (StringUtils.equalsIgnoreCase(productBaseName, BASE_FLUCELVAX)) { //LOGGER.info(productAllItem.getItemQty() +
	 * "Flucelvax for promise date : " + promisedDate); totalShipFlucelvax = totalShipFlucelvax + product.getItemQty(); }
	 * if (StringUtils.equalsIgnoreCase(productBaseName, BASE_AFLURIA)) { //LOGGER.info(productAllItem.getItemQty() +
	 * "Afluria for promise date : " + promisedDate); totalShipAfluria = totalShipAfluria + product.getItemQty(); }
	 * 
	 * totalShip = totalShipFluad + totalShipFlucelvax + totalShipAfluria; //LOGGER.info(totalShip +
	 * " Units for promise date : " + promisedDate); } else { LOGGER.error("Unknown Product Item " + materialId +
	 * " returned from API"); } } } }
	 * 
	 * } } final List<ProductDetailAllItemNumber> productsAllItem = order.getProductsAllItem(); for (final
	 * ProductDetailAllItemNumber productAllItem : productsAllItem) { if (null != productAllItem.getPromisedDate()) {
	 * final String promisedDate = formatter.format(productAllItem.getPromisedDate()); { if
	 * (StringUtils.equals(uniquePromiseDate, promisedDate) && StringUtils.isNotBlank(productAllItem.getShippingWave()))
	 * { //LOGGER.info("Promised Date : " + uniquePromiseDate + " matches with " + promisedDate); final
	 * VariantProductModel productModel = (VariantProductModel) seqirusProductService
	 * .getProductDataForCode(productAllItem.getMaterialID());
	 * 
	 * if (null != productModel) { final String productBaseName = productModel.getBaseName();
	 * 
	 * if (StringUtils.equalsIgnoreCase(productBaseName, BASE_FLUAD)) { //LOGGER.info(productAllItem.getItemQty() +
	 * "Fluad for promise date : " + promisedDate); totalShipFluad = totalShipFluad + productAllItem.getItemQty(); } if
	 * (StringUtils.equalsIgnoreCase(productBaseName, BASE_FLUCELVAX)) { //LOGGER.info(productAllItem.getItemQty() +
	 * "Flucelvax for promise date : " + promisedDate); totalShipFlucelvax = totalShipFlucelvax +
	 * productAllItem.getItemQty(); } if (StringUtils.equalsIgnoreCase(productBaseName, BASE_AFLURIA)) {
	 * //LOGGER.info(productAllItem.getItemQty() + "Afluria for promise date : " + promisedDate); totalShipAfluria =
	 * totalShipAfluria + productAllItem.getItemQty(); }
	 * 
	 * totalShip = totalShipFluad + totalShipFlucelvax + totalShipAfluria; //LOGGER.info(totalShip +
	 * " Units for promise date : " + promisedDate); } else { LOGGER.error("Unknown Product Item " +
	 * productAllItem.getMaterialID() + " returned from API"); } } } } } } LOGGER.info("Total " + totalShip +
	 * EXPECTED_SHIPPED_UNIT + " for promise date " + uniquePromiseDate + " with Fluad : " + totalShipFluad +
	 * " : totalShipFlucelvax : " + totalShipFlucelvax + " : Afluria : " + totalShipAfluria);
	 * 
	 * final SeqirusOrderQuantityData orderQty = new SeqirusOrderQuantityData();
	 * orderQty.setAfluriaOrderQuantity(totalShipAfluria); orderQty.setFluadOrderQuantity(totalShipFluad);
	 * orderQty.setFlucelvaxOrderQuantity(totalShipFlucelvax); orderQty.setTotalOrderQty(totalShip);
	 * orderQty.setPromiseDate(uniquePromiseDate); listofOrdersQty.add(orderQty);
	 * 
	 * } }
	 * 
	 * 
	 * 
	 *//**
		 * Preparing Promised Data for a single order
		 *
		 * @param products
		 * @param promisedDates
		 */

	/*
	 * private void preparePromisedDates(final List<ProductDetailAllItemNumber> products, final Set<String>
	 * promisedDates) { for (final ProductDetailAllItemNumber product : products) { if (null != product) { final Date
	 * promiseDate = product.getPromisedDate(); if (null != promiseDate &&
	 * StringUtils.isNotBlank(product.getShippingWave())) { promisedDates.add(formatter.format(promiseDate)); } } } }
	 * 
	 *//**
		 * Set PGI Dates for Shipping Visibility Calendar
		 *
		 * @param pgiDates
		 * @param calendarOfshipments
		 *//*
			 * private void setPGIDatesForCalendar(final Set<String> pgiDates, final List<SeqirusShipmentCalendarData>
			 * calendarOfshipments) { final String CLASS_NAME = "orders_cal-delivered"; final String ICON_CLASS = "truck";
			 * if (CollectionUtils.isNotEmpty(pgiDates)) { //LOGGER.info("Total No of PGI Dates : " + pgiDates.size() +
			 * " in All Orders"); for (final String pgiDate : pgiDates) { //LOGGER.info("PGI Date matched with c1"); final
			 * SeqirusShipmentCalendarData itemShipped = new SeqirusShipmentCalendarData(); itemShipped.setTitle(" ");
			 * itemShipped.setStart(pgiDate); itemShipped.setEnd(" "); itemShipped.setDescription(" ");
			 * itemShipped.setClassName(CLASS_NAME); //itemShipped.setIcon(ICON_CLASS);
			 * calendarOfshipments.add(itemShipped); } } }
			 */

	@RequestMapping(value = "/getChartandTableData", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public ChartandTableData getShipmentTableData(final HttpServletRequest request, final Model model)
	{
		final ChartandTableData data = new ChartandTableData();
		final List<ShipmentTableData> tableDataList = new ArrayList<ShipmentTableData>();
		List<OrderSummary> orders = (List<OrderSummary>) sessionService.getAttribute("allOrders");
		List<Date> pgiDates = new ArrayList<>();
		for (final OrderSummary summary : orders)
		{
			List<ShipmentDetail> shipments = summary.getShipments();

			if (CollectionUtils.isNotEmpty(shipments))
			{
				for (ShipmentDetail shipment : shipments)
				{
					pgiDates.add(shipment.getPGIDate());
					Collections.sort(pgiDates);
					Collections.reverse(pgiDates);
				}
				for (Date date : pgiDates)
				{
					LOGGER.info("After Sorting : " + formatter.format(date));
				}
			}
		}
		
		for (Date date : pgiDates)
		{
			for (final OrderSummary summary : orders)
			{
				List<ShipmentDetail> shipments = summary.getShipments();

				if (CollectionUtils.isNotEmpty(shipments))
				{
					for (ShipmentDetail shipment : shipments)
					{
						if (formatter.format(date).equals(formatter.format(shipment.getPGIDate())))
						{
							LOGGER.info("Order : " + summary.getOrderID() + " : Shipped Qty : " + shipment.getTotalShippedQty()
									+ " : Location : " + summary.getShipToAddress().getAddressLine1());
							ShipmentTableData shipmentTable = new ShipmentTableData();
							shipmentTable.setOrders("#"+summary.getOrderID());
							shipmentTable.setQty(String.valueOf(summary.getTotalShippedQty()));
							StringBuffer sbLocation = new StringBuffer();
							com.seqirus.core.dataObjects.AddressDetail shipToAddr = summary.getShipToAddress();
							sbLocation.append(shipToAddr.getAddressLine1());
							if(StringUtils.isNotBlank(shipToAddr.getAddressLine2())) 
							{ 
								sbLocation.append("," + shipToAddr.getAddressLine2());
							}
							sbLocation.append("," + shipToAddr.getState()); 
							sbLocation.append(" " + shipToAddr.getZipCode());
							shipmentTable.setLocation(sbLocation.toString());	
							shipmentTable.setViewdetails("View Details");
							tableDataList.add(shipmentTable);
						}
					}
				}
			}
		}
		data.setTableData(tableDataList);
		return data;
	}

	
	
	/**
	 * Method to prepare graph from all orders
	 * 
	 * @param orders
	 * @param model
	 */
	private void prepareGraph(List<OrderSummary> orders, Model model)
	{
		int totalShippedQty = 0;
		int delivered = 0;
		int inTransit = 0;
		int processing = 0;

		List<Date> pgiDates = new ArrayList<>();

		for (final OrderSummary summary : orders)
		{
			List<ShipmentDetail> shipments = summary.getShipments();

			if (CollectionUtils.isNotEmpty(shipments))
			{
				for (final ShipmentDetail ship : shipments)
				{
					totalShippedQty = totalShippedQty + ship.getTotalShippedQty();

					if (StringUtils.equalsIgnoreCase("Delivered", ship.getStatus()))
					{
						for (ProductDetail product : ship.getShippedIems())
						{
							delivered = delivered + product.getQuantity();
						}
					}
					else if (StringUtils.equalsIgnoreCase("Out for delivery", ship.getStatus()))
					{
						for (ProductDetail product : ship.getShippedIems())
						{
							inTransit = inTransit + product.getQuantity();
						}
					}
					else if (StringUtils.equalsIgnoreCase("Yet to Process", ship.getStatus()))
					{
						for (ProductDetail product : ship.getShippedIems())
						{
							processing = processing + product.getQuantity();
						}
					}
				}
			}
		}


		LOGGER.info("Total Shipped Quantity : " + totalShippedQty + " : delivered : " + delivered + " : inTransit : " + inTransit
				+ " : processing : " + processing);
		model.addAttribute("Delivered", delivered);
		model.addAttribute("inTransit", inTransit);
		model.addAttribute("processing", processing);
		model.addAttribute("Total_Shipped_Qty", totalShippedQty);
	}


}
