
package com.seqirus.storefront.forms;

/**
 * BillingAndShippingAddress are using Customer Registration process
 *
 */
public class BillingAndShippingAddressForm
{

	private boolean sameAsShipAddr;
	private String Line1;
	private String Line2;
	private String postalCode;
	private String city;
	private String billingState;
	private String billingOrgName;
	private String phone;
	private String phoneExt;
	private String groupOrganization;
	private String firstName;
	private String lastName;
	private String email;
	private String confEmail;
	private String jobTitle;

	private String sapCustomerId;

	private String memberId;

	private String invoiceEmail;
	private String confInvoiceEmail;

	private String acctStmtEmail;
	private String confAcctStmtEmail;

	private boolean optNotifyInvoiceEmail;
	private boolean optNotifyAcctStmtEmail;

	private String duns;

	private String addressId;
	private String altEmail;

	/**
	 * @return the confInvoiceEmail
	 */
	public String getConfInvoiceEmail()
	{
		return confInvoiceEmail;
	}

	/**
	 * @param confInvoiceEmail
	 *           the confInvoiceEmail to set
	 */
	public void setConfInvoiceEmail(final String confInvoiceEmail)
	{
		this.confInvoiceEmail = confInvoiceEmail;
	}

	/**
	 * @return the acctStmtEmail
	 */
	public String getAcctStmtEmail()
	{
		return acctStmtEmail;
	}

	/**
	 * @param acctStmtEmail
	 *           the acctStmtEmail to set
	 */
	public void setAcctStmtEmail(final String acctStmtEmail)
	{
		this.acctStmtEmail = acctStmtEmail;
	}

	/**
	 * @return the confAcctStmtEmail
	 */
	public String getConfAcctStmtEmail()
	{
		return confAcctStmtEmail;
	}

	/**
	 * @param confAcctStmtEmail
	 *           the confAcctStmtEmail to set
	 */
	public void setConfAcctStmtEmail(final String confAcctStmtEmail)
	{
		this.confAcctStmtEmail = confAcctStmtEmail;
	}





	/**
	 * @return the line1
	 */
	public String getLine1()
	{
		return Line1;
	}

	/**
	 * @param line1
	 *           the line1 to set
	 */
	public void setLine1(final String line1)
	{
		Line1 = line1;
	}

	/**
	 * @return the line2
	 */
	public String getLine2()
	{
		return Line2;
	}

	/**
	 * @param line2
	 *           the line2 to set
	 */
	public void setLine2(final String line2)
	{
		Line2 = line2;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *           the postalCode to set
	 */
	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the billingOrgName
	 */
	public String getBillingOrgName()
	{
		return billingOrgName;
	}

	/**
	 * @param billingOrgName
	 *           the billingOrgName to set
	 */
	public void setBillingOrgName(final String billingOrgName)
	{
		this.billingOrgName = billingOrgName;
	}

	/**
	 * @return the billingState
	 */
	public String getBillingState()
	{
		return billingState;
	}

	/**
	 * @param billingState
	 *           the billingState to set
	 */
	public void setBillingState(final String billingState)
	{
		this.billingState = billingState;
	}

	/**
	 * @return the phone
	 */
	public String getPhone()
	{
		return phone;
	}

	/**
	 * @param phone
	 *           the phone to set
	 */
	public void setPhone(final String phone)
	{
		this.phone = phone;
	}



	/**
	 * @return the phoneExt
	 */
	public String getPhoneExt()
	{
		return phoneExt;
	}

	/**
	 * @param phoneExt
	 *           the phoneExt to set
	 */
	public void setPhoneExt(final String phoneExt)
	{
		this.phoneExt = phoneExt;
	}

	/**
	 * @return the sameAsShipAddr
	 */
	public boolean isSameAsShipAddr()
	{
		return sameAsShipAddr;
	}

	/**
	 * @param sameAsShipAddr
	 *           the sameAsShipAddr to set
	 */
	public void setSameAsShipAddr(final boolean sameAsShipAddr)
	{
		this.sameAsShipAddr = sameAsShipAddr;
	}

	/**
	 * @return the groupOrganization
	 */
	public String getGroupOrganization()
	{
		return groupOrganization;
	}

	/**
	 * @param groupOrganization
	 *           the groupOrganization to set
	 */
	public void setGroupOrganization(final String groupOrganization)
	{
		this.groupOrganization = groupOrganization;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the jobTitle
	 */
	public String getJobTitle()
	{
		return jobTitle;
	}

	/**
	 * @param jobTitle
	 *           the jobTitle to set
	 */
	public void setJobTitle(final String jobTitle)
	{
		this.jobTitle = jobTitle;
	}

	/**
	 * @return the confEmail
	 */
	public String getConfEmail()
	{
		return confEmail;
	}

	/**
	 * @param confEmail
	 *           the confEmail to set
	 */
	public void setConfEmail(final String confEmail)
	{
		this.confEmail = confEmail;
	}

	/**
	 * @return the sapCustomerId
	 */
	public String getSapCustomerId()
	{
		return sapCustomerId;
	}

	/**
	 * @param sapCustomerId
	 *           the sapCustomerId to set
	 */
	public void setSapCustomerId(final String sapCustomerId)
	{
		this.sapCustomerId = sapCustomerId;
	}

	/**
	 * @return the memberId
	 */
	public String getMemberId()
	{
		return memberId;
	}

	/**
	 * @param memberId
	 *           the memberId to set
	 */
	public void setMemberId(final String memberId)
	{
		this.memberId = memberId;
	}

	/**
	 * @return the invoiceEmail
	 */
	public String getInvoiceEmail()
	{
		return invoiceEmail;
	}

	/**
	 * @param invoiceEmail
	 *           the invoiceEmail to set
	 */
	public void setInvoiceEmail(final String invoiceEmail)
	{
		this.invoiceEmail = invoiceEmail;
	}

	/**
	 * @return the optNotifyInvoiceEmail
	 */
	public boolean isOptNotifyInvoiceEmail()
	{
		return optNotifyInvoiceEmail;
	}

	/**
	 * @param optNotifyInvoiceEmail
	 *           the optNotifyInvoiceEmail to set
	 */
	public void setOptNotifyInvoiceEmail(final boolean optNotifyInvoiceEmail)
	{
		this.optNotifyInvoiceEmail = optNotifyInvoiceEmail;
	}

	/**
	 * @return the optNotifyAcctStmtEmail
	 */
	public boolean isOptNotifyAcctStmtEmail()
	{
		return optNotifyAcctStmtEmail;
	}

	/**
	 * @param optNotifyAcctStmtEmail
	 *           the optNotifyAcctStmtEmail to set
	 */
	public void setOptNotifyAcctStmtEmail(final boolean optNotifyAcctStmtEmail)
	{
		this.optNotifyAcctStmtEmail = optNotifyAcctStmtEmail;
	}

	/**
	 * @return the duns
	 */
	public String getDuns()
	{
		return duns;
	}

	/**
	 * @param duns
	 *           the duns to set
	 */
	public void setDuns(final String duns)
	{
		this.duns = duns;
	}

	/**
	 * @return the addressId
	 */
	public String getAddressId()
	{
		return addressId;
	}

	/**
	 * @param addressId
	 *           the addressId to set
	 */
	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}

	/**
	 * @return the altEmail
	 */
	public String getAltEmail()
	{
		return altEmail;
	}

	/**
	 * @param altEmail
	 *           the altEmail to set
	 */
	public void setAltEmail(final String altEmail)
	{
		this.altEmail = altEmail;
	}


}
