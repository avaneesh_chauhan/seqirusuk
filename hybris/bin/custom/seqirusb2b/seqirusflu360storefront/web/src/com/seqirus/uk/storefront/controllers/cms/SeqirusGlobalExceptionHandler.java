/*
 *
 */
package com.seqirus.storefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.RedirectView;

import com.seqirus.storefront.controllers.ControllerConstants;




/**
 * The Class SeqirusGlobalExceptionHandler.
 */
@Controller
@ControllerAdvice
class SeqirusGlobalExceptionHandler extends AbstractPageController
{

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SeqirusGlobalExceptionHandler.class);


	/**
	 * Default error handler.
	 *
	 * @param req
	 *           the req
	 * @param e
	 *           the e
	 * @return the redirect view
	 */
	@ExceptionHandler(value = Exception.class)
	public RedirectView defaultErrorHandler(final HttpServletRequest req, final Exception e)
	{
		LOGGER.error("Global Exception Handler == ", e);
		final String strView = req.getContextPath() + "/errorpage";
		return (new RedirectView(strView));
	}

	//@RequestMapping(value = "/errorpage", method = RequestMethod.GET)
	/**
	 * Error.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@GetMapping(path = "/errorpage")
	public String error(final Model model) throws CMSItemNotFoundException
	{
		LOGGER.info("Inside errorPage");
		storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.GLOBAL_ERROR));
		return getViewForPage(model);
	}

	/**
	 * Error step.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@GetMapping(value = "**/errorpage")
	public String errorStep(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.GLOBAL_ERROR));
		return getViewForPage(model);
	}
}
