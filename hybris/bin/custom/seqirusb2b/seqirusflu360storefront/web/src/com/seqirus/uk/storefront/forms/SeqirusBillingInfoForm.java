/**
 * 
 */
package com.seqirus.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;

/**
 * @author 172553
 *
 */
public class SeqirusBillingInfoForm extends RegisterForm
{
	private SeqirusAddressForm billingAddressForm;
	private boolean paying;
	private boolean shipping;
	private boolean acctStmtByEmail;
	private String acctStmtEmailAddress;
	private boolean invoiceByEmail;
	private String invoiceEmailAddress;
	
	
	/**
	 * @return the billingAddressForm
	 */
	public SeqirusAddressForm getBillingAddressForm()
	{
		return billingAddressForm;
	}
	/**
	 * @param billingAddressForm the billingAddressForm to set
	 */
	public void setBillingAddressForm(SeqirusAddressForm billingAddressForm)
	{
		this.billingAddressForm = billingAddressForm;
	}
	/**
	 * @return the paying
	 */
	public boolean isPaying()
	{
		return paying;
	}
	/**
	 * @param paying the paying to set
	 */
	public void setPaying(boolean paying)
	{
		this.paying = paying;
	}
	/**
	 * @return the shipping
	 */
	public boolean isShipping()
	{
		return shipping;
	}
	/**
	 * @param shipping the shipping to set
	 */
	public void setShipping(boolean shipping)
	{
		this.shipping = shipping;
	}
	/**
	 * @return the acctStmtByEmail
	 */
	public boolean isAcctStmtByEmail()
	{
		return acctStmtByEmail;
	}
	/**
	 * @param acctStmtByEmail the acctStmtByEmail to set
	 */
	public void setAcctStmtByEmail(boolean acctStmtByEmail)
	{
		this.acctStmtByEmail = acctStmtByEmail;
	}
	/**
	 * @return the acctStmtEmailAddress
	 */
	public String getAcctStmtEmailAddress()
	{
		return acctStmtEmailAddress;
	}
	/**
	 * @param acctStmtEmailAddress the acctStmtEmailAddress to set
	 */
	public void setAcctStmtEmailAddress(String acctStmtEmailAddress)
	{
		this.acctStmtEmailAddress = acctStmtEmailAddress;
	}
	/**
	 * @return the invoiceByEmail
	 */
	public boolean isInvoiceByEmail()
	{
		return invoiceByEmail;
	}
	/**
	 * @param invoiceByEmail the invoiceByEmail to set
	 */
	public void setInvoiceByEmail(boolean invoiceByEmail)
	{
		this.invoiceByEmail = invoiceByEmail;
	}
	/**
	 * @return the invoiceEmailAddress
	 */
	public String getInvoiceEmailAddress()
	{
		return invoiceEmailAddress;
	}
	/**
	 * @param invoiceEmailAddress the invoiceEmailAddress to set
	 */
	public void setInvoiceEmailAddress(String invoiceEmailAddress)
	{
		this.invoiceEmailAddress = invoiceEmailAddress;
	}
	
	
}
