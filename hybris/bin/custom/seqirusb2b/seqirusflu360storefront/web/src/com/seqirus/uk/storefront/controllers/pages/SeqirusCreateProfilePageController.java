/**
 * 
 */
package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.util.Collections;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.seqirus.storefront.controllers.ControllerConstants;

/**
 * The Class SeqirusCreateProfilePageController.
 *
 * @author Avaneesh Chauhan
 */
@Controller
@RequestMapping(value = "/createprofile")
public class SeqirusCreateProfilePageController extends AbstractPageController
{
	

	/**
	 * Creates the user profiler.
	 *
	 * @param model the model
	 * @return the string
	 * @throws CMSItemNotFoundException the CMS item not found exception
	 */
	@GetMapping
	public String createUserProfiler(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.CREATEPROFILE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.CREATEPROFILE));
		final Breadcrumb createProfileBreadcrumbEntry = new Breadcrumb("#",
				getMessageSource().getMessage("header.register.text", null, getI18nService().getCurrentLocale()), null);
		model.addAttribute("breadcrumbs", Collections.singletonList(createProfileBreadcrumbEntry));
		return ControllerConstants.Views.Pages.Account.CreateProfilePage;
	}

}
