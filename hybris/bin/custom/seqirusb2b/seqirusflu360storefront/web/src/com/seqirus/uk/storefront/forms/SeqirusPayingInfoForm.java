/**
 * 
 */
package com.seqirus.storefront.forms;

/**
 * @author 172553
 *
 */
public class SeqirusPayingInfoForm
{
	private SeqirusAddressForm payingAddressForm;

	/**
	 * @return the payingAddressForm
	 */
	public SeqirusAddressForm getPayingAddressForm()
	{
		return payingAddressForm;
	}

	/**
	 * @param payingAddressForm the payingAddressForm to set
	 */
	public void setPayingAddressForm(SeqirusAddressForm payingAddressForm)
	{
		this.payingAddressForm = payingAddressForm;
	}
	
	
	
}
