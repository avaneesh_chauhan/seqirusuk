/**
 *
 */
package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.Collections;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seqirus.storefront.controllers.ControllerConstants;


/**
 * The Class SeqirusCDCForgotPasswordPageController.
 */
@Controller
@RequestMapping("/forgotpassword")
public class SeqirusCDCForgotPasswordPageController extends AbstractPageController
{

	/** The Constant BREADCRUMBS. */
	private static final String BREADCRUMBS = "breadcrumbs";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(SeqirusCDCForgotPasswordPageController.class);

	/** The Constant METADATA_CONTENT. */
	private static final String METADATA_CONTENT = "By creating an account with Seqirus, you can see specific product pricing, make a purchase, and manage your shipments easily.";

	/**
	 * Forgot password.
	 *
	 * @param model the model
	 * @return the string
	 * @throws CMSItemNotFoundException the CMS item not found exception
	 */
	@GetMapping
	public String forgotPassword(final Model model) throws CMSItemNotFoundException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("SeqirusCDCForgotPasswordPageController:forgotPassword() ");
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.FORGOT_PASSWORD_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.FORGOT_PASSWORD_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, METADATA_CONTENT);
		final Breadcrumb registerBreadcrumbEntry = new Breadcrumb("#", getMessageSource().getMessage("header.link.fogotpassword",
				null, getI18nService().getCurrentLocale()), null);
		model.addAttribute(BREADCRUMBS, Collections.singletonList(registerBreadcrumbEntry));
		return ControllerConstants.Views.Pages.Password.CDCForgotPasswordPage;
	}

}
