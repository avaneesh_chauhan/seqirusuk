/**
 * 
 */
package com.seqirus.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;

/**
 * @author 172553
 *
 */
public class SeqirusBusinessDetailsForm extends RegisterForm
{
	private String duns;
	private SeqirusAddressForm addressForm;
	
	/**
	 * @return the duns
	 */
	public String getDuns()
	{
		return duns;
	}
	/**
	 * @param duns the duns to set
	 */
	public void setDuns(String duns)
	{
		this.duns = duns;
	}
	/**
	 * @return the addressForm
	 */
	public SeqirusAddressForm getAddressForm()
	{
		return addressForm;
	}
	/**
	 * @param addressForm the addressForm to set
	 */
	public void setAddressForm(SeqirusAddressForm addressForm)
	{
		this.addressForm = addressForm;
	}
	
	
	
}
