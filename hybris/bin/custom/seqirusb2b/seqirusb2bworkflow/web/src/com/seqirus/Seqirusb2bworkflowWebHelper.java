/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus;

import org.apache.log4j.Logger;


/**
 * Simple test class to demonstrate how to include utility classes to your webmodule.
 */
public class Seqirusb2bworkflowWebHelper // NOSONAR
{
	/** Edit the local|project.properties to change logging behavior (properties log4j.*). */
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(Seqirusb2bworkflowWebHelper.class.getName());

	public static final String getTestOutput()
	{
		return "testoutput";
	}
}
