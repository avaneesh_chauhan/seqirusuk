/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.services.impl;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.core.model.user.EmployeeModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.dao.B2BRegistrationDao;
import com.seqirus.services.B2BRegistrationService;


/**
 * Default implementation of {@link B2BRegistrationService}
 */
public class DefaultB2BRegistrationService implements B2BRegistrationService
{

	private B2BRegistrationDao registrationDao;

	private EmailService emailService;

	/**
	 * @param registrationDao
	 *           the registrationDao to set
	 */
	@Required
	public void setRegistrationDao(final B2BRegistrationDao registrationDao)
	{
		this.registrationDao = registrationDao;
	}

	/**
	 * @param emailService
	 *           the emailService to set
	 */
	@Required
	public void setEmailService(final EmailService emailService)
	{
		this.emailService = emailService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.seqirus.services.B2BRegistrationService#getEmployeesInUserGroup(java.lang.String)
	 */
	@Override
	public List<EmployeeModel> getEmployeesInUserGroup(final String userGroup)
	{
		return registrationDao.getEmployeesInUserGroup(userGroup);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.seqirus.services.B2BRegistrationService#getEmailAddressesOfEmployees(java.util.List)
	 */
	@Override
	public List<EmailAddressModel> getEmailAddressesOfEmployees(final List<EmployeeModel> employees)
	{

		final List<EmailAddressModel> emails = new ArrayList<>();
		for (final EmployeeModel employee : employees)
		{
			if (StringUtils.isNotBlank(employee.getUid()))
			{
				final EmailAddressModel emailAddress = emailService.getOrCreateEmailAddressForEmail(employee.getUid().toLowerCase(),
						employee.getName());
				emails.add(emailAddress);
			}
		}
		/*
		 * for (final AddressModel address : Lists.newArrayList(employee.getE)) { if
		 * (BooleanUtils.isTrue(address.getContactAddress())) { if (StringUtils.isNotBlank(address.getEmail())) { final
		 * EmailAddressModel emailAddress = emailService.getOrCreateEmailAddressForEmail(address.getEmail(),
		 * employee.getName()); emails.add(emailAddress); } break; } }
		 */

		return emails;

	}


	@Override
	public List<SeqirusB2BTempCustomerModel> getRegistrationDetailsOfCustomer(final String userId)
	{
		return registrationDao.getRegistrationByUserId(userId);
	}
}
