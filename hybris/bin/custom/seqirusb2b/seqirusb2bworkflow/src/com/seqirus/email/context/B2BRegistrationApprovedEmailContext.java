/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import com.seqirus.model.B2BRegistrationApprovedProcessModel;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import de.hybris.platform.acceleratorcms.model.components.SimpleBannerComponentModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForTemplateModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Email context used to render B2B approval emails with reset password links
 */
public class B2BRegistrationApprovedEmailContext extends B2BRegistrationEmailContext
{

	private static final String CUSTOMER = "customer";

	private static final String COPYRIGHT = "copyright";
	private static final String SITE_LOGO = "sitelogo";
	private static final String ADDRESS = "address";
	private static final String HEADER = "header";


	private Converter<UserModel, CustomerData> customerConverter;
	private CustomerData customerData;
	private String passwordResetToken;
	
	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	public CustomerData getCustomer()
	{
		return customerData;
	}

	/**
	 * @return the passwordResetToken
	 */
	public String getPasswordResetToken()
	{
		return passwordResetToken;
	}

	/**
	 * @param passwordResetToken
	 *           the passwordResetToken to set
	 */
	public void setPasswordResetToken(final String passwordResetToken)
	{
		this.passwordResetToken = passwordResetToken;
	}

	/**
	 * @return The url-encoded representation of the reset password token
	 * @throws UnsupportedEncodingException
	 */
	public String getUrlEncodedToken() throws UnsupportedEncodingException
	{
		return URLEncoder.encode(getPasswordResetToken(), "UTF-8");
	}

	/**
	 * @return The full URL used to reset a password
	 * @throws UnsupportedEncodingException
	 */
	public String getSecureResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true,
				"/login/pw/change", "token=" + getUrlEncodedToken());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#init(de.hybris.platform.
	 * processengine.model.BusinessProcessModel, de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel)
	 */
	@Override
	public void init(final StoreFrontCustomerProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		/*if (businessProcessModel instanceof B2BRegistrationApprovedProcessModel)
		{
			final B2BRegistrationApprovedProcessModel registrationProcessModel = (B2BRegistrationApprovedProcessModel) businessProcessModel;
			setPasswordResetToken(registrationProcessModel.getPasswordResetToken());
		}*/
		customerData = getCustomerConverter().convert(getCustomer(businessProcessModel));

		if (null != customerData)
		{
			put(CUSTOMER, customerData);
		}
		final PageTemplateModel masterTemplate = emailPageModel.getMasterTemplate();
		final List<ContentSlotForTemplateModel> templateSlots = masterTemplate.getContentSlots();
		if (CollectionUtils.isNotEmpty(templateSlots))
		{
			for (final ContentSlotForTemplateModel contentSlotForTemplateModel : templateSlots)
			{
				final String position = contentSlotForTemplateModel.getPosition();
				final ContentSlotModel contentSlot = contentSlotForTemplateModel.getContentSlot();
				if (null != contentSlot)
				{
					final List<AbstractCMSComponentModel> components = contentSlot.getCmsComponents();
					if (CollectionUtils.isNotEmpty(components))
					{
						final AbstractCMSComponentModel component = components.get(0);
						if (StringUtils.equalsIgnoreCase(position, "SiteLogo"))
						{
							final SimpleBannerComponentModel siteLogoComponent = (SimpleBannerComponentModel) component;
							final String completeUrl = getMediaSecureBaseUrl() + siteLogoComponent.getMedia().getURL();
							put(SITE_LOGO, completeUrl);
						}
						else if (StringUtils.equalsIgnoreCase(position, "TopContent"))
						{
							put(HEADER, component);
						}
						else if (StringUtils.equalsIgnoreCase(position, "BottomContent"))
						{
							put(ADDRESS, component);
						}
						else if (StringUtils.equalsIgnoreCase(position, "CopyrightContent"))
						{
							put(COPYRIGHT, component);
						}
					}
				}
			}
		}

		put(CUSTOMER, customerData);
	}

}