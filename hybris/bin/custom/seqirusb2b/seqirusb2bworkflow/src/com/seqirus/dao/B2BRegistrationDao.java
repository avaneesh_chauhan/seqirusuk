/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.dao;

import de.hybris.platform.core.model.user.EmployeeModel;

import java.util.List;

import com.seqirus.core.model.SeqirusB2BTempCustomerModel;


/**
 * DAO with B2B registration specific methods
 */
public interface B2BRegistrationDao
{

	/**
	 * @param userGroup
	 *           The user group to look for
	 * @return All employees that are part of the specified user group
	 */
	public List<EmployeeModel> getEmployeesInUserGroup(String userGroup);

	/**
	 *
	 * @param userId
	 * @return the customer registration model
	 */
	public List<SeqirusB2BTempCustomerModel> getRegistrationByUserId(final String userId);


}
