
package com.seqirus.storefront.forms;


/**
 * @author 172553
 *
 */
public class JoinAccountForm
{
	private String accountNumber;
	private String orgName;
	private String zipCode;
	private String accessCode;
	private String orgReferralEmail;
	private String searchStatus;


	/**
	 * @return the orgReferralEmail
	 */
	public String getOrgReferralEmail()
	{
		return orgReferralEmail;
	}

	/**
	 * @param orgReferralEmail
	 *           the orgReferralEmail to set
	 */
	public void setOrgReferralEmail(final String orgReferralEmail)
	{
		this.orgReferralEmail = orgReferralEmail;
	}

	/**
	 * @return the searchStatus
	 */
	public String getSearchStatus()
	{
		return searchStatus;
	}

	/**
	 * @param searchStatus
	 *           the searchStatus to set
	 */
	public void setSearchStatus(final String searchStatus)
	{
		this.searchStatus = searchStatus;
	}

	/**
	 * @return the accessCode
	 */
	public String getAccessCode()
	{
		return accessCode;
	}

	/**
	 * @param accessCode
	 *           the accessCode to set
	 */
	public void setAccessCode(final String accessCode)
	{
		this.accessCode = accessCode;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber()
	{
		return accountNumber;
	}

	/**
	 * @param accountNumber
	 *           the accountNumber to set
	 */
	public void setAccountNumber(final String accountNumber)
	{
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the orgName
	 */
	public String getOrgName()
	{
		return orgName;
	}

	/**
	 * @param orgName
	 *           the orgName to set
	 */
	public void setOrgName(final String orgName)
	{
		this.orgName = orgName;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode()
	{
		return zipCode;
	}

	/**
	 * @param zipCode
	 *           the zipCode to set
	 */
	public void setZipCode(final String zipCode)
	{
		this.zipCode = zipCode;
	}

}
