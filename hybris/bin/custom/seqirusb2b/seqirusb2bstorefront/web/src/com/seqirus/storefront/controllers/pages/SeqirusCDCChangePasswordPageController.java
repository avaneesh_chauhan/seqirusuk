/**
 *
 */
package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.Collections;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seqirus.storefront.controllers.ControllerConstants;


/**
 * The Class SeqirusCDCChangePasswordPageController.
 */
@Controller
@RequestMapping("/changepassword")
public class SeqirusCDCChangePasswordPageController extends AbstractPageController
{

	/** The Constant BREADCRUMBS. */
	private static final String BREADCRUMBS = "breadcrumbs";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(SeqirusCDCChangePasswordPageController.class);


	/** The Constant METADATA_CONTENT. */
	private static final String METADATA_CONTENT = "By creating an account with Seqirus, you can see specific product pricing, make a purchase, and manage your shipments easily.";


	/** The Constant PASSWORD_CHANGE_CMS_PAGE. */
	private static final String PASSWORD_CHANGE_CMS_PAGE = "changepassword";


	/**
	 * Change password.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String changePassword(final Model model) throws CMSItemNotFoundException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("SeqirusCDCchangePasswordPageController:changePassword() ");
		}
		storeCmsPageInModel(model, getChangePasswordPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getChangePasswordPage());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, METADATA_CONTENT);
		final Breadcrumb registerBreadcrumbEntry = new Breadcrumb("#", getMessageSource().getMessage("header.link.changePassword",
				null, getI18nService().getCurrentLocale()), null);
		model.addAttribute(BREADCRUMBS, Collections.singletonList(registerBreadcrumbEntry));
		return getView();
	}


	/**
	 * Gets the change password page.
	 *
	 * @return the change password page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected AbstractPageModel getChangePasswordPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId(PASSWORD_CHANGE_CMS_PAGE);
	}


	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	protected String getView()
	{
		return ControllerConstants.Views.Pages.Password.CDCChangePasswordPage;
	}

}
