/**
 *
 */
package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seqirus.core.exceptions.SeqirusCustomException;
import com.seqirus.facades.invoice.SeqirusInvoiceFacade;
import com.seqirus.facades.invoice.data.SeqirusReturnsAndCreditsResponse;
import com.seqirus.facades.invoice.data.SeqirusReturnsAndCreditsResponseData;
import com.seqirus.facades.invoice.utils.SeqirusInvoiceUtils;
import com.seqirus.storefront.controllers.ControllerConstants;


/**
 * The Class SeqirusCDCChangePasswordPageController- Controller for Returns and Credit Page
 *
 * @author Avaneesh Chauhan
 */
@Controller
@RequestMapping("/returnsandcredit")
public class SeqirusReturnsAndCreditPageController extends AbstractPageController
{

	/** The Constant SEQIRUS_UK. */
	private static final String SEQIRUS_UK = "SeqirusUK";

	/** The Constant BREADCRUMBS. */
	private static final String BREADCRUMBS = "breadcrumbs";

	/** The session service. */
	@Resource
	private SessionService sessionService;

	/** The user service. */
	@Resource
	private UserService userService;


	/**
	 * Sets the user service.
	 *
	 * @param userService
	 *           the new user service
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


	/** The seqirus invoice facade. */
	@Resource(name = "seqirusInvoiceFacade")
	private SeqirusInvoiceFacade seqirusInvoiceFacade;

	/**
	 * Sets the seqirus invoice facade.
	 *
	 * @param seqirusInvoiceFacade
	 *           the new seqirus invoice facade
	 */
	public void setSeqirusInvoiceFacade(final SeqirusInvoiceFacade seqirusInvoiceFacade)
	{
		this.seqirusInvoiceFacade = seqirusInvoiceFacade;
	}


	/**
	 * Sets the session service.
	 *
	 * @param sessionService
	 *           the new session service
	 */
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(SeqirusReturnsAndCreditPageController.class);


	/** The Constant METADATA_CONTENT. */
	private static final String METADATA_CONTENT = "By creating an account with Seqirus, you can see specific product pricing, make a purchase, and manage your shipments easily.";


	/**
	 * Gets the returns and credits data for the user account
	 *
	 * @param model
	 *           the model
	 * @return the returns and credits page with data
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String getReturnsAndCredits(final Model model) throws CMSItemNotFoundException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("SeqirusReturnsAndCreditPageController:returnsandcredit() ");
		}
		try
		{
			final B2BCustomerModel b2bCustomer = (B2BCustomerModel) userService.getCurrentUser();
			final B2BUnitModel defaultB2bUnit = b2bCustomer.getDefaultB2BUnit();
			if (null != defaultB2bUnit && StringUtils.isNotBlank(defaultB2bUnit.getUid())
					&& !defaultB2bUnit.getUid().equalsIgnoreCase(SEQIRUS_UK))
			{
				//Get the Response Data from SAP ECC for Credit for the user's account
				final SeqirusReturnsAndCreditsResponseData returnsAndCreditData = seqirusInvoiceFacade
						.getReturnsAndCreditsResponse(defaultB2bUnit.getUid());
				if (null != returnsAndCreditData.getCreditList())
				{
					LOG.info("Data Fetched Successfully, total credit entries are:" + returnsAndCreditData.getCreditList().size());
				}
				returnsAndCreditData.setAccountName(defaultB2bUnit.getCompanyName());
				sessionService.setAttribute("returnsAndCreditResponse", returnsAndCreditData);
				model.addAttribute("creditsData", returnsAndCreditData);
			}
			else
			{
				model.addAttribute("creditsData", SeqirusInvoiceUtils.getNoCreditReponse(new SeqirusReturnsAndCreditsResponse()));
			}
			storeCmsPageInModel(model,
					getContentPageForLabelOrId(ControllerConstants.Views.Pages.ReturnsAndCredit.RETURNS_AND_CREDIT_CMS_PAGE));
			setUpMetaDataForContentPage(model,
					getContentPageForLabelOrId(ControllerConstants.Views.Pages.ReturnsAndCredit.RETURNS_AND_CREDIT_CMS_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, METADATA_CONTENT);
		}
		catch (final SeqirusCustomException e)
		{
			LOG.error("Error while fetching returns and credit data: ", e);
			GlobalMessages.addErrorMessage(model, e.getWsMessage());
			storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.GLOBAL_ERROR));
			return getViewForPage(model);
		}
		return getViewForPage(model);
	}

	/**
	 * Gets the invoices.
	 *
	 * @param model
	 *           the model
	 * @return the invoices
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/getCredits", method = RequestMethod.GET)
	@ResponseBody
	public SeqirusReturnsAndCreditsResponseData getInvoices(final Model model) throws CMSItemNotFoundException
	{
		return (SeqirusReturnsAndCreditsResponseData) sessionService.getAttribute("returnsAndCreditResponse");
	}


}
