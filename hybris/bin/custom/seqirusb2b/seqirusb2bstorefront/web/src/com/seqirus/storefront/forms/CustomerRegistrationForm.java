/**
 *
 */
package com.seqirus.storefront.forms;

import java.util.List;

/**
 * @author 700196
 *
 */
public class CustomerRegistrationForm
{
	private String companyName;
	private String companyType;
	private String companyRegNumber;
	private String businessType;
	private String otherBusinessType;
	private String vatNumber;
	private String nhcNumber;
	private Integer tradingSince;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private Integer phoneExt;
	private String jobTitle;
	private ContactsAndAddressesForm payingContactInfo;
	private ContactsAndAddressesForm billingContactInfo;
	private ContactsAndAddressesForm invoicingContractInfo;
	private List<ContactsAndAddressesForm> shippingLocations;
	private String shipLocationSubmitFlag;
	private String invoiceContactSubmitFlag;
	private String payerContactSubmitFlag;
	private String billingContactSubmitFlag;
	private String userName;
	private String userEmail;
	private String buildingStreet;
	private String additionalStreet;
	private String city;
	private String postalCode;
	private String country;
	private String lookup;
	private String accountnumber;
	private String sapAccessCode;
	private String updateCompanyInfoFlag;




	/**
	 * @return the otherBusinessType
	 */
	public String getOtherBusinessType()
	{
		return otherBusinessType;
	}

	/**
	 * @param otherBusinessType
	 *           the otherBusinessType to set
	 */
	public void setOtherBusinessType(final String otherBusinessType)
	{
		this.otherBusinessType = otherBusinessType;
	}

	/**
	 * @return the sapAccessCode
	 */
	public String getSapAccessCode()
	{
		return sapAccessCode;
	}

	/**
	 * @param sapAccessCode
	 *           the sapAccessCode to set
	 */
	public void setSapAccessCode(final String sapAccessCode)
	{
		this.sapAccessCode = sapAccessCode;
	}

	/**
	 * @return the accountnumber
	 */
	public String getAccountnumber()
	{
		return accountnumber;
	}

	/**
	 * @param accountnumber
	 *           the accountnumber to set
	 */
	public void setAccountnumber(final String accountnumber)
	{
		this.accountnumber = accountnumber;
	}

	/**
	 * @return the lookup
	 */
	public String getLookup()
	{
		return lookup;
	}

	/**
	 * @param lookup the lookup to set
	 */
	public void setLookup(final String lookup)
	{
		this.lookup = lookup;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName()
	{
		return companyName;
	}

	/**
	 * @param companyName
	 *           the companyName to set
	 */
	public void setCompanyName(final String companyName)
	{
		this.companyName = companyName;
	}

	/**
	 * @return the companyType
	 */
	public String getCompanyType()
	{
		return companyType;
	}

	/**
	 * @param companyType
	 *           the companyType to set
	 */
	public void setCompanyType(final String companyType)
	{
		this.companyType = companyType;
	}

	/**
	 * @return the companyRegNumber
	 */
	public String getCompanyRegNumber()
	{
		return companyRegNumber;
	}

	/**
	 * @param companyRegNumber the companyRegNumber to set
	 */
	public void setCompanyRegNumber(final String companyRegNumber)
	{
		this.companyRegNumber = companyRegNumber;
	}

	/**
	 * @return the businessType
	 */
	public String getBusinessType()
	{
		return businessType;
	}

	/**
	 * @param businessType
	 *           the businessType to set
	 */
	public void setBusinessType(final String businessType)
	{
		this.businessType = businessType;
	}


	/**
	 * @return the vatNumber
	 */
	public String getVatNumber()
	{
		return vatNumber;
	}

	/**
	 * @param vatNumber
	 *           the vatNumber to set
	 */
	public void setVatNumber(final String vatNumber)
	{
		this.vatNumber = vatNumber;
	}


	/**
	 * @return the tradingSince
	 */
	public Integer getTradingSince()
	{
		return tradingSince;
	}

	/**
	 * @param tradingSince
	 *           the tradingSince to set
	 */
	public void setTradingSince(final Integer tradingSince)
	{
		this.tradingSince = tradingSince;
	}

	/**
	 * @return the payingContactInfo
	 */
	public ContactsAndAddressesForm getPayingContactInfo()
	{
		return payingContactInfo;
	}

	/**
	 * @param payingContactInfo
	 *           the payingContactInfo to set
	 */
	public void setPayingContactInfo(final ContactsAndAddressesForm payingContactInfo)
	{
		this.payingContactInfo = payingContactInfo;
	}

	/**
	 * @return the billingContactInfo
	 */
	public ContactsAndAddressesForm getBillingContactInfo()
	{
		return billingContactInfo;
	}

	/**
	 * @param billingContactInfo
	 *           the billingContactInfo to set
	 */
	public void setBillingContactInfo(final ContactsAndAddressesForm billingContactInfo)
	{
		this.billingContactInfo = billingContactInfo;
	}

	/**
	 * @return the invoicingContractInfo
	 */
	public ContactsAndAddressesForm getInvoicingContractInfo()
	{
		return invoicingContractInfo;
	}

	/**
	 * @param invoicingContractInfo
	 *           the invoicingContractInfo to set
	 */
	public void setInvoicingContractInfo(final ContactsAndAddressesForm invoicingContractInfo)
	{
		this.invoicingContractInfo = invoicingContractInfo;
	}

	/**
	 * @return the shippingLocations
	 */
	public List<ContactsAndAddressesForm> getShippingLocations()
	{
		return shippingLocations;
	}

	/**
	 * @param shippingLocations
	 *           the shippingLocations to set
	 */
	public void setShippingLocations(final List<ContactsAndAddressesForm> shippingLocations)
	{
		this.shippingLocations = shippingLocations;
	}

	/**
	 * @return the nhcNumber
	 */
	public String getNhcNumber()
	{
		return nhcNumber;
	}

	/**
	 * @param nhcNumber
	 *           the nhcNumber to set
	 */
	public void setNhcNumber(final String nhcNumber)
	{
		this.nhcNumber = nhcNumber;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *           the phoneNumber to set
	 */
	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the phoneExt
	 */
	public Integer getPhoneExt()
	{
		return phoneExt;
	}

	/**
	 * @param phoneExt
	 *           the phoneExt to set
	 */
	public void setPhoneExt(final Integer phoneExt)
	{
		this.phoneExt = phoneExt;
	}

	/**
	 * @return the jobTitle
	 */
	public String getJobTitle()
	{
		return jobTitle;
	}

	/**
	 * @param jobTitle
	 *           the jobTitle to set
	 */
	public void setJobTitle(final String jobTitle)
	{
		this.jobTitle = jobTitle;
	}

	/**
	 * @return the shipLocationSubmitFlag
	 */
	public String getShipLocationSubmitFlag()
	{
		return shipLocationSubmitFlag;
	}

	/**
	 * @param shipLocationSubmitFlag the shipLocationSubmitFlag to set
	 */
	public void setShipLocationSubmitFlag(final String shipLocationSubmitFlag)
	{
		this.shipLocationSubmitFlag = shipLocationSubmitFlag;
	}

	/**
	 * @return the invoiceContactSubmitFlag
	 */
	public String getInvoiceContactSubmitFlag()
	{
		return invoiceContactSubmitFlag;
	}

	/**
	 * @param invoiceContactSubmitFlag the invoiceContactSubmitFlag to set
	 */
	public void setInvoiceContactSubmitFlag(final String invoiceContactSubmitFlag)
	{
		this.invoiceContactSubmitFlag = invoiceContactSubmitFlag;
	}

	/**
	 * @return the payerContactSubmitFlag
	 */
	public String getPayerContactSubmitFlag()
	{
		return payerContactSubmitFlag;
	}

	/**
	 * @param payerContactSubmitFlag the payerContactSubmitFlag to set
	 */
	public void setPayerContactSubmitFlag(final String payerContactSubmitFlag)
	{
		this.payerContactSubmitFlag = payerContactSubmitFlag;
	}

	/**
	 * @return the billingContactSubmitFlag
	 */
	public String getBillingContactSubmitFlag()
	{
		return billingContactSubmitFlag;
	}

	/**
	 * @param billingContactSubmitFlag the billingContactSubmitFlag to set
	 */
	public void setBillingContactSubmitFlag(final String billingContactSubmitFlag)
	{
		this.billingContactSubmitFlag = billingContactSubmitFlag;
	}

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(final String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return the userEmail
	 */
	public String getUserEmail()
	{
		return userEmail;
	}

	/**
	 * @param userEmail the userEmail to set
	 */
	public void setUserEmail(final String userEmail)
	{
		this.userEmail = userEmail;
	}

	/**
	 * @return the buildingStreet
	 */
	public String getBuildingStreet()
	{
		return buildingStreet;
	}

	/**
	 * @param buildingStreet
	 *           the buildingStreet to set
	 */
	public void setBuildingStreet(final String buildingStreet)
	{
		this.buildingStreet = buildingStreet;
	}

	/**
	 * @return the additionalStreet
	 */
	public String getAdditionalStreet()
	{
		return additionalStreet;
	}

	/**
	 * @param additionalStreet
	 *           the additionalStreet to set
	 */
	public void setAdditionalStreet(final String additionalStreet)
	{
		this.additionalStreet = additionalStreet;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *           the postalCode to set
	 */
	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *           the country to set
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * @return the updateCompanyInfoFlag
	 */
	public String getUpdateCompanyInfoFlag()
	{
		return updateCompanyInfoFlag;
	}

	/**
	 * @param updateCompanyInfoFlag
	 *           the updateCompanyInfoFlag to set
	 */
	public void setUpdateCompanyInfoFlag(final String updateCompanyInfoFlag)
	{
		this.updateCompanyInfoFlag = updateCompanyInfoFlag;
	}





}
