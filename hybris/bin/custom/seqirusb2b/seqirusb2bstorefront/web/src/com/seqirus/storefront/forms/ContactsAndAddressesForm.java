package com.seqirus.storefront.forms;

/**
 * ContactsAndAddressesForm are using Customer Registration process
 *
 */
public class ContactsAndAddressesForm
{


	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String mobilePhone;
	private String jobTitle;
	private String organizationName;
	private String additionalEmail;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String postalCode;
	private Integer phoneExt;
	private String country;
	private String licenseNumber;
	private String licenseName;
	private String nhsCode;
	private String deletionFlag;
	private String addressID;


	/**
	 * @return the deletionFlag
	 */
	public String getDeletionFlag()
	{
		return deletionFlag;
	}

	/**
	 * @param deletionFlag
	 *           the deletionFlag to set
	 */
	public void setDeletionFlag(final String deletionFlag)
	{
		this.deletionFlag = deletionFlag;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone()
	{
		return phone;
	}

	/**
	 * @param phone
	 *           the phone to set
	 */
	public void setPhone(final String phone)
	{
		this.phone = phone;
	}

	/**
	 * @return the jobTitle
	 */
	public String getJobTitle()
	{
		return jobTitle;
	}

	/**
	 * @param jobTitle
	 *           the jobTitle to set
	 */
	public void setJobTitle(final String jobTitle)
	{
		this.jobTitle = jobTitle;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1()
	{
		return addressLine1;
	}

	/**
	 * @param addressLine1
	 *           the addressLine1 to set
	 */
	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2()
	{
		return addressLine2;
	}

	/**
	 * @param addressLine2
	 *           the addressLine2 to set
	 */
	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *           the postalCode to set
	 */
	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the phoneExt
	 */
	public Integer getPhoneExt()
	{
		return phoneExt;
	}

	/**
	 * @param phoneExt
	 *           the phoneExt to set
	 */
	public void setPhoneExt(final Integer phoneExt)
	{
		this.phoneExt = phoneExt;
	}

	/**
	 * @return the additionalEmail
	 */
	public String getAdditionalEmail()
	{
		return additionalEmail;
	}

	/**
	 * @param additionalEmail
	 *           the additionalEmail to set
	 */
	public void setAdditionalEmail(final String additionalEmail)
	{
		this.additionalEmail = additionalEmail;
	}


	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *           the country to set
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the organizationName
	 */
	public String getOrganizationName()
	{
		return organizationName;
	}

	/**
	 * @param organizationName
	 *           the organizationName to set
	 */
	public void setOrganizationName(final String organizationName)
	{
		this.organizationName = organizationName;
	}

	/**
	 * @return the licenseNumber
	 */
	public String getLicenseNumber()
	{
		return licenseNumber;
	}

	/**
	 * @param licenseNumber
	 *           the licenseNumber to set
	 */
	public void setLicenseNumber(final String licenseNumber)
	{
		this.licenseNumber = licenseNumber;
	}

	/**
	 * @return the licenseName
	 */
	public String getLicenseName()
	{
		return licenseName;
	}

	/**
	 * @param licenseName
	 *           the licenseName to set
	 */
	public void setLicenseName(final String licenseName)
	{
		this.licenseName = licenseName;
	}

	/**
	 * @return the nhsCode
	 */
	public String getNhsCode()
	{
		return nhsCode;
	}

	/**
	 * @param nhsCode
	 *           the nhsCode to set
	 */
	public void setNhsCode(final String nhsCode)
	{
		this.nhsCode = nhsCode;
	}

	/**
	 * @return the addressID
	 */
	public String getAddressID()
	{
		return addressID;
	}

	/**
	 * @param addressID
	 *           the addressID to set
	 */
	public void setAddressID(final String addressID)
	{
		this.addressID = addressID;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone()
	{
		return mobilePhone;
	}

	/**
	 * @param mobilePhone
	 *           the mobilePhone to set
	 */
	public void setMobilePhone(final String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}



}
