/**
 *
 */
package com.seqirus.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seqirus.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.core.exceptions.SeqirusCustomException;
import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.model.SeqirusSeasonModel;
import com.seqirus.core.registration.service.SeqirusCustomerRegistrationService;
import com.seqirus.facades.invoice.SeqirusInvoiceFacade;
import com.seqirus.facades.invoice.data.SeqirusInvoiceDetailsResponseData;
import com.seqirus.facades.invoice.data.SeqirusInvoiceLandingResponseData;
import com.seqirus.storefront.controllers.ControllerConstants;

/**
 * @author 700196
 *
 */

@Controller
@RequestMapping(value = "/")
public class SeqirusInvoicePageController extends AbstractSearchPageController
{
	private static final Logger LOGGER = Logger.getLogger(SeqirusInvoicePageController.class);

	@Resource(name = "seqirusInvoiceFacade")
	private SeqirusInvoiceFacade seqirusInvoiceFacade;

	@Resource
	private SessionService sessionService;

	@Autowired
	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;

	@Autowired
	protected ConfigurationService configurationService;

	@RequestMapping(value = "/invoices", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getInvoice(final Model model, @RequestParam(value = "season", required = false)
	String season) throws CMSItemNotFoundException
	{
		try {
			if (null == season)
			{
				season = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.DEFAULT_SEASONENTRY);
			}
			final SeqirusInvoiceLandingResponseData invoiceLandingResponseData = seqirusInvoiceFacade.retrieveInvoiceList(season);
   		sessionService.setAttribute("invoiceLandingResponse", invoiceLandingResponseData);
   		model.addAttribute("invoiceList", invoiceLandingResponseData);
			final SeasonEntryModel seasonEntry = seqirusCustomerRegistrationService.getSeasonEntry();
			final List<SeqirusSeasonModel> seasonList = (seasonEntry == null ? null : seasonEntry.getSeasonList());
			model.addAttribute("seasonList", seasonList);
			model.addAttribute("presentSeason", season);
			model.addAttribute("seasonEntry", seasonEntry);
   		storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.Invoice.INVOICS_LANDING_CMS_PAGE));
   		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.Invoice.INVOICS_LANDING_CMS_PAGE));
		}
   	catch (final SeqirusCustomException e)
   	{
			LOGGER.error("Error while fetching invoices : ", e);
   		GlobalMessages.addErrorMessage(model,e.getWsMessage());
			storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.CmsPage.GLOBAL_ERROR));
			return getViewForPage(model);
   	}
		return getViewForPage(model);
	}

	@RequestMapping(value = "/getInvoices", method = RequestMethod.GET)
	@ResponseBody
	public SeqirusInvoiceLandingResponseData getInvoices(final Model model) throws CMSItemNotFoundException
	{
		return (SeqirusInvoiceLandingResponseData) sessionService.getAttribute("invoiceLandingResponse");
	}

	@RequestMapping(value = "/invoicesDetails", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getInvoiceDetails(final Model model,@RequestParam(value = "invoiceNumber") final String invoiceNumber) throws CMSItemNotFoundException
	{
		final SeqirusInvoiceDetailsResponseData invoiceDetailsResponseData= seqirusInvoiceFacade.retrieveInvoiceDetails(invoiceNumber);
		model.addAttribute("invoiceDetailsList", invoiceDetailsResponseData);
		storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.Invoice.INVOICS_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.Invoice.INVOICS_DETAILS_CMS_PAGE));
		return getViewForPage(model);
	}

	/**
	 * download invocie for invoice number
	 *
	 * @param request
	 * @param response
	 * @param invoiceNumber
	 * @throws IOException
	 */
	@RequestMapping(value = "/downloadInvoice", method = RequestMethod.GET)
	@RequireHardLogIn
	public void downloadInvoice(final HttpServletRequest request, final HttpServletResponse response,
			@RequestParam(value = "invoiceNumber", required = true)
			final String invoiceNumber) throws IOException
	{
		try
		{
			final String fileName = invoiceNumber + ".pdf";
			final InputStream is = seqirusInvoiceFacade.downloadInvoice(invoiceNumber);
			response.setContentType("application/application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
			is.close();
		}
		catch (final SeqirusCustomException e)
		{
			LOGGER.error("Error while fetching invoices : ", e);
		}
		finally
		{
			//Remove Cache
			final RequestCache requestCache = new HttpSessionRequestCache();
			requestCache.removeRequest(request, response);
		}


	}

}
