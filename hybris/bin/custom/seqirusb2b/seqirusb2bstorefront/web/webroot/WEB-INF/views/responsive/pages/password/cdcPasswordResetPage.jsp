<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/responsive/nav/breadcrumb"%>
  
<template:page pageTitle="${pageTitle}">
		<div id="breadcrumb" class="col-sm-12 col-md-12">
			<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}" />
		</div>
			<div class="container-fluid maincontent col-xs-12">
				<div class="pageHeader col-xs-12"></div>
				 <div class="account-section">
				  <div class="contentArea hideBoxshadow col-xs-11">
			<cms:pageSlot position="LeftContentSlot" var="component" element="div" class="hidden-xs hidden-sm col-md-4 leftimagecontainer">
				<div class="loginleftimage" style="background-image: url(${component.media.url})">
				<h1 class="leftHeader col-xs-11">${component.headline}</h1>
				<p class="leftsubHeader col-xs-11">${component.content}</p>
				</div>
				<cms:component component="${feature}" />
			</cms:pageSlot>

			 <cms:pageSlot position="BodyContent" var="feature" element="div" class="account-section-content col-xs-12 col-md-8">
                <cms:component component="${feature}" />
            </cms:pageSlot>
          <cms:pageSlot position="BottomContent" var="feature" element="div" class="accountPageBottomContent">
            <cms:component component="${feature}" />
        </cms:pageSlot>
			 </div>
			</div>
		</div>
</template:page>