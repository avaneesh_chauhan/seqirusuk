<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<div id="invoicingContact" class="tabcontent">
               <div class="section1_header col-xs-12">
                  <spring:theme code="form.register.invoice.header" />
               </div>
               <div class="section1_subheader col-xs-12">
                   <spring:theme code="form.register.invoice.subHeader" />
                  <br>   <input type="radio" id="payinginfo2" name="invoiceinfo" value="<spring:theme code="form.register.invoice.radioText" />">
                  <label class="radioLabel" for="payinginfo2"><spring:theme code="form.register.invoice.radioText" /></label>
               </div>
             
                  <div class="col-xs-12 businessformContent">
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="invoicingFirstName"><spring:theme code="form.register.invoice.firstName" /></label><br/>
                              <input  class="form-control textonly" id="invoicingFirstName" name="invoicingContractInfo.firstName" placeholder="Edward"  type="text"/>                              
                           </div>
                           <div class="form-group col-md-4">
                              <label for="invoicingLastName"><spring:theme code="form.register.invoice.lastName" /></label><br/>
                              <input class="form-control textonly" id="invoicingLastName" name="invoicingContractInfo.lastName" placeholder="Newgate"  type="text"/> 
                           </div>
                        </div>
                     </div> 

                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="invoicingEmail"><spring:theme code="form.register.invoice.email" /><span class="asteriskred">*</span></label><br/>
                              <input class="form-control required-fld-form3 email_input emailcheck3" autocomplete="no" id="invoicingEmail" name="invoicingContractInfo.email" data-error="Please enter valid Email" placeholder="email@example.co.uk"  type="email" required/>
                              <div class="help-block" id='emailerr3'></div>                                  
                           </div>
                           <div class="form-group col-md-4">
                              <label for="invoicingphoneNumber"><spring:theme code="form.register.invoice.phoneNum" /></label><br/>
                              <input  class="form-control phone-not-man"  autocomplete="no" id="invoicingphoneNumber" name="invoicingContractInfo.phone" maxlength="11"  type="text"/> 
                           	  <div class="help-block" id="message3"></div>
                           </div>
                           <div class="form-group col-md-2">
                              <label for="invoicingphoneext"><spring:theme code="form.register.invoice.phoneExt" /></label><br/>
                              <input  class="form-control numberonly" id="invoicingphoneext" name="invoicingContractInfo.phoneExt" placeholder="1234" maxlength="4" type="text"/>  
                           </div>
                        </div>
                     </div> 
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="invoicingjobtitle"><spring:theme code="form.register.invoice.jobTitle" /></label><br/>
                              <input  class="form-control textonly" id="invoicingjobtitle" name="invoicingContractInfo.jobTitle" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.business.jobtitleDesc" />" placeholder="E.g. Vaccine Buyer"  type="text"/>                             
                           </div>
                           <div class="form-group col-md-4"> 
                              <label for="invoicingorganisationname"><spring:theme code="form.register.invoice.orgName" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form3 with_space" id="invoicingorganisationname" name="invoicingContractInfo.organizationName" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.business.orgName" />" placeholder="Practice, Pharmacy or Business Name"  data-error="Please enter Organisation Name" type="text" required/>   
                           	  <div class="help-block with-errors"></div> 
                           </div>
                        </div>
                     </div>
                     
                     
                     
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                        	<div class="form-group col-md-4">
                              <label for="invoicingAdditinalemail"><spring:theme code="form.register.invoice.otherEmail" /></label><br/>
                              <input  class="form-control emailcheck_step1" id="invoicingAdditinalemail" name="invoicingContractInfo.additionalEmail" data-error="Please enter valid Email" placeholder="email@example.co.uk."  type="email"/>
                              <div class="help-block" id="addmail"></div>                               
                           </div> 
                     </div>
                     </div>  
                     <div class="row">
                        <div class="col-md-5 col-xs-12">
                           <div class="form-group col-md-12">
                              <label for="Address">Address Lookup</label>
                              <div class="input-group spcl-addon">  
                              <div class="input-group-addon"><i class="fa fa-search"></i></div>
                              <input class="form-control" style="margin-bottom:0px;" id="addresslookupInvoice" name="invoicingContractInfo.addressLookUp" placeholder="Search" type="text">
                              </div>
                             
                           </div>
                        </div>
                     </div>    
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                                                   
                           <div class="form-group col-md-4">
                              <label for="invoicingaddress"><spring:theme code="form.register.invoice.addr" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form3" id="invoicingaddress" name="invoicingContractInfo.addressLine1" data-error="Please enter Address" placeholder="Building Number & Street"  type="text" required/>
                           	  <div class="help-block with-errors"></div> 
                           </div>
                           <div class="form-group col-md-4">
                              <label for="invoicingstreet"><spring:theme code="form.register.invoice.additionalStreet" /></label><br/>
                              <input  class="form-control" id="invoicingstreet" name="invoicingContractInfo.addressLine2" placeholder="Building or Office name/number"  type="text"/> 
                           </div>
                           
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="invoicingCity"><spring:theme code="form.register.invoice.city" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form3 textonly" id="invoicingCity" name="invoicingContractInfo.city" placeholder="City" data-error="Please enter City"  type="text" required/>
                          	  <div class="help-block with-errors"></div> 
                           </div>
                           <div class="form-group col-md-2">
                              <label for="invoicingpost"><spring:theme code="form.register.invoice.postCode" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form3 post_not_man" autocomplete="no" id="invoicingpost" name="invoicingContractInfo.postalCode" data-error="Please enter valid Post Code" placeholder="SL16 8AA"  type="text" required/>   
                              <div class="help-block with-errors"></div>  
                           </div>
                           <div class="form-group col-md-3">
                              <label for="invoicingcountry"><spring:theme code="form.register.invoice.country" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form3 textonly" id="invoicingcountry" name="invoicingContractInfo.country" placeholder="Country" data-error="Please enter Country"   type="text" required/>   
                           	  <div class="help-block with-errors"></div> 
                           </div>
                        </div>
                     </div>  
                  </div>
              
               <div class="col-xs-12 previosnmail">
               <div class="prevStepbtn pagi-link sub-6" data-section-id="section-2" data-img-id="img-2">< <spring:theme code="form.register.prev" /></div>
               <button type="submit" class="step2 nxt3_3 Nextbutton3 sub-4"  id="gotosection_5"><spring:theme code="form.register.next" /></button>
               <%-- <span class="skiptext"><a  class="step4 skiplink pagi-link sub-4" href="javaScript:void(0);" data-section-id="section-5" data-img-id="img-3" id="invoiceSkipForNow"><spring:theme code="form.register.skip" /></a></span> --%>
               </div>
               </div>
