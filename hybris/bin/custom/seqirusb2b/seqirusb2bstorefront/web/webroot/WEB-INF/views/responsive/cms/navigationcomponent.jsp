<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:set value="${fn:escapeXml(component.styleClass)}"
	var="navigationClassHtml" />
<c:if test="${component.visible}">
	 <div class="col-md-9 col-xs-12 navcolumn">
	 	<!-- <div class="align-menu">
             <div class="mob-menu">MENU</div>
         </div> -->
		<nav class="primary-nav mob-navigation">
			 <ul class="navbar top-nav">
				<c:forEach items="${component.navigationNode.children}"
					var="navNode">
					 <c:choose>
						<c:when test="${headerHideStatus ne 'hide' && not empty navNode.children}">
							<li class="dropdown"><a href="#" class="dropbtn">${navNode.name}<span class="redarrw"><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
								<div class="dropdown-content">		
									<c:forEach items="${navNode.children}" var="childNavNode">
										<a href="${childNavNode.entries[0].item.url}">${childNavNode.name}</a>
									</c:forEach>
								</div>
							</li>
						</c:when>
						 <c:when test="${headerHideStatus eq 'hide' && (navNode.name=='PRODUCTS & RESOURCES'||navNode.name=='SUPPORT')}">
								<li>
									<a href="${navNode.entries[0].item.url}">${navNode.name}</a>
								</li>	
						</c:when>
						 <c:when test="${headerHideStatus ne 'hide'}">
							<li>
								<a href="${navNode.entries[0].item.url}">${navNode.name}</a>
							</li>
						</c:when> 
					</c:choose> 
				</c:forEach>
			</ul>
		</nav>
	</div>
</c:if>