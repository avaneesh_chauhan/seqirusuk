<div class="col-md-6 col-xs-12 greyboxMar">
	<div>
		<img class="greyboximg" src="${feature.portalBenefitsImage.url}"
			alt="Resource" />
	</div>
	<h3 class="secondary-header">${feature.text1}</h3>
	<p class="headerpara">${feature.text2}</p>
	<div style="margin-top:42px;">
		<a class="whitebtn" href="${contextPath}/about">See Features</a>
	</div>
</div>
