<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<template:page pageTitle="${pageTitle}">
  
         

<div class="container-fluid body-section body-section-resource">
      <div class="container">
         <nav aria-label="supportbreadcrumb" >
            <ol class="supportbreadcrumb">
            <c:url value="/" var="homeUrl" />
               <li class="supportbreadcrumb-item active"><a href="${homeUrl}" class=""><spring:theme
						code="form.register.homeLabel" /></a></li>
               <li class="supportbreadcrumb-item active"><a href="${contextPath}/productresources" class="">Products & Resources</a></li>
               <li class="supportbreadcrumb-item" aria-current="page">Resources</li>
            </ol>
         </nav>
         <div class="row">
            <div class="col-md-12 marBottom40">
               <h2 class="contactheader">
               		<cms:pageSlot position="Section1EF" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot></h2>  
            </div>
            <div class="col-md-3 no-pad">
               <div class="left-side marBottom40">
              <div class="heading">Jump to</div>
              <div class="left-nav-wrap">
                 <ul>
                    <li><a href="#Immunization_Clinic_Planning" class="active">Immunization Clinic Planning</a></li>
                    <li><a href="#Vaccine_Management">Vaccine Management</a></li>
                    <li><a href="#Finnancial_Guidance">Financial Guidance</a></li>
                    <li><a href="#Product_Returns">Product Returns</a></li>
                 </ul>
              </div>
              </div>
            </div>
            
            <!-- Immunization_Clinic_Planning Start -->
            <div class="col-md-9 right-side marBottom40">
               <div id="Immunization_Clinic_Planning" class="block-content marBottom40 section">
                  <div class="heading-title">
                  	<cms:pageSlot position="Section1EA" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot></div>
                  <div class="content-section marBottom40">
                  <cms:pageSlot position="Section3L" var="component">
                     <div class="content-head">${component.h2content}</div>
                     <div class="row marBottom40">
                     <div class="col-md-7">
                     ${component.paragraphcontent}
                     </div>
                     <div class="col-md-5 pos-right"><a href="${component.mediaForpdf.downloadurl}" class="download_button">Download All</a></div>
                     </div>
                     </cms:pageSlot>
                     <div class="row view_resource_panel" id="panel-1">
                        <div class="col-md-12">
                           <table class="col" id="toolkitTable">
                           <tr>
                           <cms:pageSlot position="Section3Z" var="component">
               <c:forEach items="${component.resourceContentList}" var="comp">
									<!--<c:if test="${empty comp.media.url}">-->
                              	 <td>${comp.paragraphcontent}</td>
                                 <td><a href="${comp.mediaForpdf.downloadurl}" class="dwnld" style="color:red;">Download PDF</a></td>
                                 
                                 
                              <!--</c:if>-->
								</c:forEach>
							</cms:pageSlot></tr>
							    <tr>
                           <cms:pageSlot position="Section3Z1" var="component">
               <c:forEach items="${component.resourceContentList}" var="comp">
									<!--<c:if test="${empty comp.media.url}">-->
                              	 <td>${comp.paragraphcontent}</td>
                                 <td><a href="${comp.mediaForpdf.downloadurl}" class="dwnld" style="color:red;">Download PDF</a></td>
                                 
                                 
                              <!--</c:if>-->
								</c:forEach>
							</cms:pageSlot></tr>
                             
                             
                             
                              <tr>
                              <cms:pageSlot position="Section7N" var="component">
               <c:forEach items="${component.resourceContentList}" var="comp">
									<!--<c:if test="${empty comp.media.url}">-->
                                 <td>${comp.paragraphcontent}</td>
                                <td><a href="${comp.mediaForpdf.downloadurl}" class="dwnld" style="color:red;">Download PDF</a></td>
                                 
                              <!--</c:if>-->
								</c:forEach>
							</cms:pageSlot></tr>
							      <tr>
                              <cms:pageSlot position="Section7N1" var="component">
               <c:forEach items="${component.resourceContentList}" var="comp">
									<!--<c:if test="${empty comp.media.url}">-->
                                 <td>${comp.paragraphcontent}</td>
                                 <td><a href="${comp.mediaForpdf.downloadurl}" class="dwnld" style="color:red;">Download PDF</a></td>
                                 
                              <!--</c:if>-->
								</c:forEach>
							</cms:pageSlot></tr>
                              <!-- <tr>
                                 <td>Resource title lorem ipsum sit amet</td>
                                 <td>Download PDF</td>
                                 <td>Resource title lorem ipsum sit amet</td>
                                 <td>Download PDF</td>
                              </tr>-->
                           </table>
                        </div>
                     </div>
                    <div class="row view-resource-pos">
                     <div class="col-md-12 pos-mid view-resource">
                        <span class="down-slide" data-panel-id="panel-1">View Resources</span><br/><i class="fa fa-chevron-circle-down toggle-resource"  aria-hidden="true"></i>
                     </div>
                    </div>
                  </div>
				<!-- Immunization_Clinic_Planning ends -->
				<!-- Clinic Toolkit Start -->
                  
                  <div class="content-section marBottom40">
					 <div class="row marBottom40">
                     <div class="col-md-5">
                     <cms:pageSlot position="Section3L1" var="component">
                     <div class="content-head">${component.h2content}</div>   
                     <span class="marBottom40">${component.paragraphcontent}</span>
                     <a href="${component.mediaForpdf.downloadurl}" class="download_button">Download All</a>
                     </div>
                     </cms:pageSlot>
                     
                     
                     
                     
               
                     <div class="col-md-7 pos-right-video">
                              <cms:pageSlot position="Section5AC" var="component">
               <c:forEach items="${component.contentList}" var="comp">
									<c:if test="${empty comp.media.url}">
									
									
                        <div class="col-md-6 video-block">
                           <div class="video-container">${comp.h2content}</div>
                           <div class="video-text">
                              <div class="vid-title">${comp.paragraphcontent}</div>
                              <div class="vid-content">${comp.text3}</div>
                             <a href="${comp.urlLink}" class="dwnld"><div class="vid-download">Download <i class="fa fa-arrow-down" aria-hidden="true"></i></div></a> 
                           </div>
                        </div></c:if>
								</c:forEach>
							</cms:pageSlot>
                     
                       <!--  <div class="col-md-6 video-block">
                           <div class="video-container">FPO Video</div>
                           <div class="video-text">
                              <div class="vid-title">Safety Guidelines</div>
                              <div class="vid-content">Short Video description will be here</div>
                             <a href="#" class="dwnld"><div class="vid-download">Download <i class="fa fa-arrow-down" aria-hidden="true"></i></div></a> 
                           </div>
                        </div>

                        <div class="col-md-6 video-block">
                           <div class="video-container">FPO Video</div>
                           <div class="video-text">
                              <div class="vid-title">Safety Guidelines</div>
                              <div class="vid-content">Short Video description will be here</div>
                             <a href="#" class="dwnld"><div class="vid-download">Download <i class="fa fa-arrow-down" aria-hidden="true"></i></div></a> 
                           </div>
                        </div>

                        <div class="col-md-6 video-block">
                           <div class="video-container">FPO Video</div>
                           <div class="video-text">
                              <div class="vid-title">Safety Guidelines</div>
                              <div class="vid-content">Short Video description will be here</div>
                             <a href="#" class="dwnld"><div class="vid-download">Download <i class="fa fa-arrow-down" aria-hidden="true"></i></div></a> 
                           </div>
                        </div>

                        <div class="col-md-6 video-block">
                           <div class="video-container">FPO Video</div>
                           <div class="video-text">
                              <div class="vid-title">Safety Guidelines</div>
                              <div class="vid-content">Short Video description will be here</div>
                             <a href="#" class="dwnld"><div class="vid-download">Download <i class="fa fa-arrow-down" aria-hidden="true"></i></div></a> 
                           </div>
                        </div>-->
                     
                     </div>
                  
                  
                  
                  
                  
                     </div>
                     
                  </div>
					<!-- Clinic Toolkit ends -->
					<!-- Clinic Toolkit print material starts -->   
                  <div class="content-section marBottom40">
                  <cms:pageSlot position="Section3L6" var="component">
                     <div class="content-head">${component.h2content}</div>
                     <div class="row marBottom40">
                     <div class="col-md-7">
                     ${component.paragraphcontent}
                     </div>
                     <div class="col-md-3 pos-mid"><a href="${component.mediaForpdf.downloadurl}" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                     <div class="col-md-2 pos-mid"><a href="#" class="download_txt">View PDF</a></div>
                     </div></cms:pageSlot>
                     
                    
                  </div>
						<!-- Clinic Toolkit print material ends --> 
						<!-- Clinic Toolkit print testimonial starts --> 
                  <div class="content-section marBottom40">
                     <div class="row marBottom40">
                     <div class="col-md-8">
                     <cms:pageSlot position="Section3L7" var="component">
                     <div class="content-head">${component.h2content}</div>   
                     <span class="marBottom40">${component.paragraphcontent}</span>
                     <a href="${component.mediaForpdf.downloadurl}" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                     </div></cms:pageSlot>
                     <div class="col-md-4 pos-right-video">
                        
                        <div class="col-md-12 video-block" style="width:100%">
                        <cms:pageSlot position="Section5AD" var="component">
                           <div class="video-container">${component.h2content}</div>
                           <div class="video-text">
                              <div class="vid-title">${component.paragraphcontent}</div>
                        
                           </div></cms:pageSlot>
                        </div>
                     
                       
                     
                     </div>
                  
                     </div> 
                  </div>
					<!-- Clinic Toolkit print testimonial ends --> 
					<!-- Clinic Toolkit EVENT Webinar1 starts --> 
                  <div class="content-section-thin marBottom40" style="overflow: hidden;">
                     <div class="col-md-6 no-pad">
                        <div class="event-container">
                           <cms:pageSlot position="Section5AA" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
                           <div class="event-btn-sontainer"><a href="#" class="event-btn">REGISTER FOR WEBINAR</a></div>
                        </div>
                     </div>
                     <div class="col-md-6 content-section marBottom40">
                     <cms:pageSlot position="Section3L8" var="component">
                           <div class="content-head">${component.h2content}</div>   
                           <span class="marBottom40">${component.paragraphcontent}</span>
                           <div class="col-md-6 no-pad"><a href="${component.mediaForpdf.downloadurl}" class="download_txt">Download Materials</a></div>
                           </cms:pageSlot>
                       
                     </div>
                  </div>
                  <!-- Clinic Toolkit EVENT Webinar1 ends --> 
                  <!-- Clinic Toolkit EVENT Webinar2 starts --> 


                  <div class="content-section-thin marBottom40" style="overflow: hidden;">
                     <div class="col-md-6 no-pad">
                        <div class="event-container">
                           <cms:pageSlot position="Section5AB" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
                           
                        </div>
                     </div>
                     <div class="col-md-6 content-section marBottom40">
                     <cms:pageSlot position="Section3L9" var="component">
                           <div class="content-head">${component.h2content}</div>   
                           <span class="marBottom40">${component.paragraphcontent}</span>
                           <div class="col-md-6 no-pad"><a href="${component.mediaForpdf.downloadurl}" class="download_txt">Download Materials</a></div>
                           <div class="col-md-6 no-pad"><a href="${comp.seqMediaForpdf.url}" class="download_txt">Watch Recording</a></div>
                       </cms:pageSlot>
                     </div>
                  </div>
                  <!-- Clinic Toolkit EVENT Webinar2 ends --> 
            
            </div>  
           		 <!--clinic event ends -->  
				<!-- Vaccine_Management start -->
            <div id="Vaccine_Management" class="block-content marBottom40 section">
               <div class="heading-title">
               <cms:pageSlot position="Section1EB" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					</div>
					
					<cms:pageSlot position="Section3L3" var="component">
               <c:forEach items="${component.resourceContentList}" var="comp">
									<!--<c:if test="${empty comp.media.url}">-->
               <div class="content-section marBottom40">
               
                
                  <div class="content-head">${comp.h2content}</div>
                  <div class="row marBottom40">
                  <div class="col-md-7">
                  ${comp.paragraphcontent}
                  </div>
                  <div class="col-md-3 pos-mid"><a href="${comp.mediaForpdf.downloadurl}" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                  <div class="col-md-2 pos-mid"><a href="${comp.seqMediaForpdf.url}" target="_blank" class="download_txt">View PDF</a></div>
                  
                  </div> 
                  
								
               </div>
               <!--</c:if>-->
								</c:forEach>
							</cms:pageSlot>
							<!-- vaccinemanagement end -->
							
							
              <!--   <div class="content-section marBottom40">
                  <div class="content-head">Best Practices Print Material</div>
                  <div class="row marBottom40">
                  <div class="col-md-7">
                  Download this toolkit for posters, guides, and other essentials for your next clinic
                  </div>
                  <div class="col-md-3 pos-mid"><a href="#" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                  <div class="col-md-2 pos-mid"><a href="#" class="download_txt">View PDF</a></div>
                  </div> 
               </div>-->
            </div>   
            
            
            
				<!-- Financial Guidance start -->
            <div id="Finnancial_Guidance" class="block-content marBottom40 section">
               <div class="heading-title">
               <cms:pageSlot position="Section1EC" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					</div>
	
               <div class="content-section marBottom40">
               <cms:pageSlot position="Section3L2" var="component">
                  <div class="content-head">${component.h2content}</div>
                  <div class="row marBottom40">
                  <div class="col-md-7">
                  ${component.paragraphcontent}
                  </div>
                  <div class="col-md-3 pos-mid"><a href="${component.mediaForpdf.downloadurl}" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                  <div class="col-md-2 pos-mid"><a href="${component.seqMediaForpdf.url}" class="download_txt">View PDF</a></div>
                  </div> </cms:pageSlot>
               </div>

			   <cms:pageSlot position="Section3L5" var="component">
               <c:forEach items="${component.resourceContentList}" var="comp">
									<!--<c:if test="${empty comp.media.url}">-->
               <div class="content-section marBottom40" style="overflow: hidden;">
                  <div class="col-md-4 no-pad img-align-logo"><img src="${fn:escapeXml(themeResourcePath)}/images/logo--flucelvax.png"/></div>
                  <div class="col-md-8 marBottom40">
                    
                        <div class="content-head">${comp.h2content}</div>   
                        <span class="marBottom40">${comp.paragraphcontent}</span>
                        <div class="col-md-5 no-pad"><a href="${comp.mediaForpdf.downloadurl}" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                        <div class="col-md-5 no-pad"><a href="${comp.seqMediaForpdf.url}" class="download_txt">View PDF</a></div>
                    
                  </div>
               </div><!--</c:if>-->
								</c:forEach>
							</cms:pageSlot>
							
							<!-- financial img2 starts -->
							<cms:pageSlot position="Section3M5" var="component">
               <c:forEach items="${component.resourceContentList}" var="comp">
									<!--<c:if test="${empty comp.media.url}">-->
               <div class="content-section marBottom40" style="overflow: hidden;">
                  <div class="col-md-4 no-pad img-align-logo"><img src="${fn:escapeXml(themeResourcePath)}/images/flaud-resourse-logo.PNG"/></div>
                  <div class="col-md-8 marBottom40">
                    
                        <div class="content-head">${comp.h2content}</div>   
                        <span class="marBottom40">${comp.paragraphcontent}</span>
                        <div class="col-md-5 no-pad"><a href="${comp.mediaForpdf.downloadurl}" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                        <div class="col-md-5 no-pad"><a href="${comp.seqMediaForpdf.url}" class="download_txt">View PDF</a></div>
                    
                  </div>
               </div><!--</c:if>-->
								</c:forEach>
							</cms:pageSlot>
							
							<!-- financial img2 ends -->
              <!--   <div class="content-section marBottom40" style="overflow: hidden;">
                  <div class="col-md-4 no-pad"><img src="images/flaud-resourse-logo.png"/></div>
                  <div class="col-md-8 marBottom40">
                    
                        <div class="content-head">Clinic Toolkit</div>   
                        <span class="marBottom40">These videos can help inform patients about the benefits of attending your clinic.</span>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">View PDF</a></div>
                    
                  </div>
               </div>
               <div class="content-section marBottom40" style="overflow: hidden;">
                  <div class="col-md-4 no-pad"><img src="images/flaud-resourse-logo.png"/></div>
                  <div class="col-md-8 marBottom40">
                    
                        <div class="content-head">Clinic Toolkit</div>   
                        <span class="marBottom40">These videos can help inform patients about the benefits of attending your clinic.</span>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">View PDF</a></div>
                    
                  </div>
               </div>
               <div class="content-section marBottom40" style="overflow: hidden;">
                  <div class="col-md-4 no-pad"><img src="images/flaud-resourse-logo.png"/></div>
                  <div class="col-md-8 marBottom40">
                    
                        <div class="content-head">Clinic Toolkit</div>   
                        <span class="marBottom40">These videos can help inform patients about the benefits of attending your clinic.</span>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">View PDF</a></div>
                    
                  </div>
               </div>
               <div class="content-section marBottom40" style="overflow: hidden;">
                  <div class="col-md-4 no-pad"><img src="images/flaud-resourse-logo.png"/></div>
                  <div class="col-md-8 marBottom40">
                    
                        <div class="content-head">Clinic Toolkit</div>   
                        <span class="marBottom40">These videos can help inform patients about the benefits of attending your clinic.</span>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">View PDF</a></div>
                    
                  </div>
               </div>
               <div class="content-section marBottom40" style="overflow: hidden;">
                  <div class="col-md-4 no-pad"><img src="images/flaud-resourse-logo.png"/></div>
                  <div class="col-md-8 marBottom40">
                    
                        <div class="content-head">Clinic Toolkit</div>   
                        <span class="marBottom40">These videos can help inform patients about the benefits of attending your clinic.</span>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">View PDF</a></div>
                    
                  </div>
               </div>
               <div class="content-section marBottom40" style="overflow: hidden;">
                  <div class="col-md-4 no-pad"><img src="images/flaud-resourse-logo.png"/></div>
                  <div class="col-md-8 marBottom40">
                    
                        <div class="content-head">Clinic Toolkit</div>   
                        <span class="marBottom40">These videos can help inform patients about the benefits of attending your clinic.</span>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">View PDF</a></div>
                    
                  </div>
               </div>
               <div class="content-section marBottom40" style="overflow: hidden;">
                  <div class="col-md-4 no-pad"><img src="images/flaud-resourse-logo.png"/></div>
                  <div class="col-md-8 marBottom40">
                    
                        <div class="content-head">Clinic Toolkit</div>   
                        <span class="marBottom40">These videos can help inform patients about the benefits of attending your clinic.</span>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                        <div class="col-md-5 no-pad"><a href="#" class="download_txt">View PDF</a></div>
                    
                  </div>
               </div>-->

            </div>   
<!-- Financial Guidance end-->

<!-- productreturns  start -->
            <div id="Product_Returns" class="block-content marBottom40 section">
               <div class="heading-title">
               <cms:pageSlot position="Section1ED" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					</div>
						<cms:pageSlot position="Section3L4" var="component">
               <c:forEach items="${component.resourceContentList}" var="comp">
									<!--<c:if test="${empty comp.media.url}">-->
               <div class="content-section marBottom40">
                  <div class="content-head">${comp.h2content}</div>
                  <div class="row marBottom40">
                  <div class="col-md-7">
                 ${comp.paragraphcontent}
                  </div>
                  <div class="col-md-3 pos-mid"><a href="${comp.mediaForpdf.downloadurl}" class="download_txt">Download Form <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                  <div class="col-md-2 pos-mid"><a href="#" class="download_txt">View Form</a></div>
                  </div> 
               </div>
                <!--</c:if>-->
								</c:forEach>
							</cms:pageSlot>
							<!-- product returns2 start -->
							<cms:pageSlot position="Section3M4" var="component">
               <c:forEach items="${component.resourceContentList}" var="comp">
									<!--<c:if test="${empty comp.media.url}">-->
               <div class="content-section marBottom40">
                  <div class="content-head">${comp.h2content}</div>
                  <div class="row marBottom40">
                  <div class="col-md-7">
                 ${comp.paragraphcontent}
                  </div>
                  <div class="col-md-3 pos-mid"><a href="${comp.mediaForpdf.downloadurl}" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                  <div class="col-md-2 pos-mid"><a href="#" class="download_txt">View PDF</a></div>
                  </div> 
               </div>
                <!--</c:if>-->
								</c:forEach>
							</cms:pageSlot>
							<!-- product returns2 ends -->
							
							
                <!--  <div class="content-section marBottom40">
                  <div class="content-head">Best Practices Print Material</div>
                  <div class="row marBottom40">
                  <div class="col-md-7">
                  Download this toolkit for posters, guides, and other essentials for your next clinic
                  </div>
                  <div class="col-md-3 pos-mid"><a href="#" class="download_txt">Download PDF <i class="fa fa-arrow-down" aria-hidden="true"></i></a></div>
                  <div class="col-md-2 pos-mid"><a href="#" class="download_txt">View PDF</a></div>
                  </div> 
               </div>-->
            </div>   
						<!-- productreturns  end -->
							


            </div>
            
         </div>

         
         
      </div>
      </div>







</template:page>