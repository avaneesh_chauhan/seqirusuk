<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<template:page pageTitle="${pageTitle}">
<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
		<div class="container-fluid contactmarginlogout">
			<nav aria-label="supportbreadcrumb">
				<ol class="supportbreadcrumb">
					<c:url value="/" var="homeUrl" />
					<li class="supportbreadcrumb-item active"><a href="${homeUrl}"
						class="blacktext"><spring:theme code="breadcrumb.home" /></a></li>
					<li class="supportbreadcrumb-item" aria-current="page"><spring:theme
							code="breadcrumb.support" /></li>
				</ol>
			</nav>
			<div class="row">
				<div class="col-md-12">
					<h2 class="contactheader">
						<cms:pageSlot position="Section4" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</h2>
				</div>
			</div>
			<div class="container support-body">
				<div class="col-md-12 padBottom96">
					<div class="blkfirst">
						<div class="col-md-12 supportlogoutwhite-background">
							<cms:pageSlot position="Section2" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
							<cms:pageSlot position="Section3" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
							<cms:pageSlot position="Section5" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</div>
				</div>
			</div>

			<%-- <cms:pageSlot position="Section5" var="feature">
			<div class="row marBottom60">
				<div class="col-md-12 grayrow">
					<div class="col-md-3 grayboxtext">${feature.h2content}</div>
					<div class="col-md-9 contatpadleft">
						<div class="subpara"></div>
						${feature.paragraphcontent}
						<div class="signdiv">
							<a class="redbtn" href="/registercustomer"><spring:theme
									code="login.form.create.account.link.title" /></a>
						</div>
						<div class="logdiv">
							<a class="greybtn" href="/login"><spring:theme
									code="login.login" /></a>
						</div>
					</div>
				</div>
			</div>
		</cms:pageSlot> --%>
		</div>
		</sec:authorize>
		<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
			<div class="container-fluid body-section" id="supportPageId">
         <div class="container">
            <nav aria-label="supportbreadcrumb">
               <ol class="supportbreadcrumb">
               <c:url value="/" var="homeUrl" />
				  <li class="supportbreadcrumb-item active"><a href="${homeUrl}"
					class="blacktext"><spring:theme code="breadcrumb.home" /></a></li>
                  
                  <!-- <li class="supportbreadcrumb-item active"><a href="#" class="">Home</a></li> -->
                  <li class="supportbreadcrumb-item" aria-current="page"><spring:theme
						code="breadcrumb.support" /></li>
					<!-- <li class="supportbreadcrumb-item" aria-current="page">Support</li> -->
               </ol>
            </nav>
            <div class="row">
               <div class="col-md-12">
                  <h2 class="contactheader">
                  	<cms:pageSlot position="Section1AA" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
                  </h2>
               </div>
            </div>
            <div class="container support-body">
               <div class="col-md-6 padBottom96">
                  <div class="blkfirst">
                     <div class="col-md-12 no-pad">
                        <div class="heading-txt marBottom30">
	                        <cms:pageSlot position="Section2AA" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
					</div>
                     </div>
                     <div class="col-md-12 supportwhite-background">
                        <div class="col-md-12 emailsection">
                        	<cms:pageSlot position="Section3AA" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
                         </div>
                        <div class="emailsubsection">
                           <cms:pageSlot position="Section3AB" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 padBottom42">
                  <div class="blklast">
                     <div class="col-md-12 no-pad">
                        <cms:pageSlot position="Section4AA" var="feature">
								<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-12 white-background emailsubsection-1">
                        <cms:pageSlot position="Section5AA" var="feature">
								<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="blklast">
                     <div class="col-md-12 martop50">
                        <cms:pageSlot position="Section6AA" var="feature">
								<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-12 white-background emailsubsection-1">
                        <cms:pageSlot position="Section7AA" var="feature">
								<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 padBottom42">
                  <div class="blklast">
                     <div class="col-md-12">
                        <cms:pageSlot position="Section8AA" var="feature">
								<cms:component component="${feature}" />
						</cms:pageSlot>
                        <div class="col-md-12 faqwhite-background">
                           <div class="col-md-4">
                              <cms:pageSlot position="Section9AA" var="feature">
								<cms:component component="${feature}" />
							 </cms:pageSlot>
                           </div>
                           <div class="col-md-4">
                            <c:choose>
	                           	<c:when test="${supportHideStatus ne '' && supportHideStatus eq 'hide'}">
	                              <cms:pageSlot position="Section9AE" var="feature">
									<cms:component component="${feature}" />
								  </cms:pageSlot>
							     </c:when>
							     <c:otherwise>
							     	<cms:pageSlot position="Section9AB" var="feature">
									<cms:component component="${feature}" />
								  </cms:pageSlot>
							     </c:otherwise>
							  </c:choose>
                           </div>
                           <div class="col-md-4">
                              <cms:pageSlot position="Section9AC" var="feature">
								<cms:component component="${feature}" />
							  </cms:pageSlot>
                           </div>
                           <div class="col-md-12 buttonsection" >
                              <a href="${contextPath}/faqs" class="redround-btn bttn" style="color: #FFF;">View All FAQs</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <%--   <div class="container-fluid faqsbody-section" style="display: none;" id="faqId">
         <div class="container">
            <nav aria-label="supportbreadcrumb">
               <ol class="supportbreadcrumb">
                  <li class="supportbreadcrumb-item active"><a href="#" class="">Home</a></li>
                  <li class="supportbreadcrumb-item active" aria-current="page">Support</li>
                  <li class="supportbreadcrumb-item" aria-current="page">FAQ</li>
               </ol>
            </nav>
            
            <div class="row marBottom30">
               <div class="col-md-12">
                  <h2 class="faqheader">
                  	<cms:pageSlot position="Section1BA" var="feature">
					   <cms:component component="${feature}" />
					</cms:pageSlot>
				</h2>
               </div>
            </div>	
            <div class="container faq-body">
               <div class="col-md-3 faqleftcolumn">
                  <ul class="faqslist">
                     <cms:pageSlot position="Section2BB" var="feature">
					   <cms:component component="${feature}" />
					</cms:pageSlot>
                  </ul>
               </div>
                <div class="col-md-8 faq-section faqwhite-background" id="faq-section-1">
                  <div class="faqheading-txt">
                  	<cms:pageSlot position="Section3BC" var="feature">
					   <cms:component component="${feature}" />
					</cms:pageSlot>
                  </div>
                  
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section3BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-2" style="display: none;">
                  	<cms:pageSlot position="Section4BC" var="feature">
					   	<cms:component component="${feature}" />
					</cms:pageSlot>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section4BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-3" style="display: none;">
                  	<cms:pageSlot position="Section5BC" var="feature">
					   	<cms:component component="${feature}" />
					</cms:pageSlot>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section5BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-4" style="display: none;">
                   <cms:pageSlot position="Section6BC" var="feature">
					   	<cms:component component="${feature}" />
					</cms:pageSlot>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section6BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-5" style="display: none;">
                  <cms:pageSlot position="Section7BC" var="feature">
					   	<cms:component component="${feature}" />
					</cms:pageSlot>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section7BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               
               
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-6" style="display: none;">
                  <div class="faqheading-txt">
                  	<cms:pageSlot position="Section9BC" var="feature">
					   <cms:component component="${feature}" />
					</cms:pageSlot>
                  </div>
                  
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section9BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
               
               
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-7" style="display: none;">
                  <div class="faqheading-txt">
                  	<cms:pageSlot position="SectionRBC" var="feature">
					   <cms:component component="${feature}" />
					</cms:pageSlot>
                  </div>
                  
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="SectionRBI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
              
              
               <div class="col-md-8 faq-section faqwhite-background" id="faq-section-8" style="display: none;">
                  <cms:pageSlot position="Section8BC" var="feature">
					   	<cms:component component="${feature}" />
					</cms:pageSlot>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BD" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BE" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row marBottom30">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BF" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BG" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BH" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                     <div class="col-md-6">
                        <cms:pageSlot position="Section8BI" var="feature">
					   		<cms:component component="${feature}" />
						</cms:pageSlot>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> --%>
		</sec:authorize>
		</template:page>