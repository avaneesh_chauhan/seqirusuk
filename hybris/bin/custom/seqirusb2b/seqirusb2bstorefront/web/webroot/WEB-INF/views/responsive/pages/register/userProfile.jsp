<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="userProfileInfo" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userCompanyInfo" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userNotificationInfo" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userAccountInfo" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userLocationsInfo" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<template:page pageTitle="${pageTitle}">
	<div class="post_login">

		<div class="container-fluid body-section">
			<div class="container" style="width: 1170px">
				<nav aria-label="supportbreadcrumb">
					<ol class="supportbreadcrumb">
						<c:url value="/" var="homeUrl" />
						<li class="supportbreadcrumb-item active"><a
							href="${homeUrl}" class=""><spring:theme code="breadcrumb.home" /></a></li>
						<li class="supportbreadcrumb-item" aria-current="page"><spring:theme code="breadcrumb.profile" /></li>
					</ol>
				</nav>

				<div class="row">
					<div class="col-md-12">
						<div class="col-md-5 white-bg">
							<table class="table dashboard-heading-table">
								<tr>
									<td class="profile_header">${customerProfileData.firstName}&nbsp;${customerProfileData.lastName}</td></tr>
                        <tr>
									<td class="lst-login"><span class="profile-lebel"><spring:theme code="userprofile.lastlogin" text="Last Login" /></span>  |  ${customerProfileData.lastLogin}</td>
								</tr>
							</table>
						</div>
					</div>
				</div>

				<div class="container dashboard-body">

					<div class="col-md-5 no-pad">
						<userAccountInfo:userAccountInfo />
						<userProfileInfo:userProfileInfo />
						<%-- <userNotificationInfo:userNotificationInfo /> --%>
					</div>

					
						<div class="col-md-7 no-pad">
							<userCompanyInfo:userCompanyInfo />
							<userLocationsInfo:userLocationsInfo />
						</div>
					<div class="clearfix1"></div>

					<div class="col-md-12 no-pad">
						<div class="blkfirst white-cote">
							<div class="col-md-12 no-pad blue-back">
								<span class="heading-txt">Consent Management</span>
							</div>

							<div
								class="col-md-12 white-background-profile white-background-profile-consent body-txt">
								<cms:pageSlot position="Section1C" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
								<%-- <cms:pageSlot position="Section1D" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot> --%>
							</div>

							<%-- <div
								class="col-md-6 white-background-profile white-background-profile-consent body-txt">
								<cms:pageSlot position="Section1B" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
								<cms:pageSlot position="Section1A" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div> --%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</template:page>