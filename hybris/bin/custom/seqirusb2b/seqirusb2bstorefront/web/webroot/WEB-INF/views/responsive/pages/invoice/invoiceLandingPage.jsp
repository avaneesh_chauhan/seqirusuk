<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<template:page pageTitle="${pageTitle}">
<div id="invoiceLandingPanelId"></div>
<div class="container-fluid body-section" id="oldInvoiceLandingReponse">
         <div class="container">
            <nav aria-label="supportbreadcrumb">
               <ol class="supportbreadcrumb">
               	  <c:url value="/" var="homeUrl" />
                  <li class="supportbreadcrumb-item active"><a href="${homeUrl}" class=""><spring:theme code="breadcrumb.home" /></a></li>
                  <li class="returnsbreadcrumb-item" aria-current="page"><spring:theme
								code="breadcrumb.financial" text="Financial" /></li>
                  <li class="supportbreadcrumb-item" aria-current="page">Invoices</li>
               </ol>
            </nav>
            <div class="row">
               <div class="col-md-12">
                  <h2 class="contactheader"><spring:theme code="invoices.landing.myinvoices"/></h2>
               </div>
            </div>
			<div class="row">
			<div class="col-md-6 col-xs-12 ">
				 <div id="invoicegraphcontainer"></div>
		    </div>
			<div class="col-md-1 col-xs-12 "></div>
			<div class="col-md-5 col-xs-12 ">
				<div class="inoverview"><spring:theme code="invoices.landing.invoiceoverriew"/></div>
				<div class="invoicedropdwn col-md-8 ">
					<div class="indrop invoicedropdwn invoice-drop-down">
						<select class="form-control selectpicker" id="invoicedrop" >
						 	<c:forEach var="seqirusSeasons" items="${seasonList}">
						 		<option value="${seqirusSeasons.orderSeason}" ${(seqirusSeasons.orderSeason eq presentSeason) ? 'selected="selected"' : ''}>${seqirusSeasons.orderSeason}&nbsp;Invoices</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="invoice-detailssection">
					<div class="row">
						<c:choose>
							<c:when test="${invoiceList.openStatusInvoiceAmount eq 0.0}">
								<div class="col-md-6 legendbox"><span class="legend-invoicebox-blue"></span><span class="legendtext"><spring:theme code="invoices.landing.openinvoices"/></span> <span class="legendprice">&#163;<fmt:formatNumber type = "number" maxFractionDigits = "0" minFractionDigits = "0" value = "${invoiceList.openStatusInvoiceAmount}"/></span></div>
							</c:when>
							<c:otherwise>
								<div class="col-md-6 legendbox"><span class="legend-invoicebox-blue"></span><span class="legendtext"><spring:theme code="invoices.landing.openinvoices"/></span> <span class="legendprice">&#163;<fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value = "${invoiceList.openStatusInvoiceAmount}"/></span></div>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${invoiceList.paidStatusInvoiceAmount eq 0.0}">
								<div class="col-md-6 legendbox"><span class="legend-invoicebox-red"></span><span class="legendtext"><spring:theme code="invoices.landing.paidinvoices"/></span> <span class="legendprice">&#163;<fmt:formatNumber type = "number" maxFractionDigits = "0" minFractionDigits = "0" value = "${invoiceList.paidStatusInvoiceAmount}"/></span></div>
							</c:when>
							<c:otherwise>
								<div class="col-md-6 legendbox"><span class="legend-invoicebox-red"></span><span class="legendtext"><spring:theme code="invoices.landing.paidinvoices"/></span> <span class="legendprice">&#163;<fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value = "${invoiceList.paidStatusInvoiceAmount}"/></span></div>
							</c:otherwise>
						</c:choose>
					</div>	
				</div>
				<input type="hidden" id="openInvAmount" value="${invoiceList.openStatusInvoiceAmount}"/>
				<input type="hidden" id="paidInvAmount" value="${invoiceList.paidStatusInvoiceAmount}"/>
				<input type="hidden" id="currentSeason" value="${presentSeason}"/>
		    </div>
			</div>
			<div class="row">
            <div class="col-xs-12 invoiceBody">
               <div class="invoiceHeader col-xs-12">${presentSeason} Invoices</div>
               <!-- Table starts -->
               <div class="tablecontainer invoiceTablecontainer"> 
                    <table id="invoiceTable" class="display">
                        <thead>
                            <tr>
                               <th><b><spring:theme code="invoices.landing.orders"/> </b></th>
                                <th><b><spring:theme code="invoices.landing.invoice"/> </b></th>
                                <th><b><spring:theme code="invoices.landing.amount"/> </b></th>
                                <th><b><spring:theme code="invoices.landing.dateIssued"/> </b></th>
                                <th><b><spring:theme code="invoices.landing.duedate"/> </b></th>
                                <th><b><spring:theme code="invoices.landing.status"/> </b></th>
                            </tr>
                        </thead> 
                    </table>
               </div>
               <!-- Table Ends -->
            </div>
			</div>

         </div>
      </div>
     </template:page> 
