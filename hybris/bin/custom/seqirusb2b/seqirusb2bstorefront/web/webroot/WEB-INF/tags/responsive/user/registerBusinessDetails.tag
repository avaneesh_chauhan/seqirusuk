<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>


<div id="BusinessDetails" class="tabcontent">
               <div class="section1_header col-xs-12">
                  <spring:theme code="form.register.business.header" />
               </div>
               <div class="section1_subheader col-xs-12">
                  <spring:theme code="form.register.business.subHeader" />
               </div>
             
                  <div class="col-xs-12 businessformContent">                     
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="businessName"><spring:theme code="form.register.business.companyName" /><span class="asteriskred">*</span></label><br/>
                              <input class="form-control required-fld-form1 with_space"  autocomplete="no" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.business.orgName" />" data-error="Please enter Organisation Name"  id="businessName" name="companyName" placeholder="Practice, Pharmacy or Business Name"  type="text" required />
                              <div class="help-block with-errors"></div>
                           </div>
                           <div class="form-group col-md-4">
                              <label for="registrationNumber"><spring:theme code="form.register.business.orgNumber" /></label><br/>
                              <input class="numberwithoutzero form-control fieldmar orgreginum" autocomplete="no" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.business.orgtooltip" />" name="companyRegNumber"  id="registrationNumber" placeholder="81122339"  type="text" maxlength="8"/> 
                           	  <div class="help-block with-errors zeroerrmsg"></div> 	
                           </div>
                        </div>
                     </div>                     
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-3">
                              <label for="vatNumber"><spring:theme code="form.register.business.vatNum" /></label><br/>
                              <input class="form-control" id="vatNumber" autocomplete="no"  name="vatNumber" placeholder="GB123456789"  type="text" maxlength="11"/>                              
                           <div class="help-block vaterror"></div>
                           </div>
                          <%--  <div class="form-group regfeilds">
                              <label for="trading"><spring:theme code="form.register.business.trading" /><span class="asteriskred">*</span></label><br/>
                           <select name="tradingSince" id="trading" class="trading-dropdown">
                              <option value="">Select</option>
                             <c:forEach var="tradingSince" items="${tradingSinceYears}">
											<option name="tradingSince" value="${tradingSince}">${tradingSince}</option>
										</c:forEach>
                           </select>  
                           <div class="help-block with-errors"></div>                        
                           </div> --%>
                           
                           <div class="form-group col-md-2">
                              <label for="trading"><spring:theme code="form.register.business.trading" /><span class="asteriskred">*</span></label><br/>
	                           <select name="tradingSince" id="trading" data-error="Please select Trading year" class="required-fld-form1 select-control trading-dropdown" required>
	                              <option value="" selected="selected">YYYY</option>
	                             <c:forEach var="tradingSince" items="${tradingSinceYears}">
												<option name="tradingSince" value="${tradingSince}">${tradingSince}</option>
											</c:forEach>
	                           </select>  
	                           <div class="help-block with-errors"></div>                        
                           </div>
                           
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-3">
                              <label for="companytype"><spring:theme code="form.register.business.businessType" /></label><br/>
                           <select  name="companyType" id="companytype" class="select-dropdowns">
                              <option value="">Select</option>
                              <c:forEach var="companyType" items="${companyTypes}">
											<option name="companyType" value="${companyType.code}"><spring:theme
															code="type.CompanyTypeEnum.${companyType.code}.name" /></option>
										</c:forEach>
                           </select>  
                                             
                           </div>
                           
                            <div class="form-group col-md-4">
                              <label for="businesstype"><spring:theme code="form.register.business.organisationType" /><span class="asteriskred">*</span></label><br/>
	                           <select name="businessType" id="businesstype" data-error="Please select Organisation Type" class="required-fld-form1 select-control select-dropdowns" required>
	                              	<option value="" selected="selected">Select</option>
	                              	<c:forEach var="businessType" items="${businessTypes}">
												<option name="businessType" value="${businessType.code}"><spring:theme
																code="type.BusinessTypeEnum.${businessType.code}.name" /></option>
											</c:forEach>
	                           	</select>                             
	                           	<div class="help-block with-errors" style="margin-top: 5px!important;"></div>                         
                            </div>
                              <div class="form-group col-md-3" id="otherBusinessType" style="display: none;">
	                              <label for="Bussinessjobtitle">Other<span class="asteriskred">*</span></label><br/>
	                              <input  class="form-control textonly required-fld-form1 width104" id="otherbusinessType" data-error="Please enter business type" name="otherBusinessType" placeholder="E.g. Occupational Health "  type="text" autocomplete="no" maxlength="100"/>                             
	                           	   <div class="help-block with-errors"></div>
                           	  </div>
                        </div>
                     </div>

                     <div class="row pull-top">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="BussinessfirstName"><spring:theme code="form.register.business.firstName" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form1 textonly" autocomplete="no" data-error="Please enter First Name" id="BussinessfirstName" name ="firstName" placeholder="Edward"  type="text" required />
                              <div class="help-block with-errors"></div>
                           </div>
                           <div class="form-group col-md-4">
                              <label for="BussinesslastName"><spring:theme code="form.register.business.lastName" /><span class="asteriskred">*</span></label><br/>
                              <input class="form-control required-fld-form1 textonly" autocomplete="no" data-error="Please enter Surname" id="BussinesslastName" name="lastName" placeholder="Newgate"  type="text" required/>                                                         
                              <div class="help-block with-errors"></div>
                           </div>
                        </div>
                     </div> 

                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-3">
                              <label for="Bussinessemail"><spring:theme code="form.register.business.email" /><span class="asteriskred">*</span></label><br/>
                              <input   class="form-control required-fld-form1 email_input emailcheck1" data-error="Please enter valid Email" id="Bussinessemail" autocomplete="no" name="email" placeholder="email@example.co.uk"  type="email" required />
                              <div class="help-block" id='emailerr'></div>
                           </div>
                           <div class="form-group col-md-3">
                              <label for="BussinessPhone"><spring:theme code="form.register.business.phoneNum" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control phonemask1 required-fld-form1" autocomplete="no" data-error="Please enter a valid UK format Telephone" name="phoneNumber" id="BussinessPhone" maxlength="11"   type="text" required/>                                                         
                              <div class="help-block" id="message"></div>
                           </div>
                           <div class="form-group col-md-2">
                              <label for="Businessphoneext"><spring:theme code="form.register.business.phoneExt" /></label><br/>
                              <input  class="form-control numberonly" id="Businessphoneext" name="phoneExt" autocomplete="no" maxlength="4" type="text"/>  
                              <div class="help-block with-errors errmsg"></div>
                           </div>
                        </div>
                     </div> 
                     <!-- <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group regfeilds fieldsmargin-4">
                              <label for="step1jobtitle">Address Lookup</label><br/>
                              <input  class="form-control textonly input-3" id="step1autoComplete" name="step1autoComplete" placeholder="Type Zip/Postal code or Street Name"  type="text"/>                             
                           </div>
                        </div>
                     </div> -->
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="Bussinessjobtitle"><spring:theme code="form.register.business.jobTitle" /></label><br/>
                              <input  class="form-control textonly" id="Bussinessjobtitle" autocomplete="no" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.business.jobtitleDesc" />" name="jobTitle" placeholder="E.g. Vaccine Buyer"  type="text"/>                             
                           </div>
                           <div class="form-group col-md-4"> 
                              <label for="BussinesslastNHSCode"><spring:theme code="form.register.business.nhsCode" /></label><br/>
                              <input  class="form-control" id="BussinesslastNHSCode" name="nhcNumber" placeholder="123456789" autocomplete="no" data-toggle="tooltip" data-placement="bottom" title="Please enter the associated NHS code for your organisation"  type="text"/>   
                          	  <div class="help-block with-errors customNHSerror"></div>
                           </div>
                        </div>
                     </div>
                      <div class="row">
                        <div class="col-md-5 col-xs-12">
                           <div class="form-group">
                              <label for="Address">Address Lookup</label>
                              <div class="input-group spcl-addon">  
                              <div class="input-group-addon"><i class="fa fa-search"></i></div>
                              <input class="form-control" style="margin-bottom:0px;" id="addresslookupbusiness" name="lookup" placeholder="Search" type="text">
                              </div>
                             
                           </div>
                        </div>
                     </div>
                       <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step1address"><spring:theme code="form.register.business.bildingStreet" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form1" id="businessAddress" name="buildingStreet" data-error="Please enter Address" placeholder="Building Number & Street" autocomplete="no"  type="text" required/>
                              <div class="help-block with-errors"></div>
                           </div>
                           <div class="form-group col-md-4">
                              <label for="step1street"><spring:theme code="form.register.business.additionalStreet" /></label><br/>
                              <input  class="form-control" id="businessStreet" autocomplete="no" name="additionalStreet"  type="text"/> 
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group col-md-4">
                              <label for="step1City"><spring:theme code="form.register.business.city" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control textonly required-fld-form1" id="businessCity" placeholder="City" data-error="Please enter City" autocomplete="no" name="city" type="text" required/>
                              <div class="help-block with-errors"></div>
                           </div>
                           <div class="form-group col-md-3">
                              <label for="step1post"><spring:theme code="form.register.business.postCode" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control required-fld-form1 spcl-fld" data-error="Please enter valid Post Code" autocomplete="no" id="businessPost" placeholder="SL16 8AA"  name="postalCode" type="text" required/>
                              <div class="help-block with-errors postcodeerror1" ></div>   
						   </div>
                           
                           <div class="form-group col-md-3">
                              <label for="BussinessCountry"><spring:theme code="form.register.business.country" /><span class="asteriskred">*</span></label><br/>
                              <input  class="form-control textonly required-fld-form1" data-error="Please enter Country" id="BussinessCountry" name="country" autocomplete="no" placeholder="Country"  type="text" required />
                              <div class="help-block with-errors"></div>
                           </div>
                        </div>
                     </div> 
                     
                  </div>
                  <div class="col-xs-12 previosnmail">
                  <div></div>
                     <%-- <div class="prevStepbtn">< <spring:theme code="form.register.prev" /></div> --%>
                  <!-- <button  class="Nextbutton nxt1" >Next</button> -->
                  <button type="submit"  class="step1 nxt1_1 Nextbutton tab-3-sub sub-2" id="gotosection_2" >Next</button>
                    <!--  <div  hidden  class="step1 nxt1_1 NextbuttonSubmit tab-3-sub pagi-link sub-2" data-section-id="section-2" data-img-id="img-2">Next</div> -->
                  </div>
              
            </div>
