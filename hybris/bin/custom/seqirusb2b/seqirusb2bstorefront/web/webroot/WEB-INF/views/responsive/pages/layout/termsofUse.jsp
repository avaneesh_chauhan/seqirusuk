<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<template:page pageTitle="${pageTitle}"> 

<main  role="main" class="home_container container col-xs-12" >
            <div class="col-md-12 margin-control" >
                <div class="terms_title">Terms of Use</div>
                <div class="terms_block">
                    <div class="terms_heading">1. General Conditions</div>
                    <p class="light_text">
                        This website is operated by Seqirus UK Limited, a company registered in England & Wales under company number 09614642 and whose registered office is at Point, 29 Market Street, Maidenhead, Berkshire, England SL6 8AA ("Seqirus"). Seqirus is a CSL Company and we adhere to CSL's global policies.  Your access to this website is conditional on your acceptance and compliance with the terms, conditions, notices and disclaimers contained on this page and elsewhere on the website (the "General Conditions"). Seqirus reserves the right to amend the General Conditions at any time. 
                    </p>
                </div>

                <div class="terms_block">
                    <div class="terms_heading">2. Copyright</div>
                    <p class="light_text">
                        Unless indicated to the contrary, Seqirus owns all copyright subsisting in the information, text, materials, graphics, software, names, logos and trademarks (the "Content") contained on this website. You may reproduce in whole or in part the Content only if: 
                    </p>
                    <p class="light_text">
                        &bull;&nbsp;  The reproduction is not for public or commercial purposes; and<br/>
                        &bull;&nbsp;  You keep all Content intact and in the same form as presented on this website (including all copyright, trademark and other proprietary notices).
                    </p>
                </div>

                <div class="terms_block">
                    <div class="terms_heading">3. Trade Marks</div>
                    <p class="light_text">
                        Unless indicated to the contrary the trademarks contained on this website are trademarks of Seqirus or CSL group affiliates. Where a mark is indicated as a registered mark it is registered in the United Kingdom and may also be registered in other jurisdictions. Nothing contained in this website is to be construed as a licence or any right to use a trademark displayed on this website without the express written permission of Seqirus.
                    </p>

                </div>

                <div class="terms_block">
                    <div class="terms_heading">4. Links</div>
                    <p class="light_text">
                        This website may contain links to third party websites. These linked websites are not under the control of Seqirus or CSL group companies and Seqirus or CSL group companies are not responsible for the contents of any linked websites or any hyperlink contained in any linked websites. Seqirus provides these links as a convenience only and the inclusion of any link does not imply any endorsement of the linked website by Seqirus. You follow links to any linked websites entirely at your own risk. 
                    </p>
                    <p class="light_text">
                        If you are linking to this website from your own website, you must link directly to the home page of this website and not to any other page within this website.</p>

                </div>

                <div class="terms_block">
                    <div class="terms_heading">5. Disclaimer</div>
                    <p class="light_text">
                        The Content contained on this website is provided for general information only. It should not be relied upon without obtaining specific advice from appropriate experts. While Seqirus has used reasonable efforts to ensure that the Content contained on this website is correct and current at the time it is published, Seqirus makes no representation and gives no warranty that the Content contained on this website is complete or accurate and takes no responsibility for any error, omission or defect in such Content. 
                    </p>
                    <p class="light_text">
                        This website may contain forward looking statements. Such statements are subject to many factors which may cause Seqirus's plans or results to differ from those expected. These include unexpected preclinical or clinical results, the need for additional research and development, delays in manufacturing, access to capital and funding and delays in the development of commercial relationships. CSL undertakes no obligation to publicly release or update the results of any forward looking statements which may be made to reflect events or circumstances including the occurrence of unanticipated events after the date at which those statements are made. 
                    </p>
                </div>

                <div class="terms_block">
                    <div class="terms_heading">6. Limitation of Liability</div>
                    <p class="light_text">
                        To the maximum extent permitted by law, Seqirus disclaims any liability to any person arising out of any action or failure to act by that person, in accessing, downloading, using or relying on any Content contained on this website whether caused by the negligence of Seqirus or otherwise. Under no circumstances will Seqirus be liable for any indirect, incidental, special or consequential damages, including loss of business or other profits, arising in relation to the use of any Content contained in this website whether caused by the negligence of Seqirus or otherwise. 
                    </p>
                </div>

                <div class="terms_block">
                    <div class="terms_heading">7. Virus Warning</div>
                    <p class="light_text">
                        Seqirus does not represent or warrant that any files obtained from or through this website are free from computer viruses or other defects. Any such files are provided and may be used on the basis that the user accepts all responsibility for any loss, damage or other consequence resulting directly or indirectly from the use of those files. 
                    </p>

                </div>
                <div class="terms_block">
                    <div class="terms_heading">8. Privacy</div>
                    <p class="light_text">
                        For further information regarding Seqirus' information handling practices, please refer to Seqirus' Privacy Policy.
                    </p>
                    <p class="light_text">
                        When you visit this website or download information from it, Seqirus' internet service provider makes a record of your visit and records your internet address, your domain name (if applicable), the date and time of your visit to the website, the pages you accessed and documents downloaded, the previous websites you have visited, and the type of browser you are using. This information is only used for statistical and website development purposes.
                    </p>
                    <p class="light_text">
                        You should also be aware that anonymous information and data may be automatically collected through the use of "cookies". Cookies are pieces of information that a website can use to recognize repeated usage, facilitate users getting access to and using the website and allow a website to track usage behaviour and compile aggregate data that will allow content improvement and targeted advertising. For further information please refer to Seqirus' Cookie Policy.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">9. Governing Law</div>
                    <p class="light_text">
                        These General Conditions are governed by, construed and enforced in accordance with the laws of England. Any disputes arising from these General Conditions or in connection with the use of this website are subject to the exclusive jurisdiction of the courts of England. This website may be accessed throughout the UK and overseas. Seqirus makes no representation that the Content on this website complies with the laws of any country outside the UK. If you access this site from outside the UK, you are responsible for ensuring that that your access to, downloading of, use of or reliance on the Content contained in this website is in compliance with all laws in the place in which you are located.
                    </p>

                </div>
            </div>
        </main>
       </template:page> 