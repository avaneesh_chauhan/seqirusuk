<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<template:page pageTitle="${pageTitle}">
<main  role="main" class="home_container container col-xs-12" >
            <div class="col-md-12 margin-control" >
                <div class="terms_title">Website Privacy Notice</div>
                <div class="terms_block">
                    <p class="light_text">
                        From time to time, just like many other companies, we collect or use personal data. In this privacy notice, we explain the:
                    </p>
                    <ul class="light_text">
                        <li>purposes we collect and use your personal data for;</li>
                        <li>categories of personal data we collect for those purposes; and</li>
                        <li>how we collect your personal data.</li>
                    </ul>
                    <p class="light_text">
                        Importantly, we also tell you what your rights are in relation to the personal data we hold and what you can do to enforce them.
                    </p>
                   <!--  <p class="light_text">
                        If you like, you can scroll down and read the whole policy, but you can also jump to the section you are interested in by clicking on one of the headings below.
                    </p>
                    <p class="redbody_text">
                        1. <u>INTRODUCTION</u><br>
                        2. <u>CATEGORIES OF PERSONAL DATA WE COLLECT</u><br> 
                        3. <u>HOW WE COLLECT YOUR PERSONAL DATA</u><br>
                        4. <u>HOW AND WHY WE USE YOUR PERSONAL DATA</u><br> 
                        5. <u>YOUR CONSENT</u><br> 
                        6. <u>HOW WE WILL KEEP YOUR DATA SECURE</u><br> 
                        7. <u>HOW LONG WE KEEP YOUR PERSONAL DATA FOR</u><br> 
                        8. <u>WHO HAS ACCESS TO YOUR PERSONAL DATA?</u><br> 
                        9. <u>DO WE TRANSFER YOUR PERSONAL DATA OUTSIDE OF EUROPE?</u><br> 
                        10. <u>WHAT RIGHTS DO YOU HAVE?</u><br> 
                        11. <u>DIRECT MARKETING</u><br>
                        12. <u>COOKIES</u><br> 
                        13. <u>CONTACT US</u><br> 
                        14. <u>UPDATES TO THIS NOTICE</u> 
                    </p> -->
                    <p class="light_text">
                        If this privacy notice doesn't answer your questions, then get in touch with us using the contact details below.
                    </p>
                </div>

                <div class="terms_block">
                    <div class="terms_red_heading">1. INTRODUCTION</div>

                    <p class="light_text">
                        At Seqirus UK Limited ("we", "us", "Seqirus") we are committed to protecting your privacy. When you use our websites, we will collect certain information that can be used to identify you ("<b>personal data</b>").
                    </p>
                    <p class="light_text">
                        Seqirus is the "data controller" of the personal data we collect. This means that we are responsible for decisions about the collection and use of personal data. It also means we are responsible for responding to your questions and requests in relation to the personal data we hold about you.
                    </p>
                    <p class="light_text">
                        This privacy notice explains how we use the personal data we collect when you use our website. It also explains the rights you have in relation to your personal data.
                    </p>
                </div>

                <div class="terms_block">
                    <div class="terms_red_heading">2. CATEGORIES OF PERSONAL DATA WE COLLECT</div>
                    <p class="light_text">
                        We may collect the following types of personal data from you when you use our website:
                    </p>
                    <ul class="light_text">
                        <li><b>Contact details</b>: this is information that allows us to contact you, such as your name, address, telephone numbers, email addresses and social media handles/user names.</li>
                        <li><b>Payment information, purchase and account history</b>: if you are a healthcare professional/ provider or a distributor of products we will collect information about your account and business with us. This may include information such as credit/debit card details, bank account details, billing addresses and customer numbers, as well as records relating to the products and services which you have purchased from us.</li>
                        <li><b>Personal data in reports and notifications you submit to us</b>: if you submit information to us about our products and services through our website, for example, through a suspected adverse event reporting form, we will collect any personal data you include within your report.</li>
                        <li><b>Health data</b>: if you submit healthcare data to us in relation to our products or services, we will collect any personal data and sensitive personal data you include.</li>
                        <li><b>Records of your discussions with us</b>: when you contact us using the contact options on the website (whether by email, phone, an online form or through social media (such as through Twitter or on Facebook), we may keep a record of the information you provide when doing this.</li>
                        <li><b>How you use our website</b>: we collect information about the pages you look at and how you use them.</li>
                        <li><b>Location information</b>: your smartphone or computer's IP address may tell us your approximate location when you connect to our website (this will usually be no more precise than a country or city location).</li>
                    </ul>
                </div>

                <div class="terms_block">
                    <div class="terms_red_heading">3. HOW WE COLLECT YOUR PERSONAL DATA</div>
                    <p class="light_text">
                        We will collect personal data from a number of sources. These include: 
                    </p>
                    <ul class="light_text">
                        <li><b>Directly from you</b>: for example, we will collect personal data directly from you when you set up an account with us, purchase products or services from us, complete forms we provide to you, make a report or notification about our products or services or contact us by phone, email, or communicate with us directly in some other way (such as through social media).</li>
                        <li><b>Our website</b>: we will collect information we observe about the way you use our website.</li>
                        <li><b>Third parties</b>: we may collect personal data about you from third parties. This typically includes: credit reference agencies (if we believe this is necessary to facilitate your purchase of products or services from us), or healthcare professional/providers (in relation to your use of our products).</li>
                    </ul>
                </div>

                <div class="terms_block">
                    <div class="terms_red_heading">4. HOW AND WHY WE USE YOUR PERSONAL DATA</div>
                    <p class="light_text">
                        We collect and use your personal data for the purposes described in the below table.
                    </p>
                    <table class="website_table">
                        <tr>
                            <th>Purpose of processing</th>
                            <th>Categories of data typically processed for the purpose</th>
                        </tr>
                        <tr>
                            <td>Contact and communicate with you in relation to our business, products and services</td>
                            <td>All the personal data listed above</td>
                        </tr>
                        <tr>
                            <td>If you are a healthcare professional/provider or distributor of our products, we will use your personal data to manage your account with us, perform credit checks where this is necessary, take payment for our products and services and arrange delivery</td>
                            <td>Contact details<br>
                                Payment information, purchase and account history<br>
                                Records of your discussions with us</td>
                        </tr>
                        <tr>
                            <td>If you contact us with any queries or complaints, we will use your personal data to help us respond to you</td>
                            <td>All the personal data listed above</td>
                        </tr>
                        <tr>
                            <td>In the course of investigating misuse of your account, fraud and debt collection</td>
                            <td>All the personal data listed above</td>
                        </tr>
                        <tr>
                            <td>To review our products and services, assess their safety and performance and to develop new products and services</td>
                            <td>All the personal data listed above</td>
                        </tr>
                        <tr>
                            <td>To perform online advertising</td>
                            <td>Payment information, purchase and account history<br>
                                How you use our website</td>
                        </tr>
                        <tr>
                            <td>To conduct market research</td>
                            <td>Contact details<br>
                                Demographic information<br>
                                Other personal data relevant to the market research being conducted</td>
                        </tr>
                        <tr>
                            <td>To conduct direct marketing</td>
                            <td>Contact details<br>
                                Demographic information<br> 
                                Purchase and account history<br> 
                                How you use our website<br> 
                                Any personal data you submit to us about products or services you are interested in 
                            </td>
                        </tr>
                        <tr>
                            <td>For individuals to report Suspected Adverse events</td>
                            <td>Contact details<br> 
                                Health data<br> 
                                (Further information about our collection and use of personal data for this purpose will usually be provided by us in a specific Suspected Adverse Event Privacy Notice) 
                            </td>
                        </tr>
                    </table>
                    <p class="light_text">
                        We will only use your personal data when the law allows us to. Most commonly, we will rely on the lawful grounds listed below when we use your personal data. 
                    </p>
                    <ul class="light_text">
                            <li><b>Consent</b>: if you have consented to the collection and use of your personal data.</li>
                            <li><b>Contract</b>: if we need to use your personal data to perform a contract between us. For example, where we enter a contract with you to provide you with our products or perform services.</li>
                            <li><b>Legal obligation</b>: if we need to comply with a legal obligation. For example, where we have a legal obligation to report suspected adverse events to our medicinal products.</li>
                            <li><b>Vital interests</b>: if our use of your personal data is needed to protect the vital interests of you or another person. For example, where there is a serious health risk to you or another person.</li>
                            <li><b>Public interest</b>: if our use of your personal data is needed in the public interest.</li>
                            <li><b>Legitimate interest</b>: if our use of your personal data is for our legitimate interests (or those of a third party) and your interests and fundamental rights do not override those interests. This includes carefully considered and specific purposes that enable us to enhance the services we provide, but which we believe also benefit our customers and ultimate consumers. It also includes the legitimate interest we have in assessing our products performance and safety and meeting our industry and regulatory obligations. Sensitive personal data (such as health data) requires us to have additional legal grounds to collect and use it. Most commonly, we will rely on the lawful grounds listed below when we use your sensitive personal data.</li>
                            <li><b>Consent</b>: if you have given us your explicit consent to use your personal data.</li>
                            <li><b>Vital interests</b>: if our use of your personal data is needed to protect the vital interests of you or another person and you are incapable of giving your consent. For example, where there is a serious health risk to you or another person.</li>
                            <li><b>Provision of health care</b>: if your personal data is needed for the provision of health care or treatment.</li>
                            <li><b>Public interest</b>: if your personal data is needed in the public interest, such as protecting against serious threats to health and ensuring high standards of our medicinal products and related healthcare.</li>
                    </ul>
                </div>

                <div class="terms_block">
                    <div class="terms_red_heading">5. YOUR CONSENT</div>
                    <p class="light_text">
                        Under certain circumstances, where the legal basis for using your personal data is that you have provided your consent, you may withdraw your consent at any time. If you withdraw your consent, this will not make processing which we undertook before you withdrew your consent unlawful.
                    </p>
                    <p class="light_text">
                        You can withdraw your consent by contacting us using the contact details listed below. 
                    </p>
                </div>

                <div class="terms_block">
                    <div class="terms_red_heading">6. HOW WE WILL KEEP YOUR DATA SECURE</div>
                    <p class="light_text">
                        We have put in place appropriate security measures to prevent your personal data from being accidentally lost or used, accessed, altered or disclosed in an unauthorised way. In addition, we limit access to your personal data by our employees and service providers, to individuals who need access to perform their job or provide a service to us. They will only use your personal data on our instructions and are required to keep your personal data confidential.
                    </p>
                    <p class="light_text">
                        We have put in place procedures to deal with suspected data security breaches and will notify you and any applicable regulators of breaches in accordance with relevant legal requirements.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_red_heading">7. HOW LONG WE KEEP YOUR PERSONAL DATA</div>
                    <p class="light_text">
                        We will only retain your personal data for as long as necessary to fulfil the purposes we collected it for, including for the purposes of satisfying any legal or reporting requirements. To determine the appropriate retention period for personal data, we consider the amount, nature, and sensitivity of the personal data, the potential risk of harm from unauthorised use or disclosure of your personal data, the purposes for which we process your personal data, whether we can achieve those purposes through other means and the applicable legal requirements.
                    </p>
                    <p class="light_text">
                        In some circumstances we may anonymise your personal data so that it can no longer be associated with you, in which case we may use such information without further notice to you.
                    </p>

                </div>
                <div class="terms_block">
                    <div class="terms_red_heading">8. WHO HAS ACCESS TO YOUR PERSONAL DATA?</div>
                    <p class="light_text">
                        We may share personal information with the following:
                    </p>
                    <ul class="light_text">
                        <li>Our staff - your personal data will be accessed by our staff but only where this is needed for their job role.</li>
                        <li>Companies in the same group of companies as us - for the purpose of providing a service to you.</li>
                        <li>Delivery companies - to deliver products that you have ordered from us.</li>
                        <li>Credit reference agencies - so that we can verify your identity, and to provide information on missed or late payments or other activity which may affect your credit score.</li>
                        <li>Other service providers and advisors - such as companies that support our IT, help us analyse the data we hold, process payments, send communications to our customers, provide us with legal or financial advice and help us deliver our services to you.</li>
                        <li>The government or our regulators - where we are required to do so by law or to assist with their investigations or initiatives, including relevant data protection and healthcare regulators. Such parties use the personal data for their own purpose and their own privacy notice will apply to the use of the personal data they hold.</li>
                    </ul>
                    <p class="light_text">
                        We do not disclose personal information except as set out above or where we have a legal obligation to do so, or we need to share information to assist with the investigation and prevention of crime. We may provide other third parties with statistical information and analytics but we will make sure that the information is aggregated and no one can be identified from this information before we disclose it.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_red_heading">9. DO WE TRANSFER YOUR PERSONAL DATA OUTSIDE OF EUROPE?</div>
                    <p class="light_text">
                        In order to process your personal data for the purposes set out in this notice, we may transfer your personal data to third parties and other companies in our group which are based outside of the UK, Switzerland, and the EU and EEA.
                    </p>
                    <p class="light_text">
                        To ensure that your personal data is secure, we will only transfer your information to countries outside of the UK, Switzerland and Europe where we do so in accordance with the UK Data Protection Act, Switzerland's Federal Act on Data Protection, and the EU's General Data Protection Regulation ("GDPR"). The GDPR requires that one of the following conditions applies:
                    </p>
                    <ul class="light_text">
                        <li>the European Commission has decided that the country provides an adequate level of protection for your personal data (in accordance with Article 45 of the GDPR);</li>
                        <li>the transfer is subject to a legally binding and enforceable commitment on the recipient to protect the personal data (in accordance with Article 46 of the GDPR);</li>
                        <li>the transfer is made subject to binding corporate rules (in accordance with Article 47 of the GDPR); or</li>
                        <li>the transfer is based on a derogation from the GDPR restrictions on transferring personal data outside of the EU (in accordance with Article 49)</li>
                    </ul>
                </div>
                <div class="terms_block">
                    <div class="terms_red_heading">10. WHAT RIGHTS DO YOU HAVE?</div>
                    <p class="light_text">
                        Under certain circumstances you have the right to:
                    </p>
                    <ul class="light_text">
                        <li><b>Request access</b> to your personal data (commonly known as a "data subject access request"). This enables you to receive a copy of the personal data we hold about you and to check that we are lawfully processing it.</li>
                        <li><b>Request correction</b> of the personal data that we hold about you. This enables you to have any incomplete or inaccurate personal data we hold about you corrected.</li>
                        <li><b>Request erasure</b> of your personal data. This enables you to ask us to delete or remove personal data where there is no good reason for us continuing to process it. You also have the right to ask us to delete or remove your personal data where you have exercised your right to object to processing (see below).</li>
                        <li><b>Object to processing</b> of your personal data where we are relying on a legitimate interest (or those of a third party) and there is something about your particular situation which makes you want to object to processing on this ground. You can also object to receiving direct marketing.</li>
                        <li><b>Request the restriction of processing</b> of your personal data. This enables you to ask us to suspend the processing of personal data about you under certain circumstances, for example if you want us to restrict processing while the accuracy of the personal data is being established.</li>
                        <li><b>Request that we transfer personal data</b> that you have provided to us to you or another party.</li>
                        <li><b>Request not to be subjected to automated decision-making</b>. We will only use automated decision making and profiling for our online operations in limited circumstances.</li>
                    </ul>
                    <p class="light_text">
                        You can contact us using the contact details at the end of this notice. We will always aim to help you when you wish to exercise your rights but in some instances we may have lawful grounds to reject your request.
                    </p>
                    <p class="light_text">
                        We will investigate any request you make immediately and will respond to you within a month of your request. That period may be extended by us for a further two months where this is needed to help us respond properly (for example if the request is complicated for us to deal with and we need more time) but we will let you know the reasons for the delay. If we decide to not take action on the request, we will inform you of the reasons for not taking action. If you do not agree with a decision we make in relation to a rights request or believe that we are in breach of data protection laws in Europe, then you can lodge a complaint with a data protection regulator in Europe.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_red_heading">11. DIRECT MARKETING</div>
                    <p class="light_text">
                        If you are a healthcare professional or provider and, depending on the marketing preferences that you indicate to us at the time we collect your personal data, we may contact you via post, telephone or electronic methods with information about our products and services. You can opt-out by the options provided to you within our communications or by contacting us using the contact details at the end of this notice.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_red_heading">12. COOKIES</div>
                    <p class="light_text">
                        When you use our website we may collect information about your use of the website through cookies.
                    </p>
                    <p class="light_text">
                        A cookie is a small text file, which contains small amounts of information that passes to the device you are viewing the website on, through your web browser, so that the website can recognise and remember your device.
                    </p>
                    <p class="light_text">
                        We may use cookies and other similar technologies, such as web beacons and web storage to collect information about your website activity, your browser and device. This data helps us to build a profile of users of our website. Some of this data will be aggregated or statistical, which means that we will not be able to identify you individually. If you are logged into an account on our website some information may be associated with that account.
                    </p>
                    <p class="light_text">
                        If you prefer, you can reject browser cookies through our <a href="${contextPath}/cookiepolicy"  class="blueLink">Cookie Policy</a>.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_red_heading">13. CONTACT US</div>
                    <p class="light_text">
                        If you have any questions about how we process your personal data or want to exercise one of your rights, you can contact us or our Data Protection Officer using the details below:
                    </p>
                    <p class="light_text">
                        <a href="mailto:privacy@cslbehring.com">privacy@cslbehring.com</a>
                    </p>
                    <p class="light_text">
                        Or, via the CSL rights portal: <a href="https://privacyinfo.csl.com/" target="_blank">https://privacyinfo.csl.com/</a>
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_red_heading">14. UPDATES TO THIS NOTICE</div>
                    <p class="light_text">
                        We may update this notice from time to time to reflect changes in the way we process personal data (e.g., if we implement new systems or processes that involve new uses of personal data) or to clarify information we have provided in the notice. Our changes will be in accordance with applicable data protection laws.
                    </p>
                    <p class="light_text">
                        We recommend that you check for updates to this notice from time to time but we will notify you directly about changes to this notice or the way we use your personal data when we are legally required to do so.
                    </p>
                    <p class="light_text">
                        <i>LAST UPDATED: MAY 2021 Publication date</i>
                    </p>
                </div>

            </div>
        </main>
</template:page>