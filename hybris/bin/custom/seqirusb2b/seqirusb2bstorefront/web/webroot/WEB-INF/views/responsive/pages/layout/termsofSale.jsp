<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<template:page pageTitle="${pageTitle}">

 <main  role="main" class="home_container container col-xs-12" >
<div class="col-md-12 margin-control" >
                <div class="terms_center_title">TERMS AND CONDITIONS OF SALE</div>
                <div class="terms_center_title">SEQIRUS UK LIMITED</div>
                <div class="header_subtext">
                    (Registered in England & Wales (registered number 9614642) registered office The Point, 29 Market Street, Maidenhead, Berkshire SL6 8AA VAT No: GB 685546196) 
                </div>
                <div class="terms_block">
                    <div class="terms_heading">1. DEFINITIONS AND INTERPRETATION</div>
                    <p class="light_text">
                        The following definitions and rules of interpretation apply to these Terms and Conditions of Sale:
                    </p>
                    <p class="light_text">
                        <b>Seqirus</b>: Seqirus UK Limited, a limited liability company under English Law, also known under its trade name "Seqirus"<br>
                        <b>Buyer</b>: the buyer of Products sold by Seqirus.<br>
                        <b>Contract</b>: the agreement between Seqirus and the Buyer for the sale and purchase of Products pursuant to an Order Form or otherwise, incorporating or governed by these Terms and Conditions of Sale.<br>
                        <b>Order Form</b>: the written order of the Buyer for the Products.<br>
                        <b>Product(s)</b>: the human vaccine product(s) identified on the Order Form or otherwise.

                    </p>
                </div>
                
                <div class="terms_block">
                    <div class="terms_heading">2. APPLICATION OF TERMS AND CONDITIONS OF SALE, ORDER ACKNOWLEDGEMENT</div>
                    <p class="light_text">
                        2.1&nbsp;&nbsp;&nbsp;These Terms and Conditions of Sale apply to all Seqirus sales of Products to the Buyer. Any variation to these Terms and Conditions of Sale, any contradicting General Contract Terms of the Buyer, and any Seqirus representations about the Products, shall have no legal effect unless expressly agreed or confirmed by Seqirus in writing. The Buyer shall have no right to rely on any statement, promise or representation made or given by or on behalf of Seqirus which is not set out in the Contract or otherwise confirmed by Seqirus in writing.
                    </p>
                    <p class="light_text">
                        2.2&nbsp;&nbsp;&nbsp;No order placed by the Buyer shall be deemed to be accepted by Seqirus until a written order acknowledgement is issued by Seqirus or (if earlier) Seqirus delivers the Products to the Buyer.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">3. ORDERS</div>
                    <p class="light_text">
                        3.1&nbsp;&nbsp;&nbsp;Orders should be made by either a) addressing the Order Form to Seqirus at the address or telephone / fax numbers indicated on the  Order Form, or as otherwise notified by Seqirus to the Buyer  or b) by filling out the Order Form via the Buyer online account at www.flu360.uk.  A purchase order is not required to purchase a Product under the www.flu360.uk platform.
                    </p>
                    <p class="light_text">
                        3.2&nbsp;&nbsp;&nbsp;It is the sole responsibility of the Buyer to ensure that it is in receipt of all necessary licenses, clearances and other consents necessary for its purchase of the Product(s). Failure by the Buyer to ensure the same shall not relieve the Buyer of its obligations to purchase and pay for the Product(s) ordered under the Contract.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">4. DELIVERY</div>
                    <p class="light_text">
                        4.1&nbsp;&nbsp;&nbsp;Any reference in the Contract to any delivery terms (such as Ex Works, etc) is a reference to ICC Incoterms as the same are applicable as of the Contract date.
                    </p>
                    <p class="light_text">
                        4.2&nbsp;&nbsp;&nbsp;Seqirus delivers Products in accordance with its delivery schedule or as agreed with the Buyer in writing. Any dates specified by Seqirus for delivery of the Products are intended to be an estimate and time for delivery shall not be made of the essence by notice. If no dates are so specified, delivery shall be made within a reasonable period of time from the time of the order confirmation.
                    </p>
                    <p class="light_text">
                        4.3&nbsp;&nbsp;&nbsp;Subject to the other provisions of these Terms and Conditions of Sale, Seqirus shall not be liable for any direct, indirect or consequential loss caused directly or indirectly by any delay in the delivery of the Products (unless caused by Seqirus' wilful intent or gross negligence).
                    </p>
                    <p class="light_text">
                        4.4&nbsp;&nbsp;&nbsp;Seqirus may deliver the Products in separate instalments. Each separate instalment shall be invoiced and paid for in accordance with the provisions of the Contract.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">5. NON-DELIVERY</div>
                    <p class="light_text">
                        5.1&nbsp;&nbsp;&nbsp;The quantity of any consignment of Products as recorded by Seqirus upon despatch from Seqirus' place of business shall be conclusive evidence of the quantity received by the Buyer on delivery unless the Buyer can provide conclusive evidence proving the contrary.
                    </p>
                    <p class="light_text">
                        5.2&nbsp;&nbsp;&nbsp;Except in the case of intent or gross negligence on the part of Seqirus, any liability of Seqirus for non- delivery of all or parts of the Products shall be limited to the obligation to supply the undelivered Products within a reasonable period of time or to issue a credit note at the pro rata Contract rate against any invoice raised for such Products.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">6. RISK/TITLE</div>
                    <p class="light_text">
                        6.1&nbsp;&nbsp;&nbsp;The Products are at the risk of the Buyer from the time of delivery in accordance with the applicable Incoterm. Legal title to the Products shall transfer simultaneously with the transfer of risk.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">7. PRICE, INVOICES, PAYMENT</div>
                    <p class="light_text">
                        7.1&nbsp;&nbsp;&nbsp;The price of sales is referred to per unit of the Product, excluding any tax burdens. All applicable sales taxes shall be payable by the Buyer at the rate applicable on the date of the relevant invoice. Unless otherwise agreed in writing between the Parties, the price shall be Seqirus' list price relating to the Buyer's territory as applicable at the date of shipment. All payments by the Buyer shall be made in UK� sterling by wire transfer of immediately available funds or such other currency as may be specified by Seqirus in the Contract.
                    </p>
                    <p class="light_text">
                        7.2&nbsp;&nbsp;&nbsp;Seqirus may invoice the Buyer at any time after the Products have been notified as ready for delivery. Time of payment shall be of the essence. No payment shall be deemed to have been received until Seqirus has received cleared funds.
                    </p>
                    <p class="light_text">
                        7.3&nbsp;&nbsp;&nbsp;Except where agreed otherwise in writing, should a Contract terminate for whatever reason, any payment to be made to Seqirus under such Contract shall become due for immediate payment on the termination date. The Buyer shall make all payments due under the Contract in full without any deduction whether by way of set-off, counterclaim, discount, abatement or otherwise unless the Buyer has a valid court order requiring an amount equal to such deduction to be paid by Seqirus to the Buyer.
                    </p>
                    <p class="light_text">
                        7.4&nbsp;&nbsp;&nbsp;If the Buyer fails to pay Seqirus any sum due to Seqirus, the Buyer shall pay interest on such sum accruing daily from the due date to the date of payment at an annual rate of 8% p.a. (eight percent per annum) plus the Bank of England base rate.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">8. DEFECTIVE PRODUCT</div>
                    <p class="light_text">
                        8.1&nbsp;&nbsp;&nbsp;Seqirus shall supply to Buyer the Products in compliance with Seqirus' standard specifications for the applicable Products. Buyer shall inspect the Product delivered and shall notify Seqirus of any defects, damage or shortage within 15 (fifteen) calendar days of receipt by Buyer. Buyer shall notify Seqirus of any hidden or latent defects (i.e. not discoverable by routine quality control inspection), of which it becomes aware, within 5 (five) calendar days following the discovery of any such defect. Thereafter, Buyer will bear the risk for defective Product. If Buyer notifies Seqirus of a defect, damage or shortage within the above-mentioned time-frames and:</p>
                    <ol class="light_text">
                        <li>Seqirus agrees with Buyer's explanation of the cause of such defect, damage or shortage, Seqirus shall replace any defective, damaged or missing Product (unless such defect, damage or shortage is caused by the actions or omissions of Buyer or its agents, or occurs while the Product is not under the control of Seqirus); or</li>
                        <li>Seqirus disagrees with Buyer's explanation of the cause of such defect or damage, the issue shall be submitted to an independent laboratory designated by mutual agreement whose decisions with regard to the Product being defective or damaged shall be final and binding upon the Parties. The costs arising from this process shall be borne by the Party whose claim failed.</li>
                    </ol>
                    <p class="light_text">
                        In no event will Seqirus have any liability to Buyer or to any third party for such defective, damaged or missing Product beyond replacing such Product.
                    </p>


                    <p class="light_text">
                        8.2&nbsp;&nbsp;&nbsp;In respect of any claims made under clause 8.1 above, in no circumstances shall Seqirus have any obligation or liability where such claims are raised later than 12 (twelve) months from the date of delivery to the Buyer of the relevant Product.
                    </p>
                    <p class="light_text">
                        8.3&nbsp;&nbsp;&nbsp;In the event of a defect notice, Seqirus shall be given reasonable opportunity to examine the relevant Products. The Buyer shall not return defective Products unless so directed by Seqirus. Seqirus shall respond to a defect claim in writing as soon as reasonably possible upon completion of its claim examination.
                    </p>
                    <p class="light_text">
                        8.4&nbsp;&nbsp;&nbsp;Seqirus shall not be liable for defects if: (a) the Buyer has used the relevant Products after giving a defect notice; or (b) the defect arose because the Buyer failed to follow Seqirus' instructions or good trade practice; or (c) the Buyer alters or repairs the Products without Seqirus' prior written consent.
                    </p>
                    <p class="light_text">
                        8.5&nbsp;&nbsp;&nbsp;Unless agreed to in advance in writing by Seqirus, Buyer shall not return any Products to Seqirus for any reason whatsoever.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">9. LIABILITY AND LIMITATION OF LIABILITY</div>
                    <p class="light_text">
                        <b>9.1&nbsp;&nbsp;&nbsp;Except in cases stated in clause 9.2 below, Seqirus' total financial liability is limited to the amount of the purchase price for the Products paid by the Buyer to Seqirus pursuant to the Contract, and neither party shall be liable to the other for any pure economic loss, loss of profit, loss of business, loss of goodwill or otherwise, in each case whether direct, indirect or consequential, or other claims for consequential compensation (howsoever caused), or for any special, punitive or incidental damages, arising in connection with this Contract.</b>
                    </p>
                    <p class="light_text">
                        <b>9.2&nbsp;&nbsp;&nbsp;Neither Party excludes any liability for death or personal injury caused by its negligence or that of its employees, agents or sub-contractors.</b>
                    </p>
                    <p class="light_text">
                        <b>9.3&nbsp;&nbsp;&nbsp;Seqirus will not accept the return of any Product sold to Buyer except in relation to confirmed defective Products as determined under the procedure set forth in clause 8.1 above, and Seqirus shall not be liable to Buyer in respect of any Product purchased by Buyer which Buyer is subsequently unable to re-sell.</b>
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">10. FORCE MAJEURE</div>
                    <p class="light_text">
                        Seqirus reserves the right to defer the date of delivery or to cancel the Contract or reduce the volume of the Products ordered by the Buyer if it is prevented from or hindered in the performance of the Contract due to circumstances beyond its reasonable control including, but not limited to epidemic of disease, act of God, shortage of materials, war, labour disputes, accidents, fire, breakdown of machinery, influenza epidemic or pandemic, government requisition or impoundment or other acts of any governmental authority (including, without limitation, any regulatory authority), riot or civil commotion and any other acts, events or circumstances beyond Seqirus reasonable control whether or not similar in kind to the above causes; provided that, if the event in question continues for a continuous period in excess of ninety (90) days, the Buyer shall be entitled to give notice in writing to Seqirus to terminate any unfulfilled part of the Contract.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">11. PACKAGING</div>
                    <p class="light_text">
                        The Products may only be resold in their original packaging. The Buyer may not change the packaging of any of the Products or remove, alter or tamper with any trade marks, trade names, labels, or numbers on the Products or alter the appearance of the Products or any packages in which the Products are packed.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">12. CONFIDENTIALITY</div>
                    <p class="light_text">
                        The Buyer shall keep in strict confidence all information of a confidential nature which has been disclosed to the Buyer by Seqirus or its agents and the Buyer shall restrict disclosure of such confidential material to such of its employees, agents or sub-contractors as need to know the same and shall ensure that such employees, agents or sub-contractors are subject to like obligations of confidentiality as are binding the Buyer.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">13. TERMINATION</div>
                    <p class="light_text">
                        Seqirus shall have the right at any time by giving notice in writing to the Buyer to terminate the Contract forthwith if: (a) the Buyer commits a material breach of any of the terms and conditions of the Contract; or (b) the Buyer convenes a meeting of creditors, or enters into liquidation, or has a receiver or manager, administrator or administrative receiver appointed in respect of any part its undertaking, or a resolution is passed or a petition presented to any court for the winding-up of the Buyer or for the granting of an administration order in respect of the Buyer, or any proceedings are commenced  relating to the insolvency or possible insolvency of the Buyer; or (c) the Buyer ceases to carry on its business.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">14. GENERAL</div>
                    <p class="light_text">
                        14.1&nbsp;&nbsp;&nbsp;The formation, existence, construction, performance, validity and all aspects of the Contract shall be governed exclusively by the laws of England & Wales without regard to the conflicts of law provisions, and without reference to the UNCITRAL Convention on the International Sale of Goods. All disputes or differences which may arise between the parties out of or in relation to the Contract or the breach thereof which are not amicably settled shall be subject to the exclusive jurisdiction of the English courts and the parties hereby irrevocable submit to the exclusive jurisdiction of the English courts.
                    </p>
                    <p class="light_text">
                        14.2&nbsp;&nbsp;&nbsp;The failure to exercise or delay in exercising a right or remedy provided by this agreement or by law does not constitute a waiver of the right or remedy or a waiver of other rights or remedies. The invalidity or unenforceability of any of the Contract provisions shall not affect the remainder of the Contract.
                    </p>
                </div>
                <div class="terms_block">
                    <div class="terms_heading">15. COMMUNICATIONS</div>
                    <p class="light_text">
                        Communications between the parties about the Contract shall be in writing.
                    </p>
                </div>
            </div>
           </main> 
           </template:page> 