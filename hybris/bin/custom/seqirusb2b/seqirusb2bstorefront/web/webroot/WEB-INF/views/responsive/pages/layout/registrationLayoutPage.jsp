<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="registerPayingContactInfo" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="registerBillingContactInfo" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="registerInvoicingContractInfo" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="registerShippingLocations" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="registerReviewPage" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="registerBusinessDetails" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>



<template:page pageTitle="${pageTitle}">

      <div class="container-fluid bussinessmaincontent col-xs-12">
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb padLeft79">
         <c:url value="/" var="homeUrl" />
            <li class="breadcrumb-item active"><a href="${homeUrl}" class="blacktext"><spring:theme code="form.register.homeLabel" /></a></li>
            <li class="breadcrumb-item" aria-current="page"><spring:theme code="form.register.createAccount.reg" /></li>
         </ol>
      </nav>
      
      <div class="accountHeader col-xs-12"><spring:theme code="form.register.createLabel" /></div>
      <div class="accountcontentArea col-xs-11">
      <div class="leftArea col-xs-12 col-md-4">
         <div>
            <ul class="accountlist">
               <li class="tab listing"><spring:theme code="form.register.userProfile" /></li>
               <li class="tab stepTwo tab-2">
                  <a class="tablinks" data-img-id="img-1" data-section-id="section-1" id="defaultOpen"><spring:theme code="form.register.businessDetails" /></a>
               </li>
               <li class="tab listing-pending tab-3">
                  <spring:theme code="form.register.contactAddress" />
                  <ul class="accountslist">
                     <li class="tab-3-sub no-click"> 
                        <a class="tablinks sub-6 sub-3-spcl marLeft23" data-img-id="img-2" data-section-id="section-2"><spring:theme code="form.register.payingContact" /></a>
                     </li>
                     <%-- <li class="tab-3-sub no-click">
                        <a class="tablinks sub-2 marLeft23" data-img-id="img-2" data-section-id="section-3"><spring:theme code="form.register.billingContact" /></a>
                     </li> --%>
                     <li class="tab-3-sub no-click">
                        <a class="tablinks sub-7 marLeft23" data-img-id="img-2" data-section-id="section-4"><spring:theme code="form.register.invoiceContact" /></a>
                     </li>
                     <li class="tab-3-sub no-click">
                        <a class="tablinks sub-8 marLeft23" data-img-id="img-3" data-section-id="section-5"><spring:theme code="form.register.shippingContact" /></a>
                     </li>
                  </ul>
               </li>
               <li class="tab listing-pending tab-4 no-click">
                  <a class="tablinks" data-img-id="img-4" data-section-id="section-6"><spring:theme code="form.register.reviewInfo" /></a>
               </li>
            </ul>
         </div>
         <div class="white-circle"></div>
         <div class="office-icon">
            <img id="img-1" class="active-img" src="${themeResourcePath}/images/office-icon.svg"/>
            <img id="img-2" class="billingicon img-no" src="${themeResourcePath}/images/billing-contact-latest.svg"/>
            <img id="img-4" class="billingicon img-no" src="${themeResourcePath}/images/billing-contact-latest.svg"/>
            <img id="img-5" class="img-no" src="${themeResourcePath}/images/Group.svg"/>
            <img id="img-6" class="img-no" src="${themeResourcePath}/images/Review-and-confirm.svg"/>
            <img id="img-3" class="img-no" src="${themeResourcePath}/images/Group.svg"/>
            
         </div>
      </div>

	 

      <div class="rightArea col-xs-12 col-md-8" id="rightSection_1">
         
		 <div class="section1_helpSection col-xs-12"><span class="troubleText"><spring:theme code="form.register.trouble" /></span>&nbsp;<span><a  class="helpText" href="${contextPath}/support"><spring:theme code="form.register.help" /></a></span></div>
         <!-- Business Details starts-->
		<%-- <form:form method="post" modelAttribute="customerRegistrationForm" data-toggle="validator" action="${contextPath}/createUser" id="form1"> --%>
		<form:form action="${contextPath}/createuser" method="post" modelAttribute="customerRegistrationForm" data-toggle="validator" data-module="form/Form" id="form1">
         <input type="hidden" name="shipLocationSubmitFlag" id="shipLocationSubmitFlag" value="true"/>
         <input type="hidden" name="payerContactSubmitFlag" id="payerContactSubmitFlag" value="true"/>
         <input type="hidden" name="billingContactSubmitFlag" id="billingContactSubmitFlag" value="true"/>
         <input type="hidden" name="invoiceContactSubmitFlag" id="invoiceContactSubmitFlag" value="true"/>
         <input type="hidden" name="userEmail"  value="${userId}"/>
         <input type="hidden" name="userName"  value="${userName}"/>
         <div id="section-1" class="tab-section active-section">
            <registerBusinessDetails:registerBusinessDetails/>
         </div>
        
         <!-- Business Details ends-->
          <!-- Paying contact information starts -->
          <div id="section-2" class="tab-section">
          
          <registerPayingContactInfo:registerPayingContactInfo/>
          </div>
          
           <%-- <div id="section-3" class="tab-section">
             <registerBillingContactInfo:registerBillingContactInfo/>
         </div> --%>
         <!-- Business Information ends-->
         <!-- Invoicing conatct information Starts -->
         <div id="section-4" class="tab-section">
         <registerInvoicingContractInfo:registerInvoicingContractInfo/>
            
            </div>
         <!-- Invoicing contact information ends -->
         <!-- Shipping Location starts-->
         <div id="section-5" class="tab-section">
            <registerShippingLocations:registerShippingLocations/>
         </div>
         <!-- Shipping Location  ends-->
         <!-- Review and confirm starts -->
         <div id="section-6" class="tab-section">
            <registerReviewPage:registerReviewPage/>
         </div>  
          </form:form>
         </div>

        
         <!-- Review and confirm ends-->
      </div>  
   </div>

      
  
</template:page>
