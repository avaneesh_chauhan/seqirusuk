<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">

	<div class="container-fluid abtmaincontent">
	<nav aria-label="breadcrumb">
			<ol class="breadcrumb padLeft79">
				<c:url value="/" var="homeUrl" />
				<li class="breadcrumb-item active"><a href="${homeUrl}"
					class="blacktext"><spring:theme code="breadcrumb.home" /></a></li>
				 <li class="breadcrumb-item" aria-current="page"><spring:theme
						code="breadcrumb.aboutus" /></li>
			</ol>
		</nav>
		<div class="row">
			<div class="col-md-12">
				<!-- <div id="Homeredline" class="upperredborder"></div>
				<div id="Homeredline1" class="aboutleftborder"></div> -->
				<div class="container-fluid abtmaincontent">
					<div class="row">
						<div class="col-lg-12">
							<cms:pageSlot position="Section1" var="feature">
								<div class="aboutheadingtop aboutpg">
									<h1 class="abouthomeheading1">${feature.h2content}</h1>
									<p class="aboutheaderpara">${feature.paragraphcontent}</p>
								</div>
							</cms:pageSlot>
						</div>
					</div>
					<cms:pageSlot position="Section2B" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					<cms:pageSlot position="Section2D" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					<cms:pageSlot position="Section2E" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>


			</div>
		</div>
	</div>


</template:page>