<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<template:page pageTitle="${pageTitle}">

	<c:url value="/" var="homePageUrl" />
	<div class="container-fluid errorbody-section">
	  <nav class="" aria-label="errorsbreadcrumb">
            <ol class="errorsbreadcrumb errorpadLeft75">
               <li class="errorsbreadcrumb-item active"><a href="${homePageUrl}" class="">Home</a></li>
               <li class="errorsbreadcrumb-item" aria-current="page">Support</li>
            </ol>
         </nav>
		<div class="container">
		
			<cms:pageSlot position="MiddleContent" var="banner">
				<div class="errorspageHeader">${banner.h2content}</div>
				<div class="row">
					<div class="col-md-12 marbot170">
						<div class="erorrblock">
							<div class="errorheading-txt">${banner.paragraphcontent}</div>
							<cms:pageSlot position="BottomContent" var="feature">
							<div class="col-md-12 button-section">
								 <div class="rederorround-btn errorbttn">
									<a class="errorbttntxt" href="${request.contextPath}${feature.url}">${feature.linkName}</a>
								</div> 
							</div>
							</cms:pageSlot>
						</div>
					</div>
				</div>
			</cms:pageSlot>
		</div>
	</div> 

</template:page>


