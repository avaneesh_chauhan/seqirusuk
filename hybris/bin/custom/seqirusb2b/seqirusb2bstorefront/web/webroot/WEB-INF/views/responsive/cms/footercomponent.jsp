<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer"
	tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="col-xs-12 footerdiv">
         <div class="col-xs-12 footertopsection">
           <div class="col-md-3 col-xs-12 footerlogosection">
			<div class="footerlogo"></div>
			<div class="footersiteview">
				Seqirus.co.uk <span class="footerseqlink"></span>
			</div>
			<div class="footersiteview">Portal Usage Agreements</div>
		</div>
		  <div class="col-md-3 col-xs-12 footernavigation"></div>
           <div class="col-md-3 col-xs-12 footerterms">
			<c:forEach items="${navigationNodes[2].children[0].links}"
				var="eachProduct" varStatus="productImageLoop">
				<div class="footertermsheader"
					id="footerlogo_${productImageLoop.index}">
					<c:choose>
						<c:when test="${empty eachProduct.url}">
							<img src="${eachProduct.media.url}" class="footerlogoimg"
								alt="${eachProduct.media.altText}">
						</c:when>
						<c:otherwise>
							<a href="${eachProduct.url}"><img
								src="${eachProduct.media.url}" class="footerlogoimg"
								alt="${eachProduct.media.altText}" /></a>
						</c:otherwise>
					</c:choose>
				</div>
			</c:forEach>
			<c:forEach items="${navigationNodes[0].children[0].links}"
				var="navSiteNode">
				<div class="footerterm">
					<a href="${navSiteNode.url}">${navSiteNode.name}</a><span
						class="footertermsfirst"></span>
				</div>
			</c:forEach>
		</div>
		 <div class="col-md-3 col-xs-12 footerterms">
			<c:forEach items="${navigationNodes[3].children[0].links}"
				var="eachProduct" varStatus="productImageLoop">
				<div class="footertermsheader"
					id="footerlogo_${productImageLoop.index}">
					<c:choose>
						<c:when test="${empty eachProduct.url}">
							<img src="${eachProduct.media.url}" class="footerlogoimg"
								alt="${eachProduct.media.altText}">
						</c:when>
						<c:otherwise>
							<a href="${eachProduct.url}"><img
								src="${eachProduct.media.url}" class="footerlogoimg"
								alt="${eachProduct.media.altText}" /></a>
						</c:otherwise>
					</c:choose>
				</div>
			</c:forEach>
			<c:forEach items="${navigationNodes[1].children[0].links}"
				var="navSiteNode">
				<div class="footerterm">
					<a href="${navSiteNode.url}">${navSiteNode.name}</a><span
						class="footertermsfirst"></span>
				</div>
			</c:forEach>
		</div>
	</div>
	<div class="copyrighttext col-xs-12 col-md-11">
		<spring:theme code="footer.copyright.text.line1" />
		<spring:theme code="footer.copyright.text.line2" />
		<spring:theme code="footer.copyright.text.line3" />
		<br>
		<spring:theme code="footer.copyright.text.line4" />
		<br>
	</div>
</div>
