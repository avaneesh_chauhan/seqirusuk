<div class="col-md-6 col-xs-12 greyboxMar">
	<div>
		<img class="greyboximg" src="${feature.supportImage.url}"
			alt="Resource" />
	</div>
	<h3 class="secondary-header">${feature.text1}</h3>
	<p class="headerpara">${feature.text2}</p>
	<div>
		<a class="whitebtn" href="${contextPath}/support">View support options</a>
	</div>
</div>



