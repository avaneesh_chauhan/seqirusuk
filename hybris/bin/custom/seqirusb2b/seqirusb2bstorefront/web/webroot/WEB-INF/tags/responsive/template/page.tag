<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true"%>
<%@ attribute name="pageCss" required="false" fragment="true"%>
<%@ attribute name="pageScripts" required="false" fragment="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:master pageTitle="${pageTitle}">

	<jsp:attribute name="pageCss">
		<jsp:invoke fragment="pageCss" />
	</jsp:attribute>

	<jsp:attribute name="pageScripts">
		<jsp:invoke fragment="pageScripts" />
	</jsp:attribute>

	<jsp:body>
		<div class="branding-mobile hidden-md hidden-lg">
			<div class="js-mobile-logo">
				<%--populated by JS acc.navigation--%>
			</div>
		</div>
		<%-- <main data-currency-iso-code="${fn:escapeXml(currentCurrency.isocode)}"> --%>
			<%-- <spring:theme code="text.skipToContent" var="skipToContent" />
			<a href="#skip-to-content" class="skiptocontent" data-role="none">${fn:escapeXml(skipToContent)}</a>
			<spring:theme code="text.skipToNavigation" var="skipToNavigation" />
			<a href="#skiptonavigation" class="skiptonavigation" data-role="none">${fn:escapeXml(skipToNavigation)}</a> --%>


			<header:header hideHeaderLinks="${hideHeaderLinks}" />


			
			
			<a id="skip-to-content"></a>
		
			<div class="main__inner-wrapper">
				 <%-- <common:globalMessages />  --%>
				<cart:cartRestoration />
				<jsp:doBody />
			</div>
			
			<footer:footer />
			<c:if test="${cookie['cookie-notification'].getValue() ne 'ACCEPTED' && cmsPage.uid ne 'cookiePolicy'}">
				<div class="cookie_banner">
		            <div class="cookie-content box col-md-12 col-xs-12">
		                <div class="bannerClose col-md-1 col-xs-1">X</div>
		                <div class="cookie_box-body col-md-11 col-xs-11">
		                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 cookieBannerLeft">
		                            <div class="cookieBannerText1 margin-B20">We, and our partners, use cookies to help ensure that our website and services function properly. These cookies are essential and so set automatically. We would also like to use analytical and marketing cookies to make your visit more personal and these cookies are optional.</div>
		                            <div class="cookieBannerText2 margin-B20">You can choose to accept all or manage your options. To learn more about how we use cookies, or how to manage them on your device, please visit our <a href="${contextPath}/cookiepolicy">Cookie Policy.</a></div>
		
		                        </div>
		                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 cookieBannerRight">
		                            <button class="cookieBannerBtn active">Accept All Cookies</button>
		                            <a href="${contextpath}/cookiepolicy" id="manageOptions" class="cookieBannerText3 margin-B20">Manage options</a>
		                        </div>
		                    </div>
		                </div>
		                </div> 
                </c:if>
		<!-- </main> -->

	</jsp:body>

</template:master>
