 <%@ page trimDirectiveWhitespaces="true"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
 <%@ taglib prefix="footer"
 	tagdir="/WEB-INF/tags/responsive/common/footer"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 
 <%-- <c:if test="${component.visible}">
 	<div class="container-fluid">
 	    <div class="footer__top">
 	        <div class="row">
 	            <div class="footer__left col-xs-12 col-sm-12 col-md-9">
 	                <div class="row">
 	                	<c:forEach items="${component.navigationNode.children}" var="childLevel1">
 		                	<c:forEach items="${childLevel1.children}" step="${component.wrapAfter}" varStatus="i">
 							   <div class="footer__nav--container col-xs-12 col-sm-3">
 		                	       <c:if test="${component.wrapAfter > i.index}">
 	                                   <div class="title">${fn:escapeXml(childLevel1.title)}</div>
 	                               </c:if>
 	                               <ul class="footer__nav--links">
 	                                   <c:forEach items="${childLevel1.children}" var="childLevel2" begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
 	                                        <c:forEach items="${childLevel2.entries}" var="childlink" >
 		                                        <cms:component component="${childlink.item}" evaluateRestriction="true" element="li" class="footer__link"/>
 	                                        </c:forEach>
 	                                   </c:forEach>
 	                               </ul>
 	                		   </div>
 						    </c:forEach>
 	                	</c:forEach>
 	               </div>
 	           </div>
 	           <div class="footer__right col-xs-12 col-md-3">
 	               <c:if test="${showLanguageCurrency}">
 	                   <div class="row">
 	                       <div class="col-xs-6 col-md-6 footer__dropdown">
 	                           <footer:languageSelector languages="${languages}" currentLanguage="${currentLanguage}" />
 	                       </div>
 	                       <div class="col-xs-6 col-md-6 footer__dropdown">
 	                           <footer:currencySelector currencies="${currencies}" currentCurrency="${currentCurrency}" />
 	                       </div>
 	                   </div>
 	               </c:if>
 	            </div>
 	        </div>
 	    </div>
 	</div>
 	
 	<div class="footer__bottom">
 	    <div class="footer__copyright">
 	        <div class="container">${fn:escapeXml(notice)}</div>
 	    </div>
 	</div>
 </c:if> --%>
 
 <div class="col-xs-12 footerdiv">
 	<div class="col-xs-12 footertopsection">
 	<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
 		<div class="col-md-3 col-xs-12 footerlogosection">
 			<div class="footerlogo"></div>
 			<div class="footersiteview">
 			<div class="linkbtton"></div>
 				<a class="footer-anchorlink" href="https://www.seqirus.com/" target="blank">seqirus.co.uk </a>
				<br><br>
				<div class="footertermsheader" style="color: #555555;">Customer Service</div>
			<p class="footerpara-1 footermar">Our Customer Service Team is <br> available 08.00 - 17.00</p>
			<div class="footertermsheader" style="color: #555555;">Call</div>
			<div class="footerterm"><a href="tel:0845 745 1500" class="footerterms">0845 745 1500</a></div>
			<br>
			<div class="footertermsheader" style="color: #555555;">Email</div>
			<div class="footerterm"><a href="mailto:service.uk@seqirus.com" class="footerterms">service.uk@seqirus.com</a></div>
 			</div>
			<!-- <div class="footerseqlink">Adverse Event Reporting</div> -->
			<!-- <div class="footerseqlink">Portal Usage Agreements</div> -->
 		</div>
 		</sec:authorize>
 		<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
 		<div class="col-md-3 col-xs-12 footerlogosection">
 			<div class="footerlogo"></div>
 			<div class="footersiteview">
 			<div class="linkbtton"></div>
 				<a class="footer-anchorlink" href="https://www.seqirus.com/" target="blank">seqirus.co.uk </a>
				<br><br>
				<div class="footertermsheader" style="color: #555555;">Customer Service</div>
			<p class="footerpara-1 footermar">Our Customer Service Team is <br> available 08.00 - 17.00</p>
			<div class="footertermsheader" style="color: #555555;">Call</div>
			<div class="footerterm"><a href="tel:0845 745 1500" class="footerterms">0845 745 1500</a></div>
			<br>
			<div class="footertermsheader" style="color: #555555;">Email</div>
			<div class="footerterm"><a href="mailto:service.uk@seqirus.com" class="footerterms">service.uk@seqirus.com</a></div>
 			</div>
 		</div>
 		</sec:authorize>
		<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
 		<div class="col-md-3 col-xs-12 marTop-30">
                <div class="footertermsheader" style="color: #555555;">Navigate          
                </div>
                  <c:choose>
                  	<c:when test="${footerHideStatus ne 'hide'}">
	                	<div class="footerterm marTop-25"><a href="${contextPath}/profile" class="footerterms">Profile</a></div>
		               	<div class="footerterm"><a href="${contextPath}/orders" class="footerterms">Orders</a></div>
	 	                <div class="footerterm"><a href="${contextPath}/invoices" class="footerterms">Financials</a></div>
	 	                <div class="footerterm"><a href="${contextPath}/portalagreement" class="footerterms">Policies</a></div>
 	                </c:when>
 	              </c:choose> 
 	               <div class="footerterm"><a href="${contextPath}/productresources" class="footerterms">Products & Resources</a></div>
 	               <div class="footerterm"><a href="${contextPath}/support" class="footerterms">Support</a></div>
	               <br>
	               <div class="footertermsheader" style="color: #555555;">Legal</div>
	                <div class="footerterm marTop-25"><a href="${contextPath}/termsOfUse" class="footerterms">Terms of Use</a></div>
	               <div class="footerterm"><a href="${contextPath}/termsOfSale" class="footerterms">Terms of Sale</a></div>
	               <div class="footerterm"><a href="${contextPath}/privacyPolicy" class="footerterms">Privacy Policy</a></div>  
	               <div class="footerterm"><a href="${contextpath}/cookiepolicy" class="footerterms">Cookie Policy</a></div>        
	           
              <%--  <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
 	               <div class="footerterm"><a href="${contextPath}/support" class="footerterms">Support</a></div>
               </sec:authorize>          
               </sec:authorize>  --%>         
        </div>
        
        </sec:authorize>
        	<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
		<div class="col-md-3 col-xs-12 marTop-30">
               <div class="footertermsheader" style="color: #555555;">Legal          
               </div>
	               <div class="footerterm marTop-25"><a href="${contextPath}/termsOfUse" class="footerterms">Terms of Use</a></div>
	               <div class="footerterm"><a href="${contextPath}/termsOfSale" class="footerterms">Terms of Sale</a></div>
	               <div class="footerterm"><a href="${contextPath}/privacyPolicy" class="footerterms">Privacy Policy</a></div>
	               <div class="footerterm"><a href="${contextpath}/cookiepolicy" class="footerterms">Cookie Policy</a></div>
         </div>
        </sec:authorize>
 		<div class="col-md-3 col-xs-12 footerterms">
 		<c:forEach items="${component.navigationNode.children}" var="navNode">
 				<c:forEach items="${navNode.children}" var="childNavNode"
 					varStatus="productImageLoop">
 					<c:choose>
 						<c:when test="${not empty childNavNode.children}">
 							<c:forEach items="${childNavNode.children}" var="childNavNode11">
 								<div class="footerterm">
 									<a href="${childNavNode11.entries[0].item.url}">${childNavNode11.name}</a><span
 										class="footertermsfirst"></span>
 								</div>
 							</c:forEach>
 						</c:when>
 						<c:otherwise>
 							<c:if test="${not empty childNavNode.entries[0].item.media.url}">
 									<c:choose>
 										
 										<c:when test="${childNavNode.entries[0].item.media.code eq 'AdjuvantedTrivalentLogo'}">
 											<div id="footerlogos_3">
 									<a href="${childNavNode.entries[0].item.url}" target="blank">
 											<img class="adjuvantlogo"
 										src="${childNavNode.entries[0].item.media.url}"
 										alt="${childNavNode.entries[0].item.media.altText}">
 										</a></div>
 										</c:when>
 										<c:when test="${childNavNode.entries[0].item.media.code eq 'logo-fluad'}">
 											<div id="footerlogo_1">
 									
 											<img class="Fluadlogo"
 										src="${childNavNode.entries[0].item.media.url}"
 										alt="${childNavNode.entries[0].item.media.altText}">
 										</div>
 										</c:when>
 										<c:otherwise>
										<div id="flucelvaxfooterlogo_2">
 									
 											<img class="Flucelvaxlogo"
 										src="${childNavNode.entries[0].item.media.url}"
 										alt="${childNavNode.entries[0].item.media.altText}">
 										</div>
 										</c:otherwise>
 									</c:choose>
 							</c:if>
 							<c:if test="${empty childNavNode.entries[0].item.media.url}">
 								<c:choose>
 									<c:when
 										test="${not empty childNavNode.name && childNavNode.name eq 'Product Site'}">
 										<div class="footertermsheader">
 											<div class="linkbtton"></div>
 											<div class="productlinks" ><a href="${childNavNode.entries[0].item.url}"
 												 class="footerterms" target="_blank" style="color: #555555;"><b>${childNavNode.name}</b></a></div>
 										</div>
 									</c:when>
 									<c:otherwise>
 										<div class="footerterm">
 											<div class="linkbtton"></div>
 											<c:if test="${not empty childNavNode.name && childNavNode.name eq 'Summary Of Products Characteristics'}">
 											<div class="productlinks1"><a href="${childNavNode.entries[0].item.url}"
 												class="footerterms1" target="_blank">${childNavNode.name}</a></div>
 												</c:if>
 												<c:if test="${not empty childNavNode.name && childNavNode.name eq 'Prescription Information'}">
 											<div class="productlinks"><a href="${childNavNode.entries[0].item.url}"
 												class="footerterms" target="_blank">${childNavNode.name}</a></div>
 												</c:if>
 										</div>
 									</c:otherwise>
 								</c:choose>
 
 							</c:if>
 						</c:otherwise>
 					</c:choose>
 				</c:forEach>
 		</c:forEach>
 		</div>
 		<div class="col-md-3 col-xs-12 footertermsheader marTop-30">
 			<div
 				style="font-size: 14px; margin-bottom: 20px;">
 				<spring:theme code="footer.text1" />
 			</div>
 			<div>
 				<p class="footerpara-1">
 					<spring:theme code="footer.text2" />
 					<a href="https://yellowcard.mhra.gov.uk/" class="footerredlink" target="blank"><spring:theme
 							code="footer.link1" /> </a>
 					<spring:theme code="footer.text3" />
 				</p>
 				<p class="footerpara-2">
 					<spring:theme code="footer.text4" />
 					<a href="mailto:ukptc@seqirus.com"" class="footerredlink3"><spring:theme
 							code="footer.link2" /></a>
 					<spring:theme code="footer.text5" />
 				</p>
 			</div>
 		</div>
 	</div>
 	<%-- <div class="copyrighttext col-xs-12 col-md-11">
 		<spring:theme code="footer.copyright.text.line1" />
 		<spring:theme code="footer.copyright.text.line2" />
 		<spring:theme code="footer.copyright.text.line3" />
 		<br>
 		<spring:theme code="footer.copyright.text.line4" />
 		<br>
 	</div> --%>
 	<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
 	<c:if test="${cmsPage.uid eq 'homepage' || cmsPage.uid eq 'about-us' || cmsPage.uid eq 'contact-us'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="logoutstatefooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="logoutstatefooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>
     </sec:authorize>
     <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
      <c:if test="${cmsPage.uid eq 'login' || cmsPage.uid eq 'resetpassword' || cmsPage.uid eq 'changepassword' || cmsPage.uid eq 'changepasswordsuccess'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="passwordfooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="passwordfooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>
     </sec:authorize>
     <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
     <c:if test="${cmsPage.uid eq 'changepassword'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="dashboardfooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="dashboardfooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>
     </sec:authorize>
     <c:if test="${cmsPage.uid eq 'createAccount' || cmsPage.uid eq 'createprofile'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="creataccountfooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="creataccountfooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>
     <c:if test="${cmsPage.uid eq 'joinAccount' || cmsPage.uid eq 'joinAccountApproval'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="joinaccountfooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="joinaccountfooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>
     
     	<c:if test="${cmsPage.uid eq 'register'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="registerfooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="registerfooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>
     	<c:if test="${cmsPage.uid eq 'userDashboard' || cmsPage.uid eq 'userProfile'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="dashboardfooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="dashboardfooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>
     <c:if test="${cmsPage.uid eq 'orderHistoryPage' || cmsPage.uid eq 'productResources'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="ordersfooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="ordersfooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>
       <c:if test="${cmsPage.uid eq 'invoiceLandingPage' || cmsPage.uid eq 'invoiceDetailsPage' || cmsPage.uid eq 'returnsAndCreditsPage'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="invoicefooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="invoicefooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>
     <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
<c:if test="${cmsPage.uid eq 'contact-us' || cmsPage.uid eq 'faqs' || cmsPage.uid eq 'notFound' || cmsPage.uid eq 'portalAgreement'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="loginsupportfooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="loginsupportfooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>     
     </sec:authorize>
     <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
<c:if test="${cmsPage.uid eq 'notFound'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="loginsupportfooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="loginsupportfooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if>     
     </sec:authorize>
     
     
     <c:if test="${cmsPage.uid eq 'termsOfSale' || cmsPage.uid eq 'termsOfUse' || cmsPage.uid eq 'privacyPolicy' || cmsPage.uid eq 'cookiePolicy'}">
 	<div class="copyrighttext col-xs-12 col-md-11">
          	<spring:theme code="termsfooter.copyright.text.line1" />
 			<br>
 			<spring:theme code="termsfooter.copyright.text.line4" />
 			<br>
     </div>
     </c:if> 
     
 </div>