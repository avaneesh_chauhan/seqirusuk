$(document).ready(function () {

    $('#shipping-container').on('click', '.removebutton', function () {
        $(this).parent().parent().parent().remove();
    });
    $(function () {
        $('#fullName,#companyName,#jobtitle,#companyname,#City,#Street').keydown(function (e) {
            if (e.shiftKey || e.ctrlKey || e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (
                        key >= 65 && key <= 90))) {
                    e.preventDefault();
                }
            }
        });
    });
    $("#phoneNumber,#payphone,#invoicephoneNumber").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#errmsg").html("Numbers Only").show().fadeOut("slow");
            return false;
        }
    });
      //Profile Page JS starts from here:-
$('.mob-menu').on('click', function() {
      $(".mob-navigation").slideToggle("slow");
  });

//Datatable Js for Profile page Starts here
  var accTable_pro = $('#proTable').dataTable({
    "ajax": {
        "url": ACC.config.contextPath + "/getMyLocTableData",
	      	    "type": "GET",
	         "dataSrc": "tableData"
	         
	         
    },
   
  "columns": [
          //  { "data": "edit", render : function(data, type, row) {return '<div class="coverCircle" id=data ><i class="fa fa-pencil" aria-hidden="true"></i></div>'},width:"50px"},
           { "data": "addressId", "render": function ( data, type, row, meta ) { return '<a id="'+data+'"><div class="coverCircle" id="'+data+'" ><i class="fa fa-pencil-alt" aria-hidden="true"></i></div></a>'; }},
            { "data": "type"},
            { "data": "name" },
            { "data": "address" },
            { "data": "status"},
            
        ],
    "columnDefs": [
      { "orderable": false, "targets": 0 },
      { "width": 150, "targets":3}
    ],
    "initComplete": function(settings, json) {
   // console.log("type1-"+data);
     
     //var abc = accTable_pro.rows(0).data();
    //console.log(abc[0].name());
 var locations =  $('#proTable').find("td:nth-child(2)").text();
 var payer = "Paying";
 var invoice = "Invoicing";
 var shipping = "Shipping";
 var totalPer = 0;
    var percentage = 0;
    if(locations.indexOf(payer)!=-1){
    percentage=percentage+33.33;
    $(".selectPayTo"). attr('disabled', true);
    }
    if(locations.indexOf(invoice)!=-1){
    percentage=percentage+33.33;
    $(".selectInvoice").attr('disabled', true);
    }
    if(locations.indexOf(shipping)!=-1){
    percentage=percentage+33.33;
    }
    
    $(function() {
    
    $('#proTable tbody tr td:last-child:contains("Processing")').each(function() {
        
        $(this).parent().addClass("noeditsymbol");
    });
});
setTimeout(function() {
 
    
    $("#proTable .noeditsymbol .coverCircle").fadeOut();
}, 10);
$(".sorting").click(function(){
        $(".dataTables_scrollBody thead").hide();
      });
      $("#proTable tbody tr td:nth-child(4)").css("width", "161px");
      $(".proTablecontainer .dataTable tr th:nth-child(1)").removeClass("sorting_asc").addClass("sorting_disabled");
      $("#proTable thead").hide();
      $("#proTable table th").css("background-color","#f4f7fa");
      $('.coverCircle').on('click', function () {
    var id = $(this).attr('id');
		$(".ship-radio-buttons").fadeOut("fast");
        $(".Addbutton_profile").addClass("button-disabled");
         $(".data-table-section").fadeOut("fast", function(){
         $(".add-ship-section").fadeIn("fast");
         	  if(id.includes("Invoicing"))
              {
              $(".show-radioPay").hide("fast");
            $(".show-radioInv").show("fast");
            $(".show-radioShip").hide("fast");
              }
              if(id.includes("Paying"))
              {
              $(".show-radioPay").show("fast");
            $(".show-radioInv").hide("fast");
            $(".show-radioShip").hide("fast");
              }
              if(id.includes("Shipping"))
              {
              $(".show-radioPay").hide("fast");
            $(".show-radioInv").hide("fast");
            $(".show-radioShip").show("fast");
              
              }
              
              fetchEditLocData(id);
        
    });
    });
    },
    "lengthChange": false,
    "bFilter": false,
    "info":false,
    "autoWidth":false,
    "scrollY":        "200px",
    "scrollCollapse": true,
    "paging":         false
    });
  
    //Profile Page JS starts from here:-
    $('.mob-menu').on('click', function () {
        $(".mob-navigation").slideToggle("slow");
    });
    //Profile page Js starts here
    $('.edit-profile').on('click', function () {
        $(this).fadeOut();
        $(".profile-details").fadeOut("fast", function () {
            $(".profile-details-edit").fadeIn("fast");
        });
    });
    $('.edit-company').on('click', function () {
        $(this).fadeOut();
        $(".company-details").fadeOut("fast", function () {
            $(".edit-company-details").fadeIn("fast");
        });
    });
    $('.edit_email').on('click', function () {
        $(this).fadeOut();
        $(".email-section").fadeOut("fast", function () {
            $(".edit-email").fadeIn("fast");
        });
    });
    $('.close-email-edit').on('click', function () {
        $(".edit-email").fadeOut("fast", function () {
            $(".email-section").fadeIn("fast");
            $(".edit_email").fadeIn();
        });
    });
    $('.update-email-info').on('click', function () {
        var edit_email_company = $('#edit_email_company').val();
        var edit_password = $('#edit_password').val();
        $('#company_email_main').text(edit_email_company);
        $('#company_main_password').val(edit_password);
        $(".edit-email").fadeOut("fast", function () {
            $(".email-section").fadeIn("fast");
            $(".edit_email").fadeIn();
        });
    });
  $('.close-company-edit').on('click', function () {
 
$(".edit-company-details").fadeOut("fast", function () {
$(".company-details").fadeIn("fast");
$(".edit-company").fadeIn();
 });
 
location.reload();
 
 });

    $("#invPhone").on("keyup",function(){
        var phoneunmasklength1 = $("#invPhone").val().length;
        if(phoneunmasklength1<10)
        {
          $("#message_phninv").html("Please enter a valid UK format Telephone");
        }
        else{
          $("#message_phninv").html('');
       }
      });
    function checkpostinvoice(){
        $("#invPostal").inputmask("Regex",{regex:"([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
        var nhscount = $("#invPostal").val().length;
        var thirdchar = $("#invPostal").val().slice(2,3);
        var fourthchar = $("#invPostal").val().slice(3,4);
        var fifthchar = $("#invPostal").val().slice(4,5);
          if(nhscount == 6 && thirdchar == " ") 
          {
            $(".invpostcodeerror_edit").empty();
            return true;
          }
          else if(nhscount == 7 && fourthchar == " ")
          {
            $(".invpostcodeerror_edit").empty();
            return true;
          }
          else if(nhscount == 8 && fifthchar == " ")
          {
            $(".invpostcodeerror_edit").empty();
            return true;
          }
          else
          {
            $(".invpostcodeerror_edit").empty().append("Please enter valid UK Post Code");
            return false;
          }
          
        }
    
        $("#invPostal").on("keyup",function(){
            checkpostinvoice();    
        });    
    $('.rqrd_edit_inv, #invAddr2').on('keyup change blur', function(e) { 
        var userinput = $('#invEmail').val();   
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
        $('.rqrd_edit_inv').each(function() {
            var length_count = $('.rqrd_edit_inv').length;
            var flag = $('.rqrd_edit_inv').filter(function(){return this.value != ''});
            if(flag.length >= 0 && (flag.length != length_count)){  
               $('.myproinvoiceUpdate').prop("type", "submit");
               $('.myproinvoiceUpdate').attr('disabled','disabled');   
            }
            else{
                if(pattern.test(userinput) && $('#message_phninv').html()=='' && checkpostinvoice()){
                    $('.myproinvoiceUpdate').prop("type", "button");
                    $('.myproinvoiceUpdate').removeAttr('disabled');
    
                   }
                   else{
                    $('.myproinvoiceUpdate').prop("type", "submit");
                    $('.myproinvoiceUpdate').attr('disabled','disabled');
                   }
                
            }
           
        });  
       
      });
    
    $('.rqrd_edit_inv').each(function() {
        if($('.rqrd_edit_inv').val() == ''){
            $('.myproinvoiceUpdate').attr('disabled','disabled');
        }
        else{
            $('.myproinvoiceUpdate').removeAttr('disabled');
        }
       
    });  
    /*Shiping Location My Profile start*/
    var myprobusinessType = $('#edit_company_business_type').val();
     var shipLicenseEdit= true;
    $("#shipLicNum").on("keyup",function(){
        var myprolicenseID = $(this).attr("id");
        $("#shipLicNum").parent().find(".editshipLicenserror").addClass(myprolicenseID);
        myprolicence(myprolicenseID);
        
    });  
    function myprolicence(passID){
        
        myprobusinessType = myprobusinessType.replace(/-/g, '_');
        if(myprobusinessType == "GP Medical Practice")
        {
        $("#"+passID).inputmask("Regex",{regex:"([0-9]){7}}"});
        var liccount = $("#"+passID).val().length;
          if(liccount < 7)
          {
            $('.'+passID).empty().append("Please provide your 7 digit license");
            shipLicenseEdit = false;
            return false;
          }
          else
          {
            $('.'+passID).empty();
            shipLicenseEdit = true;
            return true;
          }
        }
        else if(myprobusinessType == "Pharmacy" || myprobusinessType == "Hospitals _ Public" || myprobusinessType == "Hospitals _ Private" || myprobusinessType == "Correctional Facility" || myprobusinessType == "Nursing Homes")
        {
          $("#"+passID).inputmask("Regex",{regex:"2([0-9]){6}|[0-9]{4}"});
          var liccount = $("#"+passID).val().length;
          var firstnum = $("#"+passID).val().slice(0,1);
            if(liccount < 4)
            {
              $('.'+passID).empty().append("Please enter a valid license number");
              shipLicenseEdit = false;
              return false;
             
            }
            else if(liccount > 4 && liccount < 7 && firstnum ==2) 
            {
              $('.'+passID).empty().append("Please enter 7 digit license starting with 2");
              shipLicenseEdit = false;
              return false;
            }
            else
            {
              $('.'+passID).empty();
              shipLicenseEdit = true;
              return true;
            }
        }else{
              $('.'+passID).empty();
              shipLicenseEdit = true;
            return true;
        }
      
      } 



     // For NHS code
     var nhs_edit_ship = true;
     $("#shipLicNhs").on("keyup",function(){
       // mypronhs();
        nhs_edit_ship = mypronhs();
      });  
      var nhscodetext = $("#shipLicNhs").val();
      function mypronhs(){
        if(myprobusinessType == "GP Medical Practice")
        {
            $("#shipLicNhs").inputmask("Regex",{regex:"[A-Za-z]{1}([0-9]){5}}"});
            var nhscount = $("#shipLicNhs").val().length;
            if(nhscount < 6)
            {
            $(".editshipNHSerror").html("Please provide a valid NHS code e.g. Y02906");
                if(event.keyCode==8 && nhscount==0){
                    $('.editshipNHSerror').html('');  
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
            $('.editshipNHSerror').html('');
            return true;
            }
        }
        else if(myprobusinessType == "Pharmacy")
        {
            $("#shipLicNhs").inputmask("Regex",{regex:"[F|f]([A-Za-z]|[0-9]){4}}"});
            var nhscount = $("#shipLicNhs").val().length;
            if(nhscount < 5)
            {
            $(".editshipNHSerror").html("Please provide a valid NHS code e.g. F1J4D");
                if(event.keyCode==8 && nhscount==0){
                $('.editshipNHSerror').html('');  
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else {
            $('.editshipNHSerror').html('');  
            return true;
            }    
        }
        else if(myprobusinessType == "Other")
        {
            $("#shipLicNhs").inputmask("Regex",{regex:"[A-Za-z0-9]{}}"});
            var nhscount = $("#shipLicNhs").val().length;
            if(nhscount < 7)
            {
            $(".editshipNHSerror").html("Please provide a valid NHS code e.g. F1J4HSD");
                if(event.keyCode==8 && nhscount==0){
                $('.editshipNHSerror').html('');  
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else {
            $('.editshipNHSerror').html('');  
            return true;
            }    
        }
        else {
            $('.editshipNHSerror').html('');  
            return true;
            } 
    }
    /* Shiping Location My Profile end*/
        /*---------------function For My company Edit-------*/
        $("#edit_company_phone_contact").on("keyup",function(){
            //alert();
            var phoneunmasklength1 = $("#edit_company_phone_contact").val().length;
            //console.log(phoneunmasklength1);
            if(phoneunmasklength1<10)
            {
              $("#message_phn").html("Please enter a valid UK format Telephone");
            }
            else{
              $("#message_phn").html('');
           }
          });
    
          function checkpost(){
            $("#edit_company_zip").inputmask("Regex",{regex:"([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
            var nhscount = $("#edit_company_zip").val().length;
            var thirdchar = $("#edit_company_zip").val().slice(2,3);
            var fourthchar = $("#edit_company_zip").val().slice(3,4);
            var fifthchar = $("#edit_company_zip").val().slice(4,5);
              if(nhscount == 6 && thirdchar == " ") 
              {
                $(".postcodeerror_edit").empty();
                return true;
              }
              else if(nhscount == 7 && fourthchar == " ")
              {
                $(".postcodeerror_edit").empty();
                return true;
              }
              else if(nhscount == 8 && fifthchar == " ")
              {
                $(".postcodeerror_edit").empty();
                return true;
              }
              else
              {
                $(".postcodeerror_edit").empty().append("Please enter valid UK Post Code");
                return false;
              }
              
            }  
    
    
         /*---------------function For My company Edit Ends-------*/   
    
         /*---------------function For Shipping Location Edit-------*/
    
         $("#shipPhone").on("keyup blur",function(){
            //alert();
            var phoneunmasklength1 = $("#shipPhone").val().length;
            //console.log(phoneunmasklength1);
            if(phoneunmasklength1<10)
            {
              $("#message_ship").html("Please enter a valid UK format Telephone");
            }
            else{
              $("#message_ship").html('');
           }
          });
    
          function checkpost_editship(){
            $("#shipPostal").inputmask("Regex",{regex:"([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
            var nhscount = $("#shipPostal").val().length;
            var thirdchar = $("#shipPostal").val().slice(2,3);
            var fourthchar = $("#shipPostal").val().slice(3,4);
            var fifthchar = $("#shipPostal").val().slice(4,5);
              if(nhscount == 6 && thirdchar == " ") 
              {
                $(".postcodeerror_edit_ship").empty();
                return true;
              }
              else if(nhscount == 7 && fourthchar == " ")
              {
                $(".postcodeerror_edit_ship").empty();
                return true;
              }
              else if(nhscount == 8 && fifthchar == " ")
              {
                $(".postcodeerror_edit_ship").empty();
                return true;
              }
              else
              {
                $(".postcodeerror_edit_ship").empty().append("Please enter valid UK Post Code");
                return false;
              }
              
            } 
    
        /*---------------function For Shipping Location Edit Ends here-------*/   
        
         /*---------------function For paying Edit starts here-------*/
    
         $("#payPhone").on("keyup",function(){
            //alert();
            var phoneunmasklength1 = $("#payPhone").val().length;
            //console.log(phoneunmasklength1);
            if(phoneunmasklength1<10)
            {
              $("#message_paying").html("Please enter a valid UK format Telephone");
            }
            else{
              $("#message_paying").html('');
           }
          });
    
          function checkpost_editpaying(){
            $("#payZip").inputmask("Regex",{regex:"([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
            var nhscount = $("#payZip").val().length;
            var thirdchar = $("#payZip").val().slice(2,3);
            var fourthchar = $("#payZip").val().slice(3,4);
            var fifthchar = $("#payZip").val().slice(4,5);
              if(nhscount == 6 && thirdchar == " ") 
              {
                $(".postcodeerror_edit_paying").empty();
                return true;
              }
              else if(nhscount == 7 && fourthchar == " ")
              {
                $(".postcodeerror_edit_paying").empty();
                return true;
              }
              else if(nhscount == 8 && fifthchar == " ")
              {
                $(".postcodeerror_edit_paying").empty();
                return true;
              }
              else
              {
                $(".postcodeerror_edit_paying").empty().append("Please enter valid UK Post Code");
                return false;
              }
              
            } 
    
        /*---------------function For paying Edit ends here-------*/
    /*-------My Company Edit validation Starts here-----------*/
    $('.rqrd_edit_fld_company').each(function() {
        if($('.rqrd_edit_fld_company').val() == ''){
            $('.update-company-info').attr('disabled','disabled');
        }
     
       
    });  
    
    $("#edit_company_zip").on("keyup",function(){
        checkpost();
        
      });
 
      

      $(".addrs_two").on("keyup",function(){
        $('.rqrd_edit_fld_company').trigger('blur');
        
      });

    
      $('.rqrd_edit_fld_company').on('keyup change blur', function(e) { 
        var userinput = $('#edit_company_email_contact').val();   
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
        $('.rqrd_edit_fld_company').each(function() {
            var length_count = $('.rqrd_edit_fld_company').length;
            var flag = $('.rqrd_edit_fld_company').filter(function(){return this.value != ''});
            if(flag.length >= 0 && (flag.length != length_count)){  
               $('.update-company-info').prop("type", "submit");
               $('.update-company-info').attr('disabled','disabled');   
            }
            else{
                if(pattern.test(userinput) && $('#message_phn').html()=='' && checkpost() ){
                    $('.update-company-info').prop("type", "button");
                    $('.update-company-info').removeAttr('disabled');
    
                   }
                   else{
                    $('.update-company-info').prop("type", "submit");
                    $('.update-company-info').attr('disabled','disabled');
                   }
                
            }
           
        });  
       
      });
        /*-------My Company Edit validation Ends here-----------*/
       /*-------Shipping Location Edit validation starts here-----------*/
      $("#shipPostal").on("keyup",function(){
        checkpost_editship();
        
      });


      $('.edit_rqrd_shipping').each(function() {
        if($('.edit_rqrd_shipping').val() == ''){
            $('.new-location').attr('disabled','disabled');
        }
        else{
            $('.new-location').removeAttr('disabled');
        }
       
    }); 
     
      $(".edit_rqrd_shipping, #shipLicNhs, #shipAddr2").on("keyup change", function() { 
         
        
        if($('#shipLicNhs').val().length>0){
            nhs_edit_ship = mypronhs();
        }
        var userinput = $('#shipEmail').val();   
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
        $('.edit_rqrd_shipping').each(function() {
            var length_count = $('.edit_rqrd_shipping').length;
            var flag = $('.edit_rqrd_shipping').filter(function(){return this.value != ''});
            if(flag.length >= 0 && (flag.length != length_count)){  
               $('.new-location').prop("type", "submit");
               $('.new-location').attr('disabled','disabled');
            }
            else{
                if(pattern.test(userinput) && checkpost_editship() && $('#message_ship').html()=='' && shipLicenseEdit  && nhs_edit_ship){
                  $('.new-location').prop("type", "button");
                    $('.new-location').removeAttr('disabled'); 
    
                   }
                   else{
                    $('.new-location').prop("type", "submit");
                    $('.new-location').attr('disabled','disabled');
                   }
                
            }
           
        });  
       
      });
/*-------Shipping Location Edit validation end here-----------*/
 /*-------Paying section Edit validation starts-----------*/
 $("#payZip").on("keyup",function(){
    checkpost_editpaying();
    
  });
  $('.edit_rqrd_paying').each(function() {
    if($('.edit_rqrd_paying').val() == ''){
        $('.update_paying').attr('disabled','disabled');
    }
    else{
        $('.update_paying').removeAttr('disabled');
    }
   
});  
  $('.edit_rqrd_paying, #payAddr2').on('keyup change blur', function(e) { 
    var userinput = $('#payEmail').val();   
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
    $('.edit_rqrd_paying').each(function() {
        var length_count = $('.edit_rqrd_paying').length;
        var flag = $('.edit_rqrd_paying').filter(function(){return this.value != ''});
        if(flag.length >= 0 && (flag.length != length_count)){  
           $('.update_paying').prop("type", "submit");
           $('.update_paying').attr('disabled','disabled');   
        }
        else{
            if(pattern.test(userinput) && checkpost_editpaying() && $('#message_paying').html()==''){
                $('.update_paying').prop("type", "button");
                $('.update_paying').removeAttr('disabled');
               }
               else{
                $('.update_paying').prop("type", "submit");
                $('.update_paying').attr('disabled','disabled');
               }
            
        }
       
    });  
   
  });
/*-------Paying section Edit validation end here-----------*/
      $('.update-company-info').on('click', function () {
        document.getElementById('companyInfoFlag').value = "true";
        
        var edit_company_reg = $('#edit_company_reg').val();
        var edit_company_trading = $('#edit_company_trading').val();
        var edit_company_business_type = $('#edit_company_business_type').val();
        var edit_company_vat = $('#edit_company_vat').val();
        var edit_company_nhc = $('#edit_company_nhc').val();
        var edit_company_type = $('#edit_company_type').val();
        var edit_company_address = $('#edit_company_address').val();
        var edit_company_address2 = '';
        var edit_company_city = $('#edit_company_city').val();
        var edit_company_zip = $('#edit_company_zip').val();
        var edit_company_country = $('#edit_company_country').val();
        var edit_company_contact = $('#edit_company_contact').val();
        var company_email_contact = $('#edit_company_email_contact').val();
        var company_phone_contact = $('#edit_company_phone_contact').val();
        var edit_company_phone_ext = $('#edit_company_phone_ext').val();
        var edit_job_title = $('#edit_job_title').val();
        var edit_first_name = $('#edit_first_name').val();
        var edit_last_name = $('#edit_last_name').val();
        
        
       var  job_title = $('#edit_job_title').val();
       if($('#edit_company_address2').val()!=''){
       	edit_company_address2=$('#edit_company_address2').val()+',';
       }
       /*-------Company view mode---------*/
       $('#company_job_title').text(job_title);
        $('#company_reg').text(edit_company_reg);
        $('#company_trading').text(edit_company_trading);
        $('#company_business_type').text(edit_company_business_type);
        $('#company_vat').text(edit_company_vat);
        $('#company_nhs_num').text(edit_company_nhc);
        $('#company_type').text(edit_company_type);
        $('#company_address').text(edit_company_address+','+edit_company_address2+ edit_company_city);
        $('#company_zip').text(edit_company_zip+','+ edit_company_country);
        $('#company_main_contact').text(edit_company_contact);
        $('#company_email').text(company_email_contact);
        $('#company_job_title').text(edit_job_title);
        $('#company_first_name').text(edit_first_name);
        $('#company_last_name').text(edit_last_name);
        $('#company_contact_phone').text(company_phone_contact+', ext. '+ edit_company_phone_ext);
        $(".edit-company-details").fadeOut("fast", function () {
            $(".company-details").fadeIn("fast");
            $(".edit-company").css("display","none");
        });
        
        $('#edit_job_title').text(job_title);
        $('#edit_company_reg').text(edit_company_reg);
        $('#edit_company_trading').text(edit_company_trading);
        $('#edit_company_business_type').text(edit_company_business_type);
        $('#edit_company_vat').text(edit_company_vat);
        $('#edit_company_nhc').text(edit_company_nhc);
        $('#edit_company_address2').text(edit_company_address2);
        $('#edit_company_address').text(edit_company_address);
        $('#edit_company_zip').text(edit_company_zip);
        $('#edit_company_contact').text(edit_company_contact);
        $('#edit_company_email_contact').text(company_email_contact);
        $('#edit_company_phone_contact').text(company_phone_contact);
        $('#edit_company_city').text(edit_company_city);
        $('#edit_company_country').text(edit_company_country);
        $('#edit_first_name').text(edit_first_name);
        $('#edit_last_name').text(edit_last_name);
        $('#edit_company_phone_ext').text(edit_company_phone_ext);
        $('#edit_job_title').text(edit_job_title);
         
    var dataString = $("#edit_mycompany").serialize();
 
    $.ajax({
      type: "POST",
      data: dataString,
      url: ACC.config.contextPath + "/update-profile",
      async: false,
      dataType: "json",
      success: function (data) {
       
      },
      error: function (data) {
      
      }
});
    });
    $('.close-profile-edit').on('click', function () {
       
        $(".profile-details-edit").fadeOut("fast", function () {
            $(".profile-details").fadeIn("fast");
            $(".edit-profile").fadeIn();
        });
    });

    $('.Addbutton_profile').on('click', function () {
    $('.add-ship-section .help-block').empty();
    $(".ship-radio-buttons").fadeIn("fast");
        $(this).addClass("button-disabled");
       document.getElementById('shipLocationSubmitFlag').value = "true";
        $(".data-table-section").fadeOut("fast", function () {
            $(".add-ship-section").fadeIn("fast");
            $(".show-radioShip").show("fast");
            $(".show-radioPay").hide("fast");
            $(".show-radioInv").hide("fast");
        });
    });
    $('.close-shipping').on('click', function () {
    $('.show-radioShip input[type=text]').val('');
$('.show-radioPay input[type=text]').val('');
$('.show-radioInv input[type=text]').val('');
    $('.add-ship-section .help-block').empty();
        $(".add-ship-section").fadeOut("fast", function(){
       $(".data-table-section").fadeIn("fast");
       $(".Addbutton_profile").removeClass("button-disabled");
     });
     location.reload();
    });
     $('.Nextbutton-profile').on('click', function () {
      $(".add-ship-section").fadeOut("fast", function(){
    $(".confirm-msg").fadeIn("fast");
    
     });
     
   
    });
    $('.finish-btn').on('click', function () {
        $(".confirm-msg").fadeOut("fast", function () {
            $(".data-table-section").fadeIn("fast");
            $(".Addbutton_profile").removeClass("button-disabled");
            
        
         var invFlag = $("#invoiceContactSubmitFlag").val();
    var shipFlag = $("#shipLocationSubmitFlag").val();
    var payFlag = $("#payerContactSubmitFlag").val();
    
    var dataString;
    
	if(invFlag == "true"){
		dataString = $("#edit_invSection").serialize();
	}
    
    if(shipFlag == "true"){
		dataString = $("#edit_shiplocation").serialize();
	}
	
	if(payFlag == "true"){
		dataString = $("#edit_paying").serialize();
	}
     
 
    $.ajax({
      type: "POST",
      data: dataString,
      url: ACC.config.contextPath + "/update-profile",
      async: false,
      dataType: "json",
      success: function (data) {
      }
});
location.reload();
             });  
    });
    //Profile Page JS ends here:-
    //Order Landing Page JS starts from here:-
    $(".menu-title").click(function () {
        $(this).children(".fa").toggleClass("fa-chevron-up fa-chevron-down");
        $(this).next().slideToggle("slow");
    });
    $('.menu-box').click(function (event) {
        $(".order-landing").hide();
        $(".order-details").show();
        $('.expand-menu .active').removeClass('active');
        $(this).addClass('active');
        event.preventDefault();
        $('#order-details-calendar').fullCalendar('render');
    });
    /*$('#search-orders').on('input',function(){
      var searchQuery = $(this).val().toString();
      $('.ordertoOpen').each(function(){
      if($(this).data('searchby').toString().indexOf(searchQuery) > -1) {
     $(this).removeClass('hidden');
     }else {
    $(this).addClass('hidden');
   }
   });
   });*/
    $(".contentarea #filter1").hide();
    $(".contentarea #cityfilter1").hide();
    /* Code for the clear filter button Starts */
    $(".contentarea #clearButton").click(function () {
        $(".filter-block").empty();
        $(this).hide();
        $(".contentarea #primaryContact").val('Product');
        $(".contentarea #city").val('Orders');
    });
    /* Code for the clear filter button Ends- */
    /* Code for the deliver schedule product filter button Starts */
    $(".contentarea #product_filter").on('change', function () {
        var flag = false;
        var currentval = this.value;
        $("#forprimaryfilter .control-label").each(function () {
            if (currentval == $(this).attr('value')) {
                flag = true;
            }
        });
        $(".contentarea #filteredvalues").show();
        $(".contentarea #clearButton").show();
        $(".clear-filter-order-shipping").show();
        if (this.value !== "Select") {
            $(".contentarea #forprimaryfilter").show();
            if ($("#forprimaryfilter .control-label").length + 1 <= 3 && !flag) {
                $("#forprimaryfilter").append(
                    "<div class='form-group floatLeft mRight10 filter-box' ><label class='control-label' for='location' value='" +
                    this.value + "'>" + this.value +
                    "<i class='fa fa-times clr-filter1' id='closeicon'></i></label></div>");
                var str1 = this.value;
                filterArray();
                clrfilter1();
                $(".product-forcast-filter .filter-option-inner-inner").replaceWith('(2) Product');
            }
        } else {
            $("#forprimaryfilter").hide();
        }
    });
    
    function clrfilter1() {
        $(".clr-filter1").unbind("click")
        $(".clr-filter1").click(function () {
            $(".product-forcast-filter .filter-option-inner-inner").text('(2) Product');
            $(this).parent().parent().remove();
            var filtercount1 = $("#forprimaryfilter .control-label").length + 1;
            var filtercount2 = $("#forcityfilter .control-label").length + 1;
            if (filtercount1 == 1 && filtercount2 == 1) {
                $(".contentarea #filteredvalues").hide();
                $(".clear-filter-order-shipping").hide();
            }
        });
    }
    //Filter for order details page at product filter starts
    $(".order-filter-details #product_filter_order_details").on('change', function () {
        var flag = false;
        var currentval = this.value;
        $(".order-filter-details #forprimaryfilter .control-label").each(function () {
            if (currentval == $(this).attr('value')) {
                flag = true;
            }
        });
        $(".contentarea .order-filter-details #filteredvalues").show();
        $(".contentarea .order-filter-details #clearButton").show();
        $(".order-filter-details .clear-filter-order-shipping").show();
        if (this.value !== "Select") {
            $(".contentarea .order-filter-details #forprimaryfilter").show();
            if ($(".order-filter-details #forprimaryfilter .control-label").length + 1 <= 3 && !flag) {
                $(".order-filter-details #forprimaryfilter").append(
                    "<div class='form-group floatLeft mRight10 filter-box' ><label class='control-label' for='location' value='" +
                    this.value + "'>" + this.value +
                    "<i class='fa fa-times clr-filter1' id='closeicon'></i></label></div>");
                var str1 = this.value;
                filterArray();
                clrfilter1();
                $(".order-filter-details .product-forcast-filter .filter-option-inner-inner")
                    .replaceWith('(2) Product');
            }
        } else {
            $(".order-filter-details #forprimaryfilter").hide();
        }
    });
    /*Code for the deliver schedule product button Ends */
    /*Code for the get filter value array Starts*/
    function filterArray() {
        var array = $(".control-label").map(function () {
            return $(this).attr('value')
        }).get();
        //console.log(array);
    }
    /*Code for the get filter value array  Ends */
    /* Code for the deliver schedule order filter button Starts */
    $(".contentarea #order_deliver_filter").on('change', function () {
        var flag = false;
        var currentval = this.value;
        $("#forcityfilter .control-label").each(function () {
            if (currentval == $(this).attr('value')) {
                flag = true;
            }
        });
        $(".contentarea #filteredvalues").show();
        $(".contentarea #clearButton").show();
        $(".clear-filter-order-shipping").show();
        if (this.value !== "Select") {
            $(".contentarea #forcityfilter").show();
            if ($("#forcityfilter .control-label").length + 1 <= 3 && !flag) {
                $("#forcityfilter").append(
                    "<div class='form-group floatLeft mRight10 filter-box' ><label class='control-label' for='city' value='" +
                    this.value + "'>" + this.value +
                    "<i class='fa fa-times clr-filter2' id='closeicon'></i></label></div>");
                var str2 = this.value;
                filterArray();
                clrfilter2();
                $(".order-forcast-filter .filter-option-inner-inner").replaceWith('Orders');
            }
        } else {
            $("#forcityfilter").hide();
        }
    });
    //Filter for order details page at orders filter
    $(".order-filter-details #order_deliver_filter_order_details").on('change', function () {
        var flag = false;
        var currentval = this.value;
        $(".order-filter-details #forcityfilter .control-label").each(function () {
            if (currentval == $(this).attr('value')) {
                flag = true;
            }
        });
        $(".contentarea .order-filter-details #filteredvalues").show();
        $(".contentarea .order-filter-details #clearButton").show();
        $(".order-filter-details .clear-filter-order-shipping").show();
        if (this.value !== "Select") {
            $(".contentarea .order-filter-details #forcityfilter").show();
            if ($(".order-filter-details #forcityfilter .control-label").length + 1 <= 3 && !flag) {
                $(".order-filter-details #forcityfilter").append(
                    "<div class='form-group floatLeft mRight10 filter-box' ><label class='control-label' for='city' value='" +
                    this.value + "'>" + this.value +
                    "<i class='fa fa-times clr-filter2' id='closeicon'></i></label></div>");
                var str2 = this.value;
                filterArray();
                clrfilter2();
                $(".order-filter-details .order-forcast-filter .filter-option-inner-inner").replaceWith(
                    'Orders');
            }
        } else {
            $(".order-filter-details #forcityfilter").hide();
        }
    });
    
    function clrfilter2() {
        $(".clr-filter2").unbind("click")
        $(".clr-filter2").click(function () {
            $(this).parent().parent().remove();
            $(".contentarea #order_deliver_filter").val('Orders');
            var filtercount1 = $("#forprimaryfilter .control-label").length + 1;
            var filtercount2 = $("#forcityfilter .control-label").length + 1;
            if (filtercount1 == 1 && filtercount2 == 1) {
                $(".contentarea #filteredvalues").hide();
                $(".clear-filter-order-shipping").hide();
            }
        });
    }
    /* Code for the deliver schedule order filter button Ends */
    /*Multiple Select box for Order filter start */
    $("#forcast-status").on('change', function () {
        var flag = false;
        var currentval = this.value;
        $("#forcastordersfilter1 .control-label").each(function () {
            if (currentval == $(this).attr('value')) {
                flag = true;
            }
        });
        if (this.value == "Select") {
            clrfilter();
        }
        $("#forcastordervalues").show();
        if (this.value !== "Select") {
            $("#forcastordersfilter1").show();
            $(".clear-filter-order").show();
            // $(".order-status-filter .filter-option-inner-inner").replaceWith('Orders');
            if ($("#forcastordersfilter1 .control-label").length + 1 <= 3 && !flag) {
                $("#forcastordersfilter1").append(
                    "<div class='form-group filter-box' ><label class='control-label' for='orders' value='" +
                    this.value + "'>" + this.value +
                    "<i class='fa fa-times clr-filter' id='closeicon'></i></label></div>");
                // $(".order-status-filter .filter-option-inner-inner").replaceWith('Orders');
                filterArray();
                clrfilter();
            }
        } else {
            $("#forcastordersfilter1").hide();
            $(".clear-filter-order").hide();
        }
    });
    
    function clrfilter() {
        $(".clr-filter").unbind("click")
        $(".clr-filter").click(function () {
            $(".order-filter-drop .filter-option-inner-inner").text('Orders');
            var clearvalue = ($(this).parent().attr('value'));
           // console.log(clearvalue);
            $(this).parent().parent().remove();
            $("#forcast-status").val('Orders');
            var filtercount = $("#forcastordersfilter1 .control-label").length + 1;
            if (filtercount == 1) {
                $("#forcastordersfilter1").hide();
                $("#forcastordervalues").hide();
                $("#forcast-status").val('Orders').change();
                $("#forcastordersfilter1").empty();
                $(".clear-filter-order").hide();
            }
        });
    }
    /*Multiple Select box ends */
    $("#statusclearButton").click(function () {
        $(".filter-option-inner-inner").text('Select');
        $("#forcastordersfilter1").hide();
        $("#forcastordervalues").hide();
        $("#forcast-status").val('Orders').change();
        $("#forcastordersfilter1").empty();
        $(".clear-filter-order").hide();
    });
    /*Forecast select buttons end */
    //Order Landing Page JS ends here:-
    //Datatable Js Starts here

    //console.log(jsonString);
       var season=null;
   var orderid="";
   var accTable = $('#ordersTable').dataTable({

        "ajax": {
            "url": ACC.config.contextPath + "/orders/getChartandTableData?season=" + season + "&orderID=" + orderid,
            "type": "GET",
            "dataSrc": "tableData",
            
        },

        "columns": [{
                "data": "orders", "render": function ( data, type, row, meta ) {return '<a class="invoicetbleredtxt" id="'+data+'">'+data+'</a>'; }
          },
            {
                "data": "location"
          }
      ],
        "columnDefs": [{
            "orderable": true,
            "targets": 1
      }],
        
"initComplete": function (settings, json) {
        	//var test = $("#ordersTable tbody tr").length;
        	$("#ordersTable tbody tr").addClass("ordertoOpen1");
        	var i = 0;
        		$("#ordersTable tbody tr").each(function(){
        			$(this).attr('id',"expandOrder_"+i);
i++;
        		});
        		$(".ordertoOpen1").click(function() {
    var index = this.id.split("_")[1];
    var index1 =parseInt(index)+1;
	var orderid =  $('#ordersTable').find("tr:nth-child("+index1+") td:nth-child(1)").text();
   var orderIndex= $('#orderHistory_'+orderid).parent().parent().parent().attr('id')
    $(".orderHeader").addClass("hide");
    $(".allOrderTableHeader").addClass("hide");
$(".order-landing").hide();
        $(".order-details").show();
    $(".orderHeader" + orderIndex).removeClass("hide");
    $(".tablecontainer").addClass("hide");
    $(".tablecontainer2").addClass("hide");
  //  $(".tab2scroll").removeClass("hide");
   //var orderid = $(this).find(".order-no").attr('id');
     //var orderid =  $(this).parent().find("tr td:nth-child(1)").text();
  //var orderid =52104674;
   pullTableData(orderid);
    pullGraph(orderid);

});
        	
        },
        "lengthChange": false,
        "info": false,
        "autoWidth": false,
        "scrollY": "500px",
        "scrollCollapse": true,
        "paging": false,
        "searching": true,
        "language": {
            "search": ""
        }
    });
  
    accTable = $("#ordersTable_filter input").attr("placeholder", "Search by order #, etc");
    var uri = window.location.toString();
    if (uri.length > 0 && uri.indexOf('/orders')>=0) {
    
   var totalOrderFluadTiv10x = $(".totalOrderFluadTiv10x").attr('id');
   var totalOrderFluadTiv1x = $(".totalOrderFluadTiv1x").attr('id');
   var totalOrderFluadQiv10x = $(".totalOrderFluadQiv10x").attr('id');
   var totalOrderFluadQiv1x = $(".totalOrderFluadQiv1x").attr('id');
   var totalOrderFlucelvaxQiv10x = $(".totalOrderFlucelvaxQiv10x").attr('id');
   var totalOrderFlucelvaxQiv1x = $(".totalOrderFlucelvaxQiv1x").attr('id');
   var totalOrderAdjuvantedTiv10x = $(".totalOrderAdjuvantedTiv10x").attr('id');
   var totalOrderAdjuvantedTiv1x = $(".totalOrderAdjuvantedTiv1x").attr('id');
   var totalOrderFlucelvax1x = $(".totalOrderFlucelvax1x").attr('id');
   var totalOrderFlucelvax10x = $(".totalOrderFlucelvax10x").attr('id');
   
   
   
   var totalShipFluadTiv10x = $(".totalShipFluadTiv10x").attr('id');
   var totalShipFluadTiv1x = $(".totalShipFluadTiv1x").attr('id');
   var totalShipFluadQiv10x = $(".totalShipFluadQiv10x").attr('id');
   var totalShipFluadQiv1x = $(".totalShipFluadQiv1x").attr('id');
   var totalShipFlucelvaxQiv10x = $(".totalShipFlucelvaxQiv10x").attr('id');
   var totalShipFlucelvaxQiv1x = $(".totalShipFlucelvaxQiv1x").attr('id');
   var totalShipAdjuvantedTiv10x = $(".totalShipAdjuvantedTiv10x").attr('id');
   var totalShipAdjuvantedTiv1x = $(".totalShipAdjuvantedTiv1x").attr('id');
   var totalShipFlucelvax1x = $(".totalShipFlucelvax1x").attr('id');
   var totalShipFlucelvax10x = $(".totalShipFlucelvax10x").attr('id');
  
  
  

     var totalShip = $(".totalShip").attr('id');
   var totalOrder = $(".totalOrder").attr('id');
      
 //fetchProgressBar(totalOrderFluadTiv10x, totalOrderFluadTiv1x, totalOrderFluadQiv10x, totalOrderFluadQiv1x, totalOrderFlucelvaxQiv10x, totalOrderFlucelvaxQiv1x, totalOrderAdjuvantedTiv10x, totalOrderAdjuvantedTiv1x,totalOrderFlucelvax1x,totalOrderFlucelvax10x, totalShipFluadTiv10x,totalShipFluadTiv1x,totalShipFluadQiv10x,totalShipFluadQiv1x,totalShipFlucelvaxQiv10x,totalShipFlucelvaxQiv1x,totalShipAdjuvantedTiv10x,totalShipAdjuvantedTiv1x,totalShipFlucelvax1x,totalShipFlucelvax10x, totalShip, totalOrder);
 
 fetchProgressBar('orderLanding',totalOrderFluadTiv10x, totalShipFluadTiv10x, totalOrderFluadTiv1x, totalShipFluadTiv1x, totalOrderFluadQiv10x, totalShipFluadQiv10x, totalOrderFluadQiv1x, totalShipFluadQiv1x,totalOrderFlucelvaxQiv10x,totalShipFlucelvaxQiv10x, totalOrderFlucelvaxQiv1x,totalShipFlucelvaxQiv1x,totalOrderAdjuvantedTiv10x,totalShipAdjuvantedTiv10x,totalOrderAdjuvantedTiv1x,totalShipAdjuvantedTiv1x,totalOrderFlucelvax1x,totalShipFlucelvax1x,totalOrderFlucelvax10x,totalShipFlucelvax10x, totalShip, totalOrder);
 	/* circle graphic code start */
	/* Graph1 starts */

	var percentage_graph1 = (typeof percentage_graph1 !== 'undefined') ? percentage_graph1 : totalShip;
	var totalPercentage_graph1 = (typeof totalPercentage_graph1 !== 'undefined') ? totalPercentage_graph1 : totalOrder;
	$("#circleGraphic").roundSlider({
		sliderType: "min-range",
		circleShape: "pie",
		startAngle: "270",
		lineCap: "round",
		min: 0,
		max: totalPercentage_graph1,
		svgMode: true,
		pathColor: "#f2f4f4",
		borderColor: "#f2f4f4",
		radius: 135,
		width: 10,
		handleSize: "+8",
		sliderType: "min-range",
		startValue: 0,
		readOnly: true,
		mouseScrollAction: false,
		tooltipFormat: "changeTooltip",
		editableTooltip: false,
		valueChange: function (e) {
		 var color = e.isInvertedRange ? "#f2f4f4" : "#DF323F";
		 $("#circleGraphic").roundSlider({ "rangeColor": color, "tooltipColor": color });
		}
	});
	
    var sliderObj = $("#circleGraphic").data("roundSlider");
	sliderObj.setValue(percentage_graph1, totalPercentage_graph1);
	/* Graph1 ends */ 
  }
  
  // Search code starts //
	$("#accordionOrder .panel-title a:first").trigger("click"); // Collapse all but the first row on the page.
	$("#accordionOrder .panel-heading .panel-title a:first").find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
	$(function () {
		$('#accordionOrder .panel-title a[data-toggle="collapse"]').on('click', this, function (event) {
			$('#accordionOrder .panel-collapse').on('show.bs.collapse', function () {
				$(this).siblings('.panel-heading').find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
			});
			$('#accordionOrder .panel-collapse').on('hide.bs.collapse', function () {
				$(this).siblings('.panel-heading').find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
			});
		});
	});
	/*(function () {
		var searchTerm, panelContainerId;
		// Create a new contains that is case insensitive
		$.expr[':'].containsCaseInsensitive = function (n, i, m) {
			return jQuery(n).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
		};
		$('#search-orders').on('change keyup paste click', function () {
			searchTerm = $(this).val();
			$('#accordionOrder > .panel').each(function () {
				panelContainerId = '#' + $(this).attr('id');
				$(panelContainerId + ':not(:containsCaseInsensitive(' + searchTerm + '))').hide();
				$(panelContainerId + ':containsCaseInsensitive(' + searchTerm + ')').show();
			});
		});
	}());*/
	
	(function () {
        var searchTerm, panelContainerId, menubox ;
        // Create a new contains that is case insensitive
        $.expr[':'].containsCaseInsensitive = function (n, i, m) {
            return jQuery(n).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
        };
        $('#search-orders').on('change keyup paste click', function () {
            searchTerm = $(this).val();
            $('#accordionOrder > .panel').each(function () {
                panelContainerId = '#' + $(this).attr('id');
               
                $(panelContainerId + ':not(:containsCaseInsensitive(' + searchTerm + '))').hide();
                $(panelContainerId + ':containsCaseInsensitive(' + searchTerm + ')').show();
                /*$('#accordionOrder .menu-box').each(function () {
                    menuboxid ='#' + $(this).attr('id');
                    $(menuboxid + ':not(:containsCaseInsensitive(' + searchTerm + '))').hide();
                    $(menuboxid + ':containsCaseInsensitive(' + searchTerm + ')').show();
                });*/
            });
        });
    }());
	// Search code Ends //
  
});

function changeTooltip(e) {
var val = e.value;
return "<div style='font-size:26px; font-Family:campton-bold; color:#DF323F; padding:5px;margin:18px 18px 18px -10px;'>"+val + "/" + e.options.max +"</div><div style='font-size:16px; font-Family:campton-bold; color:#5C6E7C; margin:10px 10px 10px -10px;'>Total Doses Delivered<div>";
}
//Fetch edit profileData
function fetchEditLocData(addressId) {   
	// Read SAP account number from textbox
  // var addressId= document.getElementById("accountnumber").value;
  

    $.ajax({

        url: ACC.config.contextPath + "/getEditLocData" + "?addressID=" + addressId,

        type: 'GET',

        dataType: 'json',

        contentType: 'application/json',
        
		
        success: function(jsonData) {

			
            if (typeof(jsonData) != 'undefined') {

              if(addressId.includes("Invoicing"))
              {
              
              document.getElementById('invName').value = jsonData.firstName;
              document.getElementById('invLName').value = jsonData.lastName;
               document.getElementById('invJobtitle').value = jsonData.jobTitle;
               document.getElementById('invEmail').value = jsonData.email;
               document.getElementById('invPhone').value = jsonData.phone;
               document.getElementById('invPhoneExt').value = jsonData.phoneExt;
               document.getElementById('invOrgName').value = jsonData.companyName;
               document.getElementById('invAddr1').value = jsonData.line1;
               document.getElementById('invAddr2').value = jsonData.line2;
               document.getElementById('invPostal').value = jsonData.postalCode;
               document.getElementById('invCity').value = jsonData.city;
               document.getElementById('invCountry').value = jsonData.country.name;
               document.getElementById('invAddrId').value = jsonData.addressID;
               document.getElementById('invoiceContactSubmitFlag').value = "true";

              }
              if(addressId.includes("Paying"))
              {
              
              document.getElementById('payFName').value = jsonData.firstName;
              document.getElementById('payLName').value = jsonData.lastName;
               document.getElementById('payJobTitle').value = jsonData.jobTitle;
               document.getElementById('payEmail').value = jsonData.email;
               document.getElementById('payPhone').value = jsonData.phone;
               document.getElementById('payPhoneExt').value = jsonData.phoneExt;
               document.getElementById('payOrg').value = jsonData.companyName;
               document.getElementById('payAddr1').value = jsonData.line1;
               document.getElementById('payAddr2').value = jsonData.line2;
               document.getElementById('payCity').value = jsonData.city;
               document.getElementById('payZip').value = jsonData.postalCode;
               document.getElementById('payCountry').value = jsonData.country.name;
               document.getElementById('payAddrID').value = jsonData.addressID;
               document.getElementById('payerContactSubmitFlag').value = "true";
              
                
              }
              if(addressId.includes("Shipping"))
              {
               
              document.getElementById('shipFName').value = jsonData.firstName;
              document.getElementById('shipLName').value = jsonData.lastName;
              document.getElementById('shipPhoneExt').value = jsonData.phoneExt;
              document.getElementById('shipOrg').value = jsonData.companyName;
              document.getElementById('shipCountry').value = jsonData.country.name;
               document.getElementById('shipEmail').value = jsonData.email;
               document.getElementById('shipPhone').value = jsonData.phone;
               document.getElementById('shipAddr1').value = jsonData.line1;
               document.getElementById('shipAddr2').value = jsonData.line2;
               document.getElementById('shipPostal').value = jsonData.postalCode;
               document.getElementById('shipLicName').value = jsonData.licenseName;
               document.getElementById('shipLicNum').value = jsonData.licenseNumber;
               document.getElementById('shipLicNhs').value = jsonData.nhsNumber;
               document.getElementById('shipCity').value = jsonData.city;
               document.getElementById('shipAddressId').value = jsonData.addressID;
               document.getElementById('shipLocationSubmitFlag').value = "true";
               
               function chklcns() {
    if ($('#shipLicNum').val().length == 4 || $('#shipLicNum').val().length == 7) {
        shipLicenseEdit = true;
    } else {
        shipLicenseEdit = false;
    }
}

chklcns();
$('#shipLicNum').trigger('blur');
               
               
              }
                
             }
		},
		
 			
    });

}


    	$('.selectPayTo').change(function () {
		
          
            $(".show-radioInv").hide();
           $(".show-radioShip").hide();
            $(".show-radioPay").show();
          
            $('.selectInvoice').prop("checked",false);
            $('.selectShipTo').prop("checked",false);
            
            document.getElementById('payerContactSubmitFlag').value = "true";
               document.getElementById('shipLocationSubmitFlag').value = "";
               document.getElementById('invoiceContactSubmitFlag').value = "";
             
       
    	});

    	$('.selectInvoice').change(function () {
    		
    		
            $(".show-radioShip").hide("fast");
            $(".show-radioInv").show("fast");
            $(".show-radioPay").hide("fast");
           
			$('.selectPayTo').prop("checked",false);
            $('.selectShipTo').prop("checked",false);
            
            document.getElementById('payerContactSubmitFlag').value = "";
               document.getElementById('shipLocationSubmitFlag').value = "";
               document.getElementById('invoiceContactSubmitFlag').value = "true";
        
    		});

    		

    	$('.selectShipTo').change(function () {
 			
            $(".show-radioPay").hide("fast");
            $(".show-radioInv").hide("fast");
            $(".show-radioShip").show("fast");
            
            $('.selectPayTo').prop("checked",false);
            $('.selectInvoice').prop("checked",false);
            
            document.getElementById('payerContactSubmitFlag').value = "";
               document.getElementById('shipLocationSubmitFlag').value = "true";
               document.getElementById('invoiceContactSubmitFlag').value = "";
			
    	});


    	
    	
$(".ordertoOpen").click(function() {
    var index = this.id.split("_")[1];
    var status = $("#orderStatus" + index).val();

    $(".orderHeader").addClass("hide");
    $(".allOrderTableHeader").addClass("hide");

    $(".orderHeader" + index).removeClass("hide");
    $(".tablecontainer").addClass("hide");
    $(".tablecontainer2").addClass("hide");
  //  $(".tab2scroll").removeClass("hide");
    var orderid = $(this).find(".order-no").attr('id');
    pullTableData(orderid);
    pullGraph(orderid);
    
    if (status == "Order Placed") {

       // $(".tablecontainer").addClass("hide");
    }

});

//Graph Section Js starts here
function pullGraph(orderid) {
 var season = $(".hiddenYear").attr('id');

 $.ajax({

        url: ACC.config.contextPath + "/orders/getChartandTableData" + "?orderID=" + orderid + "&season=" + season,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function success(chartandTableData) {
            var jsonData = chartandTableData.chartData;

            if (typeof jsonData != 'undefined') {

                
                var fluadTivQty10x = 0;
                var fluadTivQty1x = 0;
                var fluadQivQty10x = 0;
                var fluadQivQty1x = 0;
                var flucelvaxQivQty10x = 0;
                var flucelvaxQivQty1x = 0;
                var adjuvantedTivQty10x = 0;
                var adjuvantedTivQty1x = 0;
                var flucelvaxQty1x = 0;
                var flucelvaxQty10x = 0;
                var totalQty = 0;

                var fluadTivTotalQty10x = 0;
                var fluadTivTotalQty1x = 0;
                var fluadQivTotalQty10x = 0;
                var fluadQivTotalQty1x = 0;
                var flucelvaxQivTotalQty10x = 0;
                var flucelvaxQivTotalQty1x = 0;
                var adjuvantedTivTotalQty10x = 0;
                var adjuvantedTivTotalQty1x = 0;
                var flucelvaxTotalQty1x = 0;
                var flucelvaxTotalQty10x = 0;
                var totalOrderQty = 0;
                
                

                if (null != jsonData.fluadTivShipQuantity10x) {
                    fluadTivQty10x = jsonData.fluadTivShipQuantity10x;
                }
                
                if (null != jsonData.fluadTivShipQuantity1x) {
                    fluadTivQty1x = jsonData.fluadTivShipQuantity1x;
                }
                
                if (null != jsonData.fluadQivShipQuantity10x) {
                    fluadQivQty10x = jsonData.fluadQivShipQuantity10x;
                }
                
                if (null != jsonData.fluadQivShipQuantity1x) {
                    fluadQivQty1x = jsonData.fluadQivShipQuantity1x;
                }
                
                if (null != jsonData.flucelvaxQivShipQuantity10x) {
                    flucelvaxQivQty10x = jsonData.flucelvaxQivShipQuantity10x;
                }
                if (null != jsonData.flucelvaxQivShipQuantity1x) {
                    flucelvaxQivQty1x = jsonData.flucelvaxQivShipQuantity1x;
                }
                
                if (null != jsonData.adjuvantedTivShipQuantity10x) {
                    adjuvantedTivQty10x = jsonData.adjuvantedTivShipQuantity10x;
                }
                
                if (null != jsonData.adjuvantedTivShipQuantity1x) {
                    adjuvantedTivQty1x = jsonData.adjuvantedTivShipQuantity1x;
                }
                if (null != jsonData.flucelvaxShipQuantity1x) {
                    flucelvaxQty1x = jsonData.flucelvaxShipQuantity1x;
                }
                if (null != jsonData.flucelvaxShipQuantity10x) {
                    flucelvaxQty10x = jsonData.flucelvaxShipQuantity10x;
                }
                if (null != jsonData.totalShipQuantity) {
                    totalQty = jsonData.totalShipQuantity;
                }
                
                
                 if (null != jsonData.fluadTivOrderQuantity10x) {
                    fluadTivTotalQty10x = jsonData.fluadTivOrderQuantity10x;
                }
                
                if (null != jsonData.fluadTivOrderQuantity1x) {
                    fluadTivTotalQty1x = jsonData.fluadTivOrderQuantity1x;
                }
                
                if (null != jsonData.fluadQivOrderQuantity10x) {
                    fluadQivTotalQty10x = jsonData.fluadQivOrderQuantity10x;
                }
                
                if (null != jsonData.fluadQivOrderQuantity1x) {
                    fluadQivTotalQty1x = jsonData.fluadQivOrderQuantity1x;
                }
                
                if (null != jsonData.flucelvaxQivOrderQuantity10x) {
                    flucelvaxQivTotalQty10x = jsonData.flucelvaxQivOrderQuantity10x;
                }
                if (null != jsonData.flucelvaxQivOrderQuantity1x) {
                    flucelvaxQivTotalQty1x = jsonData.flucelvaxQivOrderQuantity1x;
                }
                
                if (null != jsonData.adjuvantedTivOrderQuantity10x) {
                    adjuvantedTivTotalQty10x = jsonData.adjuvantedTivOrderQuantity10x;
                }
                
                if (null != jsonData.adjuvantedTivOrderQuantity1x) {
                    adjuvantedTivTotalQty1x = jsonData.adjuvantedTivOrderQuantity1x;
                }
                if (null != jsonData.flucelvaxOrderQuantity1x) {
                    flucelvaxTotalQty1x = jsonData.flucelvaxOrderQuantity1x;
                }
                if (null != jsonData.flucelvaxOrderQuantity10x) {
                    flucelvaxTotalQty10x = jsonData.flucelvaxOrderQuantity10x;
                }
                

                totalOrderQty = fluadTivTotalQty10x + fluadTivTotalQty1x + fluadQivTotalQty10x + fluadQivTotalQty1x + flucelvaxQivTotalQty10x + flucelvaxQivTotalQty1x + adjuvantedTivTotalQty10x + adjuvantedTivTotalQty1x + flucelvaxTotalQty1x + flucelvaxTotalQty10x;
                
              //  $('.circleGraphic1').empty().append($('.circleGraphic1').circleGraphic(totalQty, totalOrderQty));
             
               

            if(fluadTivQty10x==0 && fluadTivTotalQty10x==0){
            	$('#fluadTiv10PxId').hide();
            }else{
            	$('#fluadTiv10PxId').show();			
				$('#fluadTiv10Px').html(fluadTivQty10x+"/"+fluadTivTotalQty10x);
			}
			
			  if(fluadTivQty1x==0 && fluadTivTotalQty1x==0){
            	$('#fluadTiv1PxId').hide();
            }else{	
            	$('#fluadTiv1PxId').show();		
				$('#fluadTiv1Px').html(fluadTivQty1x+"/"+fluadTivTotalQty1x);
			}
			
			  if(fluadQivQty10x==0 && fluadQivTotalQty10x==0){
            $('#fluadQiv10PxId').hide();
            }else{	
            $('#fluadQiv10PxId').show();		
			$('#fluadQiv10Px').html(fluadQivQty10x+"/"+fluadQivTotalQty10x);
			}
			
			  if(fluadQivQty1x==0 && fluadQivTotalQty1x==0){
            $('#fluadQiv1PxId').hide();
            }else{		
	            $('#fluadQiv1PxId').show();	
				$('#fluadQiv1Px').html(fluadQivQty1x+"/"+fluadQivTotalQty1x);
			}
			
			  if(flucelvaxQivQty10x==0 && flucelvaxQivTotalQty10x==0){
            $('#flucelvaxQiv10PxId').hide();
            }else{		
            $('#flucelvaxQiv10PxId').show();	
			$('#flucelvaxQiv10Px').html(flucelvaxQivQty10x+"/"+flucelvaxQivTotalQty10x);
			}
			
			  if(flucelvaxQivQty1x==0 && flucelvaxQivTotalQty1x==0){
            $('#flucelvaxQiv1PxId').hide();
            }else{			
             $('#flucelvaxQiv1PxId').show();
			$('#flucelvaxQiv1Px').html(flucelvaxQivQty1x+"/"+flucelvaxQivTotalQty1x);
			}
			
			  if(adjuvantedTivQty10x==0 && adjuvantedTivTotalQty10x==0){
            $('#adjuvantedTiv10PxId').hide();
            }else{		
            $('#adjuvantedTiv10PxId').show();	
			$('#adjuvantedTiv10Px').html(adjuvantedTivQty10x+"/"+adjuvantedTivTotalQty10x);
			}
			
			if(adjuvantedTivQty1x==0 && adjuvantedTivTotalQty1x==0){
				$('#adjuvantedTiv1PxId').hide();
			}else{
				$('#adjuvantedTiv1PxId').show();
				$('#adjuvantedTiv1Px').html(adjuvantedTivQty1x+"/"+adjuvantedTivTotalQty1x);
			}
			
			
			if(flucelvaxQty1x==0 && flucelvaxTotalQty1x==0){
				$('#flucelvax1PxId').hide();
			}else{
				$('#flucelvax1PxId').show();
				$('#flucelvax1Px').html(flucelvaxQty1x+"/"+flucelvaxTotalQty1x);
			}
			
			if(flucelvaxQty10x==0 && flucelvaxTotalQty10x==0){
				$('#flucelvax10PxId').hide();
			}else{
				$('#flucelvax10PxId').show();
				$('#flucelvax10Px').html(flucelvaxQty10x+"/"+flucelvaxTotalQty10x);
			}
			
			
			 fetchProgressBar('orderDetails',fluadTivTotalQty10x, fluadTivQty10x, fluadTivTotalQty1x, fluadTivQty1x, fluadQivTotalQty10x, fluadQivQty10x,fluadQivTotalQty1x, fluadQivQty1x,flucelvaxQivTotalQty10x,flucelvaxQivQty10x,flucelvaxQivTotalQty1x,flucelvaxQivQty1x,adjuvantedTivTotalQty10x,adjuvantedTivQty10x,adjuvantedTivTotalQty1x,adjuvantedTivQty1x,flucelvaxTotalQty1x,flucelvaxQty1x,flucelvaxTotalQty10x,flucelvaxQty10x,totalQty, totalOrderQty);
			
			
			
	/* Graph2 Starts */
	var totshipdoses_graph2= 600;
	var valueofdoses_graph2= 600;
	var percentage_graph2 = (typeof percentage_graph2 !== 'undefined') ? percentage_graph2 : totalQty;
	var totalPercentage_graph2 = (typeof totalPercentage_graph2 !== 'undefined') ? totalPercentage_graph2 : totalOrderQty;
	$("#circleGraphic1").roundSlider({
		sliderType: "min-range",
		circleShape: "pie",
		startAngle: "270",
		lineCap: "round",
		min: 0,
		max: totalPercentage_graph2,
		svgMode: true,
		pathColor: "#f2f4f4",
		borderColor: "#f2f4f4",
		radius: 135,
		width: 10,
		handleSize: "+8",
		sliderType: "min-range",
		startValue: 0,
		readOnly: true,
		mouseScrollAction: false,
		tooltipFormat: "changeTooltip",
		editableTooltip: false,
		valueChange: function (e) {
		 var color = e.isInvertedRange ? "#f2f4f4" : "#DF323F";
		 $("#circleGraphic1").roundSlider({ "rangeColor": color, "tooltipColor": color });
		}
	});
	
	var sliderObj1 = $("#circleGraphic1").data("roundSlider");
	sliderObj1.setValue(percentage_graph2, totalPercentage_graph2);
	/* Graph2 ends */
	
}
}
});

}
    	
 
    function pullTableData(orderid){
    $(".tablecontainer2").removeClass("hide");
     var season = $(".hiddenYear").attr('id');
     $('#ordersTable1').DataTable().destroy();
    var accTable1 = $('#ordersTable1').dataTable({
        "ajax": {
            "url": ACC.config.contextPath + "/orders/getChartandTableData?season=" + season + "&orderID=" + orderid,
            "type": "GET",
            "dataSrc": "tableData",
            
        },

        "columns": [{
                "data": "orders" 
          },
            {
                "data": "location"
          }
      ],
        "columnDefs": [{
            "orderable": true,
            "targets": 1
      }],
        "initComplete": function (settings, json) {},
        "lengthChange": false,
        "info": false,
        "autoWidth": false,
        "scrollY": "500px",
        "scrollCollapse": true,
        "paging": false
        
       
    });
    
   
    }
    //Datatable Js Ends here

 $("#ordersDropdown").on('change',function(){
 setTimeout(function(){
 	 var season= $("#ordersDropdown").parent().find(".btn").attr("title");
 	 window.location.href =  ACC.config.contextPath + "/orders" + "?&season=" + season;
 	
 },500)

 });
 
 	function updateCSS(progress, id){
 		if(progress==100){
        		$('#'+id).addClass('progress-now-vertical').removeClass('progress-now-vertical1');
        	}else{
        		$('#'+id).addClass('progress-now-vertical1').removeClass('progress-now-vertical');
        	}
 	}
 
 
     function fetchProgressBar(pageType,totalOrderFluadTiv10x, _val1, totalOrderFluadTiv1x, _val2, totalOrderFluadQiv10x, _val3, totalOrderFluadQiv1x, _val4,totalOrderFlucelvaxQiv10x, _val5, totalOrderFlucelvaxQiv1x, _val6,totalOrderAdjuvantedTiv10x, _val7,totalOrderAdjuvantedTiv1x, _val8,totalOrderFlucelvax1x, _val9,totalOrderFlucelvax10x, _val10, totalShip, totalOrder)
     {
      function progressBar1(_val1, totalOrderFluadTiv10x) {
      if(totalOrderFluadTiv10x!=0){
        var val = _val1,
            progress = 0;
        //val += 0;
        //progress = (val * 100 / totalOrderFluadTiv10x) ? totalOrderFluadTiv10x : val * 1;
        progress = (val / totalOrderFluadTiv10x) * 100; 
        $('.progress-now').attr('style', 'height: ' + progress + '%');
        if(pageType=='orderLanding'){
        	updateCSS(progress, 'totalOrderFluadTiv10x');
        	$('#totalOrderFluadTiv10x').attr('style', 'width: ' + progress + '%');
        }else{
        	updateCSS(progress, 'totalOrderFluadTiv10xDetails');
        	$('#totalOrderFluadTiv10xDetails').attr('style', 'width: ' + progress + '%');
        }
        if (val > 3) val = 0;
        }
    }
    progressBar1(_val1, totalOrderFluadTiv10x);
    
      function progressBar2(_val2, totalOrderFluadTiv1x) {
      if(totalOrderFluadTiv1x!=0){
        var val = _val2,
            progress = 0;
       // val += 0;
        //progress = (val * 1 > totalOrderFluadTiv1x) ? totalOrderFluadTiv1x : val * 1;
        progress = (val / totalOrderFluadTiv1x) * 100; 
        $('.progress-now').attr('style', 'height: ' + progress + '%');
        if(pageType=='orderLanding'){
        	updateCSS(progress, 'totalOrderFluadTiv1x');
        	$('#totalOrderFluadTiv1x').attr('style', 'width: ' + progress + '%');
        }else{
        	updateCSS(progress, 'totalOrderFluadTiv1xDetails');
        	$('#totalOrderFluadTiv1xDetails').attr('style', 'width: ' + progress + '%');
        }
        if (val > 3) val = 0;
        }
    }
    progressBar2(_val2, totalOrderFluadTiv1x);
    
      function progressBar3(_val3, totalOrderFluadQiv10x) {
       if(totalOrderFluadQiv10x!=0){
        var val = _val3,
            progress = 0;
       // val += 0;
        //progress = (val * 1 > totalOrderFluadQiv10x) ? totalOrderFluadQiv10x : val * 1;
        progress = (val / totalOrderFluadQiv10x) * 100; 
        $('.progress-now').attr('style', 'height: ' + progress + '%');
        if(pageType=='orderLanding'){
        	updateCSS(progress, 'totalOrderFluadQiv10x');
        	$('#totalOrderFluadQiv10x').attr('style', 'width: ' + progress + '%');
        }else{
        	updateCSS(progress, 'totalOrderFluadQiv10xDetails');
        	$('#totalOrderFluadQiv10xDetails').attr('style', 'width: ' + progress + '%');
        }
        if (val > 3) val = 0;
        }
    }
    progressBar3(_val3, totalOrderFluadQiv10x);
    
      function progressBar4(_val4, totalOrderFluadQiv1x) {
      if(totalOrderFluadQiv1x!=0){
        var val = _val4,
            progress = 0;
       // val += 0;
        //progress = (val * 1 > totalOrderFluadQiv1x) ? totalOrderFluadQiv1x : val * 1;
        progress = (val / totalOrderFluadQiv1x) * 100; 
        $('.progress-now').attr('style', 'height: ' + progress + '%');
        if(pageType=='orderLanding'){
        	updateCSS(progress, 'totalOrderFluadQiv1x');
        	$('#totalOrderFluadQiv1x').attr('style', 'width: ' + progress + '%');
        }else{
        	updateCSS(progress, 'totalOrderFluadQiv1xDetails');
        	$('#totalOrderFluadQiv1xDetails').attr('style', 'width: ' + progress + '%');
        }
        if (val > 3) val = 0;
        }
    }
    progressBar4(_val4, totalOrderFlucelvaxQiv10x);
    
      function progressBar5(_val5, totalOrderFlucelvaxQiv10x) {
      if(totalOrderFlucelvaxQiv10x!=0){
        var val = _val5,
            progress = 0;
        //val += 0;
        //progress = (val * 1 > totalOrderFlucelvaxQiv10x) ? totalOrderFlucelvaxQiv10x : val * 1;
        progress = (val / totalOrderFlucelvaxQiv10x) * 100; 
        $('.progress-now').attr('style', 'height: ' + progress + '%');
        if(pageType=='orderLanding'){
        	updateCSS(progress, 'totalOrderFlucelvaxQiv10x');
        	$('#totalOrderFlucelvaxQiv10x').attr('style', 'width: ' + progress + '%');
        }else{
        	updateCSS(progress, 'totalOrderFlucelvaxQiv10xDetails');
        	$('#totalOrderFlucelvaxQiv10xDetails').attr('style', 'width: ' + progress + '%');
        }
        if (val > 3) val = 0;
        }
    }
    progressBar5(_val5, totalOrderFlucelvaxQiv10x);
    
      function progressBar6(_val6, totalOrderFlucelvaxQiv1x) {
      if(totalOrderFlucelvaxQiv1x!=0){
        var val = _val6,
            progress = 0;
      //  val += 0;
        //progress = (val * 1 > totalOrderFlucelvaxQiv1x) ? totalOrderFlucelvaxQiv1x : val * 1;
        progress = (val / totalOrderFlucelvaxQiv1x) * 100; 
        $('.progress-now').attr('style', 'height: ' + progress + '%');
        if(pageType=='orderLanding'){
        	updateCSS(progress, 'totalOrderFlucelvaxQiv1x');
        	$('#totalOrderFlucelvaxQiv1x').attr('style', 'width: ' + progress + '%');
        }else{
        	updateCSS(progress, 'totalOrderFlucelvaxQiv1xDetails');
        	$('#totalOrderFlucelvaxQiv1xDetails').attr('style', 'width: ' + progress + '%');
        }
        if (val > 3) val = 0;
        }
    }
    progressBar6(_val6, totalOrderFlucelvaxQiv1x);
    
      function progressBar7(_val7, totalOrderAdjuvantedTiv10x) {
      if(totalOrderAdjuvantedTiv10x!=0){
        var val = _val7,
            progress = 0;
        //val += 0;
       // progress = (val * 1 > totalOrderAdjuvantedTiv10x) ? totalOrderAdjuvantedTiv10x : val * 1;
        progress = (val / totalOrderAdjuvantedTiv10x) * 100; 
        $('.progress-now').attr('style', 'height: ' + progress + '%');
        if(pageType=='orderLanding'){
        	updateCSS(progress, 'totalOrderAdjuvantedTiv10x');
        	$('#totalOrderAdjuvantedTiv10x').attr('style', 'width: ' + progress + '%');
        }else{
        	updateCSS(progress, 'totalOrderAdjuvantedTiv10xDetails');
        	$('#totalOrderAdjuvantedTiv10xDetails').attr('style', 'width: ' + progress + '%');
        }
        if (val > 3) val = 0;
        }
    }
    progressBar7(_val7, totalOrderAdjuvantedTiv10x);
    
      function progressBar8(_val8, totalOrderAdjuvantedTiv1x) {
      if(totalOrderAdjuvantedTiv1x!=0){
        var val = _val8,
            progress = 0;
       // val += 0;
       // progress = (val * 1 > totalOrderAdjuvantedTiv1x) ? totalOrderAdjuvantedTiv1x : val * 1;
        progress = (val / totalOrderAdjuvantedTiv1x) * 100; 
        $('.progress-now').attr('style', 'height: ' + progress + '%');
         if(pageType=='orderLanding'){
         	updateCSS(progress, 'totalOrderAdjuvantedTiv1x');
        	$('#totalOrderAdjuvantedTiv1x').attr('style', 'width: ' + progress + '%');
        }else{
        	updateCSS(progress, 'totalOrderAdjuvantedTiv1xDetails');
        	$('#totalOrderAdjuvantedTiv1xDetails').attr('style', 'width: ' + progress + '%');
        }
        if (val > 3) val = 0;
        }
    }
    progressBar8(_val8, totalOrderAdjuvantedTiv1x);
    
    function progressBar9(_val9, totalOrderFlucelvax1x) {
    if(totalOrderFlucelvax1x!=0){
        var val = _val9,
            progress = 0;
       // val += 0;
        //progress = (val * 1 > totalOrderFlucelvax1x) ? totalOrderFlucelvax1x : val * 1;
        progress = (val / totalOrderFlucelvax1x) * 100; 
        $('.progress-now').attr('style', 'height: ' + progress + '%');
         if(pageType=='orderLanding'){
         	updateCSS(progress, 'totalOrderFlucelvax1x');
        	$('#totalOrderFlucelvax1x').attr('style', 'width: ' + progress + '%');
        }else{
        	updateCSS(progress, 'totalOrderFlucelvax1xDetails');
        	$('#totalOrderFlucelvax1xDetails').attr('style', 'width: ' + progress + '%');
        }
        if (val > 3) val = 0;
        }
    }
    progressBar9(_val9, totalOrderFlucelvax1x);
    
    function progressBar10(_val10, totalOrderFlucelvax10x) {
    if(totalOrderFlucelvax10x!=0){
        var val = _val10,
            progress = 0;
       // val += 0;
        //progress = (val * 1 > totalOrderFlucelvax10x) ? totalOrderFlucelvax10x : val * 1;
        progress = (val / totalOrderFlucelvax10x) * 100; 
        $('.progress-now').attr('style', 'height: ' + progress + '%');
        if(pageType=='orderLanding'){
        	updateCSS(progress, 'totalOrderFlucelvax10x');
        	$('#totalOrderFlucelvax10x').attr('style', 'width: ' + progress + '%');
        }else{
        	updateCSS(progress, 'totalOrderFlucelvax10xDetails');
        	$('#totalOrderFlucelvax10xDetails').attr('style', 'width: ' + progress + '%');
        }
        if (val > 3) val = 0;
        }
    }
    progressBar10(_val10, totalOrderFlucelvax10x);
    
    //$('.circleGraphic').circleGraphic(totalShip, totalOrder);
    //Graph Section ends here
  }