$(document).ready(function() {
    var uri = window.location.toString();
    if (uri.length > 0 && uri.indexOf('/')>=0)
    {
        if($.cookie('visitor_type')==null && $.cookie('visitor_type')!='healthcare'){
            $('#healthCare').click(function(){
                  $.cookie('visitor_type', 'healthcare', {
                        expires : 1
                    });
          });
        }
    }
   
    //  Cookie notification
    
    $('.cookieBannerBtn').click(function () {
        $.cookie('cookie-notification',  'ACCEPTED', { expires : 365 });
		$.cookie('cookie-preferences',  'ACCEPTED', { expires : 365 });
        $('.cookie_banner').hide();
    });

	
    
    $('.cookiePolicyLogoutBtn').click(function () {
    	if($('#allCookie').is(':checked')){
    		$.cookie('cookie-notification',  'ACCEPTED', { expires : 365 });
			$.cookie('cookie-preferences',  'ACCEPTED', { expires : 365 });
	        $('.cookie_banner').hide();
    	}else if($('#essentialCookie').is(':checked')){
    		$.cookie('cookie-notification',  'ACCEPTED', { expires : 365 });
			$.cookie('cookie-preferences',  'NOT_ACCEPTED', { expires : 365 });
	        $('.cookie_banner').hide();
    	}
    	location.reload();
    });
    
    $(".bannerClose").click(function () {
        $(".cookie_banner").hide();
        });
    
    
    $('ul.topnav li a').click(function(){
$('li a').removeClass("active-1");
$(this).addClass("active-1");
});
   
   //join account join button js
   			$("#joinActBtn").click(function () {	            
	            //var accountNumber = $("#accountnumber").val();
				//var orgName = $("#orgName").val();
				//var zipCode = $("#postcode").val();
				//var accessCode=$("#accesscode").val();
				var msg=false;
				if( ($("#accountnumber").val()=="" && $("#accesscode").val()=="")  || ( $("#postcode").val()=="" && $("#accesscode").val()=="")  ){
				msg=true;}
				
				else{
				var data = {};
			
	            data["accountNumber"] = $("#accountnumber").val();
	            //data["orgName"] = $("#orgName").val();
	            data["zipCode"] = $("#postcode").val();
		    	//data["orgReferralEmail"] = $("#orgemail").val();
		    	data["accessCode"] = $("#accesscode").val();
		    	if(message==true)
		    	{
				 	data["searchStatus"] = "failure";
				 	
				 	//alert(data["searchStatus"]);
				}
				 else 
				 {
				 data["searchStatus"] = "success";
				 //alert(data["searchStatus"]);
				
				}
				}
	
	            $.ajax({
	                url: ACC.config.encodedContextPath + "/joinActRegister",
	                dataType: "json",
	                contentType: "application/json",
	                async: false,
	                type: "POST",
	                data: JSON.stringify(data),
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("Accept", "application/json");
	                    xhr.setRequestHeader("Content-Type", "application/json");
	                    xhr.setRequestHeader("CSRFToken", ACC.config.CSRFToken);
	                },
	                success: function (response) {
	         
						//alert(response);
						//debugger;
						if(response == "success")
						{      
							location.href = ACC.config.encodedContextPath + "/dashboard";
							
						}
						else if(response == "failure"){
						location.href = ACC.config.encodedContextPath + "/joinaccountapproval";
				}
						
	                },
	                error: function (jqXHR, textStatus, errorThrown) {
						//console.log("The following error occurred: " + textStatus, errorThrown);
						
						
					}
	
	            });
	        });
			
				//Forgot Password Popup
				$(document).on("click",".js-password-forgotten",function(e){
   				e.preventDefault();

   				var titleHtml = ACC.common.encodeHtml($(this).data("cboxTitle"));
   				ACC.colorbox.open(
   					titleHtml,
   					{
   						href: $(this).data("link"),
   						width:"350px",
   						fixed: true,
   						top: 150,
   						onOpen: function()
   						{
   							$('#validEmail').remove();
   						},
   						onComplete: function(){
   							$('form#forgottenPwdForm').ajaxForm({
   								success: function(data)
   								{
   									if ($(data).closest('#validEmail').length)
   									{
   										
   										if ($('#validEmail').length === 0)
   										{
   											$(".forgotten-password").replaceWith(data);
   											ACC.colorbox.resize();
   										}
   									}
   									else
   									{
   										$("#forgottenPwdForm .control-group").replaceWith($(data).find('.control-group'));
   										ACC.colorbox.resize();
   									}
   								}
   							});
   						}
   					}
   				);
   			});
   
});


   			


  //copy payingdata js
  
$("#payinginfo1").click(function()
{
	//$(this).removeClass('btn-primary').addClass('btn-disabled');
	$("#step2FirstName").val($("#step1FirstName").val());
	$("#step2LastName").val($("#step1LastName").val());
	$("#step2organisationname").val($("#step1organisationname").val());
	$("#step2Email").val($("#step1Email").val());
	$("#step2address").val($("#step1address").val());
	$("#step2phoneNumber").val($("#step1phoneNumber").val());
	$("#step2phoneext").val($("#step1phoneext").val());
	$("#step2jobtitle").val($("#step1jobtitle").val());
	$("#step2street").val($("#step1street").val());
	$("#step2City").val($("#step1City").val());
	$("#step2post").val($("#step1post").val());
	$("#step2country").val($("#step1country").val());
});


$("#payinginfo2").click(function()
{
	//$(this).removeClass('btn-primary').addClass('btn-disabled');
	$("#invoicingFirstName").val($("#step1FirstName").val());
	$("#invoicingLastName").val($("#step1LastName").val());
	$("#invoicingorganisationname").val($("#step1organisationname").val());
	$("#invoicingEmail").val($("#step1Email").val());
	$("#invoicingaddress").val($("#step1address").val());
	$("#invoicingphoneNumber").val($("#step1phoneNumber").val());
	$("#invoicingphoneext").val($("#step1phoneext").val());
	$("#invoicingjobtitle").val($("#step1jobtitle").val());
	$("#invoicingstreet").val($("#step1street").val());
	$("#invoicingCity").val($("#step1City").val());
	$("#invoicingpost").val($("#step1post").val());
	$("#invoicingcountry").val($("#step1country").val());
	$("#invoicingEmail,#invoicingorganisationname,#invoicingaddress,#invoicingCity,#invoicingpost,#invoicingcountry").blur();
	$("#emailerr3").html("");
});


$("#shippingInfo2").click(function()
{
	$("#shippingFirstName").val($("#BussinessfirstName").val());
	$("#shippingLastName").val($("#BussinesslastName").val());
	$("#shippingorgname").val($("#businessName").val());
	$("#shippingEmail").val($("#Bussinessemail").val());
	$("#shippingaddress").val($("#businessAddress").val());
	$("#shippingphoneNumber").val($("#BussinessPhone").val());
	$("#shippingphoneext").val($("#Businessphoneext").val());
	$("#shippingstreet").val($("#businessStreet").val());
	$("#shippingcity").val($("#businessCity").val());
	$("#shippingpostcode").val($("#businessPost").val());
	$("#shippingcountry").val($("#BussinessCountry").val());
	$("#shippingNHS").val($("#BussinesslastNHSCode").val());
	$("#shippingFirstName,#shippingLastName,#shippingorgname,#shippingEmail,#shippingaddress,#shippingphoneNumber,#shippingphoneext,#shippingstreet,#shippingcity,#shippingpostcode,#shippingcountry,#shippingNHS").blur();
});

$("#payInfo1").click(function()
{
	$("#step1FirstName").val($("#BussinessfirstName").val());
	$("#step1LastName").val($("#BussinesslastName").val());
	$("#step1organisationname").val($("#businessName").val());
	$("#step1Email").val($("#Bussinessemail").val());
	$("#step1address").val($("#businessAddress").val());
	$("#step1phoneNumber").val($("#BussinessPhone").val());
	$("#step1phoneext").val($("#Businessphoneext").val());
	$("#step1street").val($("#businessStreet").val());
	$("#step1City").val($("#businessCity").val());
	$("#step1post").val($("#businessPost").val());
	$("#step1country").val($("#BussinessCountry").val());
	$("#step1jobtitle").val($("#Bussinessjobtitle").val());
	$("#step1FirstName,#step1LastName,#step1organisationname,#step1Email,#step1address,#step1phoneNumber,#step1phoneext,#step1street,#step1City,#step1post,#step1country,#step1jobtitle").blur();
});

$("#payerSkipForNow").click(function()
{
	$('#payerContactSubmitFlag').val("false");
});
$("#payerNext").click(function()
{
	$('#payerContactSubmitFlag').val("true");
});

$("#billingSkipForNow").click(function()
{
	$('#billingContactSubmitFlag').val("false");
});
$("#billingNext").click(function()
{
	$('#billingContactSubmitFlag').val("true");
});
$("#invoiceSkipForNow").click(function()
{
	$('#invoiceContactSubmitFlag').val("false");
});
$("#invoiceNext").click(function()
{
	$('#invoiceContactSubmitFlag').val("true");
});

$("#gotosection_6").click(function()
{
//review page business Details
$('#compName').html($('#businessName').val());
$('#regVal').html($('#registrationNumber').val());
$('#vatNum').html($('#vatNumber').val());
$('#tradingVal').html($('#trading').val());
if($("#companytype").children("option:selected").text()!='Select'){
	$('#companyTypeVal').html($("#companytype").children("option:selected").text());
}else{
	$('#companyTypeVal').html('');
}
if($("#businesstype").children("option:selected").text()!='Select'){
	$('#businessTypeVal').html($("#businesstype").children("option:selected").text());
}else{
	$('#businessTypeVal').html('');
}
$('#gmcVal').html($('#BussinesslastNHSCode').val());
$('#businessAddr').html($('#businessAddress').val());
$('#businessStr').html($('#businessStreet').val());
$('#businessci').html($('#businessCity').val());
$('#businesspo').html($('#businessPost').val());
$('#businesscou').html($('#BussinessCountry').val());

//review page paying details
var payingFormattedAdd= $('#step1address').val() +', '+ $('#step1City').val();
$('#payerFullName').html($('#step1FirstName').val());
$('#payerCompany').html($('#step1organisationname').val());
$('#payerEmail').html($('#step1Email').val());
$('#payerPhone').html($('#step1phoneNumber').val());
$('#payerAddress').html(payingFormattedAdd);
$('#payerPostalCode').html($('#step1post').val());
$('#payerCountry').html($('#step1country').val());

if($('#payerContactSubmitFlag').val()=='true' && ($('#step1address').val()!='' || $('#step1City').val() || $('#step1FirstName').val()!='' 
	|| $('#step1organisationname').val()!='' || $('#step1Email').val()!='' || $('#step1phoneNumber').val()!='' || $('#step1post').val()!='' || $('#step1country').val()!='' )){
	$('#reviewPayerId').show();
}else{
	$('#reviewPayerId').hide();
}

//review page billing details
/*var billingFormattedAdd= $('#step2address').val() +', '+ $('#step2City').val();
$('#billingName').html($('#step2FirstName').val());
$('#billingCompany').html($('#step2organisationname').val());
$('#billingEmail').html($('#step2Email').val());
$('#billingPhone').html($('#step2phoneNumber').val());
$('#billingAddress').html(billingFormattedAdd);
$('#billingPostalCode').html($('#step2post').val());
$('#billingCountry').html($('#step2country').val());

if($('#billingContactSubmitFlag').val()=='true' && ($('#step2address').val()!='' || $('#step2City').val() || $('#step2FirstName').val()!='' 
	|| $('#step2organisationname').val()!='' || $('#step2Email').val()!='' || $('#step2phoneNumber').val()!='' || $('#step2post').val()!='' || $('#step2country').val()!='' )){
	$('#reviewBillingId').show();
}else{
	$('#reviewBillingId').hide();
}*/
//review page invoice details
var invoiceFormattedAdd= $('#invoicingaddress').val() +', '+ $('#invoicingCity').val();
$('#invoiceName').html($('#invoicingFirstName').val());
$('#invoiceCompany').html($('#invoicingorganisationname').val());
$('#invoiceEmail').html($('#invoicingEmail').val());
$('#invoicePhone').html($('#invoicingphoneNumber').val());
$('#invoiceAddress').html(invoiceFormattedAdd);
$('#invoicePostalCode').html($('#invoicingpost').val());
$('#invoiceCountry').html($('#invoicingcountry').val());
if($('#invoiceContactSubmitFlag').val()=='true' && ($('#invoicingaddress').val()!='' || $('#invoicingCity').val() || $('#invoicingFirstName').val()!='' 
	|| $('#invoicingorganisationname').val()!='' || $('#invoicingEmail').val()!='' || $('#invoicingphoneNumber').val()!='' || $('#invoicingpost').val()!='' || $('#invoicingcountry').val()!='' )){
	$('#reviewInvoiceId').show();
}else{
	$('#reviewInvoiceId').hide();
}
//shipping Locations
			if($("#shippingLocations").val()==0){
				$('#totalShippingLocations').html('1 shipping location(s) entered');
			}else{
				$('#totalShippingLocations').html($(".add-shipping").length+ ' shipping location(s) entered');
			}

//shipping Locations
/*$("#step5Skip").click(function()
		{
				$('#shipLocationSubmitFlag').val("false");
				$('#totalShippingLocations').html('0 shipping location(s) entered');
		});*/

});

$('#businesstype').change(function(){
	if($("#businesstype").children("option:selected").text()!='Other'){
	      $('#otherBusinessType').hide();
	      $(".width104").removeAttr('required');
	      $("#otherBusinessType").removeClass("has-error has-danger");
	      $(".width104").val('');
	}
	else{
		$(".width104").val('');
		$(".width104").attr('required', 'required');
		$("#otherBusinessType").addClass("has-error has-danger");
	     $('#otherBusinessType').show();
	}
	});
  
  //join account searchaccount js
  
  var message=false;
function isAccountExists(x) {   

	var accesscodeflag=false;
//var orgemail= document.getElementById("orgemail").value;
//var orgname= document.getElementById("orgName").value;	
	var account;		
	var postcode='';
	var triggerAjax=false;
	// Read SAP account number from textbox
	if(x=='accountnumber')
	{
		if($('#accountnumber').val()!='' && $('#postcode').val()!=''){
			$('#joinform').validator();
		 	account= document.getElementById("accountnumber").value;
		 	postcode= document.getElementById("postcode").value;	
		  if($('#accesscode').val()!=''){
	      	$('#accesscode').val('');
	      	$(".lengtherroraccess").show();
	      	$(".jointcodeerror1").show();
      	  }
      	  triggerAjax=true;
      }else{
      	$('#joinform').validator();
      	$(".lengtherroraccess").show();
	    $(".jointcodeerror1").show();
      }
	}
    else{
    	$('#joinform1').validator();
     account= document.getElementById("accesscode").value;
     accesscodeflag=true;
     if($('#accountnumber').val()!='' || $('#postcode').val()!=''){
      	$('#accountnumber').val('');
      	$('#postcode').val('');
      	$(".lengtherror").show();
      }
      triggerAjax=true;
    }
    
    var flag=false;
    if( account===""){
      flag=true;
      
    }

   // alert(account);
   if(triggerAjax==true){
    $.ajax({

        url: ACC.config.contextPath + "/searchAccount" + "?account=" + account+ "&postcode=" + postcode + "&accesscodeflag=" + accesscodeflag,

        type: 'GET',

        dataType: 'json',

        contentType: 'application/json',
        
        success: function(jsonData) {
        
	//alert(jsonData);
           // debugger;
			//console.log(jsonData);
			           
			
            if (((typeof(jsonData) != 'undefined' && x=="accountnumber") &&(postcode !== null && postcode !== '')) ){
            
             //retrieve unit name
              
               $('#accountNameTxt').html(jsonData);
               $('#accountNameText').html(jsonData);
               document.getElementById("successDiv").style.display = 'inline';
               document.getElementById("successSubDiv").style.display = 'inline';
               document.getElementById("joinActDiv").style.display = 'inline';
               $('#joinActBtn').html("Access Account");
               document.getElementById("joinActBtn").disabled = true;
               
               document.getElementById("successIcon1").style.display = 'inline';
               /*if(orgemail !== null && orgemail !== ''){
               document.getElementById("successIcon2").style.display = 'inline';
               }*/
              
               if(postcode !== null && postcode !== ''){
               document.getElementById("successIcon3").style.display = 'inline';
               document.getElementById("jointcodeerror1").style.display = 'none';
               }
               
              
               document.getElementById("failureDiv").style.display = 'none';
               document.getElementById("failureSubDiv").style.display = 'none';
               
               document.getElementById("failureIcon1").style.display = 'none';
               //document.getElementById("failureIcon2").style.display = 'none';
               document.getElementById("failureIcon3").style.display = 'none';
               //document.getElementById("failureIcon4").style.display = 'none';
               document.getElementById("failureIcon5").style.display = 'none';
               
                
             }
             
            
              if (typeof(jsonData) != 'undefined' && x=="accesscode" && x !== null && x !== ''){
              
             //retrieve unit name
              
               $('#accountNameTxt').html(jsonData);
               $('#accountNameText').html(jsonData);
               document.getElementById("successDiv").style.display = 'inline';
               document.getElementById("successSubDiv").style.display = 'inline';
               document.getElementById("joinActDiv").style.display = 'inline';
               document.getElementById("joinActDiv").disabled = false;
               $('#joinActBtn').html("Access Account");
               document.getElementById("joinActBtn").disabled = true;
               
               
               document.getElementById("successIcon5").style.display = 'inline';
               document.getElementById("failureIcon5").style.display = 'none';
               document.getElementById("failureIcon1").style.display = 'none';
               document.getElementById("failureIcon3").style.display = 'none';
             
             
               
               document.getElementById("failureDiv").style.display = 'none';
               document.getElementById("failureSubDiv").style.display = 'none';
                
               
              }
                 
               
              
               
              },
	
		error: function (jqXHR, textStatus, errorThrown) {
		
		if(flag==true){
		 		document.getElementById("failureDiv").style.display = 'none';
                document.getElementById("failureSubDiv").style.display = 'none';
                document.getElementById("joinActDiv").style.display = 'none';
                document.getElementById("joinActDiv").disabled = true;
                
        
                
               
                
                
		}else{
				message=true;
				if(postcode !== null && postcode !== ''){
				document.getElementById("joinActBtn").disabled = false;
				$('#joinActBtn').html("Request Assistance");
				document.getElementById("successDiv").style.display = 'none';
                document.getElementById("successSubDiv").style.display = 'none';
                
                document.getElementById("successIcon1").style.display = 'none';
                //document.getElementById("successIcon2").style.display = 'none';
                document.getElementById("successIcon3").style.display = 'none';
                //document.getElementById("successIcon4").style.display = 'none';
                
                document.getElementById("failureDiv").style.display = 'inline';
                document.getElementById("failureSubDiv").style.display = 'inline';
                document.getElementById("joinActDiv").style.display = 'inline';
                document.getElementById("joinActDiv").disabled = true;
                
                
                document.getElementById("failureIcon1").style.display = 'inline';
                /*if(orgemail !== null && orgemail !== ''){
                document.getElementById("failureIcon2").style.display = 'inline';}*/
                
                
                document.getElementById("failureIcon3").style.display = 'inline';
                }
                
                
                if(x=="accesscode" && x !== null && x !== '' ){
                $('#joinActBtn').html("Request Assistance");
                document.getElementById("failureIcon5").style.display = 'inline';
                document.getElementById("successIcon5").style.display = 'none';
                //document.getElementById("failureIcon1").style.display = 'none';
                //document.getElementById("failureIcon2").style.display = 'none';
                //document.getElementById("failureIcon3").style.display = 'none';
                document.getElementById("joinActBtn").disabled = false;
				
				document.getElementById("successDiv").style.display = 'none';
                document.getElementById("successSubDiv").style.display = 'none';
                
                document.getElementById("successIcon1").style.display = 'none';
                //document.getElementById("successIcon2").style.display = 'none';
                document.getElementById("successIcon3").style.display = 'none';
                //document.getElementById("successIcon4").style.display = 'none';
                
                document.getElementById("failureDiv").style.display = 'inline';
                document.getElementById("failureSubDiv").style.display = 'inline';
                document.getElementById("joinActDiv").style.display = 'inline';
                document.getElementById("joinActDiv").disabled = true;}
                //document.getElementById("failureIcon4").style.display = 'none';
                 
                //console.log("The following error occurred: " + textStatus, errorThrown);
                }
               
                

         },
 			
    });
  }

}

 //joinButton Enable function
$(function () {
        $("input[name='privacyPolicy']").click(function () {
            if ($("#privacyPolicy").is(":checked")) {
                $("#joinActBtn").removeAttr("disabled");
                $("#joinActBtn").focus();
                message=false;
            } else {
                $("#joinActBtn").attr("disabled", "disabled");
            }
        });
    });  
 

$(".dropbtn").hover(function(){                     
    $('span i').toggleClass('fa-angle-up fa-angle-down');
});

 
