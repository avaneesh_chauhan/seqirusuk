/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.fulfilmentprocess.test.actions;

import com.seqirus.fulfilmentprocess.actions.order.ScheduleForCleanUpAction;


/**
 * Test counterpart for {@link ScheduleForCleanUpAction}
 */
public class ScheduleForCleanUp extends TestActionTemp
{
	//EMPTY
}
