/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.fulfilmentprocess.test.actions;

import com.seqirus.fulfilmentprocess.actions.order.OrderManualCheckedAction;

/**
 * Test counterpart for {@link OrderManualCheckedAction}
 */
public class OrderManualChecked extends TestActionTemp
{
	//EMPTY
}
