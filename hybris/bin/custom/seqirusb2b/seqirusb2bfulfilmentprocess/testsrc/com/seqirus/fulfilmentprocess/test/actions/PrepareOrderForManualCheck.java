/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.fulfilmentprocess.test.actions;

import com.seqirus.fulfilmentprocess.actions.order.PrepareOrderForManualCheckAction;

/**
 * Test counterpart for
 * {@link PrepareOrderForManualCheckAction}
 */
public class PrepareOrderForManualCheck extends TestActionTemp
{
	//EMPTY
}
