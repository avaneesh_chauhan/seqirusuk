/**
 *
 */
 package com.seqirus.facades.customer;



import java.util.List;

import com.seqirus.core.exceptions.SeqirusBusinessException;
import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;
import com.seqirus.core.model.SeqirusB2BCustomerRegistrationModel;
/**
 * @author nesingh

 * Facade calss for checkout activities
 *
 */
public interface CustomerRegistrationFacade 

{

	void registerCustomerRequest(SeqirusB2BCustomerRegistrationData seqCustRegisterData) throws SeqirusBusinessException;
	
	SeqirusB2BCustomerRegistrationModel getregByEmailId(String email);

}

