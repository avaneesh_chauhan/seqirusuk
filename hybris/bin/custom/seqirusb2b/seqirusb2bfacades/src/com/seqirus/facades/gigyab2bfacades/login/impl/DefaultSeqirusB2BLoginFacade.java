package com.seqirus.facades.gigyab2bfacades.login.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.Constants;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.gigya.gigyafacades.login.impl.DefaultGigyaLoginFacade;
import de.hybris.platform.gigya.gigyafacades.util.GigyaDataCenterUtils;
import de.hybris.platform.gigya.gigyaservices.api.exception.GigyaApiException;
import de.hybris.platform.gigya.gigyaservices.constants.GigyaservicesConstants;
import de.hybris.platform.gigya.gigyaservices.data.GigyaJsOnLoginInfo;
import de.hybris.platform.gigya.gigyaservices.data.GigyaUserObject;
import de.hybris.platform.gigya.gigyaservices.model.GigyaConfigModel;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.Config;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigya.socialize.GSKeyNotFoundException;
import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSResponse;
import com.seqirus.facades.customer.SeqirusCustomerRegistrationFacade;

/**
 * Seqirus B2B Login facade to handle Customer registration in the B2B Context.
 *
 * @author Avaneesh Chauhan
 */
public class DefaultSeqirusB2BLoginFacade extends DefaultGigyaLoginFacade
{
	/** The Constant JOBTITLE_OTHER. */
	private static final String JOBTITLE_OTHER = "other";

	/** The Constant WORK_TITLE. */
	private static final String WORK_TITLE = "title";

	/** The Constant WORK. */
	private static final String WORK = "work";

	/** The Constant LAST_NAME. */
	private static final String LAST_NAME = "lastName";

	/** The Constant FIRST_NAME. */
	private static final String FIRST_NAME = "firstName";

	/** The Constant UID. */
	private static final String UID = "UID";

	/** The Constant EMAIL. */
	private static final String EMAIL = "email";

	/** The Constant PROFILE. */
	private static final String PROFILE = "profile";

	/** The Constant SEQIRUS_UK. */
	private static final String SEQIRUS_UK = "SeqirusUK";

	/** The Constant SEQIRUS_UK_UNIT_NAME. */
	private static final String SEQIRUS_UK_UNIT_NAME = "Seqirus UK Unit";

	/** The Constant SEQIRUS_UK_UNIT_DESCRIPTION. */
	private static final String SEQIRUS_UK_UNIT_DESCRIPTION = "Default B2B Unit for Seqirus UK";

	/** The Constant DATA. */
	private static final String DATA = "data";

	/** The Constant JOBTITLE. */
	private static final String JOBTITLE = "jobtitle";

	/** The Constant CDC_DATA_JOBTITLE. */
	private static final String CDC_DATA_JOBTITLE = ".cdc.data.jobtitle";

	/** The Constant NUMBER. */
	private static final String NUMBER = "number";

	/** The Constant CDC_DATA_PHONE_NUMBER. */
	private static final String CDC_DATA_PHONE_NUMBER = ".cdc.data.phone.number";

	/** The Constant PHONE. */
	private static final String PHONE = "phone";

	/** The Constant CDC_DATA_PHONE. */
	private static final String CDC_DATA_PHONE = ".cdc.data.phone";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultSeqirusB2BLoginFacade.class);

	/** The b 2 b unit service. */
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;



	/** The session service. */
	@Resource
	private SessionService sessionService;

	/** The i 18 N service. */
	@Resource
	private I18NService i18NService;

	/** The seqirus customer registration facade. */
	@Resource(name = "seqirusCustomerRegistrationFacade")
	private SeqirusCustomerRegistrationFacade seqirusCustomerRegistrationFacade;

	/** The cms site service. */
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/**
	 * Sets the i 18 N service.
	 *
	 * @param i18nService
	 *           the new i 18 N service
	 */
	public void setI18NService(final I18NService i18nService)
	{
		i18NService = i18nService;
	}

	/**
	 * Sets the session service.
	 *
	 * @param sessionService
	 *           the new session service
	 */
	@Override
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * Gets the b 2 b unit service.
	 *
	 * @return the b 2 b unit service
	 */
	public B2BUnitService<B2BUnitModel, B2BCustomerModel> getB2bUnitService()
	{
		return b2bUnitService;
	}

	/**
	 * Sets the B 2 b unit service.
	 *
	 * @param b2bUnitService
	 *           the b 2 b unit service
	 */
	public void setB2bUnitService(final B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService)
	{
		this.b2bUnitService = b2bUnitService;
	}

	/**
	 * Process gigya login.
	 *
	 * @param jsInfo
	 *           the js info
	 * @param gigyaConfig
	 *           the gigya config
	 * @return true, if successful
	 */
	@Override
	public boolean processGigyaLogin(final GigyaJsOnLoginInfo jsInfo, final GigyaConfigModel gigyaConfig)
	{
		try
		{

			final Boolean isSiteGlobal = jsInfo.getIsGlobal();
			final String idToken = jsInfo.getIdToken();

			Boolean isCallVerified = false;

			if (BooleanUtils.isTrue(isSiteGlobal) && StringUtils.isBlank(idToken))
			{
				return false;
			}
			else if (StringUtils.isNotBlank(idToken))
			{
				isCallVerified = getGigyaLoginService().verifyGigyaCallWithIdToken(gigyaConfig, idToken);
			}
			else
			{
				isCallVerified = getGigyaLoginService().verifyGigyaCall(gigyaConfig, jsInfo.getUID(), jsInfo.getUIDSignature(),
						jsInfo.getSignatureTimestamp());
			}

			if (isCallVerified)
			{
				B2BCustomerModel nonWebCustomer = null;
				final UserModel gigyaUser = getGigyaLoginService().findCustomerByGigyaUid(jsInfo.getUID());
				final GigyaUserObject gigyaUserObj = getGigyaLoginService().fetchGigyaInfo(gigyaConfig, jsInfo.getUID());
				nonWebCustomer = getNonWebCustomer(jsInfo, gigyaConfig, nonWebCustomer, gigyaUserObj);
				if (gigyaUser != null)
				{
					updateUser(gigyaConfig, gigyaUser);
				}
				else if (nonWebCustomer != null)
				{
					LOG.info("Non Web Customer:" + nonWebCustomer.getUid());
					nonWebCustomer.setGyUID(jsInfo.getUID());
					nonWebCustomer.setGyDataCenter(
							GigyaDataCenterUtils.getGigyaApiDomain(gigyaUserObj.getDataCenter(), gigyaConfig.getGigyaDataCenter()));
					nonWebCustomer.setCustomerID(UUID.randomUUID().toString());
					nonWebCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
					nonWebCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
					getModelService().save(nonWebCustomer);
					updateUser(gigyaConfig, nonWebCustomer);
				}
				else
				{
					// On creation all customer data from gigya is synched
					createNewCustomer(gigyaConfig, jsInfo.getUID());
				}
			}
			return isCallVerified;

		}
		catch (final DuplicateUidException e)
		{
			try
			{
				LOG.error("Error duplicate ID found for gigyaUid=" + jsInfo.getUID(), e);
				getGigyaLoginService().notifyGigyaOfLogout(gigyaConfig, jsInfo.getUID());
			}
			catch (final GigyaApiException e1)
			{
				LOG.error(e1);
			}
			return false;
		}
		catch (final Exception e)
		{
			try
			{
				LOG.error("Error: " + e.getMessage(), e);
				getGigyaLoginService().notifyGigyaOfLogout(gigyaConfig, jsInfo.getUID());
			}
			catch (final GigyaApiException e1)
			{
				LOG.error(e1);
			}
			return false;
		}
	}

	/**
	 * Gets the non web customer.
	 *
	 * @param jsInfo
	 *           the js info
	 * @param gigyaConfig
	 *           the gigya config
	 * @param nonWebCustomer
	 *           the non web customer
	 * @param gigyaUserObj
	 *           the gigya user obj
	 * @return the non web customer
	 */
	private B2BCustomerModel getNonWebCustomer(final GigyaJsOnLoginInfo jsInfo, final GigyaConfigModel gigyaConfig,
			B2BCustomerModel nonWebCustomer, final GigyaUserObject gigyaUserObj)
	{
		try
		{
			nonWebCustomer = (B2BCustomerModel) getUserService().getUserForUID(gigyaUserObj.getEmail());
		}
		catch (final UnknownIdentifierException uex)
		{
			LOG.info("Creating new profile for: " + gigyaUserObj.getEmail());
		}
		return nonWebCustomer;
	}

	/**
	 * Creates the new customer.
	 *
	 * @param gigyaConfig
	 *           the gigya config
	 * @param uid
	 *           the uid
	 * @return the user model
	 * @throws DuplicateUidException
	 *            the duplicate uid exception
	 */
	@Override
	public UserModel createNewCustomer(final GigyaConfigModel gigyaConfig, final String uid) throws DuplicateUidException
	{
		final GigyaUserObject gigyaUserObject = getGigyaLoginService().fetchGigyaInfo(gigyaConfig, uid);
		if (gigyaConfig == null)
		{
			return null;
		}

		if (StringUtils.isEmpty(gigyaUserObject.getEmail()))
		{
			throw new GigyaApiException("Gigya User does not have an email address");
		}

		if (getGigyaLoginService().findCustomerByGigyaUid(uid) != null)
		{
			throw new DuplicateUidException("User with Gigya UID: " + uid + " already exists.");
		}

		return createB2BCustomer(gigyaConfig, uid, gigyaUserObject);

	}

	/**
	 * Creates the B 2 B customer.
	 *
	 * @param gigyaConfig
	 *           the gigya config
	 * @param uid
	 *           the uid
	 * @param gigyaUserObject
	 *           the gigya user object
	 * @return the b 2 B customer model
	 */
	private B2BCustomerModel createB2BCustomer(final GigyaConfigModel gigyaConfig, final String uid,
			final GigyaUserObject gigyaUserObject)
	{
		final B2BCustomerModel gigyaUser = getModelService().create(B2BCustomerModel.class);
		gigyaUser.setGyIsOriginGigya(true);
		gigyaUser.setName(getCustomerNameStrategy().getName(gigyaUserObject.getFirstName(), gigyaUserObject.getLastName()));
		gigyaUser.setUid(gigyaUserObject.getEmail());
		gigyaUser.setOriginalUid(gigyaUserObject.getEmail());
		gigyaUser.setGyUID(uid);
		gigyaUser.setGyApiKey(gigyaConfig.getGigyaApiKey());
		gigyaUser.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		gigyaUser.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		gigyaUser.setEmail(gigyaUserObject.getEmail());
		gigyaUser.setCustomerID(UUID.randomUUID().toString());
		gigyaUser.setGyDataCenter(
				GigyaDataCenterUtils.getGigyaApiDomain(gigyaUserObject.getDataCenter(), gigyaConfig.getGigyaDataCenter()));

		assignB2BUnitForNewProfile(gigyaUser);

		getModelService().save(gigyaUser);
		getGigyaConsentFacade().synchronizeConsents(getPreferencesObject(gigyaUserObject), gigyaUser);
		scheduleDataSyncFromCDCToCommerce(gigyaUser);
		return gigyaUser;
	}


	/**
	 * Assign B 2 B unit for new profile.
	 *
	 * @param gigyaUser
	 *           the gigya user
	 */
	private void assignB2BUnitForNewProfile(final B2BCustomerModel gigyaUser)
	{
		final B2BUnitModel b2bUnit = b2bUnitService.getUnitForUid(SEQIRUS_UK);

		if (b2bUnit != null)
		{
			gigyaUser.setDefaultB2BUnit(b2bUnit);
		}
		else
		{
			sessionService.executeInLocalView(new SessionExecutionBody()
			{
				@Override
				public void executeWithoutResult()
				{
					sessionService.setAttribute(SessionContext.USER, getUserService().getUser(Constants.USER.ADMIN_EMPLOYEE));
					final B2BUnitModel b2bUnit = getModelService().create(B2BUnitModel.class);
					b2bUnit.setUid(SEQIRUS_UK);
					b2bUnit.setDescription(SEQIRUS_UK_UNIT_DESCRIPTION);
					final Locale currentLocale = i18NService.getCurrentLocale();
					b2bUnit.setName(SEQIRUS_UK_UNIT_NAME);
					b2bUnit.setLocName(SEQIRUS_UK_UNIT_NAME, currentLocale);
					b2bUnit.setGroups(Collections.emptySet());
					b2bUnit.setActive(Boolean.TRUE);
					getModelService().save(b2bUnit);
				}
			});
			gigyaUser.setDefaultB2BUnit(b2bUnit);
		}
	}

	/**
	 * Update user.
	 *
	 * @param gigyaConfig
	 *           the gigya config
	 * @param user
	 *           the user
	 * @throws GSKeyNotFoundException
	 *            the GS key not found exception
	 */
	@Override
	public void updateUser(final GigyaConfigModel gigyaConfig, final UserModel user) throws GSKeyNotFoundException
	{
		// Update mandatory info i.e. UID, name and then update based on mappings
		final ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

		if (user instanceof B2BCustomerModel)
		{
			final B2BCustomerModel gigyaUser = (B2BCustomerModel) user;
			final GSResponse accountInfo = getAccountInfo(gigyaConfig, mapper, gigyaUser);

			updateBasicInformation(gigyaUser, accountInfo, gigyaConfig);

			getModelService().save(gigyaUser);

			getGigyaConsentFacade().synchronizeConsents(getPreferenceData(accountInfo), gigyaUser);
			scheduleDataSyncFromCDCToCommerce(gigyaUser);
			//seqirusCustomerRegistrationFacade.sendUpdateRegistrationEmail();
		}
	}

	/**
	 * Gets the account info.
	 *
	 * @param gigyaConfig
	 *           the gigya config
	 * @param mapper
	 *           the mapper
	 * @param gigyaUser
	 *           the gigya user
	 * @return the account info
	 */
	@Override
	protected GSResponse getAccountInfo(final GigyaConfigModel gigyaConfig, final ObjectMapper mapper,
			final CustomerModel gigyaUser)
	{
		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(UID, gigyaUser.getGyUID());
		params.put("include", "loginIDs,emails,profile,data,preferences,groups");
		params.put("extraProfileFields", WORK);

		final GSObject gigyaParams = convertMapToGsObject(mapper, params);

		final GSResponse gsResponse = getGigyaService().callRawGigyaApiWithConfigAndObject("accounts.getAccountInfo", gigyaParams,
				gigyaConfig, GigyaservicesConstants.MAX_RETRIES, GigyaservicesConstants.TRY_NUM);
		return gsResponse;
	}

	/**
	 * Update basic information.
	 *
	 * @param gigyaUser
	 *           the gigya user
	 * @param accountInfo
	 *           the account info
	 * @param gigyaConfig
	 *           the gigya config
	 * @throws GSKeyNotFoundException
	 *            the GS key not found exception
	 */
	protected void updateBasicInformation(final B2BCustomerModel gigyaUser, final GSResponse accountInfo,
			final GigyaConfigModel gigyaConfig) throws GSKeyNotFoundException
	{
		if (accountInfo.hasData() && accountInfo.getData().get(PROFILE) != null)
		{
			final GSObject profile = (GSObject) accountInfo.getData().get(PROFILE);
			final String emailId = profile.getString(EMAIL);
			gigyaUser.setGyUID(accountInfo.getData().getString(UID));
			gigyaUser.setGyApiKey(gigyaConfig.getGigyaApiKey());
			gigyaUser.setFirstName(profile.getString(FIRST_NAME));
			gigyaUser.setLastname(profile.getString(LAST_NAME));

			if (profile.containsKey(WORK))
			{
				setJobTitle(gigyaUser, accountInfo, profile);
			}
			gigyaUser.setPhone(getPhoneNumber(accountInfo));
			gigyaUser.setName(getCustomerNameStrategy().getName(profile.getString(FIRST_NAME), profile.getString(LAST_NAME)));

			// Checks if email ID is updated in the gigya profile, if yes that
			// needs to be
			// updated in commerce as well
			if (!StringUtils.equals(gigyaUser.getUid(), emailId))
			{
				gigyaUser.setUid(emailId);
				sessionService.setAttribute("emailUpdated", true);
			}
		}
		gigyaUser.setGyIsOriginGigya(true);
	}


	/**
	 * Sets the job title.
	 *
	 * @param gigyaUser
	 *           the gigya user
	 * @param accountInfo
	 *           the account info
	 * @param profile
	 *           the profile
	 * @throws GSKeyNotFoundException
	 *            the GS key not found exception
	 */
	private void setJobTitle(final B2BCustomerModel gigyaUser, final GSResponse accountInfo, final GSObject profile)
			throws GSKeyNotFoundException
	{
		final GSObject work = profile.getObject(WORK);

		String jobTitle = null;
		if (work != null)
		{
			jobTitle = work.getString(WORK_TITLE);
			LOG.info("Job Title value is:" + jobTitle);
			if (StringUtils.isNotEmpty(jobTitle) && jobTitle.equalsIgnoreCase(JOBTITLE_OTHER))
			{
				jobTitle = getJobTitleOther(accountInfo);
				LOG.info("Job Title other value is:" + jobTitle);
			}
			gigyaUser.setJobTitle(jobTitle);
		}
	}


	/**
	 * Gets the phone number.
	 *
	 * @param accountInfo
	 *           the account info
	 * @return the phone number
	 * @throws GSKeyNotFoundException
	 *            the GS key not found exception
	 */
	private String getPhoneNumber(final GSResponse accountInfo) throws GSKeyNotFoundException
	{
		if (accountInfo.hasData() && accountInfo.getData().get(DATA) != null)
		{
			final GSObject data = (GSObject) accountInfo.getData().get(DATA);
			if (data.containsKey(getCustomField(CDC_DATA_PHONE, PHONE)))
			{
				final GSObject phone = data.getObject(getCustomField(CDC_DATA_PHONE, PHONE));
				return phone.getString(getCustomField(CDC_DATA_PHONE_NUMBER, NUMBER));
			}
		}
		return null;
	}

	/**
	 * Gets the job title other.
	 *
	 * @param accountInfo
	 *           the account info
	 * @return the job title other
	 * @throws GSKeyNotFoundException
	 *            the GS key not found exception
	 */
	private String getJobTitleOther(final GSResponse accountInfo) throws GSKeyNotFoundException
	{
		if (accountInfo.hasData() && accountInfo.getData().get(DATA) != null)
		{
			final GSObject data = (GSObject) accountInfo.getData().get(DATA);
			return data.getString(getCustomField(CDC_DATA_JOBTITLE, JOBTITLE));
		}
		return null;
	}

	/**
	 * Gets the custom field.
	 *
	 * @param key
	 *           the key
	 * @param defaultValue
	 *           the default value
	 * @return the custom field
	 */
	private String getCustomField(final String key, final String defaultValue)
	{
		final String currentSite = cmsSiteService.getCurrentSite().getUid();
		return Config.getString(currentSite + key, defaultValue);
	}
}
