/**
 *
 */
package com.seqirus.facades.invoice.data;

import java.util.List;

/**
 * @author 700196
 *
 */
public class SeqirusInvoiceDetailsResponseData
{
	private String invoiceNumber;
	private String paymentTermDescription;
	private String salesOrderNumber;
	private String invoiceDate;
	private float amountDue;
	private String paymentDueDay;
	private String paymentDueMnthYr;
	private String deliveryNumber;
	private String poNumber;
	private String poDate;
	private float subTotal;
	private float total;
	private float vat;
	public SeqirusInvoiceDetailsSoldToResponseData soldTo;
	public SeqirusInvoiceDetailsShipToResponseData shipTo;
	public List<SeqirusInvoiceDetailsListResponse> lineItems;
	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}
	/**
	 * @param invoiceNumber the invoiceNumber to set
	 */
	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}
	/**
	 * @return the paymentTermDescription
	 */
	public String getPaymentTermDescription()
	{
		return paymentTermDescription;
	}
	/**
	 * @param paymentTermDescription the paymentTermDescription to set
	 */
	public void setPaymentTermDescription(final String paymentTermDescription)
	{
		this.paymentTermDescription = paymentTermDescription;
	}
	/**
	 * @return the salesOrderNumber
	 */
	public String getSalesOrderNumber()
	{
		return salesOrderNumber;
	}
	/**
	 * @param salesOrderNumber the salesOrderNumber to set
	 */
	public void setSalesOrderNumber(final String salesOrderNumber)
	{
		this.salesOrderNumber = salesOrderNumber;
	}
	/**
	 * @return the invoiceDate
	 */
	public String getInvoiceDate()
	{
		return invoiceDate;
	}
	/**
	 * @param invoiceDate the invoiceDate to set
	 */
	public void setInvoiceDate(final String invoiceDate)
	{
		this.invoiceDate = invoiceDate;
	}

	/**
	 * @return the amountDue
	 */
	public float getAmountDue()
	{
		return amountDue;
	}
	/**
	 * @param amountDue the amountDue to set
	 */
	public void setAmountDue(final float amountDue)
	{
		this.amountDue = amountDue;
	}

	/**
	 * @return the paymentDueDay
	 */
	public String getPaymentDueDay()
	{
		return paymentDueDay;
	}
	/**
	 * @param paymentDueDay the paymentDueDay to set
	 */
	public void setPaymentDueDay(final String paymentDueDay)
	{
		this.paymentDueDay = paymentDueDay;
	}
	/**
	 * @return the paymentDueMnthYr
	 */
	public String getPaymentDueMnthYr()
	{
		return paymentDueMnthYr;
	}
	/**
	 * @param paymentDueMnthYr the paymentDueMnthYr to set
	 */
	public void setPaymentDueMnthYr(final String paymentDueMnthYr)
	{
		this.paymentDueMnthYr = paymentDueMnthYr;
	}
	/**
	 * @return the deliveryNumber
	 */
	public String getDeliveryNumber()
	{
		return deliveryNumber;
	}
	/**
	 * @param deliveryNumber the deliveryNumber to set
	 */
	public void setDeliveryNumber(final String deliveryNumber)
	{
		this.deliveryNumber = deliveryNumber;
	}
	/**
	 * @return the poNumber
	 */
	public String getPoNumber()
	{
		return poNumber;
	}
	/**
	 * @param poNumber the poNumber to set
	 */
	public void setPoNumber(final String poNumber)
	{
		this.poNumber = poNumber;
	}
	/**
	 * @return the poDate
	 */
	public String getPoDate()
	{
		return poDate;
	}
	/**
	 * @param poDate the poDate to set
	 */
	public void setPoDate(final String poDate)
	{
		this.poDate = poDate;
	}

	/**
	 * @return the soldTo
	 */
	public SeqirusInvoiceDetailsSoldToResponseData getSoldTo()
	{
		return soldTo;
	}
	/**
	 * @param soldTo the soldTo to set
	 */
	public void setSoldTo(final SeqirusInvoiceDetailsSoldToResponseData soldTo)
	{
		this.soldTo = soldTo;
	}
	/**
	 * @return the shipTo
	 */
	public SeqirusInvoiceDetailsShipToResponseData getShipTo()
	{
		return shipTo;
	}
	/**
	 * @param shipTo the shipTo to set
	 */
	public void setShipTo(final SeqirusInvoiceDetailsShipToResponseData shipTo)
	{
		this.shipTo = shipTo;
	}
	/**
	 * @return the lineItems
	 */
	public List<SeqirusInvoiceDetailsListResponse> getLineItems()
	{
		return lineItems;
	}
	/**
	 * @param lineItems the lineItems to set
	 */
	public void setLineItems(final List<SeqirusInvoiceDetailsListResponse> lineItems)
	{
		this.lineItems = lineItems;
	}

	/**
	 * @return the subTotal
	 */
	public float getSubTotal()
	{
		return subTotal;
	}

	/**
	 * @param subTotal
	 *           the subTotal to set
	 */
	public void setSubTotal(final float subTotal)
	{
		this.subTotal = subTotal;
	}

	/**
	 * @return the total
	 */
	public float getTotal()
	{
		return total;
	}

	/**
	 * @param total
	 *           the total to set
	 */
	public void setTotal(final float total)
	{
		this.total = total;
	}

	/**
	 * @return the vat
	 */
	public float getVat()
	{
		return vat;
	}

	/**
	 * @param vat
	 *           the vat to set
	 */
	public void setVat(final float vat)
	{
		this.vat = vat;
	}



}
