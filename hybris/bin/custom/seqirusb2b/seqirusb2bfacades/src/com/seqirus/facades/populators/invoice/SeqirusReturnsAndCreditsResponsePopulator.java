/**
 *
 */
package com.seqirus.facades.populators.invoice;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.fest.util.Collections;

import com.seqirus.core.dataObjects.ReturnsAndCreditsListResponse;
import com.seqirus.core.dataObjects.ReturnsAndCreditsResponse;
import com.seqirus.facades.constants.Seqirusb2bFacadesConstants;
import com.seqirus.facades.invoice.data.SeqirusReturnsAndCreditsResponse;
import com.seqirus.facades.invoice.data.SeqirusReturnsAndCreditsResponseData;
import com.seqirus.facades.invoice.utils.SeqirusInvoiceUtils;



/**
 * The Class SeqirusReturnsAndCreditsResponsePopulator.
 *
 * @author Avaneesh Chauhan
 */
public class SeqirusReturnsAndCreditsResponsePopulator
		implements Populator<ReturnsAndCreditsResponse, SeqirusReturnsAndCreditsResponseData>
{

	/** The parse date. */
	private final SimpleDateFormat parseDate = new SimpleDateFormat("yyyy/MM/dd");

	/** The format date. */
	private final SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(SeqirusReturnsAndCreditsResponsePopulator.class);

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final ReturnsAndCreditsResponse source, final SeqirusReturnsAndCreditsResponseData target)
			throws ConversionException
	{
		final List<SeqirusReturnsAndCreditsResponse> creditList = new ArrayList<>();
		double availableCredit = 0.00;
		final DecimalFormat decimalFormat = new DecimalFormat("0.00");
		SeqirusReturnsAndCreditsResponse returnsAndCreditsResponse;
		if (!Collections.isEmpty(source.getInvoices()))
		{
			for (final ReturnsAndCreditsListResponse response : source.getInvoices())
			{
				returnsAndCreditsResponse = new SeqirusReturnsAndCreditsResponse();
				setReturnsAndCreditResponse(response, returnsAndCreditsResponse,decimalFormat);
				availableCredit = addAvailableCredit(returnsAndCreditsResponse, availableCredit);
				creditList.add(returnsAndCreditsResponse);
			}
			target.setCreditList(creditList);
			LOG.info("Total Available credit is: " + availableCredit);
			target.setTotalAvailableCredit(decimalFormat.format(availableCredit));
		}
		else
		{
			returnsAndCreditsResponse = new SeqirusReturnsAndCreditsResponse();
			LOG.info("Credits not available, setting no credits response. Total Available credit: " + availableCredit);
			creditList.add(SeqirusInvoiceUtils.getNoCreditReponse(returnsAndCreditsResponse));
			target.setCreditList(creditList);
			target.setTotalAvailableCredit(decimalFormat.format(availableCredit));
		}
	}


	/**
	 * Sets the no credit reponse.
	 *
	 * @param returnsAndCreditsResponse
	 *           the new no credit reponse
	 */
	private void setNoCreditReponse(final SeqirusReturnsAndCreditsResponse returnsAndCreditsResponse)
	{
		returnsAndCreditsResponse.setInvoiceNumber(Seqirusb2bFacadesConstants.NA_STATUS);
		returnsAndCreditsResponse.setInvoiceDate(Seqirusb2bFacadesConstants.NA_STATUS);
		returnsAndCreditsResponse.setStatus(Seqirusb2bFacadesConstants.NA_STATUS);
	}


	/**
	 * Adds the available credit.
	 *
	 * @param returnsAndCreditsData
	 *           the returns and credits data
	 * @param availableCredit
	 *           the available credit
	 * @return the double
	 */
	private double addAvailableCredit(final SeqirusReturnsAndCreditsResponse returnsAndCreditsData, double availableCredit)
	{
		if (returnsAndCreditsData.getStatus().equalsIgnoreCase("Available"))
		{
			availableCredit = availableCredit + returnsAndCreditsData.getAmoutWithTax();
		}
		return availableCredit;
	}


	/**
	 * Sets the returns and credit response.
	 *
	 * @param response
	 *           the response
	 * @param returnsAndCreditsData
	 *           the returns and credits data
	 * @param decimalFormat
	 *           the decimal format
	 */
	private void setReturnsAndCreditResponse(final ReturnsAndCreditsListResponse response,
			final SeqirusReturnsAndCreditsResponse returnsAndCreditsData, final DecimalFormat decimalFormat)
	{
		if (StringUtils.isNotBlank(response.getInvoiceNumber()))
		{
			returnsAndCreditsData.setInvoiceNumber(response.getInvoiceNumber());
		}
		if (StringUtils.isNotBlank(response.getInvoiceDate()))
		{
			try
			{
				returnsAndCreditsData.setInvoiceDate(formatDate.format(parseDate.parse(response.getInvoiceDate())));
			}
			catch (final ParseException e)
			{
				LOG.error("Exception while parsing the date", e);
			}
		}
		if (StringUtils.isNotBlank(response.getCurrency()))
		{
			final Currency currency = Currency.getInstance(response.getCurrency());
			returnsAndCreditsData.setCurrency(currency.getSymbol());
		}
		returnsAndCreditsData.setAmoutWithTax(Double.parseDouble(decimalFormat.format(response.getAmoutWithTax())));
		returnsAndCreditsData.setStatus(response.getStatus());
	}

}
