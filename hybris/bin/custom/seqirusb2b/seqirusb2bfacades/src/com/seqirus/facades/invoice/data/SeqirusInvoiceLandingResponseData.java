/**
 *
 */
package com.seqirus.facades.invoice.data;

import java.util.List;

/**
 * @author 700196
 *
 */
public class SeqirusInvoiceLandingResponseData
{
	private double openStatusInvoiceAmount;
	private double paidStatusInvoiceAmount;
	private int openInvoiceCount;
	private int paidInvoiceCount;
	public List<SeqirusInvoiceLandingListResponse> invoices;


	/**
	 * @return the openStatusInvoiceAmount
	 */
	public double getOpenStatusInvoiceAmount()
	{
		return openStatusInvoiceAmount;
	}

	/**
	 * @param openStatusInvoiceAmount the openStatusInvoiceAmount to set
	 */
	public void setOpenStatusInvoiceAmount(final double openStatusInvoiceAmount)
	{
		this.openStatusInvoiceAmount = openStatusInvoiceAmount;
	}

	/**
	 * @return the paidStatusInvoiceAmount
	 */
	public double getPaidStatusInvoiceAmount()
	{
		return paidStatusInvoiceAmount;
	}

	/**
	 * @param paidStatusInvoiceAmount the paidStatusInvoiceAmount to set
	 */
	public void setPaidStatusInvoiceAmount(final double paidStatusInvoiceAmount)
	{
		this.paidStatusInvoiceAmount = paidStatusInvoiceAmount;
	}


	/**
	 * @return the openInvoiceCount
	 */
	public int getOpenInvoiceCount()
	{
		return openInvoiceCount;
	}

	/**
	 * @param openInvoiceCount
	 *           the openInvoiceCount to set
	 */
	public void setOpenInvoiceCount(final int openInvoiceCount)
	{
		this.openInvoiceCount = openInvoiceCount;
	}

	/**
	 * @return the paidInvoiceCount
	 */
	public int getPaidInvoiceCount()
	{
		return paidInvoiceCount;
	}

	/**
	 * @param paidInvoiceCount
	 *           the paidInvoiceCount to set
	 */
	public void setPaidInvoiceCount(final int paidInvoiceCount)
	{
		this.paidInvoiceCount = paidInvoiceCount;
	}

	/**
	 * @return the invoices
	 */
	public List<SeqirusInvoiceLandingListResponse> getInvoices()
	{
		return invoices;
	}

	/**
	 * @param invoices the invoices to set
	 */
	public void setInvoices(final List<SeqirusInvoiceLandingListResponse> invoices)
	{
		this.invoices = invoices;
	}


}
