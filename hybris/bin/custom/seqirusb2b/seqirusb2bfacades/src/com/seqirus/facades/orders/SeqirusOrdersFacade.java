/**
 *
 */
package com.seqirus.facades.orders;

import java.util.List;

import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;
import com.seqirus.core.dataObjects.OrderSummary;

/**
 * @author 614269
 *
 */
public interface SeqirusOrdersFacade
{
	/**
	 * @param customerId
	 * @param season
	 * @return
	 */
	List<OrderSummary> getOrders(String customerId, String season);

	SeqirusB2BCustomerRegistrationData getPartnerDetails(String id, String partnerType);

	SeqirusB2BCustomerRegistrationData getCustomerDetailsFromSAP(String soldToId, String zipCode);

}
