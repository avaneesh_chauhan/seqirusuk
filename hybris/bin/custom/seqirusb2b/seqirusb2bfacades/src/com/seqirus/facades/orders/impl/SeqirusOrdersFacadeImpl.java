/**
 *
 */
package com.seqirus.facades.orders.impl;

import com.seqirus.facades.orders.SeqirusOrdersFacade;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.seqirus.core.dataObjects.CustomerSummary;
import com.seqirus.core.dataObjects.PartnerSummary;
import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;
import com.seqirus.core.dataObjects.OrderSummary;
import com.seqirus.core.orders.service.SeqirusOrdersService;


/**
 * @author 614269
 *
 */
public class SeqirusOrdersFacadeImpl implements SeqirusOrdersFacade
{
	private static final Logger LOGGER = Logger.getLogger(SeqirusOrdersFacadeImpl.class);

	@Resource(name = "seqirusOrdersService")
	private SeqirusOrdersService seqirusOrdersService;

	@Autowired
	SessionService sessionService;

	@Override
	public List<OrderSummary> getOrders(final String customerId, final String season)
	{
		return seqirusOrdersService.getOrders(customerId, season);
	}

	protected Populator<Object, SeqirusB2BCustomerRegistrationData> customerRegisterPopulator;

	/**
	 * @return the customerRegisterPopulator
	 */
	public Populator<Object, SeqirusB2BCustomerRegistrationData> getCustomerRegisterPopulator()
	{
		return customerRegisterPopulator;
	}

	/**
	 * @param customerRegisterPopulator
	 *           the customerRegisterPopulator to set
	 */
	public void setCustomerRegisterPopulator(final Populator<Object, SeqirusB2BCustomerRegistrationData> customerRegisterPopulator)
	{
		this.customerRegisterPopulator = customerRegisterPopulator;
	}

	@Override
	public SeqirusB2BCustomerRegistrationData getPartnerDetails(final String id, final String partnerType)
	{
		// XXX Auto-generated method stub

		final SeqirusB2BCustomerRegistrationData seqCustRegData = new SeqirusB2BCustomerRegistrationData();

		if (StringUtils.isBlank(partnerType)) {

		final PartnerSummary partnerSummary  = seqirusOrdersService.fetchPartnerSummary(id);

		if (null != partnerSummary && StringUtils.isBlank(partnerType))
		{
			 getCustomerRegisterPopulator().populate(partnerSummary, seqCustRegData);
		}

		}
		else
		{
			seqCustRegData.setPartnerType(partnerType);
			//getCustomerRegisterPopulator().populate(new Object(), seqCustRegData);
			getCustomerRegisterPopulator().populate(seqirusOrdersService.fetchPartnerDetails(id, partnerType), seqCustRegData);

		}

		return seqCustRegData;
	}

	@Override
	public SeqirusB2BCustomerRegistrationData getCustomerDetailsFromSAP(final String soldToId, final String zipCode)
	{
		final SeqirusB2BCustomerRegistrationData seqCustRegData = new SeqirusB2BCustomerRegistrationData();

		final CustomerSummary customerProfileData = seqirusOrdersService.fetchCustomerDetail(soldToId, zipCode);
		if (null != customerProfileData)
		{
			getCustomerRegisterPopulator().populate(customerProfileData, seqCustRegData);
			sessionService.setAttribute("account", seqCustRegData);
		}

		return seqCustRegData;
	}


}
