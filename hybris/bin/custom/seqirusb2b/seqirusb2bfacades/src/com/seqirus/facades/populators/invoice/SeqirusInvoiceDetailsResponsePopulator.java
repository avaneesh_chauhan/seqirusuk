/**
 *
 */
package com.seqirus.facades.populators.invoice;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.seqirus.core.dataObjects.InvoiceDetailsLineItemsResponse;
import com.seqirus.core.dataObjects.InvoiceDetailsResponse;
import com.seqirus.core.orders.service.product.SeqirusProductService;
import com.seqirus.facades.constants.Seqirusb2bFacadesConstants;
import com.seqirus.facades.invoice.data.SeqirusInvoiceDetailsListResponse;
import com.seqirus.facades.invoice.data.SeqirusInvoiceDetailsResponseData;
import com.seqirus.facades.invoice.data.SeqirusInvoiceDetailsShipToResponseData;
import com.seqirus.facades.invoice.data.SeqirusInvoiceDetailsSoldToResponseData;

/**
 * @author 700196
 *
 */
public class SeqirusInvoiceDetailsResponsePopulator implements Populator<InvoiceDetailsResponse, SeqirusInvoiceDetailsResponseData>
{
	private final SimpleDateFormat parseDate = new SimpleDateFormat(Seqirusb2bFacadesConstants.YYYYMMDD_FORMAT);
	private final SimpleDateFormat formatDate = new SimpleDateFormat(Seqirusb2bFacadesConstants.DDMMYYYY_FORMAT);

	@Resource
	private SeqirusProductService seqirusProductService;

	@Override
	public void populate(final InvoiceDetailsResponse source, final SeqirusInvoiceDetailsResponseData target) throws ConversionException
	{
		if (StringUtils.isNotBlank(source.getInvoiceNumber()))
		{
			target.setInvoiceNumber(source.getInvoiceNumber());
		}
		if (StringUtils.isNotBlank(source.getPaymentTermDescription()))
		{
			target.setPaymentTermDescription(source.getPaymentTermDescription());
		}
		if (StringUtils.isNotBlank(source.getPaymentDueDate()))
		{
			target.setPaymentDueDay(formayPaymentDueDate(source.getPaymentDueDate(),Seqirusb2bFacadesConstants.DAY_STRING));
			target.setPaymentDueMnthYr(formayPaymentDueDate(source.getPaymentDueDate(),""));
		}
		try
		{
			if (StringUtils.isNotBlank(source.getInvoiceDate()))
			{
   			target.setInvoiceDate(formatDate.format(parseDate.parse(source.getInvoiceDate())));
   		}
			if (StringUtils.isNotBlank(source.getPoDate()))
			{
   			target.setPoDate(formatDate.format(parseDate.parse(source.getPoDate())));
   		}
		}
		catch (final ParseException e)
		{
			e.printStackTrace();
		}
		if(source.getAmountWithTax() > 0){
			target.setAmountDue(source.getAmountWithTax());
		}
		if (StringUtils.isNotBlank(source.getSalesOrderNumber()))
		{
			target.setSalesOrderNumber(source.getSalesOrderNumber());
		}
		if (StringUtils.isNotBlank(source.getDeliveryNumber()))
		{
			target.setDeliveryNumber(source.getDeliveryNumber());
		}
		if (StringUtils.isNotBlank(source.getPoNumber()))
		{
			target.setPoNumber(source.getPoNumber());
		}
		mapInvoiceShipToDetails(source, target);
		mapInvoiceSoldToDetails(source, target);
		mapInvoiceDetailsLineItems(source, target);
	}


	/**
	 * @param lineItems
	 * @param lineItemsResponse
	 */
	private void mapInvoiceDetailsLineItems(final InvoiceDetailsResponse source, final SeqirusInvoiceDetailsResponseData target)
	{
		float subTotal = 0;
		final float totalVat = 0;
		float totalCost = 0;
		final List<SeqirusInvoiceDetailsListResponse> lineItems = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(source.getLineItems()))
		{
			for (final InvoiceDetailsLineItemsResponse lineItemsResponse : source.getLineItems())
			{
				if (StringUtils.isNotBlank(lineItemsResponse.getQuantity()) && !lineItemsResponse.getQuantity().equals("0"))
				{
					final SeqirusInvoiceDetailsListResponse detailsListResponse = new SeqirusInvoiceDetailsListResponse();
					mapSubLineItems(lineItemsResponse, detailsListResponse);
					if (lineItemsResponse.getTotalCost() > 0)
					{
						detailsListResponse.setTotalCost(lineItemsResponse.getTotalCost());
						subTotal = subTotal + lineItemsResponse.getTotalCost();
						//totalVat = totalVat + lineItemsResponse.getTax();
					}
					lineItems.add(detailsListResponse);
			}
			}
			totalCost = subTotal + totalVat;
			target.setSubTotal(subTotal);
			//target.setVat(totalVat);
			target.setTotal(totalCost);
			target.setLineItems(lineItems);
		}
	}

	/**
	 * @param lineItemsResponse
	 * @param detailsListResponse
	 */
	private void mapSubLineItems(final InvoiceDetailsLineItemsResponse lineItemsResponse,
			final SeqirusInvoiceDetailsListResponse detailsListResponse)
	{
		if (StringUtils.isNotBlank(lineItemsResponse.getBatchNo()))
		{
			detailsListResponse.setBatchNumber(lineItemsResponse.getBatchNo());
		}
		if (lineItemsResponse.getNetAmount() > 0)
		{
			detailsListResponse.setNetAmount(lineItemsResponse.getNetAmount());
		}
		if (lineItemsResponse.getUnitPrice() > 0)
		{
			detailsListResponse.setUnitPrice(lineItemsResponse.getUnitPrice());
		}
		if (lineItemsResponse.getTax() > 0)
		{
			detailsListResponse.setLineItemTax(lineItemsResponse.getTax());
		}
		if (StringUtils.isNotBlank(lineItemsResponse.getQuantity()))
		{
			detailsListResponse.setDoses(lineItemsResponse.getQuantity());
		}
		final VariantProductModel productModel = (VariantProductModel) seqirusProductService
				.getProductDataForCode(StringUtils.stripStart(lineItemsResponse.getMaterialNumber(), "0"));
		if (null != productModel && StringUtils.isNotBlank(productModel.getName()))
		{
			detailsListResponse.setProductName(productModel.getName());
		}
		if (StringUtils.isNotBlank(lineItemsResponse.getMaterialNumber()))
		{
			detailsListResponse.setMaterialNumber(StringUtils.stripStart(lineItemsResponse.getMaterialNumber(), "0"));
		}

	}

	/**
	 * @param source
	 * @param soldToResponseData
	 */
	private void mapInvoiceSoldToDetails(final InvoiceDetailsResponse source,
			final SeqirusInvoiceDetailsResponseData target)
	{
		final SeqirusInvoiceDetailsSoldToResponseData soldToResponseData=new SeqirusInvoiceDetailsSoldToResponseData();
		if (null != source.getSoldTo())
		{
			if (StringUtils.isNotBlank(source.getSoldTo().getId()))
			{
				soldToResponseData.setSoldToId(source.getSoldTo().getId());
   		}
			if (StringUtils.isNotBlank(source.getSoldTo().getName()))
			{
				soldToResponseData.setSoldToName(source.getSoldTo().getName());
   		}
			if (StringUtils.isNotBlank(source.getSoldTo().getStreet())
					|| StringUtils.isNotBlank(source.getSoldTo().getAdditionalStreet()))
			{
				soldToResponseData.setSoldToStreet(
						populateInvoiceAddress(source.getSoldTo().getStreet(), source.getSoldTo().getAdditionalStreet()));
   		}
			if (StringUtils.isNotBlank(source.getSoldTo().getCity()) || StringUtils.isNotBlank(source.getSoldTo().getCountry()))
			{
				soldToResponseData
						.setSoldToCity(populateInvoiceAddress(source.getSoldTo().getCity(), source.getSoldTo().getCountry()));
   		}
   		target.setSoldTo(soldToResponseData);
		}
	}

	/**
	 * @param source
	 * @param shipToResponseData
	 */
	private void mapInvoiceShipToDetails(final InvoiceDetailsResponse source,
			final SeqirusInvoiceDetailsResponseData target)
	{
		final SeqirusInvoiceDetailsShipToResponseData shipToResponseData= new SeqirusInvoiceDetailsShipToResponseData();
		if(null!=source.getShipTo()){
			if (StringUtils.isNotBlank(source.getShipTo().getId()))
			{
   			shipToResponseData.setShipToId(source.getShipTo().getId());
   		}
			if (StringUtils.isNotBlank(source.getShipTo().getName()))
			{
   			shipToResponseData.setShipToName(source.getShipTo().getName());
   		}
			if (StringUtils.isNotBlank(source.getShipTo().getStreet())
					|| StringUtils.isNotBlank(source.getShipTo().getAdditionalStreet()))
			{
   			shipToResponseData.setShipToStreet(populateInvoiceAddress(source.getShipTo().getStreet(),source.getShipTo().getAdditionalStreet()));
   		}
			if (StringUtils.isNotBlank(source.getShipTo().getCity()) || StringUtils.isNotBlank(source.getShipTo().getCountry()))
			{
   			shipToResponseData.setShipToCity(populateInvoiceAddress(source.getShipTo().getCity(),source.getShipTo().getCountry()));
   		}
   		target.setShipTo(shipToResponseData);
		}
	}

	/**
	 * @param var1
	 * @param var2
	 * @return String
	 */
	private String populateInvoiceAddress(final String var1, final String var2){
		final StringBuilder billToStreet=new StringBuilder();
		return removePrefixAndSufix(billToStreet.append(var1).append(Seqirusb2bFacadesConstants.COMMA_SIGN)
				.append(Seqirusb2bFacadesConstants.EMPTY_SPACE).append(var2).toString(), Seqirusb2bFacadesConstants.COMMA_SIGN);
	}

	/**
	 * @param address
	 * @param delimeter
	 * @return String
	 */
	private String removePrefixAndSufix(final String address,final String delimeter){
		String fmtAddress=address;
		if (address != null && delimeter != null && address.startsWith(delimeter))
		{
			fmtAddress=address.substring(delimeter.length()).trim();
		}
		if (fmtAddress != null && delimeter != null && fmtAddress.endsWith(delimeter))
		{
			fmtAddress = fmtAddress.substring(0, fmtAddress.length() - delimeter.length());
		}
		return fmtAddress.trim();
	}

	/**
	 * @param paymentDueDate
	 * @return String
	 */
	private String formayPaymentDueDate(final String paymentDueDate,final String formatType)
	{
		String formattedDate=null;
		try{
			final Date dueDate = parseDate.parse(paymentDueDate);
			final Calendar cals = Calendar.getInstance();
			cals.setTime(dueDate);
			if (formatType.equals(Seqirusb2bFacadesConstants.DAY_STRING))
			{
				final String dayNumberSuffix = getDayNumberSuffix(cals.get(Calendar.DAY_OF_MONTH));
				final DateFormat dayFormat = new SimpleDateFormat(Seqirusb2bFacadesConstants.DAY_FORMAT + dayNumberSuffix + Seqirusb2bFacadesConstants.APOSTROPHE_STRING);
				formattedDate= dayFormat.format(cals.getTime());
			}
			else
			{
				final DateFormat mthYearFormat = new SimpleDateFormat(Seqirusb2bFacadesConstants.MNTHYR_FORMAT);
				formattedDate= mthYearFormat.format(cals.getTime());
			}
		}
		catch (final ParseException e)
		{
			e.printStackTrace();
		}
		return formattedDate;
	}

	private String getDayNumberSuffix(final int day) {
	    if (day >= 11 && day <= 13) {
	        return Seqirusb2bFacadesConstants.DAYNUMBERSUFFIX_TH;
	    }
	    switch (day % 10) {
	    case 1:
	        return Seqirusb2bFacadesConstants.DAYNUMBERSUFFIX_ST;
	    case 2:
	        return Seqirusb2bFacadesConstants.DAYNUMBERSUFFIX_ND;
	    case 3:
	        return Seqirusb2bFacadesConstants.DAYNUMBERSUFFIX_RD;
	    default:
	        return Seqirusb2bFacadesConstants.DAYNUMBERSUFFIX_TH;
	    }
	}
}
