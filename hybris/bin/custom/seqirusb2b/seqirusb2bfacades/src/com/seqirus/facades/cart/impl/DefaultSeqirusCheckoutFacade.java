package com.seqirus.facades.cart.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.seqirus.facades.cart.SeqirusCheckoutFacade;
import com.seqirus.core.constants.Seqirusb2bCoreConstants;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import com.seqirus.core.model.SeqirusCartEntryModel;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.servicelayer.model.ModelService;
/**
 * @author nesingh DefaultSeqirusCheckoutFacade deals with checkout activies
 *
 */
public class DefaultSeqirusCheckoutFacade extends DefaultCheckoutFacade implements SeqirusCheckoutFacade
{
	protected static final Logger LOG = Logger.getLogger(DefaultSeqirusCheckoutFacade.class);
	private static final boolean IS_DEBUG_ENABLED = LOG.isDebugEnabled();
	
	/**
	 * @return the commerceCartService
	 */
	public CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	/**
	 * @param commerceCartService
	 *           the commerceCartService to set
	 */
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "cartService")
	private CartService cartService;
	
	@Resource(name = "commerceCartService")
	private CommerceCartService commerceCartService;
	
	@Resource(name = "modelService")
	private ModelService modelService;
	
	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "defaultAddressReverseConverter")
	private Converter<AddressData, AddressModel> defaultAddressReverseConverter;
	private Converter<AddressModel, AddressData> addressConverter;


	/**
	 * @return the addressConverter
	 */
	@Override
	public Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	/**
	 * @param addressConverter
	 *           the addressConverter to set
	 */
	@Override
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}


	/**
	 * @return the defaultAddressReverseConverter
	 */
	public Converter<AddressData, AddressModel> getDefaultAddressReverseConverter()
	{
		return defaultAddressReverseConverter;
	}

	/**
	 * @param defaultAddressReverseConverter
	 *           the defaultAddressReverseConverter to set
	 */
	public void setDefaultAddressReverseConverter(
			final Converter<AddressData, AddressModel> defaultAddressReverseConverter)
	{
		this.defaultAddressReverseConverter = defaultAddressReverseConverter;
	}


	/**
	 * Method to populate all addresses for Sold-To account linked to user.
	 *
	 * @return
	 */
	@Override
	public List<AddressData> populateSoldToLinkedAddresses()
	{
		if (IS_DEBUG_ENABLED)
		{
			LOG.debug("Logged in:::: populateSoldToDeliveryAddress()");
		}
		final List<AddressData> addressData = new ArrayList<AddressData>();

		final UserModel checkoutCustomer = userService.getCurrentUser();
			setupAddressData(addressData, checkoutCustomer);

		if (CollectionUtils.isEmpty(addressData))
		{
			LOG.error("Error Case: No addresses attached to the User.");
		}

		return addressData;
	}

	/**
	 * @param addressData
	 * @param checkoutCustomer
	 */
	private void setupAddressData(final List<AddressData> addressData, final UserModel checkoutCustomer)
	{

		if (checkoutCustomer instanceof B2BCustomerModel)
		{
			final B2BCustomerModel b2bcustomer = (B2BCustomerModel) checkoutCustomer;
			if (b2bcustomer.getDefaultB2BUnit() != null
					&& CollectionUtils.isNotEmpty(b2bcustomer.getDefaultB2BUnit().getAddresses()))
			{
				for (final AddressModel address : b2bcustomer.getDefaultB2BUnit().getAddresses())
				{
					final AddressData addr = new AddressData();
					addressConverter.convert(address, addr);
					addressData.add(addr);

				}

			}

		}
	}


	@Override
	public List<AddressData> populateLinkedAddressesForGivenType(final String addressType, final List<AddressData> allAddressList)
	{
		if (IS_DEBUG_ENABLED)
		{
			LOG.debug("Logged in:::: populateLinkedAddressesForGivenType(): addressType= " + addressType);
		}

		final List<AddressData> custAddressDataList = new ArrayList<AddressData>();
		AddressData contactAddress = null;

		if (CollectionUtils.isNotEmpty(allAddressList))
		{
			for (final AddressData address : allAddressList)
			{
				if (StringUtils.equalsIgnoreCase(addressType, Seqirusb2bCoreConstants.SHIPPING_ADDRESS)
						&& address.isShippingAddress())
				{
					custAddressDataList.add(address);
				}

				if (null == contactAddress && address.isDefaultAddress())
				{
					contactAddress = address;
				}
			}

			//adding contact address as default billing/shipping address in-case No such address available.
			if (CollectionUtils.isEmpty(custAddressDataList) && null != contactAddress)
			{
				LOG.info("Making Contact address as default billing/shipping address as No such address available");
				custAddressDataList.add(contactAddress);
			}
			else if (contactAddress == null)
			{
				LOG.error("Error Case: There is No Contact address attached to the User's B2BUnit.");
			}

		}
		else
		{
			LOG.error("Error Case: No addresses attached to the User's B2BUnit.");
		}

		return custAddressDataList;
	}


	@Override
	public void addToSeqCart(final long code, final long quantity, final AddressData address)
	{
		final ProductModel product = productService.getProductForCode(String.valueOf(code));

		final CartModel cartModel = cartService.getSessionCart();

		//final UserModel userModel = getUserService().getCurrentUser();
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setProduct(product);
		parameter.setCreateNewEntry(true);
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		parameter.setEnableHooks(true);
		parameter.setQuantity(quantity);
		parameter.setShippingAddress(address);
		CommerceCartModification modification = null;
		try
		{
			modification = getCommerceCartService().addToCart(parameter);
		}
		catch (final CommerceCartModificationException e)
		{
			// YTODO Auto-generated catch block
			e.printStackTrace();
		}
   ((SeqirusCartEntryModel) modification.getEntry()).setShipAddressID(address.getAddressId());
		

		//entry.setShippingAddress(getDefaultAddressReverseConverter().convert(address));
		modelService.save((modification.getEntry()));
		//return null;
		//
	}
}

