/**
 *
 */
package com.seqirus.facades.employee.impl;

import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.user.UserMatchingService;
import de.hybris.platform.core.model.user.EmployeeModel;

import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.seqirus.core.services.SeqirusEmployeeService;
import com.seqirus.facades.employee.SeqirusEmployeeFacade;


/**
 * @author 172553
 *
 */
public class SeqirusEmployeeFacadeImpl implements SeqirusEmployeeFacade
{
	private static final Logger LOG = Logger.getLogger(SeqirusEmployeeFacadeImpl.class);

	@Autowired
	private SeqirusEmployeeService seqirusEmployeeService;

	@Autowired
	private UserMatchingService userMatchingService;



	@Override
	public void forgottenPassword(final String id)
	{
		LOG.info("ASM Agen ID in facade : " + id);
		Assert.hasText(id, "The field [id] cannot be empty");
		final EmployeeModel employee = userMatchingService.getUserByProperty(id.toLowerCase(Locale.ENGLISH), EmployeeModel.class);
		LOG.info("ASM Employee Model in facade : " + employee);
		seqirusEmployeeService.forgottenPassword(employee);
		LOG.info("ASM Forgot Password Facade Complete");
	}

	@Override
	public void updatePassword(final String token, final String newPassword) throws TokenInvalidatedException
	{
		seqirusEmployeeService.updatePassword(token, newPassword);
	}

}
