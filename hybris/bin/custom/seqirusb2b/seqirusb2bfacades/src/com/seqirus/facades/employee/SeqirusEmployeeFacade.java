/**
 *
 */
package com.seqirus.facades.employee;

import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;


/**
 * @author 172553
 *
 */
public interface SeqirusEmployeeFacade
{
	/**
	 * Sends a forgotten password request for the employee specified. The given <code>id</code> is one of the unique
	 * identifiers that is used to recognize the employee in matching strategies.
	 *
	 * @param id
	 *           the id of the employee to send the forgotten password mail for.
	 * @throws UnknownIdentifierException
	 *            if the employee cannot be found for the id specified
	 * @see de.hybris.platform.commerceservices.user.impl.DefaultUserMatchingService
	 */
	void forgottenPassword(String id);



	/**
	 * Update the password for the user by decrypting and validating the token.
	 *
	 * @param token
	 *           the token to identify the the employee to reset the password for.
	 * @param newPassword
	 *           the new password to set
	 * @throws IllegalArgumentException
	 *            If the new password is empty or the token is invalid or expired
	 * @throws TokenInvalidatedException
	 *            if the token was already used or there is a newer token
	 */
	void updatePassword(String token, String newPassword) throws TokenInvalidatedException;
}
