package com.seqirus.facades.customer;

import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;


/**
 * Customer address facade interface.
 */
public interface CustomerAddressFacade
{

	public List<AddressData> getAddressBook();
}
