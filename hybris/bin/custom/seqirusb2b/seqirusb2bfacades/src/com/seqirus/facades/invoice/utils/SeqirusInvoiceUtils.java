/**
 *
 */
package com.seqirus.facades.invoice.utils;

import com.seqirus.facades.constants.Seqirusb2bFacadesConstants;
import com.seqirus.facades.invoice.data.SeqirusReturnsAndCreditsResponse;


/**
 * The Class SeqirusInvoiceUtils.
 *
 * @author Avaneesh Chauhan
 */
public class SeqirusInvoiceUtils
{

	/**
	 * Instantiates a new seqirus invoice utils.
	 */
	private SeqirusInvoiceUtils()
	{

	}


	/**
	 * Gets the no credit reponse.
	 *
	 * @param returnsAndCreditsResponse
	 *           the returns and credits response
	 * @return the no credit reponse
	 */
	public static SeqirusReturnsAndCreditsResponse getNoCreditReponse(
			final SeqirusReturnsAndCreditsResponse returnsAndCreditsResponse)
	{
		returnsAndCreditsResponse.setInvoiceNumber(Seqirusb2bFacadesConstants.NA_STATUS);
		returnsAndCreditsResponse.setInvoiceDate(Seqirusb2bFacadesConstants.NA_STATUS);
		returnsAndCreditsResponse.setStatus(Seqirusb2bFacadesConstants.NA_STATUS);
		return returnsAndCreditsResponse;
	}
}
