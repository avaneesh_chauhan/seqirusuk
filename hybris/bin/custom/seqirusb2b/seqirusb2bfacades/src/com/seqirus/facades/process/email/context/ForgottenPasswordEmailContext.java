/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.facades.process.email.context;

import de.hybris.platform.acceleratorcms.model.components.SimpleBannerComponentModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForTemplateModel;
import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * Velocity context for a forgotten password email.
 */
public class ForgottenPasswordEmailContext extends AbstractEmailContext<StoreFrontCustomerProcessModel>
{
	private static final Logger LOG = Logger.getLogger(ForgottenPasswordEmailContext.class);

	private static final String EMPLOYEE = "employee";
	private static final String CUSTOMER = "customer";
	private static final String COPYRIGHT = "copyright";
	private static final String SITE_LOGO = "sitelogo";
	private static final String ADDRESS = "address";
	private static final String HEADER = "header";
	private int expiresInMinutes = 30;
	private String token;

	@Override
	public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(storeFrontCustomerProcessModel, emailPageModel);
		if (storeFrontCustomerProcessModel instanceof ForgottenPasswordProcessModel)
		{
			final ForgottenPasswordProcessModel forgottenPasswordProcessModel = (ForgottenPasswordProcessModel) storeFrontCustomerProcessModel;
			setToken(forgottenPasswordProcessModel.getToken());
			final EmployeeModel employee = forgottenPasswordProcessModel.getEmployee();
			if (null != employee)
			{
				put(EMPLOYEE, employee);
				put(DISPLAY_NAME, employee.getDisplayName());
				put(EMAIL, employee.getUid());
			}
		}
		final PageTemplateModel masterTemplate = emailPageModel.getMasterTemplate();
		final List<ContentSlotForTemplateModel> templateSlots = masterTemplate.getContentSlots();
		if (CollectionUtils.isNotEmpty(templateSlots))
		{
			for (final ContentSlotForTemplateModel contentSlotForTemplateModel : templateSlots)
			{
				final String position = contentSlotForTemplateModel.getPosition();
				final ContentSlotModel contentSlot = contentSlotForTemplateModel.getContentSlot();
				if (null != contentSlot)
				{
					final List<AbstractCMSComponentModel> components = contentSlot.getCmsComponents();
					if (CollectionUtils.isNotEmpty(components))
					{
						final AbstractCMSComponentModel component = components.get(0);
						if (StringUtils.equalsIgnoreCase(position, "SiteLogo"))
						{
							final SimpleBannerComponentModel siteLogoComponent = (SimpleBannerComponentModel) component;
							final String completeUrl = getMediaSecureBaseUrl() + siteLogoComponent.getMedia().getURL();
							put(SITE_LOGO, completeUrl);
						}
						else if (StringUtils.equalsIgnoreCase(position, "TopContent"))
						{
							put(HEADER, component);
						}
						else if (StringUtils.equalsIgnoreCase(position, "BottomContent"))
						{
							put(ADDRESS, component);
						}
						else if (StringUtils.equalsIgnoreCase(position, "CopyrightContent"))
						{
							put(COPYRIGHT, component);
						}
					}
				}
			}
		}
	}

	public int getExpiresInMinutes()
	{
		return expiresInMinutes;
	}

	public void setExpiresInMinutes(final int expiresInMinutes)
	{
		this.expiresInMinutes = expiresInMinutes;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}

	public String getURLEncodedToken() throws UnsupportedEncodingException
	{
		LOG.info("Token : " + token);
		return URLEncoder.encode(token, "UTF-8");
	}

	public String getRequestResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), false,
				"/login/pw/request/external");
	}

	public String getSecureRequestResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true,
				"/login/pw/request/external");
	}

	public String getResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), false,
				"/login/pw/change", "token=" + getURLEncodedToken());
	}

	public String getSecureResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true,
				"/login/pw/change", "token=" + getURLEncodedToken());
	}

	public String getDisplayResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), false,
				"/my-account/update-password");
	}

	public String getDisplaySecureResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true,
				"/my-account/update-password");
	}

	@Override
	protected LanguageModel getEmailLanguage(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}

	@Override
	protected BaseSiteModel getSite(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getSite();

	}

	@Override
	protected CustomerModel getCustomer(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		return null;
	}
}
