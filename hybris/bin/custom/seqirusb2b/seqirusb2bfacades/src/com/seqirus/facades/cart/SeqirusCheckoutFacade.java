/**
 *
 */
 package com.seqirus.facades.cart;

import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;

/**
 * @author nesingh

 * Facade calss for checkout activities
 *
 */
public interface SeqirusCheckoutFacade extends CheckoutFacade
{

	/**
	 * Method to populate all addresses for Sold-To account linked to user.
	 *
	 * @return
	 */
	public List<AddressData> populateSoldToLinkedAddresses();

	/**
	 * Method to populate address for a given address Type.
	 *
	 * @return
	 */
	public List<AddressData> populateLinkedAddressesForGivenType(final String addressType, final List<AddressData> allAddressList);
	
	void addToSeqCart(final long code, final long quantity, final AddressData address);
}

