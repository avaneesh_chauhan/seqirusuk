package com.seqirus.facades.populators;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * Populator for extending additional address fields.
 */
public class SeqirusAddressPopulator implements Populator<AddressModel, AddressData>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final AddressModel source, final AddressData target) throws ConversionException
	{
		target.setSapCustomerId(source.getSapCustomerID());
	}

}
