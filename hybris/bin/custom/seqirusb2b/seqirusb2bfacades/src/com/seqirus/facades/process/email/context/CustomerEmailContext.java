/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.facades.process.email.context;

import de.hybris.platform.acceleratorcms.model.components.SimpleBannerComponentModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForTemplateModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.seqirus.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.facades.customer.data.SeqirusCustomerData;


/**
 * Velocity context for a customer email.
 */
public class CustomerEmailContext extends AbstractEmailContext<StoreFrontCustomerProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomerEmailContext.class);

	private static final String CUSTOMER = "customer";

	private static final String COPYRIGHT = "copyright";
	private static final String SITE_LOGO = "sitelogo";
	private static final String ADDRESS = "address";
	private static final String HEADER = "header";


	private Converter<UserModel, CustomerData> customerConverter;

	@Autowired
	private ModelService modelService;

	@Resource
	private Populator<B2BCustomerModel, SeqirusCustomerData> seqirusCustomerPopulator;

	@Autowired
	protected ConfigurationService configurationService;



	@Override
	public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(storeFrontCustomerProcessModel, emailPageModel);
		//customerData = getCustomerConverter().convert(getCustomer(storeFrontCustomerProcessModel));
		final SeqirusCustomerData customerData = new SeqirusCustomerData();
		final B2BCustomerModel customerModel = (B2BCustomerModel) getCustomer(storeFrontCustomerProcessModel);
		seqirusCustomerPopulator.populate(customerModel, customerData);
		final String customerRemovalFlag = configurationService.getConfiguration()
				.getString(Seqirusb2bCoreConstants.WELCOMECUSTOMER_REMOVALFLAG);
		LOG.info("customerRemovalFlag.." + customerRemovalFlag);
		LOG.debug("customerRemovalFlag.." + customerRemovalFlag);

		LOG.info("sapConsumerID.." + customerData.getSapConsumerID());
		LOG.debug("sapConsumerID.." + customerData.getSapConsumerID());
		if (StringUtils.isNotBlank(customerData.getSapConsumerID()) && customerRemovalFlag.equals("true"))
		{
			LOG.info("removing welcome customer");
			modelService.remove(getCustomer(storeFrontCustomerProcessModel));
		}else {
			if(null!=customerModel) {
				LOG.info("updating welcome customer");
				customerModel.setWelcomeCustomerSoldToId(StringUtils.EMPTY);
				modelService.save(customerModel);
			}
		}
		if (null != customerData)
		{
			put(CUSTOMER, customerData);
		}
		final PageTemplateModel masterTemplate = emailPageModel.getMasterTemplate();
		final List<ContentSlotForTemplateModel> templateSlots = masterTemplate.getContentSlots();
		if (CollectionUtils.isNotEmpty(templateSlots))
		{
			for (final ContentSlotForTemplateModel contentSlotForTemplateModel : templateSlots)
			{
				final String position = contentSlotForTemplateModel.getPosition();
				final ContentSlotModel contentSlot = contentSlotForTemplateModel.getContentSlot();
				if (null != contentSlot)
				{
					final List<AbstractCMSComponentModel> components = contentSlot.getCmsComponents();
					if (CollectionUtils.isNotEmpty(components))
					{
						final AbstractCMSComponentModel component = components.get(0);
						if (StringUtils.equalsIgnoreCase(position, "SiteLogo"))
						{
							final SimpleBannerComponentModel siteLogoComponent = (SimpleBannerComponentModel) component;
							final String completeUrl = getMediaSecureBaseUrl() + siteLogoComponent.getMedia().getURL();
							LOG.info(":::mediaURL...." + completeUrl);
							put(SITE_LOGO, completeUrl);
						}
						else if (StringUtils.equalsIgnoreCase(position, "TopContent"))
						{
							put(HEADER, component);
						}
						else if (StringUtils.equalsIgnoreCase(position, "BottomContent"))
						{
							put(ADDRESS, component);
						}
						else if (StringUtils.equalsIgnoreCase(position, "CopyrightContent"))
						{
							put(COPYRIGHT, component);
						}
					}
				}
			}
		}

		put(CUSTOMER, customerData);

	}

	@Override
	protected BaseSiteModel getSite(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getSite();

	}

	@Override
	protected CustomerModel getCustomer(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getCustomer();
	}

	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}


	@Override
	protected LanguageModel getEmailLanguage(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}

}
