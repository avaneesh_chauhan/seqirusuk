/**
 *
 */
package com.seqirus.facades.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.seqirus.flu360.facades.cutomer.data.CustomerRegistrationAddressData;
import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;


/**
 * @author 172553
 *
 */
public class SeqirusB2BUnitPopulator implements Populator<SeqirusB2BCustomerRegistrationData, B2BUnitModel>
{
	@Autowired
	private ModelService modelService;

	@Override
	public void populate(final SeqirusB2BCustomerRegistrationData source, final B2BUnitModel target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setUid(source.getSoldToId());
		final CountryModel country = modelService.create(CountryModel.class);
		country.setIsocode("US");
		country.setName("United States");
		target.setCountry(country);
		final CustomerRegistrationAddressData businessDetails = source.getOrgAddress();
		target.setName(businessDetails.getOrganizationName());

		final AddressModel contactAddress = modelService.create(AddressModel.class);
		contactAddress.setOwner(target);
		contactAddress.setContactAddress(Boolean.TRUE);
		contactAddress.setFirstname(businessDetails.getFirstName());
		contactAddress.setLastname(businessDetails.getLastName());
		contactAddress.setPhone1(businessDetails.getPhone());
		contactAddress.setEmail(businessDetails.getEmailId());
		contactAddress.setAddressId(businessDetails.getAddressId());
		contactAddress.setCustomerType("Sold To");
		final Set<String> sapCustomerType = new HashSet<>();
		sapCustomerType.add("SP");
		contactAddress.setSapCustomerType(sapCustomerType);
		contactAddress.setStreetname(businessDetails.getAddressLine1());
		contactAddress.setLine1(businessDetails.getAddressLine1());
		contactAddress.setStreetnumber(businessDetails.getAddressLine2());
		contactAddress.setLine2(businessDetails.getAddressLine2());
		contactAddress.setTown(businessDetails.getCity());
		//final RegionModel region = modelService.create(RegionModel.class);
		//region.setIsocode(Reg);
		contactAddress.setPostalcode(businessDetails.getZipCode());
	}

}
