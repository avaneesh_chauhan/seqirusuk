package com.seqirus.facades.populators;


import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.seqirus.core.model.CustomerRegistrationAddressModel;
import com.seqirus.core.model.CustomerRegistrationLicenseDetailModel;
import com.seqirus.core.model.SeqirusB2BCustomerRegistrationModel;
import com.seqirus.core.model.SeqirusCustomerShippingAddressModel;
import com.seqirus.flu360.facades.cutomer.data.CustomerRegistrationAddressData;
import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;
import com.seqirus.flu360.facades.cutomer.data.SeqirusCustomerShippingAddressData;


/**
 * CustomerRegisterReversePopulator to convert the data from SeqirusB2BCustomerRegistrationData object to
 * SeqirusB2BCustomerRegistrationModel object.
 */
public class CustomerRegisterReversePopulator
		implements Populator<SeqirusB2BCustomerRegistrationData, SeqirusB2BCustomerRegistrationModel>
{
	private static final Logger LOGGER = Logger.getLogger(CustomerRegisterReversePopulator.class);

	private static final String COMMA_SEPARATOR = ", ";

	private UserService userService;
	private ModelService modelService;

	/**
	 * @return the userService
	 */
	public synchronized UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public synchronized void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


	/**
	 * method to populate SeqirusB2BCustomerRegistrationModel type from SeqirusB2BCustomerRegistrationData.
	 *
	 * @param source
	 * @param target
	 */
	@Override
	public void populate(final SeqirusB2BCustomerRegistrationData source, final SeqirusB2BCustomerRegistrationModel target)
			throws ConversionException
	{

		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		final CustomerRegistrationAddressData businessDetails = source.getOrgAddress();
		//Set Business Details
		if (null != businessDetails)
		{
			final CustomerRegistrationAddressModel orgAddress = modelService.create(CustomerRegistrationAddressModel.class);
			orgAddress.setOwner(target);
			orgAddress.setAddressId(businessDetails.getAddressId());
			orgAddress.setOrganizationName(businessDetails.getOrganizationName());
			orgAddress.setDuns(businessDetails.getDuns());
			orgAddress.setLine1(businessDetails.getAddressLine1());
			orgAddress.setLine2(businessDetails.getAddressLine2());
			orgAddress.setTown(businessDetails.getCity());
			orgAddress.setState(businessDetails.getState());
			orgAddress.setPostalcode(businessDetails.getZipCode());
			modelService.save(orgAddress);
			target.setCustomerRegistrationOrgAddress(orgAddress);
		}

		//Set Billing Details
		final CustomerRegistrationAddressData billingDetails = source.getBillingAddress();
		final CustomerRegistrationAddressModel billingAddress = modelService.create(CustomerRegistrationAddressModel.class);
		billingAddress.setOwner(target);
		if (null != billingDetails)
		{
			//Contact Details
			billingAddress.setFirstname(billingDetails.getFirstName());
			billingAddress.setLastname(billingDetails.getLastName());
			billingAddress.setEmail(billingDetails.getEmailId());
			billingAddress.setPhone1(billingDetails.getPhone());
			billingAddress.setPhoneExt(billingDetails.getPhoneExt());
			//Address Details
			billingAddress.setAddressId(billingDetails.getAddressId());
			billingAddress.setOrganizationName(billingDetails.getOrganizationName());
			billingAddress.setLine1(billingDetails.getLine1());
			billingAddress.setLine2(billingDetails.getLine2());
			billingAddress.setTown(billingDetails.getTown());
			billingAddress.setState(billingDetails.getState());
			billingAddress.setPostalcode(billingDetails.getPostalCode());
			//Communication Preferences
			if (StringUtils.isNotBlank(billingDetails.getInvoiceEmailId()))
			{
				billingAddress.setOptNotifyInvoiceEmail(Boolean.TRUE);
				billingAddress.setInvoiceEmailId(billingDetails.getInvoiceEmailId());
			}
			else
			{
				billingAddress.setOptNotifyInvoiceEmail(Boolean.FALSE);
			}
			if (StringUtils.isNotBlank(billingDetails.getAcctStmtEmailId()))
			{
				billingAddress.setOptNotifyAcctStmtEmail(Boolean.TRUE);
				billingAddress.setAcctStmtEmailId(billingDetails.getAcctStmtEmailId());
			}
			else
			{
				billingAddress.setOptNotifyAcctStmtEmail(Boolean.FALSE);
			}
		}
		modelService.save(billingAddress);
		target.setCustomerRegistrationBillingAddress(billingAddress);

		//Set Payer Details
		final CustomerRegistrationAddressData payerDetails = source.getPayerAddress();
		final CustomerRegistrationAddressModel payerAddress = modelService.create(CustomerRegistrationAddressModel.class);
		payerAddress.setOwner(target);
		if (null != payerDetails)
		{
			//Contact Details
			payerAddress.setFirstname(payerDetails.getFirstName());
			payerAddress.setLastname(payerDetails.getLastName());
			payerAddress.setEmail(payerDetails.getEmailId());
			payerAddress.setPhone1(payerDetails.getPhone());
			payerAddress.setPhoneExt(payerDetails.getPhoneExt());
			//Address Details
			payerAddress.setAddressId(billingDetails.getAddressId());
			payerAddress.setOrganizationName(billingDetails.getOrganizationName());
			payerAddress.setLine1(billingDetails.getLine1());
			payerAddress.setLine2(billingDetails.getLine2());
			payerAddress.setTown(billingDetails.getTown());
			payerAddress.setState(billingDetails.getState());
			payerAddress.setPostalcode(billingDetails.getPostalCode());
		}
		modelService.save(payerAddress);
		target.setCustomerRegistrationPayerAddress(payerAddress);

		// license and shipping details

		if (CollectionUtils.isNotEmpty(source.getShippingDetailsList()))
		{
			final List<SeqirusCustomerShippingAddressModel> shipModels = new ArrayList<SeqirusCustomerShippingAddressModel>();

			for (final SeqirusCustomerShippingAddressData data : source.getShippingDetailsList())
			{
				final SeqirusCustomerShippingAddressModel shippingAddressModel = modelService
						.create(SeqirusCustomerShippingAddressModel.class);
				shippingAddressModel.setOwner(target);
				//Contact Details
				shippingAddressModel.setFirstname(data.getFirstName());
				shippingAddressModel.setLastname(data.getLastName());
				shippingAddressModel.setEmail(data.getEmail());
				shippingAddressModel.setPhone1(data.getPhone());
				shippingAddressModel.setPhoneExt(data.getPhoneExt());
				//Address Details
				shippingAddressModel.setAddressId(data.getAddressId());
				shippingAddressModel.setShippingOrganizationName(data.getShippingOrganizationName());
				shippingAddressModel.setShippingAddressLine1(data.getShippingAddressLine1());
				shippingAddressModel.setShippingAddressLine2(data.getShippingAddressLine2());
				shippingAddressModel.setTown(data.getTown());
				shippingAddressModel.setShippingState(data.getShippingState());
				shippingAddressModel.setPostalcode(data.getPostalCode());

				//License Details
				if (data.getLicDetils() != null)
				{
					final CustomerRegistrationLicenseDetailModel customerLiscence = modelService
							.create(CustomerRegistrationLicenseDetailModel.class);
					customerLiscence.setLicenceName(data.getLicDetils().getLicenseName());
					customerLiscence.setLicenceStateNumber(data.getLicDetils().getLicenseStateNumber());
					//License Address Details
					customerLiscence.setLicenceAddressLine1(data.getLicDetils().getLicenceAddressLine1());
					customerLiscence.setLicenceAddressLine2(data.getLicDetils().getLicenceAddressLine2());
					customerLiscence.setCity(data.getLicDetils().getCity());
					customerLiscence.setTerritory(data.getLicDetils().getTerritory());
					customerLiscence.setPostalCode(data.getLicDetils().getPostalCode());
					modelService.save(customerLiscence);
					shippingAddressModel.setLicDetails(customerLiscence);
				}
				modelService.save(shippingAddressModel);
				shipModels.add(shippingAddressModel);
			}
			target.setShippingList(shipModels);
		}

	}


	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 *
	 * @return
	 */
	private String generateAddressId()
	{
		final Random r = new Random();
		final int random = r.nextInt(90000000) + 10000000;
		return String.valueOf(random);
	}
}
