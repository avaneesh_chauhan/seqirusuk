/**
 *
 */
package com.seqirus.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.seqirus.core.dataObjects.JoinAccountAPILocation;
import com.seqirus.core.dataObjects.JoinAccountAPIResponse;
import com.seqirus.core.dataObjects.JoinAccountLocationContact;
import com.seqirus.facades.customer.data.SeqirusCustomerLocationResponseData;
import com.seqirus.facades.customer.data.SeqirusCustomerResponseData;

/**
 * @author 700196
 *
 */
public class SeqirusJoinAccountResponsePopulator implements Populator<JoinAccountAPIResponse, SeqirusCustomerResponseData>
{
	@Override
	public void populate(final JoinAccountAPIResponse source, final SeqirusCustomerResponseData target) throws ConversionException
	{

		if (null != source)
		{
			populateCompanyDetails(source, target);
			populateInvoiceDetails(source, target);
			populatePayerDetails(source, target);
			populateShippingDetails(source, target);
		}
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateCompanyDetails(final JoinAccountAPIResponse source, final SeqirusCustomerResponseData target)
	{
		if (null != source.getCompanyDetails())
		{
			final SeqirusCustomerLocationResponseData companyDetails = new SeqirusCustomerLocationResponseData();
			if (StringUtils.isNotBlank(source.getCompanyDetails().getPartnerId()))
			{
				companyDetails.setPartnerId(source.getCompanyDetails().getPartnerId());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getOrgName()))
			{
				companyDetails.setOrganizationName(source.getCompanyDetails().getOrgName());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getPartnerFunction()))
			{
				companyDetails.setSapCustomerType(source.getCompanyDetails().getPartnerFunction());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getCountry()))
			{
				companyDetails.setCountry(source.getCompanyDetails().getCountry());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getNhsCode()))
			{
				companyDetails.setNhsCode(source.getCompanyDetails().getNhsCode());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getRegistrationNumber()))
			{
				companyDetails.setRegistrationNumber(source.getCompanyDetails().getRegistrationNumber());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getVatNumber()))
			{
				companyDetails.setVatNumber(source.getCompanyDetails().getVatNumber());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getBusinessType()))
			{
				companyDetails.setBusinessType(source.getCompanyDetails().getBusinessType());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getAddressLine1()))
			{
				companyDetails.setAddressLine1(source.getCompanyDetails().getAddressLine1());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getAddressLine2()))
			{
				companyDetails.setAddressLine2(source.getCompanyDetails().getAddressLine2());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getCity()))
			{
				companyDetails.setCity(source.getCompanyDetails().getCity());
			}
			if (StringUtils.isNotBlank(source.getCompanyDetails().getZipCode()))
			{
				companyDetails.setPostalCode(source.getCompanyDetails().getZipCode());
			}
			if (null != source.getCompanyDetails().getSoldToContact())
			{
				mapLocationContact(companyDetails, source.getCompanyDetails().getSoldToContact());
			}
			target.setCompanyDetails(companyDetails);
		}
	}

	/**
	 * @param companyDetails
	 * @param locationContact
	 */
	private void mapLocationContact(final SeqirusCustomerLocationResponseData companyDetails,
			final JoinAccountLocationContact locationContact)
	{
		if (StringUtils.isNotBlank(locationContact.getFirstName()))
		{
			companyDetails.setContactFirstName(locationContact.getFirstName());
		}
		if (StringUtils.isNotBlank(locationContact.getLastName()))
		{
			companyDetails.setContactLastName(locationContact.getLastName());
		}
		if (StringUtils.isNotBlank(locationContact.getEmail()))
		{
			companyDetails.setContactEmail(locationContact.getEmail());
		}
		if (StringUtils.isNotBlank(locationContact.getJobTitle()))
		{
			companyDetails.setContactJobTitle(locationContact.getJobTitle());
		}
		if (StringUtils.isNotBlank(locationContact.getTelephone()))
		{
			companyDetails.setContactTelephone(locationContact.getTelephone());
		}
		if (StringUtils.isNotBlank(locationContact.getExtension()))
		{
			companyDetails.setContactExtension(locationContact.getExtension());
		}
		if (StringUtils.isNotBlank(locationContact.getContactNumber()))
		{
			companyDetails.setContactNumber(locationContact.getContactNumber());
		}
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateInvoiceDetails(final JoinAccountAPIResponse source, final SeqirusCustomerResponseData target)
	{
		if (null != source.getBillTo())
		{
			final SeqirusCustomerLocationResponseData billTo = new SeqirusCustomerLocationResponseData();
			if (StringUtils.isNotBlank(source.getBillTo().getPartnerId()))
			{
				billTo.setPartnerId(source.getBillTo().getPartnerId());
			}
			if (StringUtils.isNotBlank(source.getBillTo().getOrgName()))
			{
				billTo.setOrganizationName(source.getBillTo().getOrgName());
			}
			if (StringUtils.isNotBlank(source.getBillTo().getAddressLine1()))
			{
				billTo.setAddressLine1(source.getBillTo().getAddressLine1());
			}
			if (StringUtils.isNotBlank(source.getBillTo().getAddressLine2()))
			{
				billTo.setAddressLine2(source.getBillTo().getAddressLine2());
			}
			if (StringUtils.isNotBlank(source.getBillTo().getCity()))
			{
				billTo.setCity(source.getBillTo().getCity());
			}
			if (StringUtils.isNotBlank(source.getBillTo().getZipCode()))
			{
				billTo.setPostalCode(source.getBillTo().getZipCode());
			}
			if (StringUtils.isNotBlank(source.getBillTo().getCountry()))
			{
				billTo.setCountry(source.getBillTo().getCountry());
			}
			if (null != source.getBillTo().getBillToContact())
			{
				mapLocationContact(billTo, source.getBillTo().getBillToContact());
			}
			target.setInvoicingContractInfo(billTo);
		}
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populatePayerDetails(final JoinAccountAPIResponse source, final SeqirusCustomerResponseData target)
	{
		if (null != source.getPayer())
		{
			final SeqirusCustomerLocationResponseData payer = new SeqirusCustomerLocationResponseData();
			if (StringUtils.isNotBlank(source.getPayer().getPartnerId()))
			{
				payer.setPartnerId(source.getPayer().getPartnerId());
			}
			if (StringUtils.isNotBlank(source.getPayer().getOrgName()))
			{
				payer.setOrganizationName(source.getPayer().getOrgName());
			}
			if (StringUtils.isNotBlank(source.getPayer().getAddressLine1()))
			{
				payer.setAddressLine1(source.getPayer().getAddressLine1());
			}
			if (StringUtils.isNotBlank(source.getPayer().getAddressLine2()))
			{
				payer.setAddressLine2(source.getPayer().getAddressLine2());
			}
			if (StringUtils.isNotBlank(source.getPayer().getCity()))
			{
				payer.setCity(source.getPayer().getCity());
			}
			if (StringUtils.isNotBlank(source.getPayer().getZipCode()))
			{
				payer.setPostalCode(source.getPayer().getZipCode());
			}
			if (StringUtils.isNotBlank(source.getPayer().getCountry()))
			{
				payer.setCountry(source.getPayer().getCountry());
			}
			if (null != source.getPayer().getPayerContact())
			{
				mapLocationContact(payer, source.getPayer().getPayerContact());
			}
			target.setPayingContactInfo(payer);
		}
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateShippingDetails(final JoinAccountAPIResponse source, final SeqirusCustomerResponseData target)
	{
		if (CollectionUtils.isNotEmpty(source.getShipTo()))
		{
			final List<SeqirusCustomerLocationResponseData> shipLocationList = new ArrayList<>();
			for (final JoinAccountAPILocation shipLocation : source.getShipTo())
			{
				final SeqirusCustomerLocationResponseData shipping = new SeqirusCustomerLocationResponseData();
				if (StringUtils.isNotBlank(shipLocation.getPartnerId()))
				{
					shipping.setPartnerId(shipLocation.getPartnerId());
				}
				if (StringUtils.isNotBlank(shipLocation.getOrgName()))
				{
					shipping.setOrganizationName(shipLocation.getOrgName());
				}
				if (StringUtils.isNotBlank(shipLocation.getAddressLine1()))
				{
					shipping.setAddressLine1(shipLocation.getAddressLine1());
				}
				if (StringUtils.isNotBlank(shipLocation.getAddressLine2()))
				{
					shipping.setAddressLine2(shipLocation.getAddressLine2());
				}
				if (StringUtils.isNotBlank(shipLocation.getCity()))
				{
					shipping.setCity(shipLocation.getCity());
				}
				if (StringUtils.isNotBlank(shipLocation.getZipCode()))
				{
					shipping.setPostalCode(shipLocation.getZipCode());
				}
				if (StringUtils.isNotBlank(shipLocation.getCountry()))
				{
					shipping.setCountry(shipLocation.getCountry());
				}
				if (null!=shipLocation.getShipToLicense() && StringUtils.isNotBlank(shipLocation.getShipToLicense().getLicenseName()))
				{
					shipping.setLicenseName(shipLocation.getShipToLicense().getLicenseName());
				}
				if (null != shipLocation.getShipToLicense()
						&& StringUtils.isNotBlank(shipLocation.getShipToLicense().getLicenseNumber()))
				{
					shipping.setLicenseNumber(shipLocation.getShipToLicense().getLicenseNumber());
				}
				mapShipContact(shipLocation, shipping);
				shipLocationList.add(shipping);
			}
			target.setShippingLocations(shipLocationList);
		}
	}

	/**
	 * @param shipLocation
	 * @param shipping
	 */
	private void mapShipContact(final JoinAccountAPILocation shipLocation, final SeqirusCustomerLocationResponseData shipping)
	{
		if (null != shipLocation && null != shipLocation.getShipToContact())
		{
			for (final JoinAccountLocationContact shipContact : shipLocation.getShipToContact())
			{
				if (StringUtils.isNotBlank(shipContact.getFirstName()))
				{
					shipping.setContactFirstName(shipContact.getFirstName());
				}
				if (StringUtils.isNotBlank(shipContact.getLastName()))
				{
					shipping.setContactLastName(shipContact.getLastName());
				}
				if (StringUtils.isNotBlank(shipContact.getEmail()))
				{
					shipping.setContactEmail(shipContact.getEmail());
				}
				if (StringUtils.isNotBlank(shipContact.getJobTitle()))
				{
					shipping.setContactJobTitle(shipContact.getJobTitle());
				}
				if (StringUtils.isNotBlank(shipContact.getTelephone()))
				{
					shipping.setContactTelephone(shipContact.getTelephone());
				}
				if (StringUtils.isNotBlank(shipContact.getExtension()))
				{
					shipping.setContactExtension(shipContact.getExtension());
				}
				if (StringUtils.isNotBlank(shipContact.getContactNumber()))
				{
					shipping.setContactNumber(shipContact.getContactNumber());
				}
			}
		}
	}
}
