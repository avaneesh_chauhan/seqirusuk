/**
 *
 */
package com.seqirus.facades.invoice.data;



/**
 * @author 700196
 *
 */
public class SeqirusInvoiceLandingListResponse
{
	public String invoiceNumber;
	public String salesOrderNumber;
	public String invoiceDate;
	public String dueDate;
	public String status;
	public double amount;
	public String fmtAmount;

	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber
	 *           the invoiceNumber to set
	 */
	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return the salesOrderNumber
	 */
	public String getSalesOrderNumber()
	{
		return salesOrderNumber;
	}

	/**
	 * @param salesOrderNumber
	 *           the salesOrderNumber to set
	 */
	public void setSalesOrderNumber(final String salesOrderNumber)
	{
		this.salesOrderNumber = salesOrderNumber;
	}


	/**
	 * @return the invoiceDate
	 */
	public String getInvoiceDate()
	{
		return invoiceDate;
	}

	/**
	 * @param invoiceDate
	 *           the invoiceDate to set
	 */
	public void setInvoiceDate(final String invoiceDate)
	{
		this.invoiceDate = invoiceDate;
	}

	/**
	 * @return the dueDate
	 */
	public String getDueDate()
	{
		return dueDate;
	}

	/**
	 * @param dueDate
	 *           the dueDate to set
	 */
	public void setDueDate(final String dueDate)
	{
		this.dueDate = dueDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final String status)
	{
		this.status = status;
	}

	/**
	 * @return the amount
	 */
	public double getAmount()
	{
		return amount;
	}

	/**
	 * @param amount
	 *           the amount to set
	 */
	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	/**
	 * @return the fmtAmount
	 */
	public String getFmtAmount()
	{
		return fmtAmount;
	}

	/**
	 * @param fmtAmount
	 *           the fmtAmount to set
	 */
	public void setFmtAmount(final String fmtAmount)
	{
		this.fmtAmount = fmtAmount;
	}



}
