/**
 *
 */
package com.seqirus.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.seqirus.constants.Seqirusb2bworkflowConstants;
import com.seqirus.core.enums.TempObjectStatusEnum;
import com.seqirus.core.model.CustomerAddressModel;
import com.seqirus.core.model.SeqirusCompanyInfoModel;
import com.seqirus.core.registration.service.SeqirusCustomerRegistrationService;
import com.seqirus.facades.B2BRegistrationFacade;
import com.seqirus.facades.cutomer.data.SeqirusCustomerRegistrationAddressData;
import com.seqirus.facades.cutomer.data.SeqirusCustomerRegistrationData;
import com.seqirus.model.B2BRegistrationModel;


/**
 * @author 614269
 *
 */
public class SeqirusB2BRegistrationWFDataPopulator implements Populator<SeqirusCustomerRegistrationData, B2BRegistrationModel>
{

	@Resource(name = "userService")
	UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Autowired
	private B2BRegistrationFacade b2bRegistrationFacade;

	@Override
	public void populate(final SeqirusCustomerRegistrationData source, final B2BRegistrationModel target) throws ConversionException
	{

		final UserModel currentUser = userService.getCurrentUser();
		final B2BCustomerModel customer = (B2BCustomerModel) currentUser;

		target.setName(customer.getName());
		if (null != customer.getDefaultB2BUnit())
		{
			target.setAccountNumber(customer.getDefaultB2BUnit().getUid());
			target.setCompanyName(customer.getDefaultB2BUnit().getName());
		}
		target.setEmail(customer.getUid());
		target.setTelephone(customer.getPhone());

		populateCustomerModel(source, target);
		populatePayingAddressModel(source, target, customer);
		populateinvoicingAddressModel(source, target, customer);
		populateShippingAddressModel(source, target, customer);

	}


	private void populateCustomerModel(final SeqirusCustomerRegistrationData customerRegistrationData,
			final B2BRegistrationModel target)
	{
		if (null != customerRegistrationData.getUpdateCustInfoFlag()
				&& customerRegistrationData.getUpdateCustInfoFlag().equals("true"))
		{
		final SeqirusCompanyInfoModel companyInfo = new SeqirusCompanyInfoModel();

		if (StringUtils.isNotBlank(customerRegistrationData.getCompanyName()))
		{
			companyInfo.setCompanyName(customerRegistrationData.getCompanyName());
		}
		if (StringUtils.isNotBlank(customerRegistrationData.getCompanyType()))
		{
			companyInfo.setCompanyType(customerRegistrationData.getCompanyType());
		}
		if (null != customerRegistrationData.getCompanyRegNumber())
		{
			companyInfo.setRegistrationNumber(customerRegistrationData.getCompanyRegNumber());
		}
		if (StringUtils.isNotBlank(customerRegistrationData.getBusinessType()))
		{
			companyInfo.setBusinessType(customerRegistrationData.getBusinessType());
		}
		if (StringUtils.isNotBlank(customerRegistrationData.getVatNumber()))
		{
			companyInfo.setVatNumber(customerRegistrationData.getVatNumber());
		}

		if (null != customerRegistrationData.getTradingSince())
		{
			companyInfo.setTradingSince(customerRegistrationData.getTradingSince());
		}
		if (null != customerRegistrationData.getFirstName())
		{
			companyInfo.setFirstName(customerRegistrationData.getFirstName());
		}
		if (null != customerRegistrationData.getLastName())
		{
			companyInfo.setLastName(customerRegistrationData.getLastName());
		}
		if (null != customerRegistrationData.getEmail())
		{
			companyInfo.setEmail(customerRegistrationData.getEmail());
		}
		if (null != customerRegistrationData.getPhoneNumber())
		{
			companyInfo.setPhoneNumber(customerRegistrationData.getPhoneNumber());
		}
		if (null != customerRegistrationData.getPhoneExt())
		{
			companyInfo.setPhoneExt(customerRegistrationData.getPhoneExt());
		}
		if (null != customerRegistrationData.getJobTitle())
		{
			companyInfo.setJobTitle(customerRegistrationData.getJobTitle());
		}
		if (null != customerRegistrationData.getNhcNumber())
		{
			companyInfo.setNhcNumber(customerRegistrationData.getNhcNumber());
		}
		if (null != customerRegistrationData.getBuildingStreet())
		{
			companyInfo.setBuildingStreet(customerRegistrationData.getBuildingStreet());
		}
		if (null != customerRegistrationData.getAdditionalStreet())
		{
			companyInfo.setAdditionalStreet(customerRegistrationData.getAdditionalStreet());
		}
		if (null != customerRegistrationData.getCity())
		{
			companyInfo.setCity(customerRegistrationData.getCity());
		}
		if (null != customerRegistrationData.getPostCode())
		{
			companyInfo.setPostCode(customerRegistrationData.getPostCode());
		}
		final CountryModel businessCountry = fetchCountry(customerRegistrationData.getCountry());
		if (null != businessCountry)
		{
			companyInfo.setCountry(businessCountry);
		}
		target.setCompanyInfo(companyInfo);

		b2bRegistrationFacade.updateAccount(target, Seqirusb2bworkflowConstants.Workflows.UPDATE_BUSINESS_DETAILS_WORKFLOW);
	}
}

	/**
	 * @param customer
	 * @param custRegistrationForm
	 */


	private void populatePayingAddressModel(final SeqirusCustomerRegistrationData customerRegistrationData,
			final B2BRegistrationModel target, final B2BCustomerModel customer)
	{
		if (null != customerRegistrationData.getPayingContactData())
		{
			String workflowname = null;
			final SeqirusCustomerRegistrationAddressData payerAddress = customerRegistrationData.getPayingContactData();
			final CustomerAddressModel payingDetailsModel = modelService.create(CustomerAddressModel.class);
			payingDetailsModel.setOwner(target);
			if (null == payerAddress.getAddressID())
			{
				//payingDetailsModel.setAddressId(customer.getCustomerID() + "-Paying");
				payingDetailsModel.setStatus(TempObjectStatusEnum.NEW);
				workflowname = Seqirusb2bworkflowConstants.Workflows.ADD_LOCATION_WORKFLOW;

			}
			else
			{
				payingDetailsModel.setStatus(TempObjectStatusEnum.CHANGE);
				workflowname = Seqirusb2bworkflowConstants.Workflows.UPDATE_LOCATION_WORKFLOW;
			}


			if (null != payerAddress.getCompanyName())
			{
				payingDetailsModel.setCompany(payerAddress.getCompanyName());
			}

			if (null != payerAddress.getEmail())
			{
				payingDetailsModel.setEmail(payerAddress.getEmail());
			}

			if (null != payerAddress.getLine1())
			{
				payingDetailsModel.setBuildingStreet(payerAddress.getLine1());
			}

			if (null != payerAddress.getPhone())
			{
				payingDetailsModel.setPhone1(payerAddress.getPhone());
			}

			if (null != payerAddress.getLine2())
			{
				payingDetailsModel.setAdditionalStreet(payerAddress.getLine2());
			}

			if (null != payerAddress.getPhoneExt())
			{
				payingDetailsModel.setPhoneExt(payerAddress.getPhoneExt().toString());
			}

			if (null != payerAddress.getCity())
			{
				payingDetailsModel.setCity(payerAddress.getCity());
			}

			if (null != payerAddress.getJobTitle())
			{
				payingDetailsModel.setJobTitle(payerAddress.getJobTitle());
			}

			if (null != payerAddress.getFirstName())
			{
				payingDetailsModel.setFirstname(payerAddress.getFirstName());
			}
			if (null != payerAddress.getLastName())
			{
				payingDetailsModel.setLastname(payerAddress.getLastName());
			}
			if (null != payerAddress.getJobTitle())
			{
				payingDetailsModel.setJobTitle(payerAddress.getJobTitle());
			}
			if (null != payerAddress.getCompanyName())
			{
				payingDetailsModel.setOrganizationName(payerAddress.getCompanyName());
			}
			if (null != payerAddress.getPostalCode())
			{
				payingDetailsModel.setZipCode(payerAddress.getPostalCode());
			}
			final CountryModel payerCountry = fetchCountry(payerAddress.getCountry());
			if (null != payerCountry)
			{
				payingDetailsModel.setCountry(payerCountry);
			}

			target.setPayingAddress(payingDetailsModel);
			b2bRegistrationFacade.updateAccount(target, workflowname);

		}
	}

	/**
	 * @param custRegistrationForm
	 * @param customerRegistrationData
	 * @param customer
	 */


	private void populateinvoicingAddressModel(final SeqirusCustomerRegistrationData customerRegistrationData,
			final B2BRegistrationModel target, final B2BCustomerModel customer)
	{
		if (null != customerRegistrationData.getInvoiceContractData())
		{
			String workflowname = null;
			final SeqirusCustomerRegistrationAddressData invoiceContract = customerRegistrationData.getInvoiceContractData();

			final CustomerAddressModel invoicingContractModel = modelService.create(CustomerAddressModel.class);
			invoicingContractModel.setOwner(target);

			if (null == invoiceContract.getAddressID())
			{

				//invoicingContractModel.setAddressId(customer.getCustomerID() + "-Invoicing");
				invoicingContractModel.setStatus(TempObjectStatusEnum.NEW);
				workflowname = Seqirusb2bworkflowConstants.Workflows.ADD_LOCATION_WORKFLOW;

			}
			else
			{
				invoicingContractModel.setStatus(TempObjectStatusEnum.CHANGE);
				workflowname = Seqirusb2bworkflowConstants.Workflows.UPDATE_LOCATION_WORKFLOW;
			}


			if (null != invoiceContract.getCompanyName())
			{
				invoicingContractModel.setCompany(invoiceContract.getCompanyName());
			}

			if (null != invoiceContract.getEmail())
			{
				invoicingContractModel.setEmail(invoiceContract.getEmail());
			}

			if (null != invoiceContract.getLine1())
			{
				invoicingContractModel.setBuildingStreet(invoiceContract.getLine1());
			}

			if (null != invoiceContract.getPhone())
			{
				invoicingContractModel.setPhone1(invoiceContract.getPhone().toString());
			}

			if (null != invoiceContract.getLine2())
			{
				invoicingContractModel.setAdditionalStreet(invoiceContract.getLine2());
			}

			if (null != invoiceContract.getPhoneExt())
			{
				invoicingContractModel.setPhoneExt(invoiceContract.getPhoneExt().toString());
			}

			if (null != invoiceContract.getCity())
			{
				invoicingContractModel.setCity(invoiceContract.getCity());
			}

			if (null != invoiceContract.getJobTitle())
			{
				invoicingContractModel.setJobTitle(invoiceContract.getJobTitle());
			}

			if (null != invoiceContract.getPostalCode())
			{
				invoicingContractModel.setZipCode(invoiceContract.getPostalCode());
			}

			if (null != invoiceContract.getAdditionalEmail())
			{
				invoicingContractModel.setOptionalEmail(invoiceContract.getAdditionalEmail());
			}
			if (null != invoiceContract.getFirstName())
			{
				invoicingContractModel.setFirstname(invoiceContract.getFirstName());
			}
			if (null != invoiceContract.getLastName())
			{
				invoicingContractModel.setLastname(invoiceContract.getLastName());
			}
			if (null != invoiceContract.getJobTitle())
			{
				invoicingContractModel.setJobTitle(invoiceContract.getJobTitle());
			}
			if (null != invoiceContract.getCompanyName())
			{
				invoicingContractModel.setOrganizationName(invoiceContract.getCompanyName());
			}
			final CountryModel invoiceCountry = fetchCountry(invoiceContract.getCountry());
			if (null != invoiceCountry)
			{
				invoicingContractModel.setCountry(invoiceCountry);
			}

			target.setInvoiceAddress(invoicingContractModel);
			b2bRegistrationFacade.updateAccount(target, workflowname);


		}
	}


	/**
	 * @param custRegistrationForm
	 * @param customerRegistrationData
	 * @param customer
	 */
	private void populateShippingAddressModel(final SeqirusCustomerRegistrationData customerRegistrationData,
			final B2BRegistrationModel target, final B2BCustomerModel customer)
	{

		if (null != customerRegistrationData.getShippingLocationsData())
		{

			String workflowname = null;
			final List<SeqirusCustomerRegistrationAddressData> shippingAddressList = customerRegistrationData
					.getShippingLocationsData();
			final List<CustomerAddressModel> shippingLocModelList = new ArrayList<CustomerAddressModel>();

			for (final SeqirusCustomerRegistrationAddressData shippingAddress : shippingAddressList)
			{
				final CustomerAddressModel shippingLocationsModel = modelService.create(CustomerAddressModel.class);
				if (null != shippingAddress.getAddressID())
				{
					shippingLocationsModel.setAddressId(shippingAddress.getAddressID());
					shippingLocationsModel.setStatus(TempObjectStatusEnum.CHANGE);
					workflowname = Seqirusb2bworkflowConstants.Workflows.UPDATE_LOCATION_WORKFLOW;

				}
				else
				{
					//shippingLocationsModel.setAddressId(customer.getCustomerID() + "-Shipping");
					shippingLocationsModel.setStatus(TempObjectStatusEnum.NEW);
					workflowname = Seqirusb2bworkflowConstants.Workflows.ADD_LOCATION_WORKFLOW;
				}
				setShippingData(shippingAddress, shippingLocationsModel);
				shippingLocationsModel.setOwner(target);
				shippingLocModelList.add(shippingLocationsModel);


			}
			target.setShippingAddress(shippingLocModelList);
			b2bRegistrationFacade.updateAccount(target, workflowname);
		}
	}

	/**
	 * @param shippingAddress
	 * @param shippingLocationsModel
	 */
	private void setShippingData(final SeqirusCustomerRegistrationAddressData shippingAddress,
			final CustomerAddressModel shippingLocationsModel)
	{
		if (null != shippingAddress.getLine1())
		{
			shippingLocationsModel.setBuildingStreet(shippingAddress.getLine1());
		}

		if (null != shippingAddress.getLine2())
		{
			shippingLocationsModel.setAdditionalStreet(shippingAddress.getLine2());
		}

		if (null != shippingAddress.getCity())
		{
			shippingLocationsModel.setCity(shippingAddress.getCity());
		}

		if (null != shippingAddress.getPostalCode())
		{
			shippingLocationsModel.setZipCode(shippingAddress.getPostalCode());
		}
		if (null != shippingAddress.getCompanyName())
		{
			shippingLocationsModel.setOrganizationName(shippingAddress.getCompanyName());
			shippingLocationsModel.setCompany(shippingAddress.getCompanyName());
		}
		if (null != shippingAddress.getFirstName())
		{
			shippingLocationsModel.setFirstname(shippingAddress.getFirstName());
		}
		if (null != shippingAddress.getLastName())
		{
			shippingLocationsModel.setLastname(shippingAddress.getLastName());
		}
		if (null != shippingAddress.getEmail())
		{
			shippingLocationsModel.setEmail(shippingAddress.getEmail());
		}
		if (null != shippingAddress.getPhone())
		{
			shippingLocationsModel.setPhone1(shippingAddress.getPhone());
		}
		if (null != shippingAddress.getPhoneExt())
		{
			shippingLocationsModel.setPhoneExt(shippingAddress.getPhoneExt().toString());
		}
		if (null != shippingAddress.getLicenseNumber())
		{
			shippingLocationsModel.setLicenseNumber(shippingAddress.getLicenseNumber());
		}
		if (null != shippingAddress.getLicenseName())
		{
			shippingLocationsModel.setLicenseName(shippingAddress.getLicenseName());
		}
		if (null != shippingAddress.getNhsNumber())
		{
			shippingLocationsModel.setNhsCode(shippingAddress.getNhsNumber());
		}
		final CountryModel shipCountry = fetchCountry(shippingAddress.getCountry());
		if (null != shipCountry)
		{
			shippingLocationsModel.setCountry(shipCountry);
		}
	}

	public CountryModel fetchCountry(final CountryData countryData)
	{
		if (null != countryData && StringUtils.isNotBlank(countryData.getName()))
		{
			final CountryModel countryModelByName = seqirusCustomerRegistrationService.getCountryByName(countryData.getName());
			if (null != countryModelByName)
			{
				return commonI18NService.getCountry(countryModelByName.getIsocode());
			}
		}
		return null;
	}


	/**
	 * @return the seqirusCustomerRegistrationService
	 */
	public SeqirusCustomerRegistrationService getSeqirusCustomerRegistrationService()
	{
		return seqirusCustomerRegistrationService;
	}


	/**
	 * @param seqirusCustomerRegistrationService
	 *           the seqirusCustomerRegistrationService to set
	 */
	public void setSeqirusCustomerRegistrationService(final SeqirusCustomerRegistrationService seqirusCustomerRegistrationService)
	{
		this.seqirusCustomerRegistrationService = seqirusCustomerRegistrationService;
	}

}
