/**
 *
 */
package com.seqirus.facades.populators;

import de.hybris.platform.commercefacades.consent.data.ConsentData;
import de.hybris.platform.commerceservices.model.consent.ConsentModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * @author 813784
 *
 */
public class SeqirusConsentPopulator implements Populator<ConsentModel, ConsentData>
{

	@Override
	public void populate(final ConsentModel source, final ConsentData target) throws ConversionException
	{
		target.setCode(source.getCode());
		target.setConsentGivenDate(source.getConsentGivenDate());
		target.setConsentWithdrawnDate(source.getConsentWithdrawnDate());
		target.setName(source.getConsentTemplate().getName());
	}



}
