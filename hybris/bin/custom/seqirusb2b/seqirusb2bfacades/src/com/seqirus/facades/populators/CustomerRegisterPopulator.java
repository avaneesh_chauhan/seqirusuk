/**
 *
 */
package com.seqirus.facades.populators;

import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.seqirus.core.dataObjects.BillingSummary;
import com.seqirus.core.dataObjects.CustomerSummary;
import com.seqirus.core.dataObjects.PartnerSummary;
import com.seqirus.core.dataObjects.PayerSummary;
import com.seqirus.core.dataObjects.ShippingSummary;
import com.seqirus.core.dataObjects.SoldToSummary;
import com.seqirus.core.model.SeqirusB2BCustomerRegistrationModel;
import com.seqirus.core.model.SeqirusCustomerShippingAddressModel;
import com.seqirus.flu360.facades.cutomer.data.CustomerRegistrationAddressData;
import com.seqirus.flu360.facades.cutomer.data.CustomerRegistrationLicenseDetailData;
import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;
import com.seqirus.flu360.facades.cutomer.data.SeqirusCustomerShippingAddressData;
//import com.seqirus.facades.util.SeqirusFacadeUtils;



/**
 * Converter class to populate Customer API Service data to front end data
 *
 * @author 172553
 *
 */
public class CustomerRegisterPopulator implements Populator<Object, SeqirusB2BCustomerRegistrationData>
{

	private static final String SOLDTO = "SoldTo";
	private static final String BILLTO = "BillTo";
	private static final String PAYER = "Payer";
	private static final String SHIPTO = "ShipTo";

	private static final Logger LOGGER = Logger.getLogger(CustomerRegisterPopulator.class);


	@Override
	public void populate(final Object source, final SeqirusB2BCustomerRegistrationData target) throws ConversionException

	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		LOGGER.info("Partner Type :: " + target.getPartnerType());

		if (source instanceof SeqirusB2BCustomerRegistrationModel)
		{
			final SeqirusB2BCustomerRegistrationModel regModel = (SeqirusB2BCustomerRegistrationModel) source;

			final List<SeqirusCustomerShippingAddressModel> shippingAddressModels = regModel.getShippingList();

			final List<SeqirusCustomerShippingAddressData> shippingAddrDataList = new ArrayList();

			if (CollectionUtils.isNotEmpty(shippingAddressModels))
			{
				for (final SeqirusCustomerShippingAddressModel seqCustAddrModel : shippingAddressModels)
				{
					final SeqirusCustomerShippingAddressData seqCustAddrData = new SeqirusCustomerShippingAddressData();

					seqCustAddrData.setShippingOrganizationName(seqCustAddrModel.getShippingOrganizationName());
					seqCustAddrData.setAddressId(seqCustAddrModel.getAddressId());
					seqCustAddrData.setLine1(seqCustAddrModel.getShippingAddressLine1());
					seqCustAddrData.setFirstName(seqCustAddrModel.getFirstname());
					seqCustAddrData.setLastName(seqCustAddrModel.getLastname());
					seqCustAddrData.setShippingAddressLine1(seqCustAddrModel.getShippingAddressLine1());

					seqCustAddrData.setLine2(seqCustAddrModel.getShippingAddressLine2());

					seqCustAddrData.setShippingAddressLine2(seqCustAddrModel.getShippingAddressLine2());

					seqCustAddrData.setTown(seqCustAddrModel.getTown());

					seqCustAddrData.setShippingPosition(seqCustAddrModel.getShippingPosition());

					final RegionData region = new RegionData();

					region.setIsocodeShort(seqCustAddrModel.getShippingState());

					region.setIsocode(seqCustAddrModel.getShippingState());


					seqCustAddrData.setRegion(region);

					seqCustAddrData.setShippingState(seqCustAddrModel.getShippingState());

					seqCustAddrData.setPostalCode(seqCustAddrModel.getPostalcode());

					seqCustAddrData.setZipcode(seqCustAddrModel.getPostalcode());

					seqCustAddrData.setPhone(seqCustAddrModel.getPhone1());

					seqCustAddrData.setHoursOfOperation(seqCustAddrModel.getHoursOfOperation());

					shippingAddrDataList.add(seqCustAddrData);

					target.setShippingDetailsList(shippingAddrDataList);
				}

			}
		}


		else if (source instanceof PartnerSummary)
		{
			LOGGER.info("START : CustomerRegisterPopulator : Populating All Partner IDs");

			final PartnerSummary partner = (PartnerSummary) source;


			final CustomerRegistrationAddressData orgAddress = new CustomerRegistrationAddressData();
			orgAddress.setAddressId(partner.getSoldToAccount());
			target.setOrgAddress(orgAddress);
			target.setSoldToId(partner.getSoldToAccount());

			final CustomerRegistrationAddressData billingAddress = new CustomerRegistrationAddressData();
			billingAddress.setAddressId(partner.getBillToAccount());
			target.setBillingAddress(billingAddress);
			target.setBillToId(partner.getBillToAccount());

			final CustomerRegistrationAddressData payerAddress = new CustomerRegistrationAddressData();
			payerAddress.setAddressId(partner.getPayerAccount());
			target.setPayerAddress(payerAddress);
			target.setPayerId(partner.getPayerAccount());

			target.setShippingCount(String.valueOf(partner.getShippingCount()));

			LOGGER.info("END : CustomerRegisterPopulator : Populating All Partner IDs");


		}

		else if (SOLDTO.equalsIgnoreCase(target.getPartnerType()))
		{
			LOGGER.info("CustomerRegisterPopulator : Partner Type : " + target.getPartnerType());
			if (source instanceof SoldToSummary)
			{
				//final CustomerSummary custSummary = (CustomerSummary) source;

				final SoldToSummary soldTOSummary = (SoldToSummary) source;
				//custSummary.getSoldTO();
				LOGGER.info("Populating Customer Details for :: " + soldTOSummary.getId());

				// User and Org Info

				target.setEmailId(soldTOSummary.getLoginEmail());
				target.setFirstName(soldTOSummary.getFirstName());
				target.setLastName(soldTOSummary.getLastName());
				target.setPosition(soldTOSummary.getJobTitle());
				target.setPhoneNumber(soldTOSummary.getContactPhone());
				target.setOrganizationName(soldTOSummary.getOrgName());
				target.setSoldToId(soldTOSummary.getId());

				final CustomerRegistrationAddressData orgAddr = new CustomerRegistrationAddressData();

				orgAddr.setAddressId(soldTOSummary.getId());
				orgAddr.setEmailId(soldTOSummary.getContactEmail());
				orgAddr.setOrganizationName(soldTOSummary.getOrgName());
				orgAddr.setAddressLine1(soldTOSummary.getAddressLine1());
				orgAddr.setAddressLine2(soldTOSummary.getAddressLine2());
				orgAddr.setCity(soldTOSummary.getCity());
				orgAddr.setState(soldTOSummary.getState());
				orgAddr.setZipCode(soldTOSummary.getZipCode());
				orgAddr.setPhone(soldTOSummary.getContactPhone());
				orgAddr.setPhoneExt(soldTOSummary.getContactPhoneExt());

				target.setOrgAddress(orgAddr);
			}
		}

		else if (BILLTO.equalsIgnoreCase(target.getPartnerType()))
		{
			LOGGER.info("CustomerRegisterPopulator : Partner Type : " + target.getPartnerType());

			final CustomerRegistrationAddressData billingAddr = new CustomerRegistrationAddressData();

			final BillingSummary billingSumm = (BillingSummary) source;

			if (null != billingSumm)
			{

				billingAddr.setAddressId(billingSumm.getId());
				billingAddr.setFirstName(billingSumm.getFirstName());
				billingAddr.setLastName(billingSumm.getLastName());
				billingAddr.setPosition(billingSumm.getJobTitle());
				billingAddr.setOrganizationName(billingSumm.getOrgName());
				billingAddr.setAddressLine1(billingSumm.getAddressLine1());
				billingAddr.setAddressLine2(billingSumm.getAddressLine2());
				billingAddr.setCity(billingSumm.getCity());
				billingAddr.setState(billingSumm.getState());
				billingAddr.setPostalCode(billingSumm.getZipCode());
				if (null != billingSumm.getContactEmail())
				{
					billingAddr.setEmailId(billingSumm.getContactEmail().toLowerCase());
				}
				billingAddr.setPhone(billingSumm.getContactPhone());
				billingAddr.setPhoneExt(billingSumm.getContactPhoneExt());

				final String invoiceEmailId = billingSumm.getInvoiceEmail();
				if (null != invoiceEmailId)
				{
					billingAddr.setInvoiceEmailId(invoiceEmailId.toLowerCase());
				}
				if (StringUtils.isNotBlank(invoiceEmailId))
				{
					billingAddr.setOptNotifyInvoiceEmail(Boolean.TRUE);
				}
				else
				{
					billingAddr.setOptNotifyInvoiceEmail(Boolean.FALSE);
				}

				final String acctStmtEmailId = billingSumm.getAcctStmtEmail();
				if (null != acctStmtEmailId)
				{
					billingAddr.setAcctStmtEmailId(acctStmtEmailId.toLowerCase());
				}
				if (StringUtils.isNotBlank(acctStmtEmailId))
				{
					billingAddr.setOptNotifyAcctStmtEmail(Boolean.TRUE);
				}
				else
				{
					billingAddr.setOptNotifyAcctStmtEmail(Boolean.FALSE);
				}

				target.setBillToId(billingSumm.getId());

			}

			target.setBillingAddress(billingAddr);

			LOGGER.info("CustomerRegisterPopulator : Populating BILLTO ID : " + billingSumm.getId());
		}

		else if (PAYER.equalsIgnoreCase(target.getPartnerType()))
		{
			LOGGER.info("CustomerRegisterPopulator : Partner Type : " + target.getPartnerType());

			final CustomerRegistrationAddressData payerAddress = new CustomerRegistrationAddressData();

			final PayerSummary payerSumm = (PayerSummary) source;

			if (null != payerSumm)
			{
				LOGGER.info("CustomerRegisterPopulator : Populating PAYER ID : " + payerSumm.getId());

				payerAddress.setAddressId(payerSumm.getId());
				payerAddress.setFirstName(payerSumm.getFirstName());
				payerAddress.setLastName(payerSumm.getLastName());
				payerAddress.setPosition(payerSumm.getJobTitle());
				payerAddress.setOrganizationName(payerSumm.getOrgName());
				payerAddress.setAddressLine1(payerSumm.getAddressLine1());
				payerAddress.setAddressLine2(payerSumm.getAddressLine2());
				payerAddress.setCity(payerSumm.getCity());
				payerAddress.setState(payerSumm.getState());
				payerAddress.setPostalCode(payerSumm.getZipCode());
				payerAddress.setPhone(payerSumm.getContactPhone());
				payerAddress.setPhoneExt(payerSumm.getContactPhoneExt());
				payerAddress.setEmailId(payerSumm.getContactEmail());
			//	if (null != payerSumm.getDunsNumber())
				//{
					//payerAddress.setDuns(seqirusFacadeUtils.getOnlyDigits(payerSumm.getDunsNumber()));
				//}

				target.setPayerId(payerSumm.getId());

			}

			target.setPayerAddress(payerAddress);

		}

		else if (SHIPTO.equals(target.getPartnerType()))
		{
			LOGGER.info("CustomerRegisterPopulator : Partner Type : " + target.getPartnerType());

			final ShippingSummary shippingSumm = (ShippingSummary) source;

			final List<SeqirusCustomerShippingAddressData> shippingDetailsList = new ArrayList();


			if (null != shippingSumm)
			{

				final SeqirusCustomerShippingAddressData shippingData = new SeqirusCustomerShippingAddressData();

				shippingData.setAddressId(shippingSumm.getId());
				shippingData.setShippingOrganizationName(shippingSumm.getOrgName());
				shippingData.setShippingAddressLine1(shippingSumm.getAddressLine1());
				shippingData.setShippingAddressLine2(shippingSumm.getAddressLine2());
				shippingData.setTown(shippingSumm.getCity());
				shippingData.setShippingState(shippingSumm.getState());
				shippingData.setPostalCode(shippingSumm.getZipCode());
				shippingData.setShipToContactPhone(shippingSumm.getContactPhone());
				shippingData.setShipToContactPhoneExt(shippingSumm.getContactPhoneExt());
				shippingData.setPhone(shippingSumm.getContactPhone());
				shippingData.setPhoneExt(shippingSumm.getContactPhoneExt());
				shippingData.setShippingPosition(shippingSumm.getJobTitle());
				shippingData.setFirstName(shippingSumm.getFirstName());
				shippingData.setLastName(shippingSumm.getLastName());
				shippingData.setEmail(shippingSumm.getContactEmail());


				if (StringUtils.isNotBlank(shippingSumm.getNameOnLicense()))
				{
					final CustomerRegistrationLicenseDetailData customerLicData = new CustomerRegistrationLicenseDetailData();

					if (StringUtils.isBlank(shippingSumm.getNameOnLicense()))
					{
						shippingData.setIsGovtFed(Boolean.TRUE);
					}

					customerLicData.setLicenseName(shippingSumm.getNameOnLicense());
					customerLicData.setLicenseStateNumber(shippingSumm.getLicenseNumber());

					final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

					if (null != shippingSumm.getLicenseExpiry())
					{
						final String strDate = dateFormat.format(shippingSumm.getLicenseExpiry());
						customerLicData.setLicexpiryDate(strDate);
					}

					customerLicData.setStateIssuingLicence(shippingSumm.getLicenseIssueState());
					customerLicData.setLicenceAddressLine1(shippingSumm.getLicenseAddressLine1());
					customerLicData.setLicenceAddressLine2(shippingSumm.getLicenseAddressLine2());
					customerLicData.setCity(shippingSumm.getCity());
					customerLicData.setTerritory(shippingSumm.getState());
					customerLicData.setPostalCode(shippingSumm.getZipCode());

					shippingData.setLicDetils(customerLicData);
				}

				shippingDetailsList.add(shippingData);
			}

			target.setShippingDetailsList(shippingDetailsList);

		}

		else if (source instanceof CustomerSummary)
		{
			final CustomerSummary custSummary = (CustomerSummary) source;

			final SoldToSummary soldTOSummary = custSummary.getSoldTO();
			LOGGER.info("Populating Customer Details for :: " + soldTOSummary.getId());

			// User and Org Info

			target.setEmailId(soldTOSummary.getLoginEmail());
			target.setFirstName(soldTOSummary.getFirstName());
			target.setLastName(soldTOSummary.getLastName());
			target.setPosition(soldTOSummary.getJobTitle());
			target.setPhoneNumber(soldTOSummary.getContactPhone());
			target.setOrganizationName(soldTOSummary.getOrgName());
			target.setSoldToId(soldTOSummary.getId());

			final CustomerRegistrationAddressData orgAddr = new CustomerRegistrationAddressData();

			orgAddr.setAddressId(soldTOSummary.getId());
			orgAddr.setEmailId(soldTOSummary.getContactEmail());
			orgAddr.setOrganizationName(soldTOSummary.getOrgName());
			orgAddr.setAddressLine1(soldTOSummary.getAddressLine1());
			orgAddr.setAddressLine2(soldTOSummary.getAddressLine2());
			orgAddr.setCity(soldTOSummary.getCity());
			orgAddr.setState(soldTOSummary.getState());
			orgAddr.setZipCode(soldTOSummary.getZipCode());
			orgAddr.setPhone(soldTOSummary.getContactPhone());
			orgAddr.setPhoneExt(soldTOSummary.getContactPhoneExt());

			target.setOrgAddress(orgAddr);

			LOGGER.info("Customer Summary Partner Type :: " + target.getPartnerType());


			if (StringUtils.isBlank(target.getPartnerType()))
			{
				//Billing Info

				final BillingSummary billingSummary = custSummary.getBilling();

				LOGGER.info("Populating Bill To ID : " + billingSummary.getId());

				final CustomerRegistrationAddressData billingAddr = new CustomerRegistrationAddressData();

				billingAddr.setAddressId(billingSummary.getId());

				billingAddr.setFirstName(billingSummary.getFirstName());
				billingAddr.setLastName(billingSummary.getLastName());
				billingAddr.setPosition(billingSummary.getJobTitle());
				billingAddr.setOrganizationName(billingSummary.getOrgName());
				billingAddr.setAddressLine1(billingSummary.getAddressLine1());
				billingAddr.setAddressLine2(billingSummary.getAddressLine2());
				billingAddr.setCity(billingSummary.getCity());
				billingAddr.setState(billingSummary.getState());

				billingAddr.setPostalCode(billingSummary.getZipCode());
				billingAddr.setPhone(billingSummary.getContactPhone());
				billingAddr.setPhoneExt(billingSummary.getContactPhoneExt());

				billingAddr.setZipCode(billingSummary.getZipCode());
				billingAddr.setPhone(billingSummary.getContactPhone());
				billingAddr.setEmail(billingSummary.getContactEmail());
				billingAddr.setEmailId(billingSummary.getContactEmail());
				billingAddr.setInvoiceEmailId(billingSummary.getInvoiceEmail());
				billingAddr.setAcctStmtEmailId(billingSummary.getAcctStmtEmail());

				target.setBillToId(billingSummary.getId());
				target.setBillingAddress(billingAddr);

				//Payer Info

				final PayerSummary payerSummary = custSummary.getPayer();
				final CustomerRegistrationAddressData payerAddr = new CustomerRegistrationAddressData();

				LOGGER.info("Populating Payer ID : " + payerSummary.getId());

				payerAddr.setAddressId(payerSummary.getId());

				payerAddr.setFirstName(payerSummary.getFirstName());
				payerAddr.setLastName(payerSummary.getLastName());
				payerAddr.setPosition(payerSummary.getJobTitle());
				payerAddr.setOrganizationName(payerSummary.getOrgName());
				payerAddr.setAddressLine1(payerSummary.getAddressLine1());
				payerAddr.setAddressLine2(payerSummary.getAddressLine2());
				payerAddr.setCity(payerSummary.getCity());
				payerAddr.setState(payerSummary.getState());
				payerAddr.setEmail(payerSummary.getContactEmail());
				payerAddr.setEmailId(payerSummary.getContactEmail());


				payerAddr.setPostalCode(payerSummary.getZipCode());
				payerAddr.setPhone(payerSummary.getContactPhone());
				payerAddr.setPhoneExt(payerSummary.getContactPhoneExt());

				target.setPayerAddress(payerAddr);

				payerAddr.setZipCode(payerSummary.getZipCode());
				payerAddr.setPhone(payerSummary.getContactPhone());
				payerAddr.setPhoneExt(payerSummary.getContactPhoneExt());
				payerAddr.setDuns(payerSummary.getDunsNumber());

				target.setPayerId(payerSummary.getId());
				target.setPayerAddress(payerAddr);

				//Setting Shipping and License data

				final List<ShippingSummary> shippingSummary = custSummary.getShippingLocations();
				final List<SeqirusCustomerShippingAddressData> shippingList = new ArrayList();


				if (CollectionUtils.isNotEmpty(shippingSummary))
				{
					for (final ShippingSummary shippingSum : shippingSummary)
					{
						final SeqirusCustomerShippingAddressData shippingData = new SeqirusCustomerShippingAddressData();

						shippingData.setAddressId(shippingSum.getId());
						shippingData.setShippingOrganizationName(shippingSum.getOrgName());
						shippingData.setShippingAddressLine1(shippingSum.getAddressLine1());
						shippingData.setShippingAddressLine2(shippingSum.getAddressLine2());
						shippingData.setTown(shippingSum.getCity());
						shippingData.setShippingState(shippingSum.getState());
						shippingData.setPostalCode(shippingSum.getZipCode());
						shippingData.setShipToContactPhone(shippingSum.getContactPhone());
						shippingData.setShipToContactPhoneExt(shippingSum.getContactPhoneExt());
						shippingData.setPhone(shippingSum.getContactPhone());
						shippingData.setPhoneExt(shippingSum.getContactPhoneExt());
						shippingData.setShippingPosition(shippingSum.getJobTitle());
						shippingData.setFirstName(shippingSum.getFirstName());
						shippingData.setLastName(shippingSum.getLastName());
						shippingData.setShippingPosition(shippingSum.getJobTitle());
						shippingData.setEmail(shippingSum.getContactEmail());


						final RegionData region = new RegionData();

						region.setIsocodeShort(shippingSum.getState());

						region.setIsocode(shippingSum.getState());


						shippingData.setRegion(region);


						final CustomerRegistrationLicenseDetailData customerLicData = new CustomerRegistrationLicenseDetailData();

						if (StringUtils.isBlank(shippingSum.getNameOnLicense()))
						{
							shippingData.setIsGovtFed(Boolean.TRUE);
						}

						customerLicData.setLicenseName(shippingSum.getNameOnLicense());
						customerLicData.setLicenseStateNumber(shippingSum.getLicenseNumber());

						if (null != shippingSum.getLicenseExpiry())
						{
							final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
							final String strDate = dateFormat.format(shippingSum.getLicenseExpiry());

							customerLicData.setLicexpiryDate(strDate);
						}

						customerLicData.setStateIssuingLicence(shippingSum.getLicenseIssueState());
						customerLicData.setLicenceAddressLine1(shippingSum.getLicenseAddressLine1());
						customerLicData.setLicenceAddressLine2(shippingSum.getLicenseAddressLine2());
						customerLicData.setCity(shippingSum.getLicenseCity());
						customerLicData.setTerritory(shippingSum.getLicenseState());
						customerLicData.setPostalCode(shippingSum.getLicenseZipCode());

						shippingData.setLicDetils(customerLicData);
						shippingList.add(shippingData);
					}

					target.setShippingDetailsList(shippingList);
				}
			}

		}

	}

}

