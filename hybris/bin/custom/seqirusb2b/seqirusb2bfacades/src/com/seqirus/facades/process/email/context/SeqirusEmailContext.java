/**
 *
 */
package com.seqirus.facades.process.email.context;

import de.hybris.platform.acceleratorcms.model.components.SimpleBannerComponentModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForTemplateModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author 172553
 *
 */
public class SeqirusEmailContext<T extends BusinessProcessModel> extends AbstractEmailContext<StoreFrontCustomerProcessModel>
{
	private static final String COPYRIGHT = "copyright";
	private static final String SITE_LOGO = "sitelogo";
	private static final String ADDRESS = "address";
	private static final String HEADER = "header";


	@Override
	public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(storeFrontCustomerProcessModel, emailPageModel);
		final PageTemplateModel masterTemplate = emailPageModel.getMasterTemplate();
		final List<ContentSlotForTemplateModel> templateSlots = masterTemplate.getContentSlots();
		if (CollectionUtils.isNotEmpty(templateSlots))
		{
			for (final ContentSlotForTemplateModel contentSlotForTemplateModel : templateSlots)
			{
				final String position = contentSlotForTemplateModel.getPosition();
				final ContentSlotModel contentSlot = contentSlotForTemplateModel.getContentSlot();
				if (null != contentSlot)
				{
					final List<AbstractCMSComponentModel> components = contentSlot.getCmsComponents();
					if (CollectionUtils.isNotEmpty(components))
					{
						final AbstractCMSComponentModel component = components.get(0);
						if (StringUtils.equalsIgnoreCase(position, "SiteLogo"))
						{
							final SimpleBannerComponentModel siteLogoComponent = (SimpleBannerComponentModel) component;
							final String completeUrl = getMediaSecureBaseUrl() + siteLogoComponent.getMedia().getURL();
							put(SITE_LOGO, completeUrl);
						}
						else if (StringUtils.equalsIgnoreCase(position, "TopContent"))
						{
							put(HEADER, component);
						}
						else if (StringUtils.equalsIgnoreCase(position, "BottomContent"))
						{
							put(ADDRESS, component);
						}
						else if (StringUtils.equalsIgnoreCase(position, "CopyrightContent"))
						{
							put(COPYRIGHT, component);
						}
					}
				}
			}
		}
	}

	@Override
	protected LanguageModel getEmailLanguage(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getLanguage();
	}

	@Override
	protected BaseSiteModel getSite(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel)
	{
		return null;
	}
}
