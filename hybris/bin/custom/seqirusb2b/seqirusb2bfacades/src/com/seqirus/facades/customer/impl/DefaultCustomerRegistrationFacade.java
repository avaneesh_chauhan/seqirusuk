
package com.seqirus.facades.customer.impl;

import com.seqirus.core.exceptions.SeqirusBusinessException;
import com.seqirus.core.model.SeqirusB2BCustomerRegistrationModel;
import com.seqirus.facades.customer.CustomerRegistrationFacade;
import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.seqirus.core.registration.service.SeqirusCustomerRegistrationService;
/**
 * DefaultCustomerRegistrationFacade deals with customer register data and sending email
 *
 */

public class DefaultCustomerRegistrationFacade implements CustomerRegistrationFacade
{
	
	@Autowired
	private ModelService modelService;
	
	@Autowired
	private SeqirusCustomerRegistrationService customerRegistrationService;


	protected Populator<SeqirusB2BCustomerRegistrationData, SeqirusB2BCustomerRegistrationModel> customerRegistrationReversePopulator;
	
	public Populator<SeqirusB2BCustomerRegistrationData, SeqirusB2BCustomerRegistrationModel> getCustomerRegistrationReversePopulator()
	{
		return customerRegistrationReversePopulator;
	}

	/**
	 * @param customerRegistrationReversePopulator
	 *           the customerRegistrationReversePopulator to set
	 */
	public void setCustomerRegistrationReversePopulator(
			final Populator<SeqirusB2BCustomerRegistrationData, SeqirusB2BCustomerRegistrationModel> customerRegistrationReversePopulator)
	{
		this.customerRegistrationReversePopulator = customerRegistrationReversePopulator;
	}



	@Override
	public void registerCustomerRequest(SeqirusB2BCustomerRegistrationData seqCustRegisterData) throws SeqirusBusinessException
	{
		//LOGGER.debug(" registerCustomerRequest of Facade Starts ::");
		final SeqirusB2BCustomerRegistrationModel seqirusCustomerRegistrationModel = createSeqirusCustomer(seqCustRegisterData);
		//sendEmail(seqirusCustomerRegistrationModel);
		//LOGGER.debug(" registerCustomerRequest of Facade Ends ::");

		
	}
	
	private SeqirusB2BCustomerRegistrationModel createSeqirusCustomer(final SeqirusB2BCustomerRegistrationData seqCustRegisterData)
	{
		
					 SeqirusB2BCustomerRegistrationModel seqirusCustomerRegistrationModel = modelService.create(SeqirusB2BCustomerRegistrationModel.class);
					 SeqirusB2BCustomerRegistrationModel seqirusCustomerRegistrationModelExisting = getregByEmailId(seqCustRegisterData.getEmailId());
		
		
		if(null==seqirusCustomerRegistrationModelExisting) {			 
	   getCustomerRegistrationReversePopulator().populate(seqCustRegisterData, seqirusCustomerRegistrationModel);
		modelService.saveAll(seqirusCustomerRegistrationModel);
		
		return seqirusCustomerRegistrationModel;
		
		}else {
			
			getCustomerRegistrationReversePopulator().populate(seqCustRegisterData, seqirusCustomerRegistrationModelExisting);
			modelService.saveAll(seqirusCustomerRegistrationModelExisting);
			
			return seqirusCustomerRegistrationModelExisting;
		}
	}
	
	@Override
	public SeqirusB2BCustomerRegistrationModel getregByEmailId(final String email)
	{
		return customerRegistrationService.getregByEmailId(email);

	}
	

}


