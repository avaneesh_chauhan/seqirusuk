package com.seqirus.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;


/**
 * Populates {@link VariantOptionData} based on {@link VariantProductModel}
 */
public class SeqirusVariantOptionDataPopulator implements Populator<VariantProductModel, VariantOptionData>
{
	@Resource
	private TimeService timeService;
	@Resource
	private UserService userService;

	@Resource
	private ModelService modelService;

	@Resource
	private PriceDataFactory priceDataFactory;

	/*
	 * Populator for VariantOptionData
	 */
	@Resource
	private Converter<MediaModel, ImageData> imageConverter;

	@Override
	public void populate(final VariantProductModel source, final VariantOptionData target)
	{

		target.setDoses(source.getDoses());
		target.setPresentationName(source.getName());
		final Collection<VariantOptionQualifierData> optionQualifierList = new ArrayList<VariantOptionQualifierData>();
		final Collection<CategoryModel> superCats = source.getSupercategories();

		for (final CategoryModel variantValueCategory : superCats)
		{
			final List<CategoryModel> parentCats = variantValueCategory.getSupercategories();

			for (final CategoryModel categoryModel2 : parentCats)
			{
				if (categoryModel2.getCode().equalsIgnoreCase("Seqirus_Formulation"))
				{
					final VariantOptionQualifierData variantOptionQualifier = new VariantOptionQualifierData();
					variantOptionQualifier.setValue(variantValueCategory.getName());
					variantOptionQualifier.setName(variantValueCategory.getCode());
					variantOptionQualifier.setQualifier(variantValueCategory.getCode());
					optionQualifierList.add(variantOptionQualifier);
				}
			}
		}
		target.setVariantOptionQualifiers(optionQualifierList);

		if (source.getIsOrderable() != null)
		{
			target.setOrderable(source.getIsOrderable());
		}

		target.setSequenceId(source.getSequenceId());

	}

}
