/**
 *
 */
package com.seqirus.facades.customer.data;

import de.hybris.platform.commercefacades.user.data.CustomerData;


/**
 * @author 700196
 *
 */
public class SeqirusCustomerData extends CustomerData
{
	private String sapConsumerID;
	private String orgName;



	/**
	 * @return the orgName
	 */
	public String getOrgName()
	{
		return orgName;
	}

	/**
	 * @param orgName
	 *           the orgName to set
	 */
	public void setOrgName(final String orgName)
	{
		this.orgName = orgName;
	}

	/**
	 * @return the sapConsumerID
	 */
	public String getSapConsumerID()
	{
		return sapConsumerID;
	}

	/**
	 * @param sapConsumerID
	 *           the sapConsumerID to set
	 */
	public void setSapConsumerID(final String sapConsumerID)
	{
		this.sapConsumerID = sapConsumerID;
	}


}
