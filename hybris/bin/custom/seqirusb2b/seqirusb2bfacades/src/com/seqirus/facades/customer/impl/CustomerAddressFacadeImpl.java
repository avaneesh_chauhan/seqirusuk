package com.seqirus.facades.customer.impl;

import com.seqirus.facades.customer.CustomerAddressFacade;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;


/**
 * This class will load all the addresses which are assigned to B2B unit.
 */
public class CustomerAddressFacadeImpl implements CustomerAddressFacade
{

	@Autowired
	UserService userService;

	Converter<AddressModel, AddressData> addressConverter;

	/*
	 * This Method will load all the addresses from B2B unit and convert into Address data.
	 */
	@Override
	public List<AddressData> getAddressBook()
	{
		final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) userService.getCurrentUser();

		final B2BUnitModel b2bUnitModel = b2bCustomerModel.getDefaultB2BUnit();

		final Collection<AddressModel> addresses = b2bUnitModel.getAddresses();

		if (CollectionUtils.isNotEmpty(addresses))
		{
			final List<AddressData> result = new ArrayList<AddressData>();

			for (final AddressModel address : addresses)
			{
				result.add(getAddressConverter().convert(address));
			}

			return result;
		}

		return Collections.emptyList();
	}

	protected Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	@Required
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}


}
