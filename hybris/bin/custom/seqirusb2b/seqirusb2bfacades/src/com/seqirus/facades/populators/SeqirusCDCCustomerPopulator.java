/**
 *
 */
package com.seqirus.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import com.seqirus.facades.cutomer.data.SeqirusCDCCustomerData;


/**
 * The Class SeqirusCDCCustomerPopulator: Populator for user profile data fetched from CDC
 */
public class SeqirusCDCCustomerPopulator implements Populator<B2BCustomerModel, SeqirusCDCCustomerData>
{

	/**
	 * Populate values from B2BCustomerModel to SeqirusCDCCustomerData.
	 *
	 * @param source
	 *           B2BCustomerModel
	 * @param target
	 *           SeqirusCDCCustomerData
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final B2BCustomerModel source, final SeqirusCDCCustomerData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (StringUtils.isNotBlank(source.getFirstName()))
		{
			target.setFirstName(source.getFirstName());
		}
		if (StringUtils.isNotBlank(source.getLastname()))
		{
			target.setLastName(source.getLastname());
		}
		if (StringUtils.isNotBlank(source.getUid()))
		{
			target.setUid(source.getUid());
		}
		if (null != source.getJobTitle())
		{
			target.setJobTitle(source.getJobTitle());
		}
		if (null != source.getPhone())
		{
			target.setPhoneNumber(source.getPhone());
		}
		if (null != source.getPhoneExt())
		{
			target.setPhoneExt(source.getPhoneExt());
		}

		if (null != source.getModifiedtime())
		{
			final SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
			target.setLastLogin(dateFormat.format(source.getModifiedtime()));
		}

	}

}
