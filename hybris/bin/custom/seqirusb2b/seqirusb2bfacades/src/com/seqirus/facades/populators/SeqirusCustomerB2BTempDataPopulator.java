/**
 *
 */
package com.seqirus.facades.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import com.seqirus.core.model.CustomerAddressModel;
import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.core.model.SeqirusCompanyInfoModel;
import com.seqirus.core.registration.service.SeqirusCustomerRegistrationService;
import com.seqirus.facades.cutomer.data.SeqirusCustomerRegistrationAddressData;
import com.seqirus.facades.cutomer.data.SeqirusCustomerRegistrationData;


/**
 * @author 614269
 *
 */
public class SeqirusCustomerB2BTempDataPopulator
		implements Populator<Object, SeqirusCustomerRegistrationData>
{

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;

	@Override
	public void populate(final Object source, final SeqirusCustomerRegistrationData target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		setCompanyInfoData(source, target);
		setShippingInfoData(source, target);
		setpayingData(source, target);
		setInvoiceData(source, target);

	}

	/**
	 * @param source
	 * @param target
	 */
	private void setCompanyInfoData(final Object source, final SeqirusCustomerRegistrationData target)
	{

		if (source instanceof SeqirusB2BTempCustomerModel)
		{
			final SeqirusCompanyInfoModel companyInfo = ((SeqirusB2BTempCustomerModel) source).getCompanyInfo();
			target.setStatus("Processing");

			if (null != companyInfo)
		{
		if (StringUtils.isNotBlank(companyInfo.getCompanyName()))
		{
			target.setCompanyName(companyInfo.getCompanyName());
		}
		if (StringUtils.isNotBlank(companyInfo.getCompanyType()))
		{
			target.setCompanyType(companyInfo.getCompanyType());
		}
		if (null != companyInfo.getRegistrationNumber())
		{
			target.setCompanyRegNumber(companyInfo.getRegistrationNumber());
		}
		if (StringUtils.isNotBlank(companyInfo.getBusinessType()))
		{
			if (companyInfo.getBusinessType().contains(","))
			{

				target.setBusinessType(companyInfo.getBusinessType().replace(",", " "));
			}
			else
			{
			target.setBusinessType(companyInfo.getBusinessType());
			}
		}
		if (StringUtils.isNotBlank(companyInfo.getVatNumber()))
		{
			target.setVatNumber(companyInfo.getVatNumber());
		}

		if (null != companyInfo.getTradingSince())
		{
			target.setTradingSince(companyInfo.getTradingSince());
		}
		if (null != companyInfo.getFirstName())
		{
			target.setFirstName(companyInfo.getFirstName());
		}
		if (null != companyInfo.getLastName())
		{
			target.setLastName(companyInfo.getLastName());
		}
		if (null != companyInfo.getEmail())
		{
			target.setEmail(companyInfo.getEmail());
		}
		if (null != companyInfo.getPhoneNumber())
		{
			target.setPhoneNumber(companyInfo.getPhoneNumber());
		}
		if (null != companyInfo.getPhoneExt())
		{
			target.setPhoneExt(companyInfo.getPhoneExt());
		}
		if (null != companyInfo.getJobTitle())
		{
			target.setJobTitle(companyInfo.getJobTitle());
		}
		if (null != companyInfo.getNhcNumber())
		{
			target.setNhcNumber(companyInfo.getNhcNumber());
		}
		if (null != companyInfo.getBuildingStreet())
		{
			target.setBuildingStreet(companyInfo.getBuildingStreet());
		}
		if (null != companyInfo.getAdditionalStreet())
		{
			target.setAdditionalStreet(companyInfo.getAdditionalStreet());
		}
		if (null != companyInfo.getCity())
		{
			target.setCity(companyInfo.getCity());
		}
		if (null != companyInfo.getPostCode())
		{
			target.setPostCode(companyInfo.getPostCode());
		}
		if (null != companyInfo.getCountry())
		{
			final CountryData country = setCountryData(companyInfo.getCountry());
			target.setCountry(country);
		}
	}
	}
	else if (source instanceof B2BUnitModel)
	{
		target.setStatus("Completed");
		if (StringUtils.isNotBlank(((B2BUnitModel) source).getCompanyName()))
		{
			target.setCompanyName(((B2BUnitModel) source).getCompanyName());
		}
		if (StringUtils.isNotBlank(((B2BUnitModel) source).getCompanyType()))
		{
			target.setCompanyType(((B2BUnitModel) source).getCompanyType());
		}
		if (null != ((B2BUnitModel) source).getRegistrationNumber())
		{
			target.setCompanyRegNumber(((B2BUnitModel) source).getRegistrationNumber());
		}
		if (StringUtils.isNotBlank(((B2BUnitModel) source).getBusinessType()))
		{
			target.setBusinessType(((B2BUnitModel) source).getBusinessType());
		}
		if (StringUtils.isNotBlank(((B2BUnitModel) source).getVatNumber()))
		{
			target.setVatNumber(((B2BUnitModel) source).getVatNumber());
		}

		if (null != ((B2BUnitModel) source).getTradingSince())
		{
			target.setTradingSince(((B2BUnitModel) source).getTradingSince());
		}
		if (null != ((B2BUnitModel) source).getFirstName())
		{
			target.setFirstName(((B2BUnitModel) source).getFirstName());
		}
		if (null != ((B2BUnitModel) source).getLastName())
		{
			target.setLastName(((B2BUnitModel) source).getLastName());
		}
		if (null != ((B2BUnitModel) source).getEmail())
		{
			target.setEmail(((B2BUnitModel) source).getEmail());
		}
		if (null != ((B2BUnitModel) source).getPhoneNumber())
		{
			target.setPhoneNumber(((B2BUnitModel) source).getPhoneNumber());
		}
		if (null != ((B2BUnitModel) source).getPhoneExt())
		{
			target.setPhoneExt(((B2BUnitModel) source).getPhoneExt());
		}
		if (null != ((B2BUnitModel) source).getJobTitle())
		{
			target.setJobTitle(((B2BUnitModel) source).getJobTitle());
		}
		if (null != ((B2BUnitModel) source).getNhcNumber())
		{
			target.setNhcNumber(((B2BUnitModel) source).getNhcNumber());
		}
		for (final AddressModel addresses : ((B2BUnitModel) source).getAddresses())
		{
			if(addresses.getContactAddress()) {
				if (null != addresses.getStreetname())
		{
			target.setBuildingStreet(addresses.getStreetname());
		}
				if (null != addresses.getLine2())
		{
			target.setAdditionalStreet(addresses.getLine2());
		}
		if (null != addresses.getTown())
		{
			target.setCity(addresses.getTown());
		}
		if (null != addresses.getPostalcode())
		{
			target.setPostCode(addresses.getPostalcode());
		}
		if (null != addresses.getCountry())
		{
			final CountryData country = setCountryData(addresses.getCountry());
			target.setCountry(country);
		}
			}
		}
	}
	}



	private void setpayingData(final Object source, final SeqirusCustomerRegistrationData target)
	{
		if ((source instanceof SeqirusB2BTempCustomerModel) && null != ((SeqirusB2BTempCustomerModel) source).getPayingAddress())
		{
			final SeqirusCustomerRegistrationAddressData payerAddress = new SeqirusCustomerRegistrationAddressData();
			final CustomerAddressModel payingDetailsModel = ((SeqirusB2BTempCustomerModel) source).getPayingAddress();

			payerAddress.setAddressID(payingDetailsModel.getAddressId());
			if (null != payingDetailsModel.getCompany())
			{
				payerAddress.setCompanyName(payingDetailsModel.getCompany());
			}

			if (null != payingDetailsModel.getEmail())
			{
				payerAddress.setEmail(payingDetailsModel.getEmail());
			}
			else
			{
				payerAddress.setEmail(StringUtils.EMPTY);
			}

			if (null != payingDetailsModel.getBuildingStreet())
			{
				payerAddress.setLine1(payingDetailsModel.getBuildingStreet());
			}
			else
			{
				payerAddress.setLine1(StringUtils.EMPTY);
			}

			if (null != payingDetailsModel.getPhone1())
			{
				payerAddress.setPhone(payingDetailsModel.getPhone1());
			}

			if (null != payingDetailsModel.getAdditionalStreet())
			{
				payerAddress.setLine2(payingDetailsModel.getAdditionalStreet());
			}
			else
			{
				payerAddress.setLine2(StringUtils.EMPTY);
			}

			if (StringUtils.isNotBlank(payingDetailsModel.getPhoneExt()))
			{
				payerAddress.setPhoneExt(Integer.valueOf(payingDetailsModel.getPhoneExt()));
			}

			if (null != payingDetailsModel.getCity())
			{
				payerAddress.setCity(payingDetailsModel.getCity());
			}

			if (null != payingDetailsModel.getJobTitle())
			{
				payerAddress.setJobTitle(payingDetailsModel.getJobTitle());
			}

			if (null != payingDetailsModel.getFirstname())
			{
				payerAddress.setFirstName(payingDetailsModel.getFirstname());
			}
			else
			{
				payerAddress.setFirstName(StringUtils.EMPTY);
			}
			if (null != payingDetailsModel.getLastname())
			{
				payerAddress.setLastName(payingDetailsModel.getLastname());
			}
			else
			{
				payerAddress.setLastName(StringUtils.EMPTY);
			}
			if (null != payingDetailsModel.getJobTitle())
			{
				payerAddress.setJobTitle(payingDetailsModel.getJobTitle());
			}
			if (null != payingDetailsModel.getOrganizationName())
			{
				payerAddress.setCompanyName(payingDetailsModel.getOrganizationName());
			}
			if (null != payingDetailsModel.getZipCode())
			{
				payerAddress.setPostalCode(payingDetailsModel.getZipCode());
			}
			if (null != payingDetailsModel.getCountry())
			{
				final CountryData country = setCountryData(payingDetailsModel.getCountry());
				payerAddress.setCountry(country);
			}
			if (null != payingDetailsModel.getStatus())
			{
				payerAddress.setStatus(payingDetailsModel.getStatus().toString());
			}
			target.setPayingContactData(payerAddress);
		}

	}


	private void setInvoiceData(final Object source, final SeqirusCustomerRegistrationData target)
	{

		if ((source instanceof SeqirusB2BTempCustomerModel) && null != ((SeqirusB2BTempCustomerModel) source).getInvoiceAddress())
		{
			final SeqirusCustomerRegistrationAddressData invoiceContractData = new SeqirusCustomerRegistrationAddressData();
			final CustomerAddressModel invoiceContractModel = ((SeqirusB2BTempCustomerModel) source).getInvoiceAddress();
			invoiceContractData.setAddressID(invoiceContractModel.getAddressId());

			if (null != invoiceContractModel.getCompany())
			{
				invoiceContractData.setCompanyName(invoiceContractModel.getCompany());
			}

			if (null != invoiceContractModel.getEmail())
			{
				invoiceContractData.setEmail(invoiceContractModel.getEmail());
			}
			else
			{
				invoiceContractData.setEmail(StringUtils.EMPTY);
			}

			if (null != invoiceContractModel.getBuildingStreet())
			{
				invoiceContractData.setLine1(invoiceContractModel.getBuildingStreet());
			}
			else
			{
				invoiceContractData.setLine1(StringUtils.EMPTY);
			}

			if (null != invoiceContractModel.getPhone1())
			{
				invoiceContractData.setPhone(invoiceContractModel.getPhone1());
			}

			if (null != invoiceContractModel.getAdditionalStreet())
			{
				invoiceContractData.setLine2(invoiceContractModel.getAdditionalStreet());
			}
			else
			{
				invoiceContractData.setLine2(StringUtils.EMPTY);
			}

			if (StringUtils.isNotBlank(invoiceContractModel.getPhoneExt()))
			{
				invoiceContractData.setPhoneExt(Integer.valueOf(invoiceContractModel.getPhoneExt()));
			}

			if (null != invoiceContractModel.getCity())
			{
				invoiceContractData.setCity(invoiceContractModel.getCity());
			}

			if (null != invoiceContractModel.getJobTitle())
			{
				invoiceContractData.setJobTitle(invoiceContractModel.getJobTitle());
			}

			if (null != invoiceContractModel.getZipCode())
			{
				invoiceContractData.setPostalCode(invoiceContractModel.getZipCode());
			}

			if (null != invoiceContractModel.getOptionalEmail())
			{
				invoiceContractData.setAdditionalEmail(invoiceContractModel.getOptionalEmail());
			}
			if (null != invoiceContractModel.getFirstname())
			{
				invoiceContractData.setFirstName(invoiceContractModel.getFirstname());
			}
			else
			{
				invoiceContractData.setFirstName(StringUtils.EMPTY);
			}
			if (null != invoiceContractModel.getLastname())
			{
				invoiceContractData.setLastName(invoiceContractModel.getLastname());
			}
			else
			{
				invoiceContractData.setLastName(StringUtils.EMPTY);
			}
			if (null != invoiceContractModel.getJobTitle())
			{
				invoiceContractData.setJobTitle(invoiceContractModel.getJobTitle());
			}
			if (null != invoiceContractModel.getOrganizationName())
			{
				invoiceContractData.setCompanyName(invoiceContractModel.getOrganizationName());
			}
			if (null != invoiceContractModel.getCountry())
			{
				final CountryData country = setCountryData(invoiceContractModel.getCountry());

				invoiceContractData.setCountry(country);
			}
			if (null != invoiceContractModel.getStatus())
			{
				invoiceContractData.setStatus(invoiceContractModel.getStatus().toString());
			}


			target.setInvoiceContractData(invoiceContractData);

		}


	}


	private void setShippingInfoData(final Object source, final SeqirusCustomerRegistrationData target)
	{

		if ((source instanceof SeqirusB2BTempCustomerModel) && null != ((SeqirusB2BTempCustomerModel) source).getShippingAddress())
		{
			final List<SeqirusCustomerRegistrationAddressData> shippingLocList = new ArrayList<SeqirusCustomerRegistrationAddressData>();
			final List<CustomerAddressModel> shippingAddrModelList = ((SeqirusB2BTempCustomerModel) source).getShippingAddress();

			for (final CustomerAddressModel shippingAddressModel : shippingAddrModelList)
			{
				final SeqirusCustomerRegistrationAddressData addressData = new SeqirusCustomerRegistrationAddressData();

				addressData.setAddressID(shippingAddressModel.getAddressId());
				if (null != shippingAddressModel.getBuildingStreet())
				{
					addressData.setLine1(shippingAddressModel.getBuildingStreet());
				}
				else
				{
					addressData.setLine1(StringUtils.EMPTY);
				}

				if (null != shippingAddressModel.getAdditionalStreet())
				{
					addressData.setLine2(shippingAddressModel.getAdditionalStreet());
				}
				else
				{
					addressData.setLine2(StringUtils.EMPTY);
				}

				if (null != shippingAddressModel.getCity())
				{
					addressData.setCity(shippingAddressModel.getCity());
				}

				if (null != shippingAddressModel.getZipCode())
				{
					addressData.setPostalCode(shippingAddressModel.getZipCode());
				}
				if (null != shippingAddressModel.getFirstname())
				{
					addressData.setFirstName(shippingAddressModel.getFirstname());
				}
				else
				{
					addressData.setFirstName(StringUtils.EMPTY);
				}
				if (null != shippingAddressModel.getLastname())
				{
					addressData.setLastName(shippingAddressModel.getLastname());
				}
				else
				{
					addressData.setLastName(StringUtils.EMPTY);
				}
				if (null != shippingAddressModel.getEmail())
				{
					addressData.setEmail(shippingAddressModel.getEmail());
				}
				else
				{
					addressData.setEmail(StringUtils.EMPTY);
				}
				if (null != shippingAddressModel.getPhone1())
				{
					addressData.setPhone(shippingAddressModel.getPhone1());
				}
				if (StringUtils.isNotBlank(shippingAddressModel.getPhoneExt()))
				{
					addressData.setPhoneExt(Integer.valueOf(shippingAddressModel.getPhoneExt()));
				}
				if (null != shippingAddressModel.getLicenseNumber())
				{
					addressData.setLicenseNumber(shippingAddressModel.getLicenseNumber());
				}
				if (null != shippingAddressModel.getLicenseName())
				{
					addressData.setLicenseName(shippingAddressModel.getLicenseName());
				}
				if (null != shippingAddressModel.getNhsCode())
				{
					addressData.setNhsNumber(shippingAddressModel.getNhsCode());
				}
				if (null != shippingAddressModel.getCountry())
				{
					final CountryData country = setCountryData(shippingAddressModel.getCountry());
					addressData.setCountry(country);
				}
				if (null != shippingAddressModel.getOrganizationName())
				{
					addressData.setCompanyName(shippingAddressModel.getOrganizationName());
				}
				if (null != shippingAddressModel.getStatus())
			{
				addressData.setStatus(shippingAddressModel.getStatus().toString());
			}
				shippingLocList.add(addressData);

			}
			target.setShippingLocationsData(shippingLocList);
		}


	}


	/**
	 * @param countryModel
	 * @return
	 */
	final CountryData setCountryData(final CountryModel countryModel)
	{
		final CountryData country = new CountryData();
		country.setIsocode(countryModel.getIsocode());
		country.setName(countryModel.getName());
		return country;
	}

}
