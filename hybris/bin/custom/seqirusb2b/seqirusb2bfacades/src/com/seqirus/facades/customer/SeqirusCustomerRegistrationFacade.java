/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.facades.customer;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.consent.data.ConsentData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import com.seqirus.facades.customer.data.SeqirusCustomerResponseData;
import com.seqirus.facades.cutomer.data.SeqirusCustomerRegistrationData;


/**
 * @author 700196
 *
 */
public interface SeqirusCustomerRegistrationFacade
{

	/**
	 * @param customerRegistrationData
	 */
	void register(SeqirusCustomerRegistrationData customerRegistrationData);

	/**
	 * Method used to link customer profile with join account
	 */
	void joinAccount();

	/**
	 * Search B2B Unit based on account number
	 *
	 * @param account
	 */
	B2BUnitModel getOrgNameByAccount(final String account);

	/**
	 * @param account
	 */
	SeqirusCustomerRegistrationData fetchCustData(B2BCustomerModel customer, boolean isCompInfo);

	/**
	 * @param customerRegistrationData
	 */
		SeqirusCustomerRegistrationData updateProfile(SeqirusCustomerRegistrationData customerRegistrationData);

	/**
	 * Send Email to Customer for registration completion
	 */
	void sendEmail(UserModel currentCustomer);

	List<ConsentData> getConsentsForCustomer(final CustomerModel customer);

	void sendUpdateRegistrationEmail();

	/**
	 * @param formattedAccnt
	 * @param zipCode
	 * @return SeqirusCustomerResponseData
	 */
	SeqirusCustomerResponseData getCustomerData(String formattedAccnt, String zipCode);

	/**
	 * @param account
	 * @param accesscode
	 *
	 */
	void joinExistingAccount(String accesscode, String account);
}
