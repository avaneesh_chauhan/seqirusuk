package com.seqirus.facades.converters.populators;


import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.seqirus.core.model.CustomerRegistrationAddressModel;
import com.seqirus.core.model.CustomerRegistrationLicenseDetailModel;
import com.seqirus.core.model.SeqirusB2BCustomerRegistrationModel;
import com.seqirus.core.model.SeqirusCustomerShippingAddressModel;
import com.seqirus.flu360.facades.cutomer.data.SeqirusB2BCustomerRegistrationData;
import com.seqirus.flu360.facades.cutomer.data.SeqirusCustomerShippingAddressData;
import com.seqirus.facades.utils.SeqirusFacadeUtils;


/**
 * CustomerRegisterReversePopulator to convert the data from SeqirusB2BCustomerRegistrationData object to
 * SeqirusB2BCustomerRegistrationModel object.
 */
public class CustomerRegisterReversePopulator
		implements Populator<SeqirusB2BCustomerRegistrationData, SeqirusB2BCustomerRegistrationModel>
{
	private static final Logger LOGGER = Logger.getLogger(CustomerRegisterReversePopulator.class);

	private static final String COMMA_SEPARATOR = ", ";

	private UserService userService;
	private ModelService modelService;
	private SeqirusFacadeUtils seqirusFacadeUtils;

	/**
	 * @return the userService
	 */
	public synchronized UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public synchronized void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the seqirusFacadeUtils
	 */
	public SeqirusFacadeUtils getSeqirusFacadeUtils()
	{
		return seqirusFacadeUtils;
	}

	/**
	 * @param seqirusFacadeUtils
	 *           the seqirusFacadeUtils to set
	 */
	public void setSeqirusFacadeUtils(final SeqirusFacadeUtils seqirusFacadeUtils)
	{
		this.seqirusFacadeUtils = seqirusFacadeUtils;
	}

	private String formatPhone(final String phone)
	{
		

		return phone;
	}


	/**
	 * method to populate SeqirusB2BCustomerRegistrationModel type from SeqirusB2BCustomerRegistrationData.
	 *
	 * @param source
	 * @param target
	 */
	@Override
	public void populate(final SeqirusB2BCustomerRegistrationData source, final SeqirusB2BCustomerRegistrationModel target)
			throws ConversionException
	{

		LOGGER.debug("CustomerRegisterReversePopulator Starts");
		final B2BCustomerModel customer = (B2BCustomerModel) userService.getCurrentUser();
		if (null != customer)
		{
			LOGGER.info("First Name : " + customer.getFirstName());
			LOGGER.info("Last Name : " + customer.getLastname());
			LOGGER.info("Job Title : " + customer.getJobTitle());
			LOGGER.info("Phone : " + customer.getPhone());
			LOGGER.info("Phone ext: " + customer.getPhoneExt());
			target.setEmailId(customer.getUid());
			target.setFirstName(customer.getFirstName());
			target.setLastName(customer.getLastname());
			target.setPhoneNumber(formatPhone(customer.getPhone()));
			if (StringUtils.isNotBlank(formatPhone(customer.getPhoneExt())))
			{
				target.setPhoneExt(formatPhone(customer.getPhoneExt()));
			}
			target.setPosition(customer.getJobTitle());
		}
		if (source != null)
		{
			/*
			 * if (source.getFirstName() != null) { target.setFirstName(source.getFirstName()); } if (source.getLastName()
			 * != null) { target.setLastName(source.getLastName()); }
			 *
			 * if (source.getEmailId() != null) { target.setEmailId(source.getEmailId()); }
			 *
			 * if (source.getPosition() != null) { target.setPosition(source.getPosition()); }
			 */

			if (source.getGroupOrganization() != null)
			{
				target.setGroupOrganization(source.getGroupOrganization());
			}
			if (source.getMemberId() != null)
			{
				target.setMemberId(source.getMemberId());
			}


			if (source.getAgentId() != null)
			{
				target.setAgentId(source.getAgentId());
			}

			if (source.getAgentName() != null)
			{
				target.setAgentName(source.getAgentName());
			}

			if (source.getAgentEmail() != null)
			{
				target.setAgentEmail(source.getAgentEmail());
			}


			target.setOptNotify(Boolean.valueOf(source.isOptNotify()));
			target.setIsGovFederalOrg(Boolean.valueOf(source.isIsFederalOrganization()));
			if (source.getOrgAddress() != null)
			{
				final CustomerRegistrationAddressModel orgAddress = modelService.create(CustomerRegistrationAddressModel.class);
				orgAddress.setOwner(target);

				if (StringUtils.isBlank(source.getOrgAddress().getAddressId()))
				{
					orgAddress.setAddressId(generateAddressId());
				}
				else
				{
					orgAddress.setAddressId(source.getOrgAddress().getAddressId());
				}

				if (source.getOrgAddress().getEmailId() != null)
				{
					orgAddress.setEmail(source.getOrgAddress().getEmailId());
					//target.setOrganizationName(source.getOrgAddress().getOrganizationName());
				}

				if (source.getOrgAddress().getOrganizationName() != null)
				{
					orgAddress.setOrganizationName(source.getOrgAddress().getOrganizationName());
				}

				if (source.getOrgAddress().getAddressLine1() != null)
				{
					orgAddress.setLine1(source.getOrgAddress().getAddressLine1());
				}
				if (source.getOrgAddress().getAddressLine2() != null)
				{
					orgAddress.setLine2(source.getOrgAddress().getAddressLine2());
				}
				if (source.getOrgAddress().getCity() != null)
				{
					orgAddress.setTown(source.getOrgAddress().getCity());
				}
				if (source.getOrgAddress().getState() != null)
				{
					orgAddress.setState(source.getOrgAddress().getState());
				}
				if (source.getOrgAddress().getZipCode() != null)
				{
					orgAddress.setPostalcode(source.getOrgAddress().getZipCode());
				}
				if (source.getOrgAddress().getDuns() != null)
				{
					orgAddress.setDuns(formatPhone(source.getOrgAddress().getDuns()));
				}
				/*
				 * if (source.getOrgAddress().getPhone() != null) {
				 * orgAddress.setPhone1(formatPhone(source.getOrgAddress().getPhone())); } if
				 * (source.getOrgAddress().getPhoneExt() != null) {
				 * orgAddress.setPhoneExt(formatPhone(source.getOrgAddress().getPhoneExt())); }
				 */
				if (source.getOrgAddress().getAltEmail() != null)
				{
					orgAddress.setAltEmail(formatPhone(source.getOrgAddress().getAltEmail()));
				}
				modelService.save(orgAddress);
				target.setCustomerRegistrationOrgAddress(orgAddress);

			}

			if (source.getBillingAddress() != null)
			{
				final CustomerRegistrationAddressModel billingAddress = modelService.create(CustomerRegistrationAddressModel.class);

				billingAddress.setOwner(target);
				//billingAddress.setIsSameAsShippingAddress(source.getBillingAddress().isIsSameAsShippingAddress());

				if (StringUtils.isBlank(source.getBillingAddress().getAddressId()))
				{
					billingAddress.setAddressId(generateAddressId());
				}
				else
				{
					billingAddress.setAddressId(source.getBillingAddress().getAddressId());
				}

				if (source.getBillingAddress().getEmailId() != null)
				{
					billingAddress.setEmail(source.getBillingAddress().getEmailId());
				}

				if (source.getBillingAddress().getOrganizationName() != null)
				{
					billingAddress.setOrganizationName(source.getBillingAddress().getOrganizationName());
				}

				if (source.getBillingAddress().getLine1() != null)
				{
					billingAddress.setLine1(source.getBillingAddress().getLine1());
				}
				if (source.getBillingAddress().getLine2() != null)
				{
					billingAddress.setLine2(source.getBillingAddress().getLine2());
				}
				if (source.getBillingAddress().getTown() != null)
				{
					billingAddress.setTown(source.getBillingAddress().getTown());
				}
				if (source.getBillingAddress().getState() != null)
				{
					billingAddress.setState(source.getBillingAddress().getState());
				}
				if (source.getBillingAddress().getPostalCode() != null)
				{
					billingAddress.setPostalcode(source.getBillingAddress().getPostalCode());
				}
				if (source.getBillingAddress().getPhone() != null)
				{
					billingAddress.setPhone1(formatPhone(source.getBillingAddress().getPhone()));
				}
				if (source.getBillingAddress().getPhoneExt() != null)
				{
					billingAddress.setPhoneExt(formatPhone(source.getBillingAddress().getPhoneExt()));
				}
				if (source.getBillingAddress().getFirstName() != null)
				{
					billingAddress.setFirstname(source.getBillingAddress().getFirstName());
				}
				if (source.getBillingAddress().getLastName() != null)
				{
					billingAddress.setLastname(source.getBillingAddress().getLastName());
				}
				if (source.getBillingAddress().getPosition() != null)
				{
					billingAddress.setPosition(source.getBillingAddress().getPosition());
				}
				if (source.getBillingAddress().getEmailId() != null)
				{
					billingAddress.setEmail(source.getBillingAddress().getEmailId());
				}
				if (StringUtils.isNotBlank(source.getBillingAddress().getInvoiceEmailId()))
				{
					billingAddress.setOptNotifyInvoiceEmail(Boolean.TRUE);
					billingAddress.setInvoiceEmailId(source.getBillingAddress().getInvoiceEmailId());
				}
				else
				{
					billingAddress.setOptNotifyInvoiceEmail(Boolean.FALSE);
				}
				if (StringUtils.isNotBlank(source.getBillingAddress().getAltEmail()))
				{
					billingAddress.setAltEmail(source.getBillingAddress().getAltEmail());
				}
				if (StringUtils.isNotBlank(source.getBillingAddress().getAcctStmtEmailId()))
				{
					billingAddress.setOptNotifyAcctStmtEmail(Boolean.TRUE);
					billingAddress.setAcctStmtEmailId(source.getBillingAddress().getAcctStmtEmailId());
				}
				else
				{
					billingAddress.setOptNotifyAcctStmtEmail(Boolean.FALSE);
				}


				modelService.save(billingAddress);
				target.setCustomerRegistrationBillingAddress(billingAddress);
			}

			/* Payer Adress Starts */

			if (source.getPayerAddress() != null)
			{
				final CustomerRegistrationAddressModel payerAddress = modelService.create(CustomerRegistrationAddressModel.class);

				payerAddress.setOwner(target);
				//payerAddress.setIsSameAsShippingAddress(source.getPayerAddress().isIsSameAsShippingAddress());
				if (StringUtils.isBlank(source.getPayerAddress().getAddressId()))
				{
					payerAddress.setAddressId(generateAddressId());
				}
				else
				{
					payerAddress.setAddressId(source.getPayerAddress().getAddressId());
				}

				if (source.getPayerAddress().getEmailId() != null)
				{
					payerAddress.setEmail(source.getPayerAddress().getEmailId());
					//target.setOrganizationName(source.getOrgAddress().getOrganizationName());
				}

				if (source.getPayerAddress().getOrganizationName() != null)
				{
					payerAddress.setOrganizationName(source.getPayerAddress().getOrganizationName());
				}

				if (source.getPayerAddress().getLine1() != null)
				{
					payerAddress.setLine1(source.getPayerAddress().getLine1());
				}
				if (source.getPayerAddress().getLine2() != null)
				{
					payerAddress.setLine2(source.getPayerAddress().getLine2());
				}
				if (source.getPayerAddress().getTown() != null)
				{
					payerAddress.setTown(source.getPayerAddress().getTown());
				}
				if (source.getPayerAddress().getState() != null)
				{
					payerAddress.setState(source.getPayerAddress().getState());
				}
				if (source.getPayerAddress().getPostalCode() != null)
				{
					payerAddress.setPostalcode(source.getPayerAddress().getPostalCode());
				}
				if (source.getPayerAddress().getPhone() != null)
				{
					payerAddress.setPhone1(formatPhone(source.getPayerAddress().getPhone()));
				}
				if (source.getPayerAddress().getPhoneExt() != null)
				{
					payerAddress.setPhoneExt(formatPhone(source.getPayerAddress().getPhoneExt()));
				}
				if (source.getPayerAddress().getFirstName() != null)
				{
					payerAddress.setFirstname(source.getPayerAddress().getFirstName());
				}
				if (source.getPayerAddress().getLastName() != null)
				{
					payerAddress.setLastname(source.getPayerAddress().getLastName());
				}
				if (source.getPayerAddress().getPosition() != null)
				{
					payerAddress.setPosition(source.getPayerAddress().getPosition());
				}
				if (source.getPayerAddress().getEmailId() != null)
				{
					payerAddress.setEmail(source.getPayerAddress().getEmailId());
				}
				if (source.getPayerAddress().getDuns() != null)
				{
					payerAddress.setDuns(formatPhone(source.getPayerAddress().getDuns()));
				}
				if (source.getPayerAddress().getAltEmail() != null)
				{
					payerAddress.setAltEmail(formatPhone(source.getPayerAddress().getAltEmail()));
				}
				modelService.save(payerAddress);
				target.setCustomerRegistrationPayerAddress(payerAddress);
			}

			/* Payer address Ends */

			// license and shipping details

			if (CollectionUtils.isNotEmpty(source.getShippingDetailsList()))
			{
				final List<SeqirusCustomerShippingAddressModel> shipModels = new ArrayList<SeqirusCustomerShippingAddressModel>();

				for (final SeqirusCustomerShippingAddressData data : source.getShippingDetailsList())
				{
					final SeqirusCustomerShippingAddressModel shippingAddressModel = modelService
							.create(SeqirusCustomerShippingAddressModel.class);
					shippingAddressModel.setOwner(target);
					LOGGER.info("ShipTo ID for " + source.getEmailId() + ":" + data.getAddressId());
					if (StringUtils.isBlank(data.getAddressId()))
					{
						shippingAddressModel.setAddressId(generateAddressId());
					}
					else
					{
						shippingAddressModel.setAddressId(data.getAddressId());
					}

					if (StringUtils.isNotEmpty(data.getFirstName()))
					{
						shippingAddressModel.setFirstname(data.getFirstName());
					}
					if (StringUtils.isNotEmpty(data.getLastName()))
					{
						shippingAddressModel.setLastname(data.getLastName());
					}
					if (StringUtils.isNotEmpty(data.getEmail()))
					{
						shippingAddressModel.setEmail(data.getEmail());
					}
					if (StringUtils.isNotEmpty(data.getShippingOrganizationName()))
					{
						shippingAddressModel.setShippingOrganizationName(data.getShippingOrganizationName());
					}
					if (StringUtils.isNotEmpty(data.getShippingAddressLine1()))
					{
						shippingAddressModel.setShippingAddressLine1(data.getShippingAddressLine1());
					}
					if (StringUtils.isNotEmpty(data.getShippingAddressLine2()))
					{
						shippingAddressModel.setShippingAddressLine2(data.getShippingAddressLine2());
					}
					if (StringUtils.isNotEmpty(data.getTown()))
					{
						shippingAddressModel.setTown(data.getTown());
					}
					if (StringUtils.isNotEmpty(data.getShippingState()))
					{
						shippingAddressModel.setShippingState(data.getShippingState());
					}
					if (StringUtils.isNotEmpty(data.getPostalCode()))
					{
						shippingAddressModel.setPostalcode(data.getPostalCode());
					}
					if (StringUtils.isNotEmpty(data.getPhone()))
					{
						shippingAddressModel.setPhone1(formatPhone(data.getPhone()));
					}
					if (StringUtils.isNotEmpty(data.getPhoneExt()))
					{
						shippingAddressModel.setPhoneExt(formatPhone(data.getPhoneExt()));
					}

					if (StringUtils.isNotEmpty(data.getHoursOfOperation()))
					{
						shippingAddressModel.setHoursOfOperation(data.getHoursOfOperation());
					}

					if (StringUtils.isNotEmpty(data.getShippingPosition()))
					{
						shippingAddressModel.setShippingPosition(data.getShippingPosition());
					}
					if (StringUtils.isNotEmpty(data.getShipToContactPhone()))
					{
						shippingAddressModel.setPhone2(formatPhone(data.getShipToContactPhone()));
					}
					if (StringUtils.isNotEmpty(data.getShipToContactPhoneExt()))
					{
						shippingAddressModel.setContactPhoneExt(formatPhone(data.getShipToContactPhoneExt()));
					}
					if (CollectionUtils.isNotEmpty(data.getNonDeliveryDays()))
					{
						shippingAddressModel.setNonDeliveryDays(String.join(COMMA_SEPARATOR, data.getNonDeliveryDays()));
					}



					//modelService.save(shippingAddressModel);
					if (data.getLicDetils() != null)
					{
						final CustomerRegistrationLicenseDetailModel customerLiscence = modelService
								.create(CustomerRegistrationLicenseDetailModel.class);


						//final CustomerRegistrationLicenseDetailData licData = data.getLicDetils();
						//final Boolean value = data.getLicDetils().isIsFederalOrganization();
						//customerLiscence.setIsFederalOrganization(value);
						if (data.getLicDetils().getLicenseName() != null)
						{
							customerLiscence.setLicenceName(data.getLicDetils().getLicenseName());
						}

						if (data.getLicDetils().getLicenseStateNumber() != null)
						{
							customerLiscence.setLicenceStateNumber(data.getLicDetils().getLicenseStateNumber());
						}

						if (data.getLicDetils().getLicexpiryDate() != null)
						{
							Date date = null;
							try
							{
								date = new SimpleDateFormat("MM/dd/yyyy").parse(data.getLicDetils().getLicexpiryDate());
								LOGGER.debug("Date Value after parse" + date);
							}
							catch (final ParseException e)
							{
								LOGGER.error("Date Parse exception in populator", e);
							}
							customerLiscence.setLicenceExpiryDate(date);
						}

						if (data.getLicDetils().getLicenceAddressLine1() != null)
						{
							customerLiscence.setLicenceAddressLine1(data.getLicDetils().getLicenceAddressLine1());
						}
						if (data.getLicDetils().getLicenceAddressLine2() != null)
						{
							customerLiscence.setLicenceAddressLine2(data.getLicDetils().getLicenceAddressLine2());
						}


						if (data.getLicDetils().getStateIssuingLicence() != null)
						{
							customerLiscence.setStateIssuingLicence(data.getLicDetils().getStateIssuingLicence());
						}
						if (data.getLicDetils().getCity() != null)
						{
							customerLiscence.setCity(data.getLicDetils().getCity());
						}
						if (data.getLicDetils().getTerritory() != null)
						{
							customerLiscence.setTerritory(data.getLicDetils().getTerritory());
						}
						if (data.getLicDetils().getPhoneNumber() != null)
						{
							customerLiscence.setPhoneNumber(formatPhone(data.getLicDetils().getPhoneNumber()));
						}
						if (data.getLicDetils().getPhoneExt() != null)
						{
							customerLiscence.setPhoneExt(formatPhone(data.getLicDetils().getPhoneExt()));
						}
						/*
						 * if (data.getLicDetils().getHoursOfOperation() != null) {
						 * customerLiscence.setHoursOfOperation(data.getLicDetils().getHoursOfOperation()); }
						 */
						if (data.getLicDetils().getPostalCode() != null)
						{
							customerLiscence.setPostalCode(data.getLicDetils().getPostalCode());
						}
						modelService.save(customerLiscence);
						shippingAddressModel.setLicDetails(customerLiscence);
					}
					modelService.save(shippingAddressModel);
					shipModels.add(shippingAddressModel);
				}
				target.setShippingList(shipModels);


			}
		}
		LOGGER.debug("CustomerRegisterReversePopulator Ends ::");

	}


	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 *
	 * @return
	 */
	private String generateAddressId()
	{
		final Random r = new Random();
		final int random = r.nextInt(90000000) + 10000000;
		return String.valueOf(random);
	}
}
