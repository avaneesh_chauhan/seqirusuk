/**
 *
 */
package com.seqirus.facades.populators.invoice;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.seqirus.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.core.dataObjects.InvoiceLandingRequest;
import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.model.SeqirusSeasonModel;
import com.seqirus.core.registration.service.SeqirusCustomerRegistrationService;
import com.seqirus.facades.invoice.data.SeqirusInvoiceLandingRequestData;

/**
 * @author 700196
 *
 */
public class SeqirusInvoiceListingRequestPopulator implements Populator<SeqirusInvoiceLandingRequestData, InvoiceLandingRequest>
{

	static Logger logger = Logger.getLogger(SeqirusInvoiceListingRequestPopulator.class);

	@Autowired
	protected ConfigurationService configurationService;

	@Autowired
	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;

	@Resource(name = "userService")
	private UserService userService;

	@Autowired
	protected CMSSiteService cmsSiteService;

	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

	/* (non-Javadoc)
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final SeqirusInvoiceLandingRequestData source, final InvoiceLandingRequest target) throws ConversionException
	{
		final List<SeqirusSeasonModel> seasons = null;
		String fromDate = "2021-09-01";
		String toDate = "2022-08-04";
		final String currentSite = cmsSiteService.getCurrentSite().getUid();
		/*if (configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.INVOICES_DATA_FLAG).equals("true"))
		{
			target.setCustomerNumber("0060098765");
			target.setFromDate("2020-01-10");
			target.setToDate("2020-12-17");
			target.setOrganizationId("0603");
		}
		else
		{*/
			final B2BCustomerModel account = (B2BCustomerModel) userService.getCurrentUser();
			if (null != account && null != account.getDefaultB2BUnit()
					&& StringUtils.isNotBlank(account.getDefaultB2BUnit().getUid()))
			{
				if(account.getDefaultB2BUnit().getUid().equals("SeqirusUK")) {
					target.setCustomerNumber("12345");
				}
				else
				{
					logger.info("Account Number : " + account.getDefaultB2BUnit().getUid());
					target.setCustomerNumber(account.getDefaultB2BUnit().getUid());
				}
			}
			final SeasonEntryModel seasonsEntry = seqirusCustomerRegistrationService.getSeasonEntry();
			if (CollectionUtils.isNotEmpty(seasonsEntry.getSeasonList()))
			{
				for (final SeqirusSeasonModel model : seasonsEntry.getSeasonList())
				{
					logger.info("Order Season : " + model.getOrderSeason());
					logger.info("Source Season : " + source.getSeason());
					if (StringUtils.isNotBlank(source.getSeason()) && source.getSeason().trim().equals(model.getOrderSeason().trim()))
					{
						logger.info("From Date : " + DATE_FORMATTER.format(model.getSeasonStartDate()));
						logger.info("To Date : " + DATE_FORMATTER.format(model.getSeasonEndDate()));
						fromDate = DATE_FORMATTER.format(model.getSeasonStartDate());
						toDate = DATE_FORMATTER.format(model.getSeasonEndDate());
						break;
					}
				}
			}
			target.setFromDate(fromDate);
			target.setToDate(toDate);
			target.setOrganizationId(
					configurationService.getConfiguration()
							.getString(currentSite + "." + Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER));
		//}
	}
}
