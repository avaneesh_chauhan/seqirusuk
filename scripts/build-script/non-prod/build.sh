#!/bin/sh
# Author           : ManojKumar Jeganathan
# Date             : 4th Feb 2020
# Modified Date    : 20th Feb 2020
# Environment      : QA
# Explanation      :
    # 1. Declaring Global Variables
    # 2. Check NFS Disk space because script get failed due to the enough disk space is not available
    # 3. If NFS_DATA disk space is greater than or equal to 90% then the script won't execute
    # 4. Checking whether git is installed. If not install then the script won't execute
    # 5. Getting User Input
    # 6. Clone the repository
    # 7. Zip the custom folder from the cloned repository
    # 8. Checking whether artifact is available in NFS SHARE and if it's not there only then it will copy the artifact to NFS SHARE location
    # 9. Stopping the hybris service
    # 10. Removing the custom folder from Hybris Home location
    # 11. Copying the artifact from NFS to Hybris Home location
    # 12. Unzip the artifact and execute the build steps
    # 13. Starting Hybris service
    # 14. Removing local copy from the machine

# Script follows here:

#################################
######## Build Starts ###########
#################################

echo "Copy custom folder from gitlab to hybris home location"
#sudo -H -u hybris bash -c "cp -rf $CI_PROJECT_DIR/bin/custom $HYBRIS_HOME"
sudo cp -rf $CI_PROJECT_DIR/bin/custom $HYBRIS_HOME_D/
sudo chown -Rf hybris:hybris $HYBRIS_HOME_D/custom

echo "Moving into platform folder"
sudo -H -u hybris bash -c 'cd /opt/hybris_6.4.0/bin/platform && . ./setantenv.sh && ant -version && ant clean all'
