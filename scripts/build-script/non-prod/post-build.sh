#!/bin/sh
# Author           : ManojKumar Jeganathan
# Date             : 4th Feb 2020
# Modified Date    : 20th Feb 2020
# Environment      : QA
# Explanation      :
    # 1. Declaring Global Variables
    # 2. Check NFS Disk space because script get failed due to the enough disk space is not available
    # 3. If NFS_DATA disk space is greater than or equal to 90% then the script won't execute
    # 4. Checking whether git is installed. If not install then the script won't execute
    # 5. Getting User Input
    # 6. Clone the repository
    # 7. Zip the custom folder from the cloned repository
    # 8. Checking whether artifact is available in NFS SHARE and if it's not there only then it will copy the artifact to NFS SHARE location
    # 9. Stopping the hybris service
    # 10. Removing the custom folder from Hybris Home location
    # 11. Copying the artifact from NFS to Hybris Home location
    # 12. Unzip the artifact and execute the build steps
    # 13. Starting Hybris service
    # 14. Removing local copy from the machine

# Script follows here:


# echo "Starting Hybris Service"
# sudo service hybris start

# echo "Check the tomcat logs"
# tail -f /opt/hybris_6.4.0/log/tomcat/console.log | while read LOGLINE
# do
#    #[[ "${LOGLINE}" == *""* ]] && pkill -P $$ tail
#     [[ "${LOGLINE}" == *"Server startup"* ]] && pkill -P $$ tail && echo "Tomcat Started Successfully" && exit 0
#     [[ "${LOGLINE}" == *"Wrapper Stopped"* ]] && pkill -P $$ tail && echo "Tomcat not started. Please check the tomcat logs" && exit 1
# done

#### ZIP the custom folder ####
echo "Zip the folder with respective pipeline ID, date and branch name"
#cd $HYBRIS_HOME_D && zip -r "custom_${CI_PIPELINE_ID}_$(date +'%m_%d_%y')_${CI_BUILD_REF_SLUG}.zip" custom
cp -rf $HYBRIS_HOME_D/custom $NFS_SHARE_Q/

#cho "Zip is completed"

echo "Copying the zip file into NFS share folder - $CI_PIPELINE_ID - $CI_BUILD_REF_SLUG"
#ls -lah $HYBRIS_HOME_D/custom*.zip
cd $NFS_SHARE_Q && zip -r "custom_${CI_PIPELINE_ID}_$(date +'%m_%d_%y')_${CI_BUILD_REF_SLUG}.zip" custom
#cp -rf $HYBRIS_HOME_D/"custom_${CI_PIPELINE_ID}_$(date +'%m_%d_%y')_${CI_BUILD_REF_SLUG}.zip" $NFS_SHARE_Q/

