#!/bin/sh
# Author           : ManojKumar Jeganathan
# Created Date     : 7th Feb 2020
# Modified Date    : 3rd Apr 2020
# Environment      : STG and PROD
# Script follows here:
#
## 1. Remove custom folder under /opt/hybris/bin/ location - Completed
## 2. Pull the latest code from version control - Completed
## 3. Copy the custom folder into /opt/hybris/bin/ location - Completed
## 4. Goto /opt/hybris/bin/platform and execute setantenv.sh - Completed
## 5. Run ant clean all command and ant production command - Completed
## 6. Wait for build to be completed and goto this folder sqr-sqr_v09.06 (increment the version)
## 7. Change the value of packageName key from the  sqr-sqr_v09.06.json file
## 8. Once the build is successful, then goto this location /opt/hybris/temp/hybris/hybrisserver
## 9. Copy the hybrisServer-AllExtensions, hybrisServer-Platform file and paste those files into this  sqr-sqr_v09.06 folder under sqr-sqr_v09.06/hybris/bin/
## 10. Zip the folder sqr-sqr_v09.06.zip
## 11. Copy the zip folder and json file into NFS Share location
## 12. Check the content of metadata.properties from sqr_sqr* folder to make sure about the environment key and value
## 13. Once the zip file is copied into NFS location then create md5 file by running this command md5sum <zip-file-name>
## 14. copy the output of the above command and save it as sqr-sqr_v09.06.md5


#################################
######## Build Starts ###########
#################################

echo "environment_type----$environment_type----version----$version----package_name----$package_name----host_name----$host_name"
echo "Copy custom folder from gitlab to hybris home location"
#sudo -H -u hybris bash -c "cp -rf $CI_PROJECT_DIR/bin/custom $HYBRIS_HOME"
sudo cp -rf $CI_PROJECT_DIR/bin/custom $HYBRIS_HOME_D/
sudo chown -Rf hybris:hybris $HYBRIS_HOME_D/custom

echo "Moving into platform folder"
sudo -H -u hybris bash -c 'cd /opt/hybris_6.4.0/bin/platform && . ./setantenv.sh && ant -version && ant clean all'

echo "ant production command"
sudo -H -u hybris bash -c 'cd /opt/hybris_6.4.0/bin/platform && . ./setantenv.sh && ant -version && ant production'