#!/bin/sh
# Author           : ManojKumar Jeganathan
# Created Date     : 7th Feb 2020
# Modified Date    : 3rd Apr 2020
# Environment      : STG and PROD
# Script follows here:
#
## 1. Remove custom folder under /opt/hybris/bin/ location - Completed
## 2. Pull the latest code from version control - Completed
## 3. Copy the custom folder into /opt/hybris/bin/ location - Completed
## 4. Goto /opt/hybris/bin/platform and execute setantenv.sh - Completed
## 5. Run ant clean all command and ant production command - Completed
## 6. Wait for build to be completed and goto this folder sqr-sqr_v09.06 (increment the version)
## 7. Change the value of packageName key from the  sqr-sqr_v09.06.json file
## 8. Once the build is successful, then goto this location /opt/hybris/temp/hybris/hybrisserver
## 9. Copy the hybrisServer-AllExtensions, hybrisServer-Platform file and paste those files into this  sqr-sqr_v09.06 folder under sqr-sqr_v09.06/hybris/bin/
## 10. Zip the folder sqr-sqr_v09.06.zip
## 11. Copy the zip folder and json file into NFS Share location
## 12. Check the content of metadata.properties from sqr_sqr* folder to make sure about the environment key and value
## 13. Once the zip file is copied into NFS location then create md5 file by running this command md5sum <zip-file-name>
## 14. copy the output of the above command and save it as sqr-sqr_v09.06.md5

####################################
#### Declaring Global Variables ####
####################################
HOME=$(pwd)
HYBRIS_HOME='/opt/hybris'
DATEFORMAT=$(date +'%m_%d_%y')
GIT_IS_AVAILABLE=''
#gitlab='stag'
#VERSION='20.04'
NFS_SHARE='/NFS_DATA/deployment'
HOST_NAME=$(hostname)

################
## User Input ##
################

echo "Enter the branch name:"
read branch

echo "Please enter 'yes' or 'no' if there is any database update:"
read dbUpdate

#echo "Please enter the bitbucket UserName"
#read userName

#echo "Please enter the bitbucket password"
#read password

echo "Enter the version number:"
read VERSION
PACKAGE_NAME=sqr-sqr_v$VERSION

echo "Enter the environment type such as stag (or) prod:"
read gitlab

#exit 1

################################
## Check NFS SHARE DISK SPACE ##
################################

current_usage=$( df -h | grep '/NFS_DATA' | awk {'print $5'} )
max_usage=98%

if [ ${current_usage%?} -ge ${max_usage%?} ];
then
    echo "Max usage exceeded. Your disk usage is at ${current_usage}. Please stop the build"
    exit 1
elif [ ${current_usage%?} -lt ${max_usage%?} ];
then
    echo "No problems. Disk usage at ${current_usage}."
fi

##########################
## Checking Git Version ##
##########################

echo "Checking Git Version"
git --version 2>&1 >/dev/null
GIT_IS_AVAILABLE=$?
if [ $GIT_IS_AVAILABLE -eq 0 ];
then
    echo "Git is installed already"
else
    echo "Git is not installed. So please install git"
    exit 1
fi

    ############################
    ## Cloning the repository ##
    ############################

    #echo "Cloning the repository"
    #git clone --branch $branch https://$userName:$password@bitbucket.org/BKearnsSeqirus/seqirusb2b.git $branch
    git clone --branch $branch https://tejaswini_more:Tejusm%4092@bitbucket.org/BKearnsSeqirus/seqirusb2b.git $branch

    #####################
    ## Pre-Build Steps ##
    #####################

    echo "Copy the custom folder into my user home location"
    mv -f $HOME/$branch/bin/custom $HOME/

    echo "Zip the folder with respective date and branch name"
    zip -r $gitlab_custom_$DATEFORMAT_$branch.zip custom
    echo "Zip is completed"

    echo "Copying custom folder into NFS location"
    mv -f $HOME/$gitlab_custom_$DATEFORMAT_$branch.zip $NFS_SHARE/

#################################
## Build and Deployment Starts ##
#################################

    echo "Stopping hybris service"
    sudo service hybris stop

    echo "Removing custom folder from Hybris home"
    sudo -H -u hybris bash -c "rm -rf $HYBRIS_HOME/bin/custom"

    #echo "Copying zip file from Home location into Hybris Home location"
    #sudo -H -u hybris bash -c "cp -rf $HOME/custom $HYBRIS_HOME/bin/"

    echo "Copying zip file from NFS share into Hybris Home location"
    sudo -H -u hybris bash -c "cp -rf $NFS_SHARE/$gitlab_custom_$DATEFORMAT_$branch.zip $HYBRIS_HOME/bin/"

    echo "Unzip the folder"
    sudo -H -u hybris bash -c "unzip $HYBRIS_HOME/bin/$gitlab_custom_$DATEFORMAT_$branch.zip -d $HYBRIS_HOME/bin/"

    echo "Moving into platform folder - $HYBRIS_HOME"
    sudo -H -u hybris bash -c 'cd /opt/hybris/bin/platform && . ./setantenv.sh && ant -version && ant clean all'

    echo "ant production command"
    sudo -H -u hybris bash -c 'cd /opt/hybris/bin/platform && . ./setantenv.sh && ant -version && ant production'

##########################
# Json File Modification #
##########################

    echo "Copy: Deployment Template folder from NFS into home location"
    cp -rf /NFS_DATA/deployment/config/$gitlab $HOME/
    cp -rf /NFS_DATA/deployment/config/template.json $HOME/

    echo "Backing up template Json file"
    cp -rf $HOME/template.json $HOME/template.json.backup && chmod 777 $HOME/template.json

    echo "Replace the ARTIFACT and ENV name with respective version in JSON file template under HOME location"
    sed -i "s/packageValue/$PACKAGE_NAME.zip/g; s/hostnameValue/$HOST_NAME/g; s/envValue/$gitlab/g; s~nfsValue~$NFS_SHARE~g;" $HOME/template.json
    mv -f $HOME/template.json "$HOME/${gitlab}_${PACKAGE_NAME}.json"
    mv -f "$HOME/${gitlab}_${PACKAGE_NAME}.json" $HOME/$gitlab/

##########################
# After Build Succeeded  #
##########################

    echo "Copy the Hybris AllExtension zip file into sqr folder"
    cp -rf $HYBRIS_HOME/temp/hybris/hybrisServer/hybrisServer-AllExtensions.zip $HOME/$gitlab/$gitlab-sqr/hybris/bin/

    echo "Copy the Hybris Platform zip file into sqr folder"
    cp -rf $HYBRIS_HOME/temp/hybris/hybrisServer/hybrisServer-Platform.zip $HOME/$gitlab/$gitlab-sqr/hybris/bin/

    echo "Renaming the folder"
    mv -f $HOME/$gitlab/$gitlab-sqr $HOME/$gitlab/$PACKAGE_NAME

    echo "Zip the sqr folder"
    cd $HOME/$gitlab && zip -r "$PACKAGE_NAME.zip" $PACKAGE_NAME
    echo "Zip is completed"

###################
# Create MD5 file #
###################

    cd $HOME/$gitlab/ && md5sum $PACKAGE_NAME.zip > $PACKAGE_NAME.md5

##########################
# Copy ZIP and JSON file #
##########################

    echo "Copying ZIP, MD5 and JSON file from HOME into NFS location"
    mv -f $HOME/$gitlab/$PACKAGE_NAME.zip $NFS_SHARE/
    mv -f "$HOME/$gitlab/${gitlab}_${PACKAGE_NAME}.json" $NFS_SHARE/
    mv -f $HOME/$gitlab/$PACKAGE_NAME.md5 $NFS_SHARE/

if [ "$HOST_NAME" = "sqr-q-ma-app-001" ]; then
        echo "Starting Hybris Service"
        sudo service hybris start
fi

#########################
## Clean-Up Activities ##
#########################

echo "Removing local working copy"
rm -rf "$HOME/$branch"
rm -rf $HOME/*json*
rm -rf $HOME/$gitlab
rm -rf $HOME/custom

#########################
## Database Update ######
#########################

if [ "$dbUpdate" = "yes" ]; then
    echo "Database is updating"
    #sudo -H -u hybris bash -c 'cd $HYBRIS_HOME/bin/platform && . ./setantenv.sh && ant -version && ant updatesystem -Dtenant=master -DconfigFile=$HOME/dbUpdate.json'
    #tail -f "$HYBRIS_HOME/log/tomcat/console.log"
else
   echo "Database won't update"
fi
